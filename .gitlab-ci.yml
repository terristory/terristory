# TerriSTORY®

# Copyright © 2022 AURA-EE

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

# https://docs.gitlab.com/ee/ci/yaml/
# Définition des différentes étapes de déploiement
stages:
    - update_git
    - lint
    - data
    - test
    - build
    - deploy
    - validate
    - cleanup

# common variables
variables:
    CURRENT_ENVIRONMENT: dev
    ANSIBLE_HOST_KEY_CHECKING: "false"
    ANSIBLE_FORCE_COLOR: "true"
    # if necessary, those variables can be used to skip one or many tests steps.
    # SKIP_TEST_STRATEGY: "true"
    # SKIP_TEST_API: "true"
    # SKIP_TEST_FRONT: "true"
# we define when the pipeline will run
workflow:
    rules:
        - if: $CI_COMMIT_REF_NAME =~ /^test-.*$/
          variables:
              CURRENT_ENVIRONMENT: test
          when: always
        - if: $CI_COMMIT_REF_NAME =~ /^prod-.*$/
          variables:
              CURRENT_ENVIRONMENT: prod
          when: always
        - if: $CI_COMMIT_REF_NAME == "master"
          when: always
        - when: never

###
# COMMON JOBS
###
.common:
    tags:
        - hetzner-dev
    before_script:
        - cd /home/ansible/terristory-ansible
        - source /home/ansible/venv-ansible/bin/activate

# Specific rules for DEV ENVIRONMENT
.rules_dev_strategy: &rules_dev_strategy
    - if: $SKIP_TEST_STRATEGY == null && $CI_COMMIT_REF_NAME == "master"
      changes: # we changed something to the strategy module
        - terriapi/terriapi/controller/actions.py
        - terriapi/terriapi/controller/strategy_export.py
        - terriapi/terriapi/controller/strategy_actions/**/*
        - terriapi/tests/strategy/**/*


# Specific rules for TEST ENVIRONMENT
.rules_test_strategy: &rules_test_strategy
    - if: $SKIP_TEST_STRATEGY == null && $CI_COMMIT_REF_NAME =~ /^test-.*$/
      changes: # we changed something to the strategy module
        - terriapi/terriapi/controller/actions.py
        - terriapi/terriapi/controller/strategy_export.py
        - terriapi/terriapi/controller/strategy_actions/**/*
        - terriapi/tests/strategy/**/*

###
# MAIN JOBS
###

# First step -> to save current alembic environment version and update local repository from git version 
update_git:
    extends:
        - .common
    stage: update_git
    script:
        # we store the alembic current version inside an environment variable
        - |
            ALEMBIC_VERSION=$(ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags get_alembic_version |
                       grep -o '"version_num": "[^"]*"' |
                       awk -F': ' '{print $2}' |
                       tr -d '"')
        - echo "ALEMBIC_VERSION=$ALEMBIC_VERSION" > "$CI_PROJECT_DIR/alembic_version.env"
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --extra-vars "app_git_branch=$CI_COMMIT_REF_NAME" --tags update_git
    artifacts:
        reports:
            dotenv: alembic_version.env

# Second step, linting the source code to check that everything is well set
linting:
    extends:
        - .common
    stage: lint
    script:
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags linting
    rules: 
        - if: $CURRENT_ENVIRONMENT != "prod"

# Third step: testing api
test_api:
    extends:
        - .common
    stage: test
    resource_group: api_testing
    script: # Liste des commandes
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags test_api
    # Code exécuté seulement si on est sur la branche master, test-xxx ou prod-xxx
    coverage: /\s+"TOTAL.*\s+(\d+\.\d+%)"/
    rules: 
        - if: $SKIP_TEST_API == null && $CURRENT_ENVIRONMENT != "prod"

# Third step: testing strategy module more extensively
# only applied for dev and test branches when 
test_strategy:
    extends:
        - .common
        # no dev here because we do not want only to be conflicted with rules
    stage: test
    resource_group: api_testing
    script: # Liste des commandes
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags test_strategy
    rules:
        - *rules_dev_strategy
        - *rules_test_strategy

# Third step: testing front
test_front:
    extends:
        - .common
    stage: test
    script:
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags test_front
    coverage: /Final code coverage for front is ([\d\.]+)\%/
    rules: 
        - if: $SKIP_TEST_FRONT == null && $CURRENT_ENVIRONMENT != "prod"

# Fourth step: save current API results
test_data_api:
    extends:
        - .common
    stage: data
    script: # Liste des commandes
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags test_data

# Fifth step: building front
build_front:
    extends:
        - .common
    stage: build
    script:
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags build_front
    artifacts: # Les artefacs sont un genre de cache que gitlab va sauvegarder, et qui va pouvoir être partagé avec d'autres partie du script
        expire_in: 1 week
        name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
        paths:
            - front/build/*

# Fifth step: building api
build_api:
    extends:
        - .common
    stage: build
    script:
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags build_api
    artifacts:
        expire_in: 1 week
        name: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
        paths:
            - terriapi/build/*

# Sixth step: deploying built versions to server
deployment:
    extends:
        - .common
    stage: deploy
    allow_failure: false
    script:
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags deploy
    dependencies: # Liste des tâches sur lesquelles on va chercher les artefacts sauvegardés
        - build_front
        - build_api
    rules:
        - if: $CI_COMMIT_REF_NAME =~ /^test-.*$/
          when: manual
        - if: $CI_COMMIT_REF_NAME =~ /^prod-.*$/
          when: manual
        - if: $CI_COMMIT_REF_NAME == "master"
          when: on_success

# Seventh step: checking data returned by the API
check_data_api:
    extends:
        - .common
    stage: validate
    script: # Liste des commandes
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags check_data

# Optional step, sending mail
notify_deployment:
    extends:
        - .common
    stage: validate
    script:
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags notify_deployment
    rules: 
        - if: $CURRENT_ENVIRONMENT == "prod"

# Optional step, building documentation
build_doc:
    extends:
        - .common
    stage: build
    script: # Liste des commandes
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags build_docs
    rules: 
        - if: $CURRENT_ENVIRONMENT != "test"

# Optional step, when something failed, we need to cleanup the database to be able to restart the database
cleanup_job:
    extends:
        - .common
    stage: cleanup
    script:
        # we retrieve alembic version stored in environment variable and then reroll if necessary
        - ansible-playbook -i inventory/terristory-$CURRENT_ENVIRONMENT.yml --user ansible playbook-deploy.yml --tags reroll_bdd -e "alembic_version=$ALEMBIC_VERSION"
    # only when something bad happened
    when: on_failure
    # we need alembic version
    dependencies:
        - update_git
