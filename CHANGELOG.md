# Changelog

Tous les changements notables sont documentés dans ce fichier.

## Version 2025.01 - 2025-02-13

### Added

- #2378 Ajout d'un simulateur EnR
- #2587 Ajout des iframes dans les tableaux de bord
- #2650 Mise à jour automatique de l'indicateur cartographique de l'avancement des PCAET à partir d'un indicateur commun

### Changed

- Amélioration de la page des objectifs supra-territoriaux dans l'interface admin
- Refactorisation du code des simulateurs
- Application d'isort et d'autoflake sur les fichiers du back
- Passage à vite pour le build du front
- Passage à vitest pour les tests du front
- Gestion de l'activation / la désactivation / l'absence d'un simulateur

### Fixed

- #2332 Activation du filtrage par catégorie dans l'agrégation PCAET 
- #2378 Plusieurs corrections :
  - Insertion des CustomFilter dans la récupération des données pour éviter les doublons éventuels sur la consommation
  - Correction d'un souci sur la différence max-year
  - Correction d'une faute d'orthographe dans le potentiel métha : "méthanisatione"
- #2631 Améliorer l'aide et la gestion des erreurs dans la page d'administration de PCAET
- Correction des indicateurs avec des filtres par défaut dans le PCAET relatif et l'agrégation

## Version 2024.12 - 2025-01-20

### Added

- #2588 Visualiser l'historique d'un indicateur dans le bandeau inférieur :
  - Structuration des graphiques de la carte
  - Séparation du chargement des données de certains graphiques et de celles de la carte
- #2617 Ajout d'un socle commun d'indicateurs, de catégories et d'équipements pour compléter celui des tables de données
- #2591 Ajout d'une échelle de couleur fixe pour les cartes choroplèthes
- Ajout d'une nouvelle API partageable au format OpenAPI
- Permet de ne pas afficher de valeurs agrégées sur la carte / dans les tableaux de bord
- Ajout de nouvelles variables permettant de sauter des tests lorsque cela est nécessaire

### Changed

- #2332 Amélioration de l'agrégation des PCAET :
  - Ajout de la méthodologie relative dans l'agrégation
  - Retrait de l'association automatique pour les SIREN
  - Migration vers la nouvelle api de l'ADEME : https://data.ademe.fr
- Simplification de la distinction entre les régions « nationales » et les autres

### Fixed

- #2423 Intégration de la chaleur fatale dans les calculs PCAET
- #2634 correction d'un bug où le nom de l'indicateur n'est pas mis à jour
- #2642 Correction d'un bug lors de la sélection directe du maximum du curseur
- Correction de la validation erronée sur les liens externes
- Correction de la création d'un Sankey (l'année n'était pas un int)
- Ajout d'une aide à la création de Sankey dans le panneau d'administration
- Permet l'utilisation de versions plus récentes des Sankeys produits par open-sankey.fr (en utilisant une structure de diagramme différente)
- Ne sélectionner que les administrateurs régionaux (et non les utilisateurs nationaux) lors de l'envoi de CHANGELOG
- Correction de la configuration de l'API pour les serveurs distants utilisant la nouvelle version de Sanic
- Correction des autorisations associées aux URLs

## Version 2024.11 - 2024-12-13

### Added

- #2332 Création d'un panneau indépendant pour l'agrégation des PCAET à une échelle départementale ou régionale (pour l'instant réservé aux administrateurs, en phase de test)
- #2608 Formulaire d'inscription à la newsletter nationale sur la page d'accueil

### Changed

- #2332 Amélioration de l'agrégation des PCAET :
  - Déplacement de cette fonctionnalité dans le menu "Analyses territoriales" (sous "Profil du territoire")
  - Meilleur format de nombres
  - Préciser l'année dans l'intitulé de la colonne de conso/ges/EnR observé
  - Permettre de choisir le périmètre d'agrégation (département + région dans un 1er temps)
  - Permettre de retirer manuellement des EPCI de l'agrégation

### Fixed

- #2335 Diverses corrections sur le module de stratégie :
  - Popup d'ancienne stratégie affiché si on définie une année d'action > année minimum 
  - NaN dans trajectoire si on déroule le summary trop tôt et qu'on modifie à la souris
- #2599 Pour les territoires n'ayant pas tous les secteurs, il n'est pas possible de voir les objectifs car certains secteurs ne sont pas désactivables 
- #2599 Les affectations d’objectifs supra sur les territoires sont réinitialisés à la modification de l’objectif
- #2612 l'impact total de la valeur ajoutée (Région + collectivité locale) n'est pas correct (ex: 1.912[Object object] au lieu de 0,912) 
- #2623 le dropdown qui permet de supprimer/dupliquer/partager une stratégie est variable selon le défilement de la page
- Autres corrections diverses
  - Les filtres de la représentation "suivi de trajectoire" ne sont pas appliqués à l’ouverture du tableau de bord
  - La duplication de stratégie ne fonctionnait pas (création d’une stratégie vide à la place)
  - Crash en Bretagne sur l’agrégation de PCAET à cause de la colonne population
  - Correction du formulaire d'édition/création d'indicateurs
    - Correction de l'ajout d'une catégorie
    - Correction de la valeur initiale pour le type de saisie de la sélection de l'ordre et de l'année
  - Corrige un problème sur l'édition des liens externe dynamique
  - Problème de migration sur les liens externes

## Version 2024.10 - 2024-11-03

### Added

- #926 Intégration de la cartographie dans des sites externes via iframe
- #2270 Ajout d'un bloc liens internes depuis les tableaux de bord vers tous les autres modules
- #2335 Améliorations du module :
  - Saisie de trajectoires en %
  - Modification de trajectoires en déplaçant les points sur le graphique
  - Sécuriser la sauvegarde : warning (ou sauvegarde auto) en sortant de la page
  - Graphique d'impacts production vs consommation
  - Duplication de stratégie
  - Renommage de stratégie
- #2383 Message d'avertissement lorsqu'on charge une ancienne stratégie
- #2542 Ajout d'un bloc de liens externes dynamiques (selon le territoire) depuis les tableaux de bord (uniquement pour les administrateurs régionaux)
- #2586 Sélectionner la catégorie d'un indicateur avec un slider
- #2599 Déclinaison trajectoires supra-territoriales par secteur et par type d'EnR
- Ajout des modules CSS pour personnaliser le style par module

### Changed

- #2586 Réorganisation de certaines parties du front et du back pour la généralisation des options associées à l'affichage d'une catégorie sur la carte

### Updated

- #2242 Mise à jour de chart js vers la version 4
- Mise à jour de plusieurs librairies du front :
  - React v18.3.1
  - rc-slider v11.1.7
  - react-table v8

### Fixed

- #2568 Correction de l'affichage de certains indicateurs en histogramme quand valeurs négatives
- #2604 Bug de calcul d'impact de la production ENR
- #2607 Disparition du champ "Année" et "Total" de la carte après navigation vers une autre page
- #2610 Enregistrement des filtres non fonctionnel dans les tableaux de bord pour les trajectoires PCAET
- #2612 Problème de calculs des impacts socio-économiques
- #2613 Bug d'affichage climat réel / climat normal
- Problème de mails non envoyés -> passage à smtplib plutôt que aiosmtplib
- Région nationale perdue sur la page de contact ou après connexion
- Bug sur les anciennes stratégies où la trajectoire avant l'année de référence était modifiée de façon bizarre lorsqu'on modifiait la trajectoire après l'année

## Version 2024.09 - 2024-10-03

### Changed

- #2481 Amélioration de l'export PDF des tabeaux de bord

### Fixed

- #1911 Correction de la liste du tableau de bord qui n'est pas mise à jour lors d'un changement d'utilisateur 
- #2422 Ajout des valeurs de diagnostic manquantes pour les énergies renouvelables thermiques dans PCAET V2 
- #2422 Utiliser le maximum de production et de consommation pour les énergies renouvelables thermiques dans PCAET V1 
- #2493 Correction du double label dans les trajectoires supra-territoriales 
- #2598 Limiter la réduction de la consommation à 60% dans l'agriculture et l'industrie 
- #2604 Problème de courbe historique en région Bretagne
- #2605 Erreur de calcul dans les paramètres avancés de l'action de méthanisation par injection
- Correction d'un crash lors de la récupération des PCAET
- Correction de CourbesComparees lorsqu'un indicateur a un filtre par défaut
- Correction temporaire du premier caractère erroné avec les claviers AZERTY
- Ajout d'une marge à la page d'accueil sous le bouton national
- Correction d'une requête trop contraignante sur un code de territoire
- Correction d'identification manquante dans StrategyEdition
- Correction d'une condition cassant les liens externes
- Simplifier la requête d'activation d'action pour le module de stratégie
- Cacher les trajectoires qui n'ont pas encore de données
- Ajout de l'ordre des indicateurs au panel d'import de données
- Petites corrections sur la page statique Modules (ajout des titres manquants)
- Correction des impacts régionaux qui ne sont pas affichés
- Correction de l'impact sur l'emploi en remplissant les paramètres vides par des zéros

## Version 2024.08.2 - 2024-09-11

### Fixed

- Impossible de rafraichir les pages d'indicateurs ou d'y accéder depuis des liens externes
- Corrections diverses mineures sur le déploiement national

## Version 2024.08 - 2024-09-06

### Added

- Première version du déploiement national de TerriSTORY®
  - #2500 Mise en place de la structure globale de la BD
  - #2501 Développement du module d'importation
- #2493 Ajout de la visualisation des trajectoires supra-territoriales dans les tableaux de bord

### Changed

- Refontes du module de stratégie :
  - #2504 Remplace 'Tout appareil' par 'Chaudière' dans les actions de conversion
  - #2546 Refonte des actions EnR électriques
  - #2335 et #2547 Version 1 de la refonte UI des formulaires de TerriSTORY
  - Correction de la gestion des taxes
- Retrait de certains paquets NPM dépréciés
- #2461 et #2481 Amélioration de l'impression en utilisant la fonctionnalité d'impression des navigateurs

### Fixed

- #2425 Bugs accès à une stratégie territoriale depuis "Mes stratégies"

## Version 2024.06 - 2024-07-03

### Changed

- #2472 Fuite de mémoires lors de multiples requêtes

### Updated

- #2565 Nouveau logo pour l'AUE Corse

### Fixed

- #2495 Mise à zéro de données manquantes pour certains territoires pour l'indicateur d'aménagements cyclables
- #2557 Equipement qui change de rubrique sans raison
- #2560 Agrégation de catégories lors du téléchargement de certaines données
- #2562 Table territoire surprotégée
- Correction de l'export avec de la confidentialité et un filtre automatique appliqué
- Correction d'un problème de clé primaire sur la table de périmètres régionaux

## Version 2024.05 - 2024-06-10

### Changed

- #2415 Simplification de l'interface et amélioration du système de mise à jour des périmètres territoriaux
- #2550 Modification de la structure de gestion de la confidentialité pour augmenter la flexibilité

### Fixed

- #2550, #2551 et #2552 Corrections dans l'import et l'application de la confidentialité dans certains cas
- #2554 Cartes de flux WMS disparaissant à l'impression / export PDF

## Version 2024.04 - 2024-05-14

### Added

- #2401 Modification du panel admin pour la gestion des tables de données :
  - Ajout d'une fenêtre pour uploader une nouvelle table de données indépendamment de tout indicateur
  - Permet de forcer la mise à jour d'une table de données avec des colonnes différentes
  - Ajout de plus de tests pour certains cas
- #2412 Ajout d'un outil d'association entre PCAET et territoire hors EPCI
- Ajout d'un *endpoint* de l'API pour export des données PCAET brutes
- Ajout d'OSM comme fond par défaut

### Changed

- #2412 Récupération des PCAET à toutes les échelles
- Force la langue de la page d'erreur pour éviter la traduction automatique
- Balises html dans les mails envoyés pour améliorer les scores
- Simplification des paramètres du front

### Updated

- Mise à jour de l'URL Agaric pour les nouveaux fonds
- Éléments sur la pipeline de déploiement
- Modification de certains paramètres pour les tests du front

### Fixed

- #2332 Unités corrigées sur les PCAET
- #2536 Conflit entre la trajectoire cible et la trajectoire PCAET
- #2536 Crash de la page des stratégies lors de la modification des trajectoires cibles
- #2537 Crash du tableau de bord en créant deux représentations différentes

## Version 2024.03.2 - 2024-04-15

### Fixed

- Reset map background color

## Version 2024.03 - 2024-04-12

### Added

- Permettre d'avoir des polygones comme POIs
- #2361 Ajouter plus de contexte à certaines actions
- Ajout de tests préliminaires sur quelques composants
- Afficher la date de création des comptes utilisateurs dans le panneau d'administration

### Changed

- #2256 Modification du panneau d'administration pour la gestion des icônes sur les POI :
  - Permet d'ajouter plusieurs icônes en utilisant les valeurs saisies par l'administrateur (dans la colonne typologie)
  - Slugifie les modalités utilisées pour créer les icônes
  - Ajout de tests pour POIManage et POIEdition (100% et 85% de couverture)
- #2398 Sélection automatique du premier mail disponible (configurable dans regions_configuration)
- Autorise un joker dans la colonne layer afin qu'un utilisateur puisse recevoir des notifications pour toutes les couches.
- Exclut l'administrateur global de la liste des utilisateurs
- Restriction de la liste des scénarios publics affichés aux scénarios de la région actuelle.

### Fixed

- #2517 La confidentialité ne fonctionne pas correctement lorsque l'on est connecté en tant qu'administrateur.
- #2518 Confidentialité incorrecte avec les champs de sélection
- #2521 Correction des sous-indicateurs multiples
- Correction de l'importation des données de flux depuis la base commune
- Correction d'un problème avec les actions désactivées (5 et 13) dans l'impact sur les emplois
- Forcer la version de sphinx pour éviter les incompatibilités

## Version 2024.02 - 2024-03-12

### Added

- #1538 Export en GIS des données des couches
- #1670 Ajout d'un sous-indicateur (panel admin, carte, graphiques)
- #2332 Base de fonctionnalités d'agrégation des PCAET d'une région dans le panel admin
  - dont table de méta-données des PCAET

### Changed

- #1819 Retrait du timeout inutile de loadAnalysis
- #2361 Mise à jour du système de chargement de paramètres de chauffage pour le cas de la Bretagne
- #2412 Début de généralisation du système de récupération des PCAET
- #2511 Édition possible de n'importe quel POI lorsque plusieurs POI se chevauchent en déplaçant le bouton d'édition
- Identifiants en minuscules pour éviter les doublons ou les problèmes de mails
- Réorganisation des fichiers :
  - Suppression de l'ancien module de stratégie et corrections diverses associées
  - Réorganisation des fichiers de fixtures pytest en décomposant `conftest.py`
  - Suppression du code obsolète utilisé pour intégrer les données dans les versions précédentes
- Changement de la taille des icônes affichées dans la légende pour éviter qu'elles ne soient déformées
- Gestion des onglets dans l'interface admin

### Updated

- Mise à jour de la documentation pour intégrer la nouvelle version de python

### Fixed

- #2502 Perte de l'indicateur dans l'URL lors de la sélection d'équipements
- #2503 Correction d'un bug où le bloc "trajectoire PCAET" ne pouvait pas être édité dans les tableaux de bord
- #2507 Réinitialisation de la couche VT
- #2511 Bug de chargement de la structure associé à une couche POI lorsque l'on quitte puis retourne sur la carte
- #2518 Correction des objectifs supra territoriaux ne prenant pas en compte les filtres personnalisés
- #2520 Utilisation du bon paramètre pour la récupération des données des PCAET
- Correction du script de mise à jour de PCAET
- Correction et refactorisation de get_export_geo_dataframe

## Version 2024.01 - 2024-02-12

### Added

- #2361 Améliorations diverses :
  - Autoriser les sous-actions optionnelles
  - Possibilité de désactiver les sous-actions
- #2486 Gestion des indicateurs avec commutateur / champ de sélection dans les autres modules que la carte
- #2361 Possibilité de charger des descriptions personnalisées pour les actions à partir de la configuration régionale

### Changed

- #2370 Meilleure gestion des filtres sur la carte pour éviter une différence entre le graphique affiché et le contenu de la carte
- #2453 Nouveau système de calcul de la balance des produits pétroliers dans les diagrammes Sankey en région Auvergne-Rhône-Alpes
- #2482 Ordre des secteurs figé dans les diagrammes de comparaison dans les Tableaux de Bord

### Fixed

- #2361 Corrections sur le module de stratégie territoriale :
  - Correction des vecteurs manquants dans l'impact de la production d'énergie
  - Du graphique production vs consommation avec des années manquantes
- #2485 Correction dans la représentation des indicateurs avec ratio dans les tableaux de bord
- #2490 Problème d'unité dans les indicateurs avec ratio dans les tableaux de bord
- #2487 Impact incohérent temporellement dans la stratégie territoriale
- Correction de l'évolution activée par défaut sur les courbes empilées
- Correction d'un problème d'import circulaire
- Correction de la sortie de la page de stratégie après exécution qui causait un bug
- Correction de la réinitialisation de l'unité lors du filtrage des données ou de la focalisation sur un territoire
- Correction de l'unité et de l'année qui ne sont pas changées lorsqu'on revient à une unité précédemment sélectionnée

## Version 2023.12 - 2024-01-10

### Added

- #812 et #1007 Page d'inscription à la newsletter
- #1136 Affichage de la charte graphique régionale dans l'export du module de synthèse territoriale
- #1710 Ajout d'une routine pour la mise à jour des PCAET
- #2326 et #2395 Ajout d'un composant de gestion du SEO et de métadonnées différenciées
- #2467 Insertion des fiches méthodologiques Indicateurs Air en région Auvergne-Rhône-Alpes
- Ajout d'une page 404 pour certains cas
- Ajout d'une indicateur de chargement au module de synthèse territoriale

### Changed

- #2405 Ajout d'un espace dans le tableau de bord autour de la comparaison
- #2410 PDF unique pour les documents méthodologiques en région Nouvelle-Aquitaine
- #2446 Incohérence (e) et * dans certains graphiques
- #2460 Harmonisation des unités dans les impacts du module de stratégie
- #2474 Améliorations pour le déploiement du simulateur dans d'autres régions 
- Meilleure gestion des couleurs dans le style appliqué aux plateformes régionales
- Refactorisation du gestionnaire de routes du front
- Amélioration de la localisation du bouton de retour à la carte

### Updated

- #2469 Mise à jour de la taille des icônes de la couche géothermie en région Auvergne-Rhône-Alpes
- #2452 Mise à jour des diagrammes Sankeys biogaz en région Auvergne-Rhône-Alpes
- #2479 Unité dans le fichier d'export des résultats pour les polluants atmosphériques en région Auvergne-Rhône-Alpes

### Fixed

- #2468 Trajectoire PCAET avec un objectif passé
- #2480 Correction d'un bloc de texte non modifiable
- #2483 Impossible d'ajouter des groupes d'indicateurs dans les tableaux de bord dans certains cas
- Correction de l'affichage des *choropleth_cat* dans les tableaux de bord
- Correction de la reprise de l'édition d'un tableau de bord après retour sur la carte

## Version 2023.11.3 - 2023-12-13

### Changed

- Amélioration des temps de calcul du module de stratégie territoriale

### Fixed

- Correction d'un bug lors de l'absence de données dans des actions cochées

## Version 2023.11.2 - 2023-12-07

### Fixed

- Éléments de tableaux de bord non fonctionnels

## Version 2023.11 - 2023-12-07

### Added

- #2264 Ajout d'une fonctionnalité permettant la bascule entre deux modalités d'un indicateur (climat réel / climat normal)
- #2264 Ajout d'une option pour désactiver l'utilisation d'un indicateur dans les tableaux de bord
- #2277 Éléments de chargement sur les boutons d'export
- #2286 Génération de documents plus complets à partir des tableaux de bord avec l'ajout de blocs de texte enrichi
- #2286 Ajout du partage de tableau de bord en lecture seule
- #2316 Ajout d'un mode de représentation par catégories qualitatives pour l'affichage des PCAET
- #2333 Changement de la borne minimale des cercles proportionnels sur la carte disponible depuis l'interface admin
- #2346 Encart lorsqu'aucun impact polluant disponible pour les actions sélectionnées
- #2359 Ajout de certaines étapes sur la pipeline
- #2362 Prise en compte des unités régionales dans les calculs du simulateur
- #2384 Ajout d'une présentation d'une catégorie associée à un indicateur sous la forme d'un champ déroulant dans l'encart de droite
- #2429 Ajout des fiches territoriales départementales et SCOT en région Auvergne-Rhône-Alpes

### Changed

- Amélioration de la fonctionnalité Open Energy Map :
    - #858 Gestion des droits utilisateurs sur les modifications d'installations, les utilisateurs pouvant être associés indistinctement à plusieurs couches et recevoir les notifications associées ;
    - #1706 Séparation des contributions en fonction des couches
    - #1770 Ajout de contraintes aux champs modifiables
- Améliorations graphiques des blocs de droite et des icônes de la barre de navigation ainsi que de plusieurs éléments de la carte
- #1090 Meilleure gestion des erreurs retournées par React et systématisation du recours à Api.callApi dans le front
- #2313 Mise à jour des mentions légales
- #2340 Gestion de l'envoi de mails selon plusieurs providers
- #2391 Agrandissement des cartes dans les TdB
- #2400 Format d'export des catégories compatible avec le format d'import
- #2421 Amélioration de la vérification du reCAPTCHA
- #2430 Suppression des PDF méthodologiques en région Bretagne
- #2457 Amélioration de la gestion des tests du module de stratégie
- #2459 Ajout de décimales dans le module de stratégie en région Occitanie

### Updated

- #1843 et #2420 Meilleur système de calcul des indicateurs Part des surfaces bio et Part d'espace naturel en région Auvergne-Rhône-Alpes
- #2121 Corrections apportées au chantier PCAET
- #2176 Mise à jour des informations relatives aux producteurs de données en région Auvergne-Rhône-Alpes
- #2413 Mise à jour du potentiel PV en région Auvergne-Rhône-Alpes
- #2434 Mise à jour du document d'aide à la création de tableau de bord

### Fixed

- #1670 Correction des indicateurs de flux vers des territoires extra-régionaux
- #2205 et #2441 Unités non cohérentes à la maille communale
- #2211 et #2442 Correction du simulateur pour les variations faibles de leviers
- #2229 et #2443 Corrections apportées aux flux WMS
- #2363 Corrections autour du remplissage du module de synthèse territoriale
- #2375 Affichage d'installation en projet non pris en compte en région Occitanie
- Nombreux fix de bugs dans le module stratégie territoriale
    - #2280 Suite du déploiement du module de stratégie territoriale en région Pays de la Loire
    - #2389 Impossible de charger une ancienne stratégie
    - #2407 Problème d'impacts socio-éco sur l'action de réduction des GES non énergétique dans l'agriculture
    - #2428 Impossible de remplir les paramètres de performance
    - #2431 Bug de certaines actions sur le nouveau module de strat
    - #2432 Paramètres économiques avancés non pris en compte dans l'impact emploi dans le module stratégie territoriale
    - #2447 Erreur de prise en compte de l'hydroélectricité > 4,5MW en région Occitanie
    - #2451 Erreur dans l'export excel format ADEME
- #2449 Erreur dans les tableaux bord pour l'indicateur "Infrastructure cyclable" en région Auvergne-Rhône-Alpes

## Version 2023.10.2 - 2023-11-14

### Changed

- #2426 Passage à Python 3.9 pour utiliser une version plus récente de la librairie d'envoi de mails

### Fixed

- #2211 Corrections de cas spécifiques sur le Simulateur mobilité
- #2441 Unités non cohérentes à la maille communale
- Bug irrégulier sur les unités
- Corrections diverses sur la pipeline.

## Version 2023.10 - 2023-11-07

### Added

- Modification des unités :
  - #1120 sur la carte via un menu déroulant
  - #2205 première version pour l'affichage de la maille communale et l'utilisation du simulateur
- #1399 Ajout des différentes modalités et des données maille/territoire dans l'export de données
- #1558 Ajout de la date de la dernière mise à jour de la donnée dans la colonne gestion des données de l'interface admin
- #2121 Amélioration du suivi PCAET et de la comparaison objectif / réel
  - ajout de visualisations dans les tableaux de bord
  - amélioration de la méthodologie de calcul en région Occitanie
  - intégration des informations par secteur (consommation et émissions) et par filière (production)
- #2211 Ouverture de la maille communale
- #2229 Intégration de l'affichage de flux WMS
- #2350 Ajout de liens externes dans le menu Analyses (par ex., vers le centre de ressources et la fiche trajectoire en région Auvergne-Rhône-Alpes)
- #2368 Affichage de la donnée en % dans l'infobulle des camemberts du module Indicateur

### Changed

- #1192 Éléments sur git et gitlab dans la documentation technique
- #2157 Retrait de la prospective sur les prix de l'énergie dans le module de Stratégie territoriale
- #2350 Modification des titres Synthèse des indicateurs territoriaux

### Updated

- #2287 Mise à jour des données du Sankey Biogaz en région Auvergne-Rhône-Alpes
- #2307 Uniformiser l'usage des données logements en région Auvergne-Rhône-Alpes

### Fixed

- #2098 Vérification des calculs sur la conversion de chauffage dans le module de Stratégie territoriale en région Occitanie
- #2280 et #2402 Correction de la représentation du mix de chauffage dans les actions correspondantes
- #2368 Bug de rechargement sur les courbes empilées lors de la modification de la légende
- #2385 Plusieurs problèmes sur la facture énergétique en région Auvergne-Rhône-Alpes
- #2399 Bug de calcul sur le covoiturage dans le simulateur mobilité
- #2404 Bug d'actualisation des tableaux de bord avec comparaison

## Version 2023.09.3 - 2023-10-19

### Fixed

- Problème de mise à jour des PCAET

## Version 2023.09.2 - 2023-10-10

### Fixed

- #2394 Graphiques des indicateurs climatiques non fonctionnels

## Version 2023.09 - 2023-10-09

### Added

- #2121 Amélioration de la méthodologie de calcul des trajectoires de PCAET
- #2331 Ajout d'une documentation au simulateur de mobilité

### Changed

- #2329 Corrections apportées et changements sur le simulateur de mobilité :
  - unités
  - organisation en base de données
  - leviers non pertinents cachés
  - ajout d'un message d'alerte pour les régions avec des données manquantes
- #2321 Page affichée au cours du chargement des tableaux de bord
- #2347 Correction d'un libellé imprécis dans le tableau d'action Réseau de chaleur EnR/gaz en région Auvergne-Rhône-Alpes

### Updated

- #1994 Mise à jour de certaines données du diagramme Sankey d'énergie en région Auvergne-Rhône-Alpes
- #2349 Mise à jour des données du solaire thermique en région Auvergne-Rhône-Alpes

### Fixed

- #2094 Lien de mot de passe oublié cassé
- #2308 Bug dans les indicateurs dépendants de certaines tables
- #2329 Corrections apportées et changements sur le simulateur de mobilité :
  - action sur la part modale
  - bascule de curseurs
- #2351 Bug de retour à la carte depuis le simulateur
- #2367 Correction de l'affichage de la couverture des tests
- #2369 Bug d'affichage sur les indicateurs climats
- #2371 Problème lors du téléchargement du fichier de données en région Corse
- #2372 Réécriture de l'URL du sankey biogaz vers le sankey énergie
- #2374 Échec de l'upload de tables sur le serveur de dev
- #2376 Invalidité de la date de modification des PCAET
- #2381 et #2382 Bug dans le fonctionnement des indicateurs "Distance domicile-travail" et "Migrations pendulaires"
- #2390 Impossible de charger une table dans le registre de données communes

## Version 2023.08.2 - 2023-09-11

### Fixed

- #2369 Graphiques des indicateurs climatiques non fonctionnels

## Version 2023.08 - 2023-09-08

### Added

- #2253 Nouvelles représentations graphiques :
  - diagramme en barre
  - ajout de l'évolution activable pour les courbes empilées
  - comparaison de trajectoires pour des indicateurs concaténant plusieurs données
- #2273 et #2310 Améliorations de l'interface admin :
  - API externes
    - pouvoir imposer une année comme année de la valeur (ne pas prendre en compte une colonne variable)
    - ajouter une nouvelle API / modifier une existante
    - filtrer les valeurs territoriales à partir des territoires régionaux
  - Sankey
    - ajouter un diagramme de Sankey
    - supprimer un diagramme de Sankey
  - Gestion du retrait de catégories d'indicateurs
- #2278 Changement des mailles de territoire depuis l'interface admin
- #2294 Ajout de la pré-visualisation des tables de données communes
- #2319 Intégration de métadonnées dans le pdf pour améliorer le référencement en région Auvergne-Rhône-Alpes
- #2336 Activation d'Open Energy Map
  - les utilisateurs connectés peuvent maintenant modifier une couche considérée comme modifiable
  - un admin peut être autorisé à valider/modifier/recevoir ces contributions
- #2348 Ajout de l'indicateur Labellisation Territoires engagés en région Occitanie

### Changed

- #2157 Retrait de la prospective sur les prix de l'énergie dans le module Stratégie territoriale
- #2238 Modification des données du compte utilisateur
- #2280 Suite du déploiement du module de stratégie territoriale en région Pays de la Loire
- #2311 Modification de l'ordre des icônes dans le menu latéral gauche
- #2311 Modification de l'ordre des liens vers les tableaux de bord dans le menu de gauche
- #2324 Correction mineure dans la page des mentions légales en région Auvergne-Rhône-Alpes
- Comportement lors de la modification des catégories associées à un indicateur depuis l'interface admin
- Reconstruction de la fonction de création des requêtes (`donnees_par_territoire`)
- Correction simple pour éviter d'utiliser plusieurs fois le même email lors de l'envoi du changelog après une nouvelle version.

### Updated

- #2288 Mise à jour des données INSEE en région Auvergne-Rhône-Alpes
- #2295 Mise à jour des données de facture énergétique en région Auvergne-Rhône-Alpes
- #2300 Mise à jour des réseaux de chaleur (données et tracés) en région Auvergne-Rhône-Alpes

### Fixed

- #2116 Corrections dans certains cas particuliers (aucune action cochée)
- #2208 Problème de masquage du panneau latéral
- #2317 Tri des utilisateurs par date de dernière connexion fonctionnel
- #2343 Premier affichage de la page dans les régions conduisait à une page d'erreur
- #2344 Mise à jour de l'affichage d'un indicateur après filtrage
- #2358 Correction d'un bug sur le module de stratégie en région Occitanie
- Rafraîchissement des pages de suivi de trajectoire hors trajectoire énergétique
- Correction d'un problème avec les requêtes sur de ratio pour les diagrammes circulaires
- Correction de problèmes associés à la maille communale
- Retrait de la double création de la table communale
- Jauges circulaire et territoriale non fonctionnelles sur les indicateurs limités à certaines mailles

## Version 2023.07.2 - 2023-08-22

### Fixed

- Problème d'URL lors du rafraîchissement automatique de l'authentification de l'utilisateur

## Version 2023.07 - 2023-08-21

### Added

- #2116 Généralisation des tables et colonnes dans différents modules :
  - ajout de tables de passage
  - ajout d'une structure de tests
  - ajout de paramètres modifiables en base pour remplacer les données statiques actuellement utilisées
  - ajout de deux tabs dans le panel admin pour afficher les actions, les exporter, récupérer les tables de passage et les modifier
- #2215 Changements divers autour de la maille communale :
  - utiliser la focalisation territoriale (zoom sur une maille) jusqu'à la maille communale
  - autoriser l'échelle Commune - maille commune pour les régions qui le souhaitent

### Changed

- #2116 Refonte du module de stratégie :
  - séparation des différentes actions
  - import automatique des données à partir de la configuration de chaque action
  - utilisation de constantes dans les calculs indépendantes de la région
  - utilisation des tables de passage pour faire correspondre aux données en base
  - fiabilisation par la désactivation automatique des actions lors d'erreurs pendant le calcul
  - corrections diverses liées à des coquilles dans les actions ou à des erreurs de conception
- #2147 Cochage/décochage instantané des filtres pour les graphiques des indicateurs
- #2250 Ajouter la liaison entre l'action "changement de motorisation bus/cars" et l'indicateur "parc de véhicules en circulation"
- #2274 Modification de l'action "Méthanisation avec injection sur le réseau de gaz"
- #2306 Rajout d'espace dans les indications d'échelles/de mailles désactivées
- #2318 Accès à la page "/support" même pour les régions où elle n'est pas configurée
- Distinction des valeurs manquantes des valeurs nulles

### Updated

- #1984 Mise à jour et reformatage des indicateurs liés aux labellisations ADEME en région Auvergne-Rhône-Alpes
- #2206 Changement du logo ENEDIS
- #2255 Mise à jour des données liées aux déchets en région Auvergne-Rhône-Alpes
- #2295 Mise à jour des données de factures énergétiques en région Auvergne-Rhône-Alpes

### Fixed

- #2118 Correction du bug d'affichage des icônes sur le fond de carte
- #2190 Réinitialisation des Equipements affichés lors du retour à la carte
- #2258 Bug dans l'affichage du total des impacts de la stratégie territoriale
- #2283 L'indicateur "Distance domicile-travail" s'affiche en dehors de la zone sélectionnée
- #2303 Correction de l'export des données part EnR/Conso en région Auvergne-Rhône-Alpes
- #2315 Manque d'une catégorie dans l'indicateur "parc de véhicules en circulation" en région Auvergne-Rhône-Alpes
- #2322 Correction d'un bug lors du téléchargement de certaines données en région Bretagne
- #2334 Correction de plusieurs bugs liés au nouveau système de table de passage
- Correction les URLs utilisateurs parfois erronées
- Correction des tabs autour du registre RTE
- Problème forçant l'activation des sankeys pour l'affichage du module d'Analyses

## Version 2023.06.3 - 2023-07-06

### Fixed

- Correction de la mise à jour du périmètre territorial impossible.

## Version 2023.06.2 - 2023-07-06

### Fixed

- #2305 Problème de mise à jour des indicateurs.

## Version 2023.06 - 2023-07-06

### Added

- #1574 et #2214 Possibilité de changement du nombre de décimales dans le module de stratégie territoriale (trajectoires et résultats)
- #1856 Ajout de la possibilité d'avoir des comptes utilisateurs avec le même mail dans plusieurs régions différentes
- #1915 Enregistrement possible des filtres lors de la création / de l'édition d'un tableau de bord
- #2050 Ajout d'une interface d'administration des diagrammes Sankey
- #2100 Ajout d'indicateurs relatifs aux bâtiments en région Auvergne-Rhône-Alpes
- #2228 Ajout d'informations sur la raison de la désactivation de certains indicateurs lors du changement d'échelle/de maille
- #2236 Amélioration de l'export des tableaux de synthèse territoriale (encodage, informations supplémentaires, inclusion du fichier méthodologique)
- #2239 Affichage du statut des données (estimées ou non) dans les fichiers d'export
- #2259 Conversion possible des couches de flux lors de la mise à jour de données d'un périmètre à un autre
- #2269 Ajout du Bilan d'activité 2023 dans la page À propos en région Auvergne-Rhône-Alpes

### Changed

- #1451 Organisation des indicateurs dans le menu de sélection dans le TdB
- #2106 Utilisation d'autres données pour la superficie en région Auvergne-Rhône-Alpes
- #2131 Retrait du pompage hydraulique dans la production EnR en région Auvergne-Rhône-Alpes
- #2233 Ordre des catégories appliqué aux diagrammes circulaires associés à la carte principale
- #2265 Corrections et améliorations apportées au simulateur de mobilité
  - Ajout d'un accès au simulateur via le menu latéral
  - Ajout des unités, du territoire en cours, d'informations supplémentaires
  - Sélection d'un impact par défaut
  - Simplification de l'interface
  - Gestion des cas de dépassement (valeurs supérieurs à 100% d'impact)
- #2271 Optimisation liée à la création d'index aux tables PostgreSQL lors de l'ajout de données, de la MAJ de données ou de la MAJ de périmètres

### Updated

- #2156 Ajout des données de population antérieures à 2020 en région Auvergne-Rhône-Alpes
- #2212 Mise à jour de la base territoire en région Auvergne-Rhône-Alpes
- #2254 Mise à jour des données des indicateurs du nb de composteurs en région Auvergne-Rhône-Alpes
- #2257 Mise à jour des données de stations GNV et bioGNV en région Auvergne-Rhône-Alpes
- #2260 Mise à jour des données de quantités de déchets collectés en région Auvergne-Rhône-Alpes
- #2272 Mise à jour des données ORCAE de consommation, production et émissions de GES et de polluants en région Auvergne-Rhône-Alpes
- #2262 Mise à jour des plaquettes nationales sur l'ensemble des régions et de la fiche régionale en région Auvergne-Rhône-Alpes

### Fixed

- #2234 Corrections diverses de bugs et refactorisation du composant SelectionObjet
- #2252 et #2284 Correction d'un problème lors de l'envoi de mails depuis TerriSTORY® et utilisation de l'adresse mail régionale d'AURA-EE
- #2279 Erreur lors de la mise à jour d'un équipement de réseaux de chaleur en région Occitanie
- #2281 Bug d'affichage du territoire dans le popup de la carte

## Version 2023.05 - 2023-06-02

### Added

- #1984 Ajout d'un outil pour questionner automatiquement une API externe et importer les données dans une table régionale
- #2148 Ajout de la mention du filtre appliqué pour les cartographies indicateur dans les tableaux de bord
- #2160 Ajout des polluants dans le simulateur de mobilité
- #2230 Possibilité de convertir une table unique dans un nouveau périmètre géographique ou d'en modifier la date indépendamment
- Ajout de tests du front de l'application
- Ajout de filtres sur plusieurs panneaux dans l'interface admin
- Ajout d'un log lors de l'envoi réussi d'un mail (inscription)
- Gestion partielle du cas commune-commune

### Changed

- #2120 Modifications sur la gestion du territoire de prédilection
- #2160 Corrections diverses autour du chargement par URL, des liens et autres éléments relatifs au simulateur
- #2212 Timeout général pour préparer des MAJ de découpages géographiques
- #2237 Retrait de l'exception facture et amélioration du système de stockage des règles de confidentialités
- #2251 Automatisation du calcul du capital fixe pour les actions relatives à l'industrie et au secteur agricol
- Travail important autour du lien entre les schémas régionaux et le schéma france (cf. fixed)
- Retrait de la librairie enzyme des tests front
- Retrait de deux boutons lors de la capture d'écran d'un tableau de bord avec comparaison
- Désactivation de la focalisation territoriale pour la maille commune (tant que le cas commune-commune n'est pas géré)

### Updated

- #2156 Ajout des données de population antérieures à 2020 en région Auvergne-Rhône-Alpes
- #2184 Mise à jour des données IRVE en région Auvergne-Rhône-Alpes
- #2232 Nom de fichier méthodologique pour les documents relatifs aux Sankeys en région Auvergne-Rhône-Alpes
- #2240 Mise à jour de la couche Mâts éoliens terrestres en région Pays de la Loire
- #2247 Statut de communes hors EPCI en région Bretagne

### Fixed

- #2107 Dysfonctionnements des données communes lors d'imports successifs
- #2222 Erreur d'affichage de l'indicateur "Infrastructures cyclables" dans un tableau de bord en région Auvergne-Rhône-Alpes
- #2243 Corrections de divers bugs relatifs aux fiches didactiques
- #2246 Correction d'un problème d'affichage (version de postile) en région Pays de la Loire
- #2248 Corrections de problèmes liés à la gestion des données communes
- Correction d'un crash lors du survol d'un POI non ponctuel sur la carte
- Correction d'une coquille dans les entêtes de fichiers d'export

## Version 2023.04 - 2023-05-12

### Added

- #1418 Exporter les données de la synthèse des indicateurs territoriaux
- #1984 Ajout d'un indicateur Labellisation Territoire Engagé Transition Ecologique en région Auvergne-Rhône-Alpes
- #2012 Activation du module Analyses territoriales sur la région Pays de la Loire
- #2120 Ajout de la notion de territoire d'un utilisateur qui peut être choisi pour être affiché par défaut à l'arrivée sur la plateforme
- #2160 Ajout d'un simulateur de mobilité
- #2166 Référence à ARIMA (donnée estimée) dans les trajectoires-cibles énergie et polluants atmosphériques
- #2191 Exporter la liste des stratégies territoriales remplies dans sa région
- #2217 Possibilité de comparer deux territoires dans des tableaux de bord
- Ajout de deux maillages géographiques supplémentaires en Nouvelle-Aquitaine (CRTE et DATAR).
- Possibilité d'ajouter des couches de pixels quantitatives dans l'interface admin

### Changed

- #1575 Améliorations diverses sur les graphiques/courbes TEPOS (prod/conso)
- #1980 Ajout d'un logo régional TerriSTORY® et de la date d'export lors de l'impression des Sankeys
- #1988 Corrections visuelles apportées aux fiches didactiques
- #2119 Corrections apportées aux données exportées et ajout aux autres modules
- #2123 Corrections sur la gestion des droits et du partage des tableaux de bord
- #2144 Changement de fonctionnement des diagrammes radar
- #2177 Corrections apportées sur les données prises en compte dans les trajectoires PCAET de production d'EnR
- #2181 Ajout d'un document méthodologique sur les Sankeys biogaz en région Auvergne-Rhône-Alpes
- #2188 Fermeture de la popup lors du zoom sur un EPCI à partir de la carte régionale
- #2189 Mise à jour du filtre lors du zoom sur un EPCI à partir de la carte régionale
- #2194 Fix temporaire sur la durée de vie des éoliennes dans le module de stratégie territoriale en attendant de corriger le problème de fond
- #2200 Thème des blocs markdown indépendant du thème du navigateur
- #1992 Comportement de l'infobulle (EPCI affiché selon la localisation du curseur de la souris)
- Centrage du logo de chargement dans le bandeau titre

### Updated

- #2053 Mise à jour des données et de la méthode de calcul des Sankeys énergie en région Auvergne-Rhône-Alpes
- #2159 Mise à jour des unités de méthanisation en région Auvergne-Rhône-Alpes

### Fixed

- #1836 Problème de mise en cache de la page principale qui empêchait la bonne mise à jour de l'application
- #2108 Décochage automatique du premier équipement coché
- #2150 Correction d'un bug sur l'indicateur Distance domicile - travail, la catégorie "Pas de transport" manquant en région Auvergne-Rhône-Alpes
- #2192 Crash de l'indicateur Accessibilité à l’emploi en région Auvergne-Rhône-Alpes
- #2195 Activation de compte : problème de réception du MdP temporaire en région Nouvelle-Aquitaine
- #2197 Tableau de bord inaccessible en région Occitanie
- #2203 Bug d'export Emission de GES avec toutes les années en région Pays de la Loire
- #2209 Problème de chargement des fiches méthodologies en région Pays de la Loire
- #2218 Récupérer toutes les zones nécessaires pour l'affichage des indicateurs à toutes les échelles
- #2227 Bug d'affichage des diagrammes radar

## Version 2023.03 - 2023-04-03

### Added

- #1914 Gestion différenciée des indicateurs climatiques pour la région Occitanie
- #1984 Ajout d'un indicateur de Labellisation Territoire Engagé Transition Ecologique en région Auvergne-Rhône-Alpes
- #2024 Ajout des sources et producteurs pour les couches de POI
- #2054 Ajout d'un diagramme de Sankey sur le biogaz en région Auvergne-Rhône-Alpes
  - Généralisation du système de Sankey
  - Ajout d'un layout de sankey pour les flux de biogaz
- #2082 Ajout des impacts en émissions de polluants dans les actions en région Auvergne-Rhône-Alpes :
  - #2083 sur le covoiturage et la réduction des trajets
  - #2084 sur les voies cyclables
  - #2086 de changement de motorisation des transports en commun
  - #2087 de rénovation
- #2116 Amélioration de l'interface administrateur :
  - ajout de la possibilité de supprimer des objets (indicateurs, couches de données, couches de POI, catégories)
  - ajout de la possibilité de trier les couches de POI dans un écran dédié
- #2119 Ajout de la possibilité d'exporter les couches d'indicateurs en fichier Excel et de sélectionner l'ensemble des années
- #2170 Permettre à un administrateur régional de connaître l'ensemble des territoires ayant renseigné une stratégie territoriale
- Ajout de la possibilité d'utiliser du Markdown dans les champs longs des tableaux de bord
- Ajout de tests automatiques pour la création d'indicateurs via l'API

### Changed

- #905 Amélioration du texte d'aide lors de modification de couches de POI éditables
- #1988 Changements divers apportés aux fiches didactiques :
  - ajout d'un niveau d'organisation
  - retrait des actions de stratégies territoriales (pour actions futures du simulateur)
- #2012 Généralisation du module d'Analyse territoriale pour l'activation future en région Pays de la Loire
- #2062 Révision des actions de changement d'équipements de chauffage
- #2082 Amélioration des visuels et textes en lien avec les polluants atmosphériques
- #2116 Amélioration de l'interface administrateur :
  - couleurs transparents acceptées (indicateurs, POI...)
  - mise à jour possible du nom des thèmes (indicateurs et POI)
  - changement de nommage des PDFs méthodologiques (utilisant le nom de l'indicateur et l'ID)
  - informations supplémentaires affichées à plusieurs endroits
- #2119 Changements visuels dans la fenêtre de téléchargement d'indicateurs
- #2172 Meilleure gestion des débuts de périodes variables pour les données climatiques
- Réorganisation importante du code source du front-end et multiples corrections :
  - retrait de certaines doubles barres de défilement
  - séparation des paramètres en deux types de fichiers plus clairs et unification des URLs
  - changement d'organisation des fichiers
- Améliorations visuelles diverses sur les tableaux de bord
- Envoi du CHANGELOG dans le script principal pour éviter d'envoyer un mail si le déploiement a échoué

### Updated

- #1973 Fichiers méthodologiques et tutoriel du module de stratégie territoriale en région Auvergne-Rhône-Alpes
- #2139 Mise à jour de l'indicateur Parc automobile statique en région Auvergne-Rhône-Alpes
- #2152 Conditions légales en accord avec l'avenant de l'accord de consortium et simplification de la structure de la page
- #2155 Données de facture énergétique en région Auvergne-Rhône-Alpes
- #2169 Tables de confidentialité en région Pays de la Loire

### Fixed

- #2130 Correction de la sélection de l'année sur le zoom des sous-territoires
- #2165 Ajout d'une table de données invisible en région Pays de la Loire
- Correction d'une petite boucle lors de l'exportation des résultats d'une stratégie avec PCAET
- Correction d'un plantage lors de l'utilisation de la carte sur l'indicateur "Polluants atmosphériques".
- Gestion de la valeur NaN pour les territoires sans données dans DonneesPourRepresentationCartoIndicateurSimple
- Correction d'une clause await manquante dans poi_properties
- Retrait de barres horizontales non nécessaires dans la légende
- Calculs erronés dans les graphiques de courbes empilées lorsque la formule intègre un ratio
- Utilisation de `request_mocks` pour les tests en lien avec l'API de l'ADEME (PCAET)
- Mauvais comportement du bouton et du back dans la procédure de mise à jour des PCAET
- Double log dans la console

## Version 2023.02.2 - 2023-03-16

Cette version est un hotfix de la version 2023.02.

### Added

- #2082 Ajout des impacts polluants atmosphériques dans le module de Stratégies territoriales

### Fixed

- #2161 Correction du partage d'URL
- #1914 Corrections mineures pour ajouter les données climatiques à la région Occitanie
- #2171 Mise à jour pop-up lors du changement d'indicateur

## Version 2023.02 - 2023-03-07

### Added

- #1988 Ajout de fiches didactiques et de logos pour enrichir les tableaux de bord
- #2012 Activation du module d'analyse territoriale en région Pays de la Loire et généralisation de la configuration de ce module (menus, activation, indicateurs, etc.)
- #2067 Ajout d'indicateurs sur les puissances EnR installées en région Auvergne-Rhône-Alpes
- #2076 Clarification du calcul du point d'ancrage des POI et ajout de l'option dans l'interface admin
- #2109 et #2145 Ajout de la possibilité d'avoir les données des mailles géographiques utilisées dans les ratios variables en fonction des années pour améliorer la pertinence des indicateurs ratios (par exemple, consommation par habitant). Ajout d'un encart prévenant lorsque l'année sélectionnée n'est pas disponible pour le dénominateur et utilisation de l'année la plus proche.
- Ajout de tests automatiques du front-end
- Point d'entrée de récupération des statistiques de consultation de toutes les régions (accessibles aux admins globaux uniquement)

### Changed

- Calcul des formules des indicateurs pour faciliter et améliorer la gestion des dénominateurs
- #1227 Changement du comportement lorsque la souris survole plusieurs objets
- #1419 Changement de l'ordre des indicateurs de polluants dans l'ensemble des éléments les affichant
- #2012 Retrait d'informations relatives au projet CESBA de la table de synthèse territoriale
- #2060 Changement de l'affichage de la trajectoire de PCAET dans le module de stratégie
- #2089 Place de la flèche de réduction du bandeau inférieur (graphiques)
- #2132 Requêtes spatiales dans le cas de combinaisons échelle-maillage hors région-X ou X-commune.
- #2142 Page ? renommée en Aide sur les régions concernées
- Retrait du composant Ol anciennement utilisé pour la carte
- Obligation de renseigner un PDF lors de la création d'une couche de POI
- Simplification des règles sur les affichages de graphiques par rapport à l'action et à la désactivation des indicateurs par zones/échelles

### Updated

- #1961 Mise à jour de la facture énergétique en région Auvergne-Rhône-Alpes
- #1973 Mise à jour du PDF méthodologique sur la stratégie territoriale
- #2037 Fin de la mise à jour des données de consommation, d'émissions de gaz à effet de serre et de polluants en région Auvergne-Rhône-Alpes
- #2126 Mise à jour du logo de TÉO Pays de la Loire
- #2127 Mise à jour du fichier d'export des résultats de stratégies territoriales en région Auvergne-Rhône-Alpes
- #2149 Correction de l'année de départ des nouvelles données de production en région Auvergne-Rhône-Alpes

### Fixed

- Grand nombre de corrections autour des PCAET dans le module de stratégie territoriale :
  - #2091 Définition de la trajectoire cible
  - #2122 Mauvaise année de démarrage de certaines actions
  - #2133 Autres soucis associés aux PCAET dans les trajectoires (=> passage de l'interpolation dans le front-end)
  - #2151 Page non chargée
- #2114 Bug du téléchargement des données et de la méthodologie lorsque le PDF est externe
- #2129 Correction de l'utilisation des années dans les indicateurs ratios
- #2136 Thématique affichée dans la légende des indicateurs
- #2137 Légende de l'indicateur sur les infrastructures cyclables
- #2143 Tables manquantes dans les périmètres géographiques de la région Auvergne-Rhône-Alpes
- Correction de bugs sur l'affichage de maps de pixels catégoriques
- Correction de bugs graphiques sur les courbes empilées et d'autres graphes Chart.js
- Correction de la page d'ajouts d'objectifs supra-territoriaux

## Version 2023.01.2 - 2023-02-13

Cette version est un hotfix de la version 2023.01.

### Changed

- #2066 Correction d'un affichage du caractère "estimé" dans les tableaux de bord

### Fixed

- #2133 Problème lors du chargement du module de stratégies territoriales suite à la MAJ des données ORCAE en région Auvergne-Rhône-Alpes
- Correction des tests associés

## Version 2023.01 - 2023-02-01

### Added

- #1874 et #1879 Ajout de représentations de cartes d'indicateurs et d'installations (par thématique) dans les tableaux de bord
- #1906 Possibilité d'éditer un type de graphique
- #1988 Ajout d'un gestionnaire de logo en prévision des pages de fiches didactiques
- Améliorations de l'interface admin :
  - #1559 Ajout des statistiques dans l'interface admin avec la possibilité de filtrer sur une plage temporelle et d'exporter les données
  - #2034 Upload possible de PDFs statiques
  - #2035 Autoriser ou interdire l'export d'indicateurs aux échelles géographiques
  - #2099 Améliorations diverses dont:
    - Afficher et exporter les données des catégories
    - Correction de la sensitivité à la casse et aux accents des fitlres des tableaux dans le panel admin
    - Exporter les données des tables régionales et des tables communes
- #2046 Ajout d'un point de vigilance "prospective" dans l'encart Stratégie territoriale
- #2066 Ajout de la possibilité de spécifier des années estimées pour un indicateur
- #2083 et #2084 Ajout des impacts sur les polluants pour des actions liées à la mobilité
- Ajout de deux nouvelles statistiques sur la page Analyse et Stratégie territoriale

### Changed

- #1090 Ajout d'une page pour remplacer les pages blanches dans certains cas
- #1559 Utilisation de la saisie de date au lieu du texte pour la sélection de la date dans le module de statistiques
- #1874 et #1879 Refonte complète du système de cartographie
- #2054 Généralisation des diagrammes de Sankey pour pouvoir disposer de plusieurs diagrammes distincts
- #2068 Amélioration de la qualité des graphiques exportés
- #2095 Augmentation du taux de couverture de test de 52% à 69%
- #2099 Amélioration de la sécurité sur certains points d'entrées de l'API
- Cacher les éléments de la légende s'ils ne sont pas disponibles (année et total)
- Afficher les informations estimées dans le tableau de bord et la page CESBA
- Retrait des appels à des APIs externes dans les tests
- Ajout de la notion de minimum et de maximum pour les indicateurs visualisés sous forme de cartes d'étoiles pour éviter des échelles erronées lors de données lacunaires

### Updated

- #2086 et #2087 Mise à jour des méthodes de calcul pour les impacts sur les polluants pour deux actions (rénovation et changement de motorisation)

### Fixed

- #2058 Difficulté à afficher/cacher les couches d'équipements
- #2096 Retrait de la possibilité d'utiliser la colonne Typologie pour différencer les groupes d'une couche d'équipements (seule type reste utilisable)
- #2097 Changement de la méthode de filtrage des données pour prendre en compte le type de territoire lors de ratio
- #2105 Correction de la carte ne se met pas à jour lors du changement de classMethod
- #2110 Cacher certains onglets qui ne devraient pas être visibles aux utilisateurs non administrateurs
- Correction du filtrage IP sur les statistiques utilisateurs
- Correction de la légende des chloroplèthes lors du filtrage sur le sous-territoire
- Correction de l'affichage d'une popup vide sur un fond osm
- Correction du z-index des boutons (impression et plein écran) sur la page des Sankeys
- Correction d'un crash en cas d'absence d'analysisManager et de dataReference
- Correction d'un canvas qui reste sur la page après avoir pris une photo d'un graphique.
- Correction des données manquantes dans la légende de droite lors du chargement depuis une URL.
- Récupération des données sur le module CESBA même si le module de consultations n'était pas disponible initialement.

## Version 2022.12.3 - 2023-01-11

Cette version est un hotfix de la version 2022.12.

### Fixed

- #1919 Corrections de bugs lors de l'interaction filtres - année - changement de périmètre
- Canvas persistent après l'enregistrement en PNG d'un graphique ou en PDF d'un tableau de bord
- Boutons associés au Sankey en arrière-plan

## Version 2022.12.2 - 2023-01-06

Cette version est un hotfix de la version 2022.12.

### Fixed

- #2015 Ouverture des liens de la page Open-Source dans une nouvelle page
- #2065 Retrait d'un effet de style sur la page d'accueil.
- #2078 Page blanche à l'arrivée sur la page CESBA
- Nouveaux logos de meilleure qualité
- Corrections des tests d'appels automatiques à l'API qui utilisaient une année de façon erronée
- Amélioration de l'outil de captures d'écran automatiques

## Version 2022.12 - 2023-01-04

### Added

- #1919 Pouvoir choisir l'année de visualisation d'indicateurs ayant plusieurs années activées
- #1930 Ajout de la possibilité de partager un tableau de bord à une ou plusieurs autres personnes qui peuvent alors le modifier
- #1952 Ajout de couches de besoins de chaleur et de potentiels géothermiques en région Auvergne-Rhône-Alpes
- #1960 Ajout des impacts sur les polluants atmosphériques des actions mobilité
- #2004 Ajout de la licence aGPL v3 en entête des fichiers du code source et à la racine du dépôt
- #2015 Ajout d'une page Open-Source relative à l'ouverture du code de l'application
- #2040 Possibilité d'ordonner les graphiques associés aux catégories d'un indicateur
- #2051 Ajout d'une troisième source (Production des données) paramétrable pour qualifier un indicateur

### Changed

- Désactiver la fenêtre d'informations de l'écran d'accueil peut se faire en cliquant n'importe où dans la zone grisée.
- #1914 Corrections mineures pour généraliser l'ajout de données climatiques à la région Occitanie
- #1991 Changement de la méthode de mise à jour des contours territoriaux à partir d'un fichier téléchargé sur le site de l'IGN
- #2003 Meilleure représentation de l'indicateur "Nom simple" dans les tableaux de bord
- #2014 Clarification du fichier README pour un meilleur aiguillage des potentielles contributions
- #2023 Harmonisation des noms des modules dans les menus
- #2027 Révision de l'action de changement de motorisation bus/cars
- #2047 Simplification de l'information de total dans la cartouche relative aux indicateurs
- #2048 Amélioration de la gestion des zéros dans les trajectoires PCAET
- #2056 Clarification de la méthode d'upload de couches non ponctuelles d'équipements

### Updated

- #1964 Mise-à-jour des données principales pour la région Occitanie
- #2052 Mise-à-jour des contours géographiques des territoires en région Bretagne
- #2059 Correction des liens méthodologiques des principaux indicateurs pour la région Occitanie
- #2034 Corrections de la page d'A propos en région Auvergne-Rhône-Alpes

### Fixed

- #2001 Mauvais affichage des titres et textes lors de l'export en PNG ou PDF des graphiques dans les tableaux de bord
- #2025 Amélioration du calcul de la confidentialité lors de la présence d'un filtre appliqué à l'indicateur
- #2036 et #2044 Correction de différents bugs sur les actions de conversion d'équipements
- #2065 Correction d'un bug d'affichage sur le bandeau en pied de page

## Version 2022.11 - 2022-12-02

### Added

- #1430 Ajout d'un label au survol sur le bouton Se déconnecter
- #1932 Possibilité d'exporter les équipements visualisés
- #1934 Pouvoir modifier la formule de calcul d'un indicateur
- #1934 Ajout d'infobulles sur les champs de création d'un indicateur
- #1952 et #1979 Amélioration de l'affichage de la carte en pixels et ajout d'une représentation en pixels par catégorie
- #1974 Ajout de redirections de terristory.eu vers terristory.fr
- #1993 Ajout des données DPE en région Pays de la Loire
- #2020 Ajout de l'historique des polluants aux trajectoires
- #2019 Ajout d'une représentation cartographique d'un indicateur sous forme d'étoiles (notes)

### Changed

- #1830 Ajout de trois nouvelles données dans le fichier d'export PCAET
- #1934 MAJ de la construction de la page d'ajout ou de modification d'un indicateur
- #1975 Ajout des trajectoires de polluants dans le fichier d'export de données
- #1987 Modification de la méthode de calcul de la taille des disques lors d'affichage cartographique pour l'échelon région-région
- #1997 Amélioration des liens des sources et producteurs de données
- #2002 Construction des trajectoires de polluants à partir des données des PCAET déclarés
- #2007 Changement des noms des modules pour uniformiser les menus

### Updated

- #1678 Mise à jour des catégories pour les DPE en région Auvergne-Rhône-Alpes
- #1989 Mise à jour des données de polluants SO2 en région Auvergne-Rhône-Alpes

### Fixed

- #1876 Problème de mise à jour des PDFs sur l'interface d'administration sur l'environnement *dev*
- #1951 Nombreuses corrections sur la page de stratégie territoriale pour en améliorer la stabilité dont :
  - gestion des cas où les données manquent
  - année de référence pour les indicateurs ayant une référence
- #1955 Module de stratégie territoriale non fonctionnel en région Occitanie
- #1958 et #2006 Correction de l'interaction entre les filtres et les options pour l'indicateur *Distance domicile-travail* en région Auvergne-Rhône-Alpes
- #1998 Problème de réinitialisation de l'analyse sélectionnée lors du changement d'échelle et retrait des couches de pixels affichées précédemment
- #2010 Problème d'affichage des représentations par aplats de couleurs quand utilisation d'une URL directe

## Version 2022.10 - 2022-11-10

### Added

- #796 Ajout de la possibilité de valoriser les producteurs et sources de données associés à un indicateur
- #933 Ajout des historiques pour les trajectoires cibles (activé en région Auvergne-Rhône-Alpes)
- #1698 Accès aux indicateurs depuis les tableaux de bord
- #1868 Export des résultats d'une stratégie territoriale (activé en région Auvergne-Rhône-Alpes) - suite de #889
- #1889 Ajout de déclinaisons territoriales dans le volet déroulant de choix des mailles en région Pays de la Loire
- #1904 Envoi automatique d'un mail aux administrateurs lors d'un déploiement
- #1908 Ajout d'un indicateur à la maille EPCI pour les données relatives aux cheptels en région Pays de la Loire
- #1928 Ajout d'une couche équipement: Installations de géothermie de surface en région Pays de la Loire
- #1929 Afficher les valeurs des diagrammes circulaires dans les tableaux de bord lors de l'export sous forme de PDF
- #1959 et #1971 Ajout des diagrammes de flux d'énergie (Sankey) pour les territoires avec gestion de la confidentialité (activé en région Auvergne-Rhône-Alpes)
- Possibilité de désactiver l'affichage d'indicateurs à certains niveaux géographiques en plus des mailles
- #1953 Ajout d'un mode de représentation sous forme de pixels pour certaines couches (pour l'instant expérimental)
- #1967 Ajout des impacts économiques des actions non énergétiques de l'agriculture en région Auvergne-Rhône-Alpes
- #1976 Création d'une nouvelle représentation statique simple pour tableau de bord

### Changed

- #1830 Ajout d'autres champs au fichier ADEME de PCAET (activés en région Auvergne-Rhône-Alpes)
- #1907 Amélioration de la représentation de l'indicateur d'accessibilité à l'emploi en région Auvergne-Rhône-Alpes
- #1921 Modification de la page Support en région Occitanie
- #1949 Modification des icônes d'équipements pour les installations géothermiques en région Auvergne-Rhône-Alpes
- #1942 Correction d'une unité de graphique sur le suivi territorial
- #1985 Ajout des types flux et pixels dans l'interface d'édition d'un indicateur

### Updated

- #1939 MAJ des potentiels solaires thermiques en région Auvergne-Rhône-Alpes
- #1946 Modification de la couche d'équipements Centres de tri et ajout d'une catégorie en région Pays de la Loire
- #1948 MAJ des potentiels photovoltaiques en région Auvergne-Rhône-Alpes

### Fixed

- #1905 Erreurs dans les valeurs des exports des indicateurs calculés
- #1925 Installations sans titre en région Occitanie
- #1944 Action non fonctionnelle relative aux transports en région Occitanie
- #1957 Problème d'affichage tableau de bord Indicateurs clefs
- #1963 Corrections relatives aux données de potentiels bois
- #1965 Impossible d'activer ou de désactiver les territoires lors de l'import de données
- #1990 Correction du partage d'URL et ajout de callbacks timés pour s'en assurer

## Version 2022.09 - 2022-10-05

### Added

- Possibilités accrues de modifications des équipements depuis l'interface d'administrateur (#1900)
- Informations quant aux données disponibles actuellement en base
- Ajout d'un historique des modifications d'indicateurs (#1902)
- Note de contribution pour les futurs contributeurs (#1689)
- Vérification de la cohérence des résultats de l'API avant et après la MAJ (#1903)
- Possibilité d'envoi d'un mail automatique aux administrateurs régionaux lors de la mise en ligne d'une nouvelle version (#1904)
- Ajout progressif d'outils dans le module de stratégie territoriale en région Auvergne-Rhône-Alpes :
  - export des résultats d'une stratégie territoriale en cours de test (#889)
  - trajectoires des polluants atmosphériques (#1060)
  - impacts économiques pour la réduction des émissions non-énergétiques dans l'industrie (#1896)
  - objectifs supra-territoriaux des polluants atmosphériques (#1941)

### Changed

- #769 Vérification de la possibilité de modifier une couche d'équipements dans l'API
- #1812 Sécurisation et nettoyage de l'application
- #1858 Améliorations autour de la version Sankey en cours de test en région Auvergne-Rhône-Alpes
- #1895 Changement de comportement lorsque les valeurs d'un indicateur sont toutes nulles dans le territoire considéré
- #1920 Amélioration du calcul d'URL pour les documents méthodologiques (notamment en cas de lien externe)
- #1938 Réautoriser le partage d'un tableau de bord par URL avec tout visiteur

### Updated

- #1578, #1822 Mise à jour du logo de la région des Pays de la Loire sur l'ensemble des supports
- #1686 Enrichissement de la documentation du code open-source
- #1913 Mise à jour des Installations H2 et IRVE en Occitanie
- #1924 Mise à jour des Installations méthanisation en Pays de la Loire

### Fixed

- #726 Correction de l'historique des équipements pour l'ensemble des régions
- #1763 Impact de la consommation énergétique de l'industrie
- #1863 Correction des couleurs sur la popup de page d'accueil
- #1890 Retour sur écran blanc après zoom sur un territoire
- #1901 Indicateur de part-modale des déplacements domicile-travail erroné en région Auvergne-Rhône-Alpes
- #1910 Affichage des tableaux de bord lorsqu'un maillage est sélectionné par défaut

## Version 2022.08 - 2022-08-31

### Added

- Suivi des statistiques de consultation sur tableaux de bord et pages autres (#1571, #1193)
- Intégration de la construction de la documentation dans la chaîne d'intégration continue (#1686)
- Activation du module de stratégie territoriale sur les Pays de la Loire (#1854)
- Documents méthodologiques (#1864)
- Documentation de l'interface admin directement sur le site (#1882)
- Déploiement de la Corse sur le serveur de test et prochainement sur le serveur officiel (#1888)

### Changed

- Important travail de consolidation des tableaux de bord :
  - sécurité (#1885), 
  - création (bugs #1886, #1891), 
  - ajout de la possibilité d'ordonner les thématiques (#1891)
  - classement par ordre alphabétique des indicateurs #1875
- Retrait de l'étape des PCAET de la MAJ des territoires
- #1763, #1776 Paramétrage des actions Industrie et Agriculture sur le module Stratégie territoriale
- #1795 Afficher *confidentielle* pour une donnée confidentielle sur la carte plutôt que 0
- #1851 Utilisation d'un histogramme pour l'indicateur d'Accessibilité à l'emploi
- #1871 Ouverture de pdfs dans un nouvel onglet
- #1872 Ajout des formes géométriques hors région
- #1877 Amélioration de l'affichage des cercles de valeurs négatives
- #1887 Nouvelle maquette du bandeau titre

### Updated

- #1873 Valeurs aberrantes dans les puits de carbone (Pays de la Loire)
- #1883 Mentions légales
- #1897 MAJ des informations démographiques utilisées pour les indicateurs déchets (Auvergne-Rhône-Alpes)
- #1912 Amélioration du référencement et en particulier de l'URL et du titre

### Fixed

- #524 Création d'un scénario de plan d'action dans une seule transaction
- #1826 Bug d'impression des couches d'équipements
- #1860 Doublons autour de la ville de Lyon et de ses arrondissements
- #1861, #1878 Indicateurs impossibles à charger
- #1862 Valeurs non correctes au sein de certains territoires et différences entre la valeur affichée dans le total et celle de la popup
- #1872 Amélioration de l'affichage des flux de migrations pendulaires
- Correction du responsable RGPD affiché sur la page d'inscription

## Version 2022.07 - 2022-07-27

### Added

- Ajout d'un prototype de diagramme de Sankey pour les EPCI n'ayant pas de confidentialité en région Auvergne-Rhône-Alpes (#943)
- Possibilité de partager une URL avec les installations affichées (#1445)
- Ajout du PDF des équipements non ponctuels (#1840)
- Prise en compte de l'équipement modifié dans le formulaire de l'interface admin
- Meilleur environnement de développement :
  - Possibilité d'avoir un utilisateur administrateur interrégion en local (#1632)
  - Paramètres configurables pour accélérer le développement

### Changed

- #1632 Nom d'un onglet dans l'interface administrateur
- #1765 / #1583 Déplacement des fichiers PDF et SVG hors de l'application
- #1807 Amélioration des requêtes territoriales dans les cas de mailles communales
- #1822 MAJ du logo des Pays de la Loire
- #1840 Ajout du PDF des tracés des réseaux de chaleur en région Auvergne-Rhône-Alpes
- #1847 Amélioration stats de consultation des différents TerriSTORY
- Données manquantes sur les emplois du tertiaire dans l'Ardèche

### Updated

- #1339 MAJ des prix de la facture énergétique et des PDFs associés
- #1825 Indicateurs socio-économiques sur données INSEE 2019 de la région Auvergne-Rhône-Alpes
- #1841 PDFs méthodologiques de la région Auvergne-Rhône-Alpes
- #1849 Ajout d'un PDF supplémentaire sur la page A propos de la région Auvergne-Rhône-Alpes

### Fixed

- #1569 Infobulles qui s'effacent bien sur les diagrammes circulaires
- #1736 Réorganisation des indicateurs et thématiques dans l'interface admin
- #1821 Indicateur Accessibilité emploi (+légère modification dans le fonctionnement des filtres)
- #1827 Correction d'un bug d'affichage sur la page de création d'un utilisateur
- #1837 Infobulles des histogrammes à nouveau fonctionnelles
- #1838 MAJ des indicateurs à nouveau possible
- #1846 Problème d'affichage d'indicateurs en région Occitanie
- #1848 Retrait d'un bouton en trop sur la page des stratégies territoriales
- #1855 Informations sur les échelles de disponibilités des indicateurs non visibles dans l'interface admin lors de l'édition

## Version 2022.06 - 2022-07-08

### Added

- Meilleure gestion de la confidentialité des données (#1775, #1823, #1824)
- Amélioration des tracés des réseaux de chaleurs (#1759)
- Ajout action "Réduction des émissions non-énergétiques dans l'industrie" (#1482)
- Documentation de l'application (#1690, #1691)
- Accéder à des indicateurs désactivés via un compte utilisateur autorisé (#1801)

### Changed

- #1678 Modication des indicateurs DPE
- #1813 Meilleur référencement
- #1811 harmonisation des identifiants de type énergie (entre AURA et Occitanie)

### Updated

- #1057 Ajouter une échelle territoriale : Région maille PPA (plan de protection de l'atmosphère) et PPA maille communes
- #1721 Mise à jour des bornes GnV
- #1722 Mise à jour des bornes IRVE
- #1723 Mise à jour des indicateurs déchets
- #1724 Mise à jour des installations déchets
- PDFs méthodologiques (#1796, #1803 et #1805)

### Fixed

- #1115 IHM - Disparition des installations
- Erreurs tableaux de bords (#1493, #1682, #1707, #1783, #1808)
- #1511 Bug - fonctionnalité imprimer - affichage légende
- #1674 Zoomer sur le territoire indiqué dans l'URL 
- #1774 Problème de valeurs dans l'indicateur sur les infrastructures cyclables
- #1780 Couleur de fond du pop-up dépendante au thème windows
- #1799 Les indicateurs sont systématiquement exécutés à deux reprises 
- #1816 Données polluants - écarts entre TerriSTORY et ORCAE
- #1821 Réparer indicateur Accessibilité emploi et légère modification dans le fonctionnement des filtres
