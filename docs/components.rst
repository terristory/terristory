Fonctionnement du front-end
==========================================

Composants du front
-------------------

.. toctree::
   :maxdepth: 2

   front/components

Contrôleurs du front
--------------------

.. toctree::
   :maxdepth: 2

   front/controllers

Routeurs
--------------------

.. toctree::
   :maxdepth: 2

   front/routes

Wrappers
--------------------

.. toctree::
   :maxdepth: 2

   front/wrappers
