Front-end
==========================================

Explications générales sur le fonctionnement du front.


Main
------------------------

.. js:autoclass:: Main
    :members:
    :private-members:

settings_data.js
------------------------

.. js:autoattribute:: configData


Fonctions utiles
------------------------

.. js:autofunction:: trim

.. js:autofunction:: buildRegionUrl

.. js:autofunction:: convertRegionToUrl

.. js:autofunction:: removeRegionFromLayer

.. js:autofunction:: normalize

.. js:autofunction:: saveAsPng
