.. TerriSTORY documentation master file, created by
   sphinx-quickstart on Fri Jun 10 11:31:14 2022.

Bienvenue sur la documentation de TerriSTORY
============================================

Cette documentation facilite la prise en main et le développement de l'application 
TerriSTORY®.

Table des matières :

.. toctree::
   :maxdepth: 2

   introduction
   installation
   notice_contribution
   architecture_logicielle
   modele_donnees
   structure_code
   terriapi
   strategy_module
   front
   components
   testing
   tools
   cas_pratiques_d_usages
   licence

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
