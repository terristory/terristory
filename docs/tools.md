# Outils pratiques

<!-- 28/09/2022

Révisions du document 

  Contributeur           Date         Version
  ---------------------- ------------ ---------
  Matthieu DENOUX   28/09/2022   V1 -->

## Visualisation de l'historique des révisions alembic

On va pouvoir s'appuyer pour visualiser l'historique des révisions de la structure de la
base de données sur un petit utilitaire.

Pour cela, on procède comme suit, en vous plaçant dans un dossier qui vous convient :

```bash
# requirements
sudo apt-get install graphviz
# clone git repo
git clone https://github.com/giliam/alembic-viz/
# use my branch
git checkout feature/use_docstring_instead_of_id
# prepare env
python3 -m venv env
source env/bin/activate
cd alembic-viz
python setup.py install
# go to terristory
cd ~/Bureau/TerriStory/terriapi/
# run
alembic-viz --enable-desc yes
```

Si tout s'est bien passé, vous devriez avoir deux fichiers créés dans votre dossier
(ici `~/Bureau/TerriStory/terriapi/`) dont une image similaire à l'image suivante :

```{figure} media/tools/migrations.png

Exemple de l'arbre des migrations
```