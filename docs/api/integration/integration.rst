Éléments du module d'intégration
=================================


Fonctions du module intégration
-------------------------------

.. automodule:: terriapi.integration
    :members:
    :undoc-members:
    :show-inheritance:

Submodules du module intégration
----------------------------------

categorie module
-------------------------------------

.. automodule:: terriapi.integration.categorie
    :members:
    :undoc-members:
    :show-inheritance:

indicateur module
--------------------------------------

.. automodule:: terriapi.integration.indicateur
    :members:
    :undoc-members:
    :show-inheritance:

loader module
----------------------------------

.. automodule:: terriapi.integration.loader
    :members:
    :undoc-members:
    :show-inheritance:

validation module
--------------------------------------

.. automodule:: terriapi.integration.validation
    :members:
    :undoc-members:
    :show-inheritance:
