Éléments des contrôleurs de l'API
==================================


Fonctions du module contrôleurs
--------------------------------

.. automodule:: terriapi.controller
    :members:
    :undoc-members:
    :show-inheritance:


Submodules du module contrôleurs
---------------------------------

actions module (controller)
----------------------------------

.. automodule:: terriapi.controller.actions
    :members:
    :undoc-members:
    :show-inheritance:

analyse module
----------------------------------

.. automodule:: terriapi.controller.analyse
    :members:
    :undoc-members:
    :show-inheritance:

territorial synthesis module (controller)
-------------------------------------------

.. automodule:: terriapi.controller.territorialsynthesis
    :members:
    :undoc-members:
    :show-inheritance:

donnees\_pour\_representation module
--------------------------------------------------------

.. automodule:: terriapi.controller.donnees_pour_representation
    :members:
    :undoc-members:
    :show-inheritance:

gestion\_images module
------------------------------------------

.. automodule:: terriapi.controller.gestion_images
    :members:
    :undoc-members:
    :show-inheritance:

indicateur\_tableau\_bord module
----------------------------------------------------

.. automodule:: terriapi.controller.indicateur_tableau_bord
    :members:
    :undoc-members:
    :show-inheritance:

mesure\_audience module
-------------------------------------------

.. automodule:: terriapi.controller.mesure_audience
    :members:
    :undoc-members:
    :show-inheritance:

pcaet\_ademe module
---------------------------------------

.. automodule:: terriapi.controller.pcaet_ademe
    :members:
    :undoc-members:
    :show-inheritance:

perimeters module
-------------------------------------

.. automodule:: terriapi.controller.perimeters
    :members:
    :undoc-members:
    :show-inheritance:

poi module
------------------------------

.. automodule:: terriapi.controller.poi
    :members:
    :undoc-members:
    :show-inheritance:

pop\_up\_accueil module
-------------------------------------------

.. automodule:: terriapi.controller.pop_up_accueil
    :members:
    :undoc-members:
    :show-inheritance:

regions\_configuration module
-------------------------------------------------

.. automodule:: terriapi.controller.regions_configuration
    :members:
    :undoc-members:
    :show-inheritance:

saisie\_objectifs module
--------------------------------------------

.. automodule:: terriapi.controller.saisie_objectifs
    :members:
    :undoc-members:
    :show-inheritance:

scenario module
-----------------------------------

.. automodule:: terriapi.controller.scenario
    :members:
    :undoc-members:
    :show-inheritance:

stations\_mesures module
--------------------------------------------

.. automodule:: terriapi.controller.stations_mesures
    :members:
    :undoc-members:
    :show-inheritance:

strategy\_export module
--------------------------------------------

.. automodule:: terriapi.controller.strategy_export
    :members:
    :undoc-members:
    :show-inheritance:

suivi\_trajectoire module
---------------------------------------------

.. automodule:: terriapi.controller.suivi_trajectoire
    :members:
    :undoc-members:
    :show-inheritance:

tableau\_bord module
----------------------------------------

.. automodule:: terriapi.controller.tableau_bord
    :members:
    :undoc-members:
    :show-inheritance:

user module (controller)
-------------------------------

.. automodule:: terriapi.controller.user
    :members:
    :undoc-members:
    :show-inheritance:

zone module
-------------------------------

.. automodule:: terriapi.controller.zone
    :members:
    :undoc-members:
    :show-inheritance:
