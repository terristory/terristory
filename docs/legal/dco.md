
### Certificat d'origine à l'usage des développeurs (Developer's Certificate of Origin)

*Variante 1.1*

*Copyright (C) 2004, 2006 The Linux Foundation et ses contributeurs.*

*Tout le monde est autorisé à copier et distribuer des copies textuelles de ce document de licence, mais il est interdit de le modifier.*

**Certificat d'origine à l'usage des développeurs 1.1**

En contribuant à ce projet, je certifie que :

**(a)** La contribution a été créée en tout ou en partie par moi et j'ai le droit de la soumettre sous la licence open source indiquée dans le fichier ; ou

**(b)** La contribution est basée sur un travail antérieur qui, à ma connaissance, est couvert par une licence open source appropriée et j'ai le droit, en vertu de cette licence, de soumettre ce travail avec des modifications, qu'elles aient été créées en tout ou en partie par moi, sous la même licence open source (sauf si je suis autorisé à soumettre sous une licence différente), comme indiqué dans le fichier ; ou

**(c)** La contribution m'a été fournie directement par une autre personne qui a certifié (a), (b) ou (c) et je ne l'ai pas modifiée.

**(d)** Je comprends et accepte que ce projet et la contribution sont publics et qu'un enregistrement de la contribution (y compris toutes les informations à caractère personnel que je soumets avec, y compris ma signature) est conservé indéfiniment avec ce projet et peut être redistribué conformément à ce projet ou la ou les licences open source concernées.

Certificat d'origine à l'usage des développeurs est une
traduction et a été conçu pour être compatible avec sa version
anglaise (même numéro de version) qui peut être utilisée
indifféremment.
