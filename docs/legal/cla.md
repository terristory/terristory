### Accord de licence de contributeur

#### Préambule

Dans le cadre du passage en open-source du logiciel TerriSTORY®
(ci-après désigné « le Logiciel »), l'Agence Auvergne Rhône Alpes
Energie Environnement (ci-après désignée « AURA-EE ») en tant que
propriétaire du Logiciel, souhaite s'assurer d'obtenir la propriété de
toutes les contributions effectuées par les Contributeurs externes sur
le Logiciel. Cet accord (ci après désigné « l'Accord ») devra être signé
par chaque Contributeur avant d'apporter sa Contribution au Logiciel.

#### Définitions

**Apport**: désigne l'acte du Contributeur qui apporte sa
Contribution au Logiciel, par tous moyens (électronique, orale, écrite)
de communication adressé à AURA-EE ;

**Contributeur(s)** : désigne la ou les personnes
physiques et / ou morales qui apporte(nt) des Contributions au
Logiciel ;

**Contribution** : désigne les développements faits par
des Contributeurs externes et internes à AURA-EE, sur le Logiciel ;

**Logiciel** : désigne le logiciel TerriSTORY®.

#### Cession

Par la signature de cet Accord, les Contributeurs affirment qu'ils
cèdent à titre gratuit, leurs droits patrimoniaux sur leurs
Contributions, et notamment leur droits d'auteur ou droit de brevets, à
AURA-EE, à la date de l'Apport. Cette cession est valable pour toute la
durée des droits d'auteur et dans le monde entier.

AURA-EE ou un tiers qu'elle désignera pourra exploiter tout ou partie
desdites Contributions sous réserve du respect du droit moral du
Contributeur personne physique.

La cession par le Contributeur, à AURA-EE, des droits sur la
Contribution englobe le droit de :

- Reproduire tout ou partie de la Contribution sur tout support,
    physique ou dématérialisé, connu ou inconnu ; à titre temporaire ou
    définitif, associés ou non à d\'autres œuvres de quelque nature
    qu\'elles soient à titre gratuit ou onéreux ;

- Traduire la Contribution en toutes langues connues, et en tout
    langage de programmation connu ou inconnu, pour tout matériel et
    système d'exploitation ;

- Représenter la Contribution, sur tout support, physique ou
    dématérialisé ;

- Adapter, corriger, transformer, retirer, arranger, modifier tout ou
    partie de la Contribution, notamment à des fins de mise à jour ou
    toute autre fin qui paraîtra nécessaire à AURA-EE pour
    l'exploitation de tout ou partie de la Contribution ;

#### Accord

Le Contributeur confirme que :

- il dispose de la capacité légale suffisante pour conclure l'Accord ;

- il est titulaire des droits d\'auteur et / ou brevet couvrant la
    Contribution avant leur transfert à AURA-EE ;

- la cession des droits du Contributeur prévue à l\'article 2 ne
    contrevient à aucune autre cession que le Contributeur a consentie
    au profit de tiers, y compris son employeur. Si le Contributeur est
    employé, il doit obtenir de son employeur l\'acceptation de conclure
    cet Accord. Si le Contributeur est mineur, ses parents ou tuteur
    devront signer l\'Accord.
