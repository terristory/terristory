# Le module de stratégie territoriale de TerriSTORY®

Un des modules principaux de TerriSTORY® est le module de stratégie territoriale.

## Prérequis

Pour fonctionner, le module nécessite un certain nombre de données :

* des données statiques dont par exemple des facteurs d'émission pour les différents vecteurs énergétiques;
* les données de consommation et de production d'énergie, ainsi que d'émissions de gaz à effet de serre;
* (*facultatif*) les caractéristiques du parc de véhicules
* (*facultatif*) les données d'émissions de polluants

La liste des dépendances par action est récapitulée de façon sommaire mais exhaustive [dans le fichier suivant](media/strategy/strategy_actions.pdf). Pour chaque action, le document précise :

1. les variables utilisées en entrée de l'action (les variables de la forme `DataSet.YYYY` correspondant aux tables de données) ;
2. les sorties produites ;
3. les dépendances en terme de données :
   1. configurations générales du module ;
   2. paramètres statiques ;
   3. paramètres entrés par l’utilisateur (soit dans le contenu de l'action - par exemple en MW installés - soit des paramètres avancés) ;
   4. données.

## Table de passage

Pour permettre de faire le lien entre les données disponibles dans un schéma régional et les données telles que manipulées par le module de stratégie territoriale, une table de passage a été mise en place. Celle-ci associe à une valeur régionale (par exemple, un nom de table, un ID de commodité ou de secteur...) une valeur telle qu'utilisée dans le code source.

Les variables présentes dans le [document mentionné ci-dessus](media/strategy/strategy_actions.pdf) sont celles utilisées dans le code.

Pour modifier cette table de passage, une page a été ajoutée dans le panel d'administration. Un exemple de table de passage est [disponible à cette adresse](media/strategy/table_passage.csv). Elle est structurée comme suit :

1. `key` : la valeur telle qu'utilisée dans le code source de TerriSTORY® (**à ne pas modifier**)
2. `match` : la valeur régionale spécifique
3. `association_type` : le type d'association (**à ne pas modifier**)
4. `aggregation` : l'attitude à appliquer en cas d'agrégation. Si vous séparez une commodité (l'éolien entre onshore et offshore par exemple) dans votre région entre plusieurs valeurs alors qu'il n'y en a qu'une seule dans la table de passage (*ie.*, TerriSTORY® ne fait pas cette distinction), il faudra insérer plusieurs lignes dans le fichier pour chaque association (une ligne pour la valeur de l'onshore et une pour l'offshore). Cependant, il faut également préciser quelle valeur doit être utilisée pour contenir les résultats de l'action (par exemple, l'action relative à l'éolien doit ajouter de l'éolien offshore puisque c'est a priori la façon la plus commune de déployer de l'éolien). Pour cela, la valeur doit être mise à *priority* au lieu de *keep*.
5. `is_confidential` : si la donnée est concernée par la confidentialité.

## Méthodologies des actions

Plus de détails sont disponibles dans le [document pdf situé ici](https://auvergnerhonealpes.terristory.fr/pdf/auvergne-rhone-alpes/guide_plan_actions_transition_%C3%A9nerg%C3%A9tique.pdf).