# Introduction

**TerriSTORY®** est une plateforme de visualisation et de mise à disposition de données et d'analyse réalisée par l'association AURA-EE (voir [ici](https://gitlab.com/terristory/terristory/)). Plus précisément, il s'agit d'une *application web* et d'*un outil d'aide au pilotage*
à destination des *territoires en transition énergétique*, *écologique*,
pédagogique et multithématique (énergie, air, émissions de gaz à effet
de serre, stockage carbone, gestion des déchets, mobilité,
environnement, climat, etc.). Il met à disposition **des données de référence** 
afin d'aider les acteurs territoriaux à identifier les
ressources et les leviers d'actions prioritaires du territoire pour
construire, suivre et évaluer leur trajectoire de transition
énergétique.

Outil "*made in Auvergne-Rhône-Alpes*", TerriSTORY® est présent dans **6 régions** et devient un outil de
référence au **niveau national**.

Le projet rassemble une vingtaine d’acteurs nationaux et régionaux ayant une mission de service public ou
d’intérêt général.

## Fonctionnement

Le principe général de fonctionnement de TerriSTORY® est une application web composée d'une API et d'un front-end. Le site est en lui-même composé de plusieurs modules reposant sur une seule base de données. Ces modules fournissent différents services autour de données quantitatives sur les enjeux cités en début de cette page, comme l'analyse historique des consommations, l'affichage cartographique d'indicateurs, la création de tableaux de bord personnalisés ou encore la simulation de scénarios stratégiques.

La plateforme est accessible à l'adresse [https://terristory.fr](https://terristory.fr). Plus de détails sur le fonctionnement technique et l'architecture logicielle de l'outil sont fournis dans la [page dédiée](architecture_logicielle). Pour consulter un guide d'utilisation de l'application, vous pouvez également consulter le [bref document suivant](https://auvergnerhonealpes.terristory.fr/pdf/auvergne-rhone-alpes/TerriSTORY_Presentation.pdf) ou la page support dédiée sur la [déclinaison régionale pour Auvergne-Rhône-Alpes](https://auvergnerhonealpes.terristory.fr/support).

## Participation au développement

Plus d'informations sont disponibles dans la [notice de contribution](notice_contribution) du présent document.
