Fonctionnement de l'API
=======================


Module contents
---------------

.. automodule:: terriapi
    :members:
    :undoc-members:
    :show-inheritance:

Modules principaux
-------------------

actions module
-----------------------

.. automodule:: terriapi.actions
    :members:
    :undoc-members:
    :show-inheritance:

api module
-------------------

.. automodule:: terriapi.api
    :members:
    :undoc-members:
    :show-inheritance:

auth module
--------------------

.. automodule:: terriapi.auth
    :members:
    :undoc-members:
    :show-inheritance:

territorialsynthesis module
-----------------------------

.. automodule:: terriapi.integration.territorialsynthesis
    :members:
    :undoc-members:
    :show-inheritance:

create\_user module
----------------------------

.. automodule:: terriapi.create_user
    :members:
    :undoc-members:
    :show-inheritance:

user module
--------------------

.. automodule:: terriapi.user
    :members:
    :undoc-members:
    :show-inheritance:


Contrôleurs de l'API
--------------------

Quelques informations sur les contrôleurs de l'API...

:ref:`Éléments des contrôleurs de l'API`
    

.. toctree::
    :maxdepth: 2
    :hidden:

    api/controllers/controller

Intégration
-----------

Idem sur le module `integration`.

:ref:`Éléments du module d'intégration`


.. toctree::
    :maxdepth: 2
    :hidden:

    api/integration/integration
