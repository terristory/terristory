# Structure de code

<!-- 
08/07/2022

Révisions du document 

  Contributeur           Date         Version
  ---------------------- ------------ ---------
  Fatima-Zahra FATTOUH   01/07/2022   V1
                                      
                                       -->

## La structure de l'application TerriSTORY®

L'application TerriSTORY® est organisée suivant l'architecture
**Modèle-Vue-Contrôleur** qui sépare le modèle de données, l\'interface
utilisateur et la logique de contrôle.

### Qu'est-ce qu'une architecture Modèle-Vue-Contrôleur ?

Modèle-vue-contrôleur (MVC) est un modèle de conception permettant de
relier l\'interface utilisateur aux modèles de données. Il consiste à
distinguer trois entités : le modèle, la vue et le contrôleur.

### Rôle de modèle 

Le modèle assure la gestion des données et garantit leur intégrité. Il
offre des méthodes pour récupérer les données et les mettre à jour
(insertion, suppression, modification, etc.).

### Rôle de vue

La vue fait l\'interface avec l\'utilisateur, elle assure l'affichage
des données qu\'elle récupère auprès du contrôleur et reçoit toutes les
actions de l'utilisateur (clic de souris, sélection d'une entrée, ...).
Ses différents événements sont envoyés au contrôleur.

### Rôle de contrôleur 

Le contrôleur est chargé de la synchronisation du modèle et de la vue.
Il reçoit tous les événements de l\'utilisateur et enclenche les actions
à effectuer. Si une action nécessite un changement des données, le
contrôleur demande la modification des données au modèle et ensuite
avertit la vue que les données ont changé pour que celle-ci se mette à
jour. Si certains événements de l\'utilisateur ne concernent pas les
données, le contrôleur demande à la vue de se modifier directement.

### Interactions entre le modèle, la vue et le contrôleur 

Le schéma suivant modélise les différentes interactions entre le modèle,
la vue et le contrôleur.

```{figure} media/structure/image32.png
:width: 5.06875in
:height: 3.80792104111986in

Schéma MVC
```

### L'architecture MVC et l'application TerriSTORY®

Le schéma suivant modélise l'architecture MVC dans l'application
TerriSTORY®

```{figure} media/structure/image33.png
:width: 5.122222222222222in
:height: 3.0625in

Modèle MVC TerriSTORY
```

## Structure du code source de l'application TerriSTORY®

### L'architecture de code source

L'application se compose de deux répertoires principaux :

**front** : représente la vue, il contient tous les fichiers Javascript
qui permettent de faire l'interface utilisateur.

**terriapi** : représente le contrôleur et le lien avec la base de
données de l'application, il contient tous les fichiers Python qui
permet de manipuler les données.

Le Javascript, côté front réalise des requêtes http. Il appelle
l\'**API** en utilisant **AJAX** en **POST** ou **GET** avec des
**paramètres** que l\'on retrouver dans la variable **Request**.

Les fonctions python sont accessible via des routes (URLs) définis dans
le fichier **front/src/settings.js** si elles contiennent la ligne
**\@bp.route** ou **\@pp.route** au-dessus de sa définition.

Exemple des fichiers qui contient des fonctions accessibles via des
routes :

- **terriapi/terriapi/api.py**

- **terriapi/terriapi/user.py**

Le schéma suivant illustre les échanges HTTP entre le client et le
serveur.

```{figure} media/structure/image34.png
:width: 3.8782392825896763in
:height: 4.603385826771653in

Les échanges HTTP entre le client et le serveur
```

Le schéma suivant illustre les répertoires principaux de l'application
TerriSTORY®

```{figure} media/structure/image35.png
:width: 3.6458333333333335in
:height: 6.722005686789151in

Arborescence de l'application
```

## Les composantes de l'application

### Le module de visualisation des indicateurs

Ce module permet de visualiser des indicateurs représentés de manière
cartographiques par des aplats de couleurs ou cercles proportionnels
selon leur définition, et de les accompagner de diagrammes circulaires
ou radar.

```{figure} media/structure/image36.png
:width: 6.138830927384077in
:height: 1.769905949256343in

Module d'Indicateurs
```

Cette fonctionnalité repose sur une fonction principale dont le rôle est
d'associer chacune des mailles territoriales à une valeur et de calculer
le total, la moyenne ou la moyenne pondérée selon le type de
l'indicateur.

Celle-ci fait appel à une architecture de classe dont la responsabilité
est de générer les requêtes SQL nécessaires à l'obtention et la
structuration des données par territoires et catégories, parfois
filtrées selon des modalités ou des valeurs.

Les fichiers liés à ce composant sont :

- Pour la récupération de données coté serveur (back) :
    **terriapi/terriapi/api.py**

    **terriapi/terriapi/controller/** **analyse.py**

    **terriapi/terriapi/controller/** **donnees\_pour\_representations.py**

- Pour la restitution de données coté client (front) :
    **front/src/components/OlMap.js**

    **front/src/components/Filters.js**

Le schéma suivant modélise l'architecture classe des différents types
d'indicateurs.

```{figure} media/structure/image37.png
:width: 6.298611111111111in
:height: 3.25in

Diagramme de classe des indicateurs
```

### Le module de visualisation des équipements

Ce module permet de visualiser des équipements représentés de manière
cartographiques par des lignes ou des points.

```{figure} media/structure/image38.png
:width: 6.298611111111111in
:height: 1.898611111111111in

Module d'Equipements
```

Ce composant repose sur une fonction simple qui permet de récupérer un
jeu de données qui associe pour chaque équipement, des points avec des
coordonnées (X, Y) à une liste de données qu'on souhaite afficher sur
l'interface cartographique.

Les fichiers liés à ce composant sont :

- Pour la récupération de données coté serveur (back) :
    **terriapi/terriapi/api.py**

    **terriapi/terriapi/controller/poi.py**

- Pour la restitution de données coté client (front) :
    **front/src/components/OlMap.js**

>   **front/src/components/Filters.js**

### La pop-up d'accueil

Le pop-up s'affiche au lancement de site régional de **terrstory.fr**,
il permet d'afficher un message d'accueil personnalisable depuis les
interfaces d'administrations.

```{figure} media/structure/image39.png
:width: 3.6399048556430444in
:height: 3.751469816272966in

Pop-up d'accueil
```

Les fichiers liés à ce composant sont :

- Fichier back : **terriapi/terriapi/controller/pop\_up\_accueil.py**

    **terriapi/terriapi/api.py**

- Fichier front principal :
    **front/src/components**/**SplashScreen.js**

- Fichiers front liée au carrousel :
    **front/src/components/Caroussel.js**

    **front/src/components/PopUpEditor.js**

    **front/src/components/** **EditeurMarkdown.js**

    **front/src/components/GestionnaireImagesCarousel.js**

### La légende

Situé dans l'interface cartographique, ce composant permet d'afficher un
certain nombre d'informations parmi lesquelles :

- Une légende accompagnée d'un formulaire de sélection de méthode de
    classification des données dans le cas où les données
    cartographiques sont représentées sous forme d'aplats de couleurs ;

- D'une entrée spécifique pour les données confidentielles ;

- D'une jauge qui permet de définir un seuil en dessous duquel on
    filtre les flux migratoires ;

```{figure} media/structure/image40.png
:width: 2.7260378390201225in
:height: 1.9166666666666667in

Légende
```

Les fichiers liés à ce composant sont :

Fichier côté client (Front) : **front/src/components/Legend.js**

   **front/src/components/Style.js**

### La légende d'information

Situé dans l'interface cartographique, ce composant permet d'afficher un
certain nombre d'informations parmi lesquelles :

- Afficher le nom de territoire sélectionné
- Afficher le nom et la thématique de l'indicateur ou l'équipement sélectionné
- Afficher la dernière année de disponibilité des données.
- Le total territoire
- Consulter la fiche méthodologique
- Exporter les données

```{figure} media/structure/image41.png
:width: 2.8833333333333333in
:height: 1.424504593175853in

Légende Informations
```

Le fichier lié à ce composant est **: front/src/components/Infos.js**

### Le module d'analyses territoriales

#### Synthèse des indicateurs territoriaux

Ce module a été développé dans le cadre du projet européen
**CESBA\_Alps**, il permet d'évaluer la performance du territoire sur de
nombreux sujets par rapport à d'autres territoires du même type
(consommation et production d'énergie, émissions de gaz à effets de
serre etc.).

Des indicateurs de performance sont calculés au moment de la procédure
d'intégration des données de l'application et leur valeur figure donc en
base de données, puis sont obtenues au moyen d'une requête simple écrite
dans un module (un fichier Python) de la partie « controller ».

Cette partie de l'application est un peu à part, car elle est la seule
qui nécessite d'intégrer des données au préalable en base de données
pour la faire fonctionner.

Les fichiers liés à ce composant sont :

- Fichier côté serveur (Back) : **terriapi/terriapi/controller/territorialsynthesis.py**
- Fichier côté client (Front) : **front/src/components/TerritorialSynthesis.js**

#### Le module de suivi de la trajectoire énergétique

Ce module permet de suivre :

- L'évolution de la consommation énergétique par secteur, par énergie, et par usage.
- L'évolution de la production énergétique

Les fichiers liés à ce composant sont :

- Fichier côté client (Front) : **front/src/components/SuiviEnergetique.js**
- Fichier côté serveur (Back) : **terriapi/terriapi/controller/suivi\_trajectoire.py** et **terriapi/terriapi/api.py**

### Le module de stratégie territoriale

#### Les trajectoires cibles

Ce composant permet de visualiser les trajectoires cibles énergie et GES
jusqu'à 2050.

```{figure} media/structure/image42.png
:width: 5.277723097112861in
:height: 3.5576640419947507in

Module stratégie territoriale - Trajectoires cibles
```

Les fichiers liés à ce composant sont :

- Pour la récupération des données énergétiques coté serveur (back) :

>    **terriapi/terriapi/user.py**
>
>    **terriapi/terriapi/controller/scenario.py**

- Pour la restitution de données coté client (front) :

>    **front/src/components/PlanActionsTrajectoiresCibles.js**
>
>    **front/src/components/PlanActionsScenarii.js**

### Les plans d'actions

Ce composant permet de simuler les résultats d'impacts d'actions
modélisées par des calculs, paramètres et coefficients dont certains
sont paramétrables et / ou modifiables depuis l'interface.

```{figure} media/structure/image43.png
:width: 6.208333333333333in
:height: 2.9618055555555554in

Module stratégie territoriale -- Plan d'actions
```

Cette partie du code peut être divisée en deux scripts de calculs
distincts.

- L'un concerne principalement les impacts énergétiques (réduction de
    consommation et de la facture énergétique, augmentation de la
    production) et les retombées fiscales. Il repose sur un calcul
    d'impacts par action d'une part, puis d'une fusion entre
    l'historique des données et les prévisions jusqu'en 2050 d'autre
    part. Les calculs d'impacts mobilisent les données des utilisateurs,
    certaines situées en base de données (nécessaire pour obtenir
    l'historique sur plusieurs années), et des paramètres indiqués dans
    des fichiers csv qui font partie intégrante de l'application.

- L'autre concerne les impacts économiques et sociaux (investissement,
    création de valeur ajoutée et emplois créés). Un seul fichier Python
    est chargé de construire la requête SQL qui mobilise des paramètres
    spécifiques construits de toute pièce, la matrice de Leontief
    d'entrées / sorties, ainsi que les données INSEE sur la nomenclature
    de l'activité française en 88 branches et la connaissance local de
    l'appareil productif. Ce dernier jeu de données est utile pour
    calculer les parts économiques captées par les territoires et la
    région des différentes phases des projets. Ce pourcentage de part
    captée est modifiable par les utilisateurs directement depuis
    l'interface.

Les fichiers liés à ce composant sont :

- Pour la mise en forme de données des impacts énergétiques coté
    serveur (back) :

    **terriapi/terriapi/controller/actions.py**

- Pour la mise en forme de données des impacts emplois coté serveur
    (back) :

    **terriapi/terriapi/actions.py**

    **terriapi/terriapi/controller/suivi\_trajectoire.py**

    **terriapi/terriapi/impact\_emploi.py**

- Pour la restitution de données coté client (front) :

>   **front/src/components/PlanActions.js**

Le schéma suivant modélise l'architecture de classe de la partie
économique du module stratégie territoriale :

```{figure} media/structure/image44.png
:width: 6.298611111111111in
:height: 2.6145833333333335in

Diagramme de classe du module stratégie territoriale
```

### Le module de tableau de bord

Il s'agit ici de la fonctionnalité de tableau de bord principalement
développée en recourant à la conception orientée objet.

La classe **DonneesPourRepresentation** du fichier Python permet de
mettre en forme les données pour chacune des représentations. Cette
classe transmet ses attributs et méthodes au moyen d'un héritage, à
plusieurs classes filles. On compte une classe fille qui hérite de cette
classe mère pour chaque représentation.

On peut résumer l'architecture de ces classes au moyen d'un diagramme
UML approprié.

```{figure} media/structure/image45.png
:width: 6.298611111111111in
:height: 4.299305555555556in

Diagramme de classe des représentations de tableau de bord
```

Il existe un composant javascript pour chaque représentation. Ces
derniers sont instanciés par une fabrique dédiée à laquelle on fait
notamment appel dans la fonctionnalité de création de tableau de bord.

Les fichiers liés à ce composant sont :

- Pour la mise en forme de données coté serveur (back) :

> **terriapi/terriapi/controller/donnees\_pour\_representation.py**

- Pour la restitution de données coté client (front) :

> **front/src/components/DiagrammeCirculaire.js**
>
> **front/src/components/** **DiagrammeRadar.js**
>
> **front/src/components/** **CourbesEmpilees.js**
>
> **front/src/components/** **CourbesComparees.js**
>
> **front/src/components/** **Histogramme.js**
>
> **front/src/components/** **Jauge.js**
>
> **front/src/components/** **MarqueurSVG.js**
>
> **front/src/components/** **FabriqueRepresentation.js**

### Les interfaces d'administration

#### La gestion des utilisateurs

Pour permettre aux utilisateurs d'enregistrer un certain nombre de
données et de les charger pour pouvoir y retravailler ou sortir les
résultats de leur travail lorsqu'ils le souhaitent, il est nécessaire
qu'ils puissent créer un compte et s'authentifier. TerriSTORY® est
donc pourvu de fonctionnalités qui permettent d'authentifier un
utilisateur en toute sécurité ainsi que d'une interface dédiée au moyen
de laquelle on peut gérer les travaux précédemment enregistrés.

Il existe également des fonctionnalités de partage de stratégies
territoriales afin que les territoires puissent s'échanger leurs travaux
entre eux.

Les fichiers qui permettent de gérer les utilisateurs sont :

- Coté client (front) : **front/src/components**/**UsersManage.js**

>    **front/src/components/Auth.js**

- Coté serveur (back) : **terriapi/terriapi/controller/auth.py**

#### La gestion des interfaces d'administration 

En outre, on distingue un autre genre d'acteur, Il s'agit des
administrateurs qui disposent en plus d'une interface spécifique qu'il
est prévu d'enrichir et grâce à laquelle l'administrateur peut faire
plusieurs actions :

- Activer / désactiver / supprimer les comptes utilisateurs
- Désactiver / activer les indicateurs
- Rendre modifiable / non modifiables les couches d'équipements.
- Personnaliser le pop-up d'accueil
- ...

```{figure} media/structure/image46.png
:width: 6.233333333333333in
:height: 2.973611111111111in

Le module des interfaces d'administrations
```

Le fichier qui permet de gérer les interfaces d'administration est :
**front/src/components/Profile.js**

### Le composant principal côté client

Au sein de ce composant **front/src/Index.js**, on distribue les données
dont on a besoin pour rendre fonctionnels tous les autres composant et
on met à disposition un certain nombre de fonctions utilisées un peu
partout dans le code JavaScript de l'application. Dans la plupart des
cas, le rôle de ces fonctions est de mettre à jour l'état du composant
principal afin de mettre à jour les données mobilisées par ailleurs dans
le reste de l'application.

Il constitue la porte d'entrée par laquelle on accède aux différentes
pages et où on fait appel à l'ensemble des composants de l'application.

### Le composant principal côté serveur

Le fichier **terriapi/terriapi/api.py** est le fichier qui fait le lien
entre les différentes composantes de l'application et les fichiers
python approprié (**api**).
