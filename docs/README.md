# Documentation

## Requis 

- `apt install python3-sphinx`
- `pip install sphinx-rtd-theme`
- `pip install sphinx_js docutils_react_docgen`
- `npm install --save-dev react-docgen`
- `npm install -g jsdoc`

## API

La génération de la documentation se fait en utilisant la librairie [sphinx](https://www.sphinx-doc.org/en/master/). Elle peut être décomposée en deux étapes :

1. la découverte automatique (via le plugin *autodoc*) des objets existants : pour cela, se placer dans le dossier de documentation (*./terriapi/docs/*), exécuter `sphinx-apidoc -o . ../terriapi/`. Ce travail a déjà été réalisé pour un instantané de l'application au mois de juin 2022 : il n'est pas nécessaire de le reproduire sauf changement majeur dans l'organisation de l'application. L'ensemble des informations générées lors de ce passage sont disponibles dans le dossier *terriapi/docs/api/*.
2. la génération des fichiers statics en HTML avec `make html` dans le même dossier (on pourra faire un `make clean` avant pour repartir d'une base sans cache).

La documentation créée est accessible en ouvrant le fichier *terriapi/docs/_build/html/index.html*.

## Front

Pour intégrer les commentaires de l'application **js**, nous utilisons la librairie [sphinx-js](https://pypi.org/project/sphinx-js/).

Les commentaires des fichiers du front seront pris en compte lors du passage `make html` (on pourra étudier les fichiers `rst` du dossier *terriapi/docs/* pour plus d'informations sur la façon dont ils sont pris en compte).

## Troubleshooting

### OSError with pip install

Si un dossier pose problème et empêche la fin de l'installation, il faut vérifier que c'est bien l'utilisateur courant qui a les permissions des dossiers dans **site-packages** et, le cas échéant, redonner la propriété avec `chown`.

### Permission error with `npm install -g jsdoc`

Dans le cas où l'installation locale de jsdoc ne fonctionne pas, il faut exécuter la commande avec **sudo**.
