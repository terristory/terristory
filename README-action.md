Use in env the command :

terriapi-action --action="..."

Available tests :
  "residential_refurbishment"
  "tertiary_refurbishment"
  "wind_power_plant"
  "hydro_power_plant"
  "pv_ground_based"
  "pv_on_roof/shade"
  "pv_on_roof/resid_small_roof"
  "pv_on_roof/serv_ind_big_roof"

Additional arguments --region("...") --zone"..." ("commune", "epci",...) --zoneid("...")  --output("...")

Default : --region("auvergne-rhone-alpes") --zone("epci") --zoneid("241501139") --output("/tmp")
                                            (Communauté de communes Pays de Salers)
If there is confidentiality on selected territory, it could be necessary to delete it or to select another territory in order to make some tests runable.
