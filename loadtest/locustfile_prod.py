from locust import HttpUser, TaskSet, between, task

REGION = "auvergne-rhone-alpes"


class FrontUser(HttpUser):
    wait_time = between(5, 9)

    @task
    def index(self):
        self.client.get("/")

    @task
    def about(self):
        self.client.get("/doc/{}/mentions_legales.html".format(REGION))


class ApiUser(HttpUser):
    wait_time = between(5, 9)

    @task
    def zone(self):
        self.client.get("/api/{}/zone".format(REGION))

    @task
    def analysis(self):
        self.client.get("/api/{}/analysis".format(REGION))

    @task
    def analysis_conso_ener(self):
        self.client.get(
            "/api/{}/analysis/1/data".format(REGION),
            params={"zone": "region", "maille": "epci", "zone_id": 1},
        )

    @task
    def analysis_stock_carbone(self):
        self.client.get(
            "/api/{}/analysis/29/data".format(REGION),
            params={"zone": "region", "maille": "epci", "zone_id": 1},
        )

    @task
    def poi(self):
        self.client.get("/api/{}/poi".format(REGION))

    @task
    def scot(self):
        self.client.get("/api/{}/zone/scot".format(REGION))


class OsmUser(HttpUser):
    wait_time = between(5, 9)

    @task
    def tile1(self):
        self.client.get("/7/63/46.pbf".format(REGION))

    @task
    def tile2(self):
        self.client.get("/7/66/45.pbf".format(REGION))
