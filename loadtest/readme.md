## Lancement des tests de charge

Remplacer le mot de passe de l'utilisateur basic auth au début du fichier locustfile.py et démarrer l'interface `locust.io`
avec le serveur cible voulu. 

    $ locust -f locustfile.py --host https://auvergnerhonealpes.dev.terristory.fr --hatch-rate 5 --users 5

Ouvrir l'url http://localhost:8089 puis lancer le run.

Le fichier locustfile_prod.py est sensiblement identique au précédent, excepté qu'il n'y a pas de mot de passe à définir (la prod est un site public).
