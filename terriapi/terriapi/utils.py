﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from __future__ import annotations

import unicodedata
from functools import wraps
from inspect import isawaitable

import slugify
from html_sanitizer import Sanitizer
from html_sanitizer.sanitizer import DEFAULT_SETTINGS
from sanic import response

from terriapi import controller
from terriapi.controller import regions_configuration, user
from terriapi.exceptions import RegionNotFound

SANITIZE_SETTINGS = dict(DEFAULT_SETTINGS)
# Add your changes
SANITIZE_SETTINGS["keep_typographic_whitespace"] = True
sanitizer = Sanitizer(SANITIZE_SETTINGS)


async def check_is_admin_in_region(current_user, region, allow_france=False):
    return current_user is not None and (
        current_user.est_admin
        and (
            current_user.region == region
            or (region == "france" and allow_france)
            or current_user.global_admin
        )
    )


def only_if_admin_in_region(region_key="region", allow_france=False):
    """
    Decorator pour tester si l'utilisateur a bien les droits d'administrateur
    sur la région considérée.

    Doit être placé après protected pour fonctionner
    """

    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            if region_key in kwargs:
                current_user = await user.auth_user(request, kwargs[region_key])
                is_admin = await check_is_admin_in_region(
                    current_user, kwargs[region_key], allow_france
                )
                if not is_admin:
                    return response.json(
                        {
                            "message": "Vous n'êtes pas autorisé·e à effectuer cette opération (non administrateur)."
                        },
                        status=401,
                    )
            else:
                return response.json(
                    {
                        "message": "Vous n'êtes pas autorisé·e à effectuer cette opération (informations manquantes)."
                    },
                    status=401,
                )
            _response = f(request, *args, **kwargs)
            if isawaitable(_response):
                _response = await _response
            return _response

        return decorated_function

    return decorator


async def valid_territory_type(region: str, territory_type: str | None) -> str | None:
    """
    Check that the territory type given is valid.
    """
    schema = region.replace("-", "_")
    res = await controller.fetch(
        f"""SELECT EXISTS (
        SELECT FROM {schema}.territoire
        WHERE type_territoire = $1
        )""",
        territory_type,
    )

    return territory_type if res[0]["exists"] or territory_type == "commune" else None


def is_valid_region(allowed_in_national_regions=False):
    """
    Decorator pour tester si l'utilisateur a bien les droits d'administrateur.

    Doit être placé après protected pour fonctionner
    """

    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            if "region" in kwargs:
                if kwargs["region"] != "france":
                    regions_settings = await regions_configuration.get_configuration(
                        kwargs["region"],
                        allow_national_regions=allowed_in_national_regions,
                    )
                    if len(regions_settings) == 0:
                        return response.json(
                            {"message": "Unavailable region."},
                            status=401,
                        )
                    region_config = regions_settings[0]
                    if (
                        region_config.get("is_national", False)
                        and region_config.get("id", "national") != "national"
                    ):
                        current_user = await user.auth_user(request, "national")
                        if current_user is None:
                            return response.json(
                                {"message": "Unauthorized user."},
                                status=401,
                            )

                        check_region = await user.is_user_in_national_region(
                            current_user.mail, kwargs["region"]
                        )

                        if not check_region:
                            return response.json(
                                {"message": "Unauthorized region."},
                                status=401,
                            )

            _response = f(request, *args, **kwargs)
            if isawaitable(_response):
                _response = await _response
            return _response

        return decorated_function

    return decorator


async def is_forbidden_technical_table(region, table_name):
    # static list of protected tables because they are used by some other code
    protected_tables = [
        "cesba_notes",
        "cesba_stats",
        "chiffres_cle",
        "clap",
        "confid_camembert",
        "confid_maille",
        "etat_conversion_perimetre",
        "global_irradiation",
        "historique_indicateurs",
        "sankey_confidentiality",
        "siren_assignment",
        "territoire",
        "zone",
    ]
    return (
        await valid_territory_type(region, table_name) or table_name in protected_tables
    )


async def check_is_valid_table(schema_name, table_name):
    # récupération du dictionnaire.
    res = await controller.fetch(
        """SELECT EXISTS (
        SELECT FROM information_schema.tables 
        WHERE  table_schema = $1
        AND    table_name   = $2
        )""",
        schema_name,
        table_name,
    )
    return res[0]["exists"]


def is_valid_tablename(
    schema_key_name, table_key_name, replace_dots_schema=True, schema_suffix=""
):
    """
    Decorator pour tester si l'utilisateur a bien les droits d'administrateur.

    Doit être placé après protected pour fonctionner
    """

    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            if table_key_name in kwargs and schema_key_name in kwargs:
                if replace_dots_schema:
                    schema = kwargs[schema_key_name].replace("-", "_")
                else:
                    schema = kwargs[schema_key_name]
                is_valid = await check_is_valid_table(
                    schema + schema_suffix, kwargs[table_key_name]
                )
                if not is_valid:
                    return response.json(
                        {
                            "message": "Vous n'êtes pas autorisé·e à effectuer cette opération (table inexistante)."
                        },
                        status=401,
                    )
            else:
                return response.json(
                    {
                        "message": "Vous n'êtes pas autorisé·e à effectuer cette opération (informations manquantes sur la table manipulée)."
                    },
                    status=401,
                )
            _response = f(request, *args, **kwargs)
            if isawaitable(_response):
                _response = await _response
            return _response

        return decorated_function

    return decorator


def parse_table_name(table_name):
    # we shorten the table name (PostgreSQL has a limit) + we replace - by _
    return slugify.slugify(table_name)[0:40].replace("-", "_").strip("_")


def parse_list_and_ranges(string: str):
    """
    Parse a str representing int values and ranges into the list of values
    Example: "2013,2018-2021" => [2013, 2018, 2019, 2020, 2021]
    """
    years_ranges = [yr.strip() for yr in string.split(",")]
    years_ranges = [_parse_range(yr) for yr in years_ranges]
    return sorted({y for yr in years_ranges for y in yr}, reverse=True)


def _parse_range(string: str):
    """
    Parse a str representing ranges: numbers separated by dashes.

    Returns:
        - The list of numbers between the lowest and highest number in the range, bounds included
        - or [] in case of an empty string
        - or [n] if there in only one number n
    """
    bounds = [int(b.strip()) for b in string.split("-") if b]
    if len(bounds) == 0:
        return []
    return list(range(min(bounds), max(bounds) + 1))


def sanitize(content):
    return sanitizer.sanitize(content)


def normalize_text(text):
    """
    Normalizes text by:
    - Converting to lowercase
    - Stripping leading and trailing whitespaces
    - Removing accents
    - Replacing multiple spaces with a single space

    Args:
        text (str): The text to normalize.

    Returns:
        str: Normalized text.
    """
    if isinstance(text, str):
        text = text.strip().lower()
        text = (
            unicodedata.normalize("NFKD", text)
            .encode("ASCII", "ignore")
            .decode("utf-8")
        )
        text = " ".join(text.split())
        return text
    return text


async def check_national_region(usermail, territory):
    check_region = await regions_configuration.check_is_valid_region(territory)
    if not check_region:
        return False
    check_region = await user.is_user_in_national_region(usermail, territory)
    if not check_region:
        return False
    return True


def is_valid_national_region():
    """
    Decorator pour tester si un schéma national est valide et associé à l'utilisateur actuel.

    Doit être placé après protected pour fonctionner
    """

    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            if "territory" in kwargs:
                current_user = await user.auth_user(request, "national")
                check_region = await check_national_region(
                    current_user.mail, kwargs["territory"]
                )
                if not check_region:
                    return response.json(
                        {"status": "error", "message": "Region not found."},
                        status=404,
                    )
            _response = f(request, *args, **kwargs)
            if isawaitable(_response):
                _response = await _response
            return _response

        return decorated_function

    return decorator
