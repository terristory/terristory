# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from __future__ import annotations

from sanic import Blueprint
from sanic.response import json
from sanic_ext import openapi

from terriapi.controller import regions_configuration

from .models import QRegion
from .serializers import region_serializer

bp_regions = Blueprint("regions")


@bp_regions.get("/")
@openapi.summary("Retrieves available regions list.")
@openapi.response(200, {"application/json": [QRegion]})
async def list(request):
    regions = await regions_configuration.get_configuration()
    parsed_regions = [region_serializer(region, request) for region in regions]

    return json(
        {
            "resources": parsed_regions,
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_regions.list",
                        _external=True,
                    ),
                }
            },
        }
    )
