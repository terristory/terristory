# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from __future__ import annotations

from sanic import Blueprint, response
from sanic_ext import openapi

from terriapi.controller import regions_configuration, zone
from terriapi.controller.analyse import analysis

from . import serializers
from .indicators import bp_indicator
from .models import *

bp_region = Blueprint("region")
regional_bp = Blueprint.group(bp_region, bp_indicator, url_prefix="/<region>")


# we check that the region used for all regional endpoints is valid.
@regional_bp.middleware("request")
async def is_region_valid(request, *args, **kwargs):
    if "region" not in request.match_info:
        return response.json({"message": "No region provided."}, status=401)
    if not await regions_configuration.check_is_valid_region_not_national(
        request.match_info.get("region")
    ):
        return response.json(
            {
                "message": "Region provided is invalid.",
                "_links": {
                    "contents": {
                        "href": request.app.url_for(
                            "terriapi.v2_regions.list",
                            _external=True,
                        )
                    }
                },
            },
            status=401,
        )


# @utils.is_valid_region()


@bp_region.get("/")
@openapi.summary("Provides details for a single region")
@openapi.response(
    200,
    {"application/json": QRegionDetails},
    "Default answer when corresponding region exists.",
)
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Region provided is invalid."
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
async def region(request, region: str):
    return response.json(
        {
            "id": region,
            "geometry": "",
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_region.region",
                        region=region,
                        _external=True,
                    ),
                },
                "up": {
                    "title": "List of all regions available",
                    "href": request.app.url_for(
                        "terriapi.v2_regions.list",
                        _external=True,
                    ),
                },
                "indicators": {
                    "title": "Indicators",
                    "href": request.app.url_for(
                        "terriapi.v2_region.indicators",
                        region=region,
                        _external=True,
                    ),
                    "methods": ["GET"],
                },
                "territories_types": {
                    "title": "Territories types",
                    "href": request.app.url_for(
                        "terriapi.v2_region.territories_types",
                        region=region,
                        _external=True,
                    ),
                    "methods": ["GET"],
                },
            },
        }
    )


@bp_region.get("/territories")
@openapi.summary("Provides types of territories available in a region")
@openapi.response(200, {"application/json": [QTerritory]})
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Region provided is invalid."
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
async def territories_types(request, region: str):
    territories = await zone.all_territorial_levels(region)
    return response.json(
        {
            "_embedded": [
                serializers.territory_type_serializer(region, territory, request)
                for territory in territories
            ],
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_region.territories_types",
                        region=region,
                        _external=True,
                    ),
                },
                "collection": {
                    "title": "Region endpoints",
                    "href": request.app.url_for(
                        "terriapi.v2_region.region",
                        region=region,
                        _external=True,
                    ),
                    "methods": ["GET"],
                },
            },
        }
    )


@bp_region.get("/territories/<territory_type>/")
@openapi.summary("Provides territories of a certain type available in a region")
@openapi.response(200, {"application/json": [QTerritory]})
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Region provided is invalid."
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
@openapi.parameter(
    "territory_type", str, "path", required=True, description="The type of territory"
)
@openapi.parameter(
    "with_geom", bool, "query", description="Include geometry or not (default: False)"
)
async def territories(
    request, region: str, territory_type: str, with_geom: bool = False
):
    territories = await zone.all_territories_for_type(region, territory_type)

    return response.json(
        {
            "_embedded": [
                serializers.territory_serializer(territory, with_geom)
                for territory in territories
            ],
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_region.territories",
                        region=region,
                        territory_type=territory_type,
                        _external=True,
                    ),
                },
                "up": {
                    "title": "Region territories types",
                    "href": request.app.url_for(
                        "terriapi.v2_region.territories_types",
                        region=region,
                        _external=True,
                    ),
                    "methods": ["GET"],
                },
            },
        }
    )


@bp_region.get("/indicators")
@openapi.summary("Provides all indicators available in a region")
@openapi.response(200, {"application/json": QIndicators})
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Region provided is invalid."
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
async def indicators(request, region: str):
    indicators = await analysis(region, only_exportable=True)

    return response.json(
        {
            "indicators": [
                serializers.indicator_serializer(region, indicator, request)
                for indicator in indicators
            ],
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_region.indicators",
                        region=region,
                        _external=True,
                    ),
                },
                "up": {
                    "title": "Region endpoints",
                    "href": request.app.url_for(
                        "terriapi.v2_region.region",
                        region=region,
                        _external=True,
                    ),
                    "methods": ["GET"],
                },
            },
        }
    )
