# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from sanic import Blueprint, response
from sanic_ext import openapi

from .api import *
from .meta import *
from .region import *
from .trajectories import *

root = Blueprint("root")
bp_api = Blueprint.group(bp_regions, regional_bp, url_prefix="/regions")
routes_v2 = Blueprint.group(
    root, bp_meta, bp_api, bp_trajectories, name_prefix="v2", version=2
)


@root.get("/")
@openapi.summary("Main entrypoint of TerriSTORY® API")
@openapi.response(200)
async def list(request):
    return response.json(
        {
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_root.list",
                        _external=True,
                    )
                },
                "regions": {
                    "href": request.app.url_for(
                        "terriapi.v2_regions.list",
                        _external=True,
                    ),
                    "rel": "regions:query",
                },
                "meta": {
                    "href": request.app.url_for(
                        "terriapi.v2_meta.list",
                        _external=True,
                    ),
                    "rel": "meta:query",
                },
                "trajectories": {
                    "href": request.app.url_for(
                        "terriapi.v2_trajectories.list",
                        _external=True,
                    ),
                    "rel": "meta:query",
                },
            }
        }
    )
