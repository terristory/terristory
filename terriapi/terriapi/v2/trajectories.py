# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from sanic import Blueprint, response
from sanic.response import HTTPResponse
from sanic_ext import openapi

from terriapi import utils
from terriapi.api import (
    get_pcaet_trajectories_list,
    get_pcaet_trajectory_details,
    get_raw_pcaet_table,
)

from .indicators import METADATA
from .models import QPCAETData, QTerriAPIError, QTrajectoryData, QTrajectoryMetadata

bp_trajectories = Blueprint("trajectories", url_prefix="/trajectories")


@bp_trajectories.route("/")
@openapi.summary("Provides all links available for trajectories")
@openapi.response(200, {"application/json": dict})
async def list(request) -> HTTPResponse:
    """
    This lists all meta parameters existing
    """

    return response.json(
        {
            "_links": {
                "raw_pcaet_table": {
                    "href": request.app.url_for(
                        "terriapi.v2_trajectories.raw_pcaet_table",
                        _external=True,
                    ),
                    "rel": "pcaet:list",
                    "type": "text/csv",
                },
                "pcaet_trajectories_list": {
                    "href": request.app.url_for(
                        "terriapi.v2_trajectories.pcaet_trajectories_list",
                        region="example_region",
                        _external=True,
                    ),
                    "rel": "region-pcaet:list",
                },
                "pcaet_trajectory_details": {
                    "href": request.app.url_for(
                        "terriapi.v2_trajectories.pcaet_trajectory_details",
                        region="example_region",
                        trajectory_id="trajectory_id",
                        _external=True,
                    ),
                    "rel": "region-pcaet-trajectory:details",
                },
                "up": {
                    "href": request.app.url_for(
                        "terriapi.v2_root.list",
                        _external=True,
                    )
                },
            },
        }
    )


@bp_trajectories.route("/raw/pcaet")
@openapi.summary("Gives access to raw PCAET data.")
@openapi.response(200, {"text/csv": [QPCAETData]})
@openapi.response(
    404, {"application/json": QTerriAPIError}, description="No PCAET found."
)
@openapi.response(
    404,
    {"application/json": QTerriAPIError},
    description="Unable to provide PCAET file.",
)
async def raw_pcaet_table(request):
    return await get_raw_pcaet_table(request)


@bp_trajectories.route(r"/<region>/pcaet/trajectories/list", methods=["GET"])
@openapi.summary("Provides list of trajectoires for a single region.")
@openapi.parameter("region", str, "path", required=True, description="The region slug")
@openapi.response(200, {"application/json": [QTrajectoryMetadata]})
@openapi.response(
    401, {"application/json": QTerriAPIError}, description="Unauthorized region."
)
@utils.is_valid_region()
async def pcaet_trajectories_list(request, region):
    return await get_pcaet_trajectories_list(request, region)


@bp_trajectories.route(
    r"/<region>/pcaet/trajectory/details/<trajectory_id>/", methods=["GET"]
)
@openapi.summary("Provides details of a single trajectory.")
@openapi.parameter("region", str, "path", required=True, description="The region slug")
@openapi.parameter(
    "trajectory_id",
    str,
    "path",
    required=True,
    description="The trajectory ID (available in trajectories list)",
)
@openapi.parameter(
    "zone_id", str, "query", description="The zone ID (default: current zone code)"
)
@openapi.parameter(
    "zone", str, "query", description="The type of zone (default: region)"
)
@openapi.response(200, {"application/json": [QTrajectoryData]})
@openapi.response(
    401, {"application/json": QTerriAPIError}, description="Unauthorized region."
)
@utils.is_valid_region()
async def pcaet_trajectory_details(request, region, trajectory_id):
    return await get_pcaet_trajectory_details(request, region, trajectory_id)
