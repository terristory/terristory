# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from sanic import Blueprint, response
from sanic.response import HTTPResponse
from sanic_ext import openapi

from .indicators import METADATA

bp_meta = Blueprint("meta", url_prefix="/meta")


@bp_meta.route("/")
@openapi.summary("Provides all metadata available in a region")
@openapi.response(200, {"application/json": dict})
async def list(request) -> HTTPResponse:
    """
    This lists all meta parameters existing
    """

    return response.json(
        {
            "metadata": [
                {"key": k, "description": v["description"]} for k, v in METADATA.items()
            ],
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_meta.list",
                        _external=True,
                    ),
                    "rel": "meta:query",
                },
                "up": {
                    "href": request.app.url_for(
                        "terriapi.v2_root.list",
                        _external=True,
                    )
                },
            },
        }
    )
