# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from typing import Dict, List, Optional, TypedDict

from sanic_ext import openapi


@openapi.component
class QLink:
    title: str
    rel: str
    href: str
    methods: List[str]


@openapi.component
class QRegion:
    id: str
    name: str
    slug: str
    contact: str
    link: QLink


@openapi.component
class QRegionDetails(QRegion):
    indicators: str
    territories: str


@openapi.component
class QCatValue:
    name: str
    color: str
    modality: str
    data: str


@openapi.component
class QTerrValue:
    code: str
    val: str
    name: Optional[str]
    x: Optional[str]
    y: Optional[str]


@openapi.component
class QDataByCat:
    id: str
    name: str
    results: Dict[str, List[QCatValue]]


@openapi.component
class QDataByTerr:
    id: str
    name: str
    results: Dict[str, List[QTerrValue]]


@openapi.component
class QTerritory:
    code: str
    name: str
    geometry: str


@openapi.component
class QCategory:
    id: str
    name: str
    modalities: List[str]


@openapi.component
class QFilter(TypedDict):
    filtre_categorie: str


@openapi.component
class QFilters:
    filters: Dict[str, List[QFilter]]


@openapi.component
class QMetaData:
    id: int
    metadata: Dict[str, Dict[str, str]]


@openapi.component
class QIndicators:
    indicators: List[Dict[str, str]]


@openapi.component
class QPCAETData:
    siren: str
    trajectory: str
    category: int
    year: int
    value: float
    is_ref: bool
    variation: float


@openapi.component
class QTrajectoryMetadata:
    id: str
    table: str
    name: str
    unit: str
    categories: Dict[str, Dict[str, str]]
    filters: Dict[str, List[QFilter]]


@openapi.component
class QTrajectoryYearData:
    annee: int
    valeur: float


@openapi.component
class QTrajectoryHistoricalData:
    nom: str
    couleur: str
    confidentiel: str
    data: List[QTrajectoryYearData]


@openapi.component
class QTrajectorySupraGoalData:
    valeurs_annees: List[QTrajectoryYearData]
    titre: str
    couleur: str
    annee_reference: int
    valeur_reference: float
    affectation: str
    description: str
    filter: List[str]
    table: str


@openapi.component
class QTrajectoryData:
    pcaet_trajectory: List[QTrajectoryHistoricalData]
    unit: str
    category: str
    ref_year: str
    historical_data: List[QTrajectoryHistoricalData]
    supra_goals: List[QTrajectorySupraGoalData]


@openapi.component
class QTerriAPIError:
    message: str
