# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json

from .models import QRegion, QTerritory


def region_serializer(data: dict, request) -> QRegion:
    return {
        "id": data["id"],
        "name": data["label"],
        "contact": data["contact_email"],
        "_links": {
            "down": {
                "title": data["label"],
                "href": request.app.url_for(
                    "terriapi.v2_region.region",
                    region=data["id"],
                    _external=True,
                ),
                "methods": ["GET"],
            },
        },
    }


def territory_type_serializer(region: str, territory_type: str, request) -> dict:
    return {
        "id": territory_type,
        "_links": {
            "item": {
                "title": territory_type,
                "href": request.app.url_for(
                    "terriapi.v2_region.territories",
                    region=region,
                    territory_type=territory_type,
                    _external=True,
                ),
                "methods": ["GET"],
            },
        },
    }


def territory_serializer(data: dict, with_geom: bool) -> QTerritory:
    out = {
        "code": data["code"],
        "name": data["name"],
    }
    if with_geom:
        out["geom"] = data["geom"]
    return out


def indicator_serializer(region: str, data: dict, request) -> dict:
    charts = data.get("charts", [])
    nb_charts = 0 if charts == [None] else len(charts)
    return {
        "id": data["id"],
        "name": data["nom"],
        "has_confidentiality_rules": data["confidentiel"] != "",
        "nb_categories": nb_charts,
        "_links": {
            "data_by_territories": {
                "title": "Aggregated data by territory",
                "href": request.app.url_for(
                    "terriapi.v2_indicator.indicator_data_by_territory",
                    region=region,
                    indicator_id=data["id"],
                    _external=True,
                ),
                "methods": ["GET"],
            },
            "data_by_category": {
                "title": "Aggregated data by category",
                "href": request.app.url_for(
                    "terriapi.v2_indicator.indicator_data_by_category",
                    region=region,
                    indicator_id=data["id"],
                    _external=True,
                ),
                "methods": ["GET"],
            },
            "metadata": {
                "title": "Full metadata",
                "href": request.app.url_for(
                    "terriapi.v2_indicator.indicator_metadata",
                    region=region,
                    indicator_id=data["id"],
                    _external=True,
                ),
                "methods": ["GET"],
            },
            "categories": {
                "title": "Categories associated",
                "href": request.app.url_for(
                    "terriapi.v2_indicator.indicator_categories_data",
                    region=region,
                    indicator_id=data["id"],
                    _external=True,
                ),
                "methods": ["GET"],
            },
        },
    }


def category_serializer(data) -> dict:
    return {
        "key": data["key"],
        "title": data["title"],
        "modalities": json.loads(data["modalities"]),
    }


def categories_serializer(region: str, data: list, request) -> dict:
    categories = [category_serializer(d) for d in data]
    return {"categories": categories}
