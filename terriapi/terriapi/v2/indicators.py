# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from __future__ import annotations

import json

from sanic import Blueprint, response
from sanic_ext import openapi

from terriapi import utils
from terriapi.controller import analyse as controller_analyse
from terriapi.controller import (
    compute_formula,
    get_update_date,
    guess_frequency_update,
    zone,
)
from terriapi.controller.analyse import analysis, list_indicator_categories
from terriapi.controller.donnees_pour_representation import DonneesPourRepresentations
from terriapi.controller.indicateur_tableau_bord import IndicateurTableauBord

from . import serializers
from .models import *

bp_indicator = Blueprint("indicator", url_prefix="/indicators/<indicator_id>")

METADATA = {
    "name": {
        "description": "Name of the indicator",
        "input_key": "nom",
    },
    "unit": {
        "description": "Unit defined as output",
        "input_key": "unit",
    },
    "available_years": {
        "description": "All years available for the data required. Warning, this concerns mostly the main data in case of ratio indicators.",
        "input_key": "years",
    },
    "estimated_years": {
        "description": "Specify if some data are estimated for some years. It can be the case when manipulating data based on statistics or models.",
        "input_key": "estimated_years",
    },
    "credits_indicator_producers": {
        "description": "Actor(s) which produced the indicator.",
        "input_key": "credits_analysis_producers",
        "func": lambda x: json.loads(x),
    },
    "credits_data_sources": {
        "description": "Actor(s) which provide the data source used to build raw data.",
        "input_key": "credits_data_sources",
        "func": lambda x: json.loads(x),
    },
    "credits_data_producers": {
        "description": "Actor(s) which produced the raw data initially.",
        "input_key": "credits_data_producers",
        "func": lambda x: json.loads(x),
    },
    "methodology_link": {
        "description": "Link to a methodology PDF file that provides more detail on the indicator.",
        "input_key": "methodo_pdf",
    },
    "categories": {
        "description": "Dictionary of all categories available for this indicator.",
        "input_key": "charts",
        "func": lambda x: {cat["categorie"]: cat["titre"] for cat in x},
    },
    "data_dependencies": {
        "description": "List of data used to compute this indicator. In most cases, it includes only one data but it can also include another data when computing ratio.",
    },
    "last_update": {
        "description": "Date of last update of the data used for this indicator."
    },
    "update_frequency": {
        "description": "Estimated frequency (in days) of update for the data used in this indicator."
    },
}


@bp_indicator.get("/metadata/")
@openapi.summary("Whole metadata available for a single indicator.")
@openapi.response(200, {"application/json": QMetaData})
@openapi.response(
    400, {"application/json": QTerriAPIError}, "Indicator id must be an integer."
)
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Indicator is disabled or unavailable."
)
@openapi.parameter(
    "indicator_id", int, "path", required=True, description="The indicator id"
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
async def indicator_metadata(request, region: str, indicator_id: int):
    try:
        indicator_id = int(indicator_id)
    except ValueError:
        return response.json(
            {"message": "Indicator id must be an integer."}, status=400
        )
    simple_metadata = await analysis(region, indicator_id)
    simple_metadata = dict(simple_metadata)

    if not simple_metadata:
        return response.json(
            {"message": f"Indicator {indicator_id} is disabled or unavailable"},
            status=404,
        )

    formula = compute_formula(simple_metadata["data"])
    dependencies = [formula["indicateur"]]
    if formula["ratio"] != "":
        dependencies.append(formula["ratio"].replace("/maille.", ""))

    data_history = await get_update_date(region, dependencies)

    custom_metadata = {
        "methodology_link": {
            "func": lambda x: f"/pdf/{region}/{x}",
        },
        "data_dependencies": {"content": dependencies},
        "last_update": {
            "content": {
                k: data_history.get(k, [{}])[0].get("date_maj", "Unknown")
                for k in dependencies
            }
        },
        "update_frequency": {
            "content": {
                k: guess_frequency_update(data_history.get(k, [{}]))
                for k in dependencies
            }
        },
    }
    metadata = {k: {**v, **custom_metadata.get(k, {})} for k, v in METADATA.items()}

    return response.json(
        {
            "id": indicator_id,
            "metadata": {
                k: v.get("func", lambda x: x)(
                    v.get("content", simple_metadata[v.get("input_key", "id")])
                )
                for k, v in metadata.items()
            },
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_indicator.indicator_metadata",
                        region=region,
                        indicator_id=indicator_id,
                        _external=True,
                    ),
                    "methods": ["GET", "POST"],
                },
                "agg_by_territory": {
                    "href": request.app.url_for(
                        "terriapi.v2_indicator.indicator_data_by_territory",
                        region=region,
                        indicator_id=indicator_id,
                        _external=True,
                    ),
                    "methods": ["GET", "POST"],
                },
                "agg_by_category": {
                    "href": request.app.url_for(
                        "terriapi.v2_indicator.indicator_data_by_category",
                        region=region,
                        indicator_id=indicator_id,
                        _external=True,
                    ),
                    "methods": ["GET", "POST"],
                },
                "up": {
                    "title": "List of regional indicators",
                    "href": request.app.url_for(
                        "terriapi.v2_region.indicators",
                        region=region,
                        _external=True,
                    ),
                    "methods": ["GET"],
                },
            },
        }
    )


@bp_indicator.get("/data/by/territory/")
@bp_indicator.post("/data/by/territory/", name="data by territory post")
@openapi.summary("Computes data for an indicator in a region.")
@openapi.description(
    """Endpoint that provides access to an indicator values, aggregated for all 
territories at a certain mesh level, for a specific geographical perimeter. 
All categories will be merged into the total value. It can be used with GET or POST 
query. POST enables to apply filters on some categories by providing a dictionary 
with categories and the modalities that should be included in the query (see example below). 
Default means all modalities of all categories will be included.
    
<pre>{
    "secteur": ["Gestion des déchets", "Industrie hors branche énergie"],
    "energie": ["Gaz"],
    "usage": ["Chauffage"]
}</pre>"""
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
@openapi.parameter(
    "mesh",
    str,
    "query",
    description="The kind of geographical division used to distribute data (default: epci)",
)
@openapi.parameter(
    "zone_id", str, "query", description="The zone ID (default: current zone code)"
)
@openapi.parameter(
    "zone_type", str, "query", description="The type of zone (default: region)"
)
@openapi.parameter(
    "indicator_id", int, "path", required=True, description="The indicator id"
)
@openapi.parameter(
    "year",
    int,
    "path",
    description="Restrict the query to a custom year (default: last year data)",
)
@openapi.parameter(
    "include_geography",
    bool,
    "query",
    description="Whether to include geographical location details (default: false)",
)
@openapi.body(
    {"application/json": QFilters},
    required=False,
    description="Provides optional filters to apply on the categories of the indicator.",
)
@openapi.response(200, {"application/json": QDataByTerr})
@openapi.response(
    400, {"application/json": QTerriAPIError}, "Indicator id must be an integer."
)
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Indicator is disabled or unavailable."
)
@openapi.response(
    404,
    {"application/json": QTerriAPIError},
    "Couldn't find your territory, please specify a valid code or refer to the list of territories.",
)
async def indicator_data_by_territory(request, region: str, indicator_id: int):
    return await indicator_data(request, region, indicator_id, "by_territory")


@bp_indicator.get("/data/by/category/")
@bp_indicator.post("/data/by/category/", name="data by category post")
@openapi.summary("Computes aggregated data by category for an indicator in a region.")
@openapi.description(
    """Endpoint that provides access to an indicator values, aggregated for all modalities
of a certain list of categories, for a specific geographical perimeter. 
All territories inside this perimeter will be merged into the total value. 
It can be used with GET or POST query. 
POST enables to apply filters on some categories by providing a dictionary 
with categories and the modalities that should be included in the query (see example below). 
Default means all modalities of all categories will be included.
    
<pre>{
    "secteur": ["Gestion des déchets", "Industrie hors branche énergie"],
    "energie": ["Gaz"],
    "usage": ["Chauffage"]
}</pre>"""
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
@openapi.parameter(
    "mesh",
    str,
    "query",
    description="The kind of geographical division used to distribute data (default: epci)",
)
@openapi.parameter(
    "zone_id", str, "query", description="The zone ID (default: current zone code)"
)
@openapi.parameter(
    "zone_type", str, "query", description="The type of zone (default: region)"
)
@openapi.parameter(
    "indicator_id", int, "path", required=True, description="The indicator id"
)
@openapi.parameter(
    "year",
    int,
    "path",
    description="Restrict the query to a custom year (default: last year data)",
)
@openapi.parameter(
    "cat_agg",
    str,
    "query",
    description="Eventual category aggregations. Must contain the list of categories to aggregate, separated by a comma (default, all categories are used to compute aggregated values).",
)
@openapi.body(
    {"application/json": QFilters},
    required=False,
    description="Provides optional filters to apply on the categories of the indicator.",
)
@openapi.response(200, {"application/json": QDataByCat})
@openapi.response(
    400, {"application/json": QTerriAPIError}, "Indicator id must be an integer."
)
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Indicator is disabled or unavailable."
)
@openapi.response(
    404,
    {"application/json": QTerriAPIError},
    "Couldn't find your territory, please specify a valid code or refer to the list of territories.",
)
async def indicator_data_by_category(request, region: str, indicator_id: int):
    return await indicator_data(request, region, indicator_id, "by_category")


async def indicator_data(
    request, region: str, indicator_id: int, type_of_agg: str
) -> response.HTTPResponse:
    try:
        indicator_id = int(indicator_id)
    except ValueError:
        return response.json(
            {"message": "Indicator id must be an integer."}, status=400
        )
    # zone géographique passée en query arg (maille de visualisation)
    zone_type = request.args.get("zone_type")
    zone_type = await utils.valid_territory_type(region, zone_type) or "region"
    mesh = request.args.get("mesh")
    mesh = await utils.valid_territory_type(region, mesh) or "epci"
    cat_agg = request.args.get("cat_agg") or ""
    if len(cat_agg) > 0:
        cat_agg = cat_agg.split(",")
    else:
        cat_agg = []

    # identifiant de la zone étudiée
    zone_id = request.args.get("zone_id")
    if zone_type == "region" and zone_id is None:
        codes = await zone.all_territories_codes_for_type(region, "region")
        zone_id = codes[0]
    if zone_id is None:
        return response.json(
            {
                "message": f"Couldn't find your territory, please specify a valid code or refer to the list of territories.",
                "_links": {
                    "contents": {
                        "href": request.app.url_for(
                            "terriapi.v2_region.territories",
                            region=region,
                            _external=True,
                        )
                    }
                },
            },
            status=404,
        )
    filtre = request.args.get("threshold")
    filtres_categorie = request.json
    reinitialiser_filtres = request.args.get("reinitFilters")

    if filtres_categorie is None:
        filtres_categorie = {}
    else:
        filtres_categorie = {
            cat: [{"filtre_categorie": f"{cat}.{f}" for f in fs}]
            for cat, fs in filtres_categorie.get("filters", {}).items()
        }
    if not filtre:
        filtre = 0

    # on ajoute l'id "métier" du territoire dans la réponse
    annee = request.args.get("year", None)
    annee = int(annee) if annee is not None else None

    indicateur = IndicateurTableauBord(region, indicator_id)
    aconf = await indicateur.meta_donnees_indicateur()
    if not aconf:
        return response.json(
            {"message": f"Indicator {indicator_id} is disabled or unavailable"},
            status=404,
        )

    # get unit
    unit = request.args.get("unit", None)
    unit = int(unit) if unit is not None else None
    unit_params = await controller_analyse.get_unit_from_analysis(
        region, unit, indicator_id, mesh
    )
    if "unit_name" in unit_params:
        aconf["unit"] = unit_params["unit_name"]

    if not aconf.get("active", False):
        return response.json(
            {"message": f"Indicator {indicator_id} is disabled or unavailable"},
            status=404,
        )

    donnees_pour_representations = DonneesPourRepresentations(
        aconf,
        mesh,
        zone_type,
        zone_id,
        specific_year=annee,
        specific_unit_params=unit_params,
    )
    results = await donnees_pour_representations.donnees_finales(
        "indicator", filtre, filtres_categorie, reinitialiser_filtres
    )

    # giving only specific data required
    if type_of_agg == "by_category":
        f_results = {}
        for chart in results["charts"]:
            if len(cat_agg) > 0 and chart["name"] not in cat_agg:
                continue
            f_results[chart["name"]] = list(
                {"name": label, "color": color, "modality": modality, "data": data}
                for data, label, modality, color in zip(
                    chart["data"],
                    chart["labels"],
                    chart["modalite_id"],
                    chart["colors"],
                )
            )
        results = f_results
        other_agg = "by_territory"
    else:
        include_geography = request.args.get("include_geography", False)
        if not include_geography:
            results = [
                {
                    "code": r["code"],
                    "val": r["val"],
                }
                for r in results["map"]
            ]
        else:
            results = results["map"]
        other_agg = "by_category"

    return response.json(
        {
            "id": indicator_id,
            "name": aconf["nom"],
            "results": results,
            "_parameters": {
                "zone_type": zone_type,
                "zone_id": zone_id,
                "mesh": mesh,
                "cat_agg": cat_agg,
                "year": annee,
            },
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_indicator.indicator_data_" + type_of_agg,
                        region=region,
                        indicator_id=indicator_id,
                        _external=True,
                    ),
                    "methods": ["GET", "POST"],
                },
                "metadata": {
                    "href": request.app.url_for(
                        "terriapi.v2_indicator.indicator_metadata",
                        region=region,
                        indicator_id=indicator_id,
                        _external=True,
                    ),
                    "methods": ["GET", "POST"],
                },
                "other_agg": {
                    "href": request.app.url_for(
                        "terriapi.v2_indicator.indicator_data_" + other_agg,
                        region=region,
                        indicator_id=indicator_id,
                        _external=True,
                    ),
                    "methods": ["GET", "POST"],
                },
                "up": {
                    "title": "List of regional indicators",
                    "href": request.app.url_for(
                        "terriapi.v2_region.indicators",
                        region=region,
                        _external=True,
                    ),
                    "methods": ["GET"],
                },
            },
        }
    )


@bp_indicator.get("/categories")
@openapi.summary("Prints available categories for an indicator in a region.")
@openapi.response(200, {"application/json": [QCategory]})
@openapi.response(
    400, {"application/json": QTerriAPIError}, "Indicator id must be an integer."
)
@openapi.response(
    404, {"application/json": QTerriAPIError}, "Indicator is disabled or unavailable."
)
@openapi.parameter("zone", str, "query", description="The zone ID (default: all zones)")
@openapi.parameter(
    "zone_type", str, "query", description="The type of zone (default: all zones types)"
)
@openapi.parameter(
    "indicator_id", int, "path", required=True, description="The indicator id"
)
@openapi.parameter("region", str, "path", required=True, description="The region slug")
async def indicator_categories_data(
    request,
    region: str,
    indicator_id: int,
    zone_type: str = "",
    zone: str = "",
):
    try:
        indicator_id = int(indicator_id)
    except ValueError:
        return response.json(
            {"message": "Indicator id must be an integer."}, status=400
        )
    simple_metadata = await analysis(region, indicator_id)
    simple_metadata = dict(simple_metadata)

    if not simple_metadata:
        return response.json(
            {"message": f"Indicator {indicator_id} is disabled or unavailable"},
            status=404,
        )
    categories = await list_indicator_categories(region, indicator_id)

    return response.json(
        {
            "id": indicator_id,
            **serializers.categories_serializer(region, categories, request),
            "_links": {
                "self": {
                    "href": request.app.url_for(
                        "terriapi.v2_indicator.indicator_categories_data",
                        region=region,
                        indicator_id=indicator_id,
                        _external=True,
                    ),
                },
                "up": {
                    "title": "All indicators",
                    "href": request.app.url_for(
                        "terriapi.v2_region.indicators",
                        region=region,
                        _external=True,
                    ),
                },
            },
        }
    )
