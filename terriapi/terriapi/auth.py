﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""
Module dédié à la partie d'authentification JWT de l'application.
"""

from datetime import datetime as dt

from sanic import Sanic, response
from sanic_jwt import BaseEndpoint, Initialize, exceptions
from sanic_jwt.decorators import protected
from sanic_jwt.responses import _set_cookie

from terriapi import settings, user
from terriapi.controller import user as controller
from terriapi.controller.user import (
    User,
    auth_user,
    delete_refresh_token,
    send_mail,
    users,
)

# Expérition des refresh tokens (en secondes). On a ici une journée.
REFRESH_EXPIRATION = 24 * 60 * 60


async def authenticate(request, region):
    """
    Authenticate a user inside current region with login and password.

    Parameters
    ----------
    request : sanic request
        must contain json args with login and password keys
    region : str
        region key


    Returns
    -------
    dict
        contains user_id structured as follows:

        ```
        {
            "user_id": {
                "mail": login,
                "region": region
            }
        }
        ```

    Raises
    ------
    exceptions.AuthenticationFailed
        when json params are missing
    exceptions.AuthenticationFailed
        when login or password are missing
    exceptions.AuthenticationFailed
        when user is not found
    exceptions.AuthenticationFailed
        when password is incorrect
    """
    if request.json is None:
        raise exceptions.AuthenticationFailed("Il manque le login ou mot de passe.")

    login = request.json.get("login", None)
    password = request.json.get("password", None)

    if not login or not password:
        raise exceptions.AuthenticationFailed("Il manque le login ou mot de passe.")

    if not await controller.is_active_user(region, login):
        raise exceptions.AuthenticationFailed(
            "Utilisatrice / utilisateur non trouvé ou compte inactif."
        )

    existing_password_hash = await controller.get_user_password(region, login)

    if controller.hash_pass(password) != existing_password_hash:
        raise exceptions.AuthenticationFailed("Mot de passe incorrect.")

    # met à jour les données de connexion
    await controller.update_connexion_info(region, login)

    if request.json.get("nationalRegion", False):
        is_valid = await controller.is_user_in_national_region(
            login, request.json["nationalRegion"]
        )
        if not is_valid:
            raise exceptions.AuthenticationFailed("Invalid region.")

    return {"user_id": {"mail": login, "region": region}}


async def extra_payload(payload: dict, *args, **kwargs):
    """
    Add information on current user inside JWT payload.

    Parameters
    ----------
    payload : dict
        contains at least the user_id information (region and mail)

    Returns
    -------
    dict
        new payload updated with user data (prenom, nom, mail, etc.)
    """
    user = await controller.user(
        payload["user_id"]["region"], payload["user_id"]["mail"]
    )
    if user is None:
        return payload
    payload["prenom"] = user["prenom"]
    payload["nom"] = user["nom"]
    payload["mail"] = user["mail"].lower()
    payload["organisation"] = user["organisation"]
    payload["fonction"] = user["fonction"]
    payload["territoire"] = user["territoire"]
    payload["territoire_predilection"] = user["territoire_predilection"]
    payload["utiliser_territoire_predilection"] = user[
        "utiliser_territoire_predilection"
    ]
    payload["code_postal"] = user["code_postal"]
    payload["profil"] = user["profil"]
    payload["publication"] = user["publication"]
    payload["can_validate_poi_contributions"] = user["can_validate_poi_contributions"]
    payload["poi_contributions"] = user["poi_contributions"]
    payload["actif"] = user["actif"]
    payload["est_admin"] = user["profil"] == "admin"
    payload["global_admin"] = user["global_admin"]
    payload["region"] = user["region"]
    payload["acces_indicateurs_desactives"] = user["acces_indicateurs_desactives"]
    return payload


async def retrieve_user(request, payload: dict, *args, **kwargs):
    """
    Retrieve user information from database and return it (used in /auth/me URL)

    Parameters
    ----------
    payload : dict
        must contain region and mail keys with user info

    Returns
    -------
    User
        object with user information
    """
    if not payload:
        return None
    user = await controller.user(payload["region"], payload["mail"].lower())
    payload["user_id"] = {
        "region": payload["user_id"]["region"].lower(),
        "mail": payload["user_id"]["mail"].lower(),
    }
    national_region = False
    if request.args.get("national_region", False):
        region = request.args["national_region"][0]
        is_valid = await controller.is_user_in_national_region(payload["mail"], region)
        if is_valid:
            national_region = region

    if user:
        return User(
            payload["user_id"],
            payload["exp"],
            payload["mail"].lower(),
            payload["profil"],
            payload["publication"],
            payload["est_admin"],
            payload["region"],
            (
                payload["territoire"].lower()
                if payload["territoire"] is not None
                else payload["territoire"]
            ),
            user.get("acces_indicateurs_desactives", False),
            user.get("global_admin", False),
            user.get("prenom", ""),
            user.get("nom", ""),
            user.get("organisation", ""),
            user.get("fonction", ""),
            user.get("territoire_predilection", "{}"),
            user.get("utiliser_territoire_predilection", False),
            user.get("code_postal", ""),
            user.get("can_validate_poi_contributions", False),
            user.get("poi_contributions", []),
            national_region,
        )
    else:
        return User(
            payload["user_id"],
            payload["exp"],
            payload["mail"].lower(),
            payload["profil"],
            payload["publication"],
            payload["est_admin"],
            payload["region"],
            (
                payload["territoire"].lower()
                if payload["territoire"] is not None
                else payload["territoire"]
            ),
            payload.get("acces_indicateurs_desactives", False),
            payload.get("global_admin", False),
            payload.get("prenom", ""),
            payload.get("nom", ""),
            payload.get("organisation", ""),
            payload.get("fonction", ""),
            payload.get("territoire_predilection", "{}"),
            payload.get("utiliser_territoire_predilection", False),
            payload.get("code_postal", ""),
            payload.get("can_validate_poi_contributions", False),
            payload.get("poi_contributions", []),
            national_region,
        )


async def store_refresh_token(user_id, refresh_token, request, *args, **kwargs):
    """
    Store the refresh token for a specific account.

    Function called at each authentication, delete old refresh token,
    if any, and create a new one.

    Parameters
    ----------
    user_id : dict
        must contain region and mail keys with user info
    """
    await controller.delete_refresh_token(user_id["region"], user_id["mail"].lower())
    await controller.store_refresh_token(
        user_id["region"], user_id["mail"].lower(), refresh_token
    )


async def retrieve_refresh_token(request, user_id, *args, **kwargs):
    """
    Retrieve and check the refresh token for a specific account.

    Parameters
    ----------
    user_id : dict
        must contain region and mail keys with user info
    """
    rset = await controller.get_refresh_token(user_id["region"], user_id["mail"])
    # If too old, we remove previous one and return None
    # We thus force new authentication
    if (dt.now() - rset["creation_date"]).seconds > REFRESH_EXPIRATION:
        await controller.delete_refresh_token(user_id["region"], user_id["mail"])
        return None
    return rset["token"]


class Logout(BaseEndpoint):
    """Route /auth/logout"""

    decorators = [protected()]

    async def get(self, request, region):
        """Écrase les valeurs des token 'access' et 'refresh'

        Permet de supprimer les valeurs des cookies d'authentification envoyées au
        navigateur. Supprime aussi le refresh token stocké en base.
        """
        userdata = await auth_user(request, region)
        # on supprime le refresh token de l'utilisateur·rice
        await delete_refresh_token(region, userdata.mail)
        # on met des valeurs vide pour les tokens 'access' et 'refresh'
        resp = response.json({"status": "logout"})
        access_token_key = self.config.cookie_access_token_name()
        refresh_token_key = self.config.cookie_refresh_token_name()
        _set_cookie(resp, access_token_key, "", self.config)
        _set_cookie(resp, refresh_token_key, "", self.config)
        # on met un max-age très petit
        resp.cookies[access_token_key]["max-age"] = 1
        resp.cookies[refresh_token_key]["max-age"] = 1
        return resp


def initialize_sanic_jwt(app: Sanic):
    return Initialize(
        app,
        authenticate=authenticate,
        retrieve_user=retrieve_user,
        extend_payload=extra_payload,
        cookie_set=True,
        cookie_max_age=24 * 60 * 60,  # 24 heures
        refresh_token_enabled=True,
        store_refresh_token=store_refresh_token,
        retrieve_refresh_token=retrieve_refresh_token,
        class_views=[("/logout", Logout)],
        url_prefix="/api/<region>/auth",
        secret=settings.get("api", "jwt_secret_key"),
    )
