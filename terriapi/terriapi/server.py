# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import argparse
from functools import partial
from typing import Optional, Sequence, Tuple

from sanic import Blueprint, Sanic, response
from sanic.log import LOGGING_CONFIG_DEFAULTS, logger
from sanic.utils import str_to_bool
from sanic.worker.loader import AppLoader
from sanic.worker.manager import WorkerManager
from sanic_ext import Extend, openapi

from terriapi import __version__, settings
from terriapi.api import bp_v1
from terriapi.auth import initialize_sanic_jwt
from terriapi.database import bp_db
from terriapi.server_utils import autodiscover
from terriapi.user import bp_user
from terriapi.v2 import routes_v2

if settings["api"]["debug"].lower() == "true":
    WorkerManager.THRESHOLD = 1000

LOGGING_CONFIG = LOGGING_CONFIG_DEFAULTS.copy()
LOGGING_CONFIG["formatters"]["generic"] = {
    "format": "[%(asctime)s.%(msecs)03d] [%(process)d] [%(levelname)s] %(message)s",
    "datefmt": "%Y-%m-%d %H:%M:%S",
    "class": "logging.Formatter",
}

# Register modules here to be loaded by default.
DEFAULT_MODULES: Tuple[str, ...] = (
    "terriapi.database",
    "terriapi.api",
    "terriapi.user",
)


def create_app(
    args=None, cors: bool = False, module_names: Optional[Sequence[str]] = None
) -> Sanic:
    """This is the app factory function used to create the Sanic app and load the different modules.
    This function is called by the AppLoader in the serve() function.

    Parameters
    ----------
    args : Namespace, optional
        Namespace object containing the command line arguments, by default None.
        Is used when app is run from the CLI with the sanic command. However arbitrary arguments passed to the CLI
        are not typed (i.e. they are all strings).
    cors : bool, optional
        If True, enable CORS, by default False
    Returns
    -------
    Sanic
        The Sanic app with all the modules loaded.
    """

    # when run from the sanic CLI, parse the --cors argument as a bool. (use --cors True or --cors False, not --cors, False by default)
    if args is not None and hasattr(args, "cors"):
        cors = str_to_bool(args.cors)

    app = Sanic(settings.get("api", "app_name"), log_config=LOGGING_CONFIG)

    # l'impact emploit peut causer des requêtes plus longues que 60s
    app.config.SERVER_NAME = settings.get("api", "server_name")
    app.config.RESPONSE_TIMEOUT = int(settings.get("api", "response_timeout"))
    app.config.REQUEST_MAX_SIZE = int(settings.get("api", "request_max_size"))
    app.config.REAL_IP_HEADER = "X-Real-IP"

    # TOFIX: To disable when not in local?
    app.config.CORS_ORIGINS = settings.get("api", "cors_origins")
    app.config.CORS_AUTOMATIC_OPTIONS = True
    app.config.CORS_SUPPORTS_CREDENTIALS = True
    # Normally, we should be able to use * here but as we support CORS credentials
    # we need to specify the headers allowed.
    # cf. https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Headers
    # "In requests with credentials, it is treated as the literal header name "*" without special semantics."
    app.config.CORS_ALLOW_HEADERS = "content-type"

    app.config.API_VERSION = "2.0.0"
    app.config.API_TITLE = "TerriAPI"
    app.config.API_DESCRIPTION = "TerriSTORY® backend API"
    app.config.API_CONTACT_EMAIL = "contact@terristory.fr"
    app.config.API_LICENSE_NAME = "Licence aGPL v3"
    app.config.API_LICENSE_URL = (
        "https://gitlab.com/terristory/terristory/-/blob/master/LICENSE.md"
    )
    app.config.API_TERMS_OF_SERVICE = (
        "https://gitlab.com/terristory/terristory/-/blob/master/docs/legal/api_tos.md"
    )
    app.config.API_SCHEMES = settings.get("api", "schemes").split(",")
    app.config.API_HOST = settings.get("api", "host")

    app.config.HTTP_AUTO_OPTIONS = True

    app.config.OAS_UI_DEFAULT = "swagger"
    app.config.SWAGGER_UI_CONFIGURATION = {
        "operationsSorter": "alpha",
        "docExpansion": "list",
    }

    app.blueprint(routes_v2)

    initialize_sanic_jwt(app)
    app.blueprint(bp_db)
    app.blueprint(bp_v1)
    app.blueprint(bp_user)

    openapi.exclude(bp=bp_v1)
    openapi.exclude(bp=bp_user)

    return app


def serve():
    """
    Entry point for running the server from the CLI with the `terriapi-serve` command in setup.py.

    Server parameters are read from the terrapi.ini file and from command line arguments.

    use --cors to enable CORS
    use --reload to enable auto-reload

    See https://sanic.dev/en/guide/running/app-loader.html
    """

    parser = argparse.ArgumentParser(
        description="TerriSTORY API version {}".format(__version__)
    )
    parser.add_argument(
        "--cors", action="store_true", help="make cross-origin AJAX possible"
    )
    parser.add_argument(
        "--reload",
        action="store_true",
        help="mécanisme de rechargement de fichiers en mode dev (i.e. debug)",
    )

    args = parser.parse_args()

    loader = AppLoader(factory=partial(create_app, cors=args.cors))
    app = loader.load()

    debug = (
        settings["api"].get("log", "true") == "true"
        and settings["api"]["debug"].lower() == "true"
    )

    app.prepare(
        host=settings["api"]["listen"],
        port=int(settings["api"]["listen_port"]),
        debug=debug,
        auto_reload=args.reload,
        workers=2,
    )
    Sanic.serve(primary=app, app_loader=loader)
