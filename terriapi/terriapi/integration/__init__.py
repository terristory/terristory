# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import datetime
import json
from pathlib import Path
from subprocess import DEVNULL, PIPE, CalledProcessError, run

import pandas as pd
import slugify
from sanic.log import logger
from sanic.request import File

from terriapi import controller, settings
from terriapi.controller import copy_to_table, get_pg_engine, perimeters
from terriapi.conversion_perimetres.conversion import (
    changement_cog_varnum,
    convert_df_to_perimeter,
)
from terriapi.exceptions import ForceUpdateAllowedException, ValidationError

from .validation import (
    existence_categorie,
    existence_table,
    lecture_headers,
    normalize,
    verifie_existence_tables,
    verifie_headers,
    verifie_table_colonne,
)

here = Path(__file__).parent

apiconf = settings["api"]
TO_SRID = "3857"

# préparation des options de la commande PSQL
# supprime les NOTICE de la stderr
psql_env = {"PGOPTIONS": "--client-min-messages=warning"}

if "pg_password" in apiconf and apiconf.get("pg_password", None):
    psql_env["PGPASSWORD"] = apiconf["pg_password"]

psql_cmd = ["psql"]
# permet de sortir du script à la première erreur
psql_cmd.append("-v ON_ERROR_STOP=1")
psql_cmd.extend(
    [
        "{} {}".format(opt, apiconf[cfg])
        for opt, cfg in (
            ("-h", "pg_host"),
            ("-p", "pg_port"),
            ("-d", "pg_name"),
            ("-U", "pg_user"),
        )
        # l'option doit être présente et non vide
        if cfg in apiconf and apiconf.get(cfg, None)
    ]
)
SHP2PGSQL_CMD = [
    "shp2pgsql",
    "-g geom",
    "-I",  # index gist
]  # l'encodage et le SRID sont rajoutés dans la fonction 'load_shp'


async def load_shp(shp, region, basename, srid="2154", encoding="UTF-8"):
    """Charge un fichier Shapefile dans la base de donnée

    Parameters
    ----------
    shp : pathlib.Path
    region : str
    basename : str
    srid : str
    """
    if not perimeters.command_exists("psql"):
        raise ValueError("psql executable command doesn't exist, please install it.")
    if not perimeters.command_exists("shp2pgsql"):
        raise ValueError(
            "shp2pgsql executable command doesn't exist, please install it."
        )
    if shp != None:
        schema = region.replace("-", "_")
        table = "{}.{}".format(schema, basename.replace("-", "_"))
        try:
            # suppression de la table si elle existe, merci SHP2PGSQL avec sa demi option -d
            drop = ["-c 'drop table if exists {} cascade'".format(table)]
            run(
                " ".join(psql_cmd + drop),
                check=True,
                shell=True,
                stdout=DEVNULL,
                stderr=PIPE,
                encoding="UTF-8",
                env=psql_env,
            )
        except CalledProcessError as exc:
            print(exc.stderr)
            raise exc

        try:
            shp2pgsql_cmd = SHP2PGSQL_CMD[:] + [
                "-s {from_srid}:{to_srid}".format(from_srid=srid, to_srid=TO_SRID)
            ]
            shp2pgsql_cmd.append("-W {encoding}".format(encoding=encoding))
            run(
                " ".join(shp2pgsql_cmd + [str(shp), table, "|"] + psql_cmd),
                check=True,
                shell=True,
                stdout=PIPE,
                stderr=PIPE,
                encoding="UTF-8",
                env=psql_env,
            )
        except CalledProcessError as exc:
            print(exc.stderr)
            raise exc
        print(f"* fichier {shp} chargé")


async def update_data(
    schema, table, fpath, colonnes_obligatoires, is_national=False, force_update=False
):
    """Mise à jour des données d'un indicateur.

    Parameters
    ----------
    schema : str
        Schéma de la région
    table : str
        Nom de la table
    fpath : Path
        Chemin du fichier CSV

    Returns
    -------
    int
        Nombre de lignes mises à jour.
    """

    is_valid = verifie_headers(fpath, colonnes_obligatoires)

    if not is_valid:
        raise ValidationError(is_valid.message)
    is_valid = await verifie_table_colonne(schema, table, fpath, is_national)
    if not force_update and is_valid.can_be_forced and not is_valid:
        raise ForceUpdateAllowedException()
    elif not force_update and not is_valid:
        raise ValidationError(is_valid.message)
    headers = lecture_headers(fpath)
    recreate_table_query = None
    if force_update and not is_valid:
        recreate_table_query = await create_indicator_data_table(
            schema,
            table,
            list(set(headers).difference(colonnes_obligatoires)),
            True,
            False,
            False,
            forced=True,
        )
    return await copy_to_table(
        schema,
        table,
        fpath,
        columns=headers,
        is_national=is_national,
        recreate_table_query=recreate_table_query,
    )


def obtenir_donnees_territoriales(indicateur_decortique):
    """Retourne la table associée aux mailles territoriales qui intervient dans le calcul de l'indicateur passé en paramètre

    Par exemple, population, emploi_tout_secteur, qu'on retrouve dans les calculs d'indicateur sous la forme
    de maille.donnee (remplacer maille par une maille territoriale. Exemple : epci.population).

    Parameters
    -----------
    indicateur_decortique : liste
        Décomposition de la formule d'un indicateur décortiqué au moyen d'une expression régulière
        Exemple : [conso_energetique*1000, /maille., /population])

    Returns
    --------
    str :
        Le jeu de donnée associé à une maille territoriale sollicité pour la mise à jour des territoires s'il y en a un
    bool :
        False sinon.
    """
    for i in range(len(indicateur_decortique)):
        if "maille" in indicateur_decortique[i]:
            return indicateur_decortique[i + 1]
    return False


async def obtenir_donnees_obligatoires_pour_maille_territoriales(region):
    """Retourne la liste des données associées aux mailles territoriales trouvées dans la colonne data de meta.indicateur

    Cette liste de données doit être présente en base de données sous forme de tables spécifiques à ajouter dans
    le schéma régional. Cette liste comprend également les tables qui interviennent directement dans le calcul des
    indicateurs et qui ne font pas l'objet d'une dans les tables territoriales de type schema.territoire (occitanie.pnr par exemple)
    mais qui interviennent dans la partie chiffres clés.

    Parameters
    -----------
    region : str
        Nom de la région sans accent si majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)

    Returns
    --------
    Liste :
        Liste des données qu'il faut mettre à jour au lancement de la mise à jour des territoires
    """
    liste_donnees_pour_chiffres_cles = await liste_donnees_chiffres_cles(
        region
    )  # Obtention des données qui interviennent dans la partie chiffres clés
    req = """
          with final as (
              with tables_territoires as (
                  select distinct (regexp_split_to_array(data, E'[./,::*()]')) as nom_table
                  from meta.indicateur
                  where region = $1
              )
              select distinct t.nom_table[array_position(nom_table, 'maille') + 1] as nom_table
              from tables_territoires t
              where nom_table[array_position(nom_table, 'maille') + 1] is not null
          )
          select f.nom_table as nom, p.date_perimetre
          from final f
          join meta.perimetre_geographique p on p.nom_table = f.nom_table and p.region = $1
          order by f.nom_table
          """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "Récupération des données nécessaires à la construction des tables associées aux mailles territoriales"
            )
            rset = await conn.fetch(req, region)
            colonnes = [dict(x) for x in rset if x["nom"]]
            for (
                elem
            ) in (
                liste_donnees_pour_chiffres_cles
            ):  # On parcourt la liste des données qui interviennent dans la partie chiffres clés
                if (
                    elem not in colonnes
                ):  # Certains noms de table ne s'y trouvent pas encore car il s'agit de données non associées aux mailles territoriales
                    colonnes.append(
                        elem
                    )  # mais dont on a besoin car elles interviennent dans la partie chiffres clés
            return colonnes  # On retourne une liste dépourvue des doublons (population par exemple est impliqué dans le calcul de plusieurs indicateurs)


def verifier_integration_donnees_obligatoires_pour_maille_territoriales(
    liste_donnees_presentes, liste_donnees_pour_indicateur, donnees_obligatoires
):
    """Vérifie la présence - ou l'absence - des données indispensables à la mise à jour des territoires

    Parameters
    ----------
    liste_donnees_presentes : liste
        Liste des tables déjà présentes en base de données
    liste_donnees_pour_indicateur : liste
        Liste des données qui interviennent directement dans le calcul d'indicateurs et non associées aux mailles territoriales
    donnees_obligatoires
        Liste des tables dont la présence est nécessaire au lancement de la mise à jour des territoires
    """
    donnees_finales = [
        {
            "nom": x["nom"],
            "date_perimetre": x["date_perimetre"],
            "presente": (x["nom"] in liste_donnees_presentes),
            "indicateur": (
                x["nom"] in [x["nom"] for x in liste_donnees_pour_indicateur]
            ),
        }
        for x in donnees_obligatoires
    ]
    return donnees_finales


async def liste_donnees_chiffres_cles(region):
    """Donne la liste des tables / données qui interviennent dans la partie chiffres clés

    Parameters
    ------------
    region : str
        Nom de la région sans accent si majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)

    Returns
    --------
    Liste
        Liste des données qui interviennent dans la partie chiffres clés.

    """
    schema = region.replace("-", "_")
    req = f"""
          with tables_chiffres_cle as (
          select distinct trim((regexp_split_to_array(donnee, E'[\*+/\]'))[1]) as nom_table
          from {schema}.chiffres_cle
          )
          select c.nom_table as nom, p.date_perimetre
          from tables_chiffres_cle c
          join meta.perimetre_geographique p on p.nom_table = c.nom_table
          where p.region = $1 order by c.nom_table;
          """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "Récupération des données nécessaires à la construction des tables associées aux mailles territoriales"
            )
            res = await conn.fetch(req, region)
            liste_donnees_pour_chiffres_cles = [dict(x) for x in res]
            return liste_donnees_pour_chiffres_cles


async def obtenir_liste_tables_pour_indicateur(region):
    """Retourne la liste des tables directement impliquées dans le calcul des indicateurs.

    Parameters
    ------------
    region : str
        Nom de la région sans accent si majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)

    Returns
    --------
    Liste
        Liste des tables qui interviennent directement dans les calculs des indicateurs.
    """
    req = """
          with tmp_final as (
              with liste_tables_indicateur as (
                  select distinct (regexp_split_to_array(data, E'[\*+/\]'))[1] as nom_table, years
                  from meta.indicateur where region = $1
                  and concatenation is null
              )
              select distinct t.nom_table, max(t.years) as years, a.date_perimetre
              from liste_tables_indicateur t
              join meta.perimetre_geographique a on a.nom_table = t.nom_table
              where t.years is not null
              group by t.nom_table, a.date_perimetre
              union all
              select distinct t.nom_table, max(t.years) as years, a.date_perimetre
              from liste_tables_indicateur t
              join meta.perimetre_geographique a on a.nom_table = t.nom_table
              where t.years is null group by t.nom_table, a.date_perimetre
          )
          select distinct tmp_final.nom_table, tmp_final.years, max(tmp_final.date_perimetre) as date_perimetre
          from tmp_final
          group by tmp_final.nom_table, tmp_final.years;
          """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "Récupération des données nécessaires à la construction des tables associées aux mailles territoriales"
            )
            res = await conn.fetch(req, region)
            liste_tables = [
                {
                    "nom": x["nom_table"].strip(),
                    "annee": max(x["years"]),
                    "date_perimetre": x["date_perimetre"],
                }
                for x in res
                if x["years"] and len(x["years"]) >= 1
            ]
            return liste_tables


async def convertir_dans_nouveau_perimetre(
    schema, liste_donnees_a_convertir, annee_nouveau_perimetre
):
    """Converti les données transmises en argument dans un nouveau périmètre géographique

    Parameters
    ----------

    schema : str
        schéma régional concerné par la conversion
    liste_donnees_a_convertir : liste
        Liste des tables à convertir dans le nouveau périmètre
    annee_nouveau_perimetre : entier
        Millésime dans lequel convertir le nouveau périmètre
    """
    engine = get_pg_engine()
    for table in liste_donnees_a_convertir:
        async with controller.db.acquire() as conn:
            async with conn.transaction():
                logger.info(
                    "Initialisation de la conversion de la table {schema}.{table}".format(
                        table=table["nom"], schema=schema
                    )
                )
                req = """
                      select *
                      from {schema}.{table};
                      """.format(
                    schema=schema + "_ancien_perimetre", table=table["nom"]
                )
                req_mise_a_jour_perimetre = """
                                            update meta.perimetre_geographique
                                            set date_perimetre = $1
                                            where nom_table = $2
                                            and region = $3
                                            """
                donnee = await conn.fetch(req)
                donnees = [dict(x) for x in donnee]
                perimetre_initial = table["date_perimetre"]
                if perimetre_initial == annee_nouveau_perimetre:
                    logger.info("Skipping table as already in latest perimeter.")
                    continue
                df = pd.DataFrame(donnees)
                if "annee" in df.keys():
                    df["an"] = df["annee"]
                    df = df.drop(["annee"], axis=1)
                df_converti = df
                if "commune" in list(df.keys()):
                    try:
                        df_converti = await changement_cog_varnum(
                            df,
                            range(table["date_perimetre"], annee_nouveau_perimetre + 1),
                            "commune",
                        )
                    except TypeError as type_error_message:
                        raise ValidationError(
                            f"Erreur dans la conversion dans l'année {annee_nouveau_perimetre} des champs de la table {schema}.{table['nom']}: {type_error_message}"
                        )
                    except AssertionError as assertion_message:
                        raise ValidationError(
                            f"Erreur dans la conversion dans l'année {annee_nouveau_perimetre} des champs de la table {schema}.{table['nom']}: {assertion_message}"
                        )
                colonnes_df_converti = [
                    x for x in df.keys() if x != "annee" and x != "valeur"
                ]
                if "annee" in df_converti.keys():
                    df_converti["annee"] = df_converti["annee"].astype(int)
                    df_converti = df_converti.drop("an", axis=1)
                if "an" in df_converti.keys():
                    df_converti["an"] = df_converti["an"].astype(int)
                    df_converti["annee"] = df_converti["an"]
                    df_converti = df_converti.drop("an", axis=1)

                logger.info(
                    "Conversion de la table {schema}.{table} en cours...".format(
                        table=table["nom"], schema=schema
                    )
                )
                if len(list(df_converti.keys())) != 0:
                    df_converti.to_sql(
                        "{table}".format(table=table["nom"]),
                        con=engine,
                        index=False,
                        schema="{schema}".format(schema=schema),
                        if_exists="replace",
                    )

                await conn.fetch(
                    req_mise_a_jour_perimetre,
                    annee_nouveau_perimetre,
                    table["nom"],
                    schema.replace("_", "-"),
                )
                logger.info(
                    "table {schema}.{table} convertie".format(
                        table=table["nom"], schema=schema
                    )
                )


async def migration_donnees_dans_nouveau_schema(schema, liste_tables_donnees):
    """Migre les tables de données dans un schéma temporaire à part (<schema>_ancien_perimetre) pour la conversion des périmètres.
    Attention, Les tables originales déplacée dans ce schema temporaire sont écrasées à chaque nouvelle migration.

    Parameters
    ----------
    schema : str
        Schéma régional concerné par la migration
    liste_tables_donnees : Liste de chaînes de caractères
        Liste des noms des tables à copier dans le schéma à part
    """
    schema_ancien_perimetre = schema + "_ancien_perimetre"
    req_suppression = "drop schema if exists " + schema_ancien_perimetre + " cascade;"
    req_creation = "create schema " + schema_ancien_perimetre + ";"
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "Suppression du schéma contenant les données exprimées dans l'ancien périmètre géographique"
            )
            await conn.fetch(req_suppression)
            logger.info("Création du schéma " + schema_ancien_perimetre)
            await conn.fetch(req_creation)
    for table in liste_tables_donnees:
        req = """
              create table {schema_ancien_perimetre}.{table}
              as
              select *
              from {schema}.{table}
              """.format(
            schema_ancien_perimetre=schema_ancien_perimetre, schema=schema, table=table
        )
        async with controller.db.acquire() as conn:
            logger.info(
                "Migration de la table "
                + table
                + " dans le schéma où sont contenues les données exprimées dans l'ancien périmètre"
            )
            await conn.fetch(req)


async def liste_tables_donnees(schema):
    """Retourne la liste des tables présentes au sein d'un schéma régional

    Parameters
    -----------
    schema : str
        Nom du schéma régional
    """
    req = """
          select table_name as nom
          from information_schema.tables
          where table_schema = $1;
          """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "Récupération des noms des tables existantes au sein d'un schéma régional"
            )
            res = await conn.fetch(req, schema)
            liste_tables_donnees = [x["nom"] for x in res]
            return liste_tables_donnees


async def creer_tables_fr(schema, year):
    """Crée la table des communes, epci, départements de France nécessaires au fonctionnement des migrations pendulaires et y insère les données

    Parameters
    -----------
    schema : str
        Nom du schéma régional
    """
    liste_tables_a_creer = ["epci_fr", "departement_fr"]

    req_suppression = f"""
                      drop table if exists {schema}.commune_fr cascade;
                      """

    req_creation = f"""
              create table {schema}.commune_fr
              (
                  nom varchar
                  , code varchar not null
                  , statut varchar
                  , x float
                  , y float
              );
              """

    req_insertion_commune = f"""
                            insert into {schema}.commune_fr
                            with tmp as (
                            select
                                distinct nom_com as nom
                                , insee_com as code
                                , statut
                                , st_pointonsurface(c.geom)::geometry(point, 3857) as center
                            from france.communes_{year} c
                            )
                            select
                                distinct nom
                                , code
                                , statut
                                , st_x(center) as x
                                , st_y(center) as y
                            from tmp
                            union all
                            select distinct nom, code, statut, x, y
                            from territoires_etrangers.communes;
                            """

    req_creation_index_com_tmp_gist = f"""
                              create index on france.communes_{year} using gist (geom);
                              """
    req_creation_index_gin = f"""
                             create unique index on {schema}.commune_fr (code);
                             """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info("Création de la table des communes de France")
            await conn.fetch(req_suppression)
            await conn.fetch(req_creation)
            await conn.fetch(req_insertion_commune)
            await conn.fetch(req_creation_index_gin)
            await conn.fetch(req_creation_index_com_tmp_gist)

    nom_colonne_code_territoire = "code_epci"
    colonne_nom_territoire = "nom_epci"
    for territoire in liste_tables_a_creer:
        if territoire == "departement_fr":
            nom_colonne_code_territoire = "insee_dep"
            colonne_nom_territoire = "nom_dep"
        req_suppression = f"""
                          drop table if exists {schema}.{territoire};
                          """

        req_creation_table = f"""
                             create table {schema}.{territoire} (
                             code varchar
                             , nom varchar
                             , commune varchar
                             , x float
                             , y float
                             );
                             """

        req_creation_mat_view_unioned = f"""
                             create materialized view IF NOT EXISTS {schema}.{territoire}_geom_union as 
                             select
                                c.{nom_colonne_code_territoire} as code
                                , coalesce(c.{colonne_nom_territoire}, c.{nom_colonne_code_territoire}) as nom
                                , st_multi(st_union(c.geom)) as geom
                            from france.communes_{year} c
                            group by c.{nom_colonne_code_territoire}, nom;
                             """

        req_creation_point_on_surface = f"""
                             create materialized view IF NOT EXISTS {schema}.{territoire}_geom_union_point as 
                             with data_point as (
                                SELECT code, 
                                ST_pointonsurface(geom) as point_on_surface
                                FROM {schema}.{territoire}_geom_union
                            )
                            SELECT
                                code,
                                point_on_surface,
                                ST_x(point_on_surface) as x,
                                ST_y(point_on_surface) as y
                            FROM data_point;
                             """

        req_indexes = [
            f"CREATE INDEX ON {schema}.{territoire}_geom_union (code);",
            f"CREATE INDEX ON {schema}.{territoire}_geom_union_point (code);",
            f"CREATE INDEX ON france.communes_{year} ({nom_colonne_code_territoire});",
        ]
        req_insertion_donnees = f"""
                                insert into {schema}.{territoire} (code, nom, commune, x, y)
                                select
                                    distinct tmp_fr.code
                                    , nom
                                    , ctf.insee_com as commune
                                    , p.x
                                    , p.y
                                from {schema}.{territoire}_geom_union tmp_fr
                                join {schema}.{territoire}_geom_union_point p on p.code = tmp_fr.code
                                join france.communes_{year} ctf on ctf.{nom_colonne_code_territoire} = tmp_fr.code
                                union all
                                select distinct p.code, p.nom, c.code as commune, p.x, p.y
                                from territoires_etrangers.communes c
                                join territoires_etrangers.pays p on p.code = substring(c.code from 0 for 3)
                                group by p.code, c.code, p.nom, p.x, p.y;
                                """

        async with controller.db.acquire() as conn:
            async with conn.transaction():
                logger.info(
                    "Création de la table des {territoire} de France".format(
                        territoire=territoire
                    )
                )
                await conn.fetch(req_suppression)
                await conn.fetch(req_creation_table)
                await conn.fetch(req_creation_mat_view_unioned)
                await conn.fetch(req_creation_point_on_surface)
                for req_index in req_indexes:
                    await conn.fetch(req_index)
                await conn.fetch(req_insertion_donnees)
                await conn.fetch(req_creation_index_gin)
        logger.info(
            "Fin de la création de la table des {territoire} de France".format(
                territoire=territoire
            )
        )


async def creer_table_territoire(schema, liste_donnees_obligatoires, type_territoire):
    """Crée la table territoriale associée au type de territoire passé en paramètre (exemple : occitanie.epci)

    Cette fonction ne permet pas de créer la table commune pour le schéma passé en argument. Ajouter cette dernière
    nécessite d'exécuter une requête différente à bien des égards et fait donc l'objet d'une fonction à part (req_creation_table_commune)

    Parameters
    ----------
    schema : str
        Nom du schéma associé à la région sélectionnée
    liste_donnees_obligatoires : liste
        Liste des tables dont la présence est nécessaire à la mise à jour des territoires
    type_territoire : str
        Type de territoire
    """
    liste_colonnes = ""
    if liste_donnees_obligatoires:
        liste_colonnes = ", " + " float, ".join(liste_donnees_obligatoires) + " float"
    colonne_communes = ""
    if type_territoire != "commune":
        colonne_communes = ", communes varchar[]"
    supprimer_table_communes = """
                               drop table if exists {schema}.{type_territoire} cascade;
                               """.format(
        schema=schema, type_territoire=type_territoire
    )
    req = """
          create table {schema}.{type_territoire} (
              nom varchar
              , code varchar primary key
              {colonne_communes}
              {liste_colonnes}
              , x float
              , y float
              , geom geometry(multipolygon, 3857)
              , siren varchar
          );
          """.format(
        liste_colonnes=liste_colonnes,
        schema=schema,
        type_territoire=type_territoire,
        colonne_communes=colonne_communes,
    )

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "Création de la table des {type_territoire}s située dans le schéma régional".format(
                    type_territoire=type_territoire
                )
            )
            await conn.execute(supprimer_table_communes)
            await conn.execute(req)
            await conn.execute(
                f"""CREATE INDEX IF NOT EXISTS {schema}_{type_territoire}_geom_idx ON {schema}.{type_territoire} USING GIST(geom);"""
            )
            await conn.execute(
                f"""CREATE INDEX IF NOT EXISTS {schema}_{type_territoire}_code_idx ON {schema}.{type_territoire} (code)""",
            )
            if type_territoire != "commune":
                await conn.execute(
                    f"""CREATE INDEX IF NOT EXISTS {schema}_{type_territoire}_communes_idx ON {schema}.{type_territoire} USING GIN(communes)""",
                )


async def req_creation_table_commune(
    schema,
    perimeter_year,
    liste_selection,
    liste_jointures,
    liste_conditions_annee,
    donnees_obligatoires,
    liste_group_by,
    region_code,
):
    """Crée la table des communes pour un schéma passé en paramètre (exemple : nouvelle_aquitaine.commune)

    Parameters
    ----------
    schema : str
        Nom du schéma associé à la région sélectionnée
    liste_selection : liste
        Liste de chaine de caractères qui contient la liste des objets à sélectionner dans les différentes tables (ex : sum(population.valeur) as population)
    Liste jointure : liste
        Liste de bouts de la requête sql qui concernent cette fois les jointures : left join schema.table on etc.
    Liste_conditions_annee : liste
        Liste des conditions liées au année pour les données qui interviennent directement dans les calculs d'indicateurs (ex : where annee = 2016)
    donnees_obligatoires : liste
        Liste des tables dont la présence est nécessaire à la mise à jour des territoires
    """
    await creer_table_territoire(schema, donnees_obligatoires, "commune")
    jointures = ""
    if liste_jointures:
        jointures = "left join " + "left join ".join(liste_jointures)
    selection = ""
    if liste_selection:
        selection = ", " + ", ".join(liste_selection)
    group_by = ""
    if liste_group_by:
        group_by = "," + ", ".join(liste_group_by)
    liste_selection_pour_integration = ""
    if donnees_obligatoires:
        liste_selection_pour_integration = ", " + "\n , ".join(donnees_obligatoires)
    req = f"""insert into {schema}.commune
             with tmp as (
                     select
                     tmp_communes.insee_com as code
                     , tmp_communes.nom_com as nom
                     {selection}
                     , rank() over (partition by tmp_communes.insee_com order by st_area(dump.geom) desc) as rank
                     , st_pointonsurface(dump.geom)::geometry(point, 3857) as center
                     , tmp_communes.geom
                     from france.communes_{perimeter_year} as tmp_communes
                     {jointures}
                     , st_dump(geom) as dump
                     where insee_reg = $1
                     group by dump.geom, tmp_communes.geom, tmp_communes.insee_com, tmp_communes.nom_com, tmp_communes.insee_com {group_by}
            )
            select
                nom
                , code
                {liste_selection_pour_integration}
                , st_x(center) as x
                , st_y(center) as y
                , geom
            from tmp
            where rank = 1;
             """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info("Insersion des données dans la table des communes")
            await conn.execute(req, region_code)


async def obtenir_liste_type_autres_territoires(schema):
    """Retourne la liste des types de territoires autre que "commune" disponibles pour une région

    Parameters
    -----------
    schema : str
        Nom du schéma de la région

    Returns
    --------
    Liste
        Liste des type de territoires autres que "commune" disponibles pour la région sélectionnée (ex : [region, pnr, epci, departement, petr])
    """
    req = f"""
          select distinct type_territoire
          from {schema}.territoire;
          """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            res = await conn.fetch(req)
    return [x["type_territoire"] for x in res if x["type_territoire"] != "commune"]


async def integration_donnees_table_territoire(
    schema, liste_selection, donnees_obligatoires, type_territoire
):
    """Crée la table des communes pour un schéma passé en paramètre
    (exemple : nouvelle_aquitaine.commune)

    Parameters
    ----------
    schema : str
        Nom du schéma associé à la région sélectionnée
    liste_selection : liste
        Liste de chaine de caractères qui contient la liste des objets à sélectionner
        dans les différentes tables (ex : sum(population.valeur) as population)
    donnees_obligatoires : liste
        Liste des tables dont la présence est nécessaire à la mise à jour des territoires
    type_territoire : str
        Type de territoire (exemples : epci, teposcv, pcaet)
    """
    await creer_table_territoire(schema, donnees_obligatoires, type_territoire)
    selection = ""
    if liste_selection:
        selection = ", " + ", ".join(liste_selection)
    colonnes = ""
    if donnees_obligatoires:
        colonnes = ", " + ", ".join(donnees_obligatoires)
    req = """
          insert into {schema}.{type_territoire}
          with tmp as (
                select
                t.nom
                , array_agg(c.code) as communes
                , t.code
                {selection}
                , st_multi(st_union(c.geom)) as geom
                from {schema}.territoire t
                join {schema}.commune c on c.code = t.commune
                where t.type_territoire = $1
                group by t.nom, t.code
             ), filter as (
                    select
                        nom
                        , code
                        , communes
                        {colonnes}
                        , tmp.geom
                        , rank() over (partition by code order by st_area(dump.geom) desc) as rank
                        , st_pointonsurface(dump.geom)::geometry(point, 3857) as center
                    from tmp,
                    st_dump(geom) as dump
                )
                select
                    nom
                    , code
                    , communes
                    {colonnes}
                    , st_x(center) as x
                    , st_y(center) as y
                    , geom
                from filter
                where rank = 1;
          """.format(
        schema=schema,
        selection=selection,
        type_territoire=type_territoire,
        colonnes=colonnes,
    )

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "Insertion des données dans la table des {type_territoire}".format(
                    type_territoire=type_territoire
                )
            )
            await conn.execute(req, type_territoire)


async def reconstitution_vue_effectifs_clap(liste_types_territoires, schema):
    debut_req = """
                create materialized view {schema}.nb_emploi_territoire_view
                as
                select
                    'commune' as territoire
                    , c.commune as territoire_id
                    , n.a88
                    , sum(c.effectif) as nb_emploi
                from {schema}.clap c
                join strategie_territoire.naf n on n.id = c.naf
                group by c.commune, n.a88
                union all
                """.format(
        schema=schema
    )
    l_req = []
    for type_territoire in liste_types_territoires:
        req_terr = """
                    select
                        '{type_territoire}' as territoire
                        , t.code as territoire_id
                        , n.a88
                        , sum(c.effectif) as nb_emploi
                    from {schema}.clap c
                    join {schema}.commune co on co.code = c.commune
                    join strategie_territoire.naf n on n.id = c.naf
                    join {schema}.{type_territoire} t on t.communes && ARRAY[co.code]
                    group by t.code, n.a88
                   """.format(
            schema=schema, type_territoire=type_territoire
        )
        l_req.append(req_terr)

    req_creation_vue = debut_req + " union all ".join(l_req) + ";"
    req_suppr_vue = (
        "drop materialized view if exists {schema}.nb_emploi_territoire_view ;".format(
            schema=schema
        )
    )
    req_creation_index = """
                         create index on {schema}.nb_emploi_territoire_view(territoire);
                         """.format(
        schema=schema
    )

    req_suppr_fonction = (
        "drop function if exists {schema}.part_territoire(varchar, varchar);".format(
            schema=schema
        )
    )

    req_creation_fonction = """
                            create function {schema}.part_territoire(
                                territoire varchar, territoire_id varchar,
                                out a88 int, out part_terr numeric)
                            returns setof record
                            as
                            $$
                                with tmp as (
                                    select
                                        sum(nbter.nb_emploi) nbter
                                        , n.a88::int
                                        , a.emploi_interieur_salarie_nb_pers as eisnp
                                        , a.emploi_interieur_total_nb_pers * 1000 as eitnb
                                        , (a.emploi_interieur_salarie_nb_pers / a.emploi_interieur_total_nb_pers) as pse
                                    from {schema}.nb_emploi_territoire_view nbter
                                    join strategie_territoire.naf n on n.a88 = nbter.a88
                                    join strategie_territoire.a88 a on a.naf = n.id
                                    where nbter.territoire = $1 and nbter.territoire_id = $2
                                    group by n.a88, a.emploi_interieur_salarie_nb_pers, a.emploi_interieur_total_nb_pers
                                ), sums as (
                                    select
                                        sum(nbter / pse) as nbterpse
                                        , (select sum(emploi_interieur_total_nb_pers) * 1000 from strategie_territoire.a88) as seitnb
                                    from tmp
                                )
                                select
                                    t.a88
                                    , least(round(
                                        ((t.nbter / t.pse) / s.nbterpse / (t.eitnb / s.seitnb))
                                        * power(log(2.0, (1+s.nbterpse/s.seitnb)::numeric), 0.3) -- pourcentage
                                     * 100, 6), 100)
                                from tmp t, sums s
                                order by 1
                            $$ language sql;
                            """.format(
        schema=schema
    )

    l_req = [
        req_suppr_vue,
        req_creation_vue,
        req_creation_index,
        req_suppr_fonction,
        req_creation_fonction,
    ]
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info("Création vue pour les impacts emploi")
            for req in l_req:
                await conn.fetch(req)


async def set_perimeter_conversion_state(schema, state):
    """Sets the state of the perimeter conversion to True or False in {schema}.etat_conversion_perimetre.conversion_reussie

    If conversion_reussie is False a perimeter conversion is ongoing : This could mean that a conversion of
    one or all the tables is currently running or that this conversion was unexpectedly interrupted.

    If conversion_reussie is True, the perimeter


    Si la dernière mise à jour a été interrompue, il faut surtout ne pas relancer la migration des données
    dont la première étape consiste à supprimer le schéma contenant les tables exprimées dans l'ancien
    périmètre. Nous relançon donc la mise à jour à partir des tables actuellement disponibles
    dans le schéma où sont organisées les tables de données exprimées dans l'ancien périmètre.

    Parameters
    ----------
    schema : str
        Schéma régional
    etat : booléen
        État de la conversion : True si la dernière mise à jour a été un succès False sinon.
    """
    etat_req = "false"
    if state:
        etat_req = "true"
    req = "update {schema}.etat_conversion_perimetre set conversion_reussie = {etat_req}".format(
        schema=schema, etat_req=etat_req
    )
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.fetch(req)


async def rebuild_all_geographic_perimeters(region, region_code, perimeter_year):
    """Lance la mise à jour des territoires

    region : str
        Nom de la région sans accent si majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)
    """
    # Nécessaire au lancement des fonctions req_creation_table_commune et integration_donnees_table_territoire
    liste_selection = []
    # Nécessaire au lancement de la fonction req_creation_table_commune
    liste_jointures = []
    # Nécessaire au lancement de la fonction req_creation_table_commune
    liste_conditions_annee = []
    liste_selection_territoire = []
    liste_group_by = []
    schema = region.replace("-", "_")
    # Liste des tables qui interviennent directement dans le calcul des indicateurs
    liste_tables_indicateur = await obtenir_liste_tables_pour_indicateur(region)
    # Liste des tables qui interviennent dans le calcul
    # des indicateurs en tant que ratio ou dans les chiffres clés
    donnees_obligatoires_dict = (
        await obtenir_donnees_obligatoires_pour_maille_territoriales(region)
    )
    toutes_donnees_obligatoires_dict = liste_tables_indicateur
    for table in donnees_obligatoires_dict:
        if table["nom"] not in [x["nom"] for x in toutes_donnees_obligatoires_dict]:
            toutes_donnees_obligatoires_dict.append(table)
    donnees_obligatoires = [x["nom"] for x in donnees_obligatoires_dict]
    toutes_donnees_obligatoires = donnees_obligatoires + [
        "territoire",
    ]
    liste_tables_donnees_sans_doublon = list(
        set([x["nom"] for x in liste_tables_indicateur] + donnees_obligatoires)
    )
    # On vérifie que toutes les tables dont la présence est nécessaire à la mise à jour
    # des territoires sont bien présentes en base de données.
    logger.info("Vérifie l'existence des tables ici.")
    est_valide = verifie_existence_tables(
        liste_tables_donnees_sans_doublon
        + [
            "territoire",
        ],
        toutes_donnees_obligatoires,
    )
    if not est_valide:
        raise ValidationError(est_valide.message)

    # On a d'abord besoin de disposer de la liste des conditions sur les années. En effet, certaines tables qui
    # interviennent directement dans le calcul des indicateurs doivent faire l'objet d'une valeur une colonne
    # spécifique des tables territoriales (par exemple occitanie.petr ou encore nouvelle_aquitaine.pcaet) et
    # pour cela, nous avons besoin de n'obtenir les données que sur une année bien spécifique.
    for (
        d
    ) in (
        liste_tables_indicateur
    ):  # On parcourt la liste des tables qui interviennent directement dans le calcul des indicateurs
        if (
            d["nom"] in donnees_obligatoires
        ):  # S'il elle fait partie des données nécessaires à la mise à jour des territoires,
            if "annee" in d.keys():
                liste_conditions_annee.append(
                    d["nom"] + ".annee = " + str(d["annee"])
                )  # Il faut ajouter une condition sur les années dans la liste dédiée

    for donnee in donnees_obligatoires:
        if donnee in [x["nom"] for x in liste_tables_indicateur]:
            l_annees = [
                x["annee"]
                for x in liste_tables_indicateur
                if x["nom"] == donnee and "annee" in x.keys()
            ]
            liste_selection.append(
                "sum({donnee}.valeur) as {donnee}".format(donnee=donnee)
            )
            if l_annees:
                liste_jointures.append(
                    "(select sum(valeur) as valeur, commune from {schema}.{donnee} where annee = {annee} group by commune) as {donnee} on {donnee}.commune = tmp_communes.insee_com".format(
                        donnee=donnee, schema=schema, annee=l_annees[0]
                    )
                    + "\n"
                )
            else:
                liste_jointures.append(
                    "(select sum(valeur) as valeur, commune from {schema}.{donnee} group by commune) as {donnee} on {donnee}.commune = tmp_communes.insee_com".format(
                        donnee=donnee, schema=schema
                    )
                    + "\n"
                )
        else:
            liste_selection.append("{donnee}.valeur as {donnee}".format(donnee=donnee))
            liste_group_by.append(donnee)
            liste_jointures.append(
                "(select sum(valeur) as valeur, commune from {schema}.{donnee} group by commune) as {donnee} on {donnee}.commune = tmp_communes.insee_com".format(
                    donnee=donnee, schema=schema
                )
                + "\n"
            )
        liste_selection_territoire.append(
            "sum(" + donnee + ") as {donnee}".format(donnee=donnee)
        )

    # Création de la table des communes. Par exemple : auvergne_rhone_alpes.commune pour la région Auvergne-Rhône-Alpes
    await req_creation_table_commune(
        schema,
        perimeter_year,
        liste_selection,
        liste_jointures,
        liste_conditions_annee,
        donnees_obligatoires,
        liste_group_by,
        region_code,
    )
    # On crée ensuite les tables des territoires autres que les communes : par exemple nouvelle_aquitaine.region, nouvelle_aquitaine.epci pour la région Nouvelle-Aquitaine
    liste_type_autres_territoires = await obtenir_liste_type_autres_territoires(schema)
    for type_territoire in liste_type_autres_territoires:
        await integration_donnees_table_territoire(schema, [], [], type_territoire)
    await creer_tables_fr(schema, int(perimeter_year))
    await set_perimeter_conversion_state(schema, True)

    # insert information about perimeters available
    sql = "DELETE FROM meta.perimeter_regional WHERE region = $1"
    await controller.execute(sql, region)
    sql = "INSERT INTO meta.perimeter_regional (year, region) VALUES($1, $2)"
    await controller.execute(sql, int(perimeter_year), region)


async def get_current_perimeter_year(region):
    current_perimeter = await controller.fetch(
        "SELECT year FROM meta.perimeter_regional WHERE region = $1", region
    )
    if len(current_perimeter) == 0:
        raise ValueError("No perimeter defined yet!")
    return int(current_perimeter[0]["year"])


async def update_all_data_to_current_perimeter(region, perimeter_year):
    schema = region.replace("-", "_")
    # Liste des tables qui interviennent directement dans le calcul des indicateurs
    liste_tables_indicateur = await obtenir_liste_tables_pour_indicateur(region)
    # Liste des tables qui interviennent dans le calcul
    # des indicateurs en tant que ratio ou dans les chiffres clés
    donnees_obligatoires_dict = (
        await obtenir_donnees_obligatoires_pour_maille_territoriales(region)
    )
    toutes_donnees_obligatoires_dict = liste_tables_indicateur
    for table in donnees_obligatoires_dict:
        if table["nom"] not in [x["nom"] for x in toutes_donnees_obligatoires_dict]:
            toutes_donnees_obligatoires_dict.append(table)
    donnees_obligatoires = [x["nom"] for x in donnees_obligatoires_dict]
    toutes_donnees_obligatoires = donnees_obligatoires + ["territoire"]
    liste_tables_donnees_sans_doublon = list(
        set([x["nom"] for x in liste_tables_indicateur] + donnees_obligatoires)
    )
    # On vérifie que toutes les tables dont la présence est nécessaire à la mise à jour
    # des territoires sont bien présentes en base de données.
    logger.info("Vérifie l'existence des tables ici.")
    est_valide = verifie_existence_tables(
        liste_tables_donnees_sans_doublon + ["territoire"],
        toutes_donnees_obligatoires,
    )
    if not est_valide:
        raise ValidationError(est_valide.message)

    logger.info("Récupère l'état de conversion du périmètre")
    req_etat_conversion = "select * from {schema}.etat_conversion_perimetre;".format(
        schema=schema
    )
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            etat_conversion_perimetre = await conn.fetch(req_etat_conversion)

    logger.info("Met la conversion réussie")
    etat_conversion_perimetre = [
        x["conversion_reussie"] for x in etat_conversion_perimetre
    ][0]
    if etat_conversion_perimetre:
        # Migration des données dans le schéma où sont contenues les données exprimées dans l'ancien périmètre
        await migration_donnees_dans_nouveau_schema(
            schema, liste_tables_donnees_sans_doublon
        )
    # Tant que la conversion n'et pas terminée, l'état est initialisé à False.
    # Ainsi, si le processus est interrompu, on démarre directement avec la conversion sans passer par la migration des tables.
    # Conversion de toutes les tables dans le nouveau périmètre et intégration dans le schéma régional
    await set_perimeter_conversion_state(schema, False)
    await convertir_dans_nouveau_perimetre(
        schema, toutes_donnees_obligatoires_dict, perimeter_year
    )  # Conversion de toutes les tables dans le nouveau périmètre et intégration dans le schéma régional
    await set_perimeter_conversion_state(schema, True)


async def rebuild_supra_perimeters(region):
    schema = region.replace("-", "_")

    # On crée ensuite les tables des territoires autres que les communes : par exemple nouvelle_aquitaine.region, nouvelle_aquitaine.epci pour la région Nouvelle-Aquitaine
    liste_type_autres_territoires = await obtenir_liste_type_autres_territoires(schema)
    for type_territoire in liste_type_autres_territoires:
        await integration_donnees_table_territoire(schema, [], [], type_territoire)
    await reconstitution_vue_effectifs_clap(liste_type_autres_territoires, schema)


async def update_metadata_indicator(
    region, schema, indicateur_id, metadata, data_custom
):
    """Mise à jour des metadonnées d'un indicateur.

    Parameters
    ----------
    schema : str
        Schéma de la région
    indicateur_id: integer
        Identifiant de l'indicateur
    metadata : dict
        Méta-données pour meta.indicateur et meta.chart
    data_custom : str
        Formule de calcul pour l'analyse
        Si data est non null, alors fpath doit être vide, et on utilise data à la place du nom de la table pour composer l'analyse
    """

    # On récupère la table associée à l'indicateur en se basant sur la colonne data

    # Mise à jour de meta.indicateur
    query_table = """
            with indicateur_table as (
                select distinct (regexp_split_to_array(data, E'[\*+/\]'))[1] as nom_table
                from meta.indicateur where id = $1
            )
            select c.column_name
            from information_schema.columns c, indicateur_table i
            where table_schema = $2
            and table_name = i.nom_table
            """

    query_update = """
        update meta.indicateur
        set nom = $1,
            unit = $2,
            decimals = $3,
            years = $4,
            filter = $5,
            color_start = $6,
            color_end = $7,
            type = $8,
            confidentiel = $9,
            only_for_zone = $10,
            disabled_for_zone = $11,
            disabled_for_macro_level = $12,
            credits_analysis_producers = $13,
            credits_data_sources = $14,
            credits_data_producers = $15,
            donnees_exportables = $16,
            estimated_years = $17,
            year_selection_input_type= $18,
            display_total= $19,
            disabled_in_dashboard = $20,
            classification_config = $21
        where id = $22
        returning id
    """
    query_update_sub_indicator = """ 
                                    INSERT INTO meta.sub_indicator(parent, indicator, region)
                                    VALUES ($1, $2, $3)
                                    ON CONFLICT (indicator)
                                    DO 
                                    UPDATE SET parent = $1
                                    WHERE meta.sub_indicator.indicator = $2
                                    AND meta.sub_indicator.region = $3

                                """

    query_update_sub_indicator_category = """ 
                                            UPDATE meta.sub_indicator
                                            SET category = $1, modality = $2, modality_id=$3 
                                            WHERE region = $4 AND indicator = $5
                                           """
    query_delete_sub_indicator = """
                                    DELETE FROM meta.sub_indicator
                                    WHERE indicator = $1
                                """
    query_update_methodo = """
        update meta.indicateur
        set methodo_pdf = $1
        where id = $2
        returning id
    """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                "récupération des colonnes de la table de données associées à l'indicateur"
            )
            rset = await conn.fetch(query_table, indicateur_id, schema)
            colonnes = [x[0] for x in rset]
            # on ordonne les champs : commune, *champs supplémentaires, annee, valeur. On extrait que les catégories
            categories = list(
                set(colonnes).difference(
                    ("commune", "annee", "valeur", "valeur_filtre")
                )
            )
            # on vérifie que les catégories dans les colonnes sont cohérentes avec les méta-données.
            metadata_categories = set(
                [category["category"] for category in metadata["categories"]]
            )
            intersection_categories = set(categories).intersection(metadata_categories)
            if metadata_categories != intersection_categories:
                raise ValidationError(
                    "Les catégories dans les colonnes ne sont pas cohérentes avec les catégories dans les métadonnées (remplies depuis cet écran ou l'écran de gestion des données)"
                )
            parent_value = metadata.get("parent")
            if parent_value and parent_value != "undefined":
                parent_id = int(parent_value)
                await conn.execute(
                    query_update_sub_indicator, parent_id, indicateur_id, region
                )

                parent_category = metadata.get("parentCategory")
                if parent_category and parent_category != "undefined":
                    modality_id = parent_category["value"]
                    category, modality = parent_category["label"].split(".")
                    await conn.execute(
                        query_update_sub_indicator_category,
                        category,
                        modality,
                        modality_id,
                        region,
                        indicateur_id,
                    )
            else:
                await conn.execute(query_delete_sub_indicator, indicateur_id)

            # Enregistrement du fichier methodo
            nomFichierMethodo = ""
            pdf_failed = False
            if metadata["fichierPdfMethodo"]:
                fichierPdf = await enregistrerPdfMethodo(
                    region,
                    metadata["fichierPdfMethodo"],
                    final_name=f'{str(indicateur_id)}_{slugify.slugify(metadata["nom"])}',
                )
                if fichierPdf["status"] == "success":
                    nomFichierMethodo = fichierPdf["valeur"]
                    rset = await conn.fetch(
                        query_update_methodo, nomFichierMethodo, indicateur_id
                    )
                    logger.info(
                        "Méthodologie de l'indicateur id=%d mise à jour", indicateur_id
                    )
                else:
                    logger.info(
                        "La mise à jour de la méthodologie de l'indicateur id=%d n'a pas fonctionné (%s) avec l'erreur :\n %s",
                        indicateur_id,
                        fichierPdf["status"],
                        fichierPdf["valeur"],
                    )
                    pdf_failed = True

            logger.info("Mise à jour dans 'meta.indicateur'")
            rset = await conn.fetch(
                query_update,
                metadata["nom"],
                metadata["unit"],
                metadata["decimals"],
                metadata["years"],
                metadata["filter"],
                "#" + metadata["color_start"],
                "#" + metadata["color_end"],
                metadata["type"],
                metadata["confidentiel"],
                metadata["only_for_zone"],
                metadata["disabled_for_zone"],
                metadata["disabled_for_macro_level"],
                metadata["credits_analysis_producers"],
                metadata["credits_data_sources"],
                metadata["credits_data_producers"],
                metadata["donnees_exportables"],
                metadata["estimated_years"],
                metadata["year_selection_input_type"],
                metadata["display_total"],
                metadata["disabled_in_dashboard"],
                metadata["classification_config"],
                indicateur_id,
            )
            if data_custom:
                await conn.fetch(
                    """update meta.indicateur
                        set data = $1
                    where id = $2""",
                    data_custom,
                    indicateur_id,
                )

            logger.info("indicateur avec id=%d mis à jour", indicateur_id)

            # Suppression des charts existants
            categories = metadata["categories"]
            logger.info("suppression dans 'meta.chart'")
            q = """SELECT categorie from meta.chart
                where indicateur = $1
                ORDER BY ordre
            """
            rset = await conn.fetch(q, indicateur_id)
            existing_categories = set([x["categorie"] for x in rset])
            new_categories = set([n["category"] for n in categories])
            added_categories = new_categories.difference(existing_categories)
            removed_categories = existing_categories.difference(new_categories)
            await refresh_dashboards_categories(
                region,
                conn,
                indicateur_id,
                added_categories,
                removed_categories,
                categories,
            )

            q = """delete from meta.chart
                where indicateur = $1
            """
            rset = await conn.fetch(q, indicateur_id)
            # Ajout des charts
            logger.info("insertion dans 'meta.chart'")
            q = """insert into meta.chart (indicateur, titre, ordre, type, categorie, visible, description, is_single_select)
            values ($1, $2, $3, $4, $5, $6, $7, $8)
            """
            if categories:
                await conn.executemany(
                    q,
                    [
                        [
                            indicateur_id,
                            cat.get("title", cat.get("category")),
                            cat.get("order", i),
                            cat.get("type", "pie"),
                            cat.get("category"),
                            True,
                            cat.get("description", None),
                            cat.get("is_single_select", False),
                        ]
                        for i, cat in enumerate(categories)
                    ],
                )

            for detail_key, detail_value in metadata["representation_details"].items():
                await conn.fetch(
                    """DELETE FROM meta.indicateur_representation_details
                        WHERE indicateur_id = $1 AND name = $2""",
                    indicateur_id,
                    detail_key,
                )
                await conn.fetch(
                    """INSERT INTO meta.indicateur_representation_details
                        VALUES($1, $2, $3)""",
                    indicateur_id,
                    detail_key,
                    str(detail_value),
                )

            return {"status": True, "pdf_failed": pdf_failed}


async def refresh_dashboards_categories(
    region, conn, indicateur_id, added_categories, removed_categories, categories_data
):
    query_dashboards = f"""SELECT tableau, t.id as thematique
    FROM meta.tableau_thematique t
    LEFT JOIN meta.tableau_bord tb ON t.tableau = tb.id
    WHERE EXISTS (
        SELECT *
        FROM json_array_elements(t.graphiques) as x(o)
        WHERE (x.o ->> 'categories')::jsonb ?| $1
        AND (x.o ->> 'id_analysis') = $2
    ) AND tb.region = $3;"""
    rset_dashboards = await controller.fetch(
        query_dashboards,
        list(added_categories.union(removed_categories)),
        str(indicateur_id),
        region,
    )
    related_dashboards_groups = set(
        int(chart["thematique"]) for chart in rset_dashboards
    )
    query = f"""SELECT id, graphiques FROM meta.tableau_thematique WHERE id = ANY($1)"""
    dashboards_groups = await conn.fetch(query, related_dashboards_groups)
    for group in dashboards_groups:
        graphiques = json.loads(group.get("graphiques", "{{}}"))
        for graph in graphiques:
            # we avoid editing other graphs than those related to current analysis
            if graph.get("id_analysis", None) != indicateur_id:
                continue
            # TODO: handle case when no more categories?
            for category_name in removed_categories:
                if category_name in graph["categories"]:
                    del graph["categories"][category_name]

            for category_name in added_categories:
                if category_name not in graph["categories"]:
                    category = next(
                        (
                            cat
                            for cat in categories_data
                            if cat["category"] == category_name
                        ),
                        None,
                    )
                    if category is None:
                        continue
                    graph["categories"][category_name] = {
                        "titre": category.get("titre", category_name),
                        "categorie": category_name,
                        "visible": False,
                    }
        await conn.execute(
            "UPDATE meta.tableau_thematique SET graphiques = $1 WHERE id = $2",
            json.dumps(graphiques),
            int(group["id"]),
        )


async def inserer_nouvelle_categorie(fpath, nom, region):
    """
    Ajout d'une nouvelle catégorie (après upload d'une fichier CSVS compatible)

    Parameters
    ----------
    fpath : Path
        Fichier de données CSV
    nom : str
        nom de la table
    region : str
        Nom de la région sans accent si majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)
    """
    est_valide = verifie_headers(fpath, ("modalite_id", "modalite", "ordre", "couleur"))
    if not est_valide:
        raise ValidationError(est_valide.message)
    else:
        headers = lecture_headers(fpath)
        headers = [normalize(x) for x in headers]
        entete = "(nom, " + ", ".join(headers) + ", " + "region)"
        with open(fpath, "r") as fichier_categorie:
            for ligne in fichier_categorie.readlines()[1:]:
                liste_valeurs = ligne.split(";")
                liste_valeurs.insert(0, nom)
                liste_valeurs.append(region)
                liste_valeurs_finale = [
                    int(elem) if elem.isdigit() else elem
                    for elem in map(str.strip, liste_valeurs)
                ]
                parametres_sql = ", ".join(
                    ["$" + str(i) for i in range(1, len(liste_valeurs) + 1)]
                )
                sql = """
                      insert into meta.categorie {colonnes}
                      VALUES({parametres})
                      """.format(
                    colonnes=entete, parametres=parametres_sql
                )
                try:
                    await controller.fetch(sql, *liste_valeurs_finale)
                except:
                    raise ValidationError(
                        "Cette catégorie existe déjà ou ne peut être ajoutée. Vérifiez le format et/ou utilisez un autre nom."
                    )


async def update_categorie(fpath, nom, region):
    """
    Mise à jour d'une catégorie (après upload d'une fichier CSVS compatible)

    Parameters
    ----------
    fpath : Path
        Fichier de données CSV
    nom : str
        nom de la table
    region : str
        Nom de la région sans accent si majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)
    """

    # Pré validation du fichier CSV
    est_valide = verifie_headers(fpath, ("modalite_id", "modalite", "ordre", "couleur"))
    if not est_valide:
        raise ValidationError(est_valide.message)

    # Supression de la catégorie originale
    sql = """
            delete from meta.categorie
            where nom = $1 and region = $2
            """
    try:
        await controller.fetch(sql, nom, region)
    except:
        raise ValidationError("Impossible de supprimer la catégorie")

    # re-création de cette catégorie
    await inserer_nouvelle_categorie(fpath, nom, region)


async def create_indicator_data_table(
    schema,
    table,
    categories,
    ajouter_annee,
    ajouter_valeur_filtre,
    verifier_categorie,
    is_pixel=False,
    forced=False,
    mesh_perimeter="commune",
):
    """On crée la structure d'une table pour un indicateur.

    Parameters
    ----------
    schema : str
    table : str
    categories : list
        Liste du nom des catégories
    """
    condition_annee = ""
    if ajouter_annee:
        condition_annee = ", annee float"
    valeur_filtre = ""
    if ajouter_valeur_filtre:
        valeur_filtre = ", valeur_filtre float"
    geom_col = ""
    if is_pixel:
        geom_col = ", geom text"
    if not forced:
        is_valid = await existence_table(schema, table)
        if not is_valid:
            raise ValidationError(is_valid.message)
    if verifier_categorie:
        is_valid = await existence_categorie(schema, categories)
        if not is_valid:
            raise ValidationError(is_valid.message)
    categories_datatypes = "\n".join(", {} int".format(x) for x in categories)
    return f"""create table {schema}.{table} (
       {mesh_perimeter} varchar
     {categories_datatypes}
     {condition_annee}
     , valeur float
     {valeur_filtre}
     {geom_col}
    )""".format(
        annee=condition_annee, valeur_filtre=valeur_filtre
    )


def req_creer_table_territoire(schema):
    req = """
          create table {schema}.territoire (
              code varchar
              , commune varchar
              , nom varchar
              , type_territoire varchar
          )
          """.format(
        schema=schema
    )
    return req


async def date_perimetre_donnee_ajoutee(conn, region, table, date, only_for_zone=""):
    req = """
          insert into meta.perimetre_geographique (nom_table, region, date_perimetre, only_for_zone)
          values ($1, $2, $3, $4);
          """
    await conn.fetch(req, table, region, date, only_for_zone)


async def integrer_donnee(
    conn, schema, table, fpath, colonnes_obligatoires, mesh_perimeter="commune"
):
    headers = lecture_headers(fpath)
    is_valid = verifie_headers(fpath, colonnes_obligatoires)
    if not is_valid:
        raise ValidationError(is_valid.message)

    logger.info("insertion des données dans le schéma régional")
    if table == "territoire":
        requete_creation_table = req_creer_table_territoire(schema)
    else:
        requete_creation_table = await create_indicator_data_table(
            schema,
            table,
            list(
                set(headers).difference(list(colonnes_obligatoires) + ["valeur_filtre"])
            ),
            True,
            False,
            False,
            mesh_perimeter=mesh_perimeter,
        )
    await conn.fetch(requete_creation_table)
    await conn.copy_to_table(
        table,
        source=fpath,
        schema_name=schema,
        header=True,
        format="csv",
        delimiter=";",
        columns=headers,
    )


async def create_indicator(region, schema, table, metadata, fpath, data_custom):
    """Création d'un nouvel indicateur.

    Parameters
    ----------
    schema : str
        Nom du schéma
    table : str
        Nom de la table de l'indicateur
    metadata : dict
        Méta-données pour meta.indicateur et meta.chart
    fpath : Path
        Chemin du fichier CSV
        Si ce chemin est vide, cela signifie que l'analyse utilise des données existantes en base
    data_custom : str
        Formule de calcul pour l'analyse
        Si data est non null, alors fpath doit être vide, et on utilise data à la place du nom de la table pour composer l'analyse
    """
    data = table
    query_create = ""
    is_pixel = False
    if metadata["type"] != "wms_feed" and not data_custom:
        colonnes_obligatoires = ("commune", "annee", "valeur")
        is_valid = verifie_headers(fpath, colonnes_obligatoires)
        if not is_valid:
            raise ValidationError(is_valid.message)
        headers = lecture_headers(fpath)
        # normalisation des headers
        headers = [normalize(x) for x in headers]
        ajouter_valeur_filtre = False
        if "valeur_filtre" in headers:
            ajouter_valeur_filtre = True
        # on ordonne les champs : commune, *champs supplémentaires, annee, valeur. On extrait que les catégories
        static_cols = {"commune", "annee", "valeur", "valeur_filtre"}
        if metadata["type"] == "pixels" or metadata["type"] == "pixels_cat":
            static_cols.add("geom")
            is_pixel = True
        categories = list(set(headers).difference(static_cols))
        # on vérifie que les catégories dans les headers sont cohérentes avec les méta-données.
        metadata_categories = set(
            [category["category"] for category in metadata["categories"]]
        )
        intersection_categories = set(categories).intersection(metadata_categories)
        if metadata_categories != intersection_categories:
            raise ValidationError(
                "Les catégories dans les headers ne sont pas cohérentes avec les catégories dans les métadonnées (remplies depuis cet écran ou l'écran de gestion des données)"
            )
        query_create = await create_indicator_data_table(
            schema, table, categories, True, ajouter_valeur_filtre, True, is_pixel
        )
    else:
        data = data_custom

    # XXX(dag) quid des index...
    # insertion dans meta.indicateur
    query_insertion = """
                      insert into meta.indicateur (
                        nom, data, type, color_start, color_end,
                        unit, decimals, ui_theme, years, region,
                        isratio, filter, confidentiel, only_for_zone, disabled_for_zone,
                        disabled_for_macro_level, donnees_exportables,
                        credits_analysis_producers, credits_data_sources, credits_data_producers,
                        estimated_years, year_selection_input_type, display_total, disabled_in_dashboard, classification_config)
                      values (
                        $1, $2, $3, $4, $5,
                        $6, $7, $8, $9, $10,
                        $11, $12, $13, $14, $15,
                        $16, $17, 
                        $18, $19, $20,
                        $21, $22, $23, $24, $25)
                      returning id
                      """
    query_update_filename = "UPDATE meta.indicateur SET methodo_pdf = $1 WHERE id = $2"

    ajout_perimetre_origine = """
                              insert into meta.perimetre_geographique
                              values ($1, $2, $3)
                              """
    query_insert_sub_indicator = """ 
                                    insert into meta.sub_indicator (indicator, parent, region)
                                    values ($1, $2, $3)
                                """

    query_insert_sub_indicator_category = """  
                                            UPDATE meta.sub_indicator
                                            SET category = $1, modality = $2, modality_id=$3 
                                            WHERE region = $4 AND indicator = $5
                                        """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info("insertion dans 'meta.indicateur'")
            rset = await conn.fetch(
                query_insertion,
                metadata["nom"],
                data,
                metadata["type"],
                "#" + metadata["color_start"],
                "#" + metadata["color_end"],
                metadata["unit"],
                metadata["decimals"],
                metadata["ui_theme"],
                metadata["years"],
                region,
                metadata["is_ratio"],
                metadata["filter"],
                metadata["confidentiel"],
                metadata["only_for_zone"],
                metadata["disabled_for_zone"],
                metadata["disabled_for_macro_level"],
                metadata["donnees_exportables"],
                metadata["credits_analysis_producers"],
                metadata["credits_data_sources"],
                metadata["credits_data_producers"],
                metadata["estimated_years"],
                metadata["year_selection_input_type"],
                metadata["display_total"],
                metadata["disabled_in_dashboard"],
                metadata["classification_config"],
            )
            indicateur_id = rset[0]["id"]

            # Enregistrement du fichier methodo
            nomFichierMethodo = ""
            if metadata["fichierPdfMethodo"]:
                logger.info(f"Essai d'enregistrement du fichier pdf")
                fichierPdf = await enregistrerPdfMethodo(
                    region,
                    metadata["fichierPdfMethodo"],
                    final_name=f'{indicateur_id}_{slugify.slugify(metadata["nom"])}',
                )
                if fichierPdf["status"] == "success":
                    logger.info(f"Fichier PDF enregistré !")
                    nomFichierMethodo = fichierPdf["valeur"]
            await conn.execute(query_update_filename, nomFichierMethodo, indicateur_id)
            if metadata["parent"] not in ["undefined", None]:
                await conn.execute(
                    query_insert_sub_indicator,
                    indicateur_id,
                    int(metadata["parent"]),
                    region,
                )

                if metadata["parentCategory"] not in ["undefined", None]:
                    modality_id = metadata["parentCategory"]["value"]
                    parentCategory = metadata["parentCategory"]["label"].split(".")
                    category = parentCategory[0]
                    modality = parentCategory[1]
                    await conn.execute(
                        query_insert_sub_indicator_category,
                        category,
                        modality,
                        modality_id,
                        region,
                        indicateur_id,
                    )

            if metadata["type"] != "wms_feed":
                date_perimetre_geo = metadata["annee_perimetre_geographique"]
                if date_perimetre_geo != "undefined":
                    await conn.fetch(
                        ajout_perimetre_origine,
                        table,
                        region,
                        int(metadata["annee_perimetre_geographique"], 10),
                    )
                logger.info("nouvel indicateur avec id=%d", indicateur_id)
                logger.info("insertion dans 'meta.chart'")
                q = """insert into meta.chart (indicateur, titre, ordre, type, categorie, visible, description, is_single_select)
                values ($1, $2, $3, $4, $5, $6, $7, $8)
                """
                categories = metadata["categories"]
                if categories:
                    await conn.executemany(
                        q,
                        [
                            [
                                indicateur_id,
                                cat.get("title", cat.get("category")),
                                cat.get("order", i),
                                cat.get("type", "pie"),
                                cat.get("category"),
                                True,
                                cat.get("description", None),
                                cat.get("is_single_select", False),
                            ]
                            for i, cat in enumerate(categories)
                        ],
                    )

                if not data_custom:
                    # créer la table
                    logger.info("création de la table '%s'", table)
                    await conn.execute(query_create)
                    # insérer les données
                    logger.info("insertion des données '%s'", fpath)
                    await conn.copy_to_table(
                        table,
                        source=fpath,
                        schema_name=schema,
                        header=True,
                        format="csv",
                        delimiter=";",
                        columns=headers,
                    )

                    # We change geom column type
                    if is_pixel:
                        await conn.execute(
                            f"""
                            ALTER TABLE {schema}.{table}
                            ALTER COLUMN geom TYPE geometry(Polygon,3857)
                            USING ST_GeomFromText(geom, 3857);
                            """
                        )
                        await conn.execute(
                            f"""
                            ALTER TABLE {schema}.{table}
                            ALTER COLUMN annee TYPE smallint;
                            """
                        )
                        await conn.execute(
                            f"""
                            ALTER TABLE {schema}.{table}
                            ADD COLUMN id SERIAL PRIMARY KEY;
                            """
                        )
                        await conn.fetch(
                            f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_geom_idx ON {schema}.{table} USING GIST(geom);""",
                        )

                    await conn.fetch(
                        f"CREATE INDEX IF NOT EXISTS {schema}_{table}_idx ON {schema}.{table} (commune)"
                    )
                    await conn.fetch(
                        f"CREATE INDEX IF NOT EXISTS {schema}_{table}_year_idx ON {schema}.{table} (annee);"
                    )

                    for category in categories:
                        await conn.fetch(
                            f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_{slugify.slugify(category["category"]).replace('-', '_')}_idx ON {schema}.{table} ({category["category"]});""",
                        )

            for detail_key, detail_value in metadata["representation_details"].items():
                await conn.fetch(
                    """INSERT INTO meta.indicateur_representation_details
                        VALUES($1, $2, $3)""",
                    indicateur_id,
                    detail_key,
                    str(detail_value),
                )

            return indicateur_id


async def enregistrerPdfMethodo(region, fichier_data, final_name=None):
    """Enregistrement du fichier PDF de méthodologie pour une analyse

    Parameters
    ----------
    region: str (région)
    fichier_data : filedata fichier PDF
    """

    # On crée un nom unique pour le fichier de données
    now = datetime.datetime.now()
    dateId = (
        "%d%02d%d_%02d%02d%02d%d"
        % (
            now.year,
            now.month,
            now.day,
            now.hour,
            now.minute,
            now.second,
            now.microsecond,
        )
    )[:-4]
    if final_name is None:
        nomFichier = "{}_{}".format(dateId, slugify.slugify(fichier_data.name))
    else:
        nomFichier = final_name + ".pdf"
    file_path = settings.get("api", "upload_methodo", fallback=False)
    if not file_path:
        logger.error(f"Le paramètre upload_methodo manque dans la configuration.")
        return {
            "status": "error",
            "valeur": f"Le chemin upload_methodo n'a pas été spécifié dans le fichier de configuration de terriapi.",
        }
    nomFichierComplet = "{}/{}/{}".format(
        settings.get("api", "upload_methodo"), region, nomFichier
    )
    logger.info(f"Tente de déplacer vers {nomFichierComplet}")

    # Enregistrement du fichier
    try:
        with open(nomFichierComplet, "wb") as fd:
            fd.write(fichier_data.body)
        return {"status": "success", "valeur": nomFichier}
    except EnvironmentError:
        logger.error(f"Tentative de sauvegarde du fichier PDF échouée.")
        return {
            "status": "error",
            "valeur": f"Problème lors du téléchargement du fichier de méthodologie (vers le chemin {nomFichierComplet})",
        }


def isConvertibleDataTable(df_data):
    """Checks that the DataFrame is a valid TerriSTORY data table that is convertible to a new perimeter :
    - the table has a column "commune" and a column "valeur"
    - the table does not have a column "geom" (pixel data is not yet supported)

    Parameters
    ----------
    df_data : DataFrame
        DataFrame of the table

    Returns
    -------
    bool
        Whether or not the table is a valid TerriSTORY data table
    """
    # Tables of flow or pixel data are not yet supported by the conversion
    # TODO : add support for flow and pixel data
    forbidden_columns = ["geom"]
    has_forbidden_columns = any([col in df_data.columns for col in forbidden_columns])
    if has_forbidden_columns:
        return False

    required_columns = ["commune", "valeur"]
    isValid = all([col in df_data.columns for col in required_columns])
    return isValid


def get_value_columns_to_convert(df_data):
    """Returns the names of the numerical value columns in the table that should be converted.
    If the aggregation function is "sum", these columns will be multiplied by the ratio of the conversion table during the perimeter conversion.

    For now only the column "valeur" is used as a value column.

    Parameters
    ----------
    df_data : DataFrame
        The DataFrame of the table

    Returns
    -------
    List[str]
        The list of columns to convert
    """
    possible_columns = ["valeur"]
    return [col for col in possible_columns if col in df_data.columns]


def get_commune_columns(df_data):
    """Returns the names of the commune columns containing the INSEE commune codes.
    Usually only "commune", but also "commune_dest" in the case of flow data.

    Parameters
    ----------
    df_data : DataFrame
        The DataFrame of the table

    Returns
    -------
    List[str]
        The list of commune columns for the perimeter conversion
    """
    possible_columns = ["commune", "commune_dest"]
    return [col for col in possible_columns if col in df_data.columns]


async def convert_single_table_to_new_perimeter(
    schema, table, initial_year, target_year, aggregation_function
):
    """Converts a single table to a new perimeter.
    It is assumed that the original data has already been migrated to the table <schema>_ancien_perimetre.<table>.
    The converted table replaces the existing table <schema>.<table>

    Parameters
    ----------
    schema : str
        The regional schema
    table : str
        The name of the table to convert
    initial_year : int
        The initial perimeter year
    target_year : int
        The target perimeter year
    aggregation_function : str
        The aggregation function to use for the conversion.
    """
    # convert to new perimeter
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(
                f"Initialising perimeter conversion for the table {schema}.{table} ({initial_year} => {target_year})"
            )
            # get the original data
            req = """
                      select *
                      from {schema}.{table};
                      """.format(
                schema=schema + "_ancien_perimetre", table=table
            )
            data = await conn.fetch(req)
            df_data = pd.DataFrame([dict(x) for x in data])
            # Check if the table meets the minimum requirements for conversion
            if not isConvertibleDataTable(df_data):
                raise ValidationError(
                    f"La table {table} ne peut pas être convertie dans un nouveau périmètre. Elle ne contient pas les colonnes obligatoires (commune, valeur) ou contient des données spéciales de type 'pixel' qui ne sont pas encore gérées par la conversion."
                )
            value_cols = get_value_columns_to_convert(df_data)
            commune_cols = get_commune_columns(df_data)
            df_converted = await convert_df_to_perimeter(
                df_data,
                initial_year,
                target_year,
                value_cols,
                commune_cols,
                aggregation_function,
            )

            # Truncate the original table
            logger.info(f"Truncating the original table {schema}.{table}")
            await conn.execute(f"truncate table {schema}.{table}")

            # insert the converted data into the table using asyncpg copy_records_to_table
            logger.info(f"Inserting converted data back into {schema}.{table}")
            await conn.copy_records_to_table(
                table_name=table,
                schema_name=schema,
                records=df_converted.itertuples(index=False),
            )

            # update the meta.perimetre_geographique table
            req_update_meta_perimetre_geographique = """
            update meta.perimetre_geographique
            set date_perimetre = $1
            where region = $2
            and nom_table = $3;
            """
            await conn.fetch(
                req_update_meta_perimetre_geographique,
                target_year,
                schema.replace("_", "-"),
                table,
            )

            logger.info(
                f"The table {schema}.{table} was successfully converted to the new perimeter ({initial_year} => {target_year})"
            )


async def get_table_perimeter_year(region, table):
    """Returns the perimeter year of a table.

    Parameters
    ----------
    region : str
        The normalized region name (ex : auvergne-rhone-alpes, occitanie)
    table : str
        The name of the table

    Returns
    -------
    int
        The perimeter year for the table
    """
    req = """
        select date_perimetre
        from meta.perimetre_geographique
        where region = $1
        and nom_table = $2
    """
    rset = await controller.fetch(req, region, table)
    if len(rset) == 0:
        raise ValueError(
            f"La table {table} n'existe pas où n'est pas correctement configurée"
        )
    if len(rset) > 1:
        raise ValueError(
            f"La table {table} n'est pas correctement configurée. Plusieurs périmètres sont associés à la table."
        )
    perimeter_year = rset[0]["date_perimetre"]
    return perimeter_year


async def convert_tables_to_new_perimeter(
    region, schema, user_info, tables, target_year
):
    """_summary_

    Parameters
    ----------
    region : str
        The regional key
    schema : str
        The regional schema
    user_info : object
        The user object. Used to log the conversions.
    tables : List(dict)
        The list of tables to convert with their aggregation function (ex : [{"table": "table1", "aggregation_function": "sum"}, {"table": "table2", "aggregation_function": "mean"}])
    target_year : int
        The conversion target year
    aggregation_function : str
        The aggregation function to use for the conversion.

    Returns
    -------
    List(dict)
        The list of tables and wether or not the conversion were successful
    """
    # first migrate all the original tables to a temporary schema
    await migration_donnees_dans_nouveau_schema(
        schema, [table["table"] for table in tables]
    )
    # a list to track which table conversions succeeded and which failed
    conversion_results = []
    for table in tables:
        table_name = table["table"]
        conversion_result = {"table": table_name}
        try:
            initial_year = await get_table_perimeter_year(
                schema.replace("_", "-"), table_name
            )
            await convert_single_table_to_new_perimeter(
                schema,
                table_name,
                initial_year,
                target_year,
                table["aggregation_function"],
            )
            # the conversion succeeded
            conversion_result["success"] = True
            conversion_results.append(conversion_result)
        except (ValueError, ValidationError) as e:
            conversion_result["success"] = False
            conversion_result["error_message"] = str(e)
            conversion_results.append(conversion_result)
        # Log the conversion in the data history table
        await controller.add_to_data_history(
            region,
            schema,
            user_info,
            f"Conversion des données dans un nouveau périmètre",
            table_name,
            f"périmètre initial : {initial_year} => périmètre final : {target_year}",
        )
    return conversion_results

    # TODO Cette fonction remplacera convertir_dans_nouveau_perimetre() et permettra de lancer la conversion
    # pour une liste de tables en utilisant les nouvelles fonctions de conversion.


def upload_svg(file_path: str, file_data: File) -> bool:
    try:
        with open(file_path, "wb") as fd:
            fd.write(file_data.body)
        return True
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {file_path}.")
        logger.error(f"Erreur: {str(e)}")
        return False
