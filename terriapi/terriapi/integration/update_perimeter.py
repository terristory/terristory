# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import argparse
import datetime
import sys

import asyncpg
import pandas as pd
from sanic.log import logger

from terriapi import controller, settings
from terriapi.controller import chercher_fichier_des_communes, perimeters

from . import load_shp

hostname = settings.get("api", "listen")
port = settings.get("api", "listen_port")
env = settings.get("api", "env")


async def setup_db():
    """initiate a postgresql connections pool"""
    pool = await asyncpg.create_pool(
        database=settings.get("api", "pg_name"),
        host=settings.get("api", "pg_host"),
        user=settings.get("api", "pg_user"),
        password=settings.get("api", "pg_password"),
        port=settings.get("api", "pg_port"),
        min_size=int(settings.get("api", "pg_max_connection")),
        max_size=int(settings.get("api", "pg_max_connection")),
    )
    controller.db = pool


async def import_passage_table(path, year):
    print("Integrating passage table...")
    df_fusions = pd.read_excel(
        path,
        sheet_name="Liste des fusions",
        skiprows=5,
        dtype={"COM_INI": str, "COM_FIN": str},
    )
    df_fusions.columns = [c.upper() for c in df_fusions.columns]
    df_fusions = df_fusions.loc[
        df_fusions["ANNEE_MODIF"] == year, ["COM_INI", "COM_FIN", "ANNEE_MODIF"]
    ]
    df_fusions.columns = [f"cod{year-1}", f"cod{year}", "annee"]
    df_fusions["typemodif"] = "f"
    df_fusions["ratio"] = 1.0
    df_scissions = pd.read_excel(
        path,
        sheet_name="Liste des scissions",
        skiprows=5,
        dtype={"COM_INI": str, "COM_FIN": str},
    )
    df_scissions.columns = [c.upper() for c in df_scissions.columns]
    df_scissions = df_scissions.loc[
        df_scissions["ANNEE_MODIF"] == year, ["COM_INI", "COM_FIN", "ANNEE_MODIF"]
    ]
    df_scissions.columns = [f"cod{year-1}", f"cod{year}", "annee"]
    df_scissions["typemodif"] = "d"
    df_scissions["ratio"] = 0.0
    df_fill_ratio = df_scissions.groupby(f"cod{year-1}")
    for municipality, group in df_fill_ratio:
        print(
            f"La commune initiale de code {municipality} est divisée en {len(group)} communes.\nPouvez-vous spécifier entre les clés de répartition entre les communes nouvelles (on pourra prendre par exemple la distribution de la population) :"
        )
        i = 0
        for _, new_municipality in group.iterrows():
            print("\t", new_municipality[f"cod{year}"])
            if i == len(group) - 1:
                ratio = (
                    1.0
                    - df_scissions.loc[
                        df_scissions[f"cod{year-1}"] == municipality, "ratio"
                    ].sum()
                ) * 100.0
            else:
                ratio = -1
                while ratio < 0 or ratio > 100:
                    ratio = input(
                        "\t Quelle clé de répartition dans cette commune nouvelle (entre 0 et 100) ? "
                    )
                    ratio = float(ratio)
            print("\t\t ==> ratio choisi :", ratio, "%")
            df_scissions.loc[
                (df_scissions[f"cod{year-1}"] == municipality)
                & (df_scissions[f"cod{year}"] == new_municipality[f"cod{year}"]),
                "ratio",
            ] = (
                ratio / 100.0
            )
            i += 1
        print("-" * 40)
    table_name = f"passage_{year-1}_{year}"
    df = pd.concat([df_fusions, df_scissions])
    df["annee"] = "01/01/" + df["annee"].astype(str)

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.execute(f"DROP TABLE IF EXISTS table_passage.{table_name}")

            await conn.execute(
                f"""CREATE TABLE table_passage.{table_name}
                    (
                        cod{year-1} character varying,
                        cod{year} character varying,
                        annee character varying,
                        typemodif character varying,
                        ratio double precision
                    )
                """
            )

            # we convert our dataframe to usable data for copy_records function
            tuples = [tuple(x) for x in df.values]
            await conn.copy_records_to_table(
                table_name,
                records=tuples,
                columns=list(df.columns),
                schema_name="table_passage",
            )


async def update_perimeters(dataset_path, year, passage_table=None):
    """
    Importe les contours.
    """
    year = int(year)
    if passage_table is not None:
        await import_passage_table(passage_table, year)
    logger.warn("Lance la récupération des fichiers shapefiles")
    chemin_telechargement_donnees_shp = settings.get(
        "api", "chemin_telechargement_donnees_shp"
    ) + datetime.datetime.today().strftime("%Y-%m-%d-%s")
    # Téléchargement des données de l'IGN
    await perimeters.convert_archive_to_folder(
        dataset_path, chemin_telechargement_donnees_shp
    )
    # chemin_telechargement_donnees_shp = "/home/mdenoux/Bureau/TerriStory/terriapi/terriapi/data/donnees_ign/2024-05-27-1716812751/"

    # Obtention du chemin vers les données des communes
    chemin_donnees_shp_commune = await chercher_fichier_des_communes(
        chemin_telechargement_donnees_shp, "COMMUNE.shp"
    )
    #  On a aussi besoin des données des arrondissements des métropoles
    chemin_donnees_shp_arrondissement = await chercher_fichier_des_communes(
        chemin_telechargement_donnees_shp, "ARRONDISSEMENT_MUNICIPAL.shp"
    )
    # Téléchargement des EPCI de France pour en obtenir les noms
    chemin_donnees_shp_epci = await chercher_fichier_des_communes(
        chemin_telechargement_donnees_shp, "EPCI.shp"
    )
    # Téléchargement des départements de France pour en obtenir les noms
    chemin_donnees_shp_departement = await chercher_fichier_des_communes(
        chemin_telechargement_donnees_shp, "DEPARTEMENT.shp"
    )

    # Si les données n'ont pas été obtenues, cela signifie que le jeu de données sélectionné
    # est structuré un peu différemment et qu'il faut chercher autrement
    if not chemin_donnees_shp_commune:
        # Nouvelle recherche avec le paramètre l93 initialisé à False
        chemin_donnees_shp_commune = await chercher_fichier_des_communes(
            chemin_telechargement_donnees_shp, "COMMUNE.shp", l93=False
        )
        # Idem avec les arrondissements
        chemin_donnees_shp_arrondissement = await chercher_fichier_des_communes(
            chemin_telechargement_donnees_shp, "ARRONDISSEMENT_MUNICIPAL.shp", l93=False
        )
        # Téléchargement des EPCI de France pour en obtenir les noms
        chemin_donnees_shp_epci = await chercher_fichier_des_communes(
            chemin_telechargement_donnees_shp, "EPCI.shp", l93=False
        )
        # Téléchargement des départements de France pour en obtenir les noms
        chemin_donnees_shp_departement = await chercher_fichier_des_communes(
            chemin_telechargement_donnees_shp, "DEPARTEMENT.shp", l93=False
        )

    logger.warn("Intègre les données IGN des communes de France")
    await load_shp(
        chemin_donnees_shp_commune, "france", f"ign_communes_{year}"
    )  # Chargement des données communales
    logger.warn("Intègre les données IGN des arrondissements de France")
    await load_shp(
        chemin_donnees_shp_arrondissement, "france", f"ign_arrondissements_{year}"
    )  # Chargement des données sur les arrondissements
    logger.warn("Intègre les données IGN des EPCI de France")
    await load_shp(chemin_donnees_shp_epci, "france", f"ign_epci_{year}")
    logger.warn("Intègre les données IGN des départements de France")
    await load_shp(chemin_donnees_shp_departement, "france", f"ign_dep_{year}")
    logger.warn("Intègre les données IGN")
    await perimeters.integrate_ign_data(year)

    # insert information about perimeters available
    sql = "DELETE FROM meta.perimeter_global WHERE year = $1"
    await controller.execute(sql, int(year))
    sql = "INSERT INTO meta.perimeter_global (year) VALUES($1)"
    await controller.execute(sql, int(year))


def _parse_args(args):
    parser = argparse.ArgumentParser(
        description="Update common geographical perimeters and passage table associated with a new year."
    )
    parser.add_argument(
        "-y",
        "--year",
        type=int,
        required=False,
        help="Perimeter year (default: current year)",
    )
    parser.add_argument(
        "-p",
        "--path",
        type=str,
        required=True,
        help="Path to .7z file with all perimeters data (see https://geoservices.ign.fr/adminexpress)",
    )
    parser.add_argument(
        "-t",
        "--passage-table",
        type=str,
        required=False,
        help="Path to .xlsx file with passage table of fusion and division of municipalities from INSEE (see `Table de passage annuelle` https://www.insee.fr/fr/information/7671867)",
    )
    args = parser.parse_args()
    return args


def main(argv=sys.argv[1:]):
    args = _parse_args(argv)
    import asyncio

    year = args.year
    if year is None:
        year = datetime.datetime.now().year
    path = args.path
    passage_table = args.passage_table
    loop = asyncio.get_event_loop()
    loop.run_until_complete(setup_db())
    loop.run_until_complete(update_perimeters(path, year, passage_table))
    loop.close()
