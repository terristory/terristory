# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import asyncio
import logging

import asyncpg

from terriapi import controller, settings
from terriapi.controller.pcaet_ademe import PcaetError, update_pcaet_from_ademe

logging.basicConfig(encoding="utf-8", level=logging.INFO)


def update_pcaet():
    loop = asyncio.get_event_loop()
    logging.info("Create connection to database")
    controller.db = loop.run_until_complete(
        asyncpg.create_pool(
            database=settings.get("api", "pg_name"),
            host=settings.get("api", "pg_host"),
            user=settings.get("api", "pg_user"),
            password=settings.get("api", "pg_password"),
            port=settings.get("api", "pg_port"),
            min_size=int(settings.get("api", "pg_max_connection")),
            max_size=int(settings.get("api", "pg_max_connection")),
            loop=loop,
            server_settings={"application_name": "terriapi"},
        )
    )

    logging.info("Launch PCAET update.")
    loop.run_until_complete(update_pcaet_from_ademe("auto_update"))
    logging.info("PCAET update successful.")
