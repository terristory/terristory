# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import argparse
import asyncio
import datetime
import logging

import asyncpg
import pandas as pd
import slugify

from terriapi import controller, settings
from terriapi.controller.actions import single_action_tester
from terriapi.controller.strategy_actions import passage_table
from terriapi.controller.strategy_actions.loaders import actions_data, param_loaders

FORMAT = "%(asctime)s :: %(levelname)s :: %(name)s :: %(funcName)s : %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger("run-action")


async def setup_db():
    """initiate a postgresql connections pool"""
    logger.debug("create the connection to the db")
    pool = await asyncpg.create_pool(
        database=settings.get("api", "pg_name"),
        host=settings.get("api", "pg_host"),
        user=settings.get("api", "pg_user"),
        password=settings.get("api", "pg_password"),
        port=settings.get("api", "pg_port"),
    )
    controller.db = pool


async def cleanup_db():
    logger.debug("close the db connection")
    await controller.db.close()


def parse_results(results, args):
    key_action = list(results.keys())[0]
    results = results[key_action]
    action_slug = slugify.slugify(args.action)

    now = str(datetime.datetime.now()).replace(" ", "_").replace(":", "-")[0:19]
    results["energie_economisee"]["secteur"].to_csv(
        f"{args.output}/{action_slug}_{now}_energie_economisee_par_secteur.csv", sep=";"
    )
    results["energie_economisee"]["energie"].to_csv(
        f"{args.output}/{action_slug}_{now}_energie_economisee_par_energie.csv", sep=";"
    )
    results["energie_produite"]["energie"].to_csv(
        f"{args.output}/{action_slug}_{now}_energie_produite.csv", sep=";"
    )
    results["facture_energetique"].result.to_csv(
        f"{args.output}/{action_slug}_{now}_facture_energetique.csv", sep=";"
    )
    results["retombees_fiscales"].result.to_csv(
        f"{args.output}/{action_slug}_{now}_retombees_fiscales.csv", sep=";"
    )


def run_single_action():
    loop = asyncio.get_event_loop()
    logging.info("Create connection to database")
    loop.run_until_complete(setup_db())

    parser = argparse.ArgumentParser(description="Exécuter une action")
    parser.add_argument(
        "--region",
        type=str,
        help="Région où exécuter l'action",
        default="auvergne-rhone-alpes",
    )
    parser.add_argument("--action", type=str, help="Clé de l'action à tester")
    parser.add_argument(
        "--zone",
        type=str,
        required=False,
        help="Zone type where the action should be executed",
        default="epci",
    )
    parser.add_argument(
        "--zoneid",
        type=str,
        required=False,
        help="Zone type where the action should be executed",
        default="241501139",
    )
    parser.add_argument(
        "--output",
        type=str,
        required=False,
        help="Output path where to put the files resulting from the tests.",
        default="/tmp",
    )
    args = parser.parse_args()

    logging.info("Launch action.")
    results = loop.run_until_complete(
        single_action_tester(
            args.action,
            args.region,
            args.zone,
            args.zoneid,
            {},  # TODO: implement regional config here
        )
    )
    parse_results(results, args)
    logging.info("Run successful.")

    loop.run_until_complete(cleanup_db())
