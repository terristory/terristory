﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Validation des données d'indicateur/analyses
"""

import unicodedata

import slugify

from terriapi.controller import list_categories, table_datatypes

# séparateur par défaut des données CSV
SEPARATEUR = ";"


class ValidationMessage:
    """Class dédiée aux messages de validation."""

    def __init__(self, valid, message, can_be_forced=False):
        self._valid = valid
        self._message = message
        self.can_be_forced = can_be_forced

    @property
    def message(self):
        return self._message

    def __bool__(self):
        return self._valid

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        prefix = "VALID" if self._valid else "INVALID"
        return ": ".join([prefix, self.message])


def normalize(name):
    """Normalisation d'un champs

    On met en minuscule, sans accent, sans tiret et sans espace.

    Parameters
    ----------
    name : str

    Returns
    -------
    str
    """
    name = (
        unicodedata.normalize("NFKD", name.lower())
        .encode("ascii", "ignore")
        .decode("utf-8")
    )
    name = slugify.slugify(name).replace("-", "_")
    return name


def lecture_headers(fpath, sep=SEPARATEUR, original=False):
    """On lit l'en-tête et renvoie le nom des champs.

    Parameters
    ----------
    fpath : Path
        Fichier de données CSV
    sep : str (optional)
        Séparateur

    Returns
    -------
    list
        Liste des en-têtes du fichier CSV
    """
    with open(fpath, "r") as fobj:
        header = next(fobj)
    if original:
        return header.replace("\n", "").split(sep)
    else:
        return header.replace("\n", "").strip().lower().split(sep)


def verifie_headers(fpath, colonnes_obligatoires):
    """Vérifie le nom des en-têtes

    Le fichier CSV doit contenir au moins quelques noms de colonnes requis.

    Parameters
    ----------
    fpath : Path
        Fichier de données CSV

    Returns
    -------
    ValidationMessage
    """
    header = lecture_headers(fpath)
    for nom in colonnes_obligatoires:
        if nom not in header:
            msg = f"Le champ '{nom}' doit être contenu dans l'en-tête du fichier CSV {fpath}"
            return ValidationMessage(False, msg)
    return ValidationMessage(True, "OK")


async def verifie_table_colonne(schema, table, fpath, is_national=False):
    """On vérifie que l'en-tête du fichier correspond aux noms des colonnes de la table

    Parameters
    ----------
    schema : str
    table : str
    fpath : Path
        Fichier de données CSV

    Returns
    -------
    ValidationMessage
    """
    datatypes = await table_datatypes(schema, table)
    headers = lecture_headers(fpath)
    if len(datatypes) == 0:
        msg = f"La table '{table}' du schéma '{schema}' n'existe pas"
        return ValidationMessage(False, msg)
    # we remove id column if it exists for pixels maps
    if "geom" in datatypes and "geom" in headers and "id" in datatypes:
        del datatypes["id"]
    if len(datatypes) != len(headers):
        msg = f"En-tête et colonnes doivent avoir le même nombre d'éléments. table ({len(datatypes)}) - fichier ({len(headers)})"
        return ValidationMessage(False, msg, True)
    if set(datatypes.keys()) != set(headers):
        msg = f"En-tête et colonnes sont différents. Table {set(datatypes.keys())} - fichier {set(headers)}"
        return ValidationMessage(False, msg, True)
    return ValidationMessage(True, "OK")


async def existence_table(schema, table):
    """Vérifie que la table n'existe pas avant de créer un nouvel indicateur.

    Parameters
    ----------
    region : str
    table : str

    Returns
    -------
    bool, str
        Valide ou non plus un message
    """
    # on récupère un dict 'nom du champs': 'type' de la table.
    datatypes = await table_datatypes(schema, table)
    if len(datatypes) != 0:
        msg = f"La table {table} du schéma {schema} existe déjà"
        return ValidationMessage(False, msg)
    return ValidationMessage(True, "OK")


async def existence_categorie(schema, categories):
    """On vérifie le nom et l'existence des catégories pour la création d'un indicateur

    La catégories doivent exister dans la table meta.categorie
    Parameters
    ----------
    schema : str
    categories : list
        Liste des noms des catégories
    """
    db_categories = await list_categories(schema.replace("_", "-"))
    for categorie in categories:
        if categorie not in db_categories:
            msg = (
                f"La catégorie '{categorie}' n'est pas présente pour la région {schema}"
            )
            return ValidationMessage(False, msg)
    return ValidationMessage(True, "OK")


def verifie_existence_tables(liste_tables_presentes, liste_donnees_obligatoires):
    """Vérification que toutes les tables nécessaires à la mise à jour des territoires sont bien présentes

    Parameters
    ----------
    liste_tables_presentes : liste
        Liste des tables présentes en base de données
    liste_donnees_obligatoires : liste
        Liste des tables nécessaires à la mise à jour des territoires

    Returns
    --------
    Objet de type ValidationMessage à afficher dans l'interface en cas d'erreur
    """
    if set(liste_donnees_obligatoires).issubset(set(liste_tables_presentes)):
        return ValidationMessage(
            True, "Toutes les tables de données indispensables sont bien présentes"
        )
    else:
        return ValidationMessage(
            False,
            "Certaines des tables indispensables à la mises à jour des territoires ne sont pas présentes en base",
        )
