﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module pour envoyer un mail aux administrateurs de région

Point d'entrée pour ``terriapi-mail``. Exemple

> terriapi-mail --env dev
"""


import argparse
import asyncio
import logging
import re
import sys
from os.path import exists as file_exists

import asyncpg
import markdown

from terriapi import controller, settings
from terriapi.controller import user as user_controller
from terriapi.controller.user import send_mail

FORMAT = "%(asctime)s :: %(levelname)s :: %(name)s :: %(funcName)s : %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger("send-mail")


def main(argv=sys.argv[1:]):
    """Fonction principale pour créer un utilisateur·trice.

    C'est le mail qui servira de login.
    """
    args = _parse_args(argv)
    env = args.env
    changelog = args.changelog
    mail = args.mail
    run(env, changelog, mail)


async def setup_db():
    """initiate a postgresql connections pool"""
    logger.debug("create the connection to the db")
    pool = await asyncpg.create_pool(
        database=settings.get("api", "pg_name"),
        host=settings.get("api", "pg_host"),
        user=settings.get("api", "pg_user"),
        password=settings.get("api", "pg_password"),
        port=settings.get("api", "pg_port"),
    )
    controller.db = pool


async def cleanup_db():
    logger.debug("close the db connection")
    await controller.db.close()


async def send_mail_to_users(env, changelog, list_mails):
    """Envoie le mail"""
    logger.info("send the mail")
    if env == "prod":
        url = f"https://terristory.fr"
    else:
        url = f"https://{env}.terristory.fr"
    content = f"""
        <html>
        <p>Une <strong>nouvelle version</strong> vient d'être mise en ligne sur le serveur {env} de TerriSTORY.</p>

        <p>Le contenu de cette nouvelle version est disponible sur <a href="{url}">{url}</a>.</p>

        <p>N'hésitez pas à nous signaler tout bug qui serait apparu à cette occasion !</p>

        <p>Bien cordialement,<br />
        L'équipe de développement de TerriSTORY</p>

        {changelog}
        </html>
        """

    if env == "prod":
        title = f"Nouvelle version de TerriSTORY"
    else:
        title = f"Nouvelle version de TerriSTORY sur {env}"

    status = True
    status = await send_mail(
        "contact@terristory.fr",
        "DEV",
        None,
        "contact@terristory.fr",
        title,
        content,
        settings,
        typemail="html",
    )
    for mail in list_mails:
        if "@" in mail:
            status = await send_mail(
                "contact@terristory.fr",
                "DEV",
                None,
                mail,
                title,
                content,
                settings,
                typemail="html",
            )
    return status


async def get_admins():
    """
    Récupère la liste des administrateurs régionaux

    :return: la liste des admins
    :rtype: list
    """
    return await user_controller.admins()


async def parse_changelog(changelog):
    if file_exists(changelog):
        with open(changelog, "r") as f:
            text = f.read()
            text = re.sub(
                r"#([0-9]+)",
                "[#\\1](https://gitlab.com/terristory/terristory/-/issues/\\1)",
                text,
            )
            html = markdown.markdown(text)
        return html
    else:
        return ""


def run(env, changelog, mail=None):
    """Lance la boucle évenementielle pour exécuter la requête en base."""
    logger.debug("start the event loop")
    loop = asyncio.get_event_loop()
    if mail is not None:
        list_mails = mail.split(",")
    else:
        loop.run_until_complete(setup_db())
        admins = loop.run_until_complete(get_admins())
        list_mails = list(set(admin["mail"] for admin in admins))
        loop.run_until_complete(cleanup_db())
    parsed_changelog = loop.run_until_complete(parse_changelog(changelog))
    loop.run_until_complete(send_mail_to_users(env, parsed_changelog, list_mails))
    loop.close()
    logger.debug("done")


def _parse_args(args):
    parser = argparse.ArgumentParser(
        description="Envoi d'un mail aux administrateurs de région"
    )
    parser.add_argument(
        "--env", type=str, required=True, help="Environnement de déploiement"
    )
    parser.add_argument(
        "--changelog", type=str, required=True, help="Changelog file path"
    )
    parser.add_argument("--mail", type=str, required=False, default=None, help="")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    main()
