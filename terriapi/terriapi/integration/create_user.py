﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module pour créer un utilisateur·trice TerriStory en base de données.

Point d'entrée pour ``terriapi-user``. Exemple

> terriapi-user --prenom Alice --nom Caroll --password rabbit \
   --mail alice@caroll.wonderland \
   --organisation "Collectivité" \
   --fonction "Chef de projet" \
   --territoire "Métropole Lyon" \
   --profil admin \
   --actif
   --region
"""


import argparse
import asyncio
import logging
import sys

import asyncpg

from terriapi import controller, settings
from terriapi.controller import regions_configuration
from terriapi.controller import user as user_controller

FORMAT = "%(asctime)s :: %(levelname)s :: %(name)s :: %(funcName)s : %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger("create-user")


def main(argv=sys.argv[1:]):
    """Fonction principale pour créer un utilisateur·trice.

    C'est le mail qui servira de login.
    """
    args = _parse_args(argv)
    mail, password = args.mail, args.password
    prenom, nom = args.prenom, args.nom
    run(
        prenom,
        nom,
        password,
        mail,
        args.organisation,
        args.fonction,
        args.territoire,
        args.region,
        args.profil,
        args.actif,
        args.is_national,
    )
    logger.info(
        "Utilisateur·trice '%s %s' avec le login '%s' en base", prenom, nom, mail
    )


async def setup_db():
    """initiate a postgresql connections pool"""
    logger.debug("create the connection to the db")
    pool = await asyncpg.create_pool(
        database=settings.get("api", "pg_name"),
        host=settings.get("api", "pg_host"),
        user=settings.get("api", "pg_user"),
        password=settings.get("api", "pg_password"),
        port=settings.get("api", "pg_port"),
    )
    controller.db = pool


async def cleanup_db():
    logger.debug("close the db connection")
    await controller.db.close()


async def create_user(
    prenom,
    nom,
    password,
    mail,
    organisation,
    fonction,
    territoire,
    region,
    profil,
    actif,
    is_national,
):
    """Crée un·e utilisateur·trice."""
    logger.info("create the user %s", mail)
    rset = await user_controller.create_user(
        prenom,
        nom,
        password,
        mail,
        organisation,
        fonction,
        '{"zoneType":"region", "zoneMaille":"epci"}',
        False,
        territoire,
        region,
        profil,
        actif,
        "Accord à demander à l'utilisateur",
        is_national,
    )
    return rset


async def check_region(region):
    region_config = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    if len(region_config) == 0:
        raise ValueError(f"No matching region for `{region}`")


def run(
    prenom,
    nom,
    password,
    mail,
    organisation,
    fonction,
    territoire,
    region,
    profil_id,
    actif,
    is_national,
):
    """Lance la boucle évenementielle pour exécuter la requête en base."""
    logger.debug("start the event loop")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(setup_db())
    loop.run_until_complete(check_region(region))
    loop.run_until_complete(
        create_user(
            prenom,
            nom,
            password,
            mail,
            organisation,
            fonction,
            territoire,
            region,
            profil_id,
            actif,
            is_national,
        )
    )
    loop.run_until_complete(cleanup_db())
    loop.close()
    logger.debug("done")


def _parse_args(args):
    def valid_profil(value):
        if value not in ("admin", "utilisateur"):
            msg = "Le profil doit être 'admin' ou 'utilisateur'"
            raise argparse.ArgumentTypeError(msg)
        return value

    parser = argparse.ArgumentParser(description="Création d'un·e utilisateur·trice")
    parser.add_argument(
        "--prenom", type=str, required=True, help="Prénom l'utilisateur·trice"
    )
    parser.add_argument(
        "--nom", type=str, required=True, help="Nom de l'utilisateur·trice"
    )
    parser.add_argument(
        "--password", type=str, required=True, help="Mot de passe (sans espace)"
    )
    parser.add_argument(
        "--mail", type=str, required=True, help="Email (servira de login)"
    )
    parser.add_argument(
        "--organisation", type=str, required=True, help="Structure-organisation"
    )
    parser.add_argument("--fonction", type=str, required=False, help="Fonction")
    parser.add_argument("--territoire", type=str, required=True, help="Territoire")
    parser.add_argument(
        "--profil",
        type=valid_profil,
        required=True,
        help="Type du profil utilisateur·trice (admin ou utilisateur).",
    )
    parser.add_argument(
        "--actif", action="store_true", help="Utilisateur·trice actif ?"
    )
    parser.add_argument(
        "--is-national",
        action="store_true",
        default=False,
        help="Utilisateur d'un territoire national",
    )
    parser.add_argument("--region", type=str, required=True, help="")
    args = parser.parse_args()
    return args
