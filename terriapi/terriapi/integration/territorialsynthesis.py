﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""
Module dédié aux indicateurs Européens territorialsynthesis
"""
import asyncio
from statistics import median

import aiohttp
import asyncpg

from terriapi import controller, settings
from terriapi.controller.analyse import analysis as list_analysis
from terriapi.controller.territorialsynthesis import (
    update_territorialsynthesis_notes,
    update_territorialsynthesis_stats,
)

hostname = settings.get("api", "listen")
port = settings.get("api", "listen_port")
env = settings.get("api", "env")


async def setup_db():
    """initiate a postgresql connections pool"""
    pool = await asyncpg.create_pool(
        database=settings.get("api", "pg_name"),
        host=settings.get("api", "pg_host"),
        user=settings.get("api", "pg_user"),
        password=settings.get("api", "pg_password"),
        port=settings.get("api", "pg_port"),
        min_size=int(settings.get("api", "pg_max_connection")),
        max_size=int(settings.get("api", "pg_max_connection")),
    )
    controller.db = pool


def compute_note(
    value: float, vmin: float, vmax: float, order: str, upperlimit: float = None
):
    """Calcul de normalisation entre 0 et 5

    Returns
    -------
    None si max - min trop proche de 0 sinon la note
    """
    if vmax - vmin == 0:
        return None
    if upperlimit:
        vmax = upperlimit
        value = min(value, upperlimit)
    if order == "asc":
        return round(((5 / (vmax - vmin)) * (value - vmin)), 2)
    # 'desc'
    return round(((5 / (vmin - vmax)) * (value - vmax)), 2)


def compute_stats(values: list, order: str, upperlimit: float = None):
    """Calcul des statistiques"""
    if not values:
        values.append(0)
    vmin, vmax, medi = min(values), max(values), round(median(values), 2)
    median_note = compute_note(medi, vmin, vmax, order, upperlimit)
    return vmin, vmax, medi, median_note


async def work(region, session, terr, aid, order, name, year):
    schema = region.replace("-", "_")

    sql = f"select code from {schema}.region"
    rset = await controller.fetch(sql)
    code_region = rset[0]["code"]

    params = {
        "zone": "region",
        "maille": terr,
        "zone_id": code_region,
        "provenance": "integration_donnees_territorialsynthesis",
        "id_utilisateur": "1",
    }
    if year:
        params["annee"] = year

    url = f"http://{hostname}:{port}/api/" + region + f"/analysis/{aid}/data/"

    resp = await session.post(
        url,
        params=params,
    )
    if resp.status != 200:
        text = await resp.text()
        raise Exception(f"{resp.status}: {text}")
    resp = await resp.json()
    if len(resp) == 0:
        print(f"\t /!\\ empty result for {aid} with maille {terr}. /!\\")
        return None

    if name == "Part Enr/Consommation d'énergie":
        upperlimit = 100
    else:
        upperlimit = None
    values = {}
    for r in resp["map"]:
        values[r["code"]] = {"val": r["val"]}
        if "confidentiel" in r.keys() and r["confidentiel"]:
            values[r["code"]]["confidentiel"] = True
        else:
            values[r["code"]]["confidentiel"] = False

    nnvalues = [
        v["val"] for v in values.values() if v["val"] is not None
    ]  # on parcourt les valeurs en n'ajoutant pas les valeurs égales à None
    vmin, vmax, medi, median_note = compute_stats(nnvalues, order, upperlimit)

    notes = []
    for code in values:
        if values[code]["val"] is not None:
            if not values[code]["confidentiel"]:
                notes.append(
                    (
                        terr,
                        code,
                        aid,
                        values[code]["val"],
                        compute_note(
                            values[code]["val"], vmin, vmax, order, upperlimit
                        ),
                    )
                )
            elif values[code][
                "confidentiel"
            ]:  # on remplace les valeurs confidentielles par -9999
                notes.append(
                    (
                        terr,
                        code,
                        aid,
                        -9999,
                        compute_note(
                            values[code]["val"], vmin, vmax, order, upperlimit
                        ),
                    )
                )
    notes.extend(
        [(terr, tid, aid, None, None) for tid, val in values.items() if val is None]
    )

    await asyncio.gather(
        update_territorialsynthesis_stats(
            schema, terr, aid, vmin, vmax, medi, median_note
        ),
        update_territorialsynthesis_notes(schema, notes),
    )


async def territorialsynthesis():
    """Tâche de calcul des notes synthèse territoriale pour chaque territoire
    et indicateur.

    Le résultat est ensuite écrit dans la base de donnée dans les tables
    territorialsynthesis_stats et territorialsynthesis_notes.
    """
    sql = "select distinct id as nom from regions_configuration WHERE ui_show_cesba = true and env = $1 AND is_national = false"
    rset = await controller.fetch(sql, env)
    regions = [r["nom"] for r in rset]
    for region in regions:
        schema = region.replace("-", "_")
        # we select all the territoires that do exist
        sql = f"""SELECT DISTINCT z.nom FROM {schema}.zone z 
            LEFT JOIN {schema}.territoire t ON t.type_territoire = z.nom WHERE t.code IS NOT NULL"""
        rset = await controller.fetch(sql)
        territoires = [r["nom"] for r in rset]

        print("vidage des tables notes et stats du schéma " + schema)
        await controller.execute("truncate table " + schema + ".cesba_stats")
        await controller.execute("truncate table " + schema + ".cesba_notes")

        async with aiohttp.ClientSession() as session:
            analysis = await list_analysis(
                region, only_active=False, with_territorialsynthesis=True
            )
            analysis = [
                (a["id"], a["order"], a["only_for_zone"], a["nom"], a["years"])
                for a in analysis
            ]
            for aid, order, only_for, name, years in analysis:
                try:
                    # on itère sur chaque territoire
                    if not only_for:
                        await asyncio.gather(
                            *[
                                work(
                                    region,
                                    session,
                                    terr,
                                    aid,
                                    order,
                                    name,
                                    years[0] if years else None,
                                )
                                for terr in territoires
                            ]
                        )
                    else:
                        await work(
                            region,
                            session,
                            only_for,
                            aid,
                            order,
                            name,
                            years[0] if years else None,
                        )
                except Exception as e:
                    print("Couldn't fill analysis:")
                    print("\t-aid :", aid)
                    print("\t-order :", order)
                    print("\t-only_for :", only_for)
                    print("\t-name :", name)
                    print("\t-years :", years)
                    print("due to\n\t", e)
                    print("-" * 20)


def main():
    import asyncio

    loop = asyncio.get_event_loop()
    loop.run_until_complete(setup_db())
    loop.run_until_complete(territorialsynthesis())
    loop.close()
