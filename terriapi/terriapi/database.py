# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from contextvars import ContextVar

import asyncpg
from sanic import Blueprint
from sqlalchemy.engine import URL
from sqlalchemy.ext.asyncio import AsyncSession, async_sessionmaker, create_async_engine

from terriapi import controller, settings

bp_db = Blueprint("db")


@bp_db.before_server_start
async def setup_db_for_serve(app, loop):
    # if not app.test_mode:
    controller.db = await setup_db(app, loop, "server listener")


def create_pool(loop) -> asyncpg.Pool:
    """
    initiate postgresql connection
    """
    return asyncpg.create_pool(
        database=settings.get("api", "pg_name"),
        host=settings.get("api", "pg_host"),
        user=settings.get("api", "pg_user"),
        password=settings.get("api", "pg_password"),
        port=settings.get("api", "pg_port"),
        min_size=int(settings.get("api", "pg_max_connection")),
        max_size=int(settings.get("api", "pg_max_connection")),
        loop=loop,
        server_settings={"application_name": "terriapi"},
        max_inactive_connection_lifetime=10000,
    )


async def setup_db(app, loop, scope):
    """
    initiate postgresql connection
    """
    return await create_pool(loop)


@bp_db.after_server_stop
async def cleanup_db_for_serve(app, loop):
    if not app.test_mode:
        await cleanup_db(app, loop, "server listener")


async def cleanup_db(app, loop, scope):
    await controller.db.close()


# Also setup the sqlalchemy engine for the ORM (see https://sanic.dev/en/guide/how-to/orm.html#sqlalchemy)
database_url = URL.create(
    drivername="postgresql+asyncpg",
    username=settings.get("api", "pg_user"),
    password=settings.get("api", "pg_password"),
    host=settings.get("api", "pg_host"),
    port=int(settings.get("api", "pg_port")),
    database=settings.get("api", "pg_name"),
)

async_engine = create_async_engine(database_url, echo=True, future=True)
_sessionmaker = async_sessionmaker(async_engine)
_base_model_session_ctx = ContextVar("session")


@bp_db.middleware("request")
async def inject_session(request):
    request.ctx.session = _sessionmaker()
    request.ctx.session_ctx_token = _base_model_session_ctx.set(request.ctx.session)


@bp_db.middleware("response")
async def close_session(request, response):
    if hasattr(request.ctx, "session_ctx_token"):
        _base_model_session_ctx.reset(request.ctx.session_ctx_token)
        await request.ctx.session.close()
