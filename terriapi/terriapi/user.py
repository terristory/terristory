﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Blueprint for user
"""

import string
from json import loads as jsonloads
from random import choice, randint

import asyncpg
import requests
from sanic import Blueprint
from sanic.log import logger
from sanic.response import json
from sanic_jwt import exceptions
from sanic_jwt.decorators import protected

from terriapi import auth as user_auth
from terriapi import settings, utils
from terriapi.controller import fetch, regions_configuration, scenario
from terriapi.controller import user as controller
from terriapi.exceptions import RegionNotFound

bp_user = Blueprint(r"user", url_prefix=r"/api/user/<region>")


def generate_password():
    """
    Generate a new random password.

    Returns
    -------
    str
        new password between 8 and 16 characters.
    """
    characters = string.ascii_letters + string.digits
    return "".join(choice(characters) for x in range(randint(8, 16)))


@bp_user.route("/get")
@protected()
async def get_user(request, region):
    """
    Retrieve user information from login (given in raw_args) and region.

    Will return 404 error when login not given, user not found or 401 error when
    trying to access data from another user (and not admin).

    Parameters
    ----------
    region : str
        region key

    Returns
    -------
    json
        user data
    """
    login = request.args.get("login", None)
    if login is None:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    user = await controller.auth_user(request, region)
    data = await controller.user(region, login)
    if data is None:
        return json({"message": "Compte non trouvé."}, 404)
    authorized = user.est_admin or user.mail == data["mail"]
    if not authorized:
        return json(
            {"message": "Vous n'êtes pas autorisé·e à voir ces informations."},
            status=401,
        )
    return json(data)


@bp_user.route("/list")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_users(request, region):
    """
    Get user list in current region schema.

    Parameters
    ----------
    region : str
        region key

    Returns
    -------
    json
        list of dict users
    """
    data = await controller.users(region)
    return json(data)


@bp_user.route("/create", methods=["POST"])
@utils.is_valid_region()
async def create_user(request, region):
    """Create a new user

    You can create a new user via the registration form. The
    account is not enabled by default. It is the admin, after receiving an e-mail,
    who must validate the account (see the 'active' field).

    Parameters
    ----------
    region : str
        region key

    Returns
    -------
    str
        user login (mail)
    """

    region_configuration = await regions_configuration.get_configuration(
        region, with_private=True
    )
    contact_email_region = region_configuration[0]["contact_email"]
    contact_email_response_to = region_configuration[0]["contact_email_response_to"]
    region_label = region_configuration[0]["label"]
    if not request.json:
        return json({"message": "Il manque des données dans la requête"}, status=400)
    server_scheme = request.scheme
    server_host = request.host
    prenom = request.json["prenom"]
    nom = request.json["nom"]
    password = None
    mail = request.json["mail"]
    organisation = request.json["organisation"]
    fonction = request.json.get("fonction")
    territoire_predilection = request.json.get("territoire_predilection")
    utiliser_territoire_predilection = request.json.get(
        "utiliser_territoire_predilection"
    )
    code_postal = request.json.get("code_postal")
    rgpd = request.json.get("rgpd", False)
    captcha_token = request.json.get("captcha_token", None)

    try:
        await check_token_captcha(captcha_token)
    except ValueError as e:
        return json({"message": str(e)}, status=401)

    rgpd_checkbox = ""
    if rgpd and str(rgpd) == "True":
        # TODO AURA-EE à remplacer ?
        rgpd_checkbox = "L'utilisateur a accepté que les informations saisies soient exploitées par AURA-EE pour le recontacter."
    else:
        return json(
            {
                "user_id": mail,
                "message": "Il vous faut absolument accepter les conditions d'utilisation de vos données.",
            },
            400,
        )

    profil = "utilisateur"
    actif = False
    # on vérifie si l'utilsateur·rice n'existe pas déjà
    user = await controller.user(region, mail)
    if user is not None:
        return json({"user_id": mail, "message": "Ce compte existe déjà."}, 400)
    await controller.create_user(
        prenom,
        nom,
        password,
        mail,
        organisation,
        fonction,
        territoire_predilection,
        utiliser_territoire_predilection,
        code_postal,
        region,
        profil,
        actif,
        rgpd_checkbox,
    )

    prefered_territory = jsonloads(territoire_predilection)
    territoire = (
        prefered_territory["zoneType"] if "zoneType" in prefered_territory else "region"
    ) + (" n°" + prefered_territory["zoneId"] if "zoneId" in prefered_territory else "")

    subject = "[TerriSTORY] Demande d'activation de compte"
    content = f"""Bonjour,

Une demande d'inscription a récemment été effectuée sur {server_scheme}://{server_host}.

* Prénom : {prenom}
* Nom : {nom}
* Mail : {mail}
* Organisation : {organisation}
* Fonction : {fonction}
* Code postal : {code_postal}
* Territoire : {territoire}

Vous pouvez dès à présent valider ce compte depuis l'interface admin. Un mot de passe
sera généré et un mail sera envoyé à '{mail}'.

--
L'équipe TerriSTORY
    """
    status = await controller.send_mail(
        contact_email_region,
        region_label,
        contact_email_response_to,
        contact_email_region,
        subject,
        content,
        settings,
    )
    return json({"user_id": mail, "email_status": status})


@bp_user.route("/admin/create", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def admin_create_user(request, region):
    """Create a new user through admin panel. Can only be called by a regional admin.

    Require json parameters containing user information (prenom, nom, mail...).

    Parameters
    ----------
    region : str
        region key
    """
    if not request.json:
        return json({"message": "Il manque des données dans la requête"}, status=400)

    region_configuration = await regions_configuration.get_configuration(
        region, with_private=True
    )
    contact_email_region = region_configuration[0]["contact_email"]
    contact_email_response_to = region_configuration[0]["contact_email_response_to"]
    region_label = region_configuration[0]["label"]

    prenom = request.json["prenom"]
    nom = request.json["nom"]
    password = None
    mail = request.json["mail"]
    organisation = request.json["organisation"]
    fonction = request.json.get("fonction")
    territoire_predilection = request.json.get("territoire_predilection")
    utiliser_territoire_predilection = request.json.get(
        "utiliser_territoire_predilection"
    )
    code_postal = request.json.get("code_postal")
    profil = request.json.get("profil")
    actif = True if request.json["actif"] else False
    # on vérifie si l'utilsateur·rice n'existe pas déjà
    user = await controller.user(region, mail)
    if user is not None:
        return json({"user_id": mail, "message": "Ce compte existe déjà."}, 400)

    await controller.create_user(
        prenom,
        nom,
        password,
        mail,
        organisation,
        fonction,
        territoire_predilection,
        utiliser_territoire_predilection,
        code_postal,
        region,
        profil,
        actif,
        "Accord à demander à l'utilisateur",
    )

    password = generate_password()
    mail_status = "non envoyé"
    if actif:
        await controller.activate_user(region, mail, password)
        subject = "[TerriSTORY] Compte activé"
        content = """
Bonjour,

Votre compte est désormais actif sur {server_scheme}://{server_host}.

* login : {mail}
* mot de passe : {password}

Merci de votre confiance.
--
L'équipe TerriSTORY
        """.format(
            mail=mail,
            password=password,
            server_scheme=request.scheme,
            server_host=request.host,
        )
        mail_status = await controller.send_mail(
            contact_email_region,
            region_label,
            contact_email_response_to,
            mail,
            subject,
            content,
            settings,
        )
    return json({"user_id": mail, "status": "ok", "email_status": mail_status})


@bp_user.route("/activate", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def activate(request, region):
    """Enable a user account and send an email to the user containing its new password.

    Parameters
    ----------
    region : str
        region key
    """

    region_configuration = await regions_configuration.get_configuration(
        region, with_private=True
    )
    contact_email_region = region_configuration[0]["contact_email"]
    contact_email_response_to = region_configuration[0]["contact_email_response_to"]
    region_label = region_configuration[0]["label"]
    if not request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    account = request.json["login"]
    # on vérifie si l'utilsateur·rice existe
    user = await controller.user(region, account)
    if user is None:
        return json({"user_id": account, "message": "Compte non trouvé."}, 404)
    password = generate_password()
    await controller.activate_user(region, account, password)
    subject = "[TerriSTORY] Compte activé"
    content = """
Bonjour,

Votre compte est désormais actif sur {server_scheme}://{server_host}.

* login : {mail}
* mot de passe : {password}

Merci de votre confiance.
--
L'équipe TerriSTORY
    """.format(
        mail=account,
        password=password,
        server_scheme=request.scheme,
        server_host=request.host,
    )
    status = await controller.send_mail(
        contact_email_region,
        region_label,
        contact_email_response_to,
        account,
        subject,
        content,
        settings,
    )
    return json({"user_id": account, "status": "ok", "email_status": status})


@bp_user.route("/deactivate", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def deactivate(request, region):
    """
    Disable a user account.

    Parameters
    ----------
    region : str
        region key
    """
    if not request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    account = request.json["login"]
    # on vérifie si l'utilsateur·rice existe
    target_user = await controller.user(region, account)
    if target_user is None:
        return json({"user_id": account, "message": "Compte non trouvé."}, 404)
    current_user = await controller.auth_user(request, region)
    if not await utils.check_is_admin_in_region(current_user, target_user["region"]):
        return json(
            {"message": "Desactivation d'utilisateur d'une autre région interdite"},
            401,
        )
    await controller.deactivate_user(region, account)
    return json({"user_id": account, "status": "inactif"})


@bp_user.route("/update", methods=["PUT"])
@utils.is_valid_region()
@protected()
async def update_user(request, region):
    """
    Update a user account.

    Parameters
    ----------
    region : str
        region key
    """
    if not request.json or "login" not in request.json:
        return json({"message": "Il manque des données dans la requête."}, 400)
    account = request.json["login"].lower()

    modified_user = await controller.user(region, account)
    current_user = await controller.auth_user(request, region)
    if modified_user is None:
        return json({"message": "Compte non trouvé."}, 404)
    if current_user.mail != account and not await utils.check_is_admin_in_region(
        current_user, region
    ):
        return json({"message": "Modification d'un autre utilisateur interdite"}, 401)

    # Get params
    userdata = {}
    userdata["prenom"] = request.json.get("prenom", None)
    userdata["nom"] = request.json.get("nom", None)
    userdata["organisation"] = request.json.get("organisation", None)
    userdata["fonction"] = request.json.get("fonction", None)
    userdata["territoire_predilection"] = request.json.get(
        "territoire_predilection", None
    )
    userdata["utiliser_territoire_predilection"] = request.json.get(
        "utiliser_territoire_predilection", None
    )
    userdata["code_postal"] = request.json.get("code_postal", None)
    if current_user.est_admin:
        # Special params only modifiable by admins
        userdata["publication"] = request.json.get("publication", False)

    await controller.update_user(region, account, userdata)
    return json({"user_id": account, "message": "Utilisateur modifié"})


@bp_user.route("/delete/<login>/", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_user(request, region: str, login: str):
    """
    Delete a user account.

    Parameters
    ----------
    region : str
        region key
    """
    # on vérifie si l'utilsateur·rice existe
    user = await controller.user(region, login)
    if user is None:
        return json({"user_id": login, "message": "Compte non trouvé."}, 404)
    current_user = await controller.auth_user(request, region)
    if not await utils.check_is_admin_in_region(current_user, user["region"]):
        return json(
            {"message": "Suppression d'utilisateur d'une autre région interdite"},
            401,
        )
    await controller.delete_user(region, login)
    return json({"user_id": login, "status": "supprimé"})


@bp_user.route("/password", methods=["POST"])
@utils.is_valid_region()
async def regenerate_password(request, region):
    """
    Request a new password for a user.

    Send a mail with new password saved.

    Parameters
    ----------
    region : str
        region key
    """
    if not request.json or "mail" not in request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    region_configuration = await regions_configuration.get_configuration(
        region, with_private=True
    )
    contact_email_region = region_configuration[0]["contact_email"]
    contact_email_response_to = region_configuration[0]["contact_email_response_to"]
    region_label = region_configuration[0]["label"]
    account = request.json["mail"]
    if not await controller.is_active_user(region, account):
        return json({"message": "Compte non trouvé ou non actif."}, 404)
    password = generate_password()
    await controller.set_user_password(region, account, password)
    subject = "[TerriSTORY] Mot de passe oublié"
    content = """
Bonjour,

Vous avez demandé un nouveau mot de passe pour https://terristory.fr.

* mot de passe : {password}

Merci de votre confiance.
--
L'équipe TerriSTORY
    """.format(
        password=password
    )
    status = await controller.send_mail(
        contact_email_region,
        region_label,
        contact_email_response_to,
        account,
        subject,
        content,
        settings,
    )
    return json({"user_id": account, "status": "ok", "email_status": status})


@bp_user.route("/setpassword", methods=["POST"])
@utils.is_valid_region()
@protected()
async def set_password(request, region):
    """
    Allow the user to assign a new password to its account.

    Parameters
    ----------
    region : str
        region key
    """
    if not request.json or "new_password" not in request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    new_password = request.json["new_password"]
    if not new_password:
        return json({"message": "Mot de passe vide interdit."}, status=400)

    user = await controller.auth_user(request, region)
    await controller.set_user_password(region, user.mail, new_password)
    return json({"user_id": user.mail, "status": "ok"})


@bp_user.route("/scenario", methods=["POST"])
@utils.is_valid_region(True)
@protected()
async def create_scenario(request, region):
    """
    Create a scenario for the authenticated user.

    Parameters
    ----------
    region : str
        region key
    """
    region_configuration = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    is_national = region_configuration[0]["is_national"]
    if not request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    titre = request.json["titre"]
    description = request.json.get("description")
    # récupération des paramètres d'action
    action_params = await controller.fetch(
        """SELECT s.id, CONCAT(s.action_number, '_', s.nom) as nom 
        FROM strategie_territoire.action_subactions s
        LEFT JOIN strategie_territoire.action_subactions_status sb ON sb.action_number = s.action_number AND sb.nom = s.nom
        WHERE sb.region = $1 AND sb.enabled = true""",
        region,
    )
    params = {x["id"]: request.json.get(x["nom"]) for x in action_params}
    zone_type = request.json["zone_type"]
    zone_id = request.json["zone_id"]
    nom_territoire = request.json["nom_territoire"]
    reference_year = request.json.get("reference_year")
    actions = request.json["actions"]
    advanced_params = request.json["advanced"]
    trajectoires_cibles = request.json["trajectoires"]
    user = await controller.auth_user(request, region)
    mail = user.mail

    try:
        scenario_id = await scenario.create_scenario(
            titre,
            mail,
            description,
            zone_type,
            zone_id,
            nom_territoire,
            region,
            is_national,
            reference_year,
            actions,
            params,
            advanced_params,
            trajectoires_cibles,
        )
    except asyncpg.exceptions.PostgresError as e:
        return json({"message": f"La transaction a échoué ({e.message}) !"}, status=400)
    if not scenario_id:
        return json({"message": "La transaction a échoué !"}, status=400)
    return json({"scenario_id": scenario_id})


@bp_user.route("/scenario/<scenario_id:int>", methods=["PUT"], name="update_scenario")
@utils.is_valid_region(True)
@protected()
async def update_scenario(request, region, scenario_id):
    """
    Update a specific scenario.

    Parameters
    ----------
    region : str
        region key
    scenario_id : int
        scenario unique identifier
    """
    if not request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    titre = request.json["titre"]
    description = request.json.get("description")
    reference_year = request.json.get("reference_year")
    actions = request.json["actions"]
    advanced_params = request.json["advanced"]
    trajectoires_cibles = request.json["trajectoires"]
    # récupération des paramètres d'action
    action_params = await controller.fetch(
        """SELECT s.id, CONCAT(s.action_number, '_', s.nom) as nom
        FROM strategie_territoire.action_subactions s
        LEFT JOIN strategie_territoire.action_subactions_status sb ON sb.action_number = s.action_number AND sb.nom = s.nom
        WHERE sb.region = $1 AND sb.enabled = true""",
        region,
    )
    params = {x["id"]: request.json.get(x["nom"]) for x in action_params}
    user = await controller.auth_user(request, region)
    data = await scenario.verifie_scenario(scenario_id)
    if data is None:
        return json({"message": "Scénario non trouvé."}, 404)
    if data["mail"] != user.mail or data["region"] != region:
        return json(
            {
                "user_id": user.mail,
                "message": "Vous n'êtes pas autorisé·e à modifier ces informations.",
            },
            401,
        )
    await scenario.update_scenario(
        scenario_id,
        titre,
        description,
        reference_year,
        actions,
        params,
        advanced_params,
        trajectoires_cibles,
    )
    return json({"scenario_id": scenario_id, "status": "mis à jour"})


@bp_user.route("/scenario/<scenario_id:int>")
@utils.is_valid_region(True)
@protected()
async def get_scenario(request, region, scenario_id):
    """
    Retrieve data and action parameters associated with a saved scenario.

    Parameters
    ----------
    region : str
        region key
    scenario_id : int
        scenario unique identifier
    """
    user = await controller.auth_user(request, region)
    data = await scenario.get_scenario(scenario_id)
    if data is None:
        return json({"message": "Scénario non trouvé."}, 404)
    partage_desactive = (
        data["partage_desactive"] if data["partage_desactive"] is not None else []
    )
    if user.mail in partage_desactive:
        return json({"message": "Scénario non trouvé."}, 404)
    # Les données sont visibles pour l'auteur de la stratégie, quand elle est
    # publique ou qu'elle a été partagée pour l'utilisateur·rice authentifié·e
    partage = data["partage"] if data["partage"] is not None else []
    if data["publique"] or user.mail in partage:
        return json(data)
    if data["mail"] != user.mail:
        return json(
            {
                "user_id": user.mail,
                "message": "Vous n'êtes pas autorisé·e à voir ces informations.",
            },
            401,
        )
    return json(data)


@bp_user.route("/scenario/list")
@utils.is_valid_region(True)
@protected()
async def get_user_scenario(request, region):
    """
    Retrieve all scenarios linked to a user.

    Parameters
    ----------
    region : str
        region key
    """
    user = await controller.auth_user(request, region)
    data = await scenario.user_scenario(region, user.mail)
    return json(data)


@bp_user.route("/scenario/<scenario_id:int>", methods=["DELETE"])
@utils.is_valid_region(True)
@protected()
async def delete_scenario(request, region, scenario_id):
    """
    Delete a specific scenario from current user scenarios.

    Parameters
    ----------
    region : str
        region key
    scenario_id : int
        scenario unique identifier
    """
    user = await controller.auth_user(request, region)
    data = await scenario.verifie_scenario(scenario_id)
    if data is None:
        return json({"message": "Scénario non trouvé."}, 404)
    # La suppression des données ne peuvent être effectuées que par l'auteur du scénario.
    if data["mail"] != user.mail or data["region"] != region:
        return json(
            {
                "user_id": user.mail,
                "message": "Vous n'êtes pas autorisé·e à supprimer ces informations.",
            },
            401,
        )
    await scenario.delete_scenario(region, scenario_id)
    return json({"scenario_id": scenario_id, "status": "supprimé"})


@bp_user.route("/scenario/<scenario_id:int>/partage", methods=["POST"])
@utils.is_valid_region()
@protected()
async def partager_scenario(request, region, scenario_id):
    """
    Share a scenario with other users inside current region.

    Parameters
    ----------
    region : str
        region key
    scenario_id : int
        scenario unique identifier
    """
    if not request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)
    region_configuration = await regions_configuration.get_configuration(
        region, with_private=True
    )
    contact_email_region = region_configuration[0]["contact_email"]
    contact_email_response_to = region_configuration[0]["contact_email_response_to"]
    region_label = region_configuration[0]["label"]
    # Nettoyage
    emails = [
        email.strip().lower()
        for email in request.json["emails"].split()
        if email.strip()
    ]
    user = await controller.auth_user(request, region)
    data = await scenario.verifie_scenario(scenario_id)
    if data is None:
        return json({"message": "Scénario non trouvé."}, 404)
    # Le partage du scénario ne peut être effectué que par l'auteur du scénario.
    if data["mail"] != user.mail or data["region"] != region:
        return json(
            {
                "user_id": user.mail,
                "message": "Vous n'êtes pas autorisé·e à partager ce scénario.",
            },
            401,
        )

    # On vérifie que chaque email correspond bien à un login
    logins = []
    users = await controller.users(region)
    logins = [user["mail"].lower() for user in users]
    for email in emails:
        if not email in logins:
            return json(
                {
                    "message": "Des emails sont inexistants dans la base des utilisateurs"
                },
                status=400,
            )

    await scenario.share_scenario(region, scenario_id, emails)

    url = (
        "https://"
        + region_configuration[0]["url"]
        + "."
        + region_configuration[0]["env"]
        + ".terristory.fr"
    )
    if region_configuration[0]["env"] == "prod":
        url = "https://" + region_configuration[0]["url"] + ".terristory.fr"

    # On envoie un email à chaque bénéficiaire
    for email in emails:
        subject = "[TerriSTORY] Partage de stratégie"
        content = f"""Bonjour,

Un partage de stratégie vient d'être effectué sur la plateforme TerriSTORY par {user.mail}

Vous pouvez dès à présent la consulter dans votre espace personnel : {url}.

--
L'équipe TerriSTORY
    """

        status = await controller.send_mail(
            contact_email_region,
            region_label,
            contact_email_response_to,
            email,
            subject,
            content,
            settings,
        )
    # TODO(dag): ajouter le statut d'envoi du mail dans la réponse ?
    return json({"status": "ok"})


@bp_user.route("/scenario/<scenario_id:int>/desactive", methods=["POST"])
@utils.is_valid_region()
@protected()
async def desactiver_scenario(request, region, scenario_id):
    """Disable a scenario that has been shared to a user by the scenario owner.

    The user will not have access to the scenario anymore.

    Parameters
    ----------
    region : str
        region key
    scenario_id : int
        scenario unique identifier"""
    user = await controller.auth_user(request, region)
    data = await scenario.verifie_scenario(scenario_id)

    if data is None:
        return json({"message": "Scénario non trouvé."}, 404)

    await scenario.disable_scenario(region, user.mail, scenario_id)
    return json({"status": "ok"})


@bp_user.route("/scenario/<scenario_id:int>/publier", methods=["POST"])
@utils.is_valid_region()
@protected()
async def publier_scenario(request, region, scenario_id):
    """Publish a scenario to all visitors.

    Parameters
    ----------
    region : str
        region key
    scenario_id : int
        scenario unique identifier
    """
    if not request.json:
        return json({"message": "Il manque des données dans la requête."}, status=400)

    user = await controller.auth_user(request, region)
    # La publication du scénario ne peut être effectué que par un utilisateur ayant les droits
    if not user.publication:
        return json(
            {
                "user_id": user.mail,
                "message": "Vous n'êtes pas autorisé·e à publier ce scénario.",
            },
            401,
        )

    data = await scenario.verifie_scenario(scenario_id)
    if data is None:
        return json({"message": "Scénario non trouvé."}, 404)
    # Le partage du scénario ne peut être effectué que par l'auteur du scénario.
    if data["mail"] != user.mail or data["region"] != region:
        return json(
            {
                "user_id": user.mail,
                "message": "Vous n'êtes pas autorisé·e à partager ce scénario.",
            },
            401,
        )

    # update utilisateur_scenario set partage_desactive = ;
    await scenario.change_public_state_scenario(
        region, scenario_id, request.json["publique"]
    )

    return json({"status": "ok"})


async def check_token_captcha(token):
    if token is None:
        raise ValueError("Il manque le token dans la requête.")
    captcha_secret_key = settings.get("api", "captcha_secret_key")
    # Sending secret key and response token to Google Recaptcha API for authentication.
    try:
        response = requests.post(
            f"https://www.google.com/recaptcha/api/siteverify?secret={captcha_secret_key}&response={token}"
        )

        # Check response status and send back to the client-side
        if response.json()["success"]:
            return True
        else:
            raise Exception("validation échouée")
    except Exception as error:
        logger.error("Error during reCAPTCHA verification: %s", error)
        # Handle any errors that occur during the reCAPTCHA verification process
        raise ValueError("Token non valide (erreur rencontrée).")


@bp_user.route("/get/user/regions", methods=["POST"])
async def authenticate_user(request, region):
    try:
        res = await user_auth.authenticate(request, "national")
    except exceptions.AuthenticationFailed as e:
        return json(
            {
                "message": "Cette action n'est pas possible.",
            },
            401,
        )
    regions_associated = await controller.get_regions_national_user(
        res["user_id"]["mail"]
    )
    return json(list(map(dict, regions_associated)))
