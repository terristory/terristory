﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""
Module API de l'application TerriStory
"""
import configparser
import importlib.metadata
import os
import sys
from pathlib import Path

__version__ = importlib.metadata.version("terriapi")

here = Path(__file__).parent

settings = configparser.ConfigParser(allow_no_value=True)
here = Path(__file__).parent
# read first in user home directory if exists
home = Path.home() / ".terriapi.ini"

if "TERRIAPI_SETTINGS" in os.environ:
    filename = Path(os.environ["TERRIAPI_SETTINGS"])
    if not filename.is_file():
        print(
            "ERREUR: la variable d'environnement TERRIAPI_SETTINGS ne référence pas un fichier: {}".format(
                str(filename)
            )
        )
        sys.exit(1)
    settings.read(filename)
elif home.exists():
    settings.read(home)
elif (here / "terriapi.ini").exists():
    settings.read(here / "terriapi.ini")
else:
    print(
        "ERREUR: aucun fichier de configuration .terriapi.ini dans {}".format(
            Path.home()
        )
    )
    print("ERREUR: aucun fichier de configuration terriapi.ini dans {}".format(here))
    print(
        "Choisissez l'un des deux emplacements et recopier le fichier d'exemple terriapi.ini.sample pour démarrer"
    )
    sys.exit(1)
