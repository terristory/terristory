﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout_colonne_ordre_affichage_table_regions_configuration

Revision ID: 0910f94f64e9
Revises: d61cad511958
Create Date: 2022-01-24 10:38:44.154032

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import Column, Integer

# revision identifiers, used by Alembic.
revision = "0910f94f64e9"
down_revision = "d61cad511958"
branch_labels = None
depends_on = None
SCHEMA = "public"


def upgrade():
    op.add_column(
        "regions_configuration", Column("ordre_affichage", Integer), schema=SCHEMA
    )


def downgrade():
    with op.batch_alter_table("regions_configuration", schema=SCHEMA) as batch_op:
        batch_op.drop_column("ordre_affichage")
