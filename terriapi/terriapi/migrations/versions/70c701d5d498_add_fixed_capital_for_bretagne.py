"""add fixed capital for bretagne

Revision ID: 70c701d5d498
Revises: 8b0762ae50d7
Create Date: 2023-11-24 17:04:03.600139

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "70c701d5d498"
down_revision = "8b0762ae50d7"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        f"""
    DELETE FROM strategie_territoire.fixed_capital WHERE region ='bretagne';
    INSERT INTO strategie_territoire.fixed_capital(category,region,value) VALUES('agriculture', 'bretagne', 10.160);
    INSERT INTO strategie_territoire.fixed_capital(category,region,value) VALUES('industry', 'bretagne', 28.358);
    UPDATE strategie_territoire.fixed_capital SET value=8.364 WHERE category='agriculture' AND region='paysdelaloire';
    UPDATE strategie_territoire.fixed_capital SET value=43.307 WHERE category='industry' AND region='paysdelaloire' """
    )


def downgrade():
    op.execute(
        f"""
    DELETE FROM strategie_territoire.fixed_capital WHERE region='bretagne';"""
    )
