﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""add columns to the pcaet ademe table for pollutants

Revision ID: 8ee824081b8c
Revises: 3d4ec5a57c55
Create Date: 2022-11-21 20:28:45.607947

"""
from alembic import op
from sqlalchemy import Column, Numeric

# revision identifiers, used by Alembic.
revision = "8ee824081b8c"
down_revision = "3d4ec5a57c55"
branch_labels = None
depends_on = None

SCHEMA = "public"
TABLENAME = "pcaet_ademe"
COLUMNAME = [
    "pm10_annee_ref",
    "pm10_annee_2021",
    "pm10_annee_2026",
    "pm10_annee_2030",
    "pm10_annee_2050",
    "pm25_annee_ref",
    "pm25_annee_2021",
    "pm25_annee_2026",
    "pm25_annee_2030",
    "pm25_annee_2050",
    "nox_annee_ref",
    "nox_annee_2021",
    "nox_annee_2026",
    "nox_annee_2030",
    "nox_annee_2050",
    "sox_annee_ref",
    "sox_annee_2021",
    "sox_annee_2026",
    "sox_annee_2030",
    "sox_annee_2050",
    "covnm_annee_ref",
    "covnm_annee_2021",
    "covnm_annee_2026",
    "covnm_annee_2030",
    "covnm_annee_2050",
    "nh3_annee_ref",
    "nh3_annee_2021",
    "nh3_annee_2026",
    "nh3_annee_2030",
    "nh3_annee_2050",
]


def upgrade():
    for x in COLUMNAME:
        op.add_column(TABLENAME, Column(x, Numeric), schema=SCHEMA)


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        for x in COLUMNAME:
            batch_op.drop_column(x)
