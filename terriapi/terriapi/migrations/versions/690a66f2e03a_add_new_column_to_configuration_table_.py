"""add new column to configuration table for newsletter

Revision ID: 690a66f2e03a
Revises: cff99cc4d3ad
Create Date: 2023-12-20 17:47:34.946415

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "690a66f2e03a"
down_revision = "cff99cc4d3ad"
branch_labels = None
depends_on = None
SCHEMA = "public"


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("enable_newsletter_national", sa.Boolean, server_default="f"),
        schema=SCHEMA,
    )
    op.add_column(
        "regions_configuration",
        sa.Column("enable_newsletter_regional", sa.Boolean, server_default="f"),
        schema=SCHEMA,
    )


def downgrade():
    with op.batch_alter_table("regions_configuration", schema=SCHEMA) as batch_op:
        batch_op.drop_column("enable_newsletter_national")
        batch_op.drop_column("enable_newsletter_regional")
