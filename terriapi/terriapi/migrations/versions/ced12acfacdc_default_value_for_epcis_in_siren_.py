"""Default value for epcis in siren_assignment

Revision ID: ced12acfacdc
Revises: 04f58c965cc4
Create Date: 2024-04-18 13:56:39.569717

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "ced12acfacdc"
down_revision = "04f58c965cc4"
branch_labels = None
depends_on = None

regions = [
    "auvergne_rhone_alpes",
    "occitanie",
    "paysdelaloire",
    "bretagne",
    "nouvelle_aquitaine",
    "corse",
]


def upgrade():
    for region in regions:
        op.execute(
            f"""
            UPDATE {region}.siren_assignment SET epcis='{{}}' WHERE epcis IS NULL;
            ALTER TABLE {region}.siren_assignment ALTER COLUMN epcis SET DEFAULT '{{}}';
            """
        )


def downgrade():
    for region in regions:
        op.execute(
            f"""
            ALTER TABLE {region}.siren_assignment ALTER COLUMN epcis DROP DEFAULT;
            """
        )
