﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Réplication Bretagne et Pays de la Loire

Revision ID: 6edade4bfef6
Revises: 032723541ee2
Create Date: 2021-05-10 17:08:32.496148

"""
from alembic import op
from sqlalchemy import Boolean, Column, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "6edade4bfef6"
down_revision = "032723541ee2"
branch_labels = None
depends_on = None


def creer_schema(schema):
    op.execute(f"create schema {schema};")


def supprimer_schema(schema):
    op.execute(f"drop schema {schema} cascade;")


def creer_table_poi(schema):
    op.create_table(
        "layer",
        Column("id", Integer, primary_key=True, comment="Identifiant"),
        Column(
            "id_utilisateur",
            Integer,
            ForeignKey("consultations.ip_localisation.id", ondelete="CASCADE"),
        ),
        Column(
            "nom", String, comment="Nom de la couche (nom de la table associée en base)"
        ),
        Column(
            "label",
            String,
            comment="Libellé de la couche (telle qu'elle apparaît dans l'application)",
        ),
        Column("couleur", String, comment="Couleur du figuré de l'installation"),
        Column(
            "modifiable",
            Boolean,
            nullable=False,
            comment="Modifiable par les utilisateurs.rices ?",
        ),
        Column(
            "rubrique", String, comment="Rubrique à laquelle appartient l'installation"
        ),
        schema=schema,
    )


def creer_table_zone(schema):
    op.create_table(
        "zone",
        Column("nom", String, comment="Nom du type de territoire sélectionnée"),
        Column("maille", String, comment="Maille sélectionnée"),
        Column("libelle", String, comment="Libellé"),
        schema=schema,
    )


def create_table_chiffres_cle(schema):
    op.create_table(
        "chiffres_cle",
        Column(
            "ordre", Integer, comment="Ordre dans lequel on affiche les chiffres clés"
        ),
        Column(
            "nom_analyse",
            String,
            comment="Nom associé au chiffre (ex : Facture énergétique, taux d'étalement urbain etc.)",
        ),
        Column(
            "donnee",
            String,
            comment="Formule de calcul du chiffre (tables impliquées dans le calcul)",
        ),
        Column(
            "nom_table_analyse",
            String,
            comment="Nom de la table principale d'où on extrait les calculs",
        ),
        Column("unite", String, comment="Unité du chiffre affiché"),
        schema=schema,
    )


def upgrade():
    creer_schema("bretagne")
    creer_schema("bretagne_poi")
    creer_schema("pays_de_la_loire")
    creer_schema("pays_de_la_loire_poi")
    creer_table_poi("bretagne_poi")
    creer_table_poi("pays_de_la_loire_poi")
    creer_table_zone("bretagne")
    creer_table_zone("pays_de_la_loire")
    create_table_chiffres_cle("pays_de_la_loire")
    create_table_chiffres_cle("bretagne")


def downgrade():
    supprimer_schema("bretagne")
    supprimer_schema("bretagne_poi")
    supprimer_schema("pays_de_la_loire")
    supprimer_schema("pays_de_la_loire_poi")
