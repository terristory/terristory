"""Replace 'Tout appareil' by 'Chaudière' in conversion actions

Revision ID: 93a34c1d6171
Revises: 09f8035a6b24
Create Date: 2024-06-14 11:57:15.610214

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "93a34c1d6171"
down_revision = "09f8035a6b24"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        UPDATE strategie_territoire.params_facteur_emission_polluants_ges_conso_atmo
        SET type_veh_cha='Chaudière' WHERE type_veh_cha='Tout appareil';
        """
    )


def downgrade():
    op.execute(
        """
        UPDATE strategie_territoire.params_facteur_emission_polluants_ges_conso_atmo
        SET type_veh_cha='Tout appareil'
        WHERE secteur='Résidentiel' AND (type_energie='Gaz' OR type_energie='Fioul');
        """
    )
