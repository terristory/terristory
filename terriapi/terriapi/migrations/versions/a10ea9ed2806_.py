"""empty message

Revision ID: a10ea9ed2806
Revises: cf081753294f, 4f675aa87b32
Create Date: 2023-06-23 16:26:56.843795

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a10ea9ed2806"
down_revision = ("cf081753294f", "4f675aa87b32")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
