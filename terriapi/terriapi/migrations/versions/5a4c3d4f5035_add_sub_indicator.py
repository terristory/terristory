"""add sub-indicator

Revision ID: 5a4c3d4f5035
Revises: f43044ecf4b1
Create Date: 2024-01-29 11:52:30.192058

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5a4c3d4f5035"
down_revision = "f43044ecf4b1"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """         
        CREATE TABLE IF NOT EXISTS meta.sub_indicator(
            indicator int PRIMARY KEY,
            parent int ,
            category varchar,
            modality varchar,
            modality_id int,
            region varchar,
            FOREIGN KEY(indicator) REFERENCES meta.indicateur(id) ON DELETE CASCADE,
            FOREIGN KEY(parent) REFERENCES meta.indicateur(id) ON DELETE CASCADE,
            FOREIGN KEY(category, modality_id, region) REFERENCES meta.categorie(nom, modalite_id, region)
            ON UPDATE CASCADE);
        """
    )


def downgrade():
    op.drop_table("sub_indicator", schema="meta")
