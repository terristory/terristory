"""Add region and relative_value to pcaet

Revision ID: 3d78e32bc241
Revises: 0cbc76d27422
Create Date: 2024-11-13 18:37:38.913080

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "3d78e32bc241"
down_revision = "0cbc76d27422"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        DELETE FROM pcaet;
        ALTER TABLE pcaet RENAME COLUMN variation TO relative_value;
        ALTER TABLE pcaet ADD region VARCHAR NOT NULL;
        ALTER TABLE pcaet DROP CONSTRAINT pcaet_pkey;
        ALTER TABLE pcaet ADD CONSTRAINT pcaet_pkey PRIMARY KEY (
            siren, trajectory, category, year, region
        );
        """
    )


def downgrade():
    op.execute(
        """
        DELETE FROM pcaet;
        ALTER TABLE pcaet DROP CONSTRAINT pcaet_pkey;
        ALTER TABLE pcaet ADD CONSTRAINT pcaet_pkey PRIMARY KEY (
            siren, trajectory, category, year
        );
        ALTER TABLE pcaet DROP region;
        ALTER TABLE pcaet RENAME COLUMN relative_value TO variation;
        """
    )
