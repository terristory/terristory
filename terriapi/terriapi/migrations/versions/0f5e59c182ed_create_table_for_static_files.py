"""Create table for static files

Revision ID: 0f5e59c182ed
Revises: cccdea3d5af9
Create Date: 2023-01-25 17:43:31.944376

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0f5e59c182ed"
down_revision = "cccdea3d5af9"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "static_files",
        sa.Column("file_id", sa.Integer, primary_key=True, comment="ID"),
        sa.Column("name", sa.String, comment="Nom du fichier"),
        sa.Column("file_name", sa.String, comment="Nom du fichier dans le dossier"),
        sa.Column("region", sa.String, comment="Région"),
        sa.Column(
            "update_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=True,
            comment="Date de mise à jour",
        ),
        schema="meta",
    )


def downgrade():
    op.execute("""drop table meta.static_files""")
