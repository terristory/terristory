﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajouter la colonne data_type dans meta.indicateur et meta.chart

Revision ID: 669f2e4ba1eb
Revises: 35dec2548360
Create Date: 2021-08-27 11:54:32.518462

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "669f2e4ba1eb"
down_revision = "35dec2548360"
branch_labels = None
depends_on = None
SCHEMA = "meta"


def ajouter_colonne(TABLENAME):
    op.add_column(TABLENAME, Column("data_type", String), schema=SCHEMA)


def upgrade():
    ajouter_colonne("indicateur")
    ajouter_colonne("chart")


def downgrade():
    with op.batch_alter_table("indicateur", schema=SCHEMA) as batch_op:
        batch_op.drop_column("data_type")

    with op.batch_alter_table("chart", schema=SCHEMA) as batch_op:
        batch_op.drop_column("data_type")
