"""empty message

Revision ID: 3ed0009af919
Revises: 901e826b3c6f, fc6b4a6d6f90
Create Date: 2024-08-02 18:10:52.265244

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "3ed0009af919"
down_revision = ("901e826b3c6f", "fc6b4a6d6f90")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
