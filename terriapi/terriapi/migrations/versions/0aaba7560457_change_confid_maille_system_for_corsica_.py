"""Change confid_maille system for corsica also

Revision ID: 0aaba7560457
Revises: 8493c5605a73
Create Date: 2023-08-18 14:43:01.556231

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0aaba7560457"
down_revision = "8493c5605a73"
branch_labels = None
depends_on = None

# redo e6dc37f046b0 for Corsica region

regions = [
    "corse",
]


def upgrade():
    for region in regions:
        schema = region.replace("-", "_")
        with op.batch_alter_table("confid_maille", schema=schema) as batch_op:
            batch_op.add_column(
                sa.Column(
                    "secteur_column",
                    sa.String(),
                    nullable=False,
                    server_default="secteur",
                )
            )
            batch_op.add_column(
                sa.Column(
                    "energie_column",
                    sa.String(),
                    nullable=False,
                    server_default="energie",
                )
            )
            batch_op.drop_column("facture_secteur")
            batch_op.drop_column("facture_energie")


def downgrade():
    for region in regions:
        schema = region.replace("-", "_")
        with op.batch_alter_table("confid_maille", schema=schema) as batch_op:
            batch_op.drop_column("secteur_column")
            batch_op.drop_column("energie_column")
            batch_op.add_column(sa.Column("facture_secteur", sa.String()))
            batch_op.add_column(sa.Column("facture_energie", sa.String()))
        op.execute(
            f"UPDATE {schema}.confid_maille SET facture_secteur = secteur, facture_energie = energie"
        )
