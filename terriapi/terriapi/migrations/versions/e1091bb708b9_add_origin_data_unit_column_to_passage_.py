"""add origin data unit column to passage table

Revision ID: e1091bb708b9
Revises: 499642eaaf0a
Create Date: 2023-11-20 17:33:22.037822

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e1091bb708b9"
down_revision = "499642eaaf0a"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
            ALTER TABLE strategie_territoire.passage_table ADD COLUMN data_unit VARCHAR;
           """
    )
    for region in ["auvergne-rhone-alpes", "occitanie", "paysdelaloire"]:
        op.execute(
            f"""
            UPDATE strategie_territoire.passage_table set data_unit = 'kteqCO2' where region='{region}' and key like 'DataSet.EMISSIONS%';
            UPDATE strategie_territoire.passage_table set data_unit = 'GWh' where region='{region}' and key like 'DataSet.CONSUMPTION%';
            UPDATE strategie_territoire.passage_table set data_unit = '€' where region='{region}' and key like 'DataSet.ENERGY_BILL%';
        """
        )
    op.execute(
        """
        UPDATE strategie_territoire.passage_table set data_unit = 't' where region='auvergne-rhone-alpes' and key like 'DataSet.POLLUTANT%';
        UPDATE simulator.simulator_impact SET unit = 't' WHERE type='impact-atmo';
    """
    )

    op.execute(
        """
        UPDATE strategie_territoire.passage_table set data_unit = 'kteqCO2' where region='nouvelle-aquitaine' and key like 'DataSet.EMISSIONS%';
        UPDATE strategie_territoire.passage_table set data_unit = 'GWh' where region='nouvelle-aquitaine' and key like 'DataSet.CONSUMPTION%';
        UPDATE strategie_territoire.passage_table set data_unit = 'M€' where region='nouvelle-aquitaine' and key like 'DataSet.ENERGY_BILL%';
    """
    )

    op.execute(
        """
        UPDATE strategie_territoire.passage_table set data_unit = 'teqCO2' where region='bretagne' and key like 'DataSet.EMISSIONS%';
        UPDATE strategie_territoire.passage_table set data_unit = 'GWh' where region='bretagne' and key like 'DataSet.CONSUMPTION%';
    """
    )


def downgrade():
    op.execute("ALTER TABLE strategie_territoire.passage_table DROP COLUMN data_unit;")
