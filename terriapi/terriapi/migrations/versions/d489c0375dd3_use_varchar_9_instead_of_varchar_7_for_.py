"""Use varchar(9) instead of varchar(7) for indicator's colors

Revision ID: d489c0375dd3
Revises: 375e3c6b2cf7
Create Date: 2023-04-24 11:33:39.854267

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d489c0375dd3"
down_revision = "375e3c6b2cf7"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ ALTER TABLE meta.indicateur ALTER COLUMN color_start TYPE VARCHAR(9);
            ALTER TABLE meta.indicateur ALTER COLUMN color_end TYPE VARCHAR(9); """
    )


def downgrade():
    op.execute(
        """ ALTER TABLE meta.indicateur ALTER COLUMN color_start TYPE VARCHAR(7) USING substring(color_start FROM 0 FOR 8);
            ALTER TABLE meta.indicateur ALTER COLUMN color_end TYPE VARCHAR(7) USING substring(color_end FROM 0 FOR 8); """
    )
