"""Fix some tables' IDs

Revision ID: 379bbc8a8464
Revises: 4b6b5ac87df6
Create Date: 2023-07-12 11:05:37.545185

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "379bbc8a8464"
down_revision = "4b6b5ac87df6"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE SEQUENCE strat_params_user_actions_params_idx_seq;
        ALTER TABLE strategie_territoire.params_user_action_params ALTER COLUMN index SET DEFAULT nextval('strat_params_user_actions_params_idx_seq');
        CREATE SEQUENCE strat_params_user_actions_params_years_idx_seq;
        ALTER TABLE strategie_territoire.params_user_action_params_years ALTER COLUMN index SET DEFAULT nextval('strat_params_user_actions_params_years_idx_seq');
        UPDATE meta.categorie SET modalite = 'EnR thermiques' WHERE modalite = 'ENR thermiques' AND nom = 'energie' AND region = 'auvergne-rhone-alpes';
        """
    )


def downgrade():
    op.execute("DROP SEQUENCE strat_params_user_actions_params_idx_seq CASCADE;")
    op.execute("DROP SEQUENCE strat_params_user_actions_params_years_idx_seq CASCADE;")
    op.execute(
        "UPDATE meta.categorie SET modalite = 'ENR thermiques' WHERE modalite = 'EnR thermiques' AND nom = 'energie' AND region = 'auvergne-rhone-alpes';"
    )
