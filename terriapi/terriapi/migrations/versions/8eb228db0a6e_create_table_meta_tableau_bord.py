﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create table meta tableau bord

Revision ID: 8eb228db0a6e
Revises: 2427b2a82738
Create Date: 2020-12-29 10:07:19.905121

"""
from alembic import op
from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Text,
    UniqueConstraint,
)

# revision identifiers, used by Alembic.
revision = "8eb228db0a6e"
down_revision = "2427b2a82738"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "tableau_bord"
# XXX(dag) doit-on le mettre en prefix utilisateur comme pour les scénario ?
# i.e. utilisateur_tableau_bord ? À terme, déplacer les différentes tables
# 'utilisateur_xxx' serait une bonne idée je pense.


def upgrade():
    op.create_table(
        TABLENAME,
        Column(
            "id", Integer, primary_key=True, comment="Identifiant"
        ),  # va créer un SERIAL
        Column("titre", String, nullable=False, comment="Titre du tableau de bord"),
        Column(
            "mail",
            String,
            ForeignKey("utilisateur.mail", ondelete="CASCADE"),
            nullable=False,
            comment="Mail de l'utilisateur·rice",
        ),
        Column("description", Text, comment="Description du tableau de bord"),
        Column("region", String, nullable=False, comment="Identifiant de la région"),
        Column(
            "date",
            DateTime,
            server_default="now",
            nullable=False,
            comment="Date de création/mise à jour",
        ),
        UniqueConstraint("titre", "mail", "region"),
        schema=SCHEMA,
    )


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
