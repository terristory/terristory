"""empty message

Revision ID: a272014697e5
Revises: eaa21f6539d0, f5cd69a7d695
Create Date: 2023-01-30 18:35:01.941946

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a272014697e5"
down_revision = ("eaa21f6539d0", "f5cd69a7d695")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
