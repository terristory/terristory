"""empty message

Revision ID: a9e4576dd74b
Revises: 0ec6aefc2ec0, f81622a8be35
Create Date: 2023-03-30 11:34:23.992575

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a9e4576dd74b"
down_revision = ("0ec6aefc2ec0", "f81622a8be35")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
