﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""colonne_donnees_registre_perimetre_geo

Revision ID: 0d3fef687169
Revises: 02b6d51471e0
Create Date: 2022-06-03 16:53:39.728878

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "0d3fef687169"
down_revision = "02b6d51471e0"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "alter table meta.perimetre_geographique add column donnees_registre boolean;"
    )


def downgrade():
    op.execute("alter table meta.perimetre_geographique drop column donnees_registre;")
