﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""colone-type-geometrie-equipement

Revision ID: eee9e289ea97
Revises: 54c2de17b94c
Create Date: 2022-03-07 09:39:55.979726

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "eee9e289ea97"
down_revision = "54c2de17b94c"
branch_labels = None
depends_on = None

schemas = [
    "auvergne_rhone_alpes",
    "occitanie",
    "nouvelle_aquitaine",
    "bretagne",
    "paysdelaloire",
    "corse",
]


def upgrade():
    for schema in schemas:
        op.execute(
            "alter table {schema}_poi.layer add column type_geom varchar;".format(
                schema=schema
            )
        )
        op.execute(
            "update {schema}_poi.layer add set type_geom = 'point';".format(
                schema=schema
            )
        )


def downgrade():
    for schema in schemas:
        op.execute(
            "alter table {schema}_poi.layer drop column type_geom;".format(
                schema=schema
            )
        )
