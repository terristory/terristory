"""add regions to category lever simulator

Revision ID: f5ca81c5952c
Revises: 10f1d3b629c4
Create Date: 2023-06-05 12:48:38.735498

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f5ca81c5952c"
down_revision = "10f1d3b629c4"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ 
        ALTER TABLE didactic_file.category_lever ADD COLUMN regions varchar[];
        """
    )


def downgrade():
    op.execute(
        """ 
        ALTER TABLE didactic_file.category_lever DROP COLUMN regions;
        """
    )
