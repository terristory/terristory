﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add order to zone choice

Revision ID: c4bd9be72469
Revises: b753b71ab6fa
Create Date: 2022-10-17 17:00:07.057338

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c4bd9be72469"
down_revision = "b753b71ab6fa"
branch_labels = None
depends_on = None


def all_regional_schemas(engine):
    with engine.connect() as con:
        rs = con.execute("SELECT * FROM regions_configuration WHERE env = 'dev'")
        for row in rs:
            schema = row["id"]
            yield schema


def upgrade():
    engine = op.get_bind()
    for schema in all_regional_schemas(engine):
        schema_name = schema.replace("-", "_")
        op.add_column("zone", sa.Column("ordre", sa.Integer), schema=schema_name)
        op.add_column(
            "zone", sa.Column("hide", sa.Boolean, default=False), schema=schema_name
        )


def downgrade():
    engine = op.get_bind()
    for schema in all_regional_schemas(engine):
        schema_name = schema.replace("-", "_")
        with op.batch_alter_table("zone", schema=schema_name) as batch_op:
            batch_op.drop_column("ordre")
            batch_op.drop_column("hide")
