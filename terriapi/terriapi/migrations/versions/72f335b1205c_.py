"""empty message

Revision ID: 72f335b1205c
Revises: 322aa04a62c8, a0301e6a515b
Create Date: 2025-02-28 17:02:14.406773

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "72f335b1205c"
down_revision = ("322aa04a62c8", "a0301e6a515b")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
