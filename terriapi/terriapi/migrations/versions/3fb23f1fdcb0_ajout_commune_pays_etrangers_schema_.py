﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout-commune-pays-etrangers-schema-commun

Revision ID: 3fb23f1fdcb0
Revises: 6edade4bfef6
Create Date: 2021-06-29 15:40:14.410996

"""
from alembic import op
from sqlalchemy import Boolean, Column, Float, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "3fb23f1fdcb0"
down_revision = "6edade4bfef6"
branch_labels = None
depends_on = None


def creer_schema(schema):
    op.execute(f"create schema {schema};")


def supprimer_schema(schema):
    op.execute(f"drop schema {schema} cascade;")


def creer_table_territoire_etranger(type_territoire):
    op.create_table(
        type_territoire,
        Column("nom", String, comment="Nom du territoire"),
        Column("code", String, comment="Code INSEE du territoire"),
        Column("statut", String, comment="Statut de la commune"),
        Column(
            "x",
            Float,
            comment="Coordonnée x projetée en pseudo-mercator 3857 exprimée en mètres",
        ),
        Column(
            "y",
            Float,
            comment="Coordonnée y projetée en pseudo-mercator 3857 exprimée en mètres",
        ),
        schema="territoires_etrangers",
    )


def ajout_colonne_geometry(table):
    op.execute(
        "alter table territoires_etrangers.{table} add column geom geometry(MultiPolygon,3857)".format(
            table=table
        )
    )


def upgrade():
    creer_schema("territoires_etrangers")
    creer_table_territoire_etranger("communes")
    creer_table_territoire_etranger("pays")
    ajout_colonne_geometry("communes")
    ajout_colonne_geometry("pays")


def downgrade():
    supprimer_schema("territoires_etrangers")
