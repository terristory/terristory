﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""derniere annee projection

Revision ID: 0b39ed7d1e26
Revises: 8cf63cf95771
Create Date: 2022-01-31 15:37:54.219605

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "0b39ed7d1e26"
down_revision = "8cf63cf95771"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "alter table meta.parametres_objectifs add column derniere_annee_projection integer;"
    )
    op.execute("alter table meta.parametres_objectifs add column description varchar;")


def downgrade():
    op.execute(
        "alter table meta.parametres_objectifs drop column derniere_annee_projection;"
    )
    op.execute("alter table meta.parametres_objectifs drop column description;")
