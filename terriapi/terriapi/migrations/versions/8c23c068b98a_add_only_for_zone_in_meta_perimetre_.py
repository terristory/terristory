"""Add only_for_zone in meta.perimetre_geographique

Revision ID: 8c23c068b98a
Revises: 04f58c965cc4
Create Date: 2024-04-23 18:22:56.693892

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8c23c068b98a"
down_revision = "04f58c965cc4"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "alter table meta.perimetre_geographique add column only_for_zone varchar default NULL;"
    )


def downgrade():
    op.execute("alter table meta.perimetre_geographique drop column only_for_zone;")
