"""Add passage tables for actions

Revision ID: 7ab3625d068f
Revises: 41b137ea80e5
Create Date: 2023-03-13 18:27:03.092019

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7ab3625d068f"
down_revision = "41b137ea80e5"
branch_labels = None
depends_on = None

POLLUTANTS = ["nox", "covnm", "pm10", "pm25", "nh3", "sox"]
CONFID_RELATED_TABLES = ["CONSUMPTION", "EMISSIONS"] + [
    "POLLUTANT_" + p.upper() for p in POLLUTANTS
]
TABLES = ["conso_energetique", "emission_ges"] + [
    "air_polluant_" + p for p in POLLUTANTS
]


def upgrade():
    op.create_table(
        "passage_table",
        sa.Column(
            "region", sa.String, primary_key=True, comment="Region", nullable=False
        ),
        sa.Column("key", sa.String, primary_key=True, comment="key", nullable=False),
        sa.Column(
            "match", sa.String, primary_key=True, comment="Table name", nullable=False
        ),
        sa.Column(
            "association_type",
            sa.String,
            comment="Type of association",
            nullable=False,
        ),
        sa.Column(
            "aggregation",
            sa.String,
            comment="Aggregation rule if needed",
            nullable=True,
            server_default="keep",
        ),
        schema="strategie_territoire",
    )

    op.execute(
        """
        TRUNCATE strategie_territoire.passage_table;

        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'DataSet.PRODUCTION', 'prod_enr', 'table'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd', 'type_prod_enr', 'column');

        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET', 'parc_auto_statique', 'table'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleType', 'type_veh', 'column'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleType.PASSENGER_CARS', '1', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleType.TRUCKS', '2', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleType.BUSES_COACHES', '3', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleType.LIGHT_COMM', '4', 'value'),
    
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel', 'energie_carburant', 'column'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.DIESEL', '1', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.GASOLINE', '2', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.GAS', '3', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELEC_H2', '4', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_DIESEL', '5', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_GASOLINE', '6', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELECTRIC', '7', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.VEHICLE_FLEET/VehicleFuel.UNKNOWN', '8', 'value');

        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.SOLAR_THER', '2', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.SOLAR_PV', '3', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.WIND', '4', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.HYDRO_LOW', '5', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.HYDRO_HIGH', '12', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_ELEC', '7', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_THER', '8', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_INJEC', '13', 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.BIOMASS_SOLID', '6', 'value');

        DELETE FROM strategie_territoire.passage_table WHERE key LIKE 'ParamDataSet.GES_FACTORS/%';
        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Usage', 'id_usage', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Usage.HEATING', '1', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Usage.HOT_WATER', '2', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Usage.COOKING', '3', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Usage.COOLING', '6', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Usage.OTHERS', '7', 'value'),

        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/RenewableProd', 'id_usage', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/RenewableProd.SOLAR_PV', '5', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/RenewableProd.WIND', '8', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/RenewableProd.HYDRO_LOW', '9', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/RenewableProd.HYDRO_HIGH', '9', 'value'),

        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity', 'type_energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.COAL', 'Charbon', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.ELECTRICITY', 'Electricité', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.BIOMASS', 'Biomasse', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.DIESEL', 'Diesel', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.GASOLINE', 'Essence', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.FUEL_OIL', 'Fioul', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.LPG', 'Gpl', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.BIOFUEL', 'Biocarburant', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.BIOGAS', 'BioGNV', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.GAS', 'Gaz', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.GAS', 'GNV', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/ParamCommodity.HEAT', 'Chaleur', 'value'),

        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1', 'id_conso_energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.ELECTRICITY', '1', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.URBAN_HEAT_COOL', '2', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.GAS', '3', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.COAL', '5', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.RENEWABLE_HEAT', '4', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.OIL', '6', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.OIL', '7', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.OIL', '8', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.OIL', '9', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#1.BIOFUELS', '10', 'value'),

        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#2', 'energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#2.OIL', 'Produits pétroliers', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Commodity#2.GAS', 'GNV', 'value')
        ;

        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Sector', 'secteur', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Sector.RESIDENTIAL', 'Résidentiel', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Sector.SERVICES', 'Tertiaire', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Sector.ROAD_TRANSPORT', 'Transport', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Sector.ALL', 'Tout secteur', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.GES_FACTORS/Sector.UNDIFFERENCIATED', 'Indifférencié', 'value')
        ;

        DELETE FROM strategie_territoire.passage_table WHERE key LIKE 'ParamDataSet.ATMO_GES_FACTORS/%';

        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity', 'type_energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.ALL', 'Toute énergie', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.ELECTRICITY', 'Elec', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.BIOMASS', 'Biomasse', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.DIESEL', 'Diesel', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.DIESEL', 'Diesel_Actuel', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.DIESEL', 'Diesel_Récent', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.FUEL_OIL', 'Fioul', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.GAS', 'Gaz', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/ParamCommodity.GAS', 'GNV', 'value'),
        
        
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Commodity#2', 'energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Commodity#2.OIL', 'Produits pétroliers', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Commodity#2.GAS', 'GNV', 'value');

        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Sector', 'secteur', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Sector.RESIDENTIAL', 'Résidentiel', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Sector.SERVICES', 'Tertiaire', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Sector.ROAD_TRANSPORT', 'Transport', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Sector.ALL', 'Tout secteur', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.ATMO_GES_FACTORS/Sector.UNDIFFERENCIATED', 'Indifférencié', 'value')
        ;


        DELETE FROM strategie_territoire.passage_table WHERE key LIKE 'ParamDataSet.PRICES/%';
        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity', 'energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.COAL', 'Charbon', 'value'), -- 1
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.ELECTRICITY', 'Electricité', 'value'), -- 2
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.BIOMASS', 'Biomasse', 'value'), -- 3
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.DIESEL', 'Diesel', 'value'), -- 5
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.GASOLINE', 'Essence', 'value'), -- 5
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.FUEL_OIL', 'Fioul', 'value'), -- 5
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.NON_ROAD_DIESEL', 'Gazole Non Routier', 'value'), -- 5
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.LPG', 'Gpl', 'value'), -- ?
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.BIOFUEL', 'Organo-carburants', 'value'), -- 6
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.BIOGAS', 'BioGNV', 'value'), -- 6
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.GAS', 'Gaz', 'value'), -- 7
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.GAS', 'GNV', 'value'), -- 7
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.HEAT', 'Chaleur', 'value'), -- 9
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/ParamCommodity.WASTE', 'Chaleur', 'value'), -- 10
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Sector', 'id_secteur', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Sector.RESIDENTIAL', '1', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Sector.INDUSTRY_WITHOUT_ENERGY', '3', 'value'),
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Sector.UNDIFFERENCIATED', '4', 'value');
        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity', 'id_energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.COAL', '1', 'value'), -- 1
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.ELECTRICITY', '2', 'value'), -- 2
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.RENEWABLE_HEAT', '3', 'value'), -- 3
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.OIL', '5', 'value'), -- 5
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.BIOFUELS', '6', 'value'), -- 6
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.GAS', '7', 'value'), -- 7
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.URBAN_HEAT_COOL', '9', 'value'), -- 9
        ('auvergne-rhone-alpes', 'ParamDataSet.PRICES/Commodity.WASTES', '10', 'value');


        DELETE FROM strategie_territoire.passage_table WHERE key LIKE 'ParamDataSet.FORCED_MIX/%';
        INSERT INTO strategie_territoire.passage_table 
        VALUES('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity', 'energie', 'column'),
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.COAL', '1', 'value'), -- 1
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.ELECTRICITY', '2', 'value'), -- 2
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.RENEWABLE_HEAT', '3', 'value'), -- 3
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.OIL', '5', 'value'), -- 5
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.BIOFUELS', '6', 'value'), -- 6
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.GAS', '7', 'value'), -- 7
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.URBAN_HEAT_COOL', '9', 'value'), -- 9
        ('auvergne-rhone-alpes', 'ParamDataSet.FORCED_MIX/Commodity.WASTES', '10', 'value');
    """
    )

    for key, table in zip(CONFID_RELATED_TABLES, TABLES):
        op.execute(
            f"""INSERT INTO strategie_territoire.passage_table 
            VALUES('auvergne-rhone-alpes', 'DataSet.{key}', '{table}', 'table');"""
        )
        op.execute(
            f"""
            INSERT INTO strategie_territoire.passage_table 
            VALUES('auvergne-rhone-alpes', 'DataSet.{key}/Sector', 'secteur', 'column'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity', 'energie', 'column');
            INSERT INTO strategie_territoire.passage_table 
            VALUES('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.ELECTRICITY', '2', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.NON_ENERGETIC', '4', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.OIL', '5', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.RENEWABLE_HEAT', '3', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.BIOFUELS', '6', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.GAS', '7', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.UNKNOWN', '8', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.URBAN_HEAT_COOL', '9', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.WASTES', '10', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.NON_RENEWABLE_HEAT', '11', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity.COAL', '1', 'value');
            INSERT INTO strategie_territoire.passage_table 
            VALUES('auvergne-rhone-alpes', 'DataSet.{key}/Sector.RESIDENTIAL', '1', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Sector.AGRICULTURE', '2', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Sector.SERVICES', '3', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Sector.WASTE_MANAGEMENT', '4', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Sector.INDUSTRY_WITHOUT_ENERGY', '5', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Sector.ROAD_TRANSPORT', '6', 'value'),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Sector.ENERGY_INDUSTRY', '8', 'value');"""
        )
        if "POLLUTANT" not in key:
            op.execute(
                f"""
                INSERT INTO strategie_territoire.passage_table 
                VALUES
                ('auvergne-rhone-alpes', 'DataSet.{key}/Usage', 'usage', 'column'),
                ('auvergne-rhone-alpes', 'DataSet.{key}/Usage.HEATING', '1', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.{key}/Usage.HOT_WATER', '2', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.{key}/Usage.COOKING', '3', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.{key}/Usage.SPECIFIC_ELEC', '4', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.{key}/Usage.OTHERS', '5', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.{key}/Usage.COOLING', '11', 'value');"""
            )


def downgrade():
    op.drop_table("passage_table", schema="strategie_territoire")
