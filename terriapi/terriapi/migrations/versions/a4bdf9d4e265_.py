"""empty message

Revision ID: a4bdf9d4e265
Revises: a17e6162d9ab, cc441053dd71
Create Date: 2024-10-10 12:03:56.601266

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a4bdf9d4e265"
down_revision = ("a17e6162d9ab", "cc441053dd71")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
