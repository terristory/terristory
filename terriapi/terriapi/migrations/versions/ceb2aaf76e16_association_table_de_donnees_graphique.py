﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""association table de donnees graphique

Revision ID: ceb2aaf76e16
Revises: 9cbb67bbe29c
Create Date: 2021-11-23 13:55:21.143183

"""
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "ceb2aaf76e16"
down_revision = "9cbb67bbe29c"
branch_labels = None
depends_on = None

schema = "meta"


def association_table_trajectoire_cible(schema):
    op.create_table(
        "association_graphique_table",
        Column("graphique", String, comment="Graphique d'une trajectoire cible"),
        Column(
            "table", String, comment="Table nécessaire à la construction du graphique"
        ),
        Column(
            "region",
            String,
            comment="Région concernée par cette association graphique / table",
        ),
        schema=schema,
    )


def upgrade():
    association_table_trajectoire_cible(schema)


def downgrade():
    req = """
          drop table {schema}.association_graphique_table;
          """.format(
        schema=schema
    )
    op.execute(req)
