﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout ordre rubriques et indicateurs

Revision ID: 04d80ed56783
Revises: d28dd2fac1bf
Create Date: 2021-08-20 12:14:39.692552

"""
from alembic import op
from sqlalchemy import Column, Integer

# revision identifiers, used by Alembic.
revision = "04d80ed56783"
down_revision = "d28dd2fac1bf"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "indicateur"


def upgrade():
    op.add_column(TABLENAME, Column("ordre_ui_theme", Integer), schema=SCHEMA)

    op.add_column(TABLENAME, Column("ordre_indicateur", Integer), schema=SCHEMA)


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("ordre_ui_theme")

    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("ordre_indicateur")
