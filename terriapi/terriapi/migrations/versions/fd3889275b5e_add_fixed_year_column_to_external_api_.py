"""Add fixed year column to external API link

Revision ID: fd3889275b5e
Revises: 36ca60c1e4c6
Create Date: 2023-08-22 11:30:27.966045

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "fd3889275b5e"
down_revision = "36ca60c1e4c6"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("external_api", schema="meta") as batch_op:
        batch_op.add_column(sa.Column("fixed_year", sa.String))


def downgrade():
    with op.batch_alter_table("external_api", schema="meta") as batch_op:
        batch_op.drop_column("fixed_year")
