"""reorganize territorial strategy tables

Revision ID: 8b826bf079f8
Revises: 8e4d7960855e
Create Date: 2024-07-09 23:36:02.773933

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8b826bf079f8"
down_revision = "8e4d7960855e"
branch_labels = None
depends_on = None


SCHEMA = "strategie_territoire"
TABLES = ["params_user_action_params", "params_user_action_params_years"]
REGIONS = [
    "auvergne-rhone-alpes",
    "occitanie",
    "paysdelaloire",
    "bretagne",
    "nouvelle-aquitaine",
]


def upgrade():
    for table in TABLES:
        # Add the new column "order"
        op.add_column(table, sa.Column("order", sa.Integer()), schema=SCHEMA)

        # Update "order" column with an incremental integer grouped by "action"
        op.execute(
            f"""
                WITH ranked_data AS (
                    SELECT
                        *,
                        ROW_NUMBER() OVER (PARTITION BY action, region ORDER BY index) AS row_num
                    FROM
                        {SCHEMA}.{table}
                )
                UPDATE
                    {SCHEMA}.{table} AS t
                SET
                    "order" = rd.row_num
                FROM ranked_data AS rd
                WHERE 
                    t.index = rd.index
                    AND t.libelle = rd.libelle
                    AND t.nom=rd.nom;
            """
        )
    op.execute(
        f"""
            CREATE TABLE {SCHEMA}.params_ifer (
                category varchar,
                value double precision,
                region varchar 
            );

            ALTER TABLE {SCHEMA}.params_action3b_ifer
            RENAME TO old_ifer;

            DROP TABLE {SCHEMA}.params_action10_ifer;
        """
    )
    op.execute(
        f"""
            UPDATE {SCHEMA}.params_user_action_params SET valeur= 18 WHERE action = '3b' and nom='Taux d''imposition foncière'
        """
    )
    op.execute(
        f"""
            -- Delete referencing rows in action_subactions_status
            DELETE FROM strategie_territoire.action_subactions_status
            WHERE action_number='12';
            
            -- INSERT 'puiss_installee' parameter if not exists
            INSERT INTO strategie_territoire.action_subactions (action_number, nom, label, unite)
            VALUES ('12', 'puiss_installee', 'Puissance installée', 'MW');
        """
    )
    op.execute(
        f"""
            -- Perform the main computation and insertion
            WITH scenario_ids AS (
                SELECT id 
                FROM public.utilisateur_scenario 
                WHERE '12' IN (SELECT unnest(actions))
            ),

            action_param AS (
                SELECT id, nom 
                FROM strategie_territoire.action_subactions 
                WHERE action_number = '12'
            ),

            params AS (
                SELECT usp.id, usp.action_param, usp.valeur::jsonb, usp.scenario, ap.nom
                FROM public.utilisateur_scenario_params usp
                JOIN action_param ap ON usp.action_param = ap.id
                JOIN scenario_ids si ON usp.scenario = si.id
            ),

            parsed_params AS (
                SELECT 
                    id,
                    action_param,
                    scenario,
                    nom,
                    kv.key::int as year,
                    (CASE WHEN kv.value::text = '' THEN '0' ELSE kv.value::text END)::numeric as value
                FROM params, jsonb_each_text(valeur) as kv
            ),

            aggregated_values AS (
                SELECT 
                    scenario,
                    year,
                    COALESCE(SUM(CASE WHEN nom = 'nb_installations_inf' THEN value ELSE 0 END), 0) * 
                    COALESCE(SUM(CASE WHEN nom = 'puiss_installee_inf' THEN value ELSE 0 END), 0) +
                    COALESCE(SUM(CASE WHEN nom = 'nb_installations_sup' THEN value ELSE 0 END), 0) * 
                    COALESCE(SUM(CASE WHEN nom = 'puiss_installee_sup' THEN value ELSE 0 END), 0) as computed_value
                FROM parsed_params
                GROUP BY scenario, year
            ),

            formatted_values AS (
                SELECT 
                    scenario,
                    jsonb_object_agg(year::text, computed_value::text) as new_valeur
                FROM aggregated_values
                GROUP BY scenario
            ),

            -- Insert the computed values into the newly inserted rows
            inserted_values AS (
                INSERT INTO public.utilisateur_scenario_params (action_param, scenario, valeur)
                SELECT 
                    (SELECT id FROM strategie_territoire.action_subactions WHERE action_number = '12' AND nom = 'puiss_installee'), 
                    scenario, 
                    new_valeur
                FROM formatted_values
                RETURNING id
            )

            -- Delete old rows that were replaced by computed values from utilisateur_scenario_params
            DELETE FROM public.utilisateur_scenario_params usp
            WHERE action_param IN (
                SELECT id
                FROM strategie_territoire.action_subactions
                WHERE action_number = '12'
            )
            AND NOT EXISTS (
                SELECT 1
                FROM inserted_values iv
                WHERE usp.id = iv.id
            );

            -- Delete referencing rows in action_subactions
            DELETE FROM strategie_territoire.action_subactions 
            WHERE action_number='12' AND nom <> 'puiss_installee';
			
            -- Update name in action table
            UPDATE strategie_territoire.action 
            SET name='Petite centrale hydroélectrique' 
            WHERE action_number='12';
        """
    )

    for region in REGIONS:
        op.execute(
            f"""
                INSERT INTO {SCHEMA}.params_ifer(category, value, region) 
                VALUES ('eolien',8.16, '{region}'),
                        ('pv',3.394, '{region}'),
                        ('hydro',3.394, '{region}');
            """
        )
        op.execute(
            f"""
                INSERT INTO {SCHEMA}.action_subactions_status(region, action_number, nom, enabled)
                VALUES ('{region}', '12', 'puiss_installee', true)
            """
        )


def downgrade():
    for table in TABLES:
        with op.batch_alter_table(table, schema=SCHEMA) as batch_op:
            batch_op.drop_column("order")

    op.execute(
        f"""
            DROP TABLE {SCHEMA}.params_ifer;

            ALTER TABLE {SCHEMA}.old_ifer
            RENAME TO params_action3b_ifer;

            CREATE TABLE {SCHEMA}.params_action10_ifer AS (SELECT * FROM {SCHEMA}.params_action3b_ifer);

            UPDATE {SCHEMA}.params_user_action_params SET valeur= 0.18 WHERE action = '3b' and nom='Taux d''imposition foncière'
        """
    )

    op.execute(
        f"""
            DELETE FROM strategie_territoire.action_subactions_status 
            WHERE action_number='12' AND nom ='puiss_installee';

            DELETE FROM {SCHEMA}.action_subactions
            WHERE action_number='12' AND nom='puiss_installee';

            INSERT INTO {SCHEMA}.action_subactions(action_number, nom, label, unite)
            VALUES 
                ('12', 'nb_installations_inf', 'Nombre d''installations dont la puissance est inférieure à 4,5 MW', 'nb'),
                ('12', 'puiss_installee_inf', 'Puissance installée inférieure à 4,5 MW', 'MW'),
                ('12', 'nb_installations_sup', 'Nombre d''installations dont la puissance est supérieure à 4,5 MW', 'nb'),
                ('12', 'puiss_installee_sup', 'Puissance installée supérieure à 4,5 MW', 'MW');
            
            UPDATE {SCHEMA}.action
            SET name='Centrale hydroélectrique'
            WHERE action_number='12';
        """
    )

    for region in REGIONS:
        op.execute(
            f"""
                INSERT INTO {SCHEMA}.action_subactions_status(region, action_number, nom, enabled)
                VALUES ('{region}', '12', 'nb_installations_inf', true),
                        ('{region}','12', 'puiss_installee_inf', true),
                        ('{region}','12', 'nb_installations_sup', true),
                        ('{region}','12', 'puiss_installee_sup', true);
            """
        )
