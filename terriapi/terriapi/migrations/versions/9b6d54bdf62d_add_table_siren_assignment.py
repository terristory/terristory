"""Add table siren_assignment

Revision ID: 9b6d54bdf62d
Revises: 690a66f2e03a
Create Date: 2024-01-19 10:29:57.911535

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9b6d54bdf62d"
down_revision = "690a66f2e03a"
branch_labels = None
depends_on = None

regions = [
    "auvergne_rhone_alpes",
    "occitanie",
    "paysdelaloire",
    "bretagne",
    "nouvelle_aquitaine",
    "corse",
]


def upgrade():
    for region in regions:
        op.execute(
            f"""
            CREATE TABLE {region}.siren_assignment
                (zone_type VARCHAR, zone_id VARCHAR, siren VARCHAR, epcis VARCHAR[]);
            CREATE UNIQUE INDEX ON {region}.siren_assignment (zone_type, zone_id);
            """
        )


def downgrade():
    for region in regions:
        op.execute(f"DROP TABLE {region}.siren_assignment")
