﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""

Revision ID: 54c2de17b94c
Revises: 737c5cb25ad9, d45784fb2ae3
Create Date: 2022-03-02 14:47:07.393535

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "54c2de17b94c"
down_revision = ("737c5cb25ad9", "d45784fb2ae3")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
