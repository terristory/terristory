﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""table-objectifs

Revision ID: d8f69de02299
Revises: 7c2540ccd418
Create Date: 2021-11-22 14:29:05.684360

"""
from alembic import op
from sqlalchemy import Boolean, Column, Float, Integer, PrimaryKeyConstraint, String

# revision identifiers, used by Alembic.
revision = "d8f69de02299"
down_revision = "7c2540ccd418"
branch_labels = None
depends_on = None

schema = "meta"


def creer_table_objectifs(schema):
    op.create_table(
        "objectifs",
        Column(
            "titre", String, comment="Titre de l'objectif (SRADDET, REPOS, SNBC etc.)"
        ),
        Column("annee", Integer, comment="Année"),
        Column("valeur", Float, comment="Année"),
        Column("graphique", String, comment="Graphique auquel ajouter l'objectif"),
        Column("annee_modifiee", Boolean, comment="L'année a-t-elle été modifiée ?"),
        Column("id", String, comment="Identifiant unique"),
        Column("region", String, comment="Région pour laquelle l'objectif est saisi"),
        PrimaryKeyConstraint("id", "graphique", "region"),
        schema=schema,
    )


def upgrade():
    creer_table_objectifs(schema)


def downgrade():
    op.execute("drop table " + schema + ".objectifs cascade ;")
