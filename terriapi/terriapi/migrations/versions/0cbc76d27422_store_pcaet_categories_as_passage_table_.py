"""Store PCAET categories as passage table labels

Revision ID: 0cbc76d27422
Revises: 0fe48e9214c6
Create Date: 2024-11-12 15:02:44.360340

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0cbc76d27422"
down_revision = "0fe48e9214c6"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        DELETE FROM pcaet;
        ALTER TABLE pcaet ALTER COLUMN category TYPE VARCHAR;
        """
    )


def downgrade():
    op.execute(
        """
        DELETE FROM pcaet;
        ALTER TABLE pcaet ALTER COLUMN category TYPE INTEGER USING category::INTEGER;
        """
    )
