"""Add region column to user ID

Revision ID: 4836156238ff
Revises: f5ca81c5952c
Create Date: 2023-06-19 11:05:41.144672

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4836156238ff"
down_revision = "f5ca81c5952c"
branch_labels = None
depends_on = None

TABLES_WITH_FOREIGN_KEY = [
    ("auvergne_rhone_alpes", "historique_indicateurs"),
    ("bretagne", "historique_indicateurs"),
    ("corse", "historique_indicateurs"),
    ("nouvelle_aquitaine", "historique_indicateurs"),
    ("occitanie", "historique_indicateurs"),
    ("paysdelaloire", "historique_indicateurs"),
    ("paysdelaloire_poi", "historique"),
    ("bretagne_poi", "historique"),
    ("corse_poi", "historique"),
    ("france", "historique_indicateurs"),
    ("france_poi", "historique"),
    ("nouvelle_aquitaine_poi", "historique"),
    ("auvergne_rhone_alpes_poi", "historique"),
    ("occitanie_poi", "historique"),
    ("public", "refresh_token"),
    ("meta", "tableau_bord"),
    ("public", "utilisateur_scenario"),
]


def upgrade():
    op.add_column("refresh_token", sa.Column("region", sa.String()), schema="public")

    op.execute(
        "DELETE FROM refresh_token WHERE mail IN ('alice@terristory-wonder.land', 'pytest@terristory-nowhere.dev', 'bob@terristory-wonder.land')"
    )
    op.execute(
        "UPDATE refresh_token r SET region = (SELECT region FROM utilisateur u WHERE r.mail = u.mail LIMIT 1)"
    )

    op.execute(
        "ALTER TABLE utilisateur DROP CONSTRAINT IF EXISTS utilisateur_pkey CASCADE"
    )
    op.create_primary_key("utilisateur_pkey", "utilisateur", ["mail", "region"])
    op.execute(
        "ALTER TABLE refresh_token DROP CONSTRAINT IF EXISTS refresh_token_mail_key CASCADE"
    )
    op.create_primary_key("refresh_token_pkey", "refresh_token", ["mail", "region"])

    for schema, table in TABLES_WITH_FOREIGN_KEY:
        if table in ("historique_indicateurs", "historique"):
            with op.batch_alter_table(table, schema=schema) as batch_op:
                batch_op.add_column(
                    sa.Column(
                        "region",
                        sa.String(),
                        server_default=schema.replace("_poi", "").replace("_", "-"),
                    )
                )
                batch_op.alter_column("utilisateur", new_column_name="mail")

        keys = ["mail", "region"]
        if schema == "france":
            continue

        action_triggered = "SET NULL"
        if table in ("refresh_token", "tableau_bord"):
            action_triggered = "CASCADE"

        op.create_foreign_key(
            f"{schema}_{table}_fkey",
            table,
            "utilisateur",
            keys,
            keys,
            onupdate=action_triggered,
            ondelete=action_triggered,
            source_schema=schema,
        )


def downgrade():
    with op.batch_alter_table("refresh_token", schema="public") as batch_op:
        batch_op.drop_column("region")
    with op.batch_alter_table("utilisateur", schema="public") as batch_op:
        batch_op.execute(
            "ALTER TABLE utilisateur DROP CONSTRAINT IF EXISTS utilisateur_pkey CASCADE"
        )
        batch_op.create_primary_key("utilisateur_pkey", ["mail"])
    with op.batch_alter_table("tableau_bord", schema="meta") as batch_op:
        batch_op.execute(
            "ALTER TABLE meta.tableau_bord ADD CONSTRAINT tableau_bord_mail_fkey FOREIGN KEY (mail) REFERENCES utilisateur(mail) ON UPDATE CASCADE ON DELETE CASCADE"
        )
    for schema, table in TABLES_WITH_FOREIGN_KEY:
        if table in ("historique_indicateurs", "historique"):
            with op.batch_alter_table(table, schema=schema) as batch_op:
                batch_op.drop_column("region")
                batch_op.alter_column("mail", new_column_name="utilisateur")
