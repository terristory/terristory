﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajouter column status installation

Revision ID: 158d77b65d4c
Revises: eee9e289ea97
Create Date: 2022-04-26 18:08:43.189186

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "158d77b65d4c"
down_revision = "eee9e289ea97"
branch_labels = None
depends_on = None

schemas = [
    "auvergne_rhone_alpes",
    "occitanie",
    "nouvelle_aquitaine",
    "bretagne",
    "paysdelaloire",
    "corse",
]


def upgrade():
    for schema in schemas:
        op.execute(
            "alter table {schema}_poi.layer add column statut boolean;".format(
                schema=schema
            )
        )
        op.execute(
            "update {schema}_poi.layer add set statut = true;".format(schema=schema)
        )
    op.execute(
        "update auvergne_rhone_alpes_poi.layer add set statut = false where nom in('geothermie','reseau_chaleur');"
    )


def downgrade():
    for schema in schemas:
        op.execute(
            "alter table {schema}_poi.layer drop column statut;".format(schema=schema)
        )
