"""Add constraints on POI tables structure for contributions

Revision ID: 1d2001dcce18
Revises: d37bf05d9872
Create Date: 2023-11-08 15:23:10.504389

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "1d2001dcce18"
down_revision = "d37bf05d9872"
branch_labels = None
depends_on = None


schemas = [
    "auvergne_rhone_alpes_poi",
    "occitanie_poi",
    "nouvelle_aquitaine_poi",
    "bretagne_poi",
    "paysdelaloire_poi",
    "corse_poi",
]


def upgrade():
    for schema in schemas:
        op.create_unique_constraint(
            "unique_layer_name_in_poi_tables",
            "layer",
            ("nom",),
            schema=schema,
        )
        op.create_table(
            "layers_structure",
            sa.Column(
                "layer_name",
                sa.String,
                sa.ForeignKey(
                    schema + ".layer.nom", onupdate="CASCADE", ondelete="CASCADE"
                ),
                primary_key=True,
                comment="Layer name",
            ),
            sa.Column("field_name", sa.String, primary_key=True, comment="Field name"),
            sa.Column(
                "field_type",
                sa.String,
                server_default=str("text"),
                comment="Content constraint",
            ),
            sa.Column(
                "details",
                sa.JSON,
                comment="Additional details",
            ),
            schema=schema,
        )


def downgrade():
    for schema in schemas:
        op.drop_table("layers_structure", schema=schema)
        op.drop_constraint(
            "unique_layer_name_in_poi_tables",
            "layer",
            schema=schema,
        )
