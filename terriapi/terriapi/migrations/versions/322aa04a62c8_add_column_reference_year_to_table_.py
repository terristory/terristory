"""Add column 'reference_year' to table 'utilisateur_scenario'

Revision ID: 322aa04a62c8
Revises: 5ce3f33c77d1
Create Date: 2025-02-13 10:35:37.357199

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "322aa04a62c8"
down_revision = "5ce3f33c77d1"
branch_labels = None
depends_on = None


def upgrade():
    # Fix broken strategies due to NULL values inside utilisateur_scenario_params
    # This problem affected 3 strategies (280, 306 and 362) on 1 parameter (3: surface_bat_tertiaire)
    op.execute(
        """
        DELETE FROM utilisateur_scenario_params WHERE valeur IS NULL;
        ALTER TABLE utilisateur_scenario_params ALTER COLUMN valeur SET NOT NULL;
        """
    )

    # Create column and set default (last available) years for each region
    op.execute(
        """
        ALTER TABLE utilisateur_scenario ADD COLUMN reference_year INT;
        UPDATE utilisateur_scenario SET reference_year=(CASE region
            WHEN 'auvergne-rhone-alpes' THEN 2023
            WHEN 'occitanie'            THEN 2022
            WHEN 'paysdelaloire'        THEN 2021
            WHEN 'bretagne'             THEN 2020
            ELSE 2023
        END);
        ALTER TABLE utilisateur_scenario ALTER COLUMN reference_year SET NOT NULL;
        """
    )

    # The reference year will be the minimum between the default reference year (depending on region),
    # the first year of trajectory and the first year of action minus 1.
    # Actions 16 and 17 are ignored as they sometimes have parameters at reference year (so the minus 1 would overshoot).
    op.execute(
        """
        UPDATE utilisateur_scenario
        SET reference_year=a.reference_year
        FROM (
            SELECT id, LEAST(reference_year, traj_year, action_year) AS reference_year
            FROM utilisateur_scenario
            LEFT JOIN (
                SELECT scenario, MIN(annee)::INT AS traj_year FROM (
                    SELECT id, scenario, JSONB_OBJECT_KEYS((valeur->'annees_valeurs')::jsonb) AS annee
                    FROM utilisateur_scenario_trajectoire_cible
                ) _
                GROUP BY scenario
            ) t ON id = t.scenario
            LEFT JOIN (
                SELECT scenario, MIN(annee)::INT - 1 AS action_year FROM (
                    SELECT p.id, scenario, JSONB_OBJECT_KEYS(valeur::jsonb) AS annee
                    FROM utilisateur_scenario_params p
                    JOIN strategie_territoire.action_subactions a ON a.id = p.action_param
                    WHERE valeur != 'null' AND a.action_number != '16' AND a.action_number != '17'
                ) _
                GROUP BY scenario
            ) a ON id = a.scenario
        ) a
        WHERE utilisateur_scenario.id = a.id
        """
    )


def downgrade():
    op.execute("ALTER TABLE utilisateur_scenario DROP COLUMN reference_year")
    op.execute(
        "ALTER TABLE utilisateur_scenario_params ALTER COLUMN valeur DROP NOT NULL"
    )
