"""Fix dashboards containing simulator link

Revision ID: 8ccaa14e2cbc
Revises: 75d9b27779fa
Create Date: 2025-01-28 15:52:00.325329

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8ccaa14e2cbc"
down_revision = "75d9b27779fa"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """UPDATE meta.tableau_thematique
        SET graphiques = REGEXP_REPLACE(graphiques::text, '"typeLink": "mobilit[^"]*"', '"typeLink": "mobility"')::json
        WHERE graphiques::text ~ '"typeLink": "mobilit[^"]*"';"""
    )


def downgrade():
    op.execute(
        """UPDATE meta.tableau_thematique
        SET graphiques = REPLACE(graphiques::text, '"typeLink": "mobility"', '"typeLink": "mobilité"')::json
        WHERE graphiques::text LIKE '%"typeLink": "mobility"%';"""
    )
