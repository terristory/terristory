﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""generer modele de donnees indicateur climat

Revision ID: 35dec2548360
Revises: 04d80ed56783
Create Date: 2021-07-29 14:26:34.388478

"""
from alembic import op
from sqlalchemy import Boolean, Column, Float, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "35dec2548360"
down_revision = "04d80ed56783"
branch_labels = None
depends_on = None
region = "auvergne_rhone_alpes"
station_mesure_schema = "auvergne_rhone_alpes_station_mesure"


def creer_schema(schema):
    op.execute(f"create schema {schema};")


def creer_table_data_indicateur(data):

    if data == "data_gel":
        op.execute(
            f"""create table {region}.{data}(
        id serial NOT NULL
        , station_id int NOT NULL
        , nom varchar
        , annee integer
        , libelle_mois varchar
        , valeur double precision NOT NULL
        , date varchar
        , jour_mois integer NOT NULL
        , mois integer NOT NULL
        , PRIMARY KEY (id)
        , FOREIGN KEY(station_id) REFERENCES  auvergne_rhone_alpes_station_mesure.station_mesure(id)
        );"""
        )
    elif data == "data_chaleur":
        op.execute(
            f"""
        create table {region}.{data}(
        id serial NOT NULL
        , station_id int NOT NULL
        , nom varchar
        , annee integer
        , libelle_mois varchar
        , valeur double precision NOT NULL
        , PRIMARY KEY (id)
        , FOREIGN KEY(station_id) REFERENCES  auvergne_rhone_alpes_station_mesure.station_mesure(id)
        );"""
        )
    elif data == "data_temperature":
        op.execute(
            f"""
        create table {region}.{data}(
        id serial NOT NULL
        , station_id int NOT NULL
        , nom varchar
        , annee integer
        , libelle_mois varchar
        , valeur double precision NOT NULL
        , PRIMARY KEY (id)
        , FOREIGN KEY(station_id) REFERENCES  auvergne_rhone_alpes_station_mesure.station_mesure(id)
        );"""
        )
    elif data == "data_enneigement":
        op.execute(
            f"""
        create table {region}.{data}(
        id serial NOT NULL
        , station_id int NOT NULL
        , nom varchar
        , annee integer
        , libelle_mois varchar
        , valeur double precision NOT NULL
        , date varchar
        , jour_mois integer NOT NULL
        , mois integer NOT NULL
        , PRIMARY KEY (id)
        , FOREIGN KEY(station_id) REFERENCES  auvergne_rhone_alpes_station_mesure.station_mesure(id)
        );"""
        )


def creer_table_data_mapping(data_mapping):
    op.create_table(
        data_mapping,
        Column(
            "station_id",
            Integer,
            ForeignKey(station_mesure_schema + ".station_mesure.id"),
            primary_key=True,
            comment="L'identifiant de la station de mesure",
            nullable=False,
        ),
        Column(
            "code_territoire",
            String,
            primary_key=True,
            comment="Code du territoire",
            nullable=False,
        ),
        Column("nom_territoire", String, comment="Nom du territoire ", nullable=False),
        Column("indicateur", String, comment="Nom indicateur"),
        schema=region,
    )


def creer_table_station_mesure(station_mesure):
    op.create_table(
        station_mesure,
        Column(
            "id",
            Integer,
            primary_key=True,
            comment="l'identifiant de la station de mesure",
        ),
        Column("code", String, comment="Code station de mesure", nullable=False),
        Column("nom", String, comment="Nom de la station de mesure", nullable=False),
        Column("si_homogeneiseex", Boolean),
        Column(
            "data_chaleur", Boolean, comment="Si la station fournit des données chaleur"
        ),
        Column(
            "data_temperature",
            Boolean,
            comment="Si la station fournit des données température",
        ),
        Column("data_gel", Boolean, comment="Si la station fournit des données gel"),
        Column(
            "data_enneignement",
            Boolean,
            comment="Si la station fournit des données enneigement",
        ),
        Column(
            "latitude",
            Float,
            comment="Coordonnée latitude de la station de mesure",
            nullable=False,
        ),
        Column(
            "longitude",
            Float,
            comment="Coordonnée longitude de la station de mesure",
            nullable=False,
        ),
        Column(
            "altitude",
            Float,
            comment="l'altitude de la station de meteo exprimé en mètre",
            nullable=False,
        ),
        Column("departement", Integer, comment="Code département"),
        schema=station_mesure_schema,
    )


def upgrade():
    creer_schema(station_mesure_schema)
    creer_table_station_mesure("station_mesure")
    creer_table_data_indicateur("data_gel")
    creer_table_data_indicateur("data_chaleur")
    creer_table_data_indicateur("data_temperature")
    creer_table_data_indicateur("data_enneigement")
    creer_table_data_mapping("mapping_data_gel")
    creer_table_data_mapping("mapping_data_chaleur")
    creer_table_data_mapping("mapping_data_temperature")
    creer_table_data_mapping("mapping_data_enneigement")


def downgrade():
    op.execute(f"drop schema {station_mesure_schema} cascade;")
    op.drop_table("data_gel", schema=region)
    op.drop_table("data_chaleur", schema=region)
    op.drop_table("data_temperature", schema=region)
    op.drop_table("data_gel", schema=region)
    op.drop_table("mapping_data_gel", schema=region)
    op.drop_table("mapping_data_chaleur", schema=region)
    op.drop_table("mapping_data_temperature", schema=region)
    op.drop_table("mapping_data_enneigement", schema=region)
