﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Ajout d'une table de consultations pour les autres pages

Revision ID: d483358c2a88
Revises: eb76cdaaf791
Create Date: 2022-08-17 09:17:13.671401

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d483358c2a88"
down_revision = "eb76cdaaf791"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "autres_pages",
        sa.Column(
            "id", sa.Integer, primary_key=True, comment="Identifiant"
        ),  # crée un identifiant incrémental
        sa.Column(
            "id_utilisateur",
            sa.Integer,
            sa.ForeignKey("consultations.ip_localisation.id", ondelete="CASCADE"),
        ),
        sa.Column("region", sa.String, nullable=False, comment="Nom de la région"),
        sa.Column(
            "page",
            sa.String,
            comment="Nom de la page à laquelle l'utilisateur.rice accède (suivi des indicateurs territoriaux ou suivi trajectoires)",
        ),
        sa.Column(
            "details",
            sa.String,
            comment="Autres informations relatives à la page consultées",
        ),
        sa.Column(
            "date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=False,
            comment="Date de l'activité",
        ),
        schema="consultations",
    )


def downgrade():
    op.drop_table(schema="consultations", table_name="autres_pages")
