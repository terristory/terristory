"""Add sankey name in configuration table

Revision ID: eaa21f6539d0
Revises: cccdea3d5af9
Create Date: 2023-01-11 11:14:58.236427

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "eaa21f6539d0"
down_revision = "cccdea3d5af9"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "donnees_sankey",
        sa.Column("sankey_name", sa.String(), server_default="Flux"),
        schema="meta",
    )
    op.add_column(
        "donnees_sankey",
        sa.Column("is_regional_default", sa.Boolean(), server_default=str("t")),
        schema="meta",
    )
    op.add_column(
        "donnees_sankey", sa.Column("layout_filename", sa.String()), schema="meta"
    )


def downgrade():
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.drop_column("sankey_name")
        batch_op.drop_column("is_regional_default")
        batch_op.drop_column("layout_filename")
