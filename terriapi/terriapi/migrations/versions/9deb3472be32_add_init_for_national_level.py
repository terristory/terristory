"""Add init for national level

Revision ID: 9deb3472be32
Revises: 26ad0930d576
Create Date: 2024-08-29 17:44:19.567930

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9deb3472be32"
down_revision = "26ad0930d576"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """INSERT INTO public.regions_configuration (id, env, url, label, theme, is_national, enabled) 
VALUES ('national', 'dev', 'national', 'National', 'vert-national', true, true)
ON CONFLICT (id, env) DO NOTHING;"""
    )


def downgrade():
    op.execute("""DELETE FROM public.regions_configuration WHERE id = 'national';""")
