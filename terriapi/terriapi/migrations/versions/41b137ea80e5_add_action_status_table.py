"""Add Action Status table

Revision ID: 41b137ea80e5
Revises: 375e3c6b2cf7
Create Date: 2023-03-10 16:13:11.399952

"""

import pandas as pd
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "41b137ea80e5"
down_revision = "375e3c6b2cf7"
branch_labels = None
depends_on = None

FILES_NAMES = """user_global_prix_energie
action10-ifer
action10-parametres-avances
action10-tarif-cre
action12-parametres-avances
action1_conso_max_etiquette
action1_taux_renovation_etiquette
action2_consommation_moyenne_unitaire
action3b-ifer
action3b-parametres-avances
action3b-tarif-cre
action7-indicateurs-carbon-energie
action7-lambdas
action7-parc-voiture-com-men
facteur-emission-polluants-ges-conso-atmo
global-conso-transport-commun
global_facteur_emission
parts_investissement_VA_ETP
user_action_params
user_action_params_years""".split(
    "\n"
)


def insert_static_params_from_csv():
    valeurs_columns = ["valeur_pourcentage", "valeur", "valeur_ifer"]
    REGIONS = ["auvergne-rhone-alpes", "occitanie", "paysdelaloire"]

    for i, region in enumerate(REGIONS):
        for file in FILES_NAMES:
            if file in (
                "parts_investissement_VA_ETP",
                "user_action_params",
                "user_action_params_years",
            ):
                file_name = file + ".csv"
            else:
                file_name = file.replace("_", "-") + ".csv"
            df = pd.read_csv("terriapi/data/" + region + "/" + file_name, sep=";")
            df["region"] = region
            for col in valeurs_columns:
                if col in df.columns:
                    if df[col].dtype == "object":
                        df[col] = df[col].str.replace(",", ".")
                        df[col] = pd.to_numeric(df[col])

            table_name = "params_" + file.replace("-", "_")
            if_exists = "append"
            if i == 0:
                if_exists = "replace"

            df.to_sql(
                table_name,
                op.get_bind(),
                schema="strategie_territoire",
                if_exists=if_exists,
            )

    mix_files_names = """mix_energie_industrie_region_2018_2050
mix_energie_agriculture_region_2018_2050""".split(
        "\n"
    )
    regions_with_mix = ["auvergne-rhone-alpes"]
    for i, region in enumerate(regions_with_mix):
        for file in mix_files_names:
            file_name = file + ".csv"
            df = pd.read_csv("terriapi/data/" + region + "/" + file_name, sep=";")
            df["region"] = region
            for col in valeurs_columns:
                if col in df.columns:
                    if df[col].dtype == "object":
                        df[col] = df[col].str.replace(",", ".")
                        df[col] = pd.to_numeric(df[col])

            table_name = "params_" + file.replace("-", "_")
            if_exists = "append"
            if i == 0:
                if_exists = "replace"

            df.to_sql(
                table_name,
                op.get_bind(),
                schema="strategie_territoire",
                if_exists=if_exists,
            )


def upgrade():
    insert_static_params_from_csv()

    # for action 8 => inheriting action 4 => thus we need some specific parameters
    op.execute(
        """
        INSERT INTO strategie_territoire.params_user_action_params (SELECT 112, 1, '8', region, libelle, nom, unite, valeur FROM strategie_territoire.params_user_action_params WHERE action ='4');

        INSERT INTO strategie_territoire.action (
            SELECT 
            id, naf, '8' as numero,
            (SELECT DISTINCT type FROM strategie_territoire.action WHERE numero = '8') as type,
            type_hyp, parametres, phase_projet, maillon,
            valeur, cout_global, unite, part_francaise, id_source, remuneration,
            (SELECT DISTINCT categorie FROM strategie_territoire.action WHERE numero = '8') as categorie, 
            (SELECT DISTINCT categorie_class FROM strategie_territoire.action WHERE numero = '8') as categorie_class,
            action_eco, 
            (SELECT DISTINCT ordre_categorie FROM strategie_territoire.action WHERE numero = '8') as ordre_categorie,
            (SELECT DISTINCT ordre_action FROM strategie_territoire.action WHERE numero = '8') as ordre_action,
            regions
            FROM strategie_territoire.action WHERE numero = '4'
        );

    """
    )

    op.rename_table("action", "action_details", schema="strategie_territoire")
    op.rename_table("action_params", "action_subactions", schema="strategie_territoire")

    op.create_table(
        "action",
        sa.Column(
            "action_numero", sa.String, primary_key=True, comment="Ancien numero"
        ),
        sa.Column(
            "action_key", sa.String, primary_key=True, comment="Action unique key"
        ),
        sa.Column("name", sa.String, comment="Action name", server_default=""),
        sa.Column("categorie", sa.String, comment="Action name"),
        sa.Column("category_class", sa.String, comment="Action name"),
        sa.Column("category_order", sa.Integer, comment="Category order"),
        sa.Column("action_order", sa.Integer, comment="Action order"),
        schema="strategie_territoire",
    )

    op.create_table(
        "action_status",
        sa.Column("region", sa.String, primary_key=True, comment="Region"),
        sa.Column(
            "action_numero", sa.String, primary_key=True, comment="Ancien numero"
        ),
        sa.Column(
            "action_key", sa.String, primary_key=True, comment="Action unique key"
        ),
        sa.Column(
            "enabled", sa.Boolean, comment="Nom du fichier", server_default="false"
        ),
        sa.Column("nb_failings", sa.Integer, comment="Nb of fails recently"),
        sa.Column("impact_eco", sa.Boolean, comment="Has enabled economic impacts"),
        sa.Column("avec_params", sa.Boolean, comment="Needs params"),
        sa.Column("first_year", sa.Integer, comment="First year inside current region"),
        sa.Column("last_year", sa.Integer, comment="Last year inside current region"),
        schema="strategie_territoire",
    )

    # Create a slugify function inside postgresql
    # cf. https://gist.github.com/abn/779166b0c766ce67351c588489831852
    op.execute(
        """
        CREATE EXTENSION IF NOT EXISTS unaccent;

        -- create the function in the public schema
        CREATE OR REPLACE FUNCTION public.slugify(
        v TEXT
        ) RETURNS TEXT
        LANGUAGE plpgsql
        STRICT IMMUTABLE AS
        $function$
        BEGIN
        -- 1. trim trailing and leading whitespaces from text
        -- 2. remove accents (diacritic signs) from a given text
        -- 3. lowercase unaccented text
        -- 4. replace non-alphanumeric (excluding hyphen, underscore) with a hyphen
        -- 5. trim leading and trailing hyphens
        RETURN trim(BOTH '-' FROM regexp_replace(lower(unaccent(trim(v))), '[^a-z0-9\\-_]+', '-', 'gi'));
        END;
        $function$;
    """
    )

    with op.batch_alter_table(
        "action_details", schema="strategie_territoire"
    ) as batch_op:
        batch_op.alter_column("categorie_class", new_column_name="category_class")
        batch_op.alter_column("ordre_categorie", new_column_name="category_order")
        batch_op.alter_column("ordre_action", new_column_name="action_order")

    # Use it to fill action table with default action key
    op.execute(
        """
        INSERT INTO strategie_territoire.action
        SELECT numero as action_numero, slugify(type) as action_key, type as name,
        categorie, category_class, category_order, action_order
        FROM strategie_territoire.action_details
        GROUP BY numero, type, categorie, category_class, category_order, action_order
    """
    )
    op.execute(
        """
        INSERT INTO strategie_territoire.action_status
        SELECT unnest(d.regions) as region, d.numero as action_numero, slugify(d.type) as action_key, 
        true as enabled, 0, m.impact_eco, m.avec_params, NULL as first_year, NULL as last_year
        FROM strategie_territoire.action_details d
        LEFT JOIN strategie_territoire.meta_action m ON m.numero = d.numero
        GROUP BY region, d.numero, d.type, m.impact_eco, m.avec_params;
    """
    )

    with op.batch_alter_table(
        "action_subactions", schema="strategie_territoire"
    ) as batch_op:
        batch_op.add_column(
            sa.Column("action_key", sa.String, primary_key=True, server_default="")
        )


def downgrade():
    for file in FILES_NAMES:
        table_name = "params_" + file.replace("-", "_")
        op.drop_table(table_name, schema="strategie_territoire", if_exists=True)

    op.drop_table("action", schema="strategie_territoire")
    op.drop_table("action_status", schema="strategie_territoire")
    op.rename_table("action_details", "action", schema="strategie_territoire")
    # we remove parameters added for action 8
    op.execute(
        """DELETE FROM strategie_territoire.action WHERE numero ='8' AND id < 100;"""
    )
    op.rename_table("action_subactions", "action_params", schema="strategie_territoire")

    with op.batch_alter_table("action", schema="strategie_territoire") as batch_op:
        batch_op.alter_column("category_class", new_column_name="categorie_class")
        batch_op.alter_column("category_order", new_column_name="ordre_categorie")
        batch_op.alter_column("action_order", new_column_name="ordre_action")

    with op.batch_alter_table(
        "action_params", schema="strategie_territoire"
    ) as batch_op:
        batch_op.drop_column("action_key")
