"""Remove column id from supra_goals_values

Revision ID: 1e6b1174be63
Revises: bcd171348a27
Create Date: 2025-01-15 10:19:51.783096

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "1e6b1174be63"
down_revision = "bcd171348a27"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        ALTER TABLE strategie_territoire.supra_goals_values DROP COLUMN id;
        """
    )


def downgrade():
    # This "id" column seems unsignificant. So we don't need to repopulate it
    op.execute(
        """
        ALTER TABLE strategie_territoire.supra_goals_values ADD COLUMN id VARCHAR;
        """
    )
