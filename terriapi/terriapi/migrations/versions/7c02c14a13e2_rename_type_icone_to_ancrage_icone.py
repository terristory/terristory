"""Rename type_icone to ancrage_icone

Revision ID: 7c02c14a13e2
Revises: a272014697e5
Create Date: 2023-02-08 12:17:34.443779

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7c02c14a13e2"
down_revision = "a272014697e5"
branch_labels = None
depends_on = None

schemas = [
    "auvergne_rhone_alpes",
    "occitanie",
    "nouvelle_aquitaine",
    "bretagne",
    "paysdelaloire",
    "corse",
]


def upgrade():
    for schema in schemas:
        op.execute(
            f"""
            ALTER TABLE {schema}_poi.layer ADD COLUMN IF NOT EXISTS type_icone varchar;
            ALTER TABLE {schema}_poi.layer RENAME COLUMN type_icone TO ancrage_icone;
            UPDATE {schema}_poi.layer SET ancrage_icone = 'milieu_bas' WHERE ancrage_icone = 'marqueur';
            """
        )


def downgrade():
    for schema in schemas:
        op.execute(
            f"""
            UPDATE {schema}_poi.layer SET ancrage_icone = 'marqueur' WHERE ancrage_icone = 'milieu_bas';
            ALTER TABLE {schema}_poi.layer RENAME COLUMN ancrage_icone TO type_icone;
            """
        )
