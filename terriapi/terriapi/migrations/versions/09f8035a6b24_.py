"""empty message

Revision ID: 09f8035a6b24
Revises: 7e95c851455c, b4ed79c48197
Create Date: 2024-06-07 10:10:12.778400

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "09f8035a6b24"
down_revision = ("7e95c851455c", "b4ed79c48197")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
