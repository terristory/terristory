"""Change impact settings keys

Revision ID: dc0c355fafb9
Revises: fc6b4a6d6f90
Create Date: 2024-08-23 14:15:39.644763

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "dc0c355fafb9"
down_revision = "fc6b4a6d6f90"
branch_labels = None
depends_on = None

new_keys = {
    "consommation_ener-secteur": "impact-ener-sector",
    "consommation_ener-energie": "impact-ener-energy",
    "enr_production-energie": "impact-ener-prod",
    "emission_ges-secteur": "impact-carbon-sector",
    "emission_ges-energie": "impact-carbon-energy",
    "prod-conso": "impact-prod-conso",
    "covnm-secteur": "impact-atmo",
    "covnm-energie": "impact-atmo",
    "nh3-secteur": "impact-atmo",
    "nh3-energie": "impact-atmo",
    "nox-secteur": "impact-atmo",
    "nox-energie": "impact-atmo",
    "pm10-secteur": "impact-atmo",
    "pm10-energie": "impact-atmo",
    "pm25-secteur": "impact-atmo",
    "pm25-energie": "impact-atmo",
    "sox-secteur": "impact-atmo",
    "sox-energie": "impact-atmo",
    "jobs": "impact-tax",
    "valueAdded": "impact-tax",
    "energyBill": "impact-facture-ener",
    "taxRevenue": "impact-tax",
    "investment": "impact-tax",
}


def upgrade():
    for new_key, old_key in new_keys.items():
        op.execute(
            f"""INSERT INTO strategie_territoire.impacts_configuration 
              (SELECT '{new_key}', region, decimal, enabled 
              FROM strategie_territoire.impacts_configuration
              WHERE type = '{old_key}')"""
        )
    op.execute(
        f"DELETE FROM strategie_territoire.impacts_configuration WHERE type LIKE 'impact-%'"
    )


def downgrade():
    duplicates = []
    for new_key, old_key in new_keys.items():
        if old_key in duplicates:
            continue
        op.execute(
            f"""INSERT INTO strategie_territoire.impacts_configuration 
              (SELECT '{old_key}', region, decimal, enabled 
              FROM strategie_territoire.impacts_configuration
              WHERE type = '{new_key}')"""
        )
        duplicates.append(old_key)
    op.execute(
        f"DELETE FROM strategie_territoire.impacts_configuration WHERE type NOT LIKE 'impact-%'"
    )
