"""Add passage tables for other regions

Revision ID: aa1a8897eef5
Revises: 28da708ce3b7
Create Date: 2023-06-28 15:10:25.464111

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "aa1a8897eef5"
down_revision = "28da708ce3b7"
branch_labels = None
depends_on = None

POLLUTANTS = ["nox", "covnm", "pm10", "pm25", "nh3", "sox"]
CONFID_RELATED_TABLES = ["CONSUMPTION", "EMISSIONS"]
TABLES = ["conso_energetique", "emission_ges"]


def upgrade_occitanie():
    op.execute(
        """
        INSERT INTO strategie_territoire.passage_table 
        VALUES('occitanie', 'DataSet.PRODUCTION', 'prod_enr', 'table'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd', 'type_prod_enr', 'column');

        INSERT INTO strategie_territoire.passage_table 
        VALUES('occitanie', 'DataSet.PRODUCTION/RenewableProd.SOLAR_THER', '12', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.SOLAR_PV', '2', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.WIND', '3', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.HYDRO_LOW', '1', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.HYDRO_HIGH', '1', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_ELEC', '4', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_THER', '5', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_INJEC', '13', 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.BIOMASS_SOLID', '11', 'value');

        INSERT INTO strategie_territoire.passage_table 
        VALUES('occitanie', 'DataSet.VEHICLE_FLEET', 'parc_auto_statique', 'table'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleType', 'type_veh', 'column'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleType.PASSENGER_CARS', '1', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleType.TRUCKS', '2', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleType.BUSES_COACHES', '3', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleType.LIGHT_COMM', '4', 'value'),
        
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel', 'energie_carburant', 'column'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.DIESEL', '1', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.GASOLINE', '2', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.GAS', '3', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELEC_H2', '4', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_DIESEL', '5', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_GASOLINE', '6', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELECTRIC', '7', 'value'),
        ('occitanie', 'DataSet.VEHICLE_FLEET/VehicleFuel.UNKNOWN', '8', 'value');
    """
    )

    op.execute(
        f"""INSERT INTO strategie_territoire.passage_table 
        (
            SELECT 'occitanie', key, match, association_type 
            FROM strategie_territoire.passage_table
            WHERE (key LIKE 'ParamDataSet.GES_FACTORS%' 
                OR key LIKE 'ParamDataSet.ATMO_GES_FACTORS%' 
                OR key LIKE 'ParamDataSet.PRICES%' 
                OR key LIKE 'ParamDataSet.FORCED_MIX%')
            AND region = 'auvergne-rhone-alpes'
        )
        ;"""
    )
    for key, table in zip(CONFID_RELATED_TABLES, TABLES):
        op.execute(
            f"""INSERT INTO strategie_territoire.passage_table 
            VALUES('occitanie', 'DataSet.{key}', '{table}', 'table');"""
        )

        op.execute(
            f"""
            INSERT INTO strategie_territoire.passage_table 
            VALUES('occitanie', 'DataSet.{key}/Sector', 'secteur', 'column'),
            ('occitanie', 'DataSet.{key}/Commodity', 'energie', 'column');
            INSERT INTO strategie_territoire.passage_table 
            VALUES('occitanie', 'DataSet.{key}/Commodity.ELECTRICITY', '2', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.NON_ENERGETIC', '4', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.OIL', '5', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.RENEWABLE_HEAT', '3', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.BIOFUELS', '6', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.GAS', '7', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.UNKNOWN', '8', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.URBAN_HEAT_COOL', '9', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.NON_RENEWABLE_HEAT', '11', 'value'),
            ('occitanie', 'DataSet.{key}/Commodity.COAL', '1', 'value');
            INSERT INTO strategie_territoire.passage_table 
            VALUES('occitanie', 'DataSet.{key}/Sector.RESIDENTIAL', '1', 'value'),
            ('occitanie', 'DataSet.{key}/Sector.AGRICULTURE', '2', 'value'),
            ('occitanie', 'DataSet.{key}/Sector.SERVICES', '3', 'value'),
            ('occitanie', 'DataSet.{key}/Sector.INDUSTRY_WITHOUT_ENERGY', '5', 'value'),
            ('occitanie', 'DataSet.{key}/Sector.ROAD_TRANSPORT', '6', 'value'),
            ('occitanie', 'DataSet.{key}/Sector.ENERGY_INDUSTRY', '8', 'value');"""
        )

        if "POLLUTANT" not in key:
            op.execute(
                f"""
                INSERT INTO strategie_territoire.passage_table 
                VALUES
                ('occitanie', 'DataSet.{key}/Usage', 'usage', 'column'),
                ('occitanie', 'DataSet.{key}/Usage.HEATING', '1', 'value'),
                ('occitanie', 'DataSet.{key}/Usage.HOT_WATER', '2', 'value'),
                ('occitanie', 'DataSet.{key}/Usage.COOKING', '3', 'value'),
                ('occitanie', 'DataSet.{key}/Usage.SPECIFIC_ELEC', '4', 'value'),
                ('occitanie', 'DataSet.{key}/Usage.OTHERS', '5', 'value'),
                ('occitanie', 'DataSet.{key}/Usage.COOLING', '11', 'value');"""
            )


def upgrade_paysdelaloire():
    op.execute(
        """
        INSERT INTO strategie_territoire.passage_table 
        VALUES('paysdelaloire', 'DataSet.PRODUCTION', 'prod_enr', 'table'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd', 'type_prod_enr', 'column');

        INSERT INTO strategie_territoire.passage_table 
        VALUES
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.HYDRO_LOW', '4', 'value'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.HYDRO_HIGH', '4', 'value'),
        
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.SOLAR_THER', '8', 'value'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.SOLAR_PV', '7', 'value'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.WIND', '2', 'value'),

        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_ELEC', '5', 'value'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_THER', '5', 'value'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.BIOGAS_INJEC', '5', 'value'),

        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.BIOMASS_SOLID', '1', 'value');

        INSERT INTO strategie_territoire.passage_table 
        VALUES('paysdelaloire', 'DataSet.VEHICLE_FLEET', 'parc_auto_statique', 'table'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleType', 'type_veh', 'column'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleType.PASSENGER_CARS', '1', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleType.TRUCKS', '2', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleType.BUSES_COACHES', '3', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleType.LIGHT_COMM', '4', 'value'),
        
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel', 'energie_carburant', 'column'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.DIESEL', '1', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.GASOLINE', '2', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.GAS', '3', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELEC_H2', '4', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_DIESEL', '5', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_GASOLINE', '6', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELECTRIC', '7', 'value'),
        ('paysdelaloire', 'DataSet.VEHICLE_FLEET/VehicleFuel.UNKNOWN', '8', 'value');
    """
    )

    op.execute(
        f"""INSERT INTO strategie_territoire.passage_table 
        (
            SELECT 'paysdelaloire', key, match, association_type 
            FROM strategie_territoire.passage_table
            WHERE (key LIKE 'ParamDataSet.GES_FACTORS%' 
                OR key LIKE 'ParamDataSet.ATMO_GES_FACTORS%' 
                OR key LIKE 'ParamDataSet.PRICES%' 
                OR key LIKE 'ParamDataSet.FORCED_MIX%')
            AND region = 'auvergne-rhone-alpes'
        )
        ;"""
    )
    for key, table in zip(CONFID_RELATED_TABLES, TABLES):
        op.execute(
            f"""INSERT INTO strategie_territoire.passage_table 
            VALUES('paysdelaloire', 'DataSet.{key}', '{table}', 'table');"""
        )

        op.execute(
            f"""
            INSERT INTO strategie_territoire.passage_table 
            VALUES('paysdelaloire', 'DataSet.{key}/Sector', 'secteur', 'column'),
            ('paysdelaloire', 'DataSet.{key}/Commodity', 'energie', 'column');
            INSERT INTO strategie_territoire.passage_table 
            VALUES('paysdelaloire', 'DataSet.{key}/Commodity.ELECTRICITY', '2', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.NON_ENERGETIC', '4', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.OIL', '5', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.RENEWABLE_HEAT', '3', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.RENEWABLE_HEAT', '8', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.BIOFUELS', '6', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.GAS', '7', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.URBAN_HEAT_COOL', '9', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.NON_RENEWABLE_HEAT', '11', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Commodity.COAL', '1', 'value');
            INSERT INTO strategie_territoire.passage_table 
            VALUES('paysdelaloire', 'DataSet.{key}/Sector.RESIDENTIAL', '1', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Sector.AGRICULTURE', '2', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Sector.SERVICES', '3', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Sector.INDUSTRY_WITHOUT_ENERGY', '5', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Sector.ROAD_TRANSPORT', '6', 'value'),
            ('paysdelaloire', 'DataSet.{key}/Sector.ENERGY_INDUSTRY', '8', 'value');"""
        )

        if "POLLUTANT" not in key:
            op.execute(
                f"""
                INSERT INTO strategie_territoire.passage_table 
                VALUES
                ('paysdelaloire', 'DataSet.{key}/Usage', 'usage', 'column'),
                ('paysdelaloire', 'DataSet.{key}/Usage.HEATING', '1', 'value'),
                ('paysdelaloire', 'DataSet.{key}/Usage.HOT_WATER', '2', 'value'),
                ('paysdelaloire', 'DataSet.{key}/Usage.COOKING', '3', 'value'),
                ('paysdelaloire', 'DataSet.{key}/Usage.SPECIFIC_ELEC', '4', 'value'),
                ('paysdelaloire', 'DataSet.{key}/Usage.COOLING', '11', 'value');"""
            )


def upgrade():
    upgrade_occitanie()
    upgrade_paysdelaloire()


def downgrade():
    op.execute(
        """DELETE FROM strategie_territoire.passage_table WHERE region IN ('occitanie', 'paysdelaloire', 'nouvelle-aquitaine');"""
    )
