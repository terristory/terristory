﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Ajout champ d'action de sankey

Revision ID: 66a036c8f1a0
Revises: 7cbb47e6dfff
Create Date: 2022-09-16 09:11:22.464272

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "66a036c8f1a0"
down_revision = "7cbb47e6dfff"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration", sa.Column("ui_show_sankey", sa.Boolean, default=False)
    )


def downgrade():
    with op.batch_alter_table("regions_configuration") as batch_op:
        batch_op.drop_column("ui_show_sankey")
