﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add rgpd parameter for user

Revision ID: 618a427d1d0c
Revises: a6c552c3d1a5
Create Date: 2022-09-27 09:28:49.402661

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "618a427d1d0c"
down_revision = "a6c552c3d1a5"
branch_labels = None
depends_on = None

TABLENAME = "utilisateur"
SCHEMA = "public"


def upgrade():
    op.add_column(
        TABLENAME,
        sa.Column("rgpd", sa.String, default="Accord a priori"),
        schema=SCHEMA,
    )


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("rgpd")
