﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Change sources and producers system

Revision ID: 60919715142b
Revises: 9f5b0900b0db
Create Date: 2022-12-16 09:58:31.323071

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "60919715142b"
down_revision = "9f5b0900b0db"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.alter_column(
            "analysis_producer", new_column_name="credits_analysis_producers"
        )
        batch_op.alter_column("analysis_source", new_column_name="credits_data_sources")
        batch_op.add_column(sa.Column("credits_data_producers", sa.JSON))


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.alter_column(
            "credits_analysis_producers", new_column_name="analysis_producer"
        )
        batch_op.alter_column("credits_data_sources", new_column_name="analysis_source")
        batch_op.drop_column("credits_data_producers")
