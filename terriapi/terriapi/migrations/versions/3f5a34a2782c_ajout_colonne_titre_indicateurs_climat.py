﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout-colonne-titre-indicateurs-climat

Revision ID: 3f5a34a2782c
Revises: a6fef8ee505c
Create Date: 2021-09-02 17:43:27.943281

"""
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "3f5a34a2782c"
down_revision = "a6fef8ee505c"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("indicateur", Column("titre_dans_infobulle", String()), schema="meta")


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("titre_dans_infobulle")
