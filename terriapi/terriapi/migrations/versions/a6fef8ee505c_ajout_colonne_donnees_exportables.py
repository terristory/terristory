﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout-colonne-donnees-exportables

Revision ID: a6fef8ee505c
Revises: 2235ce301cec
Create Date: 2021-08-27 17:56:43.688538

"""
from alembic import op
from sqlalchemy import Boolean, Column

# revision identifiers, used by Alembic.
revision = "a6fef8ee505c"
down_revision = "2235ce301cec"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("indicateur", Column("donnees_exportables", Boolean), schema="meta")


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("donnees_exportables")
