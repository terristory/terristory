﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Ajout table historique sur toutes régions

Revision ID: b1b44dc7b8e9
Revises: 60dd818543c2
Create Date: 2022-09-13 13:13:43.706085

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "b1b44dc7b8e9"
down_revision = "60dd818543c2"
branch_labels = None
depends_on = None

schemas = [
    "auvergne_rhone_alpes",
    "occitanie",
    "nouvelle_aquitaine",
    "bretagne",
    "paysdelaloire",
    "corse",
]


def upgrade():
    for schema in schemas:
        op.create_table(
            "historique_indicateurs",
            sa.Column("id", sa.Integer, primary_key=True),
            sa.Column("nom_donnee", sa.String, comment="Nom de la donnée"),
            sa.Column(
                "utilisateur",
                sa.String,
                sa.ForeignKey(
                    "utilisateur.mail", onupdate="CASCADE", ondelete="SET NULL"
                ),
                comment="Utilisateur",
            ),
            sa.Column("action", sa.String, comment="Action effectuée"),
            sa.Column("date_maj", sa.DateTime, comment="Date et heure de l'action"),
            sa.Column(
                "autres_informations",
                sa.Text,
                comment="Autres informations relatives à cette MAJ",
            ),
            schema=schema,
        )
        if (
            schema != "auvergne_rhone_alpes"
        ):  # il existe déjà une table auvergne_rhone_alpes_poi (précédente migration)
            op.create_table(
                "historique",
                sa.Column("id", sa.Integer, nullable=False),
                sa.Column("layer", sa.String, nullable=False),
                sa.Column(
                    "utilisateur",
                    sa.String,
                    sa.ForeignKey(
                        "utilisateur.mail", onupdate="CASCADE", ondelete="SET NULL"
                    ),
                    comment="Utilisateur",
                ),
                sa.Column("action", sa.String, comment="Action effectuée"),
                sa.Column(
                    "properties_precedent", sa.JSON, comment="Propriétés précédentes"
                ),
                sa.Column(
                    "properties_courant", sa.JSON, comment="Propriétés courantes"
                ),
                sa.Column(
                    "mise_a_jour", sa.TIMESTAMP, comment="Date et heure de l'action"
                ),
                schema=schema + "_poi",
            )
            ajout_colonne_geometry(schema + "_poi", "historique")


def ajout_colonne_geometry(schema, table):
    op.execute(
        f"alter table {schema}.{table} add column geom_precedent geometry(MultiPolygon,3857)"
    )
    op.execute(
        f"alter table {schema}.{table} add column geom_courant geometry(MultiPolygon,3857)"
    )


def downgrade():
    for schema in schemas:
        op.drop_table("historique_indicateurs", schema=schema)
        if schema != "auvergne_rhone_alpes":
            op.drop_table("historique", schema=schema + "_poi")
