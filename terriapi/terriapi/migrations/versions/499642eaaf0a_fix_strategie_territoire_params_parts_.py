"""Fix strategie_territoire.params_parts_investissement_VA_ETP

Revision ID: 499642eaaf0a
Revises: f1dcfe2f59f1
Create Date: 2023-11-13 12:41:14.753915

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "499642eaaf0a"
down_revision = "f1dcfe2f59f1"
branch_labels = None
depends_on = None


def upgrade():
    # redefine column types on VA_ETP investment table
    op.execute(
        """ALTER TABLE strategie_territoire."params_parts_investissement_VA_ETP" ALTER COLUMN part_invess TYPE float USING REPLACE(part_invess, ',', '.')::float"""
    )
    op.execute(
        """ALTER TABLE strategie_territoire."params_parts_investissement_VA_ETP" ALTER COLUMN "part_VA_ETP" TYPE float USING REPLACE("part_VA_ETP", ',', '.')::float"""
    )
    # remove useless column and reset index on parameters tables
    op.execute(
        """
        ALTER TABLE strategie_territoire.params_action10_ifer DROP COLUMN IF EXISTS level_0;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action10_ifer_level_0;
        CREATE INDEX ix_strategie_territoire_params_action10_ifer_level_0 ON strategie_territoire.params_action10_ifer (index);

        ALTER TABLE strategie_territoire.params_action10_parametres_avances DROP COLUMN IF EXISTS level_0;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action10_parametres_avan_bfc5;
        CREATE INDEX ix_strategie_territoire_params_action10_parametres_avan_bfc5 ON strategie_territoire.params_action10_parametres_avances (id);

        ALTER TABLE strategie_territoire.params_action10_tarif_cre DROP COLUMN IF EXISTS level_0;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action10_tarif_cre_level_0;
        CREATE INDEX ix_strategie_territoire_params_action10_tarif_cre_level_0 ON strategie_territoire.params_action10_tarif_cre (index);

        ALTER TABLE strategie_territoire.params_action3b_ifer DROP COLUMN IF EXISTS level_0;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action3b_ifer_level_0;
        CREATE INDEX ix_strategie_territoire_params_action3b_ifer_level_0 ON strategie_territoire.params_action3b_ifer (index);

        ALTER TABLE strategie_territoire.params_action3b_parametres_avances DROP COLUMN IF EXISTS level_0;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action3b_parametres_avan_a9fa;
        CREATE INDEX ix_strategie_territoire_params_action3b_parametres_avan_a9fa ON strategie_territoire.params_action3b_parametres_avances (id);

        ALTER TABLE strategie_territoire.params_action3b_tarif_cre DROP COLUMN IF EXISTS level_0;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action3b_tarif_cre_level_0;
        CREATE INDEX ix_strategie_territoire_params_action3b_tarif_cre_level_0 ON strategie_territoire.params_action3b_tarif_cre (index);
        """
    )
    # remove useless column and reset index on parameters tables
    op.execute(
        """
        ALTER TABLE strategie_territoire.params_action10_parametres_avances DROP COLUMN IF EXISTS index;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action10_parametres_avan_bfc5;
        CREATE INDEX ix_strategie_territoire_params_action10_parametres_avan_bfc5 ON
        strategie_territoire.params_action10_parametres_avances(id);

        ALTER TABLE strategie_territoire.params_action12_parametres_avances DROP COLUMN IF EXISTS index;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action12_parametres_avan_40c9;
        CREATE INDEX ix_strategie_territoire_params_action12_parametres_avan_40c9 ON
        strategie_territoire.params_action12_parametres_avances(id);

        ALTER TABLE strategie_territoire.params_action2_consommation_moyenne_unitaire DROP COLUMN IF EXISTS index;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action2_consommation_moy_ada1;
        CREATE INDEX ix_strategie_territoire_params_action2_consommation_moy_ada1 ON
        strategie_territoire.params_action2_consommation_moyenne_unitaire(id);

        ALTER TABLE strategie_territoire.params_action3b_parametres_avances DROP COLUMN IF EXISTS index;
        DROP INDEX IF EXISTS strategie_territoire.ix_strategie_territoire_params_action3b_parametres_avan_a9fa;
        CREATE INDEX ix_strategie_territoire_params_action3b_parametres_avan_a9fa ON
        strategie_territoire.params_action3b_parametres_avances(id);
        """
    )


def downgrade():
    op.execute(
        """ALTER TABLE strategie_territoire."params_parts_investissement_VA_ETP" ALTER COLUMN part_invess TYPE text USING REPLACE(part_invess::text, '.', ',')::text"""
    )
    op.execute(
        """ALTER TABLE strategie_territoire."params_parts_investissement_VA_ETP" ALTER COLUMN "part_VA_ETP" TYPE text USING REPLACE("part_VA_ETP"::text, '.', ',')::text"""
    )
