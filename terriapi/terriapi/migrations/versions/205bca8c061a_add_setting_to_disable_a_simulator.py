"""Add setting to disable a simulator

Revision ID: 205bca8c061a
Revises: 8ccaa14e2cbc
Create Date: 2025-01-28 15:54:31.358894

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "205bca8c061a"
down_revision = "8ccaa14e2cbc"
branch_labels = None
depends_on = None


def upgrade():
    # create an array with the current mobility related option inside the new array of options related to simulators
    op.execute(
        "ALTER TABLE regions_configuration RENAME COLUMN ui_show_mobility_simulator TO ui_show_simulators"
    )
    op.execute(
        """
               ALTER TABLE regions_configuration ALTER COLUMN ui_show_simulators TYPE jsonb USING jsonb_build_object(
                    'enr', false,
                    'mobility', COALESCE(ui_show_simulators, false)
                )
               """
    )


def downgrade():
    op.execute(
        "ALTER TABLE regions_configuration ALTER COLUMN ui_show_simulators TYPE boolean USING (ui_show_simulators->>'mobility')::boolean"
    )
    op.execute(
        "ALTER TABLE regions_configuration RENAME COLUMN ui_show_simulators TO ui_show_mobility_simulator"
    )
