"""add type column to simulator

Revision ID: 6a0a289c9f56
Revises: bcd171348a27
Create Date: 2024-11-13 16:47:01.167347

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6a0a289c9f56"
down_revision = "bcd171348a27"
branch_labels = None
depends_on = None


SCHEMA = "simulator"
TABLENAME_CREATE = ["simulator_level", "generic_interm_param", "generic_input_param"]

TABLENAME_UPDATE = ["category_lever", "simulator_impact"]
TABLENAME_DELETE = ["passage_table", "passage_table_id"]


def upgrade():
    # Add 'thematic' column to the tables in TABLENAME_CREATE
    for table in TABLENAME_CREATE:
        op.add_column(
            table,
            sa.Column(
                "thematic", sa.String, nullable=True, comment="Type de simulateur"
            ),
            schema=SCHEMA,
        )

        # Populate the new column
        op.execute(f"""UPDATE {SCHEMA}.{table} SET thematic = 'mobility'""")

        # Set the column to NOT NULL
        with op.batch_alter_table(table, schema=SCHEMA) as batch_op:
            batch_op.alter_column("thematic", existing_type=sa.String, nullable=False)

    # Set 'thematic' column to NOT NULL in the tables in TABLENAME_UPDATE
    for table in TABLENAME_UPDATE:
        with op.batch_alter_table(table, schema=SCHEMA) as batch_op:
            batch_op.alter_column("thematic", existing_type=sa.String, nullable=False)

    # Drop the existing primary key on 'region' for simulator_level
    op.drop_constraint(
        "simulator_level_pkey", "simulator_level", schema=SCHEMA, type_="primary"
    )

    # Create a new primary key on 'region', 'level', and 'thematic' for simulator_level
    op.create_primary_key(
        "simulator_level_pkey",
        "simulator_level",
        ["region", "level", "thematic"],
        schema=SCHEMA,
    )

    # Drop the existing primary key on 'type' for simulator_impact
    op.drop_constraint(
        "simulator_impact_pkey", "simulator_impact", schema=SCHEMA, type_="primary"
    )

    # Create a new composite primary key on 'type' and 'thematic' for simulator_impact
    op.create_primary_key(
        "simulator_impact_pkey",
        "simulator_impact",
        ["type", "thematic"],
        schema=SCHEMA,
    )

    # Clean-up simulator schema
    for table in TABLENAME_DELETE:
        op.execute(f"DROP TABLE IF EXISTS {SCHEMA}.{table}")


def downgrade():
    # Drop the new primary key for simulator_level
    op.drop_constraint(
        "simulator_level_pkey", "simulator_level", schema=SCHEMA, type_="primary"
    )

    # Re-create the original primary key on 'region' for simulator_level
    op.create_primary_key(
        "simulator_level_pkey", "simulator_level", ["region"], schema=SCHEMA
    )

    # Drop the composite primary key if it exists for simulator_impact
    op.drop_constraint(
        "simulator_impact_pkey", "simulator_impact", schema=SCHEMA, type_="primary"
    )

    # Re-create the original primary key on 'type' if it exists fro simulator_impact
    op.create_primary_key(
        "simulator_impact_pkey", "simulator_impact", ["type"], schema=SCHEMA
    )

    # Drop the 'thematic' column from the tables in TABLENAME_CREATE
    for table in TABLENAME_CREATE:
        with op.batch_alter_table(table, schema=SCHEMA) as batch_op:
            batch_op.drop_column("thematic")

    # Revert the NOT NULL constraint in TABLENAME_UPDATE
    for table in TABLENAME_UPDATE:
        with op.batch_alter_table(table, schema=SCHEMA) as batch_op:
            batch_op.alter_column("thematic", existing_type=sa.String, nullable=True)
