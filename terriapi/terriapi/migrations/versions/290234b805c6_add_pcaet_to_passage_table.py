"""Add pcaet to passage table

Revision ID: 290234b805c6
Revises: 5fe5ccb9aad6
Create Date: 2023-09-27 17:58:48.417440

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "290234b805c6"
down_revision = "5fe5ccb9aad6"
branch_labels = None
depends_on = None

TABLE = "strategie_territoire.passage_table"


def upgrade():
    table_association = [
        ("EMISSIONS", "emission_ges"),
        ("CONSUMPTION", "consommation_ener"),
        ("PRODUCTION", "enr_production"),
        ("POLLUTANT_PM10", "pm10"),
        ("POLLUTANT_PM25", "pm25"),
        ("POLLUTANT_NOX", "nox"),
        ("POLLUTANT_SOX", "sox"),
        ("POLLUTANT_COVNM", "covnm"),
        ("POLLUTANT_NH3", "nh3"),
    ]
    column_association = [
        ("Sector", "category"),
        ("RenewableProd", "category"),
    ]
    sector_value_association = [
        ("RESIDENTIAL", 1),
        ("SERVICES", 2),
        ("ROAD_TRANSPORT", 3),
        ("OTHER_TRANSPORT", 4),  # occ => ?
        ("AGRICULTURE", 5),
        ("WASTE_MANAGEMENT", 6),  # occ => ?
        ("INDUSTRY_WITHOUT_ENERGY", 7),
        ("ENERGY_INDUSTRY", 8),
    ]
    renewable_value_association = [
        ("WIND", 1),
        ("SOLAR_PV", 2),
        ("SOLAR_THERMODYNAMIC", 3),  # to add or rename to PV
        ("HYDRO_HIGH", 4),  # doesnt make the difference btwn high and low hydro
        ("BIOMASS_SOLID", 5),  # elec
        ("BIOGAS_ELEC", 6),
        ("GEOTHERMY", 7),  # elec, to add
        ("BIOMASS_SOLID", 8),  # therm
        ("HEAT_PUMP", 9),  # to add
        ("GEOTHERMY", 10),  # therm, to add
        ("SOLAR_THER", 11),
        ("BIOGAS_THER", 12),
        ("BIOGAS_INJEC", 13),
        ("BIOFUEL", 14),  # to add
    ]

    sql = f"insert into {TABLE} (region, key, match, association_type, aggregation) values"
    for table_key, table_val in table_association:
        sql += f"('pcaet', 'DataSet.{table_key}', '{table_val}', 'table', 'keep'),"
        col_key = "RenewableProd" if table_key == "PRODUCTION" else "Sector"
        col_val = next(val for key, val in column_association if key == col_key)
        sql += f"('pcaet', 'DataSet.{table_key}/{col_key}', '{col_val}', 'column', 'keep'),"
        category_association = (
            renewable_value_association
            if col_key == "RenewableProd"
            else sector_value_association
        )
        for key, val in category_association:
            sql += f"('pcaet', 'DataSet.{table_key}/{col_key}.{key}', '{val}', 'value', 'keep'),"

    sql = sql[:-1] + ";"  # replace the last comma by a semi-colon
    op.execute(sql)

    # Add missing categories in indicators passage tables
    op.execute(
        f"insert into {TABLE} (region, key, match, association_type, aggregation) values"
        + " ".join(
            f"('{region}', 'DataSet.{table}/Sector.OTHER_TRANSPORT', '7', 'value', 'keep'),"
            for region in ["auvergne-rhone-alpes", "paysdelaloire"]
            for table, _ in table_association
            if table != "PRODUCTION"
        )
        + f"""
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.BIOMASS_SOLID', '8', 'value', 'keep'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.BIOMASS_SOLID', '9', 'value', 'keep'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.BIOMASS_SOLID', '10', 'value', 'keep'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.GEOTHERMY', '14', 'value', 'keep');
        """
    )


def downgrade():
    op.execute(
        f"""
        delete from {TABLE} where region='pcaet';
        delete from {TABLE} where key like 'DataSet.%/Sector.OTHER_TRANSPORT';
        delete from strategie_territoire.passage_table where key like 'DataSet.PRODUCTION/RenewableProd.%' and match in ('8', '9', '10', '14') and region='occitanie';
        """
    )
