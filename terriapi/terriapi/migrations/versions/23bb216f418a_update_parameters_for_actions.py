"""Update parameters for actions

Revision ID: 23bb216f418a
Revises: 7ab3625d068f
Create Date: 2023-04-17 09:35:33.555344

"""

from alembic import op
from sqlalchemy.sql import text

# revision identifiers, used by Alembic.
revision = "23bb216f418a"
down_revision = "7ab3625d068f"
branch_labels = None
depends_on = None

KEYS = {
    "1": "residential_refurbishment",
    "2": "tertiary_refurbishment",
    "3": "pv_ground_based",
    "3b": "wind_power_plant",
    "4": "solar_thermal/ecs",
    "4b": "solar_thermal/combine",
    "5": "heating_network",
    "6": "methanization_cogeneration",
    "6b": "methanization_injection",
    "7": "bicycle_lanes",
    "8": "heating_network_mix",
    "10a": "pv_on_roof/resid_small_roof",
    "10b": "pv_on_roof/serv_ind_big_roof",
    "10c": "pv_on_roof/shade",
    "12": "hydro_power_plant",
    "13": "wood_energy",
    "14": "energy_efficiency_industry",
    "15": "energy_efficiency_agriculture",
    "16": "heating_conversion_resid",
    "17": "heating_conversion_services",
    "18": "mobility_commuting_time_reduction",
    "19": "mobility_carpooling_awareness",
    "20": "mobility_alternative_motor_public_tra",
    "21": "emissions_reduction_industry",
    "22": "emissions_reduction_agriculture",
}


def upgrade():
    for old_numero, key in KEYS.items():
        op.execute(
            f"""UPDATE strategie_territoire.action SET action_key = '{key}' WHERE action_numero = '{old_numero}'"""
        )
        op.execute(
            f"""UPDATE strategie_territoire.action_status SET first_year = 2022, last_year = 2050, action_key = '{key}' WHERE action_numero = '{old_numero}'"""
        )


def downgrade():
    op.execute("UPDATE strategie_territoire.action SET action_key = slugify(name)")
    op.execute(
        """
    UPDATE strategie_territoire.action_status s
    SET first_year = NULL, last_year = NULL, action_key = (SELECT slugify(name) as action_key 
        FROM strategie_territoire.action d WHERE d.action_numero = s.action_numero LIMIT 1)"""
    )
