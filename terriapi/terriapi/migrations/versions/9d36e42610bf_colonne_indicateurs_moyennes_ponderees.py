﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""colonne-indicateurs-moyennes-ponderees

Revision ID: 9d36e42610bf
Revises: 7c2540ccd418
Create Date: 2021-12-15 16:24:05.421550

"""

from alembic import op

# revision identifiers, used by Alembic.
revision = "9d36e42610bf"
down_revision = "7c2540ccd418"
branch_labels = None
depends_on = None


def upgrade():
    req = """
          alter table meta.indicateur add column moyenne_ponderee boolean;
          """
    op.execute(req)


def downgrade():
    req = """
          alter table meta.indicateur drop column moyenne_ponderee;
          """
    op.execute(req)
