"""empty message

Revision ID: 0ff69e372459
Revises: 4eb26ea2a62e, a17e6162d9ab
Create Date: 2024-10-16 12:01:41.004957

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0ff69e372459"
down_revision = ("4eb26ea2a62e", "a17e6162d9ab")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
