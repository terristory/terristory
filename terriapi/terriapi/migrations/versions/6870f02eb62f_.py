"""empty message

Revision ID: 6870f02eb62f
Revises: 01d77c16e647, 07ed2282adec
Create Date: 2023-01-27 15:44:34.438246

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6870f02eb62f"
down_revision = ("01d77c16e647", "07ed2282adec")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
