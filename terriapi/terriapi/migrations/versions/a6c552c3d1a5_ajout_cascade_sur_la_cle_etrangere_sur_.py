﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Ajout CASCADE sur la cle etrangere sur la table historique

Revision ID: a6c552c3d1a5
Revises: 66a036c8f1a0
Create Date: 2022-09-21 16:42:46.004838

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a6c552c3d1a5"
down_revision = "66a036c8f1a0"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ALTER TABLE auvergne_rhone_alpes_poi.historique
        DROP CONSTRAINT historique_utilisateur_fkey,
        ADD CONSTRAINT historique_utilisateur_fkey
        FOREIGN KEY(utilisateur)
        REFERENCES utilisateur(mail)
        ON DELETE SET NULL 
        ON UPDATE CASCADE;"""
    )


def downgrade():
    op.execute(
        """ALTER TABLE auvergne_rhone_alpes_poi.historique
        DROP CONSTRAINT historique_utilisateur_fkey,
        ADD CONSTRAINT historique_utilisateur_fkey
        FOREIGN KEY(utilisateur)
        REFERENCES utilisateur(mail);"""
    )
