﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""indicateur-simple-et-formule-calcul

Revision ID: 0d753323ba42
Revises: 9d36e42610bf
Create Date: 2021-12-21 16:09:04.711412

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "0d753323ba42"
down_revision = "9d36e42610bf"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "alter table meta.indicateur add column afficher_calcul_et_donnees_table boolean;"
    )


def downgrade():
    op.execute(
        "alter table meta.indicateur drop column afficher_calcul_et_donnees_table;"
    )
