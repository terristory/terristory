"""Add ability to disable indicator in dashboard

Revision ID: 7754d8f3576f
Revises: d0251ff7d5dc
Create Date: 2023-12-05 18:51:14.137803

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7754d8f3576f"
down_revision = "d0251ff7d5dc"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "indicateur",
        sa.Column("disabled_in_dashboard", sa.Boolean, server_default="f"),
        schema="meta",
    )


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("disabled_in_dashboard")
