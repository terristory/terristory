"""Add classification config for choropleth maps

Revision ID: 285e78a1d184
Revises: b346bccc2ef0
Create Date: 2024-12-20 11:40:58.142873

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "285e78a1d184"
down_revision = "b346bccc2ef0"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("ALTER TABLE meta.indicateur ADD classification_config jsonb NULL;")
    op.execute(
        "COMMENT ON COLUMN meta.indicateur.classification_config IS 'Classification configuration for choropleth maps';"
    )


def downgrade():
    op.execute("ALTER TABLE meta.indicateur DROP COLUMN classification_config")
