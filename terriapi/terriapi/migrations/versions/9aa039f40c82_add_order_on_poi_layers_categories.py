"""Add order on POI layers categories

Revision ID: 9aa039f40c82
Revises: 930bb5ef8d1b
Create Date: 2023-03-16 16:08:44.044286

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9aa039f40c82"
down_revision = "930bb5ef8d1b"
branch_labels = None
depends_on = None

REGIONS = [
    "auvergne_rhone_alpes",
    "occitanie",
    "nouvelle_aquitaine",
    "bretagne",
    "paysdelaloire",
    "corse",
]


def upgrade():
    for region in REGIONS:
        with op.batch_alter_table("layer", schema=region + "_poi") as batch_op:
            batch_op.alter_column("rubrique", new_column_name="theme")
            batch_op.add_column(
                sa.Column("theme_order", sa.Integer(), server_default="0")
            )
            batch_op.add_column(
                sa.Column("order_in_theme", sa.Integer(), server_default="0")
            )


def downgrade():
    for region in REGIONS:
        with op.batch_alter_table("layer", schema=region + "_poi") as batch_op:
            batch_op.alter_column("theme", new_column_name="rubrique")
            batch_op.drop_column("theme_order")
            batch_op.drop_column("order_in_theme")
