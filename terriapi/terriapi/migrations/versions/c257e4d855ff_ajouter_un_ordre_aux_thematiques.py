﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Ajouter un ordre aux thematiques

Revision ID: c257e4d855ff
Revises: eb76cdaaf791
Create Date: 2022-08-12 11:52:13.622383

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c257e4d855ff"
down_revision = "eb76cdaaf791"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("tableau_thematique", sa.Column("ordre", sa.Integer), schema="meta")


def downgrade():
    with op.batch_alter_table("tableau_thematique", schema="meta") as batch_op:
        batch_op.drop_column("ordre")
