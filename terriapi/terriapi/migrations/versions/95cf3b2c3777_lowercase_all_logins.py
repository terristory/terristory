"""Lowercase all logins

Revision ID: 95cf3b2c3777
Revises: 4eb4750e9709
Create Date: 2024-02-15 17:34:38.211114

"""

import datetime

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "95cf3b2c3777"
down_revision = "4eb4750e9709"
branch_labels = None
depends_on = None


def upgrade():
    conn = op.get_bind()
    res_users = conn.execute(
        """SELECT LOWER(mail) as mail, region, ARRAY_AGG(mail) as real_mails, 
                ARRAY_AGG(derniere_connexion) as last_login, COUNT(*)
                FROM utilisateur
                GROUP BY LOWER(mail), region
                HAVING COUNT(*) > 1;"""
    )
    users = res_users.fetchall()
    for user in users:
        youngest = max(
            user["last_login"],
            key=lambda x: datetime.datetime(1970, 1, 1) if x is None else x,
        )
        conn.execute(
            "DELETE FROM utilisateur WHERE LOWER(mail) = %(mail)s AND region = %(region)s AND (derniere_connexion IS NULL OR derniere_connexion <> %(last_login)s)",
            {"mail": user["mail"], "region": user["region"], "last_login": youngest},
        )
    conn.execute("UPDATE utilisateur SET mail = LOWER(mail)")

    tables_with_mails = [
        ("auvergne_rhone_alpes.historique_indicateurs", "mail"),
        ("auvergne_rhone_alpes_poi.historique", "mail"),
        ("bretagne.historique_indicateurs", "mail"),
        ("bretagne_poi.historique", "mail"),
        ("corse.historique_indicateurs", "mail"),
        ("corse_poi.historique", "mail"),
        ("meta.poi_proposed_changes", "user_mail"),
        ("meta.poi_contributions_contacts", "email"),
        ("france_poi.historique", "mail"),
        ("meta.tableau_bord", "mail"),
        ("nouvelle_aquitaine.historique_indicateurs", "mail"),
        ("nouvelle_aquitaine_poi.historique", "mail"),
        ("occitanie.historique_indicateurs", "mail"),
        ("occitanie_poi.historique", "mail"),
        ("paysdelaloire.historique_indicateurs", "mail"),
        ("paysdelaloire_poi.historique", "mail"),
        ("refresh_token", "mail"),
        ("utilisateur_scenario", "mail"),
    ]
    for table, column in tables_with_mails:
        conn.execute(f"UPDATE {table} SET {column} = LOWER({column})")


def downgrade():
    pass
