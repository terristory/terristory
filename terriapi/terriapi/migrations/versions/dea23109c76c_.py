"""empty message

Revision ID: dea23109c76c
Revises: d2c01be8090d, dc0c355fafb9
Create Date: 2024-09-05 14:37:55.575704

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "dea23109c76c"
down_revision = ("d2c01be8090d", "dc0c355fafb9")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
