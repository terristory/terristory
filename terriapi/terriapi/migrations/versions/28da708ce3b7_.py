"""empty message

Revision ID: 28da708ce3b7
Revises: cf081753294f, db1871c309f3
Create Date: 2023-06-28 14:24:03.330133

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "28da708ce3b7"
down_revision = ("cf081753294f", "db1871c309f3")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
