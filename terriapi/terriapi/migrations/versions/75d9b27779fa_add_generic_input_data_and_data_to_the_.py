"""Add generic input data and data to the mapping table for EnR simulator

Revision ID: 75d9b27779fa
Revises: 6a0a289c9f56
Create Date: 2024-11-28 15:43:37.673848

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "75d9b27779fa"
down_revision = "6a0a289c9f56"
branch_labels = None
depends_on = None

# [simulator.generic_input_param] French label -> English label
###############################################################
# 'Pertes liées au système photovoltaïque sur toitures' -> 'PHOTOVOLTAIC_SYSTEM_LOSSES_ROOFTOP'
# 'Pertes liées au système photovoltaïque sur ombrières' -> 'PHOTOVOLTAIC_SYSTEM_LOSSES_CANOPIES'
# 'Pertes liées au système photovoltaïque au sols' -> 'PHOTOVOLTAIC_SYSTEM_LOSSES_GROUND_MOUNTED'
# 'Ratio surface / puissance installée pour les centrales PV au sol' -> 'LAND_AREA_INSTALLED_CAPACITY_RATIO_GROUND_MOUNTED_PV_PLANTS'
# 'Ratio surface / puissance installée pour les ombrières' -> 'LAND_AREA_INSTALLED_CAPACITY_RATIO_CANOPY_PV_PLANTS'
# 'Ratio puissance installée / surface pour les panneaux photovoltaïques sur toiture' -> 'INSTALLED_CAPACITY_AREA_RATIO_ROOFTOP_PV_PANELS'
# 'Part des centrales au sol dans le mix photovoltaïque' -> 'SHARE_GROUND_MOUNTED_PLANTS_PV_MIX'
# 'Part des ombrières dans le mix photovoltaïque' -> 'SHARE_CANOPIES_PV_MIX'
# 'Part des panneaux sur toiture dans le mix photovoltaïque' -> 'SHARE_ROOFTOP_PANELS_PV_MIX'
# 'Facteur de charge d''une éolienne terrestre' -> 'LAND_BASED_WIND_TURBINE_CAPACITY_FACTOR'
# 'Facteur de charge du petit hydroélectrique' -> 'SMALL_HYDROELECTRIC_CAPACITY_FACTOR'
# 'Rapport puissance / production pour une chaufferie bois' -> 'POWER_PRODUCTION_RATIO_BIOMASS_BOILER_PLANTS'
# 'Ratio production / surface pour le solaire thermique' -> 'PRODUCTION_AREA_RATIO_SOLAR_THERMAL_PLANTS'
# 'Production par installation (PAC aérothermique)' -> 'PRODUCTION_PER_INSTALLATION_AEROTHERMAL_HEAT_PUMP'
# 'Production par installation (PAC géothermique)' -> 'PRODUCTION_PER_INSTALLATION_GEOTHERMAL_HEAT_PUMP'
# 'Part des éoliennes terrestres dans le mix éolien' -> 'SHARE_LAND_BASED_WIND_TURBINES_WIND_MIX'
# 'Part des éoliennes en mer dans le mix éolien' -> 'SHARE_OFFSHORE_WIND_TURBINES_WIND_MIX'
# 'Part de la production d''électricité par rapport à la production totale de la cogénération' -> 'SHARE_ELECTRICITY_PRODUCTION_COGENERATION_TOTAL'
# 'Part de la production supplémentaire de biomasse solide utilisée pour le résidentiel' -> 'SHARE_ADDITIONAL_SOLID_BIOMASS_PRODUCTION_RESIDENTIAL_USE'
# 'Part de la production supplémentaire de biomasse solide utilisée pour le tertiaire' -> 'SHARE_ADDITIONAL_SOLID_BIOMASS_PRODUCTION_TERTIARY_USE'
# 'Part de la production supplémentaire de biomasse solide utilisée pour les RDC' -> 'SHARE_ADDITIONAL_SOLID_BIOMASS_PRODUCTION_RDC_USE'
# 'Puissance d''un mât - éolien terrestre' -> 'LAND_BASED_WIND_TURBINE_CAPACITY'
# 'Méthanisation - Conversion de Nm3/h vers GWh/an' -> 'METHANIZATION_CONVERSION_NM3H_TO_GWH_PER_YEAR'
# 'Débit horaire d''une unité' -> 'HOURLY_FLOW_RATE_OF_UNIT'
# 'Nombre de personnes par résidence principale' -> 'NUMBER_OF_PEOPLE_PER_HOUSEHOLD'


def upgrade():
    op.execute(
        sa.text(
            """
            INSERT INTO simulator.generic_input_param(data, label, value, unit, thematic) VALUES
            ('production', 'PHOTOVOLTAIC_SYSTEM_LOSSES_ROOFTOP', 28, '%', 'enr'),
            ('production', 'PHOTOVOLTAIC_SYSTEM_LOSSES_CANOPIES', 28, '%', 'enr'),
            ('production', 'PHOTOVOLTAIC_SYSTEM_LOSSES_GROUND_MOUNTED', 26, '%', 'enr'),
            ('production', 'LAND_AREA_INSTALLED_CAPACITY_RATIO_GROUND_MOUNTED_PV_PLANTS', 3.3, 'ha/MW', 'enr'),
            ('production', 'LAND_AREA_INSTALLED_CAPACITY_RATIO_CANOPY_PV_PLANTS', 9, 'm²/kWc', 'enr'),
            ('production', 'INSTALLED_CAPACITY_AREA_RATIO_ROOFTOP_PV_PANELS', 0.12, 'kWc/m²', 'enr'),
            ('production', 'SHARE_GROUND_MOUNTED_PLANTS_PV_MIX', 0.333333333, 'GWh/GWh', 'enr'),
            ('production', 'SHARE_CANOPIES_PV_MIX', 0.333333333, 'GWh/GWh', 'enr'),
            ('production', 'SHARE_ROOFTOP_PANELS_PV_MIX', 0.333333333, 'GWh/GWh', 'enr'),
            ('production', 'LAND_BASED_WIND_TURBINE_CAPACITY_FACTOR', 23.6, '%', 'enr'),
            ('production', 'SMALL_HYDROELECTRIC_CAPACITY_FACTOR', 27.61, '%', 'enr'),
            ('production', 'POWER_PRODUCTION_RATIO_BIOMASS_BOILER_PLANTS', 2.702702703, 'GWh/MW', 'enr'),
            ('production', 'PRODUCTION_AREA_RATIO_SOLAR_THERMAL_PLANTS', 0.0005, 'GWh/m²', 'enr'),
            ('production', 'PRODUCTION_PER_INSTALLATION_AEROTHERMAL_HEAT_PUMP', 0.01, 'GWh/installation', 'enr'),
            ('production', 'PRODUCTION_PER_INSTALLATION_GEOTHERMAL_HEAT_PUMP', 0.0125, 'GWh/installation', 'enr'),
            ('production', 'SHARE_LAND_BASED_WIND_TURBINES_WIND_MIX', 1, 'GWh/GWh', 'enr'),
            ('production', 'SHARE_OFFSHORE_WIND_TURBINES_WIND_MIX', 0, 'GWh/GWh', 'enr'),
            ('production', 'SHARE_ELECTRICITY_PRODUCTION_COGENERATION_TOTAL', 46.5, '%', 'enr'),

            ('consumption', 'SHARE_ADDITIONAL_SOLID_BIOMASS_PRODUCTION_RESIDENTIAL_USE', 33.3333333, '%', 'enr'),
            ('consumption', 'SHARE_ADDITIONAL_SOLID_BIOMASS_PRODUCTION_TERTIARY_USE', 33.3333333, '%', 'enr'),
            ('consumption', 'SHARE_ADDITIONAL_SOLID_BIOMASS_PRODUCTION_RDC_USE', 33.3333333, '%', 'enr'),

            ('power', 'LAND_BASED_WIND_TURBINE_CAPACITY', 2.5, 'MW', 'enr'),

            ('conversion', 'METHANIZATION_CONVERSION_NM3H_TO_GWH_PER_YEAR', 0.091954023, '[GWh/an]/[Nm3/h]', 'enr'),
            ('conversion', 'HOURLY_FLOW_RATE_OF_UNIT', 200, 'NM3/h', 'enr'),

            ('population', 'NUMBER_OF_PEOPLE_PER_HOUSEHOLD', 2.16, 'habitants/foyer', 'enr');
            """
        )
    )

    op.execute(
        sa.text(
            """
            INSERT INTO strategie_territoire.passage_table(region, key, match, association_type, data_unit) VALUES  

            ---WIND_ENERGY_POTENTIAL

            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL', 'potentiel_eolien', 'table', 'ha'),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/WindSlope', 'pente_eolien', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes', 'enjeu', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Altitude', 'altitude', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/WindSlope.[0-5%[', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/WindSlope.[5-10%[', '2', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/WindSlope.[10-15%[', '3', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/WindSlope.[15-20%[', '4', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/WindSlope.>20%', '5', 'value', ''),

            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes.NO_CONSTRAINT', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes.POTENTIAL_EXCLUSION_ZONE', '2', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes.POINT_VIGILANCE', '3', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes.POTENTIAL_EXCLUSION_ZONE_AND_VIGILANCE', '4', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes.STRONG_CONSTRAINTS', '5', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes.POTENTIAL_EXCLUSION_ZONE_AND_STRONG_CONSTRAINTS', '6', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Stakes.EXCLUSION_ZONE_PROHIBITED_INSTALLATION', '7', 'value', ''),

            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Altitude.[0-500m[', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Altitude.[500-1000m[', '2', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Altitude.[1000-1500m[', '3', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Altitude.[1500-2000m[', '4', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.WIND_ENERGY_POTENTIAL/Altitude.>2000m', '5', 'value', ''),


            --- INSTALLED_RENEWABLE_ELECTRICITY_POWER

            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER', 'puissance_enr', 'table', 'MW'),
            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE', 'type_puissance_enr', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE.WIND', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE.PHOTOVOLTAIC', '2', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE.HYDROELECTRIC_LOW', '3', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE.HYDROELECTRIC_HIGH', '4', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE.SOLID_BIOMASS_POWER_PLANTS', '5', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE.WASTE_ENERGY_POWER_PLANTS', '6', 'value', ''),


            --- SOLAR_PHOTOVOLTAIC_POTENTIAL

            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL', 'potentiel_photovoltaique', 'table', 'MWh'),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE', 'type_photovoltaique', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIALL/PHOTOVOLTAIC_ORIENTATION', 'orientation_photovoltaique', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_CONSTRAINT', 'contrainte_photovoltaique', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.AGRICULTURAL_BUILDINGS', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.COMMERCIAL_AND_SERVICES_BUILDINGS', '2', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.INDUSTRIAL_BUILDINGS', '3', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.COLLECTIVE_RESIDENTIAL_BUILDINGS', '4', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.INDIVIDUAL_RESIDENTIAL_BUILDINGS', '5', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.SPORTS_BUILDINGS', '6', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.PARKINGS_LESS_THAN_500M2', '7', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.PARKINGS_500M2_TO_1500M2', '8', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.PARKINGS_1500M2_TO_10000M2', '9', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.PARKINGS_MORE_THAN_10000M2', '10', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_TYPE.OTHER_BUILDING_TYPE', '11', 'value', ''),

            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_ORIENTATION.EAST_WEST', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_ORIENTATION.SOUTH', '2', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_ORIENTATION.FLAT_ROOF', '3', 'value', ''),

            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_CONSTRAINT.YES', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL/PHOTOVOLTAIC_CONSTRAINT.NO', '2', 'value', ''),


            --- METHANIZATION_POTENTIAL


            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL', 'potentiel_methanisation', 'table', 'MWh'),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY', 'filiere_methanisation', 'column', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/POTENTIAL_USAGE', 'utilisation_potentiel', 'column', ''),

            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.CIVE', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.LIVESTOCK_MANURE', '2', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.CROP_RESIDUES', '3', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.COLLECTIVE_SEWERAGE', '4', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.HOUSEHOLD_BIOWASTE_SELECTIVE_COLLECTION', '5', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.DISTRIBUTION', '6', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.COLLECTIVE_RESTAURANT_HEALTH_SOCIAL', '7', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.COLLECTIVE_SCHOOL_RESTAURANT', '8', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.COMMERCIAL_RESTAURANT', '9', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.GREEN_WASTE', '10', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/METHANIZATION_PATHWAY.SMALL_BUSINESSES', '11', 'value', ''),

            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/POTENTIAL_USAGE.REMAINING_POTENTIAL', '1', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.METHANIZATION_POTENTIAL/POTENTIAL_USAGE.USED_POTENTIAL', '2', 'value', ''),

            --- MISSING ENERGY VECTOR FOR PRODUCTION
            ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.AEROTHERMAL_PAC', '10', 'value', ''),
            ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.GEOTHERMAL_PAC', '9', 'value', ''),


            --- INSEE_POPULATION
            ('auvergne-rhone-alpes', 'DataSet.INSEE_POPULATION', 'population', 'table', ''),


            --- TERTIARY_EMPLOYMENT
            ('auvergne-rhone-alpes', 'DataSet.TERTIARY_EMPLOYMENT', 'emploi_tertiaire', 'table', ''),


            -- GLOBAL_IRRADIATION

            ('auvergne-rhone-alpes', 'DataSet.GLOBAL_IRRADIATION', 'global_irradiation', 'table', '');
        """
        )
    )

    # Update data_unit in passage_table
    op.execute(
        """
            update strategie_territoire.passage_table set data_unit = 'GWh' where key = 'DataSet.PRODUCTION' and region='auvergne-rhone-alpes'

        """
    )
    # Insert meta data for EnR simulator
    ## Insert into simulator.simulator_level
    op.execute(
        sa.text(
            """
                INSERT INTO simulator.simulator_level(region, level, thematic)
                VALUES ('auvergne-rhone-alpes', 0, 'enr')
            """
        )
    )

    ## Insert into simulator.simulator_impact
    op.execute(
        sa.text(
            """
            INSERT INTO simulator.simulator_impact(type, title, unit, thematic, impact_order, regions)
            VALUES 
            ('global-results', 'Résultats globaux', 'GWh', 'enr', 1, '{auvergne-rhone-alpes}'),
            ('enr-elec', 'Zoom EnR électriques', 'GWh', 'enr', 2, '{auvergne-rhone-alpes}');
            """
        )
    )

    ## Insert into simulator.category_lever
    id_category = (
        op.get_bind()
        .execute(
            sa.text(
                """
            INSERT INTO simulator.category_lever(title, thematic, category_order, regions)
            VALUES ('Leviers EnR', 'enr', 1, '{auvergne-rhone-alpes}')
            RETURNING id;
        """
            )
        )
        .scalar()
    )

    ## Insert into simulator.lever
    id_lever = (
        op.get_bind()
        .execute(
            sa.text(
                """
            INSERT INTO simulator.lever(title, regions, lever_order, category_lever_id, key)
            VALUES ('Leviers d''actions EnR', '{auvergne-rhone-alpes}', 1, :id_category, 'enr_levers')
            RETURNING id;
        """
            ),
            {"id_category": id_category},
        )
        .scalar()
    )

    ## Insert into simulator.lever_action
    op.get_bind().execute(
        sa.text(
            """
            INSERT INTO simulator.lever_action (number, title, lever_id)
            VALUES
            ('1a', 'Photovoltaique', :id_lever),
            ('2a', 'Eolien', :id_lever),
            ('3a', 'Hydroélectricité', :id_lever),
            ('4a', 'Méthanisation', :id_lever),
            ('5a', 'Biomasse solide', :id_lever),
            ('6a', 'Solaire thermique', :id_lever),
            ('7a', 'Pompe à chaleur', :id_lever)
        """
        ),
        {"id_lever": id_lever},
    )


def downgrade():

    op.execute(
        sa.text(
            """
        DELETE FROM simulator.simulator_level
        WHERE thematic = 'enr';
        """
        )
    )

    op.execute(
        sa.text(
            """
        DELETE FROM simulator.simulator_impact
        WHERE thematic = 'enr';
        """
        )
    )

    op.execute(
        sa.text(
            """
        DELETE FROM simulator.generic_input_param
        WHERE thematic = 'enr';
        """
        )
    )

    op.execute(
        sa.text(
            """
        DELETE FROM strategie_territoire.passage_table
        WHERE key LIKE 'DataSet.WIND_ENERGY_POTENTIAL%'
           OR key LIKE 'DataSet.RENEWABLE_ELECTRICITY_POWER%'
           OR key LIKE 'DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL%'
           OR key LIKE 'DataSet.METHANIZATION_POTENTIAL%'
           OR key LIKE 'DataSet.INSEE_POPULATION%'
           OR key LIKE 'DataSet.TERTIARY_EMPLOYMENT%'
           OR key LIKE 'DataSet.GLOBAL_IRRADIATION%'
           OR key LIKE 'DataSet.PRODUCTION/RenewableProd.AEROTHERMAL_PAC'
           or key LIKE 'DataSet.PRODUCTION/RenewableProd.GEOTHERMAL_PAC'
        """
        )
    )

    # Delete the `category_lever` entry along with all related rows in cascading tables : lever and lever_action

    op.execute(
        sa.text(
            """
            DELETE FROM simulator.category_lever 
            WHERE title = 'Leviers EnR';
        """
        )
    )
