"""empty message

Revision ID: 992411345876
Revises: 6c78e92d9209, 609fcee22c15
Create Date: 2023-09-06 09:07:20.054326

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "992411345876"
down_revision = ("6c78e92d9209", "609fcee22c15")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
