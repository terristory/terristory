"""merge heads

Revision ID: 2037f4ffee0c
Revises: 4e218b1c6002, a17e6162d9ab
Create Date: 2024-10-21 17:48:45.485427

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "2037f4ffee0c"
down_revision = ("4e218b1c6002", "a17e6162d9ab")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
