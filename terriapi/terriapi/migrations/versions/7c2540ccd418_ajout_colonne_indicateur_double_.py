﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout colonne indicateur double representation proportion-valeur absolue

Revision ID: 7c2540ccd418
Revises: ddce486c86ce
Create Date: 2021-10-08 15:15:22.920750

"""
from alembic import op
from sqlalchemy import Column, String
from sqlalchemy.sql.sqltypes import Boolean

# revision identifiers, used by Alembic.
revision = "7c2540ccd418"
down_revision = "ddce486c86ce"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "indicateur"


def upgrade():
    op.add_column(TABLENAME, Column("afficher_proportion", Boolean), schema=SCHEMA)
    op.add_column(TABLENAME, Column("titre", String()), schema=SCHEMA)


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("afficher_proportion")
        batch_op.drop_column("titre")
