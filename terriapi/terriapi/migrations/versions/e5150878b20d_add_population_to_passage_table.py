"""Add population to passage table

Revision ID: e5150878b20d
Revises: efe7a32f9485
Create Date: 2024-12-11 12:37:03.504011

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e5150878b20d"
down_revision = "efe7a32f9485"
branch_labels = None
depends_on = None

passage_table = {
    "auvergne-rhone-alpes": "population",
    "bretagne": "population_bretonne",
    "corse": "population",
    "nouvelle-aquitaine": "population",
    "occitanie": "population",
    "paysdelaloire": "population",
}


def upgrade():
    for region, table in passage_table.items():
        op.execute(
            f"""
            INSERT INTO strategie_territoire.passage_table
            (region, key, match, association_type) VALUES
            ('{region}', 'DataSet.POPULATION', '{table}', 'table');
            """
        )


def downgrade():
    op.execute(
        """
        DELETE FROM strategie_territoire.passage_table
        WHERE key='DataSet.POPULATION';
        """
    )
