"""empty message

Revision ID: 930bb5ef8d1b
Revises: beae4839ca12, c45a24504046
Create Date: 2023-02-20 18:06:20.764298

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "930bb5ef8d1b"
down_revision = ("beae4839ca12", "c45a24504046")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
