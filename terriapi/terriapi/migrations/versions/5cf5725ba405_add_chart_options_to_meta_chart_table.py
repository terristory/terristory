"""Add chart options to meta.chart table

Revision ID: 5cf5725ba405
Revises: e5150878b20d
Create Date: 2024-11-04 13:58:46.305979

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5cf5725ba405"
down_revision = "e5150878b20d"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("ALTER TABLE meta.chart ADD chart_options jsonb NULL;")
    op.execute(
        "COMMENT ON COLUMN meta.chart.chart_options IS 'Additional options for the chart';"
    )


def downgrade():
    op.execute("ALTER TABLE meta.chart DROP COLUMN chart_options")
