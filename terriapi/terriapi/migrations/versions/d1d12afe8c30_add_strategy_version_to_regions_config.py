"""Add strategy version to regions_config

Revision ID: d1d12afe8c30
Revises: 63b21b5c954e
Create Date: 2023-06-08 13:49:57.677871

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d1d12afe8c30"
down_revision = "63b21b5c954e"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("enable_new_strategy_module", sa.Boolean, server_default="false"),
    )


def downgrade():
    with op.batch_alter_table("regions_configuration") as batch_op:
        batch_op.drop_column("enable_new_strategy_module")
