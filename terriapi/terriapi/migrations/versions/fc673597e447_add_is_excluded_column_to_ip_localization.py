"""Add is_excluded column to IP localization

Revision ID: fc673597e447
Revises: 08c7bd987296
Create Date: 2024-03-11 11:27:18.033761

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "fc673597e447"
down_revision = "08c7bd987296"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "ip_localisation",
        sa.Column("is_excluded", sa.Boolean, server_default=str("f")),
        schema="consultations",
    )


def downgrade():
    with op.batch_alter_table("ip_localisation", schema="consultations") as batch_op:
        batch_op.drop_column("is_excluded")
