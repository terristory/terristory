"""add configuration table for territorial strategy impacts

Revision ID: 03b6802f3f7b
Revises: f5ca81c5952c
Create Date: 2023-06-15 15:27:07.368315

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "03b6802f3f7b"
down_revision = "f5ca81c5952c"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ 
        CREATE TABLE strategie_territoire.impacts_configuration(
            type varchar,
            region varchar,
            decimal int,
            enabled boolean,
            PRIMARY key (type,region));
        """
    )


def downgrade():
    op.drop_table("impacts_configuration", schema="strategie_territoire")
