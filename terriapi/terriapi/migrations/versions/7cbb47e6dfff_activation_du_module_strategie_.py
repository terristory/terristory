﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Activation du module strategie territoriale en PdL

Revision ID: 7cbb47e6dfff
Revises: 60dd818543c2
Create Date: 2022-09-05 09:05:23.413318

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7cbb47e6dfff"
down_revision = "60dd818543c2"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """UPDATE regions_configuration 
        SET ui_show_plan_actions = true,
        titre_graph_ges = 'émissions GES',
        source_suivi_traj = '{"source": "Source de l''historique : TEO 2021", "source_impacts": "Historique", "position_annotation": -50, "titre_graphique": "Données corrigées des variations climatiques en GWh"}',
        ges_export_excel = false
        WHERE id = 'paysdelaloire'"""
    )

    op.execute(
        """UPDATE meta.indicateur
        SET data = 'conso_energetique'
        WHERE region = 'paysdelaloire' AND data LIKE 'conso_energie';"""
    )

    op.execute(
        """ALTER TABLE paysdelaloire.conso_energie
        RENAME TO conso_energetique;"""
    )

    op.execute(
        """UPDATE meta.perimetre_geographique
        SET nom_table = 'conso_energetique'
        WHERE region = 'paysdelaloire' AND nom_table = 'conso_energie';"""
    )

    op.execute(
        """UPDATE meta.categorie
        SET nom = 'type_prod_enr'
        WHERE region = 'paysdelaloire' AND nom = 'energieprod';"""
    )

    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Hydroélectricité', 1, '#A6DEF7', 1, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Solaire photovoltaïque', 2, '#FFD34D', 2, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Eolien', 3, '#446D9F', 3, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Méthanisation : Valorisation électrique biogaz', 4, '#CAE397', 4, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Méthanisation : Valorisation Thermique biogaz', 5, '#9AC73F', 5, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Déchets ménagers (UVE) : Valorisation électrique', 6, '#B9D4BD', 7, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Déchets ménagers (UVE) : Valorisation Thermique', 7, '#698C6F', 8, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Cognération bois-biomasse : Production électrique', 8, '#478351', 9, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Cogénération bois-biomasse : Production Thermique', 9, '#92CE9C', 10, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Chaufferie bois-biomasse : Production Thermique', 10, '#40CA57', 11, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Bois domestique : Valorisation Thermique', 11, '#129C29', 12, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Solaire thermique', 12, '#F6B332', 13, 'paysdelaloire');;""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Méthanisation : Biométhane injecté', 13, '#69ab3e', 6, 'paysdelaloire');""")
    # op.execute("""INSERT INTO meta.categorie VALUES('type_prod_enr', 'Géothermie', 14, '#8877A1', 14, 'paysdelaloire');""")

    op.execute(
        """INSERT INTO meta.association_graphique_table 
        VALUES('Consommation d''énergie', 'conso_energetique', 'paysdelaloire');"""
    )
    op.execute(
        """INSERT INTO meta.association_graphique_table 
        VALUES('Production EnR', 'prod_enr', 'paysdelaloire');"""
    )
    op.execute(
        """INSERT INTO meta.association_graphique_table 
        VALUES('Émissions de gaz à effet de serre', 'emission_ges', 'paysdelaloire');"""
    )

    op.execute(
        """UPDATE meta.chart
        SET categorie = 'type_prod_enr'
        WHERE categorie = 'energieprod';"""
    )

    op.execute(
        """ALTER TABLE paysdelaloire.prod_enr
        RENAME COLUMN energieprod TO type_prod_enr;"""
    )

    op.execute(
        """CREATE TABLE paysdelaloire.mobilite_insee AS 
        SELECT * FROM auvergne_rhone_alpes.mobilite_insee;"""
    )
    op.execute(
        """INSERT INTO meta.indicateur (nom, data, type, color_start, color_end, unit, methodo_pdf, confidentiel, disabled_for_zone, only_for_zone, isratio, filter, decimals, ui_theme, data_ratio, years, display_total, active, region, data_deuxieme_representation, valeur_filtre_defaut, titre_legende, titre_legende_deuxieme_representation, concatenation, ordre_ui_theme, ordre_indicateur, data_type, donnees_exportables, titre_dans_infobulle, afficher_proportion, titre, moyenne_ponderee, afficher_calcul_et_donnees_table, titre_graphiques_indicateurs) 
    SELECT nom, data, type, color_start, color_end, unit, methodo_pdf, confidentiel, disabled_for_zone, only_for_zone, isratio, filter, decimals, ui_theme, data_ratio, years, display_total, false as active, 'paysdelaloire' as region, data_deuxieme_representation, valeur_filtre_defaut, titre_legende, titre_legende_deuxieme_representation, concatenation, ordre_ui_theme, ordre_indicateur, data_type, donnees_exportables, titre_dans_infobulle, afficher_proportion, titre, moyenne_ponderee, afficher_calcul_et_donnees_table, titre_graphiques_indicateurs  FROM meta.indicateur WHERE data = 'mobilite_insee' AND region = 'auvergne-rhone-alpes';"""
    )

    op.execute(
        """UPDATE paysdelaloire.confid_camembert SET type_analyse = 'conso_energetique' WHERE type_analyse = 'conso_energie';"""
    )
    op.execute(
        """UPDATE paysdelaloire.confid_maille SET type_analyse = 'conso_energetique' WHERE type_analyse = 'conso_energie';"""
    )

    op.execute(
        """CREATE TABLE paysdelaloire.prod_enr_mapping AS 
        SELECT * FROM occitanie.prod_enr_mapping;"""
    )
    op.execute(
        """UPDATE paysdelaloire.prod_enr_mapping SET libelle = 'Valorisation énergétique des déchets' WHERE action = '6' AND cle = 'chaleur_livree';"""
    )
    op.execute(
        """UPDATE paysdelaloire.prod_enr_mapping SET libelle = 'Méthanisation : Biométhane injecté' WHERE action = '6b' AND cle = 'energie_produite';"""
    )
    op.execute(
        """UPDATE paysdelaloire.prod_enr_mapping SET libelle = 'Méthanisation' WHERE action = '6' AND cle = 'elec_livree';"""
    )
    op.execute(
        """UPDATE paysdelaloire.prod_enr_mapping SET libelle = 'Chaufferie bois-biomasse : Production Thermique' WHERE action = '13';"""
    )
    op.execute(
        """UPDATE paysdelaloire.prod_enr_mapping SET libelle = 'Bois domestique : Valorisation Thermique' WHERE action = '5';"""
    )
    op.execute(
        """UPDATE paysdelaloire.prod_enr_mapping SET libelle = 'Hydraulique' WHERE action = '12';"""
    )
    op.execute(
        """UPDATE paysdelaloire.prod_enr_mapping SET libelle = 'Photovoltaïque' WHERE action IN ('3', '10a', '10b', '10c');"""
    )

    op.execute(
        """CREATE TABLE paysdelaloire.secteur_mapping AS
        SELECT * FROM occitanie.secteur_mapping;"""
    )
    op.execute(
        """UPDATE paysdelaloire.secteur_mapping SET libelle = 'Industrie (hors branche énergie)' WHERE action = '14';"""
    )

    op.execute(
        """UPDATE strategie_territoire.action 
        SET regions = array_append(regions, 'paysdelaloire') WHERE 'paysdelaloire' <> ALL(regions) AND numero NOT IN ('14', '15', '16', '17');"""
    )

    op.execute(
        """TRUNCATE paysdelaloire.clap; 
        INSERT INTO paysdelaloire.clap 
        SELECT 
            -- on crée une colonne id
            ROW_NUMBER () OVER (ORDER BY codgeo, code_naf) as id,
            -- on récupère toutes les données
            code_naf::integer as naf,
            codgeo as commune,
            nb_etab,
            nb_eff as effectif
            -- à partir de la liste des communes de la région qui nous intéresse
            FROM paysdelaloire.commune c
            -- on joint avec les données nationales
            LEFT JOIN france.flores_2019_fr f ON f.codgeo = c.code;"""
    )

    op.execute(
        """CREATE MATERIALIZED VIEW IF NOT EXISTS paysdelaloire.nb_emploi_territoire_view
        TABLESPACE pg_default
        AS
        SELECT 'commune'::text AS territoire,
            c.commune AS territoire_id,
            n.a88,
            sum(c.effectif) AS nb_emploi
        FROM paysdelaloire.clap c
            JOIN strategie_territoire.naf n ON n.id = c.naf
        GROUP BY c.commune, n.a88
        UNION ALL
        SELECT 'departement'::text AS territoire,
            t.code AS territoire_id,
            n.a88,
            sum(c.effectif) AS nb_emploi
        FROM paysdelaloire.clap c
            JOIN paysdelaloire.commune co ON co.code::text = c.commune::text
            JOIN strategie_territoire.naf n ON n.id = c.naf
            JOIN paysdelaloire.departement t ON t.communes && ARRAY[co.code]
        GROUP BY t.code, n.a88
        UNION ALL
        SELECT 'epci'::text AS territoire,
            t.code AS territoire_id,
            n.a88,
            sum(c.effectif) AS nb_emploi
        FROM paysdelaloire.clap c
            JOIN paysdelaloire.commune co ON co.code::text = c.commune::text
            JOIN strategie_territoire.naf n ON n.id = c.naf
            JOIN paysdelaloire.epci t ON t.communes && ARRAY[co.code]
        GROUP BY t.code, n.a88
        UNION ALL
        SELECT 'region'::text AS territoire,
            t.code AS territoire_id,
            n.a88,
            sum(c.effectif) AS nb_emploi
        FROM paysdelaloire.clap c
            JOIN paysdelaloire.commune co ON co.code::text = c.commune::text
            JOIN strategie_territoire.naf n ON n.id = c.naf
            JOIN paysdelaloire.region t ON t.communes && ARRAY[co.code]
        GROUP BY t.code, n.a88;"""
    )

    op.execute("""REFRESH MATERIALIZED VIEW paysdelaloire.nb_emploi_territoire_view;""")


def downgrade():
    op.execute(
        """UPDATE regions_configuration 
        SET ui_show_plan_actions = false,
        titre_graph_ges = 'émissions GES',
        source_suivi_traj = '{"source": "Source de l''historique : TEO 2021", "source_impacts": "Historique", "position_annotation": -50, "titre_graphique": "Données corrigées des variations climatiques en GWh"}',
        ges_export_excel = false
        WHERE id = 'paysdelaloire'"""
    )

    op.execute(
        """UPDATE meta.indicateur
        SET data = 'conso_energie'
        WHERE region = 'paysdelaloire' AND data LIKE 'conso_energetique';"""
    )

    op.execute(
        """ALTER TABLE paysdelaloire.conso_energetique
        RENAME TO conso_energie;"""
    )

    op.execute(
        """UPDATE meta.perimetre_geographique
        SET nom_table = 'conso_energie'
        WHERE region = 'paysdelaloire' AND nom_table = 'conso_energetique';"""
    )

    op.execute(
        """UPDATE meta.categorie
        SET nom = 'energieprod'
        WHERE region = 'paysdelaloire' AND nom = 'type_prod_enr';"""
    )
    op.execute(
        """UPDATE paysdelaloire.confid_camembert SET type_analyse = 'conso_energie' WHERE type_analyse = 'conso_energetique';"""
    )
    op.execute(
        """UPDATE paysdelaloire.confid_maille SET type_analyse = 'conso_energie' WHERE type_analyse = 'conso_energetique';"""
    )

    op.execute(
        """UPDATE meta.chart
        SET categorie = 'energieprod'
        WHERE categorie = 'type_prod_enr';"""
    )

    op.execute(
        """ALTER TABLE paysdelaloire.prod_enr
        RENAME COLUMN type_prod_enr TO energieprod;"""
    )

    op.execute(
        """DELETE FROM meta.association_graphique_table WHERE region = 'paysdelaloire';"""
    )

    op.execute("""DROP TABLE paysdelaloire.mobilite_insee;""")
    op.execute(
        """DELETE FROM meta.indicateur WHERE data = 'mobilite_insee' AND region = 'paysdelaloire';"""
    )

    op.execute("""DROP TABLE paysdelaloire.prod_enr_mapping;""")
    op.execute("""DROP TABLE paysdelaloire.secteur_mapping;""")
    op.execute("""TRUNCATE TABLE paysdelaloire.clap;""")
    op.execute("""REFRESH MATERIALIZED VIEW paysdelaloire.nb_emploi_territoire_view;""")

    op.execute(
        """UPDATE strategie_territoire.action 
        SET regions = array_remove(regions, 'paysdelaloire') WHERE 'paysdelaloire' = ANY(regions);"""
    )
