"""empty message

Revision ID: 8e4d7960855e
Revises: af10c66212c6, 09f8035a6b24
Create Date: 2024-06-07 16:03:52.666504

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8e4d7960855e"
down_revision = ("af10c66212c6", "09f8035a6b24")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
