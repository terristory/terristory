"""empty message

Revision ID: cff99cc4d3ad
Revises: b123e5484848, f1d2c1286d9e
Create Date: 2023-12-19 16:36:10.410684

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "cff99cc4d3ad"
down_revision = ("b123e5484848", "f1d2c1286d9e")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
