"""Rename order column

Revision ID: 5b14d6a28274
Revises: 8b826bf079f8
Create Date: 2024-07-18 10:17:11.739899

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5b14d6a28274"
down_revision = "8b826bf079f8"
branch_labels = None
depends_on = None

TABLES = ["params_user_action_params", "params_user_action_params_years"]
REGIONS = [
    "auvergne-rhone-alpes",
    "occitanie",
    "paysdelaloire",
    "bretagne",
    "nouvelle-aquitaine",
]


def upgrade():
    for table in TABLES:
        op.execute(
            f'ALTER TABLE strategie_territoire.{table} RENAME COLUMN "order" TO order_param'
        )


def downgrade():
    for table in TABLES:
        op.execute(
            f'ALTER TABLE strategie_territoire.{table} RENAME COLUMN order_param TO "order"'
        )
