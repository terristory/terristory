﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create sankey regional tables

Revision ID: fc7e4a209313
Revises: 13a27552f2e6
Create Date: 2022-07-13 09:44:41.537038

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "fc7e4a209313"
down_revision = "13a27552f2e6"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "conso_energetique_sankey",
        sa.Column("source", sa.String, comment="Nom de la source du flux"),
        sa.Column("target", sa.String, comment="Nom de la cible du flux"),
        sa.Column("id_source", sa.Integer, comment="ID de la source du flux"),
        sa.Column("id_target", sa.Integer, comment="ID de la cible du flux"),
        sa.Column("colors", sa.String, comment="Couleur utilisée"),
        sa.Column("label", sa.String, comment="Label du flux"),
        sa.Column("code", sa.String, comment="Code INSEE du territoire"),
        sa.Column("valeur", sa.Float, comment="Valeur de la donnée"),
        schema="auvergne_rhone_alpes",
    )


def downgrade():
    op.execute("""drop table auvergne_rhone_alpes.conso_energetique_sankey""")
