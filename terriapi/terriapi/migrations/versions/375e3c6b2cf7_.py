"""empty message

Revision ID: 375e3c6b2cf7
Revises: 195854ee1b23, 092c74a577e2
Create Date: 2023-03-30 17:21:45.701914

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "375e3c6b2cf7"
down_revision = ("195854ee1b23", "092c74a577e2")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
