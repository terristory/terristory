﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout_colonne_action_eco

Revision ID: 26c65990401a
Revises: 0b39ed7d1e26
Create Date: 2022-02-14 15:14:06.657499

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import Boolean, Column

# revision identifiers, used by Alembic.
revision = "26c65990401a"
down_revision = "0b39ed7d1e26"
branch_labels = None
depends_on = None
SCHEMA = "strategie_territoire"


def upgrade():
    op.add_column("action", Column("action_eco", Boolean), schema=SCHEMA)


def downgrade():
    with op.batch_alter_table("action", schema=SCHEMA) as batch_op:
        batch_op.drop_column("action_eco")
