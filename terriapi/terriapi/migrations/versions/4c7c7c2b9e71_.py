﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""empty message

Revision ID: 4c7c7c2b9e71
Revises: bc7a5be5a35e, 6e81db702827, 6601cbb2f1d9, 47c410bcb03d, c4bd9be72469
Create Date: 2022-11-02 10:57:42.891513

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4c7c7c2b9e71"
down_revision = (
    "bc7a5be5a35e",
    "6e81db702827",
    "6601cbb2f1d9",
    "47c410bcb03d",
    "c4bd9be72469",
)
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
