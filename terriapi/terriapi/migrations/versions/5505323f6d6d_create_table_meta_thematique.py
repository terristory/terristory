﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create table meta thematique

Revision ID: 5505323f6d6d
Revises: 8eb228db0a6e
Create Date: 2020-12-29 10:07:15.031759

"""
from alembic import op
from sqlalchemy import JSON, Column, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "5505323f6d6d"
down_revision = "8eb228db0a6e"
branch_labels = None
depends_on = None


SCHEMA = "meta"
TABLENAME = "tableau_thematique"


def upgrade():
    op.create_table(
        TABLENAME,
        Column("id", Integer, primary_key=True, comment="Identifiant"),
        Column("titre", String, comment="Titre"),
        Column(
            "tableau",
            Integer,
            ForeignKey(".".join([SCHEMA, "tableau_bord", "id"]), ondelete="CASCADE"),
            nullable=False,
            comment="Identifiant du tableau de bord",
        ),
        Column("graphiques", JSON, nullable=False, comment="Ensemble des graphiques"),
        schema=SCHEMA,
    )


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
