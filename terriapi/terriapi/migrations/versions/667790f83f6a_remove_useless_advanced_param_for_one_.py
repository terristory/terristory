"""Remove useless advanced param for one action

Revision ID: 667790f83f6a
Revises: e6dc37f046b0
Create Date: 2023-07-04 16:54:29.694129

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "667790f83f6a"
down_revision = "e6dc37f046b0"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """DELETE FROM strategie_territoire.params_user_action_params 
        WHERE action = '6b' AND nom = 'Coefficient d''éfficacité énergétique';"""
    )


def downgrade():
    op.execute(
        """
        INSERT INTO strategie_territoire.params_user_action_params 
        VALUES(45, 5, '6b', 'auvergne-rhone-alpes', 'parametres_avances', 'Coefficient d''éfficacité énergétique', '', 0.55),
            (45, 5, '6b', 'occitanie', 'parametres_avances', 'Coefficient d''éfficacité énergétique', '', 0.55),
            (45, 5, '6b', 'paysdelaloire', 'parametres_avances', 'Coefficient d''éfficacité énergétique', '', 0.55);"""
    )
