"""add seo table

Revision ID: 281d2b6026bc
Revises: 0731a2dab73e
Create Date: 2023-11-27 15:21:15.594006

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "281d2b6026bc"
down_revision = "0731a2dab73e"
branch_labels = None
depends_on = None

regions = [
    "auvergne-rhone-alpes",
    "occitanie",
    "paysdelaloire",
    "bretagne",
    "nouvelle-aquitaine",
    "corse",
]


def upgrade():
    op.execute(
        """
        CREATE TABLE meta.seo (
            region varchar,
            page varchar,
            meta_title text,
            meta_description text,
            PRIMARY KEY(region,page)
        );"""
    )

    for region in regions:
        op.execute(
            f"""
            insert into meta.seo(region, page, meta_title, meta_description) VALUES 
                ('{region}','simulator', 'Simulateur d''impacts mobilité : Donner des ordres de grandeur d''impacts associés aux principaux leviers de la mobilité','Simulateur d''impacts mobilité : TerriSTORY, outil d''aide à la transition écologique et énergétique des territoires'),
                ('{region}','legal-notice','Transition écologique, outil de stratégie territoriale TerriSTORY','Transition écologique et énergétique, utilisez la base de données TerriSTORY®, outil de datavisualisation/visualisation de données pour appréhender les enjeux environnementaux et socio-économiques de la région'),
                ('{region}','about','Transition écologique, outil de stratégie territoriale TerriSTORY', 'Transition écologique et énergétique, utilisez la base de données TerriSTORY®, outil de stratégie territoriale, pédagogique et multithématique (énergie, climat, économie...), pour appréhender les enjeux environnementaux et socio-économiques de la région'),
                ('{region}','open-source','Transition écologique, outil de stratégie territoriale TerriSTORY', 'Transition écologique et énergétique, utilisez la base de données TerriSTORY®, outil de stratégie territoriale, pédagogique et multithématique (énergie, climat, économie...), pour appréhender les enjeux environnementaux et socio-économiques de la région'),
                ('{region}','home-page','Transition énergétique, base de données des territoires TerriSTORY', 'Utilisez la base de données TerriSTORY®, outil d''aide au pilotage de la transition écologique et énergétique, pédagogique et multithématique (énergie, climat, économie...), des territoires pour appréhender les enjeux environnementaux et socio-économiques'),
                ('{region}','dashboard','TerriSTORY, outil d''aide à la transition des territoires', 'TerriSTORY, outil d''aide à la transition des territoires'),
                ('{region}','sankey','TerriSTORY, outil d''aide à la transition des territoires', 'TerriSTORY, outil d''aide à la transition des territoires');
            """
        )


def downgrade():
    op.execute("""DROP TABLE meta.seo;""")
