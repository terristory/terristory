"""add unit to actions for mobility simulator

Revision ID: 109d70d5246f
Revises: f5ca81c5952c
Create Date: 2023-06-13 12:53:17.706493

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "109d70d5246f"
down_revision = "f5ca81c5952c"
branch_labels = None
depends_on = None

SCHEMA = "didactic_file"
TABLENAME = "lever_action"


def upgrade():
    op.add_column(
        TABLENAME,
        Column("unit", String, nullable=True, comment="unité"),
        schema=SCHEMA,
    )


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("unit")
