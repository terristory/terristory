"""empty message

Revision ID: f5cd69a7d695
Revises: 0f5e59c182ed, 6870f02eb62f
Create Date: 2023-01-30 17:23:57.634517

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f5cd69a7d695"
down_revision = ("0f5e59c182ed", "6870f02eb62f")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
