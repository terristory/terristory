﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""compte-utilisateur-acces-indicateurs-desactives

Revision ID: f718e44ef13d
Revises: 8704355c4ce7
Create Date: 2022-06-16 09:11:51.132510

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "f718e44ef13d"
down_revision = "8704355c4ce7"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "alter table utilisateur add column acces_indicateurs_desactives boolean;"
    )


def downgrade():
    op.execute("alter table utilisateur drop column acces_indicateurs_desactives;")
