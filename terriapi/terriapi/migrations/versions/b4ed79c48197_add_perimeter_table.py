"""Add perimeter table

Revision ID: b4ed79c48197
Revises: ac3ed710f881
Create Date: 2024-05-27 15:28:11.668958

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "b4ed79c48197"
down_revision = "ac3ed710f881"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "perimeter_global",
        sa.Column("year", sa.Integer, primary_key=True, comment="Year of the data"),
        sa.Column(
            "update_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=True,
            comment="Date de mise à jour",
        ),
        schema="meta",
    )
    op.create_table(
        "perimeter_regional",
        sa.Column(
            "year",
            sa.Integer,
            primary_key=True,
            comment="Year of the regional perimeter",
        ),
        sa.Column("region", sa.String, primary_key=True, comment="Région"),
        sa.Column(
            "update_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=True,
            comment="Date de mise à jour",
        ),
        schema="meta",
    )


def downgrade():
    op.execute("""drop table meta.perimeter_regional""")
    op.execute("""drop table meta.perimeter_global""")
