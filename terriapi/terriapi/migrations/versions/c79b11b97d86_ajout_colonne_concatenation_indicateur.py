﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout-colonne-concatenation-indicateur

Revision ID: c79b11b97d86
Revises: 6edade4bfef6
Create Date: 2021-06-28 16:30:00.001650

"""

from alembic import op
from sqlalchemy import Column

# revision identifiers, used by Alembic.
revision = "c79b11b97d86"
down_revision = "6edade4bfef6"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "indicateur"


def upgrade():
    op.execute("alter table meta.indicateur add column concatenation int[]")


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("concatenation")
