"""Add auto_select_maille to regions configuration

Revision ID: 04f58c965cc4
Revises: fc673597e447
Create Date: 2024-03-22 11:23:01.006086

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "04f58c965cc4"
down_revision = "fc673597e447"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("auto_select_maille", sa.Boolean, server_default="f"),
        schema="public",
    )


def downgrade():
    with op.batch_alter_table("regions_configuration", schema="public") as batch_op:
        batch_op.drop_column("auto_select_maille")
