﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Ajout d'une table de consultations pour les tableaux de bords

Revision ID: e17f2a6ea3d9
Revises: d483358c2a88
Create Date: 2022-08-17 14:35:21.051399

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e17f2a6ea3d9"
down_revision = "d483358c2a88"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "tableaux_bords",
        sa.Column(
            "id", sa.Integer, primary_key=True, comment="Identifiant"
        ),  # crée un identifiant incrémental
        sa.Column(
            "id_utilisateur",
            sa.Integer,
            sa.ForeignKey("consultations.ip_localisation.id", ondelete="CASCADE"),
        ),
        sa.Column("region", sa.String, nullable=False, comment="Nom de la région"),
        sa.Column("code_territoire", sa.String, comment="Code INSEE du territoire"),
        sa.Column(
            "type_territoire",
            sa.String,
            comment="Type de territoire sélectionné (Région, EPCI etc.)",
        ),
        sa.Column("tableau_bord_id", sa.Integer, comment="ID du tableau de bord"),
        sa.Column("tableau_bord_nom", sa.String, comment="Nom du tableau de bord"),
        sa.Column(
            "date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=False,
            comment="Date de l'activité",
        ),
        schema="consultations",
    )


def downgrade():
    op.drop_table(schema="consultations", table_name="tableaux_bords")
