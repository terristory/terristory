﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""suivi des poi

Revision ID: abe908a85a3c
Revises: 4d5d82ae8392
Create Date: 2021-02-11 11:24:45.180048

"""
from alembic import op
from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "abe908a85a3c"
down_revision = "4d5d82ae8392"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "poi",
        Column(
            "id", Integer, primary_key=True, comment="Identifiant"
        ),  # crée un identifiant incrémental
        Column(
            "id_utilisateur",
            Integer,
            ForeignKey("consultations.ip_localisation.id", ondelete="CASCADE"),
        ),
        Column("region", String, nullable=False, comment="Nom de la région"),
        Column("code_territoire", String, comment="Code INSEE du territoire"),
        Column(
            "type_territoire",
            String,
            comment="Type de territoire sélectionné (Région, EPCI etc.)",
        ),
        Column("nom_couche", String, comment="Nom de la couche POI"),
        Column(
            "cochee",
            Boolean,
            comment="La personne a-t-elle activé ou désactivé la couche ?",
        ),
        Column(
            "date",
            DateTime,
            server_default="now",
            nullable=False,
            comment="Date de l'activité",
        ),
        schema="consultations",
    )


def downgrade():
    op.drop_table("poi", schema="consultations")
