﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""fusion perimetre colonne action eco mobilite

Revision ID: 737c5cb25ad9
Revises: 26c65990401a, 82edda289958
Create Date: 2022-02-22 16:08:43.631916

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "737c5cb25ad9"
down_revision = ("26c65990401a", "82edda289958")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
