"""empty message

Revision ID: bcd171348a27
Revises: 285e78a1d184, 3d78e32bc241
Create Date: 2025-01-14 17:49:46.458017

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bcd171348a27"
down_revision = ("285e78a1d184", "3d78e32bc241")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
