"""Add communes into the 'territoire' tables

Revision ID: f2632278515b
Revises: 99922858f271
Create Date: 2023-07-12 12:35:08.937237

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f2632278515b"
down_revision = "99922858f271"
branch_labels = None
depends_on = None

SCHEMAS = [
    "auvergne_rhone_alpes",
    "bretagne",
    "corse",
    "nouvelle_aquitaine",
    "occitanie",
    "paysdelaloire",
]


def upgrade():
    for schema in SCHEMAS:
        op.execute(
            f"""
            INSERT INTO {schema}.territoire (commune, code, type_territoire, nom)
                SELECT c.code, c.code, 'commune', c.nom
                FROM {schema}.commune c
            """
        )


def downgrade():
    for schema in SCHEMAS:
        op.execute(
            f"""
            DELETE FROM {schema}.territoire WHERE type_territoire = 'commune'
            """
        )
