"""Fix injection issue

Revision ID: a17e6162d9ab
Revises: e1356f351c31
Create Date: 2024-10-03 17:11:56.586682

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a17e6162d9ab"
down_revision = "e1356f351c31"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        UPDATE strategie_territoire.params_user_action_params SET unite = '%', valeur = 98 WHERE nom = 'Taux de rendement du purificateur';
        UPDATE strategie_territoire.params_user_action_params SET unite = '%', valeur = 10, nom = 'Taux de consommation pour le chauffage du digesteur' WHERE nom = 'Taux de consommation pour la chauffage du digesteur';
               
        UPDATE utilisateur_scenario_params_advanced 
            SET params_avances = REPLACE(params_avances::text, '"Taux de rendement du purificateur", 0.', '"Taux de rendement du purificateur", ')::jsonb
            WHERE action = '6b';
        UPDATE utilisateur_scenario_params_advanced 
            SET params_avances = REPLACE(params_avances::text, '"Taux de consommation pour la chauffage du digesteur", 0.', '"Taux de consommation pour le chauffage du digesteur", ')::jsonb
            WHERE action = '6b';
        UPDATE utilisateur_scenario_params_advanced 
            SET params_avances = REPLACE(params_avances::text, '"Taux de rendement du purificateur", null, 0.', '"Taux de rendement du purificateur", "%", ')::jsonb
            WHERE action = '6b';
        UPDATE utilisateur_scenario_params_advanced 
            SET params_avances = REPLACE(params_avances::text, '"Taux de consommation pour la chauffage du digesteur", null, 0.', '"Taux de consommation pour le chauffage du digesteur", "%", ')::jsonb
            WHERE action = '6b';
        UPDATE utilisateur_scenario_params_advanced 
            SET params_avances = REPLACE(params_avances::text, '"Taux de rendement du purificateur", null, "0.', '"Taux de rendement du purificateur", "%", "')::jsonb
            WHERE action = '6b';
        UPDATE utilisateur_scenario_params_advanced 
            SET params_avances = REPLACE(params_avances::text, '"Taux de consommation pour la chauffage du digesteur", null, "0.', '"Taux de consommation pour le chauffage du digesteur", "%", "')::jsonb
            WHERE action = '6b';
    """
    )


def downgrade():
    op.execute(
        """
        UPDATE strategie_territoire.params_user_action_params SET unite = '', valeur = 0.98 WHERE nom = 'Taux de rendement du purificateur';
        UPDATE strategie_territoire.params_user_action_params SET unite = '', valeur = 0.10, nom = 'Taux de consommation pour la chauffage du digesteur' WHERE nom = 'Taux de consommation pour le chauffage du digesteur';
    """
    )
