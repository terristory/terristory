﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add possibility to specify number of classes in meta indicator

Revision ID: dcfde9318682
Revises: 75645bc0011d
Create Date: 2022-10-27 16:52:04.408941

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "dcfde9318682"
down_revision = "75645bc0011d"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "indicateur",
        sa.Column("nb_classes_color_representation", sa.Integer, default=None),
        schema="meta",
    )


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("nb_classes_color_representation")
