"""Move sankey geographical level to meta data

Revision ID: 99922858f271
Revises: a10ea9ed2806
Create Date: 2023-06-27 17:09:45.414239

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "99922858f271"
down_revision = "a10ea9ed2806"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        f"""
        ALTER TABLE meta.sankey_metadata ADD COLUMN geographical_levels_enabled TEXT;
        UPDATE meta.sankey_metadata d SET geographical_levels_enabled = (
            SELECT sankey_divisions_enabled FROM regions_configuration r
            WHERE r.id = d.region
            LIMIT 1
        );
        ALTER TABLE regions_configuration DROP COLUMN sankey_divisions_enabled;
        """
    )


def downgrade():
    op.execute(
        f"""
        ALTER TABLE regions_configuration ADD COLUMN sankey_divisions_enabled TEXT;
        UPDATE regions_configuration r SET sankey_divisions_enabled = (
            SELECT geographical_levels_enabled FROM meta.sankey_metadata d
            WHERE r.id = d.region
            LIMIT 1
        );
        ALTER TABLE meta.sankey_metadata DROP COLUMN geographical_levels_enabled;
        """
    )
