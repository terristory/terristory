﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""fusion

Revision ID: 1cd6d1273fdf
Revises: a5e7591eef22, ceb2aaf76e16
Create Date: 2021-12-09 17:39:02.719690

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "1cd6d1273fdf"
down_revision = ("a5e7591eef22", "ceb2aaf76e16")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
