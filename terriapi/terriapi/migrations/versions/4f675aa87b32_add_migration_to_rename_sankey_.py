"""Add migration to rename sankey parameters


Revision ID: 4f675aa87b32
Revises: f5ca81c5952c
Create Date: 2023-06-15 15:25:02.274355

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4f675aa87b32"
down_revision = "f5ca81c5952c"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        f"""
        ALTER TABLE meta.donnees_sankey RENAME COLUMN nom_table TO data_table;
        ALTER TABLE meta.donnees_sankey RENAME COLUMN texte_introduction TO introduction_text;
        ALTER TABLE meta.donnees_sankey RENAME COLUMN unite TO unit;
        ALTER TABLE meta.donnees_sankey RENAME COLUMN annee TO year;
        ALTER TABLE meta.donnees_sankey RENAME TO sankey_metadata;
        """
    )


def downgrade():
    op.execute(
        f"""
        ALTER TABLE meta.sankey_metadata RENAME TO donnees_sankey;
        ALTER TABLE meta.donnees_sankey RENAME COLUMN data_table TO nom_table;
        ALTER TABLE meta.donnees_sankey RENAME COLUMN introduction_text TO texte_introduction;
        ALTER TABLE meta.donnees_sankey RENAME COLUMN unit TO unite;
        ALTER TABLE meta.donnees_sankey RENAME COLUMN year TO annee;
        """
    )
