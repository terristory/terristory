﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajouter installation geothermique

Revision ID: 02b6d51471e0
Revises: f79066411d21
Create Date: 2022-04-28 13:39:29.759208

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "02b6d51471e0"
down_revision = "f79066411d21"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        insert into auvergne_rhone_alpes_poi.layer(nom,label,couleur,modifiable,rubrique,type_installation,type_geom,statut)
        values('geothermie','Installations géothermiques','#ffffff',false,'Installations EnR','','Point',false);"""
    )
    op.execute(
        """
        create table if not exists auvergne_rhone_alpes_poi.geothermie as (select * from  auvergne_rhone_alpes_poi.borne_irve);
        delete from auvergne_rhone_alpes_poi.geothermie;
        """
    )


def downgrade():
    op.execute(
        """
        drop table auvergne_rhone_alpes_poi.geothermie;
        delete from auvergne_rhone_alpes_poi.layer where nom='geothermie';
        """
    )
