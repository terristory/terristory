"""add fixed capital table

Revision ID: 10f1d3b629c4
Revises: 54dff651d20c
Create Date: 2023-06-01 10:56:07.048107

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "10f1d3b629c4"
down_revision = "54dff651d20c"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
    CREATE TABLE IF NOT EXISTS strategie_territoire.fixed_capital (
        category varchar,
        region varchar,
        value double precision,
        PRIMARY KEY(category, region)
    )
    """
    )


def downgrade():
    op.drop_table("fixed_capital", schema="strategie_territoire")
