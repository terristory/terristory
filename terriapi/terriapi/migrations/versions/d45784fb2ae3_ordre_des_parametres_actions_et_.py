﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ordre des parametres actions et rubriques

Revision ID: d45784fb2ae3
Revises: 26c65990401a
Create Date: 2022-02-24 10:24:14.464962

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "d45784fb2ae3"
down_revision = "26c65990401a"
branch_labels = None
depends_on = None

schema = "strategie_territoire"


def upgrade():
    op.execute(
        "alter table {schema}.action add column ordre_categorie int;".format(
            schema=schema
        )
    )
    op.execute(
        "alter table {schema}.action add column ordre_action int;".format(schema=schema)
    )
    op.execute(
        "alter table {schema}.action add column regions varchar[];".format(
            schema=schema
        )
    )
    op.execute(
        "update strategie_territoire.action set regions = '{\"auvergne-rhone-alpes\", \"occitanie\"}' where numero != '14' or numero != '15';"
    )
    op.execute(
        "update strategie_territoire.action set regions = '{\"auvergne-rhone-alpes\"}' where numero = '14' or numero = '15';"
    )
    op.execute(
        "update strategie_territoire.action set regions = '{\"occitanie\"}' where numero in ('16','17');"
    )
    op.execute(
        "alter table {schema}.action_params add column ordre int;".format(schema=schema)
    )


def downgrade():
    op.execute(
        "alter table {schema}.action drop column ordre_categorie;".format(schema=schema)
    )
    op.execute(
        "alter table {schema}.action drop column ordre_action;".format(schema=schema)
    )
    op.execute("alter table {schema}.action drop column regions;".format(schema=schema))
    op.execute(
        "alter table {schema}.action_params drop column ordre;".format(schema=schema)
    )
