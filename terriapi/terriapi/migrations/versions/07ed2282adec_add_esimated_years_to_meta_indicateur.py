"""Add esimated years to meta.indicateur

Revision ID: 07ed2282adec
Revises: cccdea3d5af9
Create Date: 2023-01-20 12:29:51.024351

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy import ARRAY, Column, Integer

# revision identifiers, used by Alembic.
revision = "07ed2282adec"
down_revision = "cccdea3d5af9"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "indicateur"


def upgrade():
    op.add_column(TABLENAME, Column("estimated_years", ARRAY(Integer)), schema=SCHEMA)


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("estimated_years")
