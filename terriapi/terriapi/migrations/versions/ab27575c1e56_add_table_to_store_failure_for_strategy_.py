"""Add table to store failure for strategy module

Revision ID: ab27575c1e56
Revises: 0aaba7560457
Create Date: 2023-08-31 16:51:09.266623

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "ab27575c1e56"
down_revision = "0aaba7560457"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "action_errors",
        sa.Column("region", sa.String, comment="Région"),
        sa.Column("action_numero", sa.String, comment="Action number"),
        sa.Column("action_key", sa.String, comment="Action key"),
        sa.Column("zone_id", sa.String, comment="Territory code"),
        sa.Column("zone_type", sa.String, comment="Territory type"),
        sa.Column("error_message", sa.Text, comment="Error message"),
        sa.Column(
            "error_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=True,
            comment="Error date",
        ),
        schema="strategie_territoire",
    )


def downgrade():
    op.execute("""drop table strategie_territoire.action_errors""")
