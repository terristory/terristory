﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Fusion des branches en conflit

Revision ID: a7384f680662
Revises: c79b11b97d86, 3fb23f1fdcb0
Create Date: 2021-07-01 15:34:20.656028

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a7384f680662"
down_revision = ("c79b11b97d86", "3fb23f1fdcb0")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
