"""Remove old pcaet table

Revision ID: 760b4db4a79d
Revises: 78dbc6764049
Create Date: 2024-02-09 09:56:16.884172

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "760b4db4a79d"
down_revision = "78dbc6764049"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("DROP TABLE IF EXISTS pcaet_ademe")
    op.execute("ALTER TABLE new_pcaet RENAME TO pcaet")
    op.execute(
        """
        ALTER TABLE pcaet
            ALTER siren SET NOT NULL,
            ALTER trajectory SET NOT NULL,
            ALTER category SET NOT NULL,
            ALTER year SET NOT NULL,
            ADD CONSTRAINT pcaet_pkey PRIMARY KEY (siren, trajectory, category, year);
        """
    )


def downgrade():
    op.execute(
        """
        ALTER TABLE pcaet
            DROP CONSTRAINT IF EXISTS pcaet_pkey,
            ALTER siren DROP NOT NULL,
            ALTER trajectory DROP NOT NULL,
            ALTER category DROP NOT NULL,
            ALTER year DROP NOT NULL;
        """
    )
    op.execute("ALTER TABLE pcaet RENAME TO new_pcaet")
