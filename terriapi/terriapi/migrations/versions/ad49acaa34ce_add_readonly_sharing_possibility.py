"""Add readonly sharing possibility

Revision ID: ad49acaa34ce
Revises: 8f5f96167fb9
Create Date: 2023-11-16 17:32:42.080282

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "ad49acaa34ce"
down_revision = "8f5f96167fb9"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("ALTER TABLE meta.tableau_bord RENAME partage TO full_share;")
    op.execute("ALTER TABLE meta.tableau_bord ADD COLUMN readonly_share TEXT[];")


def downgrade():
    op.execute("ALTER TABLE meta.tableau_bord RENAME full_share TO partage;")
    op.execute("ALTER TABLE meta.tableau_bord DROP COLUMN readonly_share;")
