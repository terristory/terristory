"""update didactic file DB

Revision ID: 0ec6aefc2ec0
Revises: ec0705148ef6
Create Date: 2023-03-24 14:22:48.106453

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0ec6aefc2ec0"
down_revision = "ec0705148ef6"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE meta.didactic_file_section (
            id serial,
            title varchar,
            comment varchar, 
            type varchar,
            subcategory_id int,
            section_order int,
            analysis json,
            PRIMARY KEY(id),
            CONSTRAINT fk_didactic_file_subcategory FOREIGN KEY(subcategory_id) REFERENCES meta.didactic_file_sub_category(id)
            ON DELETE CASCADE
        );

        DROP TABLE meta.didactic_file_types;
        ALTER TABLE meta.didactic_file DROP COLUMN type;
        ALTER TABLE meta.didactic_file_sub_category DROP COLUMN analysis_list;
       
        """
    )


def downgrade():

    op.drop_table("didactic_file_section", schema="meta")
    op.execute(
        """
        CREATE TABLE meta.didactic_file_types (
                type_id serial,
                value varchar,
                label varchar,
                type_order int,
                region varchar,
                PRIMARY KEY(type_id));

        ALTER TABLE meta.didactic_file ADD COLUMN type VARCHAR;
        ALTER TABLE meta.didactic_file_sub_category ADD COLUMN analysis_list json;
    """
    )
