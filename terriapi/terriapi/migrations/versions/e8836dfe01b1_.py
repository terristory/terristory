"""empty message

Revision ID: e8836dfe01b1
Revises: 1e6b1174be63, 205bca8c061a
Create Date: 2025-01-29 15:24:04.115746

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e8836dfe01b1"
down_revision = ("1e6b1174be63", "205bca8c061a")
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "ALTER TABLE regions_configuration ALTER COLUMN ui_show_simulators SET DEFAULT '{}';"
    )


def downgrade():
    pass
