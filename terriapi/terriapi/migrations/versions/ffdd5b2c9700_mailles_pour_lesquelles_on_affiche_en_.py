﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""mailles pour lesquelles on affiche en mode valeur

Revision ID: ffdd5b2c9700
Revises: d61cad511958
Create Date: 2022-01-20 11:37:56.876404

"""
from alembic import op
from sqlalchemy import Column, PrimaryKeyConstraint, String

# revision identifiers, used by Alembic.
revision = "ffdd5b2c9700"
down_revision = "d61cad511958"
branch_labels = None
depends_on = None


schema = "meta"


def creer_table_affectation_objectifs(schema):
    op.create_table(
        "affectation_objectifs",
        Column(
            "titre", String, comment="Titre de l'objectif (SRADDET, REPOS, SNBC etc.)"
        ),
        Column("graphique", String, comment="Graphique auquel ajouter l'objectif"),
        Column("region", String, comment="Région pour laquelle l'objectif est saisi"),
        Column(
            "type_territoire",
            String,
            comment="Maille géographique pour laquelle on affiche les objectifs en mode valeur",
        ),
        Column(
            "code_territoire",
            String,
            comment="Code du territoire pour laquelle on affiche les objectifs en mode valeur",
        ),
        PrimaryKeyConstraint(
            "titre", "graphique", "region", "type_territoire", "code_territoire"
        ),
        schema=schema,
    )


def upgrade():
    creer_table_affectation_objectifs(schema)


def downgrade():
    op.execute("drop table " + schema + ".affectation_objectifs cascade;")
