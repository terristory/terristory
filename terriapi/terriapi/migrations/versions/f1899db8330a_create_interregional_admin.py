﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create interregional admin

Revision ID: f1899db8330a
Revises: 9605d24996e3
Create Date: 2022-07-11 13:42:26.932006

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f1899db8330a"
down_revision = "9605d24996e3"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "utilisateur", sa.Column("global_admin", sa.Boolean, server_default=str(False))
    )


def downgrade():
    op.drop_column("utilisateur", "global_admin")
