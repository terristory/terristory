"""empty message

Revision ID: fc6b4a6d6f90
Revises: 5b14d6a28274, 93a34c1d6171
Create Date: 2024-07-23 11:16:03.713903

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "fc6b4a6d6f90"
down_revision = ("5b14d6a28274", "93a34c1d6171")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
