"""Add nb decimals column to Sankey metadata

Revision ID: f81622a8be35
Revises: ec0705148ef6
Create Date: 2023-03-27 14:00:00.933118

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f81622a8be35"
down_revision = "ec0705148ef6"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "donnees_sankey",
        sa.Column("nb_decimals", sa.Integer, server_default="0"),
        schema="meta",
    )
    op.create_primary_key(
        "pk_donnees_sankey", "donnees_sankey", ["nom_table", "region"], schema="meta"
    )


def downgrade():
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.drop_column("nb_decimals")
        batch_op.drop_constraint("pk_donnees_sankey")
