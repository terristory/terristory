﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""corriger une erreur - table data.gel

Revision ID: 2235ce301cec
Revises: 669f2e4ba1eb
Create Date: 2021-08-27 15:28:42.723897

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "2235ce301cec"
down_revision = "669f2e4ba1eb"
branch_labels = None
depends_on = None


def upgrade():
    op.drop_table("data_gel", schema="auvergne_rhone_alpes")
    op.execute(
        f"""create table auvergne_rhone_alpes.data_gel (
    id serial NOT NULL
    , station_id int NOT NULL
    , nom varchar
    , annee int
    , libelle_mois varchar
    , valeur int NOT NULL
    , nb_non_null int NOT NULL
    , PRIMARY KEY (id)
    , FOREIGN KEY(station_id) REFERENCES  auvergne_rhone_alpes_station_mesure.station_mesure(id)
);"""
    )


def downgrade():
    op.drop_table("data_gel", schema="auvergne_rhone_alpes")
