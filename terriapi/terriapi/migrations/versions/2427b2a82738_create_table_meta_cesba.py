﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create table meta cesba

Revision ID: 2427b2a82738
Revises: 6807a8a76276
Create Date: 2020-10-20 15:33:05.811671

"""

from alembic import op
from sqlalchemy import CheckConstraint, Column, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "2427b2a82738"
down_revision = "6807a8a76276"
branch_labels = None
depends_on = None


SCHEMA = "meta"
TABLENAME = "cesba"


def auvergne_rhone_alpes(table):
    """Insertion de données dans la table pour la région Auvergne-Rhône-Alpes"""
    data = [
        {
            "indicateur_id": 15,
            "numero": "B1.1",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "desc",
        },
        {
            "indicateur_id": 16,
            "numero": "B1.3",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "desc",
        },
        {
            "indicateur_id": 17,
            "numero": "B1.2",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "desc",
        },
        {
            "indicateur_id": 18,
            "numero": "B1.8",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "desc",
        },
        {
            "indicateur_id": 20,
            "numero": "E1.18",
            "theme": "E - Économie",
            "order": "desc",
        },
        {
            "indicateur_id": 21,
            "numero": "E1.20",
            "theme": "E - Économie",
            "order": "desc",
        },
        {
            "indicateur_id": 22,
            "numero": "E1.19",
            "theme": "E - Économie",
            "order": "desc",
        },
        {
            "indicateur_id": 30,
            "numero": "A8.1",
            "theme": "A - Territoire et environnement",
            "order": "desc",
        },
        {
            "indicateur_id": 31,
            "numero": "A8.3",
            "theme": "A - Territoire et environnement",
            "order": "desc",
        },
        {
            "indicateur_id": 32,
            "numero": "A8.4",
            "theme": "A - Territoire et environnement",
            "order": "desc",
        },
        {
            "indicateur_id": 34,
            "numero": "C1.11",
            "theme": "C - Infrastructures et services",
            "order": "asc",
        },
        {
            "indicateur_id": 35,
            "numero": "C5.1",
            "theme": "C - Infrastructures et services",
            "order": "asc",
        },
        {
            "indicateur_id": 37,
            "numero": "B4.1",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "desc",
        },
        {
            "indicateur_id": 38,
            "numero": "D2.27",
            "theme": "D - Société",
            "order": "asc",
        },
        {
            "indicateur_id": 39,
            "numero": "E1.6",
            "theme": "E - Économie",
            "order": "asc",
        },
        {
            "indicateur_id": 40,
            "numero": "D2.3",
            "theme": "D - Société",
            "order": "desc",
        },
        {
            "indicateur_id": 41,
            "numero": "D2.20",
            "theme": "D - Société",
            "order": "asc",
        },
        {
            "indicateur_id": 42,
            "numero": "A1.6",
            "theme": "A - Territoire et environnement",
            "order": "asc",
        },
        {
            "indicateur_id": 43,
            "numero": "A3.1",
            "theme": "A - Territoire et environnement",
            "order": "asc",
        },
        {
            "indicateur_id": 44,
            "numero": "B4.6",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "desc",
        },
        {
            "indicateur_id": 45,
            "numero": "E4.2",
            "theme": "E - Économie",
            "order": "asc",
        },
        {
            "indicateur_id": 23,
            "numero": "B1.20",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "asc",
        },
    ]
    op.bulk_insert(table, data)


def occitanie(table):
    """Insertion de données dans la table pour la région Occitanie"""
    data = [
        {
            "indicateur_id": 50,
            "numero": "B1.1",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "desc",
        },
        {
            "indicateur_id": 52,
            "numero": "E1.18",
            "theme": "E - Économie",
            "order": "desc",
        },
        {
            "indicateur_id": 53,
            "numero": "B1.20",
            "theme": "B - Énergie / Consommation de ressources",
            "order": "asc",
        },
        {
            "indicateur_id": 55,
            "numero": "A8.1",
            "theme": "A - Territoire et environnement",
            "order": "desc",
        },
        {
            "indicateur_id": 60,
            "numero": "C1.11",
            "theme": "C - Infrastructures et services",
            "order": "asc",
        },
    ]
    op.bulk_insert(table, data)


def upgrade():
    indicateur_table = ".".join([SCHEMA, "indicateur"])
    cesba_table = op.create_table(
        TABLENAME,
        Column(
            "indicateur_id",
            Integer,
            ForeignKey(indicateur_table + ".id"),
            nullable=False,
            comment="Identifiant de l'indicateur",
        ),
        Column("numero", String, comment="Numéro CESBA"),
        Column("theme", String, comment="Nom du thème CESBA"),
        Column(
            "order",
            String,
            CheckConstraint("\"order\" in ('asc', 'desc')"),
            nullable=False,
            comment="Ordre de classement pour les notes CESBA",
        ),
        comment="Table des indicateurs sur lesquels on va calculer une note CESBA",
        schema=SCHEMA,
    )
    auvergne_rhone_alpes(cesba_table)
    occitanie(cesba_table)


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
