﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Cast category color column

Revision ID: 9f5b0900b0db
Revises: f1370e335e48
Create Date: 2022-11-29 15:21:27.865241

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9f5b0900b0db"
down_revision = "f1370e335e48"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        table_name="categorie",
        schema="meta",
        column_name="couleur",
        type_=sa.String(length=30),
    )


def downgrade():
    op.alter_column(
        table_name="categorie",
        schema="meta",
        column_name="couleur",
        type_=sa.String(length=7),
    )
