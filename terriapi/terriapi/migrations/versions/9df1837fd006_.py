"""empty message

Revision ID: 9df1837fd006
Revises: a14b6b247206, a4bdf9d4e265
Create Date: 2024-10-22 18:58:20.913991

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9df1837fd006"
down_revision = ("a14b6b247206", "a4bdf9d4e265")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
