"""Add description to action

Revision ID: 4eb4750e9709
Revises: 82ba6d55b07f
Create Date: 2024-02-07 17:41:47.852402

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4eb4750e9709"
down_revision = "82ba6d55b07f"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "action_status",
        sa.Column("description", sa.String()),
        schema="strategie_territoire",
    )


def downgrade():
    with op.batch_alter_table(
        "action_status", schema="strategie_territoire"
    ) as batch_op:
        batch_op.drop_column("description")
