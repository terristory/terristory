"""Add territoire_predilection and code_postal to table utilisateurs

Revision ID: 1e93b140d164
Revises: 375e3c6b2cf7
Create Date: 2023-04-19 10:38:51.748832

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "1e93b140d164"
down_revision = "375e3c6b2cf7"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ALTER TABLE utilisateur
            ADD COLUMN territoire_predilection json DEFAULT '{"zoneType": "region", "zoneMaille": "epci"}',
            ADD COLUMN code_postal VARCHAR,
            ADD COLUMN utiliser_territoire_predilection BOOL DEFAULT false;"""
    )


def downgrade():
    op.execute(
        """ALTER TABLE utilisateur
            DROP COLUMN IF EXISTS territoire_predilection,
            DROP COLUMN IF EXISTS code_postal,
            DROP COLUMN IF EXISTS utiliser_territoire_predilection;"""
    )
