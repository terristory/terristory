"""empty message

Revision ID: bede2f649b8d
Revises: 2037f4ffee0c, 667bc2cb4c51
Create Date: 2024-10-29 10:16:53.909713

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bede2f649b8d"
down_revision = ("2037f4ffee0c", "667bc2cb4c51")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
