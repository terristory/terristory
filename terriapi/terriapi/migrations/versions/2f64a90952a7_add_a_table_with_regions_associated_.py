"""Add a table with regions associated with a national user

Revision ID: 2f64a90952a7
Revises: 3ed0009af919
Create Date: 2024-08-21 10:42:50.913220

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "2f64a90952a7"
down_revision = "3ed0009af919"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "user_national_regions",
        sa.Column("region_id", sa.String, primary_key=True, comment="Clé de la région"),
        sa.Column("mail", sa.String, primary_key=True, comment="Mail de l'utilisateur"),
        schema="meta",
    )


def downgrade():
    op.drop_table("user_national_regions", schema="meta")
