﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create table meta chart

Revision ID: 6807a8a76276
Revises: 413700e3178a
Create Date: 2020-10-20 12:00:59.304860

"""

from alembic import op
from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    PrimaryKeyConstraint,
    String,
    Text,
)

# revision identifiers, used by Alembic.
revision = "6807a8a76276"
down_revision = "413700e3178a"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "chart"


def auvergne_rhone_alpes(table):
    """Insertion de données dans la table pour la région Auvergne-Rhône-Alpes"""
    data = [
        {
            "indicateur": 1,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 1,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 1,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 2,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 3,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 4,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 5,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 6,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 7,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 8,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 9,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 10,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 11,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 12,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 13,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 14,
            "titre": "Par type de collecte",
            "type": "pie",
            "categorie": "type_collectes",
            "visible": True,
        },
        {
            "indicateur": 14,
            "titre": "Par type de déchets",
            "type": "pie",
            "categorie": "type_dechets",
            "visible": True,
        },
        {
            "indicateur": 15,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 15,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": False,
        },
        {
            "indicateur": 15,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": False,
        },
        {
            "indicateur": 16,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 16,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 16,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 17,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 17,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 17,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 19,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "facture_energie",
            "visible": True,
        },
        {
            "indicateur": 19,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "facture_secteur",
            "visible": True,
        },
        {
            "indicateur": 19,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "facture_usage",
            "visible": True,
        },
        {
            "indicateur": 20,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "facture_secteur",
            "visible": False,
        },
        {
            "indicateur": 20,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "facture_energie",
            "visible": False,
        },
        {
            "indicateur": 20,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "facture_usage",
            "visible": False,
        },
        {
            "indicateur": 21,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "facture_energie",
            "visible": True,
        },
        {
            "indicateur": 21,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "facture_secteur",
            "visible": False,
        },
        {
            "indicateur": 21,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "facture_usage",
            "visible": True,
        },
        {
            "indicateur": 22,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "facture_secteur",
            "visible": False,
        },
        {
            "indicateur": 22,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "facture_energie",
            "visible": True,
        },
        {
            "indicateur": 22,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "facture_usage",
            "visible": True,
        },
        {
            "indicateur": 23,
            "titre": "Production EnR / Consommation d'énergie finale (GWh)",
            "type": "hbar",
            "categorie": "type_prod_enr",
            "visible": True,
        },
        {
            "indicateur": 24,
            "titre": "Potentiel méthanisation",
            "type": "pie",
            "categorie": "filiere_methanisation",
            "visible": True,
        },
        {
            "indicateur": 25,
            "titre": "Type de zones",
            "type": "pie",
            "categorie": "enjeu",
            "visible": True,
        },
        {
            "indicateur": 25,
            "titre": "Gamme de pente",
            "type": "pie",
            "categorie": "pente_eolien",
            "visible": True,
        },
        {
            "indicateur": 25,
            "titre": "Gamme d'altitude",
            "type": "pie",
            "categorie": "altitude",
            "visible": True,
        },
        {
            "indicateur": 26,
            "titre": "Type d'essence",
            "type": "pie",
            "categorie": "essence",
            "visible": True,
        },
        {
            "indicateur": 26,
            "titre": "Classe de propriété",
            "type": "pie",
            "categorie": "propriete",
            "visible": True,
        },
        {
            "indicateur": 26,
            "titre": "Gamme de pente (%)",
            "type": "pie",
            "categorie": "pente",
            "visible": True,
        },
        {
            "indicateur": 27,
            "titre": "Secteur",
            "type": "pie",
            "categorie": "secteur_solaire_thermique",
            "visible": True,
        },
        {
            "indicateur": 28,
            "titre": "Type de bâtiments et parkings",
            "type": "pie",
            "categorie": "type_photovoltaique",
            "visible": True,
        },
        {
            "indicateur": 28,
            "titre": "Orientation",
            "type": "pie",
            "categorie": "orientation_photovoltaique",
            "visible": True,
        },
        {
            "indicateur": 28,
            "titre": "Contraintes patrimoniales",
            "type": "pie",
            "categorie": "contrainte_photovoltaique",
            "visible": True,
        },
        {
            "indicateur": 29,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 29,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 29,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 30,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 30,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": False,
        },
        {
            "indicateur": 30,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": False,
        },
        {
            "indicateur": 31,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 31,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 31,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 32,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 32,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 32,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 36,
            "titre": "répartition en 5 secteurs",
            "type": "pie",
            "categorie": "secteur_clap5",
            "visible": True,
        },
        {
            "indicateur": 36,
            "titre": "répartition en 17 secteurs",
            "type": "pie",
            "categorie": "secteur_clap17",
            "visible": True,
        },
        {
            "indicateur": 42,
            "titre": "Par type de surface",
            "type": "pie",
            "categorie": "type_occsol",
            "visible": True,
        },
        {
            "indicateur": 46,
            "titre": "Filière de production",
            "type": "pie",
            "categorie": "type_prod_enr",
            "visible": True,
        },
    ]
    op.bulk_insert(table, data)


def occitanie(table):
    """Insertion de données dans la table pour la région Occitanie"""
    data = [
        {
            "indicateur": 47,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 47,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 47,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 48,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 48,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 49,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 49,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 50,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 50,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": False,
        },
        {
            "indicateur": 50,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": False,
        },
        {
            "indicateur": 51,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 51,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 51,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 52,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": False,
        },
        {
            "indicateur": 52,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": False,
        },
        {
            "indicateur": 52,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": False,
        },
        {
            "indicateur": 53,
            "titre": "Production EnR / Consommation d'énergie finale (GWh)",
            "type": "hbar",
            "categorie": "type_prod_enr",
            "visible": True,
        },
        {
            "indicateur": 54,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 54,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 54,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 55,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 55,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 55,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "usage",
            "visible": True,
        },
        {
            "indicateur": 56,
            "titre": "Filière de production",
            "type": "pie",
            "categorie": "type_prod_enr",
            "visible": True,
        },
        {
            "indicateur": 57,
            "titre": "Par type d'énergie",
            "type": "pie",
            "categorie": "type_prod_enr",
            "visible": True,
        },
        {
            "indicateur": 58,
            "titre": "Par type d'energie",
            "type": "pie",
            "categorie": "type_prod_enr",
            "visible": True,
        },
        {
            "indicateur": 58,
            "titre": "Par code de tension",
            "type": "pie",
            "categorie": "code_tension",
            "visible": True,
        },
        {
            "indicateur": 59,
            "titre": "Par type de logement",
            "type": "pie",
            "categorie": "logement",
            "visible": True,
        },
        {
            "indicateur": 59,
            "titre": "Par type de residence",
            "type": "pie",
            "categorie": "residence",
            "visible": True,
        },
        {
            "indicateur": 59,
            "titre": "Par type de combustible",
            "type": "pie",
            "categorie": "combustible",
            "visible": True,
        },
    ]
    op.bulk_insert(table, data)


def nouvelle_aquitaine(table):
    """Insertion de données dans la table pour la région Nouvelle-Aquitaine"""
    data = [
        {
            "indicateur": 62,
            "titre": "Par énergie",
            "type": "pie",
            "categorie": "energie",
            "visible": True,
        },
        {
            "indicateur": 63,
            "titre": "Par secteurs",
            "type": "pie",
            "categorie": "secteur",
            "visible": True,
        },
        {
            "indicateur": 64,
            "titre": "Par usage",
            "type": "pie",
            "categorie": "production_enr_usage",
            "visible": True,
        },
        {
            "indicateur": 64,
            "titre": "Par filière",
            "type": "pie",
            "categorie": "production_enr_filiere",
            "visible": True,
        },
        {
            "indicateur": 65,
            "titre": "Par gisement regroupé",
            "type": "pie",
            "categorie": "gisement_methanisable_regroupe",
            "visible": True,
        },
        {
            "indicateur": 65,
            "titre": "Par gisement détaillé",
            "type": "pie",
            "categorie": "gisement_methanisable_detaille",
            "visible": True,
        },
        {
            "indicateur": 67,
            "titre": "Par période de construction",
            "type": "pie",
            "categorie": "logement_periode_construction",
            "visible": True,
        },
        {
            "indicateur": 67,
            "titre": "Par taille du logement",
            "type": "pie",
            "categorie": "logement_nombre_pieces",
            "visible": True,
        },
        {
            "indicateur": 67,
            "titre": "Par statut d'occupation",
            "type": "pie",
            "categorie": "logement_statut_occupation",
            "visible": True,
        },
        {
            "indicateur": 68,
            "titre": "Par tranche d'âge",
            "type": "pie",
            "categorie": "menage_tranche_age",
            "visible": True,
        },
        {
            "indicateur": 68,
            "titre": "Par nombre de personnes",
            "type": "pie",
            "categorie": "menage_nombre_personne",
            "visible": True,
        },
        {
            "indicateur": 69,
            "titre": "Par période de construction",
            "type": "pie",
            "categorie": "logement_vacant_periode_construction",
            "visible": True,
        },
        {
            "indicateur": 69,
            "titre": "Par taille du logement",
            "type": "pie",
            "categorie": "logement_vacant_nombre_pieces",
            "visible": True,
        },
        {
            "indicateur": 69,
            "titre": "Par type de logement",
            "type": "pie",
            "categorie": "logement_vacant_type",
            "visible": True,
        },
    ]
    op.bulk_insert(table, data)


def upgrade():
    indicateur_table = ".".join([SCHEMA, "indicateur"])
    chart_table = op.create_table(
        TABLENAME,
        Column("indicateur", Integer, ForeignKey(indicateur_table + ".id")),
        Column("titre", Text, nullable=False, comment="Titre du graphique"),
        Column(
            "type", String, nullable=False, comment="Type de graphe: pie, hbar, ..."
        ),
        Column("categorie", String, nullable=False, comment="Réference à la catégorie"),
        Column(
            "visible", Boolean, nullable=False, comment="Visible dans l'interface ?"
        ),
        PrimaryKeyConstraint("indicateur", "categorie"),
        comment="Table de description des graphes",
        schema=SCHEMA,
    )
    auvergne_rhone_alpes(chart_table)
    occitanie(chart_table)
    nouvelle_aquitaine(chart_table)


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
