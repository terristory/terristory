"""empty message

Revision ID: 7573260f09cc
Revises: e68a77774410, f7fb3994aadd
Create Date: 2023-10-06 15:42:18.183706

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7573260f09cc"
down_revision = ("e68a77774410", "f7fb3994aadd")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
