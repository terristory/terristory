"""Rename all fields containing action_number

Revision ID: c6f291accb51
Revises: e6d0382f38ad
Create Date: 2024-02-28 17:15:50.209475

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c6f291accb51"
down_revision = "e6d0382f38ad"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        ALTER TABLE strategie_territoire.action RENAME COLUMN action_numero TO action_number;
        ALTER TABLE strategie_territoire.action_errors RENAME COLUMN action_numero TO action_number;
        ALTER TABLE strategie_territoire.action_subactions_status RENAME COLUMN action_numero TO action_number;
        ALTER TABLE strategie_territoire.action_status RENAME COLUMN action_numero TO action_number;
        ALTER TABLE strategie_territoire.action_subactions RENAME COLUMN action TO action_number;
        """
    )


def downgrade():
    op.execute(
        """
        ALTER TABLE strategie_territoire.action RENAME COLUMN action_number TO action_numero;
        ALTER TABLE strategie_territoire.action_errors RENAME COLUMN action_number TO action_numero;
        ALTER TABLE strategie_territoire.action_subactions_status RENAME COLUMN action_number TO action_numero;
        ALTER TABLE strategie_territoire.action_status RENAME COLUMN action_number TO action_numero;
        ALTER TABLE strategie_territoire.action_subactions RENAME COLUMN action_number TO action;
        """
    )
