"""Add more metadata to Analysis page configuration table to avoid depending on indicator definition

Revision ID: 260c56ab8aac
Revises: aa1a8897eef5
Create Date: 2023-06-28 16:01:39.519493

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "260c56ab8aac"
down_revision = "aa1a8897eef5"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("analysis_page_parameters", schema="meta") as batch_op:
        batch_op.add_column(
            sa.Column("decimals", sa.Integer, server_default="0", nullable=True),
        )
        batch_op.add_column(
            sa.Column("unit", sa.String, server_default=str(""), nullable=True),
        )


def downgrade():
    with op.batch_alter_table("analysis_page_parameters", schema="meta") as batch_op:
        batch_op.drop_column("decimals")
        batch_op.drop_column("unit")
