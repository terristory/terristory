"""empty message

Revision ID: e46605acc743
Revises: 0deeafe8e564, 2ef834005cb6
Create Date: 2023-11-02 08:57:05.222270

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e46605acc743"
down_revision = ("0deeafe8e564", "2ef834005cb6")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
