﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout table meta actions

Revision ID: 496422e4f169
Revises: 02b6d51471e0
Create Date: 2022-05-18 13:35:07.095583

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "496422e4f169"
down_revision = "02b6d51471e0"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """create table if not exists strategie_territoire.meta_action (
        numero varchar,
        impact_eco boolean,
        avec_params boolean
    )"""
    )
    op.execute(
        """ insert into strategie_territoire.meta_action(numero,impact_eco,avec_params) select distinct(numero),true,true from strategie_territoire.action;
                    update strategie_territoire.meta_action set impact_eco=false where numero in ('18','19');
                    update strategie_territoire.meta_action set avec_params=false where numero in ('14','15') """
    )


def downgrade():
    op.execute("drop table strategie_territoire.meta_action;")
