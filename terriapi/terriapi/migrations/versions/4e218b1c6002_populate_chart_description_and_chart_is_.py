"""Populate chart.description and chart.is_single_select from existing parameters in indicateur_representation_details

Revision ID: 4e218b1c6002
Revises: 9ea093fd8b07
Create Date: 2024-10-10 15:16:40.900404

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4e218b1c6002"
down_revision = "9ea093fd8b07"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """UPDATE meta.chart AS c
    SET description = ird.value
    FROM (
            SELECT *,
                split_part("name", 'additional_details_text_', 2) AS category_name
            FROM meta.indicateur_representation_details ird
            WHERE "name" LIKE 'additional_details_text_%'
        ) AS ird
    WHERE c.indicateur = ird.indicateur_id
    AND c.categorie = ird.category_name;
    """
    )

    op.execute(
        """update meta.chart as c
    set is_single_select = true
    from(
            select *,
                split_part(
                    "name",
                    'additional_details_disable_in_dashboard_',
                    2
                ) as category_name
            from meta.indicateur_representation_details ird
            where "name" like 'additional_details_disable_in_dashboard_%'
        ) as ird
    where c.indicateur = ird.indicateur_id
        and c.categorie = ird.category_name
"""
    )

    op.execute(
        """
    DELETE FROM meta.indicateur_representation_details
    WHERE "name" LIKE 'additional_details_text_%'
       OR "name" LIKE 'additional_details_disable_in_dashboard_%';
    """
    )


def downgrade():

    op.execute(
        """INSERT INTO meta.indicateur_representation_details (indicateur_id, name, value)
        SELECT c.indicateur,
        CONCAT('additional_details_text_', c.categorie) AS name,
        c.description
        FROM meta.chart AS c
        WHERE c.description IS NOT NULL
        AND c.description != '' ON CONFLICT (indicateur_id, name) DO NOTHING;
        """
    )

    op.execute(
        """INSERT INTO meta.indicateur_representation_details (indicateur_id, name, value)
    SELECT c.indicateur,
    CONCAT('additional_details_disable_in_dashboard_', c.categorie) AS name,
    'true'
    FROM meta.chart AS c
    WHERE c.is_single_select"""
    )
