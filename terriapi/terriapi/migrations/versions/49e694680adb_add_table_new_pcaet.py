"""Add table new_pcaet

Revision ID: 49e694680adb
Revises: 290234b805c6
Create Date: 2023-10-02 10:55:30.160129

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "49e694680adb"
down_revision = "290234b805c6"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE IF NOT EXISTS new_pcaet (
            siren VARCHAR,
            trajectory VARCHAR,
            category INT,
            year INT,
            value DOUBLE PRECISION,
            is_ref BOOL,
            variation DOUBLE PRECISION
        )
        """
    )


def downgrade():
    op.execute("DROP TABLE new_pcaet")
