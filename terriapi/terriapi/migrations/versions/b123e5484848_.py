"""empty message

Revision ID: b123e5484848
Revises: 7754d8f3576f, 281d2b6026bc
Create Date: 2023-12-13 12:50:18.222938

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "b123e5484848"
down_revision = ("7754d8f3576f", "281d2b6026bc")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
