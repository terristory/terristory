﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""correspondance table date perimetre

Revision ID: 82edda289958
Revises: 0b39ed7d1e26
Create Date: 2022-02-08 07:18:51.072166

"""
from time import gmtime, strftime

from alembic import op
from sqlalchemy import Column, Integer, PrimaryKeyConstraint, String

# revision identifiers, used by Alembic.
revision = "82edda289958"
down_revision = "0b39ed7d1e26"
branch_labels = None
depends_on = None

schemas = [
    "auvergne_rhone_alpes",
    "occitanie",
    "nouvelle_aquitaine",
    "bretagne",
    "paysdelaloire",
    "corse",
]


def upgrade():
    req = """
          create table meta.perimetre_geographique as (
          with tmp as (
          select distinct (regexp_split_to_array(data, E'[./,::*]')) as nom_table, region, 2020 as date_perimetre
          from meta.indicateur)
          select distinct a.nom_table, a.region, a.date_perimetre
          from (
              select distinct nom_table[array_position(nom_table, 'maille') + 1] as nom_table, region, tmp.date_perimetre
              from tmp
              where nom_table[array_position(nom_table, 'maille') + 1] is not null
              union all
              select distinct nom_table[array_position(nom_table, 'NULLIF(maille') + 1]  as nom_table, region, tmp.date_perimetre
              from tmp
              where nom_table[array_position(nom_table, 'maille') + 1] is not null
              union all
              select distinct (regexp_split_to_array(data, E'[\*+/\]'))[1] as nom_table, region, 2020 as date_perimetre
              from meta.indicateur
              where (regexp_split_to_array(data, E'[\*+/\]'))[1] is not null
              ) a where a.nom_table is not null
          );
          """
    ajouter_contrainte_cle_primaire = """
                                      alter table meta.perimetre_geographique
                                      add constraint pk_perimetre_geographique
                                      primary key (nom_table, region)
                                      """
    for schema in schemas:
        req_creation_etat = """
                            create table {schema}.etat_conversion_perimetre (conversion_reussie boolean);
                            """.format(
            schema=schema
        )
        req_insertion = """
                        insert into {schema}.etat_conversion_perimetre values(true)
                        """.format(
            schema=schema
        )
        op.execute(req_creation_etat)
        op.execute(req_insertion)
    op.execute(req)
    op.execute(ajouter_contrainte_cle_primaire)
    op.execute("create schema table_passage;")
    for date in list(range(2008, int(strftime("%Y", gmtime())))):
        req = """
              create table table_passage.passage_{date_depart}_{date_arrivee}
              (cod{date_depart} varchar, cod{date_arrivee} varchar, annee varchar, typemodif varchar, ratio varchar);
              """.format(
            date_depart=date, date_arrivee=date + 1
        )
        op.execute(req)


def downgrade():
    op.execute("drop table meta.perimetre_geographique cascade;")
    op.execute("drop schema table_passage cascade;")
    for schema in schemas:
        op.execute(
            "drop table {schema}.etat_conversion_perimetre cascade ;".format(
                schema=schema
            )
        )
