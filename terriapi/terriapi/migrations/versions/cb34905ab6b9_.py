"""empty message

Revision ID: cb34905ab6b9
Revises: 4836156238ff, 03b6802f3f7b
Create Date: 2023-06-22 10:54:25.486660

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "cb34905ab6b9"
down_revision = ("4836156238ff", "03b6802f3f7b")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
