"""Allow to disable past inputs in strategy

Revision ID: 82ba6d55b07f
Revises: bb4b3a8d628a
Create Date: 2024-01-23 12:05:03.641383

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "82ba6d55b07f"
down_revision = "bb4b3a8d628a"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column(
            "enable_past_inputs_for_strategy", sa.Boolean, server_default=str("t")
        ),
        schema="public",
    )


def downgrade():
    with op.batch_alter_table("regions_configuration", schema="public") as batch_op:
        batch_op.drop_column("enable_past_inputs_for_strategy")
