"""empty message

Revision ID: f43044ecf4b1
Revises: 95cf3b2c3777, c6557411a5d5
Create Date: 2024-02-20 18:37:18.005307

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f43044ecf4b1"
down_revision = ("95cf3b2c3777", "c6557411a5d5")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
