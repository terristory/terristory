"""Make relative_pcaet a list instead of a bool

Revision ID: a0301e6a515b
Revises: 5ce3f33c77d1
Create Date: 2025-02-26 16:37:32.012219

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a0301e6a515b"
down_revision = "5ce3f33c77d1"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        ALTER TABLE regions_configuration ALTER COLUMN relative_pcaet TYPE VARCHAR[] USING (
            CASE
                WHEN relative_pcaet THEN '{consommation_ener,enr_production,emission_ges}'::VARCHAR[]
                ELSE '{}'::VARCHAR[]
            END
        )
        """
    )


def downgrade():
    op.execute(
        """
        ALTER TABLE regions_configuration ALTER COLUMN relative_pcaet TYPE BOOL USING (
            CASE WHEN relative_pcaet = '{}' THEN FALSE ELSE TRUE END
        )
        """
    )
