"""add simulator id passage table

Revision ID: f09dec164701
Revises: 05dd969dfb83
Create Date: 2023-05-30 14:36:29.648297

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f09dec164701"
down_revision = "05dd969dfb83"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """create table if not exists didactic_file.passage_table_id (
            id integer,
            category varchar,
            type varchar,
            region varchar,
            PRIMARY KEY(category,type,region)
    )"""
    )


def downgrade():
    op.drop_table("passage_table_id", schema="didactic_file")
