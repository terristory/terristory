"""Remove enable_new_strategy_module

Revision ID: e6d0382f38ad
Revises: 4eb4750e9709
Create Date: 2024-02-21 16:57:59.254647

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e6d0382f38ad"
down_revision = "4eb4750e9709"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("regions_configuration") as batch_op:
        batch_op.drop_column("enable_new_strategy_module")


def downgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("enable_new_strategy_module", sa.Boolean, server_default="false"),
    )
