﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout-fonction-tri-chaine

Revision ID: ddce486c86ce
Revises: 23361f3c38c9
Create Date: 2021-09-17 17:39:18.853184

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "ddce486c86ce"
down_revision = "23361f3c38c9"
branch_labels = None
depends_on = None


def upgrade():
    req = """
          CREATE OR REPLACE FUNCTION tri_chaine_caracteres(a text) RETURNS text AS $$
            declare t1 text;
            BEGIN
              select(array_to_string (
                array(
                  select * from unnest(string_to_array(a, '||')) order by 1), '||')) into t1;
              RETURN t1;
            END;
          $$ LANGUAGE plpgsql;
          """
    op.execute(req)


def downgrade():
    op.execute("drop function tri_chaine_caracteres")
