"""Add relative_pcaet to regions configuration

Revision ID: 04d2c5e679f0
Revises: 992411345876
Create Date: 2023-09-07 14:09:50.366418

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "04d2c5e679f0"
down_revision = "992411345876"
branch_labels = None
depends_on = None

SCHEMA = "public"


def upgrade():
    op.add_column(
        "regions_configuration", sa.Column("relative_pcaet", sa.Boolean), schema=SCHEMA
    )


def downgrade():
    with op.batch_alter_table("regions_configuration", schema=SCHEMA) as batch_op:
        batch_op.drop_column("relative_pcaet")
