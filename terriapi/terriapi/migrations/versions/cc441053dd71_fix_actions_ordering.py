"""Fix actions ordering

Revision ID: cc441053dd71
Revises: e1356f351c31
Create Date: 2024-10-04 11:34:59.457980

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "cc441053dd71"
down_revision = "e1356f351c31"
branch_labels = None
depends_on = None

categories = [
    {"old": "Actions Bâtiment", "new": "Bâtiment"},
    {"old": "Actions Mobilité Durable", "new": "Mobilité durable"},
    {"old": "Industrie", "new": "Industrie"},
    {"old": "Agriculture", "new": "Agriculture"},
    {"old": "Actions Production renouvelable", "new": "Production renouvelable"},
]
actions_order = {
    # mobility
    "20": 1,
    "18": 2,
    "19": 4,
    # industry
    "21": 2,
    # agriculture
    "15": 1,
    "22": 2,
}


def upgrade():
    sql = """UPDATE strategie_territoire.action
        SET categorie='{new}', category_order={order}
        WHERE categorie='{old}'"""
    for i, category in enumerate(categories):
        op.execute(sql.format(order=i + 1, **category))

    sql = """UPDATE strategie_territoire.action
        SET action_order={order}
        WHERE action_number='{number}'"""
    for number, order in actions_order.items():
        op.execute(sql.format(number=number, order=order))


def downgrade():
    sql = """UPDATE strategie_territoire.action
        SET categorie='{old}'
        WHERE categorie='{new}'"""
    for category in categories:
        op.execute(sql.format(**category))
