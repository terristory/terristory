﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add source and data producer for analyses

Revision ID: 6601cbb2f1d9
Revises: b753b71ab6fa
Create Date: 2022-10-19 13:37:55.139154

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6601cbb2f1d9"
down_revision = "b753b71ab6fa"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("indicateur", sa.Column("analysis_producer", sa.JSON), schema="meta")
    op.add_column("indicateur", sa.Column("analysis_source", sa.JSON), schema="meta")


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("analysis_producer")
        batch_op.drop_column("analysis_source")
