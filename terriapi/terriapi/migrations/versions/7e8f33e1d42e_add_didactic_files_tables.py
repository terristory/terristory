"""add didactic files tables

Revision ID: 7e8f33e1d42e
Revises: a272014697e5
Create Date: 2023-02-06 12:22:19.843682

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7e8f33e1d42e"
down_revision = "a272014697e5"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ 
        CREATE TABLE meta.didactic_file (
            id serial, 
            title varchar, 
            type json,
            description varchar,
            region varchar,
            PRIMARY KEY(id)
        );

        CREATE TABLE meta.didactic_file_category (
            id serial, 
            title varchar,
            didactic_file_id int,
            category_order int,
            PRIMARY KEY(id),
            CONSTRAINT fk_didactic_file FOREIGN KEY(didactic_file_id) REFERENCES meta.didactic_file(id)
            ON DELETE CASCADE
        );


        CREATE TABLE meta.didactic_file_sub_category (
            id serial,
            title varchar, 
            type varchar,
            analysis_list json,
            category_id int,
            subcategory_order int,
            PRIMARY KEY(id),
            CONSTRAINT fk_didactic_file_category FOREIGN KEY(category_id) REFERENCES meta.didactic_file_category(id)
            ON DELETE CASCADE
        );

        CREATE TABLE meta.didactic_file_types (
            type_id serial,
            value varchar,
            label varchar,
            type_order int,
            region varchar,
            PRIMARY KEY(type_id)
        );
        """
    )


def downgrade():
    tables_name = [
        "didactic_file_sub_category",
        "didactic_file_category",
        "didactic_file",
        "didactic_file_types",
    ]
    for table in tables_name:
        op.drop_table(table, schema="meta")
