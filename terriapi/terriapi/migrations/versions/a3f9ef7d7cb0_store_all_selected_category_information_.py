"""Store all selected category information in meta.charts

Revision ID: a3f9ef7d7cb0
Revises: e1356f351c31
Create Date: 2024-10-04 16:11:20.241055

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a3f9ef7d7cb0"
down_revision = "e1356f351c31"
branch_labels = None
depends_on = None


def upgrade():

    op.add_column(
        "chart",
        sa.Column(
            "description",
            sa.String,
            nullable=True,
            comment="This description is displayed on hover",
        ),
        schema="meta",
    )

    # Add a is_single_select column
    op.add_column(
        "chart",
        sa.Column(
            "is_single_select",
            sa.Boolean(),
            nullable=False,
            server_default=sa.false(),
            comment="If true, then only one option of this category should be allowed to be selected at once",
        ),
        schema="meta",
    )

    # Remove the server_default
    op.alter_column("chart", "is_single_select", server_default=None, schema="meta")


def downgrade():
    op.drop_column("chart", "is_single_select", schema="meta")
    op.drop_column("chart", "description", schema="meta")
