"""empty message

Revision ID: 08c7bd987296
Revises: 5a4c3d4f5035, c6f291accb51
Create Date: 2024-03-01 17:07:21.325661

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "08c7bd987296"
down_revision = ("5a4c3d4f5035", "c6f291accb51")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
