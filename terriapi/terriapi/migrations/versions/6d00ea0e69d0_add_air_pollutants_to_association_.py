﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""add air pollutants to association_graphique_table

Revision ID: 6d00ea0e69d0
Revises: a6c552c3d1a5
Create Date: 2022-09-23 14:49:12.014469

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6d00ea0e69d0"
down_revision = "a6c552c3d1a5"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        insert into meta.association_graphique_table values ('Émissions de COVNM','air_polluant_covnm','auvergne-rhone-alpes');
        insert into meta.association_graphique_table values ('Émissions de NOx','air_polluant_nox','auvergne-rhone-alpes');
        insert into meta.association_graphique_table values ('Émissions de SO2','air_polluant_sox','auvergne-rhone-alpes');
        insert into meta.association_graphique_table values ('Émissions de PM2.5','air_polluant_pm25','auvergne-rhone-alpes');
        insert into meta.association_graphique_table values ('Émissions de PM10','air_polluant_pm10','auvergne-rhone-alpes');
        insert into meta.association_graphique_table values ('Émissions de NH3','air_polluant_nh3','auvergne-rhone-alpes');"""
    )


def downgrade():
    op.execute(
        """delete from meta.association_graphique_table where "table" like 'air_polluant%' and region = 'auvergne-rhone-alpes';"""
    )
