"""add simulator tables

Revision ID: 180d7edc1ac8
Revises: 375e3c6b2cf7
Create Date: 2023-03-31 16:30:52.747871

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "180d7edc1ac8"
down_revision = "375e3c6b2cf7"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ 
        CREATE SCHEMA IF NOT EXISTS didactic_file;

        CREATE TABLE IF NOT EXISTS didactic_file.category_lever(
            id SERIAL primary key,
            title varchar,
            thematic varchar,
            category_order int);
        
        CREATE TABLE IF NOT EXISTS didactic_file.lever(
            id SERIAL primary key,
            title varchar,
            regions varchar[],
            lever_order int,
            category_lever_id int,
            CONSTRAINT fk_category_lever FOREIGN KEY(category_lever_id) REFERENCES didactic_file.category_lever(id)
            ON DELETE CASCADE);
        
        CREATE TABLE IF NOT EXISTS didactic_file.lever_action(
            id SERIAL primary key,
            number varchar,
            title varchar,
            lever_id int,
            comment varchar,
            CONSTRAINT fk_lever_id FOREIGN KEY(lever_id) REFERENCES didactic_file.lever(id)
            ON DELETE CASCADE);
        
        CREATE TABLE IF NOT EXISTS didactic_file.simulator_impact(
            type varchar primary key,
            title varchar,
            unit varchar,
            thematic varchar,
            impact_order int,
            regions varchar[],
            color varchar);

        CREATE TABLE IF NOT EXISTS didactic_file.simulator_level(
            id SERIAL primary key,
            region varchar,
            level int);
        
        CREATE TABLE IF NOT EXISTS didactic_file.passage_table(
            key varchar NOT NULL,
            region varchar NOT NULL,
            match varchar NOT NULL,
            PRIMARY KEY(region, key)
        );

        CREATE TABLE IF NOT EXISTS didactic_file.generic_input_param(
            id SERIAL primary key, 
            data varchar,
            label varchar,
            value float,
            unit varchar
        );

        CREATE TABLE IF NOT EXISTS didactic_file.generic_interm_param(
            id SERIAL primary key, 
            data varchar,
            category varchar,
            engine varchar,
            value float,
            unit varchar
        
        )

        """
    )


def downgrade():
    tables_name = [
        "lever_action",
        "lever",
        "category_lever",
        "simulator_impact",
        "simulator_level",
        "passage_table",
        "generic_input_param",
        "generic_interm_param",
    ]
    for table in tables_name:
        op.drop_table(table, schema="didactic_file")

    op.execute("DROP SCHEMA IF EXISTS didactic_file")
