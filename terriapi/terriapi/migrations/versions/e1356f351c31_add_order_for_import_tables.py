"""Add order for import tables

Revision ID: e1356f351c31
Revises: dea23109c76c
Create Date: 2024-09-11 09:22:10.991714

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e1356f351c31"
down_revision = "dea23109c76c"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("national_import_metadata", schema="meta") as batch_op:
        batch_op.add_column(sa.Column("table_order", sa.Integer, server_default="0"))
        batch_op.add_column(sa.Column("requirements", sa.JSON, server_default="{}"))
        batch_op.add_column(sa.Column("optional", sa.JSON, server_default="{}"))


def downgrade():
    with op.batch_alter_table("national_import_metadata", schema="meta") as batch_op:
        batch_op.drop_column("requirements")
        batch_op.drop_column("optional")
        batch_op.drop_column("table_order")
