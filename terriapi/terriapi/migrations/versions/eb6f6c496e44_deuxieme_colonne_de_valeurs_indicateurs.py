﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""deuxieme colonne de valeurs indicateurs

Revision ID: eb6f6c496e44
Revises: 2eeda6c68656
Create Date: 2021-02-19 09:41:10.808389

"""
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "eb6f6c496e44"
down_revision = "2eeda6c68656"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "indicateur", Column("data_deuxieme_representation", String()), schema="meta"
    )

    op.add_column("indicateur", Column("valeur_filtre_defaut", String()), schema="meta")

    op.add_column("indicateur", Column("titre_legende", String()), schema="meta")

    op.add_column(
        "indicateur",
        Column("titre_legende_deuxieme_representation", String()),
        schema="meta",
    )


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("data_deuxieme_representation")

    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("valeur_filtre_defaut")

    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("titre_legende")

    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("titre_legende_deuxieme_representation")
