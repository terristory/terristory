﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Ajout de colonnes aux metadonnees sankey

Revision ID: aed983949a0c
Revises: 60dd818543c2
Create Date: 2022-09-14 11:57:28.475185

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "aed983949a0c"
down_revision = "60dd818543c2"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.add_column(
            sa.Column("texte_introduction", sa.Text)
        )  # , comment="Texte introductif"))
        batch_op.add_column(
            sa.Column("unite", sa.String)
        )  # , comment="Unité énergétique de la table"))
        batch_op.add_column(
            sa.Column("source", sa.Text)
        )  # , comment="Source des données"))
        batch_op.add_column(
            sa.Column("copyright", sa.Boolean)
        )  # , comment="Copyright TerriFlux"))


def downgrade():
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.drop_column("texte_introduction")
        batch_op.drop_column("unite")
        batch_op.drop_column("source")
        batch_op.drop_column("copyright")
