"""Update confidentiality table to have a flexible structure

Revision ID: e6dc37f046b0
Revises: 260c56ab8aac
Create Date: 2023-06-28 17:12:41.606009

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e6dc37f046b0"
down_revision = "260c56ab8aac"
branch_labels = None
depends_on = None

regions = [
    "auvergne-rhone-alpes",
    "occitanie",
    "nouvelle-aquitaine",
    "bretagne",
    "paysdelaloire",
    "france",
]


def upgrade():
    for region in regions:
        schema = region.replace("-", "_")
        with op.batch_alter_table("confid_maille", schema=schema) as batch_op:
            batch_op.add_column(
                sa.Column(
                    "secteur_column",
                    sa.String(),
                    nullable=False,
                    server_default="secteur",
                )
            )
            batch_op.add_column(
                sa.Column(
                    "energie_column",
                    sa.String(),
                    nullable=False,
                    server_default="energie",
                )
            )
            batch_op.drop_column("facture_secteur")
            batch_op.drop_column("facture_energie")


def downgrade():
    for region in regions:
        schema = region.replace("-", "_")
        with op.batch_alter_table("confid_maille", schema=schema) as batch_op:
            batch_op.drop_column("secteur_column")
            batch_op.drop_column("energie_column")
            batch_op.add_column(sa.Column("facture_secteur", sa.String()))
            batch_op.add_column(sa.Column("facture_energie", sa.String()))
        op.execute(
            f"UPDATE {schema}.confid_maille SET facture_secteur = secteur, facture_energie = energie"
        )
