"""Add confidentiality passage table

Revision ID: 63b21b5c954e
Revises: 23bb216f418a
Create Date: 2023-04-21 12:10:14.729909

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "63b21b5c954e"
down_revision = "23bb216f418a"
branch_labels = None
depends_on = None

POLLUTANTS = ["nox", "covnm", "pm10", "pm25", "nh3", "sox"]
CONFID_RELATED_TABLES = ["CONSUMPTION", "EMISSIONS"] + [
    "POLLUTANT_" + p.upper() for p in POLLUTANTS
]
TABLES = ["conso_energetique", "emission_ges"] + [
    "air_polluant_" + p for p in POLLUTANTS
]


def upgrade():
    op.create_table(
        "passage_table_confidentiality",
        sa.Column(
            "region", sa.String, primary_key=True, comment="Region", nullable=False
        ),
        sa.Column("key", sa.String, primary_key=True, comment="key", nullable=False),
        sa.Column(
            "is_confidential", sa.Boolean, comment="Is confidential", nullable=False
        ),
        schema="strategie_territoire",
    )

    op.execute(
        """
        INSERT INTO strategie_territoire.passage_table_confidentiality
        VALUES
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION', false),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd', false);
    """
    )

    for key, table in zip(CONFID_RELATED_TABLES, TABLES):
        op.execute(
            f"""INSERT INTO strategie_territoire.passage_table_confidentiality VALUES
            ('auvergne-rhone-alpes', 'DataSet.{key}', true),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Sector', true),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Commodity', true),
            ('auvergne-rhone-alpes', 'DataSet.{key}/Usage', false);"""
        )


def downgrade():
    op.drop_table("passage_table_confidentiality", schema="strategie_territoire")
