"""Add unit conversion table

Revision ID: c5661c307965
Revises: 6fbf1bf3906b
Create Date: 2023-09-28 17:56:53.288552

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c5661c307965"
down_revision = "6fbf1bf3906b"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "data_units",
        sa.Column("unit_id", sa.Integer, primary_key=True, comment="ID"),
        sa.Column(
            "region",
            sa.String,
            comment="Region",
        ),
        sa.Column("unit_type", sa.String, comment="Type of unit"),
        sa.Column("unit_name", sa.String, comment="Name"),
        sa.Column(
            "conversion_factor",
            sa.Float,
            comment="If reference unit => 1, else something else",
        ),
        schema="meta",
    )
    op.create_table(
        "data_units_indicators_association",
        sa.Column(
            "indicator_id",
            sa.Integer,
            sa.ForeignKey("meta.indicateur.id", ondelete="CASCADE"),
            comment="Indicator ID",
        ),
        sa.Column(
            "unit_id",
            sa.Integer,
            sa.ForeignKey("meta.data_units.unit_id", ondelete="CASCADE"),
            comment="Unit ID",
        ),
        sa.Column(
            "is_default_for_zone_type",
            sa.String,
            comment="Is default for specified zone types separated by comma",
        ),
        sa.Column(
            "is_hidden_for_zone_type",
            sa.String,
            comment="Is hidden for specified zone types separated by comma",
        ),
        schema="meta",
    )
    op.create_table(
        "data_units_other_modules_association",
        sa.Column(
            "unit_id",
            sa.Integer,
            sa.ForeignKey("meta.data_units.unit_id", ondelete="CASCADE"),
            comment="Unit ID",
        ),
        sa.Column(
            "module_key",
            sa.String,
            comment="Module key",
        ),
        sa.Column(
            "other_key",
            sa.String,
            comment="Other key useful to handle units",
        ),
        sa.Column(
            "region",
            sa.String,
            comment="Region",
        ),
        sa.Column(
            "is_default_for_zone_type",
            sa.String,
            comment="Is default for specified zone types separated by comma",
        ),
        schema="meta",
    )


def downgrade():
    op.execute("""drop table if exists meta.data_units_other_modules_association""")
    op.execute("""drop table if exists meta.data_units_indicators_association""")
    op.execute("""drop table if exists meta.data_units""")
