"""empty message

Revision ID: 54dff651d20c
Revises: f09dec164701, d89bf73c4382
Create Date: 2023-05-31 17:30:30.713906

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "54dff651d20c"
down_revision = ("f09dec164701", "d89bf73c4382")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
