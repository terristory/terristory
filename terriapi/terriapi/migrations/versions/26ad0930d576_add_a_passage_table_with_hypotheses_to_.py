"""Add a passage table with hypotheses to estimate usages

Revision ID: 26ad0930d576
Revises: 2f64a90952a7
Create Date: 2024-08-26 11:56:33.394644

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "26ad0930d576"
down_revision = "2f64a90952a7"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "national_hypotheses",
        sa.Column("region", sa.String, primary_key=True, comment="Nom de la région"),
        sa.Column("table_name", sa.String, primary_key=True, comment="Table concernée"),
        sa.Column(
            "column_name", sa.String, primary_key=True, comment="Colonne des valeurs"
        ),
        sa.Column("key", sa.String, primary_key=True, comment="Valeur associée"),
        sa.Column("filters", sa.JSON, comment="Conditions"),
        sa.Column("value", sa.Numeric, comment="Texte à afficher"),
        schema="strategie_territoire",
    )


def downgrade():
    op.drop_table("national_hypotheses", schema="strategie_territoire")
