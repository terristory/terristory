"""empty message

Revision ID: 5476cd9be7d9
Revises: ec0705148ef6, 9aa039f40c82
Create Date: 2023-03-29 13:32:33.939267

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5476cd9be7d9"
down_revision = ("ec0705148ef6", "9aa039f40c82")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
