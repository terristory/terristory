﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""tables-analyses-territoriales

Revision ID: 4d5d82ae8392
Revises: dfa9ebf28687
Create Date: 2021-02-10 15:43:24.196511

"""
from alembic import op
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "4d5d82ae8392"
down_revision = "dfa9ebf28687"
branch_labels = None
depends_on = None


def upgrade():

    op.add_column(
        "ip_localisation",
        Column("region_selectionnee", String()),
        schema="consultations",
    )

    op.add_column(
        "consultations_indicateurs",
        Column("code_territoire", String()),
        schema="consultations",
    )

    op.add_column(
        "consultations_indicateurs",
        Column("type_territoire", String()),
        schema="consultations",
    )

    op.add_column(
        "consultations_indicateurs",
        Column(
            "id_utilisateur",
            Integer,
            ForeignKey("consultations.ip_localisation.id", ondelete="CASCADE"),
        ),
        schema="consultations",
    )

    op.add_column(
        "actions_cochees", Column("code_territoire", String()), schema="consultations"
    )

    op.add_column(
        "actions_cochees", Column("type_territoire", String()), schema="consultations"
    )

    op.add_column(
        "actions_cochees",
        Column(
            "id_utilisateur",
            Integer,
            ForeignKey("consultations.ip_localisation.id", ondelete="CASCADE"),
        ),
        schema="consultations",
    )

    with op.batch_alter_table(
        "consultations_indicateurs", schema="consultations"
    ) as batch_op:
        batch_op.drop_column("ip")

    with op.batch_alter_table("actions_cochees", schema="consultations") as batch_op:
        batch_op.drop_column("ip")

    op.create_table(
        "analyses_territoriales",
        Column(
            "id", Integer, primary_key=True, comment="Identifiant"
        ),  # crée un identifiant incrémental
        Column(
            "id_utilisateur",
            Integer,
            ForeignKey("consultations.ip_localisation.id", ondelete="CASCADE"),
        ),
        Column("region", String, nullable=False, comment="Nom de la région"),
        Column("code_territoire", String, comment="Code INSEE du territoire"),
        Column(
            "type_territoire",
            String,
            comment="Type de territoire sélectionné (Région, EPCI etc.)",
        ),
        Column(
            "page",
            String,
            comment="Nom de la page à laquelle l'utilisateur.rice accède (suivi des indicateurs territoriaux ou suivi trajectoires)",
        ),
        Column(
            "date",
            DateTime,
            server_default="now",
            nullable=False,
            comment="Date de l'activité",
        ),
        schema="consultations",
    )


def downgrade():
    with op.batch_alter_table(
        "consultations_indicateurs", schema="consultations"
    ) as batch_op:
        batch_op.drop_column("code_territoire")

    with op.batch_alter_table(
        "consultations_indicateurs", schema="consultations"
    ) as batch_op:
        batch_op.drop_column("type_territoire")

    with op.batch_alter_table(
        "consultations_indicateurs", schema="consultations"
    ) as batch_op:
        batch_op.drop_column("id_utilisateur")

    with op.batch_alter_table("actions_cochees", schema="consultations") as batch_op:
        batch_op.drop_column("code_territoire")

    with op.batch_alter_table("actions_cochees", schema="consultations") as batch_op:
        batch_op.drop_column("type_territoire")

    with op.batch_alter_table("actions_cochees", schema="consultations") as batch_op:
        batch_op.drop_column("id_utilisateur")

    with op.batch_alter_table("ip_localisation", schema="consultations") as batch_op:
        batch_op.drop_column("region_selectionnee")

    op.drop_table(schema="consultations", table_name="analyses_territoriales")
