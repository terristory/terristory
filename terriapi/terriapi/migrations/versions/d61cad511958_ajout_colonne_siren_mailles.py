﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout_colonne_siren_mailles

Revision ID: d61cad511958
Revises: 4d3cefcb2195
Create Date: 2022-01-14 15:52:50.316248

"""
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "d61cad511958"
down_revision = "4d3cefcb2195"
branch_labels = None
depends_on = None
schema = [
    "occitanie",
    "auvergne_rhones_alpes",
    "nouvelle_acquitaine",
    "paysdelaloire",
    "bretagne",
]
dict_schema_table = {
    "occitanie": ["region", "departement", "petr", "scot", "pnr"],
    "auvergne_rhone_alpes": [
        "region",
        "departement",
        "scot",
        "pnr",
        "tepcv",
        "teposcv",
        "crte",
    ],
    "nouvelle_aquitaine": ["region", "departement"],
    "paysdelaloire": ["region", "departement"],
    "bretagne": ["region", "departement", "pays"],
}


def upgrade():
    for sch, list_tbl in dict_schema_table.items():
        for tbl in list_tbl:
            op.add_column(tbl, Column("siren", String), schema=sch)


def downgrade():
    for sch, list_tbl in dict_schema_table.items():
        for tbl in list_tbl:
            with op.batch_alter_table(tbl, schema=sch) as batch_op:
                batch_op.drop_column("siren")
