"""upgrade passage table

Revision ID: f7fb3994aadd
Revises: 49e694680adb
Create Date: 2023-10-04 22:39:27.095134

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f7fb3994aadd"
down_revision = "49e694680adb"
branch_labels = None
depends_on = None

regions = [
    "auvergne-rhone-alpes",
    "occitanie",
    "corse",
    "paysdelaloire",
    "nouvelle-aquitaine",
]

pollutants = ["POLLUTANT_NOX", "POLLUTANT_PM10", "POLLUTANT_PM25", "POLLUTANT_COVNM"]


def upgrade_consumption_ghg():
    op.execute(
        """INSERT INTO strategie_territoire.passage_table 
            VALUES('auvergne-rhone-alpes', 'DataSet.CONSUMPTION/Usage.PASSENGER_TRANSPORT', '12', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.CONSUMPTION/Usage.GOODS_TRANSPORT', '13', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.EMISSIONS/Usage.PASSENGER_TRANSPORT', '12', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.EMISSIONS/Usage.GOODS_TRANSPORT', '13', 'value'),
                ('nouvelle-aquitaine', 'DataSet.CONSUMPTION', 'conso_energetique', 'table'),
                ('nouvelle-aquitaine', 'DataSet.CONSUMPTION/Sector', 'secteur', 'column'),
                ('nouvelle-aquitaine', 'DataSet.CONSUMPTION/Sector.ROAD_TRANSPORT', '5', 'value'),
                ('nouvelle-aquitaine', 'DataSet.EMISSIONS', 'emissions_ges', 'table'),
                ('nouvelle-aquitaine', 'DataSet.EMISSIONS/Sector', 'secteur', 'column'),
                ('nouvelle-aquitaine', 'DataSet.EMISSIONS/Sector.ROAD_TRANSPORT', '5', 'value');
            
        """
    )


def upgrade_pollutants():
    for pollutant in pollutants:
        op.execute(
            f"""INSERT INTO strategie_territoire.passage_table 
                VALUES('auvergne-rhone-alpes', 'DataSet.{pollutant}/Usage.PASSENGER_TRANSPORT', '12', 'value'),
                    ('auvergne-rhone-alpes', 'DataSet.{pollutant}/Usage.GOODS_TRANSPORT', '13', 'value'),
                    ('auvergne-rhone-alpes', 'DataSet.{pollutant}/Usage', 'usage', 'column');
            """
        )


def upgrade_fleet():
    op.execute(
        f"""INSERT INTO strategie_territoire.passage_table 
            VALUES('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET', 'parc_auto_statique', 'table'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleType', 'type_veh', 'column'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleType.PASSENGER_CARS', '1', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleType.TRUCKS', '2', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleType.BUSES_COACHES', '3', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleType.LIGHT_COMM', '4', 'value'),
        
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel', 'energie_carburant', 'column'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.DIESEL', '1', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.GASOLINE', '2', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.GAS', '3', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELEC_H2', '4', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_DIESEL', '5', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_GASOLINE', '6', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.ELECTRIC', '7', 'value'),
            ('nouvelle-aquitaine', 'DataSet.VEHICLE_FLEET/VehicleFuel.UNKNOWN', '8', 'value');
        """
    )


def upgrade_energy_bill():
    op.execute(
        """INSERT INTO strategie_territoire.passage_table 
            VALUES('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL', 'facture_energetique', 'table'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector', 'secteur', 'column'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity', 'energie', 'column'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage', 'usage', 'column'),

                ('occitanie', 'DataSet.ENERGY_BILL', 'facture_energetique', 'table'),
                ('occitanie', 'DataSet.ENERGY_BILL/Sector', 'secteur', 'column'),
                ('occitanie', 'DataSet.ENERGY_BILL/Commodity', 'energie', 'column'),
                ('occitanie', 'DataSet.ENERGY_BILL/Usage', 'usage', 'column'),

                ('paysdelaloire', 'DataSet.ENERGY_BILL', 'depense_energie', 'table'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector', 'secteur', 'column'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Commodity', 'energie', 'column'),

                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL', 'depense_energetique', 'table'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Sector', 'secteur', 'column'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Commodity', 'energie', 'column'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Usage', 'usage', 'column'),

                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.ELECTRICITY', '2', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.NON_ENERGETIC', '4', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.OIL', '5', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.RENEWABLE_HEAT', '3', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.BIOFUELS', '6', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.GAS', '7', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.UNKNOWN', '8', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.URBAN_HEAT_COOL', '9', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.WASTES', '10', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Commodity.COAL', '1', 'value'),

                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.RESIDENTIAL', '1', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.AGRICULTURE', '2', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.SERVICES', '3', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.WASTE_MANAGEMENT', '4', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.INDUSTRY_WITHOUT_ENERGY', '5', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.ROAD_TRANSPORT', '6', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.OTHER_TRANSPORT', '7', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Sector.ENERGY_INDUSTRY', '8', 'value'),

                ('occitanie', 'DataSet.ENERGY_BILL/Sector.RESIDENTIAL', '1', 'value'),
                ('occitanie', 'DataSet.ENERGY_BILL/Sector.AGRICULTURE', '2', 'value'),
                ('occitanie', 'DataSet.ENERGY_BILL/Sector.SERVICES', '3', 'value'),
                ('occitanie', 'DataSet.ENERGY_BILL/Sector.NOT_AFFECTED', '4', 'value'),
                ('occitanie', 'DataSet.ENERGY_BILL/Sector.INDUSTRY', '5', 'value'),
                ('occitanie', 'DataSet.ENERGY_BILL/Sector.ROAD_TRANSPORT', '6', 'value'),
                ('occitanie', 'DataSet.ENERGY_BILL/Sector.ENERGY_PROD', '8', 'value'),

                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.RESIDENTIAL', '1', 'value'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.AGRICULTURE', '2', 'value'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.SERVICES', '3', 'value'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.WASTE_MANAGEMENT', '4', 'value'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.INDUSTRY_WITHOUT_ENERGY', '5', 'value'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.ROAD_TRANSPORT', '6', 'value'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.OTHER_TRANSPORT', '7', 'value'),
                ('paysdelaloire', 'DataSet.ENERGY_BILL/Sector.ENERGY_INDUSTRY', '8', 'value'),

                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Sector.RESIDENTIAL', '1', 'value'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Sector.INDUSTRY', '2', 'value'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Sector.SERVICES', '3', 'value'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Sector.AGRICULTURE', '4', 'value'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Sector.ROAD_TRANSPORT', '5', 'value'),
                ('nouvelle-aquitaine', 'DataSet.ENERGY_BILL/Sector.WASTE_MANAGEMENT', '6', 'value'),

                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.HEATING', '1', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.HOT_WATER', '2', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.COOKING', '3', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.SPECIFIC_ELEC', '4', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.OTHERS', '5', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.COOLING', '11', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.PASSENGER_TRANSPORT', '12', 'value'),
                ('auvergne-rhone-alpes', 'DataSet.ENERGY_BILL/Usage.GOODS_TRANSPORT', '13', 'value');"""
    )


def add_mobility():
    for region in regions:
        op.execute(
            f"""
            INSERT INTO strategie_territoire.passage_table
            VALUES('{region}', 'DataSet.MOBILITY', 'mobilite_insee', 'table'),
                ('{region}', 'DataSet.MOBILITY/Filter', 'valeur_filtre', 'column'),
                ('{region}', 'DataSet.MOBILITY/PPAL_TRANS', 'trans_ppal', 'column'),
                ('{region}', 'DataSet.MOBILITY/PPAL_TRANS.TWO_WHEEL_MOTORIZED', '1', 'value'),
                ('{region}', 'DataSet.MOBILITY/PPAL_TRANS.CAR_TRUCK_VAN', '2', 'value'),
                ('{region}', 'DataSet.MOBILITY/PPAL_TRANS.WALK', '3', 'value'),
                ('{region}', 'DataSet.MOBILITY/PPAL_TRANS.PUBLIC_TRANSPORT', '4', 'value'),
                ('{region}', 'DataSet.MOBILITY/PPAL_TRANS.BIKE', '6', 'value');
            """
        )


def upgrade():
    upgrade_consumption_ghg()
    upgrade_pollutants()
    upgrade_energy_bill()
    upgrade_fleet()
    add_mobility()


def downgrade():
    op.execute(
        f"""
            DELETE FROM strategie_territoire.passage_table
            WHERE key Like 'DataSet.MOBILITY%'
                OR key LIKE 'DataSet.ENERGY_BILL%'
                OR key LIKE '%/Usage.PASSENGER_TRANSPORT'
                OR key LIKE '%/Usage.GOODS_TRANSPORT' ;
        """
    )
