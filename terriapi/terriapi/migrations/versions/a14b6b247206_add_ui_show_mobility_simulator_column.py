"""add ui_show_mobility_simulator column

Revision ID: a14b6b247206
Revises: a17e6162d9ab
Create Date: 2024-10-15 11:34:25.554723

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a14b6b247206"
down_revision = "a17e6162d9ab"
branch_labels = None
depends_on = None


SCHEMA = "public"
TABLENAME = "regions_configuration"


def upgrade():
    op.add_column(
        TABLENAME, sa.Column("ui_show_mobility_simulator", sa.Boolean), schema=SCHEMA
    )
    op.execute(
        """UPDATE regions_configuration 
        SET ui_show_mobility_simulator = true
        WHERE id IN ('auvergne-rhone-alpes', 'occitanie', 'paysdelaloire', 'bretagne')"""
    )
    op.execute(
        """
            UPDATE meta.tableau_thematique 
            SET graphiques = (
                SELECT jsonb_agg(
                    CASE
                        WHEN elem->>'id_analysis' IN ('simulateur', 'simulator') THEN
                            jsonb_set(
                                jsonb_set(
                                    jsonb_set(
                                        jsonb_set(
                                            jsonb_set(
                                                jsonb_set(
                                                    elem,
                                                    '{id_analysis}', '"simulateur"'),
                                                '{keyLink}', '"simulateur"'),
                                            '{icon}', '"simulator_mobility"'),
                                        '{typeLink}', '"mobilité"'),
                                    '{nameTypeLink}', '"type"'),
                                '{nom}', '"Simulateur Mobilité"')
                        ELSE
                            elem
                    END
                )
                FROM jsonb_array_elements(graphiques::jsonb) AS elem
                WHERE elem->>'representation' = 'link-launcher'
            )
            WHERE EXISTS (
                SELECT 1
                FROM jsonb_array_elements(graphiques::jsonb) AS elem
                WHERE elem->>'representation' = 'link-launcher'
            );
        """
    )


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("ui_show_mobility_simulator")
