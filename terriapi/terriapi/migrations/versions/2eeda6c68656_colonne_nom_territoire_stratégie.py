﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""colonne nom territoire stratégie

Revision ID: 2eeda6c68656
Revises: abe908a85a3c
Create Date: 2021-02-16 08:16:10.987152

"""
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "2eeda6c68656"
down_revision = "abe908a85a3c"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "utilisateur_scenario", Column("nom_territoire", String()), schema="public"
    )


def downgrade():
    with op.batch_alter_table("utilisateur_scenario", schema="public") as batch_op:
        batch_op.drop_column("nom_territoire")
