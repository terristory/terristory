﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""tables mesures audiance

Revision ID: dfa9ebf28687
Revises: 1a53c45b698e
Create Date: 2021-02-09 15:30:15.223636

"""
from alembic import op
from sqlalchemy import Column, DateTime, ForeignKey, Integer, String
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = "dfa9ebf28687"
down_revision = "1a53c45b698e"
branch_labels = None
depends_on = None


def creer_schema():
    op.execute("create schema consultations;")


def supprimer_schema():
    op.execute("drop schema consultations cascade;")


def upgrade():
    creer_schema()

    op.create_table(
        "ip_localisation",
        Column(
            "id", Integer, primary_key=True, comment="Identifiant"
        ),  # crée un identifiant incrémental
        Column("ip", String, comment="Adresse IP de l'utilisateur.rice"),
        Column(
            "commune",
            String,
            comment="Nom de la commune depuis laquelle la personne utilise l'application",
        ),
        schema="consultations",
    )

    op.create_table(
        "consultations_indicateurs",
        Column(
            "id", Integer, primary_key=True, comment="Identifiant"
        ),  # crée un identifiant incrémental
        Column(
            "provenance",
            String,
            nullable=False,
            comment="Page depuis laquelle l'url a été lancée",
        ),
        Column("region", String, nullable=False, comment="Nom de la région"),
        Column("id_indicateur", Integer, comment="identifiant de l'indicateur"),
        Column("ip", String, comment="Adresse IP de l'utilisateur.rice"),
        Column(
            "date",
            DateTime,
            server_default="now",
            nullable=False,
            comment="Date de l'activité",
        ),
        schema="consultations",
    )

    op.create_table(
        "actions_cochees",
        Column(
            "id", Integer, primary_key=True, comment="Identifiant"
        ),  # crée un identifiant incrémental
        Column("region", String, nullable=False, comment="Nom de la région"),
        Column(
            "liste_actions",
            postgresql.ARRAY(String()),
            comment="Liste des actions cochées avant le lancement du calcul",
        ),
        Column(
            "liste_trajectoires_cibles",
            postgresql.ARRAY(String()),
            comment="Liste des trajectoires cibles activées avant le lancement du calcul",
        ),
        Column(
            "type_action", String, comment="Lancement d'un calcul ou export ADEME ?"
        ),
        Column("ip", String, comment="Adresse IP de l'utilisateur.rice"),
        Column(
            "date",
            DateTime,
            server_default="now",
            nullable=False,
            comment="Date de l'activité",
        ),
        schema="consultations",
    )


def downgrade():
    supprimer_schema()
