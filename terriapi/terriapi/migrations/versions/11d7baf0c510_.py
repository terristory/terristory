﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""add parameter to enable poi export

Revision ID: 11d7baf0c510
Revises: 4c7c7c2b9e71
Create Date: 2022-11-04 11:35:39.146885

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy import Column
from sqlalchemy.sql.sqltypes import Boolean

# revision identifiers, used by Alembic.
revision = "11d7baf0c510"
down_revision = "4c7c7c2b9e71"
branch_labels = None
depends_on = None

TABLENAME = "layer"


def all_regional_schemas(engine):
    with engine.connect() as con:
        rs = con.execute("SELECT * FROM regions_configuration WHERE env = 'dev'")
        for row in rs:
            schema = row["id"]
            yield schema.replace("-", "_")


def upgrade():
    for schema in all_regional_schemas(op.get_bind()):
        op.add_column(
            TABLENAME, Column("donnees_exportables", Boolean), schema=schema + "_poi"
        )


def downgrade():
    for schema in all_regional_schemas(op.get_bind()):
        with op.batch_alter_table(TABLENAME, schema=schema + "_poi") as batch_op:
            batch_op.drop_column("donnees_exportables")
