"""Add contact mails for OpenEnergyMap

Revision ID: 6c78e92d9209
Revises: a4ccf5bbba32
Create Date: 2023-08-24 16:55:00.224251

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6c78e92d9209"
down_revision = "a4ccf5bbba32"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("poi_contributions_contacts", sa.String()),
        schema="public",
    )
    op.add_column(
        "utilisateur",
        sa.Column(
            "can_validate_poi_contributions", sa.Boolean(), server_default="false"
        ),
        schema="public",
    )


def downgrade():
    with op.batch_alter_table("regions_configuration", schema="public") as batch_op:
        batch_op.drop_column("poi_contributions_contacts")
    with op.batch_alter_table("utilisateur", schema="public") as batch_op:
        batch_op.drop_column("can_validate_poi_contributions")
