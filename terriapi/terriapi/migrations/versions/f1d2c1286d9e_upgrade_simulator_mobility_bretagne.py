"""upgrade simulator mobility bretagne

Revision ID: f1d2c1286d9e
Revises: 7754d8f3576f
Create Date: 2023-12-14 11:52:56.559666

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f1d2c1286d9e"
down_revision = "7754d8f3576f"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        update simulator.simulator_level set level = 1 where region ='bretagne';
        update simulator.lever set regions = '{auvergne-rhone-alpes,bretagne}' where id in (2,3);
        """
    )


def downgrade():
    op.execute(
        """
            update simulator.simulator_level set level = 0 where region ='bretagne';
            update simulator.lever set regions = '{auvergne-rhone-alpes}' where id in (2,3);
        """
    )
