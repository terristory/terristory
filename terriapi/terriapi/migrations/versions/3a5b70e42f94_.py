"""empty message

Revision ID: 3a5b70e42f94
Revises: 8ff36f50e560, e5150878b20d
Create Date: 2025-01-07 10:55:42.964197

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "3a5b70e42f94"
down_revision = ("8ff36f50e560", "e5150878b20d")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
