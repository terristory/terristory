﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""creer table parametres objectifs

Revision ID: 9cbb67bbe29c
Revises: d8f69de02299
Create Date: 2021-11-22 15:24:35.056184

"""
from alembic import op
from sqlalchemy import Column, Integer, PrimaryKeyConstraint, String

# revision identifiers, used by Alembic.
revision = "9cbb67bbe29c"
down_revision = "d8f69de02299"
branch_labels = None
depends_on = None

schema = "meta"


def creer_table_parametres_objectifs(schema):
    op.create_table(
        "parametres_objectifs",
        Column(
            "titre", String, comment="Titre de l'objectif (SRADDET, REPOS, SNBC etc.)"
        ),
        Column("annee_reference", Integer, comment="Année de référence"),
        Column("graphique", String, comment="Graphique où ajouter cet objectif"),
        Column("couleur", String, comment="Couleur de la trajectoire cible"),
        Column("region", String, comment="Région où l'objectif est saisi"),
        PrimaryKeyConstraint("titre", "graphique", "region"),
        schema=schema,
    )


def ajouter_contrainte_cle_etrangere(schema):
    sql = """
          ALTER TABLE {schema}.objectifs ADD CONSTRAINT fk_titre_objectif
          FOREIGN KEY(titre, graphique, region) REFERENCES {schema}.parametres_objectifs (titre, graphique, region)
          """.format(
        schema=schema
    )
    op.execute(sql)


def upgrade():
    creer_table_parametres_objectifs(schema)
    ajouter_contrainte_cle_etrangere(schema)


def downgrade():
    op.execute("drop table " + schema + ".parametres_objectifs cascade ;")
