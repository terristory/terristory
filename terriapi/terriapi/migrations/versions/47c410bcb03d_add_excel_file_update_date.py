﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add excel file update date

Revision ID: 47c410bcb03d
Revises: b753b71ab6fa
Create Date: 2022-10-19 16:02:48.985555

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "47c410bcb03d"
down_revision = "b753b71ab6fa"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column(
            "export_excel_update_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            comment="Date de la MAJ du fichier excel d'export de résultats",
        ),
    )


def downgrade():
    with op.batch_alter_table("regions_configuration") as batch_op:
        batch_op.drop_column("export_excel_update_date")
