"""enable simulator mobility national

Revision ID: 5ce3f33c77d1
Revises: e8836dfe01b1
Create Date: 2025-02-05 12:11:18.280268

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5ce3f33c77d1"
down_revision = "e8836dfe01b1"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
                UPDATE regions_configuration
                SET ui_show_simulators = jsonb_set(ui_show_simulators, '{mobility}', 'true')
                WHERE is_national
            """
    )


def downgrade():
    op.execute(
        """
                UPDATE regions_configuration
                SET ui_show_simulators = jsonb_set(ui_show_simulators, '{mobility}', 'false')
                WHERE is_national
            """
    )
