﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajout-colonne-type-icone-equipement

Revision ID: ecdd5b34494b
Revises: 02b6d51471e0
Create Date: 2022-06-03 09:35:22.806035

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "ecdd5b34494b"
down_revision = "02b6d51471e0"
branch_labels = None
depends_on = None

schemas = ["auvergne_rhone_alpes", "occitanie", "nouvelle_aquitaine", "bretagne"]


def upgrade():
    for schema in schemas:
        op.execute(
            "alter table {schema}_poi.layer add column type_icone varchar;".format(
                schema=schema
            )
        )


def downgrade():
    for schema in schemas:
        op.execute(
            "alter table {schema}_poi.layer drop column type_icone;".format(
                schema=schema
            )
        )
