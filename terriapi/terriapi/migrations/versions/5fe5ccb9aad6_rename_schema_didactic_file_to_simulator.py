"""rename schema didactic_file to simulator

Revision ID: 5fe5ccb9aad6
Revises: 04d2c5e679f0
Create Date: 2023-09-19 12:15:48.467006

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5fe5ccb9aad6"
down_revision = "04d2c5e679f0"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        f"""
            ALTER SCHEMA didactic_file RENAME TO simulator;
        """
    )


def downgrade():
    op.execute(
        f"""
            ALTER SCHEMA simulator RENAME TO didactic_file;
        """
    )
