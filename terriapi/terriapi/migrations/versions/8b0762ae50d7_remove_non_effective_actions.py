"""Remove non effective actions

Revision ID: 8b0762ae50d7
Revises: e1091bb708b9
Create Date: 2023-11-24 10:17:51.665537

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8b0762ae50d7"
down_revision = "e1091bb708b9"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "UPDATE strategie_territoire.action_status SET enabled = false WHERE action_numero = '7'"
    )


def downgrade():
    op.execute(
        "UPDATE strategie_territoire.action_status SET enabled = true WHERE action_numero = '7'"
    )
