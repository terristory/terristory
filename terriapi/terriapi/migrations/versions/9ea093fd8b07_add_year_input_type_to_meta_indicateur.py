"""Add year input type to meta.indicateur

Revision ID: 9ea093fd8b07
Revises: a3f9ef7d7cb0
Create Date: 2024-10-08 12:20:44.432211

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9ea093fd8b07"
down_revision = "a3f9ef7d7cb0"
branch_labels = None
depends_on = None


def upgrade():

    op.add_column(
        "indicateur",
        sa.Column(
            "year_selection_input_type",
            sa.String,
            nullable=True,
            comment="The type of input to use for year selection. can be 'selection', 'slider', or 'switch-button' if only 2 years",
        ),
        schema="meta",
    )


def downgrade():
    op.drop_column("indicateur", "year_selection_input_type", schema="meta")
