"""empty message

Revision ID: cf081753294f
Revises: 109d70d5246f, cb34905ab6b9
Create Date: 2023-06-23 15:12:40.278924

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "cf081753294f"
down_revision = ("109d70d5246f", "cb34905ab6b9")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
