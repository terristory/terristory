"""Allow to disable subactions

Revision ID: bb4b3a8d628a
Revises: 690a66f2e03a
Create Date: 2024-01-16 17:40:16.350400

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bb4b3a8d628a"
down_revision = "690a66f2e03a"
branch_labels = None
depends_on = None


def upgrade():
    op.create_unique_constraint(
        "uq_subactions_params",
        "action_subactions",
        ["action", "nom"],
        schema="strategie_territoire",
    )
    op.create_table(
        "action_subactions_status",
        sa.Column("region", sa.String, primary_key=True, comment="Region"),
        sa.Column(
            "action_numero", sa.String, primary_key=True, comment="Ancien numero"
        ),
        sa.Column("nom", sa.String, primary_key=True, comment="Subaction unique key"),
        sa.Column("enabled", sa.Boolean, comment="Is enabled", server_default="true"),
        sa.ForeignKeyConstraint(
            ["action_numero", "nom"],
            [
                "strategie_territoire.action_subactions.action",
                "strategie_territoire.action_subactions.nom",
            ],
        ),
        schema="strategie_territoire",
    )
    op.execute(
        """
        INSERT INTO strategie_territoire.action_subactions_status
        SELECT s.region, s.action_numero, sb.nom, true
        FROM strategie_territoire.action_subactions sb
        LEFT JOIN strategie_territoire.action_status s ON s.action_numero = sb.action;
    """
    )


def downgrade():
    op.drop_table("action_subactions_status", schema="strategie_territoire")
    op.drop_constraint(
        "uq_subactions_params", "action_subactions", schema="strategie_territoire"
    )
