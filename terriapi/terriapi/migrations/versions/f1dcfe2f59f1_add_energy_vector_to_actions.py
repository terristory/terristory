"""Add energy vector to actions

Revision ID: f1dcfe2f59f1
Revises: 77c60d48fd7c
Create Date: 2023-11-03 17:33:35.994185

"""

import json

import psycopg2
import sqlalchemy as sa
from alembic import op
from sanic.log import logger

# revision identifiers, used by Alembic.
revision = "f1dcfe2f59f1"
down_revision = "77c60d48fd7c"
branch_labels = None
depends_on = None

links = {
    "RenewableProd.BIOMASS_THER_CHAUFFERIE": "EnergyVector.HEAT",
    "RenewableProd.BIOMASS_THER_COGENERATION": "EnergyVector.HEAT",
    "RenewableProd.BIOMASS_THER_DOMESTIC": "EnergyVector.HEAT",
    "RenewableProd.HYDRO_LOW": "EnergyVector.ELECTRICITY",
    "RenewableProd.HYDRO_HIGH": "EnergyVector.ELECTRICITY",
    "RenewableProd.BIOGAS_ELEC": "EnergyVector.ELECTRICITY",
    "RenewableProd.BIOGAS_THER": "EnergyVector.HEAT",
    "RenewableProd.BIOGAS_INJEC": "EnergyVector.BIOMETHANE",
    "RenewableProd.SOLAR_PV": "EnergyVector.ELECTRICITY",
    "RenewableProd.SOLAR_THER": "EnergyVector.HEAT",
    "RenewableProd.WIND": "EnergyVector.ELECTRICITY",
    "RenewableProd.GEOTHERMY_THER": "EnergyVector.HEAT",
    "RenewableProd.GEOTHERMY_ELEC": "EnergyVector.ELECTRICITY",
    "RenewableProd.WASTE_VALORIZATION_THER": "EnergyVector.HEAT",
    "RenewableProd.WASTE_VALORIZATION_ELEC": "EnergyVector.ELECTRICITY",
}
regions = ["auvergne-rhone-alpes", "occitanie", "paysdelaloire"]


def upgrade():
    for region in regions:
        op.execute(
            f"""INSERT INTO strategie_territoire.passage_table 
            VALUES('{region}', 'DataSet.PRODUCTION/EnergyVector', 
                (
                    SELECT match 
                    FROM strategie_territoire.passage_table 
                    WHERE region = '{region}' 
                    AND key = 'DataSet.PRODUCTION/RenewableProd' AND association_type = 'column'
                ),
                'column', 'keep'
            )""",
        )

    already_inserted = set()
    conn = op.get_bind()
    for prod_name, vector in links.items():
        c_regions = regions
        # only AuRA has double hydro power distinction
        # other regions might have some issues
        if prod_name == "RenewableProd.HYDRO_HIGH":
            c_regions = ["auvergne-rhone-alpes"]
        res_passage = conn.execute(
            f"""SELECT DISTINCT region, 'DataSet.PRODUCTION/{vector}' as key, match, association_type, 'keep'
            FROM strategie_territoire.passage_table 
            WHERE key = 'DataSet.PRODUCTION/{prod_name}'
            AND region IN ('{"', '".join(c_regions)}')"""
        )
        passage_elts = res_passage.fetchall()
        for elt in passage_elts:
            if (
                json.dumps([elt["region"], elt["key"], elt["match"]])
                in already_inserted
            ):
                continue
            conn.execute(
                "INSERT INTO strategie_territoire.passage_table VALUES(%s, %s, %s, %s, %s)",
                elt,
            )
            already_inserted.add(json.dumps([elt["region"], elt["key"], elt["match"]]))


def downgrade():
    op.execute(
        f"""DELETE FROM strategie_territoire.passage_table 
               WHERE key LIKE '%/EnergyVector%'
            AND region IN ('{"', '".join(regions)}')"""
    )
