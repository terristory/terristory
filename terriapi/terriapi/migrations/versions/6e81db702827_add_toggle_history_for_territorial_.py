﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add toggle history for territorial strategy

Revision ID: 6e81db702827
Revises: b753b71ab6fa
Create Date: 2022-10-27 10:23:05.100942

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6e81db702827"
down_revision = "b753b71ab6fa"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("enable_history_actions_plan", sa.Boolean, default=False),
    )


def downgrade():
    with op.batch_alter_table("regions_configuration") as batch_op:
        batch_op.drop_column("enable_history_actions_plan")
