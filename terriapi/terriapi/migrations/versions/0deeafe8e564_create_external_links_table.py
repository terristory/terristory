"""Create external links table

Revision ID: 0deeafe8e564
Revises: 853708b99f1f
Create Date: 2023-10-19 16:22:37.885811

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0deeafe8e564"
down_revision = "853708b99f1f"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "navbar_other_links",
        sa.Column("region_id", sa.String, primary_key=True, comment="Nom de la région"),
        sa.Column("category", sa.String, primary_key=True, comment="Groupe de liens"),
        sa.Column("label", sa.String, primary_key=True, comment="Texte à afficher"),
        sa.Column("url", sa.String, comment="Lien utilisé"),
        sa.Column("zones_types", sa.String, comment="Niveaux géographiques où activé"),
        sa.Column("links_order", sa.Integer, comment="Ordre d'affichage"),
        sa.Column(
            "show_external",
            sa.Boolean,
            server_default=str("t"),
            comment="Est un lien externe",
        ),
        sa.Column(
            "replace_epci_group_names",
            sa.Boolean,
            server_default=str("f"),
            comment="Remplacer les noms de groupes dans les noms d'EPCI",
        ),
        sa.Column(
            "zones_excluded",
            sa.JSON,
            comment="Zones géographiques exclues de cet affichage",
        ),
        schema="meta",
    )


def downgrade():
    op.drop_table("navbar_other_links", schema="meta")
