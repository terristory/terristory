﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add regional division inside configuration

Revision ID: bc7a5be5a35e
Revises: 5705b2c332b4
Create Date: 2022-10-25 11:42:35.422993

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bc7a5be5a35e"
down_revision = "5705b2c332b4"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration", sa.Column("sankey_divisions_enabled", sa.Text)
    )
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.drop_column("division_enabled")


def downgrade():
    with op.batch_alter_table("regions_configuration") as batch_op:
        batch_op.drop_column("sankey_divisions_enabled")
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.add_column(
            sa.Column("division_enabled", sa.Text)
        )  # comment="Divisions géographiques activées")
