"""Add POI source and producer credits

Revision ID: ec0705148ef6
Revises: 930bb5ef8d1b
Create Date: 2023-02-28 09:39:48.747050

"""

from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "ec0705148ef6"
down_revision = "930bb5ef8d1b"
branch_labels = None
depends_on = None

SCHEMAS = [
    "auvergne_rhone_alpes_poi",
    "occitanie_poi",
    "nouvelle_aquitaine_poi",
    "bretagne_poi",
    "paysdelaloire_poi",
]
TABLE = "layer"


def upgrade():
    for schema in SCHEMAS:
        op.add_column(TABLE, Column("credits_data_sources", String()), schema=schema)
        op.add_column(TABLE, Column("credits_data_producers", String()), schema=schema)


def downgrade():
    for schema in SCHEMAS:
        with op.batch_alter_table(TABLE, schema=schema) as batch_op:
            batch_op.drop_column("credits_data_sources")
            batch_op.drop_column("credits_data_producers")
