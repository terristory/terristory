﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create table meta affectation tableau bord

Revision ID: b1c5838bac03
Revises: 5505323f6d6d
Create Date: 2021-01-05 09:19:53.446131

"""
from alembic import op
from sqlalchemy import Column, ForeignKey, Integer, String

# revision identifiers, used by Alembic.
revision = "b1c5838bac03"
down_revision = "5505323f6d6d"
branch_labels = None
depends_on = None


SCHEMA = "meta"
TABLENAME = "tableau_affectation"


def upgrade():
    op.create_table(
        TABLENAME,
        Column(
            "tableau",
            Integer,
            ForeignKey(".".join([SCHEMA, "tableau_bord", "id"]), ondelete="CASCADE"),
            nullable=False,
            comment="Identifiant du tableau de bord",
        ),
        Column("zone", String, nullable=False, comment="Type de territoire"),
        Column(
            "zone_id",
            String,
            comment="Identifiant du territoire si tableau de bord spécifique à un territoire donné",
        ),
        schema=SCHEMA,
    )


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
