"""empty message

Revision ID: 667bc2cb4c51
Revises: 9df1837fd006, 0ff69e372459
Create Date: 2024-10-28 16:22:26.244041

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "667bc2cb4c51"
down_revision = ("9df1837fd006", "0ff69e372459")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
