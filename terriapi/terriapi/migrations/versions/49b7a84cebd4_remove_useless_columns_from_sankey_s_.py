"""Remove useless columns from sankey's table

Revision ID: 49b7a84cebd4
Revises: fd3889275b5e
Create Date: 2023-08-23 09:11:06.170914

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "49b7a84cebd4"
down_revision = "fd3889275b5e"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("sankey_metadata", schema="meta") as batch_op:
        batch_op.drop_column("layout_filename")


def downgrade():
    op.add_column(
        "sankey_metadata", sa.Column("layout_filename", sa.String()), schema="meta"
    )
