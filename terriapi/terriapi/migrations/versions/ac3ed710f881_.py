"""empty message

Revision ID: ac3ed710f881
Revises: ced12acfacdc, 8c23c068b98a
Create Date: 2024-05-06 18:09:56.587544

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "ac3ed710f881"
down_revision = ("ced12acfacdc", "8c23c068b98a")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
