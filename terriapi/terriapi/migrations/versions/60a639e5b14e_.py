"""empty message

Revision ID: 60a639e5b14e
Revises: c5661c307965, 853708b99f1f
Create Date: 2023-10-18 15:24:21.604640

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "60a639e5b14e"
down_revision = ("c5661c307965", "853708b99f1f")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
