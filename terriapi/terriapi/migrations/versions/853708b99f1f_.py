"""empty message

Revision ID: 853708b99f1f
Revises: f9a34f2cadd6, 6fbf1bf3906b
Create Date: 2023-10-12 17:40:44.270961

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "853708b99f1f"
down_revision = ("f9a34f2cadd6", "6fbf1bf3906b")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
