"""Move POI contribution admin rights to standalone table

Revision ID: d37bf05d9872
Revises: 8f5f96167fb9
Create Date: 2023-11-08 13:37:24.933424

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d37bf05d9872"
down_revision = "8f5f96167fb9"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "poi_contributions_contacts",
        sa.Column("region", sa.String, primary_key=True, comment="Region ID"),
        sa.Column("email", sa.String, primary_key=True, comment="User email"),
        sa.Column("layer_name", sa.String, primary_key=True, comment="POI layer"),
        sa.Column(
            "is_receiving_mails",
            sa.Boolean,
            server_default=str("t"),
            comment="Wants to receive notifications",
        ),
        sa.Column(
            "update_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=True,
            comment="Date de mise à jour",
        ),
        schema="meta",
    )
    op.create_foreign_key(
        "fk_poi_contributions_contacts",
        "poi_contributions_contacts",
        "utilisateur",
        ["region", "email"],
        ["region", "mail"],
        source_schema="meta",
    )
    op.execute(
        """INSERT INTO meta.poi_contributions_contacts (
            SELECT DISTINCT id, unnest(string_to_array(poi_contributions_contacts, ',')), '*', true, NOW() FROM regions_configuration
        )"""
    )
    with op.batch_alter_table("regions_configuration", schema="public") as batch_op:
        batch_op.drop_column("poi_contributions_contacts")
    with op.batch_alter_table("utilisateur", schema="public") as batch_op:
        batch_op.drop_column("can_validate_poi_contributions")


def downgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("poi_contributions_contacts", sa.String()),
        schema="public",
    )
    op.add_column(
        "utilisateur",
        sa.Column(
            "can_validate_poi_contributions", sa.Boolean(), server_default="false"
        ),
        schema="public",
    )
    op.execute(
        """UPDATE utilisateur u SET can_validate_poi_contributions = true 
               WHERE EXISTS (SELECT 1 FROM meta.poi_contributions_contacts p WHERE p.email = u.mail
               AND p.region = u.region)"""
    )
    op.execute(
        """UPDATE regions_configuration r SET poi_contributions_contacts = (SELECT COALESCE(STRING_AGG(p.email, ','), '') FROM meta.poi_contributions_contacts p WHERE p.region = r.id)"""
    )
    op.drop_table("poi_contributions_contacts", schema="meta")
