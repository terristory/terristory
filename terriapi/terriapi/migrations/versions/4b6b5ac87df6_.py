"""empty message

Revision ID: 4b6b5ac87df6
Revises: 99922858f271, 667790f83f6a
Create Date: 2023-07-05 15:37:25.175963

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4b6b5ac87df6"
down_revision = ("99922858f271", "667790f83f6a")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
