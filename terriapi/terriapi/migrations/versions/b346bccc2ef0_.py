"""merge heads

Revision ID: b346bccc2ef0
Revises: 3a5b70e42f94, 5cf5725ba405
Create Date: 2025-01-10 11:51:01.703113

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "b346bccc2ef0"
down_revision = ("3a5b70e42f94", "5cf5725ba405")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
