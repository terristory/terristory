"""create table for data source logo

Revision ID: 01d77c16e647
Revises: cccdea3d5af9
Create Date: 2023-01-12 22:24:28.938386

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "01d77c16e647"
down_revision = "cccdea3d5af9"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """ 
        CREATE TABLE meta.data_source_logo(
            id SERIAL primary key,
            url varchar,
            name varchar,
            logo_path varchar,
            extension varchar,
            region varchar);
        """
    )


def downgrade():
    op.drop_table("data_source_logo", schema="meta")
