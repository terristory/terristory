﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""organiser installation rubriques

Revision ID: 3a19b2c3521e
Revises: eb6f6c496e44
Create Date: 2021-03-15 10:45:59.424571

"""
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "3a19b2c3521e"
down_revision = "eb6f6c496e44"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "layer", Column("rubrique", String()), schema="auvergne_rhone_alpes_poi"
    )

    op.add_column("layer", Column("rubrique", String()), schema="occitanie_poi")


def downgrade():
    with op.batch_alter_table("layer", schema="occitanie_poi") as batch_op:
        batch_op.drop_column("rubrique")

    with op.batch_alter_table("layer", schema="auvergne_rhone_alpes_poi") as batch_op:
        batch_op.drop_column("rubrique")
