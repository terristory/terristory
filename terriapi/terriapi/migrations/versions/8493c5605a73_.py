"""empty message

Revision ID: 8493c5605a73
Revises: f2632278515b, 379bbc8a8464
Create Date: 2023-08-10 17:11:34.657352

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8493c5605a73"
down_revision = ("f2632278515b", "379bbc8a8464")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
