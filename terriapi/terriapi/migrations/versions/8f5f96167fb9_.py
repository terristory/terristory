"""empty message

Revision ID: 8f5f96167fb9
Revises: 60a639e5b14e, e46605acc743
Create Date: 2023-11-03 16:26:50.498846

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "8f5f96167fb9"
down_revision = ("60a639e5b14e", "e46605acc743")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
