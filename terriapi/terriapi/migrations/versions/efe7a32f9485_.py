"""empty message

Revision ID: efe7a32f9485
Revises: 189465744863, bede2f649b8d
Create Date: 2024-11-07 11:58:29.805735

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "efe7a32f9485"
down_revision = ("189465744863", "bede2f649b8d")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
