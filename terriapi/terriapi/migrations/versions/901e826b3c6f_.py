"""empty message

Revision ID: 901e826b3c6f
Revises: 8b826bf079f8, e28755e3d14f
Create Date: 2024-07-15 17:00:18.124449

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "901e826b3c6f"
down_revision = ("8b826bf079f8", "e28755e3d14f")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
