"""Add table for parameters for analysis page

Revision ID: beae4839ca12
Revises: a272014697e5
Create Date: 2023-02-08 09:53:00.624672

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.sql import table

# revision identifiers, used by Alembic.
revision = "beae4839ca12"
down_revision = "a272014697e5"
branch_labels = None
depends_on = None
TABLENAME = "analysis_page_parameters"
SCHEMA = "meta"

page_names = {
    "suivi_energetique": {
        "chartEvolConsoSecteur": ["paysdelaloire", "auvergne-rhone-alpes", "occitanie"],
        "chartEvolConsoEnergie": ["paysdelaloire", "auvergne-rhone-alpes", "occitanie"],
        "chartEvolProd": ["paysdelaloire", "auvergne-rhone-alpes", "occitanie"],
        "barConsoVsPnr": ["paysdelaloire", "auvergne-rhone-alpes", "occitanie"],
        "chartEvoConsoUsage": ["auvergne-rhone-alpes", "occitanie"],
    },
    "suivi_emission_ges": {
        "chartEvoEmiSecteur": ["paysdelaloire", "auvergne-rhone-alpes"],
        "chartEvoEmiEnergie": ["paysdelaloire", "auvergne-rhone-alpes"],
        "chartEvoEmiUsage": ["auvergne-rhone-alpes"],
    },
    "suivi_polluants_covnm": {
        "chartEvoEmiSecteur": [
            "auvergne-rhone-alpes",
        ],
    },
    "suivi_polluants_nh3": {
        "chartEvoEmiSecteur": [
            "auvergne-rhone-alpes",
        ],
    },
    "suivi_polluants_nox": {
        "chartEvoEmiSecteur": [
            "auvergne-rhone-alpes",
        ],
    },
    "suivi_polluants_pm10": {
        "chartEvoEmiSecteur": [
            "auvergne-rhone-alpes",
        ],
    },
    "suivi_polluants_pm25": {
        "chartEvoEmiSecteur": [
            "auvergne-rhone-alpes",
        ],
    },
    "suivi_polluants_so2": {
        "chartEvoEmiSecteur": [
            "auvergne-rhone-alpes",
        ],
    },
}
envs = ["dev", "test", "prod"]


def upgrade():
    params_table = op.create_table(
        TABLENAME,
        sa.Column("region_id", sa.String, primary_key=True, comment="Nom de la région"),
        sa.Column(
            "env_name", sa.String, primary_key=True, comment="Nom de l'environnement"
        ),
        sa.Column(
            "page_name", sa.String, primary_key=True, comment="Nom de la page affichée"
        ),
        sa.Column("graph_type", sa.String, primary_key=True, comment="Région"),
        sa.Column("status", sa.Boolean, server_default=str("t"), comment="Région"),
        sa.Column(
            "update_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=True,
            comment="Date de mise à jour",
        ),
        schema=SCHEMA,
    )

    op.bulk_insert(
        params_table,
        [
            {
                "region_id": region,
                "env_name": env,
                "page_name": page_name,
                "graph_type": graph_type,
                "status": True,
            }
            for page_name, graph_types in page_names.items()
            for graph_type, regions_enabled in graph_types.items()
            for region in regions_enabled
            for env in envs
        ],
    )
    op.add_column(
        "regions_configuration",
        sa.Column("ui_show_cesba", sa.Boolean, server_default=str("t")),
    )


def downgrade():
    with op.batch_alter_table("regions_configuration") as batch_op:
        batch_op.drop_column("ui_show_cesba")
    op.drop_table(TABLENAME, schema=SCHEMA)
