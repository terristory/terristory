"""Add waste heat valorisation to PCAET passage table

Revision ID: 0fe48e9214c6
Revises: b346bccc2ef0
Create Date: 2024-10-24 10:47:18.287395

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0fe48e9214c6"
down_revision = "b346bccc2ef0"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        INSERT INTO strategie_territoire.passage_table(region, key, match, association_type, aggregation)
        VALUES ('pcaet', 'DataSet.PRODUCTION/RenewableProd.WASTE_HEAT_VALORIZATION', '50', 'value', 'keep');
        """
    )


def downgrade():
    op.execute(
        """
        DELETE FROM strategie_territoire.passage_table
        WHERE key='DataSet.PRODUCTION/RenewableProd.WASTE_HEAT_VALORIZATION';
        """
    )
