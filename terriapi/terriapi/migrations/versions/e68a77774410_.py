"""empty message

Revision ID: e68a77774410
Revises: ab27575c1e56, 49e694680adb
Create Date: 2023-10-05 10:31:11.555137

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e68a77774410"
down_revision = ("ab27575c1e56", "49e694680adb")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
