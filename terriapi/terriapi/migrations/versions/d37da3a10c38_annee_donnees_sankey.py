﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""annee donnees sankey

Revision ID: d37da3a10c38
Revises: adde05548803
Create Date: 2022-07-18 08:35:19.909078

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "d37da3a10c38"
down_revision = "adde05548803"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("alter table meta.donnees_sankey add column annee int;")


def downgrade():
    op.execute("alter table meta.donnees_sankey drop column annee;")
