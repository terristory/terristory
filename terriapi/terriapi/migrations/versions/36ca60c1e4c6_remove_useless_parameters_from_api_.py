"""Remove useless parameters from API entrypoint

Revision ID: 36ca60c1e4c6
Revises: 0aaba7560457
Create Date: 2023-08-22 08:39:22.523571

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "36ca60c1e4c6"
down_revision = "0aaba7560457"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("UPDATE meta.external_api SET main_url = CONCAT(main_url, url_suffix)")
    with op.batch_alter_table("external_api", schema="meta") as batch_op:
        batch_op.drop_column("url_suffix")
        batch_op.alter_column("main_url", new_column_name="url")
        batch_op.add_column(
            sa.Column("subkeys", sa.String)
        )  # comment="Divisions géographiques activées")


def downgrade():
    with op.batch_alter_table("external_api", schema="meta") as batch_op:
        batch_op.add_column(
            sa.Column("url_suffix", sa.String)
        )  # comment="Divisions géographiques activées")
        batch_op.drop_column("subkeys")
        batch_op.alter_column("url", new_column_name="main_url")
