"""empty message

Revision ID: c6557411a5d5
Revises: 4eb4750e9709, 760b4db4a79d
Create Date: 2024-02-19 08:25:25.734188

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c6557411a5d5"
down_revision = ("4eb4750e9709", "760b4db4a79d")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
