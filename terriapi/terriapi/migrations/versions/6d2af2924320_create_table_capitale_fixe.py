﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""create-table-capitale-fixe

Revision ID: 6d2af2924320
Revises: 7c2540ccd418
Create Date: 2021-12-21 15:04:49.121265

"""
from alembic import op
from sqlalchemy import Column, Float, String

# revision identifiers, used by Alembic.
revision = "6d2af2924320"
down_revision = "7c2540ccd418"
branch_labels = None
depends_on = None

SCHEMA = "auvergne_rhone_alpes"
TABLENAME = "capitale_fixe"


def upgrade():
    op.create_table(
        TABLENAME,
        Column(
            "code_territoire",
            String,
            primary_key=True,
            comment="Identifiant Territoire",
        ),  # contient tous les codes des territoires possibles(EPCI, TEPOSCV, Departement, Region)
        Column(
            "capitale_industrie",
            Float,
            nullable=False,
            comment="Capitale net fixe pour le secteur 5 : industrie",
        ),
        Column(
            "capitale_agriculture",
            Float,
            nullable=False,
            comment="Capitale net fixe pour le secteur 2 : agriculture",
        ),
        schema=SCHEMA,
    )


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
