"""Add meta data for national regions import forms

Revision ID: d2c01be8090d
Revises: 9deb3472be32
Create Date: 2024-08-30 12:41:33.515754

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d2c01be8090d"
down_revision = "9deb3472be32"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "national_import_metadata",
        sa.Column("region", sa.String, primary_key=True, comment="Nom de la région"),
        sa.Column("table_key", sa.String, primary_key=True, comment="Clé de la table"),
        sa.Column("table_name", sa.String, primary_key=True, comment="Nom à afficher"),
        sa.Column("description", sa.Text, comment="Description"),
        sa.Column("template_file", sa.String, comment="Nom d'un fichier de template"),
        sa.Column("categories", sa.ARRAY(sa.String), comment="Catégories"),
        schema="meta",
    )
    op.execute("ALTER TABLE national SET SCHEMA meta;")


def downgrade():
    op.drop_table("national_import_metadata", schema="meta")
    op.execute("ALTER TABLE meta.national SET SCHEMA public;")
