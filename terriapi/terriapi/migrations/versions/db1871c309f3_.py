"""empty message

Revision ID: db1871c309f3
Revises: d1d12afe8c30, cb34905ab6b9
Create Date: 2023-06-28 14:20:30.339994

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "db1871c309f3"
down_revision = ("d1d12afe8c30", "cb34905ab6b9")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
