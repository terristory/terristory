"""Add contact mail for responses

Revision ID: bd3eeecb8ffd
Revises: 8f5f96167fb9
Create Date: 2023-11-07 18:31:23.741576

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "bd3eeecb8ffd"
down_revision = "8f5f96167fb9"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "regions_configuration",
        sa.Column("contact_email_response_to", sa.String()),
        schema="public",
    )
    op.execute(
        "UPDATE regions_configuration SET contact_email_response_to = contact_email"
    )
    op.execute(
        "UPDATE regions_configuration SET contact_email = CONCAT(id, '@terristory.fr')"
    )


def downgrade():
    op.execute(
        "UPDATE regions_configuration SET contact_email = contact_email_response_to"
    )
    with op.batch_alter_table("regions_configuration", schema="public") as batch_op:
        batch_op.drop_column("contact_email_response_to")
