"""Add 'region' column to scenarios'

Revision ID: 092c74a577e2
Revises: ec0705148ef6
Create Date: 2023-03-27 14:50:51.950084

"""

from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "092c74a577e2"
down_revision = "ec0705148ef6"
branch_labels = None
depends_on = None

SCHEMA = "public"
TABLENAME = "utilisateur_scenario"


def upgrade():
    op.add_column(
        TABLENAME,
        Column("region", String, nullable=True, comment="Nom de la région"),
        schema=SCHEMA,
    )
    op.execute(
        """
        update utilisateur_scenario
        set region = utilisateur.region
        from utilisateur
        where utilisateur_scenario.mail = utilisateur.mail;
        """
    )
    op.alter_column("utilisateur_scenario", "region", nullable=False)


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("region")
