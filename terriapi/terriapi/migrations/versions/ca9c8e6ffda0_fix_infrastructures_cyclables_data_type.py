"""Fix infrastructures_cyclables data type

Revision ID: ca9c8e6ffda0
Revises: 7c02c14a13e2
Create Date: 2023-02-16 11:14:29.318312

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "ca9c8e6ffda0"
down_revision = "7c02c14a13e2"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "UPDATE meta.indicateur SET data_type = 'infrastructures_cyclables' WHERE data_type = 'inrastructures_cyclables'"
    )


def downgrade():
    op.execute(
        "UPDATE meta.indicateur SET data_type = 'inrastructures_cyclables' WHERE data_type = 'infrastructures_cyclables'"
    )
