﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add regional division and factor inside configuration

Revision ID: 5705b2c332b4
Revises: b753b71ab6fa
Create Date: 2022-10-25 11:06:47.079144

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5705b2c332b4"
down_revision = "b753b71ab6fa"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.add_column(
            sa.Column("division_enabled", sa.Text)
        )  # comment="Divisions géographiques activées")
        batch_op.add_column(
            sa.Column("division_factors", sa.JSON)
        )  # comment="Facteurs multiplicatifs associés aux divisions géographiques")
        batch_op.add_column(
            sa.Column("division_units", sa.JSON)
        )  # comment="Unités associées aux divisions géographiques")


def downgrade():
    with op.batch_alter_table("donnees_sankey", schema="meta") as batch_op:
        batch_op.drop_column("division_enabled")
        batch_op.drop_column("division_factors")
        batch_op.drop_column("division_units")
