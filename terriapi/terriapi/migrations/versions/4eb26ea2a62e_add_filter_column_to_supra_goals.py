"""Add filter column to supra goals

Revision ID: 4eb26ea2a62e
Revises: e1356f351c31
Create Date: 2024-10-07 12:27:22.557419

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "4eb26ea2a62e"
down_revision = "e1356f351c31"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "supra_goals",
        sa.Column("filter", sa.ARRAY(sa.String), server_default="{}"),
        schema="strategie_territoire",
    )


def downgrade():
    with op.batch_alter_table("supra_goals", schema="strategie_territoire") as batch_op:
        batch_op.drop_column("filter")
