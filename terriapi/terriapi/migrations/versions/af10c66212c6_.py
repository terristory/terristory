"""empty message

Revision ID: af10c66212c6
Revises: 7e95c851455c, 84f9864e2641
Create Date: 2024-05-31 11:24:57.223054

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "af10c66212c6"
down_revision = ("7e95c851455c", "84f9864e2641")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
