﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add additional detail for indicator

Revision ID: e010b9b4a598
Revises: 3d4ec5a57c55
Create Date: 2022-11-24 17:23:21.086940

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e010b9b4a598"
down_revision = "3d4ec5a57c55"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "indicateur_representation_details"


def upgrade():
    op.create_table(
        TABLENAME,
        sa.Column(
            "indicateur_id",
            sa.Integer,
            sa.ForeignKey("meta.indicateur.id", ondelete="CASCADE"),
            nullable=False,
            primary_key=True,
        ),
        sa.Column(
            "name",
            sa.String,
            primary_key=True,
            comment="Current detail name",
        ),  # contient tous les codes des territoires possibles(EPCI, TEPOSCV, Departement, Region)
        sa.Column(
            "value",
            sa.Text,
            comment="Value of current detail",
        ),
        schema=SCHEMA,
    )


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
