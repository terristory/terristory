﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""pcaet_table

Revision ID: a5e7591eef22
Revises: 7c2540ccd418
Create Date: 2021-11-17 14:43:12.017541

"""
from alembic import op
from sqlalchemy import VARCHAR, Boolean, Column, Integer, Numeric

# revision identifiers, used by Alembic.
revision = "a5e7591eef22"
down_revision = "7c2540ccd418"
branch_labels = None
depends_on = None

SCHEMA = "public"
TABLENAME = "pcaet_ademe"


def upgrade():
    op.create_table(
        TABLENAME,
        Column("siren", VARCHAR, primary_key=True),
        Column("annee_compta_emission", Integer, nullable=False),
        Column("emission_ges_annee_ref", Numeric),
        Column("emission_ges_annee_2021", Numeric),
        Column("emission_ges_annee_2026", Numeric),
        Column("emission_ges_annee_2030", Numeric),
        Column("emission_ges_annee_2050", Numeric),
        Column("annee_compta_conso", Integer, nullable=False),
        Column("consommation_ener_annee_ref", Numeric),
        Column("consommation_ener_annee_2021", Numeric),
        Column("consommation_ener_annee_2026", Numeric),
        Column("consommation_ener_annee_2030", Numeric),
        Column("consommation_ener_annee_2050", Numeric),
        Column("annee_diagnostique_enr", Integer, nullable=False),
        Column("enr_production_annee_ref", Numeric),
        Column("enr_production_annee_2021", Numeric),
        Column("enr_production_annee_2026", Numeric),
        Column("enr_production_annee_2030", Numeric),
        Column("enr_production_annee_2050", Numeric),
        Column("date_modif_pcaet", Boolean),
        schema=SCHEMA,
    )


def downgrade():
    op.drop_table(TABLENAME, schema=SCHEMA)
