"""update renewable passage table

Revision ID: f9a34f2cadd6
Revises: 290234b805c6
Create Date: 2023-10-04 12:02:18.065747

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f9a34f2cadd6"
down_revision = "290234b805c6"
branch_labels = None
depends_on = None

TABLE = "strategie_territoire.passage_table"
pcaet_renewable_association = [
    ("WIND", 1),
    ("SOLAR_PV", 2),
    ("SOLAR_THERMODYNAMIC", 3),
    ("HYDRO_LOW", 4),
    ("HYDRO_HIGH", 4),
    ("BIOMASS_ELEC", 5),
    ("BIOGAS_ELEC", 6),
    ("GEOTHERMY_ELEC", 7),
    ("BIOMASS_THER_COGENERATION", 8),
    ("BIOMASS_THER_CHAUFFERIE", 8),
    ("BIOMASS_THER_DOMESTIC", 8),
    ("HEAT_PUMP", 9),
    ("GEOTHERMY_THER", 10),
    ("SOLAR_THER", 11),
    ("BIOGAS_THER", 12),
    ("BIOGAS_INJEC", 13),
    ("BIOFUEL", 14),
]
occitanie_renewable_association = [
    ("HYDRO_LOW", 1),
    ("HYDRO_HIGH", 1),
    ("SOLAR_PV", 2),
    ("WIND", 3),
    ("BIOGAS_ELEC", 4),
    ("BIOGAS_THER", 5),
    # ("WASTE_ELEC", 6), ("WASTE_THER", 7),
    ("BIOMASS_ELEC", 8),
    ("BIOMASS_THER_COGENERATION", 9),
    ("BIOMASS_THER_CHAUFFERIE", 10),
    ("BIOMASS_THER_DOMESTIC", 11),
    ("SOLAR_THER", 12),
    ("BIOGAS_INJEC", 13),
    ("GEOTHERMY_ELEC", 14),
    ("GEOTHERMY_THER", 14),
]
aura_renewable_association = [
    # ("PAC_AEROTHERMIC", 1),
    ("SOLAR_THER", 2),
    ("SOLAR_PV", 3),
    ("WIND", 4),
    ("HYDRO_LOW", 5),
    ("BIOMASS_ELEC", 6),
    ("BIOMASS_THER_COGENERATION", 6),
    ("BIOMASS_THER_CHAUFFERIE", 6),
    ("BIOMASS_THER_DOMESTIC", 6),
    ("BIOGAS_ELEC", 7),
    ("BIOGAS_THER", 8),
    # ("WASTE_ELEC", 9), ("WASTE_THER", 10), ("OTHER_RENEWABLES", 11),
    ("HYDRO_HIGH", 12),
    ("BIOGAS_INJEC", 13),
    ("GEOTHERMY_ELEC", 14),
    ("GEOTHERMY_THER", 14),
]
pdl_renewable_association = [
    ("BIOMASS_ELEC", 1),
    ("BIOMASS_THER_COGENERATION", 1),
    ("BIOMASS_THER_CHAUFFERIE", 1),
    ("BIOMASS_THER_DOMESTIC", 1),
    ("WIND", 2),
    ("GEOTHERMY_ELEC", 3),
    ("GEOTHERMY_THER", 3),
    ("HYDRO_LOW", 4),
    ("HYDRO_HIGH", 4),
    ("BIOGAS_ELEC", 5),
    ("BIOGAS_THER", 5),
    ("BIOGAS_INJEC", 5),
    # ("PAC", 6),
    ("SOLAR_PV", 7),
    ("SOLAR_THER", 8),
    # ("WASTE_ELEC", 9), ("WASTE_THER", 9)
]


def upgrade():
    op.execute(
        f"delete from {TABLE} where region='pcaet' and key like 'DataSet.PRODUCTION/RenewableProd.%'"
    )
    op.execute(
        f"insert into {TABLE} (region, key, match, association_type, aggregation) values"
        + ", ".join(
            f"('pcaet', 'DataSet.PRODUCTION/RenewableProd.{key}', '{modalite}', 'value', 'keep')"
            for key, modalite in pcaet_renewable_association
        )
    )

    op.execute(
        f"delete from {TABLE} where region='occitanie' and key like 'DataSet.PRODUCTION/RenewableProd.%'"
    )
    op.execute(
        f"insert into {TABLE} (region, key, match, association_type, aggregation) values"
        + ", ".join(
            f"('occitanie', 'DataSet.PRODUCTION/RenewableProd.{key}', '{modalite}', 'value', 'keep')"
            for key, modalite in occitanie_renewable_association
        )
    )

    op.execute(
        f"delete from {TABLE} where region='auvergne-rhone-alpes' and key like 'DataSet.PRODUCTION/RenewableProd.%'"
    )
    op.execute(
        f"insert into {TABLE} (region, key, match, association_type, aggregation) values"
        + ", ".join(
            f"('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.{key}', '{modalite}', 'value', 'keep')"
            for key, modalite in aura_renewable_association
        )
    )

    op.execute(
        f"delete from {TABLE} where region='paysdelaloire' and key like 'DataSet.PRODUCTION/RenewableProd.%'"
    )
    op.execute(
        f"insert into {TABLE} (region, key, match, association_type, aggregation) values"
        + ", ".join(
            f"('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.{key}', '{modalite}', 'value', 'keep')"
            for key, modalite in pdl_renewable_association
        )
    )


def downgrade():
    op.execute(
        f"update {TABLE} set key='DataSet.PRODUCTION/RenewableProd.BIOMASS' where key like 'DataSet.PRODUCTION/RenewableProd.BIOMASS_%'"
    )
