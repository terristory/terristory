"""add is_national column to regions_configuration

Revision ID: 84f9864e2641
Revises: ac3ed710f881
Create Date: 2024-05-29 16:59:12.124552

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "84f9864e2641"
down_revision = "ac3ed710f881"
branch_labels = None
depends_on = None

SCHEMA = "public"
TABLENAME = "regions_configuration"


def upgrade():

    op.create_table(
        "national",
        sa.Column("enabled", sa.Boolean, nullable=False),
        schema=SCHEMA,
    )
    op.add_column(
        TABLENAME,
        sa.Column(
            "is_national",
            sa.Boolean,
            nullable=False,
            server_default=sa.false(),
            comment="Indique si la région est nationale",
        ),
        schema=SCHEMA,
    )
    op.add_column(
        TABLENAME,
        sa.Column(
            "enabled",
            sa.Boolean,
            nullable=False,
            server_default=sa.true(),
            comment="Indique si la région est activée sur le serveur",
        ),
        schema=SCHEMA,
    )
    op.execute(
        """
            update regions_configuration
            set is_national = true 
            where id='national';

            insert into national values(true);
        """
    )
    op.execute(
        """
            CREATE EXTENSION IF NOT EXISTS unaccent;
            
            ALTER TABLE simulator.lever
            ADD COLUMN key VARCHAR;

            UPDATE simulator.lever
            SET key = LOWER(REPLACE(unaccent(SUBSTRING(title FROM '–\s*(.*)')), ' ', '_'));
        """
    )
    op.execute(
        """
            -- Drop existing primary key constraint
            ALTER TABLE simulator.simulator_level
            DROP CONSTRAINT IF EXISTS simulator_level_pkey;

            -- Set region as the new primary key, if it doesn't already exist
            ALTER TABLE simulator.simulator_level
            ADD PRIMARY KEY (region);

            -- Drop the id column, if it exists
            ALTER TABLE simulator.simulator_level
            DROP COLUMN IF EXISTS id;

        """
    )


def downgrade():
    op.drop_table("national", schema=SCHEMA)
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("is_national")
        batch_op.drop_column("enabled")
    with op.batch_alter_table("lever", schema="simulator") as batch_op:
        batch_op.drop_column("key")
