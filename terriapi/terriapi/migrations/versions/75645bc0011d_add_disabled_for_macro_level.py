﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add disabled for macro level

Revision ID: 75645bc0011d
Revises: 886758e9dd9f
Create Date: 2022-10-17 18:25:55.352251

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "75645bc0011d"
down_revision = "886758e9dd9f"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "indicateur",
        sa.Column("disabled_for_macro_level", sa.String(), default=""),
        schema="meta",
    )


def downgrade():
    with op.batch_alter_table("indicateur", schema="meta") as batch_op:
        batch_op.drop_column("disabled_for_macro_level")
