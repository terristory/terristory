"""empty message

Revision ID: 12f4726a8fe0
Revises: 180d7edc1ac8, d489c0375dd3
Create Date: 2023-05-12 09:18:44.217277

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "12f4726a8fe0"
down_revision = ("180d7edc1ac8", "d489c0375dd3")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
