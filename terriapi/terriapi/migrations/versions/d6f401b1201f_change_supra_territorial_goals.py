"""Change supra territorial goals

Revision ID: d6f401b1201f
Revises: e46605acc743
Create Date: 2023-11-03 11:30:01.846515

"""

import sqlalchemy as sa
from alembic import op

from terriapi.controller import (
    conversion_binaire_en_chaine,
    conversion_chaine_en_binaire,
)

# revision identifiers, used by Alembic.
revision = "d6f401b1201f"
down_revision = "e46605acc743"
branch_labels = None
depends_on = None


def upgrade():
    relevant_keys = {
        "Production d'EnR": "DataSet.PRODUCTION",
        "Production EnR": "DataSet.PRODUCTION",
        "Consommation d'énergie": "DataSet.CONSUMPTION",
        "Émissions de gaz à effet de serre": "DataSet.EMISSIONS",
        "Émissions de NOx": "DataSet.POLLUTANT_NOX",
        "Émissions de COVNM": "DataSet.POLLUTANT_COVNM",
        "Émissions de PM10": "DataSet.POLLUTANT_PM10",
        "Émissions de PM2.5": "DataSet.POLLUTANT_PM25",
        "Émissions de NH3": "DataSet.POLLUTANT_NH3",
        "Émissions de SOx": "DataSet.POLLUTANT_SOX",
        "Émissions de SO2": "DataSet.POLLUTANT_SOX",
    }
    req = f"""
          SELECT region, key, match FROM strategie_territoire.passage_table 
          WHERE association_type = 'table' AND key IN ('{"', '".join(relevant_keys.values())}')
          """
    conn = op.get_bind()
    res_passage = conn.execute(req)
    passage_names = res_passage.fetchall()
    regions = list(set(row["region"] for row in passage_names))

    conv_to_passage = {region: {} for region in regions}
    for row in passage_names:
        conv_to_passage[row["region"]][row["match"]] = row["key"]

    res = conn.execute("SELECT titre, graphique, region FROM meta.objectifs")
    results = res.fetchall()

    # before updating, we remove any constraint
    conn.execute("ALTER TABLE meta.objectifs DROP CONSTRAINT fk_titre_objectif")
    for row in results:
        conn.execute(
            """UPDATE meta.objectifs SET titre = %s, graphique = %s WHERE titre = %s AND graphique = %s AND region = %s""",
            (
                conversion_binaire_en_chaine(row["titre"]),
                relevant_keys[row["graphique"]],
                row["titre"],
                row["graphique"],
                row["region"],
            ),
        )

    res = conn.execute(
        "SELECT titre, graphique, region, description FROM meta.parametres_objectifs"
    )
    results = res.fetchall()
    for row in results:
        conn.execute(
            """UPDATE meta.parametres_objectifs SET titre = %s, description = %s, graphique = %s WHERE titre = %s AND graphique = %s AND region = %s""",
            (
                conversion_binaire_en_chaine(row["titre"]),
                conversion_binaire_en_chaine(row["description"]),
                relevant_keys[row["graphique"]],
                row["titre"],
                row["graphique"],
                row["region"],
            ),
        )

    res = conn.execute(
        "SELECT titre, graphique, region FROM meta.affectation_objectifs"
    )
    results = res.fetchall()
    for row in results:
        conn.execute(
            """UPDATE meta.affectation_objectifs SET titre = %s, graphique = %s WHERE titre = %s AND graphique = %s AND region = %s""",
            (
                conversion_binaire_en_chaine(row["titre"]),
                conv_to_passage[row["region"]][row["graphique"]],
                row["titre"],
                row["graphique"],
                row["region"],
            ),
        )

    conn.execute("ALTER TABLE meta.objectifs RENAME TO supra_goals_values")
    conn.execute("ALTER TABLE meta.supra_goals_values SET SCHEMA strategie_territoire")
    conn.execute("ALTER TABLE meta.parametres_objectifs RENAME TO supra_goals")
    conn.execute("ALTER TABLE meta.supra_goals SET SCHEMA strategie_territoire")
    conn.execute("ALTER TABLE meta.affectation_objectifs RENAME TO supra_goals_links")
    conn.execute("ALTER TABLE meta.supra_goals_links SET SCHEMA strategie_territoire")

    # cleaning the table => we remove elements in links which are not linked to existing supra goals
    conn.execute(
        """DELETE FROM strategie_territoire.supra_goals_links l 
                 WHERE exists (
                    SELECT 1 FROM strategie_territoire.supra_goals_links g 
                    LEFT JOIN strategie_territoire.supra_goals s 
                    ON s.titre = g.titre AND g.region = s.region AND s.graphique = g.graphique 
                    WHERE s.couleur IS NULL 
                AND g.titre = l.titre AND g.graphique = l.graphique AND g.region = l.region
    );"""
    )

    # after updating, we add the constraint
    conn.execute(
        """ALTER TABLE strategie_territoire.supra_goals
        ADD CONSTRAINT unique_supra_goal_details
        UNIQUE(titre, graphique, region)"""
    )
    conn.execute(
        "ALTER TABLE strategie_territoire.supra_goals DROP CONSTRAINT parametres_objectifs_pkey"
    )
    conn.execute(
        "ALTER TABLE strategie_territoire.supra_goals ADD COLUMN supra_goal_id SERIAL PRIMARY KEY"
    )

    # we edit the linked table to use only ID
    conn.execute(
        "ALTER TABLE strategie_territoire.supra_goals_values ADD COLUMN supra_goal_id BIGINT"
    )
    conn.execute(
        "ALTER TABLE strategie_territoire.supra_goals_links ADD COLUMN supra_goal_id BIGINT"
    )
    conn.execute(
        """UPDATE strategie_territoire.supra_goals_values s SET supra_goal_id = (
            SELECT supra_goal_id FROM strategie_territoire.supra_goals g
            WHERE g.titre = s.titre AND g.graphique = s.graphique AND g.region = s.region
        )"""
    )
    conn.execute(
        """UPDATE strategie_territoire.supra_goals_links s SET supra_goal_id = (
            SELECT supra_goal_id FROM strategie_territoire.supra_goals g
            WHERE g.titre = s.titre AND g.graphique = s.graphique AND g.region = s.region
        )"""
    )
    # after updating, we add the foreign key constraints
    conn.execute(
        """ALTER TABLE strategie_territoire.supra_goals_values ADD CONSTRAINT fk_supra_goal_main_object
        FOREIGN KEY (supra_goal_id)
        REFERENCES strategie_territoire.supra_goals(supra_goal_id)
        ON UPDATE CASCADE ON DELETE CASCADE"""
    )
    conn.execute(
        """ALTER TABLE strategie_territoire.supra_goals_links ADD CONSTRAINT fk_supra_goal_main_object
        FOREIGN KEY (supra_goal_id)
        REFERENCES strategie_territoire.supra_goals(supra_goal_id)
        ON UPDATE CASCADE ON DELETE CASCADE"""
    )
    # we can then drop useless columns
    conn.execute(
        """ALTER TABLE strategie_territoire.supra_goals_links DROP COLUMN region;
        ALTER TABLE strategie_territoire.supra_goals_links DROP COLUMN titre;
        ALTER TABLE strategie_territoire.supra_goals_links DROP COLUMN graphique;"""
    )
    conn.execute(
        """ALTER TABLE strategie_territoire.supra_goals_values DROP COLUMN region;
        ALTER TABLE strategie_territoire.supra_goals_values DROP COLUMN titre;
        ALTER TABLE strategie_territoire.supra_goals_values DROP COLUMN graphique;"""
    )
    # after updating, we add the constraint
    conn.execute(
        """ALTER TABLE strategie_territoire.supra_goals_links
        ADD CONSTRAINT unique_supra_goal_link_details
        UNIQUE(type_territoire, code_territoire, supra_goal_id)"""
    )


def downgrade():
    # we can then drop useless columns
    op.execute(
        """ALTER TABLE strategie_territoire.supra_goals_links ADD COLUMN region character varying;
        ALTER TABLE strategie_territoire.supra_goals_links ADD COLUMN titre character varying;
        ALTER TABLE strategie_territoire.supra_goals_links ADD COLUMN graphique character varying;"""
    )
    op.execute(
        """ALTER TABLE strategie_territoire.supra_goals_values ADD COLUMN region character varying;
        ALTER TABLE strategie_territoire.supra_goals_values ADD COLUMN titre character varying;
        ALTER TABLE strategie_territoire.supra_goals_values ADD COLUMN graphique character varying;"""
    )

    op.execute(
        """UPDATE strategie_territoire.supra_goals_values s SET region = (
            SELECT region FROM strategie_territoire.supra_goals g
            WHERE g.supra_goal_id = s.supra_goal_id
        ), titre = (
            SELECT titre FROM strategie_territoire.supra_goals g
            WHERE g.supra_goal_id = s.supra_goal_id
        ), graphique = (
            SELECT graphique FROM strategie_territoire.supra_goals g
            WHERE g.supra_goal_id = s.supra_goal_id
        )"""
    )
    op.execute(
        """UPDATE strategie_territoire.supra_goals_links s SET region = (
            SELECT region FROM strategie_territoire.supra_goals g
            WHERE g.supra_goal_id = s.supra_goal_id
        ), titre = (
            SELECT titre FROM strategie_territoire.supra_goals g
            WHERE g.supra_goal_id = s.supra_goal_id
        ), graphique = (
            SELECT graphique FROM strategie_territoire.supra_goals g
            WHERE g.supra_goal_id = s.supra_goal_id
        )"""
    )

    op.execute(
        "ALTER TABLE strategie_territoire.supra_goals_values DROP CONSTRAINT IF EXISTS fk_supra_goal_main_object"
    )
    op.execute(
        "ALTER TABLE strategie_territoire.supra_goals_links DROP CONSTRAINT IF EXISTS fk_supra_goal_main_object"
    )
    op.execute(
        "ALTER TABLE strategie_territoire.supra_goals_links DROP CONSTRAINT IF EXISTS unique_supra_goal_link_details"
    )
    op.execute("ALTER TABLE strategie_territoire.supra_goals DROP COLUMN supra_goal_id")
    op.execute(
        """ALTER TABLE strategie_territoire.supra_goals 
        ADD CONSTRAINT parametres_objectifs_pkey 
        PRIMARY KEY(titre, graphique, region)"""
    )
    op.execute(
        """ALTER TABLE strategie_territoire.supra_goals DROP CONSTRAINT unique_supra_goal_details"""
    )
    op.execute(
        "ALTER TABLE strategie_territoire.supra_goals_values DROP COLUMN supra_goal_id"
    )
    op.execute(
        "ALTER TABLE strategie_territoire.supra_goals_links DROP COLUMN supra_goal_id"
    )

    op.execute("ALTER TABLE strategie_territoire.supra_goals_values SET SCHEMA meta")
    op.execute("ALTER TABLE meta.supra_goals_values RENAME TO objectifs")
    op.execute("ALTER TABLE strategie_territoire.supra_goals SET SCHEMA meta")
    op.execute("ALTER TABLE meta.supra_goals RENAME TO parametres_objectifs")
    op.execute("ALTER TABLE strategie_territoire.supra_goals_links SET SCHEMA meta")
    op.execute("ALTER TABLE meta.supra_goals_links RENAME TO affectation_objectifs")

    relevant_keys = {
        "DataSet.PRODUCTION": "Production d'EnR",
        "DataSet.CONSUMPTION": "Consommation d'énergie",
        "DataSet.EMISSIONS": "Émissions de gaz à effet de serre",
        "DataSet.POLLUTANT_NOX": "Émissions de NOx",
        "DataSet.POLLUTANT_COVNM": "Émissions de COVNM",
        "DataSet.POLLUTANT_PM10": "Émissions de PM10",
        "DataSet.POLLUTANT_PM25": "Émissions de PM2.5",
        "DataSet.POLLUTANT_NH3": "Émissions de NH3",
        "DataSet.POLLUTANT_SOX": "Émissions de SOx",
    }
    req = f"""
          SELECT region, key, match FROM strategie_territoire.passage_table 
          WHERE association_type = 'table' AND key IN ('{"', '".join(relevant_keys.keys())}')
          """
    conn = op.get_bind()
    res_passage = conn.execute(req)
    passage_names = res_passage.fetchall()
    regions = list(set(row["region"] for row in passage_names))

    conv_to_passage = {region: {} for region in regions}
    for row in passage_names:
        conv_to_passage[row["region"]][row["key"]] = row["match"]

    res = conn.execute("SELECT titre, graphique, region FROM meta.objectifs")
    results = res.fetchall()

    for row in results:
        conn.execute(
            """UPDATE meta.objectifs SET titre = %s, graphique = %s WHERE titre = %s AND graphique = %s AND region = %s""",
            (
                conversion_chaine_en_binaire(row["titre"]),
                relevant_keys[row["graphique"]],
                row["titre"],
                row["graphique"],
                row["region"],
            ),
        )

    res = conn.execute(
        "SELECT titre, graphique, region, description FROM meta.parametres_objectifs"
    )
    results = res.fetchall()
    for row in results:
        conn.execute(
            """UPDATE meta.parametres_objectifs SET titre = %s, description = %s, graphique = %s WHERE titre = %s AND graphique = %s AND region = %s""",
            (
                conversion_chaine_en_binaire(row["titre"]),
                conversion_chaine_en_binaire(row["description"]),
                relevant_keys[row["graphique"]],
                row["titre"],
                row["graphique"],
                row["region"],
            ),
        )

    res = conn.execute(
        "SELECT titre, graphique, region FROM meta.affectation_objectifs"
    )
    results = res.fetchall()
    for row in results:
        conn.execute(
            """UPDATE meta.affectation_objectifs SET titre = %s, graphique = %s WHERE titre = %s AND graphique = %s AND region = %s""",
            (
                conversion_chaine_en_binaire(row["titre"]),
                conv_to_passage[row["region"]][row["graphique"]],
                row["titre"],
                row["graphique"],
                row["region"],
            ),
        )

    # after updating, we add the constraint
    conn.execute(
        """ALTER TABLE meta.objectifs ADD CONSTRAINT fk_titre_objectif 
        FOREIGN KEY (titre, graphique, region) 
        REFERENCES meta.parametres_objectifs(titre, graphique, region)"""
    )
