﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""titre-graphique-indicateurs-climat

Revision ID: d3c22db67adc
Revises: eee9e289ea97
Create Date: 2022-04-26 10:54:39.713295

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = "d3c22db67adc"
down_revision = "eee9e289ea97"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "alter table meta.indicateur add column titre_graphiques_indicateurs varchar;"
    )


def downgrade():
    op.execute("alter table meta.indicateur drop column titre_graphiques_indicateurs;")
