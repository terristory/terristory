"""empty message

Revision ID: d89bf73c4382
Revises: 05dd969dfb83, fdae0e728839
Create Date: 2023-05-26 12:40:14.364283

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d89bf73c4382"
down_revision = ("05dd969dfb83", "fdae0e728839")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
