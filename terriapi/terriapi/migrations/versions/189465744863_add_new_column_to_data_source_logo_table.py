"""add new column to data_source_logo table

Revision ID: 189465744863
Revises: 667bc2cb4c51
Create Date: 2024-10-24 23:02:27.317056

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "189465744863"
down_revision = "667bc2cb4c51"
branch_labels = None
depends_on = None


SCHEMA = "meta"
TABLENAME = "data_source_logo"


def upgrade():
    op.add_column(TABLENAME, sa.Column("enabled_zone", sa.String), schema=SCHEMA)
    op.add_column(TABLENAME, sa.Column("default_url", sa.String), schema=SCHEMA)


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("enabled_zone")
        batch_op.drop_column("default_url")
