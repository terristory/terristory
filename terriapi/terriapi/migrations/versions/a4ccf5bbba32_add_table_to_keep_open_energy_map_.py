"""Add table to keep proposed open energy map changes

Revision ID: a4ccf5bbba32
Revises: 0aaba7560457
Create Date: 2023-08-24 09:32:48.778782

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a4ccf5bbba32"
down_revision = "0aaba7560457"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "poi_proposed_changes",
        sa.Column("edit_id", sa.Integer, primary_key=True, comment="ID"),
        sa.Column("region", sa.String, comment="Region key"),
        sa.Column("poi_table", sa.String, comment="POI layer key"),
        sa.Column("user_mail", sa.String, comment="User mail"),
        sa.Column("poi_id", sa.Integer, comment="POI ID"),
        sa.Column("old_properties", sa.JSON, comment="New properties values"),
        sa.Column("new_properties", sa.JSON, comment="New properties values"),
        sa.Column("action", sa.String, comment="Action type"),
        sa.Column("state", sa.String, comment="Current state"),
        sa.Column(
            "submitted_date",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=True,
            comment="Date de mise à jour",
        ),
        schema="meta",
    )
    op.execute(
        "ALTER TABLE meta.poi_proposed_changes ADD COLUMN old_geometry geometry(Point,3857)"
    )
    op.execute(
        "ALTER TABLE meta.poi_proposed_changes ADD COLUMN new_geometry geometry(Point,3857)"
    )


def downgrade():
    op.drop_table(schema="meta", table_name="poi_proposed_changes")
