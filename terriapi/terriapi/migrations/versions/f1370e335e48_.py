﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""empty message

Revision ID: f1370e335e48
Revises: 8ee824081b8c, e010b9b4a598, 9dc107ddeaef
Create Date: 2022-11-28 19:34:51.110816

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "f1370e335e48"
down_revision = ("8ee824081b8c", "e010b9b4a598", "9dc107ddeaef")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
