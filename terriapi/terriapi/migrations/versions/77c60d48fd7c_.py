"""empty message

Revision ID: 77c60d48fd7c
Revises: 8f5f96167fb9, d6f401b1201f
Create Date: 2023-11-03 17:33:21.667946

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "77c60d48fd7c"
down_revision = ("8f5f96167fb9", "d6f401b1201f")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
