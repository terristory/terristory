"""empty message

Revision ID: c45a24504046
Revises: ca9c8e6ffda0, 7e8f33e1d42e
Create Date: 2023-02-20 09:26:56.791890

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "c45a24504046"
down_revision = ("ca9c8e6ffda0", "7e8f33e1d42e")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
