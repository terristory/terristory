"""empty message

Revision ID: d0251ff7d5dc
Revises: dfc3659d83a4, 1d2001dcce18
Create Date: 2023-12-01 12:24:38.140164

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "d0251ff7d5dc"
down_revision = ("dfc3659d83a4", "1d2001dcce18")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
