"""Update renewable passage table

Revision ID: 2ef834005cb6
Revises: 853708b99f1f
Create Date: 2023-10-19 15:51:30.297923

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "2ef834005cb6"
down_revision = "853708b99f1f"
branch_labels = None
depends_on = None

TABLE = "strategie_territoire.passage_table"


def upgrade():
    op.execute(
        f"""insert into {TABLE} (region, key, match, association_type) values
        ('pcaet', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_ELEC', 5, 'value'),
        ('pcaet', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_THER', 8, 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_ELEC', 9, 'value'),
        ('auvergne-rhone-alpes', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_THER', 10, 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_ELEC', 6, 'value'),
        ('occitanie', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_THER', 7, 'value'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_ELEC', 9, 'value'),
        ('paysdelaloire', 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_THER', 9, 'value')
        """
    )


def downgrade():
    op.execute(
        f"""delete from {TABLE}
        where key = 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_ELEC'
        or key = 'DataSet.PRODUCTION/RenewableProd.WASTE_VALORIZATION_THER'
        """
    )
