"""empty message

Revision ID: 195854ee1b23
Revises: 5476cd9be7d9, a9e4576dd74b
Create Date: 2023-03-30 14:00:14.278366

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "195854ee1b23"
down_revision = ("5476cd9be7d9", "a9e4576dd74b")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
