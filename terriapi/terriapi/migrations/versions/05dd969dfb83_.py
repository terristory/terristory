"""empty message

Revision ID: 05dd969dfb83
Revises: 12f4726a8fe0, 1e93b140d164
Create Date: 2023-05-12 12:40:51.830295

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "05dd969dfb83"
down_revision = ("12f4726a8fe0", "1e93b140d164")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
