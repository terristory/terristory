"""Better use of passage table

Revision ID: 6fbf1bf3906b
Revises: 7573260f09cc
Create Date: 2023-10-06 16:04:53.550991

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "6fbf1bf3906b"
down_revision = "7573260f09cc"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "UPDATE strategie_territoire.passage_table SET key = REPLACE(key, 'PPAL_TRANS', 'PrincipalMode') WHERE key LIKE 'DataSet.MOBILITY%';"
    )


def downgrade():
    op.execute(
        "UPDATE strategie_territoire.passage_table SET key = REPLACE(key, 'PrincipalMode', 'PPAL_TRANS') WHERE key LIKE 'DataSet.MOBILITY%';"
    )
