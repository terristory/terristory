﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""empty message

Revision ID: a187a356182a
Revises: a6c552c3d1a5, 8dc033aa477b, aed983949a0c
Create Date: 2022-09-28 15:11:41.442003

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "a187a356182a"
down_revision = ("a6c552c3d1a5", "8dc033aa477b", "aed983949a0c")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
