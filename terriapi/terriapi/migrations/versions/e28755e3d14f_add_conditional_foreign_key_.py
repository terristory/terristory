"""add conditional foreign key constraintfor scenario

Revision ID: e28755e3d14f
Revises: 8e4d7960855e
Create Date: 2024-07-08 17:40:27.193804

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e28755e3d14f"
down_revision = "8e4d7960855e"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
            -- Add a new column to indicate the type of data
            ALTER TABLE public.utilisateur_scenario
            ADD COLUMN type character varying;

            -- Update existing rows to set valid values for the type column
            UPDATE public.utilisateur_scenario
            SET type = 'regional';

            -- Add NOT NULL constraint to ensure all rows have a value
            ALTER TABLE public.utilisateur_scenario
            ALTER COLUMN type SET NOT NULL;

            -- Add CHECK constraint to restrict values to 'national' or 'regional'
            ALTER TABLE public.utilisateur_scenario
            ADD CONSTRAINT type_check CHECK (type IN ('national', 'regional'));

            -- Drop the existing foreign key constraint
            ALTER TABLE public.utilisateur_scenario
            DROP CONSTRAINT public_utilisateur_scenario_fkey;

            -- Function to check the national foreign key constraint
            CREATE OR REPLACE FUNCTION check_national_foreign_key()
            RETURNS TRIGGER AS $$
            BEGIN
                IF NEW.type = 'national' THEN
                    PERFORM 1 FROM public.utilisateur WHERE mail = NEW.mail AND territoire = NEW.region;
                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Foreign key violation: record not found in public.utilisateur for (mail, territoire)';
                    END IF;
                END IF;
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;

            -- Function to check the regional foreign key constraint
            CREATE OR REPLACE FUNCTION check_regional_foreign_key()
            RETURNS TRIGGER AS $$
            BEGIN
                IF NEW.type = 'regional' THEN
                    PERFORM 1 FROM public.utilisateur WHERE mail = NEW.mail AND region = NEW.region;
                    IF NOT FOUND THEN
                        RAISE EXCEPTION 'Foreign key violation: record not found in public.utilisateur for (mail, region)';
                    END IF;
                END IF;
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;

            -- Trigger for checking national foreign key
            CREATE TRIGGER trigger_check_national_foreign_key
            BEFORE INSERT OR UPDATE ON public.utilisateur_scenario
            FOR EACH ROW EXECUTE FUNCTION check_national_foreign_key();

            -- Trigger for checking regional foreign key
            CREATE TRIGGER trigger_check_regional_foreign_key
            BEFORE INSERT OR UPDATE ON public.utilisateur_scenario
            FOR EACH ROW EXECUTE FUNCTION check_regional_foreign_key();
        """
    )


def downgrade():
    op.execute(
        """
            -- Step 1: Drop the new column 'type'
            DELETE FROM public.utilisateur_scenario WHERE type='national';

            ALTER TABLE public.utilisateur_scenario
            DROP COLUMN IF EXISTS type;

            -- Step 2: Drop the triggers for checking foreign key constraints
            DROP TRIGGER IF EXISTS trigger_check_national_foreign_key ON public.utilisateur_scenario;
            DROP TRIGGER IF EXISTS trigger_check_regional_foreign_key ON public.utilisateur_scenario;

            -- Step 3: Drop the functions for checking foreign key constraints
            DROP FUNCTION IF EXISTS check_national_foreign_key() CASCADE;
            DROP FUNCTION IF EXISTS check_regional_foreign_key() CASCADE;

            -- Step 4: Restore the original foreign key constraint
            ALTER TABLE public.utilisateur_scenario
            DROP CONSTRAINT IF EXISTS utilisateur_scenario_national_fkey,
            ADD CONSTRAINT public_utilisateur_scenario_fkey FOREIGN KEY (mail, region)
            REFERENCES public.utilisateur (mail, region) MATCH SIMPLE
            ON UPDATE SET NULL
            ON DELETE SET NULL;
    """
    )
