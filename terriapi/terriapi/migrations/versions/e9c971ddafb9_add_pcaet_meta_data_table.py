"""Add PCAET meta-data table

Revision ID: e9c971ddafb9
Revises: 9b6d54bdf62d
Create Date: 2024-01-25 17:57:34.170300

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "e9c971ddafb9"
down_revision = "9b6d54bdf62d"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        """
        CREATE TABLE meta.pcaet_header(
            siren VARCHAR NOT NULL,
            statut VARCHAR,
            date_lancement VARCHAR,
            date_modification VARCHAR,
            oblige BOOL
        );
        ALTER TABLE meta.pcaet_header ADD CONSTRAINT pcaet_header_pkey PRIMARY KEY (siren);
        """
    )


def downgrade():
    op.execute("DROP TABLE IF EXISTS meta.pcaet_header;")
