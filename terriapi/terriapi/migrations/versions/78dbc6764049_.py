"""empty message

Revision ID: 78dbc6764049
Revises: e9c971ddafb9, 82ba6d55b07f
Create Date: 2024-02-09 09:54:42.507873

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "78dbc6764049"
down_revision = ("e9c971ddafb9", "82ba6d55b07f")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
