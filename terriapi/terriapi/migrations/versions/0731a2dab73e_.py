"""empty message

Revision ID: 0731a2dab73e
Revises: 70c701d5d498, bd3eeecb8ffd
Create Date: 2023-11-27 11:12:45.951971

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "0731a2dab73e"
down_revision = ("70c701d5d498", "bd3eeecb8ffd")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
