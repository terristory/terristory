"""Change confid maille structure to jsonb column

Revision ID: 7e95c851455c
Revises: ac3ed710f881
Create Date: 2024-05-24 17:51:33.406650

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "7e95c851455c"
down_revision = "ac3ed710f881"
branch_labels = None
depends_on = None

regions = [
    "auvergne_rhone_alpes",
    "occitanie",
    "paysdelaloire",
    "bretagne",
    "nouvelle_aquitaine",
    "corse",
]


def upgrade():
    for region in regions:
        op.execute(
            f"""
            ALTER TABLE {region}.confid_maille ADD COLUMN conditions jsonb;
            UPDATE {region}.confid_maille SET conditions = CONCAT('{{"secteur":"', secteur, '", "energie": "', energie, '"}}')::jsonb;
            ALTER TABLE {region}.confid_maille DROP COLUMN energie;
            ALTER TABLE {region}.confid_maille DROP COLUMN secteur;
            ALTER TABLE {region}.confid_maille DROP COLUMN energie_column;
            ALTER TABLE {region}.confid_maille DROP COLUMN secteur_column;
            """
        )


def downgrade():
    for region in regions:
        op.execute(
            f"""
            ALTER TABLE {region}.confid_maille ADD COLUMN energie varchar;
            ALTER TABLE {region}.confid_maille ADD COLUMN secteur varchar;
            ALTER TABLE {region}.confid_maille ADD COLUMN energie_column varchar;
            ALTER TABLE {region}.confid_maille ADD COLUMN secteur_column varchar;
            UPDATE {region}.confid_maille SET energie_column = 'energie', energie = conditions->>'energie', secteur_column = 'secteur', secteur = conditions->>'secteur';
            ALTER TABLE {region}.confid_maille DROP COLUMN conditions;
            """
        )
