"""Add default value for zone

Revision ID: 609fcee22c15
Revises: 49b7a84cebd4
Create Date: 2023-09-04 08:44:22.821407

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "609fcee22c15"
down_revision = "49b7a84cebd4"
branch_labels = None
depends_on = None

schemas = [
    "auvergne_rhone_alpes",
    "occitanie",
    "nouvelle_aquitaine",
    "bretagne",
    "paysdelaloire",
    "corse",
]


def upgrade():
    for schema in schemas:
        op.execute(f"UPDATE {schema}.zone SET hide = false WHERE hide IS NULL;")


def downgrade():
    pass
