# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add external API tables

Revision ID: fdae0e728839
Revises: 375e3c6b2cf7
Create Date: 2023-04-20 10:27:42.801100

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "fdae0e728839"
down_revision = "375e3c6b2cf7"
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "external_api",
        sa.Column("region", sa.String, nullable=False, comment="Region name"),
        sa.Column("name", sa.String, nullable=False, comment="External API name"),
        sa.Column("slug", sa.String, nullable=False, comment="Slug"),
        sa.Column("actor", sa.String, comment="Actor"),
        sa.Column("main_url", sa.String, comment="Root URL"),
        sa.Column(
            "api_key_parameter",
            sa.String,
            comment="Authentication key parameter",
        ),
        sa.Column("key", sa.Text, comment="Authentification key"),
        sa.Column("format", sa.String, comment="Response format"),
        sa.Column("method", sa.String, comment="GET or POST query"),
        sa.Column("url_suffix", sa.String, comment="URL entrypoint"),
        sa.Column("table_name", sa.String, comment="Local table name"),
        sa.Column(
            "perimeter_year", sa.Integer, comment="Local geographical perimeter year"
        ),
        sa.Column(
            "details_column",
            sa.String,
            comment="Column name to give details about each row in details popup",
        ),
        sa.Column("geographical_column", sa.String, comment="Geographical column name"),
        sa.Column("post_data", sa.JSON, comment="POST data"),
        sa.Column("columns", sa.JSON, comment="Columns mapping"),
        sa.Column("filters", sa.JSON, comment="Filters configuration"),
        sa.Column(
            "date_maj",
            sa.DateTime,
            server_default=sa.func.current_timestamp(),
            nullable=False,
            comment="Last update",
        ),
        sa.UniqueConstraint("region", "slug"),
        schema="meta",
    )


def downgrade():
    op.drop_table(table_name="external_api", schema="meta")
