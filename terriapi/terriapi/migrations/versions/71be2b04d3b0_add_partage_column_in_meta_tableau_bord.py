﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Add partage column in meta.tableau_bord

Revision ID: 71be2b04d3b0
Revises: 9f5b0900b0db
Create Date: 2022-12-21 15:17:15.141604

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "71be2b04d3b0"
down_revision = "9f5b0900b0db"
branch_labels = None
depends_on = None


def upgrade():
    op.execute("alter table meta.tableau_bord add column partage text[];")


def downgrade():
    op.execute("alter table meta.tableau_bord drop column partage;")
