﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""modifier-type-colonne-donnees-exportables

Revision ID: 23361f3c38c9
Revises: 3f5a34a2782c
Create Date: 2021-09-03 17:00:13.025941

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "23361f3c38c9"
down_revision = "3f5a34a2782c"
branch_labels = None
depends_on = None


def upgrade():
    op.execute(
        "alter table meta.indicateur alter column donnees_exportables type varchar;"
    )


def downgrade():
    op.execute("alter table meta.indicateur drop column donnees_exportables;")
    op.execute("alter table meta.indicateur add column donnees_exportables boolean;")
