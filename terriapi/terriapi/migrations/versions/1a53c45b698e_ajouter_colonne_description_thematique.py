﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""ajouter colonne description thematique

Revision ID: 1a53c45b698e
Revises: b1c5838bac03
Create Date: 2021-01-25 10:44:52.128275

"""
from alembic import op
from sqlalchemy import Column, String

# revision identifiers, used by Alembic.
revision = "1a53c45b698e"
down_revision = "b1c5838bac03"
branch_labels = None
depends_on = None

SCHEMA = "meta"
TABLENAME = "tableau_thematique"


def upgrade():
    op.add_column(TABLENAME, Column("description", String()), schema=SCHEMA)


def downgrade():
    with op.batch_alter_table(TABLENAME, schema=SCHEMA) as batch_op:
        batch_op.drop_column("description")
