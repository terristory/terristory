"""empty message

Revision ID: dfc3659d83a4
Revises: ad49acaa34ce, 0731a2dab73e
Create Date: 2023-11-30 09:41:38.872829

"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "dfc3659d83a4"
down_revision = ("ad49acaa34ce", "0731a2dab73e")
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
