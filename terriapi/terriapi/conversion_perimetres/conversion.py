﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from datetime import datetime
from pathlib import Path
from time import gmtime, strftime

import asyncpg
import numpy as np
import pandas as pd
import requests
from psycopg2.extensions import AsIs

from terriapi import controller

here = Path(__file__).parent


async def changement_cog_varnum(
    data, annees, code, agregation=True, extra=None, libgeo=False, donnees_insee=True
):
    """
    :param data: Table que l'on veut convertir à la geographie officielle du 1er janvier d'une année donnée
    :param annees: liste des années qui séparent le code officiel géographique de départ et d'arrivée
    :param agregation: vaut TRUE si la table souhaitée doit sommer toutes les lignes qui concernent une même commune
    :param libgeo: TRUE si l'on veut rajouter dans la table une colonne nommée "nom_commune"...
    :param donnees_insee: TRUE si les données manipulées sont produites par l'Insee
    :return: Renvoie la table finale à la geographie officielle souhaitée

    Voir https://github.com/antuki/COGugaison/blob/master/R/changement_COG_varNum.R
    """

    annees_possibles = [1968, 1975, 1982, 1990, 1999] + list(
        range(2008, int(strftime("%Y", gmtime())) + 1)
    )

    annee_ref = int(strftime("%Y", gmtime()))

    var_num = list(data._get_numeric_data())

    if np.logical_not(code in data.columns):
        raise ValueError("code doit être une colonne de data.")

    assert len(annees) > 0, "La liste des années possibles est vide"
    if any(a > annee_ref for a in annees) | any(a < 1968 for a in annees):
        raise ValueError(
            "annees ne doit contenir que des années comprises entre 1968 et "
            + str(annee_ref)
            + "."
        )

    if any(var not in data.columns.tolist() for var in var_num):
        raise ValueError(
            "var_num doit être un vecteur de colonne(s) de type numérique de data."
        )

    inter = list(set(annees_possibles) & set(annees))

    if annees[0] <= annees[-1]:
        inter = sorted(inter)
        annees = sorted(
            list(
                set(
                    list(range(annees[0], inter[0]))
                    + inter
                    + list(range(inter[-1], annees[-1]))
                )
            )
        )

    else:
        inter = sorted(inter, reverse=True)
        annees = sorted(
            list(
                set(
                    list(range(annees[0], inter[0]))
                    + inter
                    + list(range(inter[-1], annees[-1]))
                )
            ),
            reverse=True,
        )

    dic = {}

    dic_cog = {}

    if len(annees) == 1:
        return data
    for i, val in enumerate(annees[:-1]):
        if donnees_insee == True and len(annees) != 1:
            req = """
                  select *
                  from table_passage.passage_{date_depart}_{date_arrivee};
                  """.format(
                date_depart=annees[i], date_arrivee=annees[i + 1]
            )

            rep = await controller.fetch(req)
            if len(rep) > 0:
                resultats = [dict(x) for x in rep]
                df_passage = pd.DataFrame(resultats)
                df_passage["ratio"] = df_passage["ratio"].astype(np.float64)
                dtype = {"cod" + str(annees[i]): str, "cod" + str(annees[i + 1]): str}
                dic.update({f"PASSAGE_{annees[i]}_{annees[i + 1]}": df_passage})
        if (
            len(annees) == 1
            or len(dic.get(f"PASSAGE_{annees[i]}_{annees[i + 1]}", [])) == 0
        ):
            table_finale = data
        else:
            provisoire = pd.merge(
                data,
                dic.get(f"PASSAGE_{annees[i]}_{annees[i + 1]}"),
                left_on=code,
                right_on=("cod" + str(annees[i])),
                how="outer",
                indicator=True,
            )

            provisoire = provisoire[provisoire["_merge"] != "right_only"]

            provisoire.drop(["cod" + str(annees[i]), "_merge"], axis=1, inplace=True)

            provisoire.loc[:, "ratio"] = np.where(
                provisoire["cod" + str(annees[i + 1])].isnull(), 1, provisoire["ratio"]
            )

            provisoire.loc[:, "cod" + str(annees[i + 1])] = np.where(
                provisoire["cod" + str(annees[i + 1])].isnull(),
                provisoire[code].astype(str),
                provisoire.loc[:, "cod" + str(annees[i + 1])],
            )

            provisoire[var_num] = (
                provisoire[var_num + ["ratio"]].multiply(
                    provisoire["ratio"], axis="index"
                )
            ).drop("ratio", axis=1)

            provisoire = provisoire.drop(code, axis=1)

            provisoire.rename({"cod" + str(annees[i + 1]): code}, axis=1, inplace=True)

            table_finale = provisoire[list(data)]

        data = table_finale
        if agregation:
            l = [i for i in list(table_finale.keys()) if i != "valeur"]
            table_finale = table_finale.groupby(l).sum().reset_index()
        if libgeo:
            if donnees_insee:
                dic_cog.update(
                    {
                        f"COG_{annees[-1]}": pd.read_csv(
                            path + f"COG_{annees[-1]}_insee.csv", sep=",", index_col=0
                        )
                    }
                )

                table_finale = pd.merge(
                    table_finale,
                    dic_cog[f"COG_{annees[-1]}"].iloc[:, [0, 1]],
                    left_on=code,
                    right_on="CODGEO",
                    how="outer",
                    indicator=True,
                )

                table_finale = table_finale[table_finale["_merge"] != "right_only"]

                table_finale.drop("_merge", axis=1, inplace=True)

                cols = table_finale.columns.tolist()

                cols = [cols[0]] + [cols[-1]] + list(range(2, len(cols)))

                table_finale = table_finale[cols]

                table_finale.rename(
                    columns={table_finale.columns[1]: "nom_commune"}, inplace=True
                )

                table_finale = table_finale.sort_values(code)
    for colonne in list(table_finale.keys()):
        if "valeur" not in colonne:
            if table_finale[colonne].dtype == np.float64:
                table_finale[colonne] = table_finale[colonne].round().astype("Int64")
    if "an" in list(table_finale.keys()):
        table_finale["annee"] = table_finale["an"]
    return table_finale


async def get_perimeter_conversion_table(
    initial_year, target_year, schema="table_passage"
):
    """Gets the perimeter conversion table for initial_year => target_year from the database.

    Parameters
    ----------
    initial_year : int
        Initial year
    target_year : int
        Target year

    Returns
    -------
    DataFrame
        Conversion table with three columns : ["cod<initial_year>", "cod<target_year>", "ratio"]
    """
    if abs(target_year - initial_year) != 1:
        raise ValueError("Target year and initial year should be one year apart")

    req = f"""
            select cod{initial_year}, cod{target_year}, ratio
            from {schema}.passage_{initial_year}_{target_year};
            """
    try:
        rset = await controller.fetch(req)
    except asyncpg.exceptions.UndefinedTableError as e:
        raise asyncpg.exceptions.UndefinedTableError(
            f"La table de passage {initial_year} => {target_year} n'existe pas et doit être ajoutée à la base de donnée"
        )

    # convert table to DataFrame
    df_conversion_table = pd.DataFrame([dict(x) for x in rset])
    # if the conversion table exists but is empty (technically possible if no perimeter changes this year) create an empty df with the proper columns
    if not rset:
        df_conversion_table = df = pd.DataFrame(
            columns=[f"cod{initial_year}", f"cod{target_year}", "ratio"]
        )
    # convert ratio column to float
    df_conversion_table["ratio"] = df_conversion_table["ratio"].astype(float)
    return df_conversion_table


async def convert_df_to_perimeter(
    data, initial_year, target_year, value_cols, commune_cols, aggregation_function
):
    """Converts a data table to the geographical perimeter of a specified year.
    Based on the conversion tables and the function changement_COG_varNum() of the R package COGugaison (https://antuki.github.io/COGugaison/)

    Parameters
    ----------
    data : DataFrame
        The table to convert.
    initial_year : int
        The year of the initial perimeter of the data table
    target_year : int
        The year of the perimeter in which to convert the data
    value_cols : List[str]
        The names of the numerical columns that should be converted (aggregated after being multiplied by the ratio if the aggregation function is "sum")
    commune_cols : List[str]
        The names of the columns that contain the commune code. (usually "commune", but also "commune_dest" in the case of flow data)
    code_col : str
        The name of the column that contains the INSEE commune code
    aggregation_function : function, str, list or dict
        Function to use for aggregating the data. Must work with pandas.DataFrame.agg().
        The value columns will be multiplied by the ratio only if the aggregation function is "sum".


    Returns
    -------
    DataFrame
        Data table converted in the target_year perimeter
    """
    # conversion tables are available starting from 2008
    min_year = 2008
    current_year = datetime.now().year

    if not (
        min_year <= initial_year <= current_year
        and min_year <= target_year <= current_year
    ):
        raise ValueError(
            f"La date initiale et la date finale doivent être comprises entre 2008 et {current_year}"
        )

    if any(col not in data.columns for col in value_cols):
        raise ValueError(
            "La colonne de valeur n'a pas été trouvée dans la table de données. Vérifiez que la table contient bien une colonne 'valeur'."
        )

    if any(col not in data.columns for col in commune_cols):
        raise ValueError(
            "La colonne de commune n'a pas été trouvée dans la table de données. Vérifiez que la table contient bien une colonne 'commune'."
        )

    # Define a step based on whether we are converting forward or backward in time.
    step = 1 if initial_year <= target_year else -1
    years = range(initial_year, target_year, step)

    if len(years) == 0:
        return data

    df_converted = data
    for year in years:
        # get the conversion table from database
        df_conversion_table = await get_perimeter_conversion_table(year, year + step)
        for code_col in commune_cols:
            df_converted = pd.merge(
                df_converted,
                df_conversion_table,
                left_on=code_col,
                right_on="cod" + str(year),
                how="left",
            )
            # communes with no changes have a ratio of one
            df_converted["ratio"] = df_converted["ratio"].fillna(1)
            # communes with no changes keep their code
            df_converted["cod" + str(year + step)] = df_converted[
                "cod" + str(year + step)
            ].fillna(df_converted[code_col])
            # If the aggregation function is "sum", the values in the case of a commune split can be approximated using the population ratio
            if aggregation_function == "sum":
                df_converted[value_cols] = df_converted[value_cols].multiply(
                    df_converted["ratio"], axis=0
                )
            # the column with the commune code of the target year becomes the main column code
            df_converted = df_converted.drop(code_col, axis=1).rename(
                columns={"cod" + str(year + step): code_col}
            )
            # keep only columns from the original data
            df_converted = df_converted[data.columns]

        # aggregate the rows by all columns except the value columns using the specified aggregation function
        df_converted = (
            df_converted.groupby(
                df_converted.columns.difference(value_cols).tolist(), dropna=False
            )
            .agg(aggregation_function)
            .reset_index()
        )

    # reorder columns
    df_converted = df_converted[data.columns]
    return df_converted
