import csv
import datetime
import json

import pandas as pd
import requests
import slugify

from terriapi import controller, utils
from terriapi.controller import zone as controller_zone


async def get_external_api_list(region):
    """
    Retrieve external API list from database.

    Parameters
    ----------
    region : str
        region name

    Returns
    -------
    list
        all elements as dicts
    """
    api_list = await controller.fetch(
        "SELECT * FROM meta.external_api WHERE region = $1", region
    )

    output = []
    for api in api_list:
        api = dict(api)
        api["columns"] = json.loads(api["columns"])
        api["filters"] = json.loads(api["filters"])
        api["post_data"] = json.loads(api["post_data"])
        api["date_maj"] = api["date_maj"].strftime("%Y-%m-%d")
        output.append(api)
    return output


async def get_external_api_config(region, key):
    """
    Get external API config from database based on the slug provided.

    Parameters
    ----------
    region : str
        region name
    key : str
        slug of the external API element

    Returns
    -------
    dict
        external API config
    """
    api_data = await get_external_api_list(region)
    return next((api for api in api_data if api["slug"] == key), None)


async def update_meta_data(region, api_id, new_data, default_data):
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            meta_data = f"""
                UPDATE meta.external_api
                SET name = $3,
                actor = $4,
                url = $5,
                api_key_parameter = $6,
                key = $7,
                    format = $8,
                    method = $9,
                    table_name = $10,
                    perimeter_year = $11,
                    details_column = $12,
                    geographical_column = $13,
                    post_data = $14,
                    columns = $15,
                    filters = $16,
                    subkeys = $17,
                    fixed_year = $18,
                    date_maj = NOW()
                WHERE region = $1 AND slug = $2
            """
            table_name = utils.parse_table_name(
                new_data.get("table_name", default_data["table_name"])
            )

            await conn.execute(
                meta_data,
                region,
                api_id,
                new_data["name"],
                new_data.get("actor", default_data["actor"]),
                new_data.get("url", default_data["url"]),
                new_data.get("api_key_parameter", default_data["api_key_parameter"]),
                # for secret key, we either use new one or default as empty field
                # means no modification was performed
                new_data.get("key", None) or default_data["key"],
                new_data.get("format", default_data["format"]),
                new_data.get("method", default_data["method"]),
                table_name,
                int(new_data.get("perimeter_year", default_data["perimeter_year"])),
                new_data.get("details_column", default_data["details_column"]),
                new_data.get(
                    "geographical_column", default_data["geographical_column"]
                ),
                json.dumps(new_data.get("post_data", default_data["post_data"])),
                json.dumps(new_data.get("columns", default_data["columns"])),
                json.dumps(new_data.get("filters", default_data["filters"])),
                new_data.get("subkeys", default_data["subkeys"]),
                new_data.get("fixed_year", default_data["fixed_year"]),
            )


async def add_new_api(region, new_data):
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            meta_data = f"""
                INSERT INTO meta.external_api (region, name, slug, actor, url, api_key_parameter, key, 
                    format, method, table_name, perimeter_year, details_column, geographical_column,
                    post_data, columns, filters, subkeys, fixed_year)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18)
            """

            today = datetime.date.today()
            table_name = utils.parse_table_name(new_data["table_name"])

            await conn.execute(
                meta_data,
                region,
                new_data["name"],
                slugify.slugify(new_data["name"]),
                new_data.get("actor", ""),
                new_data["url"],
                new_data.get("api_key_parameter", ""),
                new_data.get("key", ""),
                new_data.get("format", ""),
                new_data.get("method", ""),
                table_name,
                int(new_data.get("perimeter_year", today.year)),
                new_data.get("details_column", ""),
                new_data.get("geographical_column", ""),
                json.dumps(new_data.get("post_data", {})),
                json.dumps(new_data.get("columns", {})),
                json.dumps(new_data.get("filters", {})),
                new_data.get("subkeys", ""),
                new_data.get("fixed_year", ""),
            )


async def delete_api(region, api_id):
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.execute(
                f"""DELETE FROM meta.external_api WHERE slug = $1 AND region = $2""",
                api_id,
                region,
            )


async def call_external_api(api_config):
    """
    Call external API based on the config provided. Current methods allowed:

    * GET
    * POST

    Parameters
    ----------
    api_config : dict
        external API config

    Returns
    -------
    list
        element that can be used to create a pandas DataFrame
    """
    url = api_config["url"]

    if api_config["method"] == "GET":
        return await call_external_GET_api(url, api_config)
    elif api_config["method"] == "POST":
        return await call_external_POST_api(url, api_config)
    else:
        raise ValueError("La méthode de requête n'est pas autorisée (GET ou POST).")


async def handle_response(response, api_config):
    """
    Handle response depending on format. Current formats allowed:

    * application/json
    * text/csv

    Parameters
    ----------
    response : Response
        the response given by the API call
    api_config : dict
        external API config from database

    Returns
    -------
    list or dict
        data from json or csv
    """
    if response.status_code == 200:
        if api_config.get("format", "json") == "json":
            return response.json()
        elif api_config.get("format", "csv") == "csv":
            content = csv.DictReader(
                response.content.decode("utf-8").splitlines(), delimiter=";"
            )
            return list(content)
        else:
            raise ValueError(
                "Le format de sortie n'est pas autorisé (application/json ou text/csv)."
            )
    else:
        raise ValueError("La requête a échoué !")


def get_query_headers(api_config):
    """
    Create the HTTP headers to send alongside the query. Currently, only API key
    parameter is used to provide authentication. Each external API has its own
    parameter and can send its key in the header.

    Parameters
    ----------
    api_config : dict
        external API config from database

    Returns
    -------
    dict
        HTTP headers
    """
    api_key_parameter = api_config.get("api_key_parameter", "apikey")
    if api_key_parameter is None:
        api_key_parameter = "apikey"
    key = api_config.get("key", "")
    if key == "":
        headers = {}
    else:
        headers = {api_key_parameter: key}
    return headers


async def call_external_GET_api(url, api_config):
    headers = get_query_headers(api_config)
    response = requests.get(url, headers=headers)
    return await handle_response(response, api_config)


async def call_external_POST_api(url, api_config):
    headers = get_query_headers(api_config)
    response = requests.post(url, data=api_config.get("post_data", {}), headers=headers)
    return await handle_response(response, api_config)


async def import_data(region, api_config, response):
    """
    Import data from external API response to local database.

    This function can fail in the following cases:

    * no destination table name provided
    * geographical column is not valid (only epci or commune currently allowed)
    * columns are not valid (must be annee, valid geography, valeur)

    Parameters
    ----------
    region : str
        region name
    api_config : dict
        external API config from database
    response : list or dict
        parsed elements from external API response
    """
    schema = region.replace("-", "_")

    dest_table = api_config.get("table_name", None)
    if dest_table is None or dest_table == "":
        raise ValueError("Aucune table de donnée définie pour cette API")

    geographical_column = api_config.get("geographical_column", "commune")

    valid_geographical_columns = await controller_zone.all_territorial_levels(region)
    # TODO: improve here to allow more geographical columns?
    if geographical_column not in valid_geographical_columns:
        raise ValueError("L'échelle géographique n'est pas valide.")

    defaults_columns = {"commune": "commune", "valeur": "valeur", "annee": "annee"}
    columns = api_config.get("columns", defaults_columns)
    if set(columns.values()) != {"annee", geographical_column, "valeur"}:
        raise ValueError(
            "Les colonnes doivent être dans : valeur, annee, " + geographical_column
        )

    subkeys = api_config.get("subkeys", "")
    if subkeys:
        subkeys = subkeys.split(",")
        for subkey in subkeys:
            response = response[subkey]

    fixed_year = api_config.get("fixed_year", "")

    filters = api_config.get("filters", {})
    df = pd.DataFrame(response)
    for key, value in filters.items():
        df = df.loc[df[key].astype(str) == value]

    converted_df = df[[*columns.keys()]].rename(columns=columns)
    if fixed_year:
        converted_df["annee"] = fixed_year
    converted_df = converted_df[[geographical_column, "valeur", "annee"]]
    valid_codes = await controller_zone.all_territories_codes_for_type(
        region, geographical_column
    )
    converted_df = converted_df.loc[converted_df[geographical_column].isin(valid_codes)]
    converted_df["annee"] = converted_df["annee"].astype(int)
    converted_df["valeur"] = pd.to_numeric(converted_df["valeur"])
    converted_df = converted_df.dropna(subset=["valeur"])

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.execute(f"DROP TABLE IF EXISTS {schema}.{dest_table}")

            await conn.execute(
                f"""CREATE TABLE {schema}.{dest_table}
                    (
                        {geographical_column} varchar(255),
                        valeur double precision,
                        annee integer
                    )
                """
            )

            # we convert our dataframe to usable data for copy_records function
            tuples = [tuple(x) for x in converted_df.values]
            await conn.copy_records_to_table(
                dest_table,
                records=tuples,
                columns=list(converted_df.columns),
                schema_name=schema,
            )

            perimeter_year = api_config.get("perimeter_year", 2022)
            await conn.execute(
                f"DELETE FROM meta.perimetre_geographique WHERE region = $1 AND nom_table = $2",
                region,
                dest_table,
            )
            await conn.execute(
                f"INSERT INTO meta.perimetre_geographique VALUES($1, $2, $3, $4)",
                dest_table,
                region,
                int(perimeter_year),
                False,
            )

    return {
        "status": "success",
        "message": "Les données ont bien été importées depuis l'API externe "
        + api_config.get("name", "inconnue")
        + ".",
    }
