﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Fonctions et requêtes SQL pour l'impact énergie des actions
"""

import pandas as pd
from openpyxl import load_workbook

from terriapi import controller, here
from terriapi.controller.strategy_actions import (
    DataKeys,
    DataSet,
    RenewableProd,
    Sector,
)
from terriapi.controller.strategy_actions.actions_dispatcher import ActionsDispatcher
from terriapi.controller.strategy_actions.impacts import Impacts
from terriapi.controller.strategy_actions.loaders import (
    DefaultInputsLoader,
    InputsLoader,
    actions_data,
    data_loaders,
)
from terriapi.controller.strategy_actions.loaders.specific_loaders import (
    heating_systems_distribution,
)
from terriapi.controller.strategy_actions.passage_table import PassageTable
from terriapi.controller.strategy_actions.tests.testing import ActionsTester

EXCEL_ADEME_TEMPLATE = here / "ressources" / "action_PCAET_ADEME_vide.xlsx"

# no. action -> (id secteur, nom secteur)
# 1 : Rénovation résidentielle -> secteur "Résidentiel"
# 2 : Rénovation tertiaire -> secteur "Tertiaire"
# 7 : Voies cyclables catégories "Actions mobilités durables" -> secteur "Transport" (transports routiers)

# no. action -> (id type prod enr, nom de l'énergie renouvelable)
# 3  : Centrale photovoltaïque au sol
# 3b : Eolien
# 4  : Installation solaire thermique Eau Chaude Sanitaire sur logement collectif
# 4b : Installation solaire thermique Combinée (Chauffage et Eau Chaude Sanitaire)
# 5  : Réseau de chaleur EnR/gaz
# 6  : Méthanisation en cogénération
# 6b : Méthanisation avec injection sur le réseau de gaz
# 10a : Photovoltaïque sur toiture (résidentiel, petite toiture)
# 10b : Photovoltaïque sur toiture (tertiaire, grande toiture)
# 10c : Photovoltaïque sur toiture (ombrières de parking)


def override_user_params(csv_params, advanced, aname, anum):
    """Surchage les paramètres utilisateurs (CSV) par les données
    entrées dans l'interface

    Parameters
    ----------
    advanced: obj
        dict issu d'une requête POST avec les valeurs utilisateurs
    aname: str
        nom du tableau des paramètres avancés
    anum: str
        numéro de l'action

    Returns
    -------
    dict
        dictionnaire contenant une seule clé contenant le tableau avec les nouvelles
        valeurs ou un dictionnaire vide le cas échéant.
    """
    if anum not in advanced or aname not in advanced[anum]:
        return {}
    user_values = [value for (key, value) in advanced[anum][aname].items()]
    if user_values:
        # surchage des tableaux modifiables par les utilisateurs
        user_action_params = csv_params["user_action_params"]
        return {aname: user_action_params[anum][aname].assign(valeur=user_values)}
    return {}


async def diagnostic_polluants_atmospheriques(region, zone, zone_id):
    l_pollutants = [
        DataSet.POLLUTANT_PM10,
        DataSet.POLLUTANT_PM25,
        DataSet.POLLUTANT_NOX,
        DataSet.POLLUTANT_SOX,
        DataSet.POLLUTANT_COVNM,
        DataSet.POLLUTANT_NH3,
    ]

    passage_table = PassageTable(region, "Pollutants table")
    await passage_table.load(l_pollutants)
    if passage_table.empty:
        return None
    loader = data_loaders.SingleDataLoader(region)
    # À faire : Rendre cette liste paramétrable (en la passant en argument par exemple)

    historique_pollutants_air = {}
    for pollutant in l_pollutants:
        pollutant_table = passage_table.convert_table_key(pollutant)
        if not pollutant_table:
            continue
        pollutant_sector_col = passage_table.convert_column_key(Sector, pollutant)
        await loader.load(
            pollutant_table, zone, zone_id, [pollutant_sector_col], all_years=True
        )
        data = loader.data
        historic_data = passage_table.convert_data(pollutant, data)
        historic_data["confidentiel"] = "non"
        historique_pollutants_air[str(pollutant)] = historic_data
    if len(historique_pollutants_air) == 0:
        return None
    return {"polluants_atmospheriques": historique_pollutants_air}


def export_excel_polluants_atmospheriques(polluants, wb):
    """Remplit un fichier Excel au format ADEME sur les polluants atmosphériques.

    Parameters
    ----------
    polluants : dict
        Données sur les polluants atmosphériques par secteur pour chaque type de polluant.
    wb : openpyxl.workbook.Workbook
    """
    sheet_name_polluants = wb.sheetnames[5]
    sheet_polluants = wb[sheet_name_polluants]
    polluants_atmospheriques = polluants["polluants_atmospheriques"]

    ligne_polluants_atmospheriques_secteur = {
        "RESIDENTIAL": "7",
        "SERVICES": "8",
        "ROAD_TRANSPORT": "9",
        "OTHER_TRANSPORT": "10",
        "AGRICULTURE": "11",
        "WASTE_MANAGEMENT": "12",
        "INDUSTRY_WITHOUT_ENERGY": "13",
        "ENERGY_INDUSTRY": "14",
    }

    col_polluants_atmospheriques = {
        "POLLUTANT_PM10": "C",
        "POLLUTANT_PM25": "D",
        "POLLUTANT_NOX": "E",
        "POLLUTANT_SOX": "F",
        "POLLUTANT_COVNM": "G",
        "POLLUTANT_NH3": "H",
    }

    for nom_polluant, col_polluants in col_polluants_atmospheriques.items():
        pollutant_key = DataSet[nom_polluant]
        current_data = polluants_atmospheriques[str(pollutant_key)]

        polluants_filtres = {
            Sector[nom]: ligne
            for nom, ligne in ligne_polluants_atmospheriques_secteur.items()
            if Sector[nom] in current_data["SECTOR"].unique()
        }
        for nom_secteur, ligne_secteur in polluants_filtres.items():
            data_polluants = pd.DataFrame(
                current_data.loc[current_data["SECTOR"] == nom_secteur]
            )
            annee_polluant_max = data_polluants["annee"].max()
            data_polluants = data_polluants.set_index("annee")
            sheet_polluants[col_polluants + ligne_secteur] = (
                data_polluants["valeur"].loc[annee_polluant_max].sum()
            )
            sheet_polluants[col_polluants + "16"] = annee_polluant_max


async def export_excel_conso_ges_enr(region, impact_energie, wb, ges_export_excel):
    """Remplit un fichier Excel au format ADEME pour la conso énergie, émission GES et prod EnR
    à partir des résultats des actions : conso énergie, émission GES, etc.

    Parameters
    ----------
    impact_energie : dict
        Impacts sur la production des EnR causés par la saisie de paramètres dans les actions de production d'énergie renouvelable
    wb : openpyxl.workbook.Workbook
    """

    # 2e feuille de calcul : "1.GES et Conso énergie"
    sheetname = wb.sheetnames[1]
    sheet = wb[sheetname]
    # 3e feuille 3.ENR
    sheet_name_prod = wb.sheetnames[3]
    sheet_prod = wb[sheet_name_prod]

    # conso energie par secteur
    conso_energie = impact_energie["energie_economisee"]["secteur"]
    # émission GES par secteur
    emission_ges = impact_energie["emission_ges"]["secteur"]
    # secteur par indice
    # polluants atmospheriques par secteur
    energie_produite = impact_energie["energie_produite"]["energie"]

    col_enr = "D"
    col_an = "E"

    # À rendre paramétrable
    ligne_prod_enr = {
        "WIND": "5",
        "SOLAR_PV": "6",
        "WASTE_VALORIZATION_ELEC": "9",
        "BIOMASS_ELEC": "9",
        "SOLAR_THER": "15",
        "SOLAR_THERMODYNAMIC": "7",
        "HYDRO_LOW": "8",
        "HYDRO_HIGH": "8",
        "BIOGAS_ELEC": "10",
        "WASTE_VALORIZATION_THER": "12",
        "BIOMASS_THER_COGENERATION": "12",
        "BIOMASS_THER_DOMESTIC": "12",
        "BIOMASS_THER_CHAUFFERIE": "12",
        "HEAT_PUMP": "13",
        "BIOGAS_THER": "16",
        "GEOTHERMY_ELEC": "11",
        "GEOTHERMY_THER": "14",
        "BIOGAS_INJEC": "17",
    }

    # À rendre paramétrable
    ligne_prod_enr_obj = {
        "WIND": "22",
        "SOLAR_PV": "23",
        "SOLAR_THER": "32",
        "SOLAR_THERMODYNAMIC": "24",
        "HYDRO_LOW": "25",
        "HYDRO_HIGH": "25",
        "WASTE_VALORIZATION_ELEC": "26",
        "BIOMASS_ELEC": "26",
        "BIOGAS_ELEC": "27",
        "BIOGAS_THER": "33",
        "WASTE_VALORIZATION_THER": "29",
        "BIOMASS_THER_COGENERATION": "29",
        "BIOMASS_THER_DOMESTIC": "29",
        "BIOMASS_THER_CHAUFFERIE": "29",
        "HEAT_PUMP": "30",
        "GEOTHERMY_ELEC": "28",
        "GEOTHERMY_THER": "31",
        "BIOGAS_INJEC": "34",
    }
    #  TODO : Rendre générique l'année de comptabilisation qui pourrait
    #  peut-être varier en fonction du polluant dont il s'agit.

    annee_prod = impact_energie["energie_produite"]["max_annee"]

    liste_annee_prod = {"2026": "D", "2030": "E", "2050": "F"}
    liste_annee_conso = {"2026": "G", "2030": "H", "2050": "I"}

    # TODO(dag): découper en deux fonctions : (1) diagnostic et (2) objectifs ?
    # TODO(dag): découper en plusieurs fonctions : une fonction par feuille de calcul ?
    # TODO(dag): Simplifier le code en supprimant la variable enr_filtree qui n'a plus de
    # sens depuis la mise à jour de données prod EnR.
    # TODO(dag): Ajouter un test pour être certain que tous les types d'EnR figurent
    # dans l'historique de chaque territoire.
    valeur_hydro = 0
    valeur_biomasse_thermique = 0
    valeur_biomasse_electrique = 0

    enr_filtrees = {
        RenewableProd[nom]: ligne
        for nom, ligne in ligne_prod_enr.items()
        if RenewableProd[nom] in energie_produite["RENEWABLEPROD"].unique()
    }

    for nom, ligne in enr_filtrees.items():
        data_prod = pd.DataFrame(
            energie_produite.loc[energie_produite["RENEWABLEPROD"] == nom]
        ).set_index("annee")
        sheet_prod[col_an + ligne] = annee_prod
        if data_prod.empty:
            continue
        # S'il s'agit de l'action hydroélectrique, la valeur à saisir dans le
        # tableur est la somme de la production petite hydraulique et de la
        # grande production hydraulique.
        if ligne == "8":  # Il s'agit de la production hydroélectrique
            valeur_hydro += data_prod["valeur"].loc[annee_prod].sum()
            sheet_prod[col_enr + ligne] = valeur_hydro
        elif (
            ligne == "9"
        ):  # Valorisation électrique des déchets et bois ou autre biomasse solide
            valeur_biomasse_electrique += data_prod["valeur"].loc[annee_prod].sum()
            sheet_prod[col_enr + ligne] = valeur_biomasse_electrique
        elif (
            ligne == "12"
        ):  # Valorisation thermique des déchets et bois ou autre biomasse solide
            valeur_biomasse_thermique += data_prod["valeur"].loc[annee_prod].sum()
            sheet_prod[col_enr + ligne] = valeur_biomasse_thermique
        else:
            if annee_prod in data_prod["valeur"].index:
                sheet_prod[col_enr + ligne] = data_prod["valeur"].loc[annee_prod].sum()

    for annee_prod_proj in liste_annee_prod:
        valeur_hydro_obj = 0
        valeur_biomasse_elec_obj = 0
        valeur_biomasse_therm_obj = 0
        for nom_obj, ligne_obj in ligne_prod_enr_obj.items():
            # On vérifie que chaque type de production EnR est bien disponible au sein du territoire sélectionné.
            data_prod = pd.DataFrame(
                energie_produite.loc[
                    energie_produite["RENEWABLEPROD"] == RenewableProd[nom_obj]
                ]
            ).set_index("annee")
            if data_prod.empty:
                continue
            if ligne_obj == "25":
                valeur_hydro_obj += data_prod["valeur"].loc[int(annee_prod_proj)].sum()
                col = liste_annee_prod[annee_prod_proj]
                sheet_prod[col + ligne_obj] = valeur_hydro_obj
            elif ligne_obj == "26":
                valeur_biomasse_elec_obj += (
                    data_prod["valeur"].loc[int(annee_prod_proj)].sum()
                )
                col = liste_annee_prod[annee_prod_proj]
                sheet_prod[col + ligne_obj] = valeur_biomasse_elec_obj
            elif ligne_obj == "29":
                valeur_biomasse_therm_obj += (
                    data_prod["valeur"].loc[int(annee_prod_proj)].sum()
                )
                col = liste_annee_prod[annee_prod_proj]
                sheet_prod[col + ligne_obj] = valeur_biomasse_therm_obj
            else:
                col = liste_annee_prod[annee_prod_proj]
                # x1000 pour GWh -> MWh (onglet 3.ENR est en MWh)
                sheet_prod[col + ligne_obj] = (
                    data_prod["valeur"].loc[int(annee_prod_proj)].sum()
                )

    # Diagnostic
    col_emission_ges = "C"
    col_conso = "D"
    # Années de comptabilisation
    annee_conso = impact_energie["energie_economisee"]["max_annee"]
    annee_emission = impact_energie["emission_ges"]["max_annee"]
    # les nom sont repris de la table 'secteur'. On fait la correspondance avec les lignes dans le Excel.
    liste_annee_emission = {"2026": "F", "2030": "G", "2050": "H"}
    liste_annee_conso = {"2026": "J", "2030": "K", "2050": "L"}
    ligne_secteur = {
        "RESIDENTIAL": "6",
        "SERVICES": "7",
        "ROAD_TRANSPORT": "8",
        "OTHER_TRANSPORT": "9",
        "AGRICULTURE": "10",
        "WASTE_MANAGEMENT": "11",
        "INDUSTRY_WITHOUT_ENERGY": "12",
        "ENERGY_INDUSTRY": "13",
    }
    # Il est nécessaire de distinguer les secteurs filtrés pour la conso et ceux pour les émissions de gaz à effets
    # de serre car ils ne sont parfois pas tous disponibles. On a alors une erreur list index out of range
    # Il faut repérer les secteurs pour lesquelles la données est confidentielles en dissociant bien la conso des émissions
    unique_confid_conso_energie = (
        conso_energie[["nom", "SECTOR"]].groupby(["nom", "SECTOR"]).size().reset_index()
    )
    confid_consumption_sectors = [
        row
        for _, row in unique_confid_conso_energie.iterrows()
        if "Autres :" in row["nom"]
    ]

    unique_confid_emission_ges = (
        emission_ges[["nom", "SECTOR"]].groupby(["nom", "SECTOR"]).size().reset_index()
    )
    confid_emissions_sectors = [
        row
        for _, row in unique_confid_emission_ges.iterrows()
        if "Autres :" in row["nom"]
    ]

    secteurs_filtres_conso = {
        Sector[nom]: ligne
        for nom, ligne in ligne_secteur.items()
        if Sector[nom]
        in conso_energie.loc[conso_energie["confidentiel"] == "non", "SECTOR"].unique()
    }

    secteurs_filtres_ges = {
        Sector[nom]: ligne
        for nom, ligne in ligne_secteur.items()
        if Sector[nom]
        in emission_ges.loc[emission_ges["confidentiel"] == "non", "SECTOR"].unique()
    }

    for nom, ligne in ligne_secteur.items():
        passage_key = Sector[nom]
        is_consumption_confid = (
            len(confid_consumption_sectors) > 0
            and str(passage_key) in confid_consumption_sectors[0]["SECTOR"]
        )
        is_emissions_confid = (
            len(confid_emissions_sectors) > 0
            and str(passage_key) in confid_emissions_sectors[0]["SECTOR"]
        )

        # Deux types de confidentialité nécessitent donc deux vérifications et deux remplissages différents
        # d'où les deux blocs de code.
        if is_consumption_confid:
            sheet[col_conso + ligne] = "Confidentiel"
            for annee_conso_proj in liste_annee_conso:
                col = liste_annee_conso[annee_conso_proj]
                sheet[col + ligne] = "Confidentiel"

        if is_emissions_confid:
            sheet[col_emission_ges + ligne] = "Confidentiel"
            for annee_emission_proj in liste_annee_emission:
                col = liste_annee_emission[annee_emission_proj]
                sheet[col + ligne] = "Confidentiel"

        #  Si les données ne sont pas confidentielles et disponibles
        if passage_key in secteurs_filtres_conso and not is_consumption_confid:
            data_conso = pd.DataFrame(
                conso_energie.loc[conso_energie["SECTOR"] == passage_key]
            )
            # if we have at least some recordings
            if len(data_conso) > 0:
                data_conso.set_index("annee", inplace=True)
                # if we can find adequate year
                if annee_conso in data_conso["valeur"].index:
                    sheet[col_conso + ligne] = (
                        data_conso["valeur"].loc[annee_conso].sum()
                    )

                # we add more years for targets tables
                for annee_conso_proj in liste_annee_conso:
                    col = liste_annee_conso[annee_conso_proj]
                    # if we can find adequate year
                    if int(annee_conso_proj) in data_conso["valeur"].index:
                        sheet[col + ligne] = (
                            data_conso["valeur"].loc[int(annee_conso_proj)].sum()
                        )
        # Pour l'instant je n'ai pas vu d'autres possibilités pour différencier les deux régions.
        if ges_export_excel:
            if passage_key in secteurs_filtres_ges and not is_emissions_confid:
                sheet[col_emission_ges + ligne] = "Confidentiel"

                data_emission = pd.DataFrame(
                    emission_ges.loc[emission_ges["SECTOR"] == passage_key]
                ).set_index("annee")

                # if we can find adequate year
                if annee_emission in data_emission["valeur"].index:
                    sheet[col_emission_ges + ligne] = (
                        data_emission["valeur"].loc[annee_emission].sum()
                    )

                # we add more years for targets tables
                for annee_emission_proj in liste_annee_emission:
                    col = liste_annee_emission[annee_emission_proj]
                    # if we can find adequate year
                    if int(annee_emission_proj) in data_emission["valeur"].index:
                        sheet[col + ligne] = (
                            data_emission["valeur"].loc[int(annee_emission_proj)].sum()
                        )

    # récupération de la dernière année où l'on a des données
    sheet[col_emission_ges + "15"] = annee_emission
    sheet[col_conso + "15"] = annee_conso


async def export_excel(
    region, impact_energie, polluants, output_filepath, ges_export_excel
):
    """Génère un fichier Excel au format ADEME pour les PCAET.

    Récupère à la fois les dernières données disponibles ainsi que les résultats des stratégies territoriales pour les émissions GES,
    conso énergie, production EnR, polluants atmosphériques et remplit les feuilles de calcul du fichier Excel au format ADEME.

    Parameters
    ----------
    impact_energie : dict
        dictionnaire des impacts ges, énergie, production EnR
    polluants : dict
        dictionnaire des données de polluants atmosphériques
    output_filepath : str

    """
    # TODO(dag): refactoring de la fonction d'export pour utiliser directement les
    # DataFrame retournés par la fonction 'impact_total' On parcourt chaque
    # dictionnaire pour passer les DataFrame au format de graphe attendu par
    # l'application web, identique aux structures de données pour la fonction
    # d'export Excel.
    # cf. https://gitlab.com/terristory/terristory/-/issues/901
    wb = load_workbook(EXCEL_ADEME_TEMPLATE)

    await export_excel_conso_ges_enr(region, impact_energie, wb, ges_export_excel)
    if polluants:
        export_excel_polluants_atmospheriques(polluants, wb)
    wb.save(output_filepath)
    return output_filepath


async def get_percent_equipement_chauffage_residentiel_tertiaire(
    region, zone, zone_id, action, provenance, advanced_params={}
):
    """retourne les répartitions des équipements de chauffage dans le secteur résidentiel / tertaire
        pour l'année de référence (la dernière année de la disponibilité des données dans la BD TS).

    Parameters
    ----------
    region : str
        Nom de la région (schéma)
    zone : str
        Nom du territoire
    zone_id : str
        Identifiant du territoire
    action : float
        numéro de l'action
    provenance : str
    advanced_params : dict
        les paramètres avancés

    Returns
    ----------
    dict  : %
        Avec l'ensemble des répartitions des équipements de chauffage
    """

    dispatcher = ActionsDispatcher(region, zone, zone_id, limited_to_actions=[action])
    # we create passage tables
    passage_table = await dispatcher.get_passage_table()

    relevant_action = dispatcher.get_action_by_action_number(action)

    # we handle static params
    await dispatcher.load_required_data(passage_table)
    await dispatcher.load_required_params()

    # paramètres avancés des actions modifiables par l'utilisateur
    user_action_params = await actions_data.get_user_action_params(region)

    kwargs = {}
    # TOFIX: uncomment
    if provenance != "api":
        # surcharge par les paramètres entrées dans l'interface
        for aname in user_action_params.get(action, {}).keys():
            kwargs.update(
                override_user_params(
                    {"user_action_params": user_action_params},
                    advanced_params,
                    aname,
                    action,
                )
            )
    conso_data_key = DataKeys(relevant_action.CONSUMPTION_KEYS)
    conso_usage_residentiel_chauffage = dispatcher.data[conso_data_key]

    # Coefficient de performance pompe à chaleur : COP moyen
    COP = (
        kwargs.get(
            "Coefficient de performance",
            user_action_params[action]["Coefficient de performance"],
        )
        .iloc[0]
        .valeur
    )

    # get distribution between vectors
    repartition_conso_gaz = user_action_params[action][
        relevant_action.PARAM_GAS_DISTRIB
    ].set_index("nom")["valeur"]
    repartition_conso_pp = user_action_params[action][
        relevant_action.PARAM_OIL_DISTRIB
    ].set_index("nom")["valeur"]
    repartition_conso_EnRt = user_action_params[action][
        relevant_action.PARAM_HEAT_DISTRIB
    ].set_index("nom")["valeur"]
    repartition_conso_elec = user_action_params[action][
        relevant_action.PARAM_ELEC_DISTRIB
    ].set_index("nom")["valeur"]

    # get real distribution from current consumption
    res = await heating_systems_distribution(
        conso_usage_residentiel_chauffage,
        COP,
        repartition_conso_EnRt,
        repartition_conso_elec,
        repartition_conso_pp,
        repartition_conso_gaz,
        relevant_action.input_distrib_keys,
    )
    return {**res, "annee_ref": int(dispatcher.max_years[conso_data_key])}


async def get_list(region):
    req = """
          SELECT DISTINCT(type) FROM strategie_territoire.action_details
          WHERE $1 = ANY(regions)
          ORDER BY type;
          """
    resp = await controller.fetch(req, region)
    return [dict(i) for i in resp]


async def compute_total_impact(
    region,
    zone,
    zone_id,
    years,
    action_year,
    action_params,
    advanced_params,
    advanced_economic_params,
    advanced_params_years={},
    regional_config={},
):
    dispatcher = ActionsDispatcher(region, zone, zone_id)

    # we set regional config
    dispatcher.set_config(regional_config)

    # we retrieve actions settings
    actions_settings = await actions_data.get_actions_list(region, zone, zone_id)
    years = dispatcher.set_fixed_years(years, actions_settings)
    dispatcher.set_actions_settings(actions_settings)

    # we handle user inputs
    defaults_inputs = DefaultInputsLoader(region, zone, zone_id)
    # TODO: use required params to specify which params to load
    defaults_inputs.set_defaults(
        actions_settings,
        await actions_data.get_subactions_list(region, zone, zone_id),
        await actions_data.get_advanced_params_list(region, zone, zone_id),
    )
    inputs = InputsLoader(region, zone, zone_id)
    inputs.load_inputs(
        years,
        action_year,
        action_params,
        advanced_params,
        advanced_economic_params,
        advanced_params_years,
        regional_config,
    )
    dispatcher.merge_inputs(defaults_inputs, inputs)

    # we check which inputs are activated
    dispatcher.load_and_check_inputs()

    # we create passage tables
    passage_table = await dispatcher.get_passage_table()

    # we handle static params
    await dispatcher.load_required_params()

    # finally, we handle data
    await dispatcher.load_required_data(passage_table)

    # we check that all requisites are met
    dispatcher.load_and_check_requisites()

    return dispatcher


async def single_action_tester(
    action_key,
    region,
    zone,
    zone_id,
    regional_config,
):
    dispatcher = ActionsTester(region, zone, zone_id)

    # we set regional config
    dispatcher.set_config(regional_config)

    # we retrieve actions settings
    actions_settings = await actions_data.get_actions_list(region, zone, zone_id)
    dispatcher.set_actions_settings(actions_settings)

    # we handle user inputs
    defaults_inputs = DefaultInputsLoader(region, zone, zone_id)
    # TODO: use required params to specify which params to load
    defaults_inputs.set_defaults(
        actions_settings,
        await actions_data.get_subactions_list(region, zone, zone_id),
        await actions_data.get_advanced_params_list(region, zone, zone_id),
    )

    # we create passage tables
    passage_table = await dispatcher.get_passage_table()

    # we handle static params
    await dispatcher.load_required_params()

    # finally, we handle data
    await dispatcher.load_required_data(passage_table)

    tests = dispatcher.get_action_tests(action_key)
    impacts = {}

    for test in tests:
        mocked_inputs = InputsLoader(region, zone, zone_id)
        mocked_inputs.load_mocked_inputs(test.get_mocked_inputs())
        dispatcher.merge_inputs(defaults_inputs, mocked_inputs, mocked=True)

        # we check that all inputs are loaded
        dispatcher.load_and_check_inputs()

        # we check that all requisites are met
        dispatcher.load_and_check_requisites()

        # we execute
        outputs_by_action = await dispatcher.execute_all()
        _impacts = await dispatcher.merge_outputs(outputs_by_action)
        impacts[str(test)] = _impacts

    return impacts


async def get_actions_errors(region):
    res = await controller.fetch(
        """SELECT action_number, action_key, zone_id, zone_type,
        error_message,
        to_char(error_date, 'YYYY-MM-DD"T"HH24:MI:SS') as error_date
        FROM strategie_territoire.action_errors WHERE region = $1""",
        region,
    )
    return [dict(row) for row in res]


async def get_first_year_data_from_action(region, action_id):
    res = await controller.fetch(
        """SELECT first_year
        FROM strategie_territoire.action_status
        WHERE region = $1 AND action_number = $2""",
        region,
        action_id,
    )
    res = [dict(row) for row in res]
    if len(res) == 0:
        raise ValueError("Action not available in your region")
    return res[0]["first_year"]
