﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


from terriapi import controller


async def all_zones(region: str, with_order=False):
    """Fonction qui renvoie une liste de dict des mailles et des sous mailles associées ainsi que leur libellé.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)

    Returns
    -------
    list
    """

    req = f"""
        SELECT nom, maille, libelle, hide{", ordre" if with_order else ""}
        FROM {region}.zone
        ORDER BY ordre ASC
    """

    rset = await controller.fetch(req)
    result = [dict(row) for row in rset]

    return result


async def all_territorial_levels(region: str) -> list[str]:
    """Fonction qui renvoie une liste de toutes les mailles existantes.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)

    Returns
    -------
    list
    """
    schema = region.replace("-", "_")
    req = f"""
        SELECT DISTINCT type_territoire
        FROM {schema}.territoire
        ORDER BY type_territoire ASC
    """

    rset = await controller.fetch(req)
    result = [row["type_territoire"] for row in rset]

    return result


async def all_territories_codes_for_type(region: str, level):
    """Fonction qui renvoie une liste de tous les codes uniques pour un niveau
    territorial donné.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    level : str
        Maille territoriale

    Returns
    -------
    list
    """
    schema = region.replace("-", "_")
    req = f"""
        SELECT DISTINCT code
        FROM {schema}.territoire
        WHERE type_territoire = $1
    """

    rset = await controller.fetch(req, level)
    result = [row["code"] for row in rset]

    return result


async def all_territories_for_type(region: str, level) -> list[dict]:
    schema = region.replace("-", "_")
    req = f"""
        SELECT DISTINCT code, nom as name
        FROM {schema}.territoire
        WHERE type_territoire = $1
    """

    rset = await controller.fetch(req, level)
    return [dict(row) for row in rset]


async def edit_zone(region: str, old_name, new_name, zone, maille):
    """Fonction qui modifie une zone.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    old_name : str
        old zone key
    new_data : dict
        new zone data
    """

    req = f"""
        UPDATE {region}.zone SET 
        libelle = $1, nom = $2, maille = $3 WHERE libelle = $4
    """

    return await controller.execute(req, new_name, zone, maille, old_name)


async def add_zone(region: str, name, zone, maille, order):
    """Fonction qui modifie une zone.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    old_name : str
        old zone key
    new_data : dict
        new zone data
    """

    req = f"""
        INSERT INTO {region}.zone (nom, maille, libelle, ordre, hide)
        VALUES($1, $2, $3, $4, false)
    """

    return await controller.execute(req, zone, maille, name, order)


async def toggle_zone(region: str, name):
    """Hide/show a zone.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    name : dict
        zone name
    """

    req = f"""
        UPDATE {region}.zone SET hide = NOT hide WHERE libelle = $1
    """

    return await controller.execute(req, name)


async def delete_zone(region: str, name, current_position):
    """Delete a zone.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    name : dict
        zone name
    """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            req = f"""
                DELETE FROM {region}.zone WHERE libelle = $1
            """
            await conn.execute(req, name)
            req = f"""
                UPDATE {region}.zone SET ordre = ordre - 1 WHERE ordre > $1
            """
            await conn.execute(req, current_position)


async def change_order_zone(region: str, name, current_position, new_position):
    """Fonction qui modifie une zone.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    old_name : str
        old zone key
    new_data : dict
        new zone data
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            req = f"""
                UPDATE {region}.zone SET
                ordre = $1 WHERE libelle <> $2 AND ordre = $3
            """
            await conn.execute(req, current_position, name, new_position)
            req = f"""
                UPDATE {region}.zone SET
                ordre = $1 WHERE libelle = $2
            """
            await conn.execute(req, new_position, name)
