﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import os
import re

from terriapi.controller import conversion_chaine_en_binaire, fetch
from terriapi.controller.gestion_images import SuppressionUniqueFichierDossier


async def update_actu_pop_up(region, texte_markdown):
    """Fonction qui met à jour le contenu de la fenêtre d'actu.

    Parameters
    ----------
    region : la région à mettre à jour
    texte_markdown : le texte en langage markdown
    """
    texte_markdown = conversion_chaine_en_binaire(texte_markdown)
    req = """
            UPDATE public.regions_configuration
            SET actu = $1
            WHERE id = $2
          """
    await fetch(req, texte_markdown, region)


def liste_images_carousel_avec_path(path, all_images, region):
    """Fonction qui trie les images à défiler dans le carousel dans l'ordre et par groupe d'images.

    Parameters
    ----------
    region : la région à mettre à jour
    str
    path : le chemin vers le dossier
    str
    all_images : la liste des noms d'images
    list

    Returns
    -------
    list
    """
    # liste contenant l'ensemble des listes d'images
    all_images_region = []
    # liste qui trie les noms d'images
    all_images.sort()
    goupe_img = 0
    # boucle qui itère chaque image
    for img in all_images:
        # si nom img contient le nom de la région
        if re.search(region, img):
            # récupère les deux indices numériques dans la nom de l'image
            liste_chaine_placement = re.findall(r"[1-9]_[1-9]", img)
            # séparation des deux valeurs
            liste_val_position = liste_chaine_placement[0].split("_")
            # construction de l'url vers l'image
            dict_url = {"url": path + img}
            # détermination de la position de l'image dans la liste d'images
            position = int(liste_val_position[1]) - 1
            # si la valeur du groupe d'images est différente de celle de la valeur qui détermine à quel groupe d'image appartient l'image
            if int(liste_val_position[0]) != goupe_img:
                # changement valeur groupe image
                goupe_img = int(liste_val_position[0])
                # ajout nouvelle liste au contenant l'ensemble des images du nouveau groupe d'images
                all_images_region.append([])
            # insertion de l'image à la liste représentant associé à la valeur du groupe d'images moins 1
            all_images_region[goupe_img - 1].insert(position, dict_url)

    return all_images_region


def suppression_images_region_pop_up_accueil(region, path_folder):
    """Fonction qui supprime les images qu'une région avait choisi pour son carousel.

    Parameters
    ----------
    region : la région à mettre à jour
    str
    path : le chemin vers le dossier
    str
    """
    for filename in os.listdir(path_folder):
        if re.search(region, filename):
            SuppressionUniqueFichierDossier(
                path_folder
            ).supprimer_unique_fichier_dossier(filename)
