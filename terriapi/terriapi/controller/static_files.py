# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from datetime import datetime

import asyncpg
from sanic.log import logger

from terriapi.controller import execute, fetch


def _parse_static_files_data(x):
    """
    Parse a static file related data retrieved from database to turn it into a
    python dict. Also parse the `update_date` field from python datetime into
    a string (with French format `DD/MM/YYYY, hh:mm:ss`).

    Parameters
    ----------
    x : Record
        the input data corresponding to one row

    Returns
    -------
    dict
        the data correctfully parsed
    """
    d = dict(x)
    if "update_date" in d:
        d["update_date"] = d["update_date"].strftime("%d/%m/%Y, %H:%M:%S")
    return d


async def get_list(region):
    """
    Retrieve static files list from database for region considered.

    Parameters
    ----------
    region : str
        region name

    Returns
    -------
    list of dict
        dict containing file_id, name, file_name and update_date for each static
        file related to current region.
    """
    query = """SELECT file_id, name, file_name, update_date
    FROM meta.static_files WHERE region = $1"""
    rset = await fetch(query, region)
    return [_parse_static_files_data(x) for x in rset]


async def get_static_file(region, id):
    """
    Retrieve and check that a static file does exist for current region and given
    ID inside `meta.static_files` table. Return related data if existing.

    Parameters
    ----------
    region : str
        region name
    id : str or int
        the file ID

    Returns
    -------
    dict
        file_id, name, file_name, update_date data if the static_file has been found

    Raises
    ------
    ValueError
        when the query returns no record or more than one record
    """
    query = """SELECT file_id, name, file_name, update_date
    FROM meta.static_files WHERE region = $1 and file_id = $2"""
    rset = await fetch(query, region, int(id))
    rset = [_parse_static_files_data(x) for x in rset]
    if len(rset) != 1:
        logger.error(
            f"Couldn't find static file corresponding to region ({region}) and id ({id})."
        )
        raise ValueError
    return rset[0]


async def update_static_file(region, id):
    """
    Update date for a file inside database.

    Parameters
    ----------
    region : str
        region name
    id : str or int
        the file ID

    Returns
    -------
    datetime.datetime
        new update date
    """
    now = datetime.now()
    query = """UPDATE meta.static_files SET update_date = $3 WHERE region = $1 and file_id = $2"""

    try:
        await execute(query, region, int(id), now)
    except asyncpg.PostgresError as e:
        logger.error(e)
        return False
    return now
