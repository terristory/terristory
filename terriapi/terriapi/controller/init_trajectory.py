# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
from __future__ import annotations

from collections import defaultdict

import pandas as pd

from terriapi.controller import fetch
from terriapi.controller.analyse import total_territory_value
from terriapi.controller.pcaet_ademe import get_pcaet_ademe, pcaet_trajectories_meta
from terriapi.controller.strategy_actions import BehaviorWhenMissing, DataRequisite
from terriapi.controller.strategy_actions.constants import ColumnPassageType, DataSet
from terriapi.controller.strategy_actions.loaders import DataLoader
from terriapi.controller.strategy_actions.passage_table import PassageTable

TRAJECTORY_TYPES = [
    "emission_ges",
    "consommation_ener",
    "enr_production",
    "pm10",
    "pm25",
    "nox",
    "sox",
    "covnm",
    "nh3",
]


async def get_pcaet_initial_trajectory(
    region: str,
    zone_type: str,
    zone_id: str,
    vals_observatoire: dict,
    observatory_year: int,
    enable_relative_pcaet: list[str],
) -> dict[str, dict[str, float]] | None:
    """
    Fonction qui renvoie les valeurs des trajectoires cibles initiales venant des PCAET.
    La première année correspond à la dernière année de l'observatoire disponible

    Parameters
    ----------
    enable_relative_pcaet: list[str]
        Si on veut les PCAET relatifs ou bruts (pour chaque trajectoire)
    """

    vals_observatoire_keys = list(vals_observatoire.keys())
    # Reorder the key of vals_observatoire to correspond to TRAJECTORY_TYPES's order
    VAL_OBSERVATOIRE_KEYS_ORDER = [2, 0, 1, 5, 6, 3, 4, 7, 8]
    vals_observatoire_keys = [
        vals_observatoire_keys[i] for i in VAL_OBSERVATOIRE_KEYS_ORDER
    ]

    pcaet_df = await get_pcaet_ademe(region, zone_type, zone_id, enable_relative_pcaet)
    if pcaet_df is None or pcaet_df.empty:
        # If there is no pcaet associated to our zone_id
        return None

    pcaet_df = pcaet_df.groupby(["trajectory", "is_ref", "year"]).sum()

    # Fill the trajectories
    pcaet_ademe = {}
    for i, trajectory in enumerate(TRAJECTORY_TYPES):
        if not vals_observatoire[vals_observatoire_keys[i]]:
            continue
        pcaet_ademe[trajectory] = {
            "ref": vals_observatoire[vals_observatoire_keys[i]],
        }

        if trajectory not in pcaet_df.index:
            continue
        for year, objective in pcaet_df.loc[(trajectory, False)].iterrows():
            if year > observatory_year:
                pcaet_ademe[trajectory][str(year)] = float(objective["value"])

    pcaet_ademe = pcaet_value_regulation(pcaet_ademe)
    return pcaet_ademe


def pcaet_value_regulation(
    dict_pcaet: dict[str, dict[str, float]]
) -> dict[str, dict[str, float]]:
    """
    Si la consommation d'énergie ou l'émission de GES est supérieure à l'objectif d'évolution, le pourcentage d'évolution est de 0%.
    Si la production d'ENR est inférieure à l'objectif d'évolution, le pourcentage d'évolution est de 0%.
    """
    # Categories for which the evolution should be positive
    increasing_categories = ["enr_production"]

    for category, pcaet_values in dict_pcaet.items():
        keys_list = sorted(pcaet_values.keys())
        # The "ref" key is sorted at the end but we want it at the begining
        keys_list.insert(0, keys_list.pop())

        for key, next_key in zip(keys_list[:-1], keys_list[1:]):
            if category in increasing_categories:
                should_replace = pcaet_values[next_key] < pcaet_values[key]
            else:
                should_replace = pcaet_values[next_key] > pcaet_values[key]
            if should_replace:
                pcaet_values[next_key] = pcaet_values[key]

    return dict_pcaet


async def get_pcaet_objectives(
    region: str, zone: str, zone_id: str, enable_relative_pcaet: list[str]
) -> dict[str, dict[str,]] | None:
    """
    Fonction qui renvoie les valeurs des trajectoires PCAET complètes.

    Parameters
    ----------
    enable_relative_pcaet: list[str]
        Si on veut les PCAET relatifs ou bruts (pour chaque trajectoire)
    """
    TRAJECTORY_INDICATORS = [
        "emission_ges",
        "conso_energetique",
        "prod_enr",
        "air_polluant_pm10",
        "air_polluant_pm25",
        "air_polluant_nox",
        "air_polluant_sox",
        "air_polluant_covnm",
        "air_polluant_nh3",
    ]
    TRAJECTORY_REFERENCES = [
        "emission_ges_ref",
        "energie_economisee_ref",
        "energie_produite_ref",
        "pm10_ref",
        "pm25_ref",
        "nox_ref",
        "sox_ref",
        "covnm_ref",
        "nh3_ref",
    ]
    SHORT_NAMES = ["GES", "Conso", "ENR", "PM10", "PM2.5", "NOx", "SO2", "COVNM", "NH3"]

    pcaet_df = await get_pcaet_ademe(region, zone, zone_id, enable_relative_pcaet)
    if pcaet_df is None or pcaet_df.empty:
        # If there is no pcaet associated to our zone_id
        return None

    pcaet_df = pcaet_df.groupby(["trajectory", "is_ref", "year"]).sum()

    final_pcaet = {}
    # Fill the trajectories
    for i, trajectory in enumerate(TRAJECTORY_TYPES):
        table = TRAJECTORY_INDICATORS[i]
        col_ref = TRAJECTORY_REFERENCES[i]
        if trajectory not in pcaet_df.index:
            continue

        ref_value = float(pcaet_df.loc[(trajectory, True), "value"])

        final_pcaet[col_ref] = {
            "valeurs_annees": [],
            "titre": "PCAET ADEME - " + SHORT_NAMES[i],
            "couleur": "#0DCAF0",
            "affectation": '{"' + zone + 's":""}',
            "description": "",
            "table": table,
            "annee_reference": int(
                pcaet_df.loc[(trajectory, True)].reset_index()["year"]
            ),
            "valeur_reference": ref_value,
        }

        for year, objective in pcaet_df.loc[(trajectory, False)].iterrows():
            final_pcaet[col_ref]["valeurs_annees"].append(
                {
                    "annee": year,
                    # Get reduction percentage from values.
                    "valeur": (objective["value"] / ref_value - 1) * 100,
                }
            )

    return final_pcaet


def get_historical_trajectory(df: pd.DataFrame) -> list | None:
    if df.empty:
        return None
    return df.groupby("annee").sum().reset_index().to_dict("records")


def get_historical_trajectory_by_category(
    df: pd.DataFrame, category: str
) -> pd.DataFrame | None:
    if df.empty:
        return None
    return df.groupby(["annee", category])[["valeur"]].sum().reset_index()


def get_total_from_historical_trajectory(
    key: str, data_loader: DataLoader, year: int | None
) -> float | None:
    if key not in data_loader.data or data_loader.data[key].empty:
        return None
    df = data_loader.data[key]
    if not (df["annee"] == year).any():
        year = data_loader.max_years.get(key, df["annee"].max())
    return float(df.loc[df["annee"] == year, "valeur"].sum())


async def get_initial_trajectories(
    region: str,
    zone: str,
    zone_id: str,
    actions: list[dict],
    enable_pollutants: bool = False,
    enable_history: bool = True,
):
    """Récupérer les valeurs initiales des trajectoires cibles (partie plan d'action)

    Parameters
    ----------
    region: str
    zone : str
    zone_id : str

    Returns
    -------
    dict
    """
    passage_table = PassageTable(region)

    tableKeys: list[DataSet] = [
        key["table"] for key in pcaet_trajectories_meta(enable_pollutants)
    ]
    await passage_table.load(tableKeys)

    data_last_year = None
    if actions:
        data_last_year = min([action["first_year"] for action in actions]) - 1

    default_val = BehaviorWhenMissing(BehaviorWhenMissing.Types.DEFAULT_VALUE, None)
    requisites = [DataRequisite(key.name, default_val, key) for key in tableKeys]

    data_loader = DataLoader(passage_table)
    await data_loader.load(region, zone, zone_id, requisites, all_years=True)

    territory_values = {
        key: get_total_from_historical_trajectory(str(key), data_loader, data_last_year)
        for key in tableKeys
    }

    # historical value
    history_values = {}
    if enable_history:
        history_values = {
            key: get_historical_trajectory(data_loader.data[str(key)])
            for key in tableKeys
        }

    # the name of the value fields corresponds to the names of the result curves of the action plan page
    return {
        "history": {
            "energie_economisee": history_values.get(DataSet.CONSUMPTION, []),
            "energie_produite": history_values.get(DataSet.PRODUCTION, []),
            "emission_ges": history_values.get(DataSet.EMISSIONS, []),
            "nox": history_values.get(DataSet.POLLUTANT_NOX, []),
            "sox": history_values.get(DataSet.POLLUTANT_SOX, []),
            "pm10": history_values.get(DataSet.POLLUTANT_PM10, []),
            "pm25": history_values.get(DataSet.POLLUTANT_PM25, []),
            "covnm": history_values.get(DataSet.POLLUTANT_COVNM, []),
            "nh3": history_values.get(DataSet.POLLUTANT_NH3, []),
        },
        "future": {
            "energie_economisee": territory_values.get(DataSet.CONSUMPTION),
            "energie_produite": territory_values.get(DataSet.PRODUCTION),
            "emission_ges": territory_values.get(DataSet.EMISSIONS),
            "nox": territory_values.get(DataSet.POLLUTANT_NOX),
            "sox": territory_values.get(DataSet.POLLUTANT_SOX),
            "pm10": territory_values.get(DataSet.POLLUTANT_PM10),
            "pm25": territory_values.get(DataSet.POLLUTANT_PM25),
            "covnm": territory_values.get(DataSet.POLLUTANT_COVNM),
            "nh3": territory_values.get(DataSet.POLLUTANT_NH3),
        },
        "observatory_year": data_last_year,
    }


async def get_initial_trajectory(
    region: str,
    zone: str,
    zone_id: str,
    table_key: DataSet,
    category_key: ColumnPassageType,
):
    """Récupérer les valeurs initiales des trajectoires cibles (partie plan d'action)

    Parameters
    ----------
    region: str
    zone : str
    zone_id : str

    Returns
    -------
    dict
    """
    passage_table = PassageTable(region)

    await passage_table.load([table_key])
    category = passage_table.convert_column_key(category_key, table_key)

    default_val = BehaviorWhenMissing(BehaviorWhenMissing.Types.DEFAULT_VALUE, None)
    requisites = [
        DataRequisite(table_key.name, default_val, table_key),
    ]

    data_loader = DataLoader(passage_table)
    await data_loader.load(region, zone, zone_id, requisites, all_years=True)
    trajectory = get_historical_trajectory_by_category(
        data_loader.data[str(table_key)], category
    )
    if trajectory is None:
        return category, []

    # retrieve metadata
    categories_meta = await fetch(
        """ SELECT modalite_id, modalite, couleur FROM meta.categorie
            WHERE nom = $1 AND region = $2 """,
        category,
        region,
    )
    categories_meta = pd.DataFrame([dict(row) for row in categories_meta])
    trajectory[category] = trajectory[category].astype(int)
    trajectory = trajectory.join(categories_meta.set_index("modalite_id"), on=category)

    # agregates according to right format
    new_trajectory = []
    for index, group in trajectory.groupby(["modalite", "couleur"]):
        name, color = index
        new_trajectory.append(
            {
                "nom": name,
                "couleur": color,
                "confidentiel": "non",  # TODO
                "data": [
                    row[["annee", "valeur"]].to_dict() for _, row in group.iterrows()
                ],
            }
        )

    # historical value
    return (
        category,
        new_trajectory,
    )


async def get_trajectory_objectives(region: str, zone: str, zone_id: str):
    """Récupérer les valeurs des trajectoires cibles par défaut entrés via les interfaces admins (partie plan d'action)

    Parameters
    ----------
    region: str
    zone: str
    zone_id: str

    Returns
    -------
    dict
    """

    # all supra-territorial objectives metadata of "conso_energetique", "emission_ges", "prod_enr", "air_polluant_covnm", ...
    # retrieve all existing (title, graphic)
    req = """
        SELECT f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference, f.description, 
        json_object_agg(f.type_territoire, f.code_territoire order by f.type_territoire) as affectation
        FROM (
            SELECT p.supra_goal_id, p.titre, p.couleur, p.annee_reference, p.description, p.graphique, 
            a.type_territoire, a.code_territoire
            FROM strategie_territoire.supra_goals p
            FULL OUTER JOIN strategie_territoire.supra_goals_links a 
            ON a.supra_goal_id = p.supra_goal_id
            WHERE p.region = $1
        ) AS f
        WHERE f.type_territoire is not null
        GROUP BY f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference, f.description

        UNION ALL

        SELECT f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference, f.description,
        json_object_agg('None', '') as affectation
        FROM (
            SELECT p.supra_goal_id, p.titre, p.graphique, p.couleur, p.annee_reference, p.description, 
            a.type_territoire, a.code_territoire
            FROM strategie_territoire.supra_goals p
            FULL OUTER JOIN strategie_territoire.supra_goals_links a ON a.supra_goal_id = p.supra_goal_id
            WHERE p.region = $1
        ) as f
        WHERE f.type_territoire is null
        GROUP BY f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference, f.description;
        """

    result = await fetch(req, region)
    liste_graphiques = [dict(x) for x in result]
    if len(liste_graphiques) == 0:
        return {
            "energie_economisee_ref": [],
            "energie_produite_ref": [],
            "emission_ges_ref": [],
            "covnm_ref": [],
            "nox_ref": [],
            "sox_ref": [],
            "pm10_ref": [],
            "pm25_ref": [],
            "nh3_ref": [],
        }

    table_keys = list(set([g["graphique"] for g in liste_graphiques]))
    passage_table = PassageTable(region)
    res = await passage_table.load(table_keys)
    table_names = passage_table.convert_table_keys(table_keys)
    custom_filters_by_table = {
        table_key: passage_table.get_custom_real_conditions(DataSet[table_key])
        for table_key in table_keys
    }

    # retrieve all names, reference years and colors of the supra-territorial
    # objectives by analysis : "conso_energetique", "emission_ges", "prod_enr", "air_polluant_covnm", ...
    list_params_objectifs = defaultdict(list)
    for x in liste_graphiques:
        table_key = x["graphique"]

        request = """
                  select o.annee, o.valeur
                  from strategie_territoire.supra_goals_values o
                  where o.supra_goal_id = $1
                  order by o.annee
                  """
        res = await fetch(request, x["supra_goal_id"])
        valeurReference = await total_territory_value(
            region,
            zone,
            zone_id,
            table_names[table_key],
            x["annee_reference"],
            custom_filters_by_table[table_key],
        )
        list_params_objectifs[table_key].append(
            {
                "valeurs_annees": [dict(a) for a in res],
                "titre": x["titre"],
                "couleur": x["couleur"],
                "annee_reference": x["annee_reference"],
                "valeur_reference": valeurReference,
                "affectation": x["affectation"],
                "description": x["description"],
                "table": table_key,
            }
        )

    # configurations of "conso_energetique", "emission_ges", "prod_enr" analysis and air pollutants
    return {
        "energie_economisee_ref": list_params_objectifs.get("DataSet.CONSUMPTION", []),
        "energie_produite_ref": list_params_objectifs.get("DataSet.PRODUCTION", []),
        "emission_ges_ref": list_params_objectifs.get("DataSet.EMISSIONS", []),
        "covnm_ref": list_params_objectifs.get("DataSet.POLLUTANT_COVNM", []),
        "nox_ref": list_params_objectifs.get("DataSet.POLLUTANT_NOX", []),
        "sox_ref": list_params_objectifs.get("DataSet.POLLUTANT_SOX", []),
        "pm10_ref": list_params_objectifs.get("DataSet.POLLUTANT_PM10", []),
        "pm25_ref": list_params_objectifs.get("DataSet.POLLUTANT_PM25", []),
        "nh3_ref": list_params_objectifs.get("DataSet.POLLUTANT_NH3", []),
    }


async def get_supra_goals_for_trajectory(
    region: str, zone: str, zone_id: str, trajectory_id: str
) -> list[dict]:
    """Récupérer les valeurs des trajectoires cibles par défaut entrés via les interfaces admins (partie plan d'action)

    Parameters
    ----------
    region: str
    zone: str
    zone_id: str

    Returns
    -------
    dict
    """

    traj_meta = next(
        meta for meta in pcaet_trajectories_meta() if meta["id"] == trajectory_id
    )

    # all supra-territorial objectives metadata of "conso_energetique", "emission_ges", "prod_enr", "air_polluant_covnm", ...
    # retrieve all existing (title, graphic)
    # TODO: Refactor affectation with CASE WHEN ... THEN ... ELSE ... END ?
    req = """
        SELECT f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference, f.description, f.filter,
            json_object_agg(f.type_territoire, f.code_territoire ORDER BY f.type_territoire) AS affectation
        FROM (
            SELECT p.supra_goal_id, p.titre, p.graphique, p.couleur, p.annee_reference,
                p.description, p.filter, a.type_territoire, a.code_territoire
            FROM strategie_territoire.supra_goals p
            FULL OUTER JOIN strategie_territoire.supra_goals_links a ON a.supra_goal_id = p.supra_goal_id
            WHERE p.region = $1 AND p.graphique = $2
        ) AS f
        WHERE f.type_territoire IS NOT null
        GROUP BY f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference, f.description, f.filter

        UNION ALL

        SELECT f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference,
            f.description, f.filter, json_object_agg('None', '') AS affectation
        FROM (
            SELECT p.supra_goal_id, p.titre, p.graphique, p.couleur, p.annee_reference,
                p.description, p.filter, a.type_territoire, a.code_territoire
            FROM strategie_territoire.supra_goals p
            FULL OUTER JOIN strategie_territoire.supra_goals_links a ON a.supra_goal_id = p.supra_goal_id
            WHERE p.region = $1 AND p.graphique = $2
        ) AS f
        WHERE f.type_territoire IS null
        GROUP BY f.supra_goal_id, f.titre, f.graphique, f.couleur, f.annee_reference, f.description, f.filter;
    """

    result = await fetch(req, region, str(traj_meta["table"]))
    graphiques = [dict(x) for x in result]
    if len(graphiques) == 0:
        return []

    passage_table = PassageTable(region)
    res = await passage_table.load([traj_meta["table"]])
    table_name = passage_table.convert_table_key(traj_meta["table"])
    custom_filter = passage_table.get_custom_real_conditions(
        DataSet[str(traj_meta["table"])]
    )

    # retrieve all names, reference years and colors of the supra-territorial
    # objectives by analysis : "conso_energetique", "emission_ges", "prod_enr", "air_polluant_covnm", ...
    list_params_objectifs = []

    request = """
              select o.annee, o.valeur
              from strategie_territoire.supra_goals_values o
              where o.supra_goal_id = $1
              order by o.annee
              """

    for graphique in graphiques:
        res = await fetch(request, graphique["supra_goal_id"])
        valeurReference = await total_territory_value(
            region,
            zone,
            zone_id,
            table_name,
            graphique["annee_reference"],
            custom_filter,
            graphique["filter"],
        )
        list_params_objectifs.append(
            {
                "valeurs_annees": [dict(a) for a in res],
                "titre": graphique["titre"],
                "couleur": graphique["couleur"],
                "annee_reference": graphique["annee_reference"],
                "valeur_reference": valeurReference,
                "affectation": graphique["affectation"],
                "description": graphique["description"],
                "filter": graphique["filter"],
                "table": str(traj_meta["table"]),
            }
        )

    return list_params_objectifs
