﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import datetime
import json
import os

import asyncpg
import numpy as np
import pandas as pd
import slugify

from terriapi import controller, here, settings, utils


def set_link_value_sankey(
    link_data_tags: list,  # correspond à nodeTags (étiquettes de noeuds)' ex: Région 1 2010 ou Région 2 2015
    link_flux_tags: list,  # correspond à fluxTags (étiquettes de flux)'
    fluxTags: dict,  # avec link_flux_tags
    depth: int,  # profondeur par exemple région année a une profondeur de 2
    v: dict,  # dict à construire de manière récurrente
    value: float,  # valeur à assigner
    display_value: str,
):
    v_tags = {}
    for i, key in enumerate(fluxTags.keys()):
        v_tags[key] = link_flux_tags[i]

    if depth == len(link_data_tags):
        v["value"] = value
        v["display_value"] = display_value
        v["tags"] = v_tags
        v["extension"] = {}
    else:
        data_tag = link_data_tags[depth]
        if data_tag not in v:
            v[data_tag] = {}
        set_link_value_sankey(
            link_data_tags,
            link_flux_tags,
            fluxTags,
            depth + 1,
            v[data_tag],
            value,
            display_value,
        )


async def get_confidentiality_sankey(region, code, type_territoire, annee):
    try:
        sql_confid = """
                select *
                from {schema}.sankey_confidentiality
                where code_territoire = $1
                and type_territoire = $2
                and annee = $3
                """.format(
            schema=region.replace("-", "_")
        )
        res_confid = await controller.fetch(sql_confid, code, type_territoire, annee)
        res_confid = [dict(x) for x in res_confid]
        links_concerned = set(
            [(s["energie"], s["secteur"]) for s in res_confid]
            + [("-", s["energie"]) for s in res_confid]
        )
        return links_concerned
    # when no confidentiality table has been defined, we use main confid tables
    # to estimate if there is any rule corresponding to current territory
    # if yes => we disable the whole graph
    # otherwise => full display
    except asyncpg.exceptions.UndefinedTableError as e:
        sql_confid = """
                select *
                from {schema}.confid_maille
                where code = $1
                and type_territoire = $2
                and annee = $3
                and type_analyse = ANY($4)
                """.format(
            schema=region.replace("-", "_")
        )
        res_confid = await controller.fetch(
            sql_confid, code, type_territoire, annee, ["conso_energetique"]
        )
        return len(res_confid) > 0


def replace_display_tokens(
    target_dict: dict, fields: list[str], token_map: dict
) -> None:
    for field in fields:
        if field in target_dict:
            for old_token, new_token in token_map.items():
                target_dict[field] = target_dict[field].replace(old_token, new_token)


async def generate_json(region, table, type_territoire, code):
    """
    Génération du fichier json pour l'affichage des diagrammes de Sankey
    """

    sql_config = """
                select year, COALESCE(division_factors, '{}'::json) as division_factors,
                COALESCE(division_units, '{}'::json) as division_units,
                nb_decimals
                from meta.sankey_metadata
                where data_table = $1 AND region = $2
                """

    res_config = await controller.fetch(sql_config, table, region)

    annee = [x["year"] for x in res_config][0]
    config = [dict(x) for x in res_config][0]

    # we try loading layout file
    main_path_to_files = settings.get("api", "upload_other_files", fallback="/tmp")
    layout_path = f"{main_path_to_files}/{region}_{slugify.slugify(table)}.json"
    if not os.path.isfile(layout_path):
        return {"statut": "layout_unavailable"}

    nb_decimals = config.get("nb_decimals", False)
    factor_multi = config.get("division_factors", False)
    if factor_multi:
        factor_multi = json.loads(factor_multi).get(type_territoire, 1.0)
    else:
        factor_multi = 1.0

    res_confid = await get_confidentiality_sankey(region, code, type_territoire, annee)

    if isinstance(res_confid, bool) and res_confid:
        return {"statut": "confidentiel"}

    sql = """
          select source, target, colors, t.code, sum(valeur)*$2 as value, t.nom as nom_territoire, t.type_territoire
          from {schema}.{table} a
          join {schema}.territoire t on t.commune = a.code
          where t.code = $1
          group by source, target, colors, t.code, t.nom, t.type_territoire;
          """.format(
        schema=region.replace("-", "_"), table=table, type_territoire=type_territoire
    )
    res = await controller.fetch(sql, code, float(factor_multi))

    # if we have no data, we stop right here
    if len(res) == 0:
        return {"statut": "no_data"}

    csv_data = pd.DataFrame([dict(i) for i in res])
    csv_data = apply_specific_treatment(csv_data, region, type_territoire, table)
    csv_data.loc[csv_data["value"] < 0, "value"] = 0

    max_value = csv_data["value"].max()
    min_value = max(0.0, csv_data["value"].min()) * 0.9

    layout_file = open(layout_path, encoding="utf-8", mode="r")
    layout_data = json.load(layout_file)

    # Convert new versions of OpenSankey layout to current style
    tokens_replace = {"bottom": "bas", "top": "haut", "middle": "milieu"}
    for _, node in layout_data["nodes"].items():
        if "display_style" in node:
            replace_display_tokens(
                node["display_style"], ["label_vert", "label_horiz"], tokens_replace
            )
        elif "local" in node:
            replace_display_tokens(
                node["local"],
                ["label_vert_valeur", "label_horiz_valeur"],
                tokens_replace,
            )

    nodes = {}
    links = {}
    nodes_names = np.unique(
        np.hstack((csv_data["source"], csv_data["target"]))
    ).tolist()
    nodes_names_layout = layout_data["nodes"]
    links_names_layout = layout_data["links"]
    # TODO: ajouter le calcul du max ID pour les cas de noeuds et de liens non répertoriés
    # max_id_node = [
    #     key for key in nodes_names_layout if nodes_names_layout[key]['name'].strip() == node_name.strip()
    # ]
    nodes_ids = {}

    for node_name in nodes_names:
        corresponding_nodes = [
            key
            for key in nodes_names_layout
            if nodes_names_layout[key]["name"].strip() == node_name.strip()
        ]
        if len(corresponding_nodes) == 0:
            continue
        idNode = corresponding_nodes[0]
        nodes_ids[node_name] = idNode
        nodes[idNode] = {
            "idNode": idNode,
            "color": "grey",
            "name": node_name,
            "type": "sector",
            "definition": "",
            "dimensions": {"Primaire": {"level": 1}},
            "label_visible": 1,
            "shape_visible": 1,
            "display": 1,
            "node_visible": 1,
        }

    territory_data = csv_data[csv_data["code"] == code].to_dict("records")
    link_data_tags = []
    link_flux_tags = []
    dataTags = {}
    nodeTags = {}
    fluxTags = {}

    i = 0
    for row in territory_data:
        source_name = row["source"]
        target_name = row["target"]
        source_nodes = [
            nodes[key]
            for key in nodes.keys()
            if nodes[key]["name"].strip() == source_name.strip()
        ]
        if len(source_nodes) == 0:
            continue
        source_node = source_nodes[0]
        target_nodes = [
            nodes[key]
            for key in nodes.keys()
            if nodes[key]["name"].strip() == target_name.strip()
        ]
        if len(target_nodes) == 0:
            continue
        target_node = target_nodes[0]
        corresponding_links = [
            key
            for key in links_names_layout
            if links_names_layout[key]["idSource"] == source_node["idNode"]
            and links_names_layout[key]["idTarget"] == target_node["idNode"]
        ]
        if len(corresponding_links) == 0:
            continue
        idLink = corresponding_links[0]

        # if we have confidentiality on this flow
        if res_confid == True or (
            not isinstance(res_confid, bool)
            and (
                (source_name, target_name) in res_confid
                or ("-", target_name) in res_confid
            )
        ):
            if row["value"] == 0:
                continue
            value = {}
            display_value = "confidentielle"
            set_link_value_sankey(
                # link_data_tags
                link_data_tags,
                # link_flux_tags
                link_flux_tags,
                # fluxTags
                fluxTags,
                # depth
                0,
                # v
                value,
                # value
                2.0,
                # display_value
                display_value,
            )
            dashed = True

        else:
            value = {}
            if nb_decimals == 0:
                display_value = int(round(row["value"], 0))
            else:
                display_value = round(row["value"], nb_decimals)
            display_value = str(display_value)
            # we just handle case where only one value is available
            if max_value == min_value:
                current_value = 10.0
            else:
                current_value = round(
                    10.0 * (row["value"] - min_value) / (max_value - min_value), 6
                )
            set_link_value_sankey(
                # link_data_tags
                link_data_tags,
                # link_flux_tags
                link_flux_tags,
                # fluxTags
                fluxTags,
                # depth
                0,
                # v
                value,
                # value
                current_value,
                # display_value
                display_value,
            )

            dashed = False

        new_link = {
            "idLink": idLink,
            "idSource": source_node["idNode"],
            "idTarget": target_node["idNode"],
            "value": value,
            "display_value": display_value,
            "dashed": dashed,
        }

        if "colors" in row and row["colors"] != "":
            new_link["color"] = row["colors"]

        links[new_link["idLink"]] = new_link

        i += 1

    return {
        "version": "0.8",
        "dataTags": dataTags,
        "nodeTags": nodeTags,
        "fluxTags": fluxTags,
        "nodes": nodes,
        "links": links,
        "labels": {},
        "layout": layout_data,
    }


def parse_configuration(d):
    """
    Parse the configuration dictionary to cast some fields and fix some values.

    Parameters
    ----------
    d : dict
        the dictionary containing main field of configuration

    Returns
    -------
    dict
        the dictionary parsed
    """
    geo_levels = d.get("geographical_levels_enabled", None)
    if geo_levels is not None:
        d["geographical_levels_enabled"] = geo_levels.split(",")
    else:
        d["geographical_levels_enabled"] = []
    return d


async def get_meta_data(region, sankey_chosen=None, all=False):
    if sankey_chosen is not None:
        condition_sankey = """ AND data_table = $2"""
    elif not all:
        condition_sankey = """ AND is_regional_default = true
                    LIMIT 1"""
    else:
        condition_sankey = ""
    meta_data = f"""select year, unit, introduction_text, source, copyright,
                COALESCE(division_factors, '{{}}'::json) as division_factors,
                COALESCE(division_units, '{{}}'::json) as division_units,
                sankey_name, is_regional_default, data_table, nb_decimals,
                geographical_levels_enabled
                from meta.sankey_metadata
                where region = $1
                {condition_sankey}
                """
    if sankey_chosen is not None:
        res_region = await controller.fetch(meta_data, region, sankey_chosen)
    else:
        res_region = await controller.fetch(meta_data, region)
    return [parse_configuration(dict(x)) for x in res_region]


async def delete_diagram(region, sankey_table):
    main_path_to_files = settings.get("api", "upload_other_files", fallback="/tmp")
    filename = f"{main_path_to_files}/{region}_{slugify.slugify(sankey_table)}.json"

    if os.path.isfile(filename):
        os.remove(filename)

    schema = region.replace("-", "_")
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.execute(
                f"DROP TABLE {schema}.{sankey_table};",
            )
            await conn.execute(
                f"DELETE FROM meta.sankey_metadata WHERE region = $1 AND data_table = $2",
                region,
                sankey_table,
            )


async def update_meta_data(region, sankey_table, new_data, default_data):
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            if new_data.get("is_regional_default", default_data["is_regional_default"]):
                await conn.execute(
                    "UPDATE meta.sankey_metadata SET is_regional_default = false WHERE region = $1",
                    region,
                )
            geographical_levels = new_data.get(
                "geographical_levels_enabled",
                default_data["geographical_levels_enabled"],
            )
            if isinstance(geographical_levels, list):
                geographical_levels = ",".join(geographical_levels)
            meta_data = f"""
                UPDATE meta.sankey_metadata
                SET year = $3,
                unit = $4,
                introduction_text = $5,
                source = $6,
                copyright = $7,
                division_factors = $8,
                division_units = $9,
                sankey_name = $10,
                is_regional_default = $11,
                nb_decimals = $12,
                geographical_levels_enabled = $13
                WHERE region = $1 AND data_table = $2
            """
            await conn.execute(
                meta_data,
                region,
                sankey_table,
                int(new_data.get("year", default_data["year"])),
                new_data.get("unit", default_data["unit"]),
                new_data.get("introduction_text", default_data["introduction_text"]),
                new_data.get("source", default_data["source"]),
                new_data.get("copyright", default_data["copyright"]),
                json.dumps(
                    new_data.get("division_factors", default_data["division_factors"])
                ),
                json.dumps(
                    new_data.get("division_units", default_data["division_units"])
                ),
                new_data.get("sankey_name", default_data["sankey_name"]),
                bool(
                    new_data.get(
                        "is_regional_default", default_data["is_regional_default"]
                    )
                ),
                int(new_data.get("nb_decimals", default_data["nb_decimals"])),
                geographical_levels,
            )


async def add_new_metadata(region, new_data):
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            if new_data.get("is_regional_default", False):
                await conn.execute(
                    "UPDATE meta.sankey_metadata SET is_regional_default = false WHERE region = $1",
                    region,
                )
            geographical_levels = new_data.get(
                "geographical_levels_enabled",
                "",
            )
            if isinstance(geographical_levels, list):
                geographical_levels = ",".join(geographical_levels)
            meta_data = f"""
                INSERT INTO meta.sankey_metadata (region, data_table, year, unit, introduction_text, source, copyright, 
                    division_factors, division_units, sankey_name, is_regional_default, nb_decimals, geographical_levels_enabled)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13)
            """

            today = datetime.date.today()
            data_table = utils.parse_table_name(new_data["data_table"])

            res_region = await conn.execute(
                meta_data,
                region,
                data_table,
                int(new_data.get("year", today.year)),
                new_data.get("unit", ""),
                new_data.get("introduction_text", ""),
                new_data.get("source", ""),
                new_data.get("copyright", False),
                json.dumps(new_data.get("division_factors", [])),
                json.dumps(new_data.get("division_units", [])),
                new_data.get("sankey_name", ""),
                new_data.get("is_regional_default", False),
                int(new_data.get("nb_decimals", 0)),
                geographical_levels,
            )

            schema = region.replace("-", "_")
            await conn.execute(
                f"""CREATE TABLE {schema}.{data_table}
                        (source character varying NOT NULL,
                        target character varying NOT NULL,
                        id_source integer,
                        id_target integer,
                        colors character varying,
                        label character varying,
                        code character varying NOT NULL,
                        valeur double precision NOT NULL)"""
            )
            await conn.execute(
                f"""CREATE INDEX IF NOT EXISTS {schema}_{data_table}_idx ON {schema}.{data_table} (code)""",
            )


async def update_data(region, sankey_table, filename):
    schema = region.replace("-", "_")
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.execute(f"TRUNCATE {schema}.{sankey_table};")
            result = await conn.copy_to_table(
                sankey_table,
                source=filename,
                schema_name=schema,
                header=True,
                format="csv",
                delimiter=";",
            )
            # we check we have added at least on row otherwise it is weird
            check = await conn.fetch(f"SELECT COUNT(*) FROM {schema}.{sankey_table}")
            return check[0]["count"] > 0


def apply_specific_treatment(data, region, type_territoire, table):
    if (
        region == "auvergne-rhone-alpes"
        and type_territoire != "commune"
        and table == "sankey_energie"
    ):
        return apply_import_export_balance(data)
    else:
        return data


def apply_import_export_balance(data):
    # OIL PRODUCTS IMPORTS
    # we get the caracteristics of the import and export flows

    reffinery_output_table = data.loc[
        -(data["target"] == "Pertes ou torchage") & (data["source"] == "Raffinerie")
    ]
    oil_consumption_table = data.loc[
        (data["source"] == "Produits pétroliers secondaires (intermédiaires)")
    ]

    reffinery_output = reffinery_output_table["value"].sum()
    oil_consumption = oil_consumption_table["value"].sum()

    if reffinery_output > 0:
        self_use_oil = min(oil_consumption, reffinery_output)
        oil_import = max(oil_consumption - self_use_oil, 0)
        corrected_reffinery_output = reffinery_output - self_use_oil

        # we remove according data from the data flow
        data = data.loc[  # remove oil_import
            ~(
                (data["source"] == "Produits pétroliers secondaires (imports)")
                & (data["target"] == "Produits pétroliers secondaires (intermédiaires)")
            )
        ]
        data = data.loc[  # remove reffinery_output
            ~(
                (data["source"] == "Raffinerie")
                & (data["target"] == "Exportations (produits pétroliers)")
            )
        ]
        data = data.loc[  # remove self_use_oil
            ~(
                (data["source"] == "Raffinerie")
                & (data["target"] == "Produits pétroliers secondaires (intermédiaires)")
            )
        ]

        # Add corrected values

        data = pd.concat(
            [
                data,
                pd.DataFrame(  # Add oil_import
                    {
                        "source": "Produits pétroliers secondaires (imports)",
                        "target": "Produits pétroliers secondaires (intermédiaires)",
                        "value": oil_import,
                        "colors": oil_consumption_table.iloc[0]["colors"],
                        "code": oil_consumption_table.iloc[0]["code"],
                        "nom_territoire": oil_consumption_table.iloc[0][
                            "nom_territoire"
                        ],
                        "type_territoire": oil_consumption_table.iloc[0][
                            "type_territoire"
                        ],
                    },
                    index=[0],
                ),
            ],
            ignore_index=True,
        )

        data = pd.concat(
            [
                data,
                pd.DataFrame(  # Add reffinery_output
                    {
                        "source": "Raffinerie",
                        "target": "Exportations (produits pétroliers)",
                        "value": corrected_reffinery_output,
                        "colors": reffinery_output_table.iloc[0]["colors"],
                        "code": reffinery_output_table.iloc[0]["code"],
                        "nom_territoire": reffinery_output_table.iloc[0][
                            "nom_territoire"
                        ],
                        "type_territoire": reffinery_output_table.iloc[0][
                            "type_territoire"
                        ],
                    },
                    index=[0],
                ),
            ],
            ignore_index=True,
        )

        data = pd.concat(
            [
                data,
                pd.DataFrame(  # Add self_use_oil
                    {
                        "source": "Raffinerie",
                        "target": "Produits pétroliers secondaires (intermédiaires)",
                        "value": self_use_oil,
                        "colors": reffinery_output_table.iloc[0]["colors"],
                        "code": reffinery_output_table.iloc[0]["code"],
                        "nom_territoire": reffinery_output_table.iloc[0][
                            "nom_territoire"
                        ],
                        "type_territoire": reffinery_output_table.iloc[0][
                            "type_territoire"
                        ],
                    },
                    index=[0],
                ),
            ],
            ignore_index=True,
        )

    else:
        # If no reffinery_output

        oil_import = oil_consumption

        data = data.loc[  # remove oil_import
            ~(
                (data["source"] == "Produits pétroliers secondaires (imports)")
                & (data["target"] == "Produits pétroliers secondaires (intermédiaires)")
            )
        ]

        data = pd.concat(
            [
                data,
                pd.DataFrame(  # Add oil_import
                    {
                        "source": "Produits pétroliers secondaires (imports)",
                        "target": "Produits pétroliers secondaires (intermédiaires)",
                        "value": oil_import,
                        "colors": oil_consumption_table.iloc[0]["colors"],
                        "code": oil_consumption_table.iloc[0]["code"],
                        "nom_territoire": oil_consumption_table.iloc[0][
                            "nom_territoire"
                        ],
                        "type_territoire": oil_consumption_table.iloc[0][
                            "type_territoire"
                        ],
                    },
                    index=[0],
                ),
            ],
            ignore_index=True,
        )

    # ELECTRICITY
    # we get the caracteristics of the import and export flows
    elec_import = data.loc[
        (data["target"] == "Électricité (vecteur)")
        & (data["source"] == "Électricité (imports)")
    ]
    elec_export = data.loc[
        (data["source"] == "Électricité (vecteur)") & (data["target"] == "Exportations")
    ]

    # we compute the difference between production and consumption
    elec_prod_data = data.loc[
        (data["target"] == "Électricité (vecteur)")
        & (data["source"] != "Électricité (imports)")
    ]
    elec_consumption_data = data.loc[
        (data["source"] == "Électricité (vecteur)") & (data["target"] != "Exportations")
    ]
    import_export = elec_prod_data["value"].sum() - elec_consumption_data["value"].sum()

    # we remove according data from the data flow
    data = data.loc[
        ~(
            (data["source"] == "Électricité (imports)")
            & (data["target"] == "Électricité (vecteur)")
        )
        & ~(
            (data["source"] == "Électricité (vecteur)")
            & (data["target"] == "Exportations")
        )
    ]
    # if we produced less than we consumed, we need to import
    if import_export < 0:
        data = pd.concat(
            [
                data,
                pd.DataFrame(
                    {
                        "source": "Électricité (imports)",
                        "target": "Électricité (vecteur)",
                        "value": -import_export,
                        "colors": elec_import.iloc[0]["colors"],
                        "code": elec_import.iloc[0]["code"],
                        "nom_territoire": elec_import.iloc[0]["nom_territoire"],
                        "type_territoire": elec_import.iloc[0]["type_territoire"],
                    },
                    index=[0],
                ),
            ],
            ignore_index=True,
        )
    # otherwise, we need to export
    elif import_export > 0:
        data = pd.concat(
            [
                data,
                pd.DataFrame(
                    {
                        "source": "Électricité (vecteur)",
                        "target": "Exportations",
                        "value": import_export,
                        "colors": elec_export.iloc[0]["colors"],
                        "code": elec_export.iloc[0]["code"],
                        "nom_territoire": elec_export.iloc[0]["nom_territoire"],
                        "type_territoire": elec_export.iloc[0]["type_territoire"],
                    },
                    index=[0],
                ),
            ],
            ignore_index=True,
        )
    return data
