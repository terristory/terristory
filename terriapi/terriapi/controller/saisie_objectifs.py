﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
from __future__ import annotations

import asyncpg

from terriapi.controller import execute, executemany, fetch


async def check_goal_exists(region: str, supra_goal_id: int) -> bool:
    req_delete_goal = """
        SELECT EXISTS (
            SELECT * FROM strategie_territoire.supra_goals
            WHERE supra_goal_id = $1
            AND region = $2
        )
        """
    res = await fetch(req_delete_goal, int(supra_goal_id), region)
    return res[0]["exists"]


async def supprimer_objectif(region: str, supra_goal_id: int):
    """Supprime en base de données un objectif

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    titre : str
        Titre de l'objectif à supprimer
    graphique : str
        Nom du graphique auquel s'appliquait l'objectif à supprimer
    """
    req_delete_goal = """
        DELETE FROM strategie_territoire.supra_goals
        WHERE supra_goal_id = $1
        AND region = $2;
    """
    await fetch(req_delete_goal, int(supra_goal_id), region)


async def ajout_objectif(region: str, donnees: dict, supra_goal_id: int | None = None):
    """Ajoute un objectif en base de données

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    donnees : {
        "params": dict,
        "obj": list[{ "annee": int, "valeur": float, "annee_modifiee": bool }],
        "link": list[{ "code_territoire": str, "type_territoire": str }]
    }
    """
    donnees_trajectoire = donnees["obj"]
    donnees_territoires = donnees["link"]
    sql_integration_prametres_objectif = """
        INSERT INTO strategie_territoire.supra_goals
        (titre, annee_reference, graphique, couleur, region, derniere_annee_projection, description, filter, supra_goal_id)
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8,
            COALESCE($9, NEXTVAL('strategie_territoire.supra_goals_supra_goal_id_seq'))
        )
        RETURNING supra_goal_id;
    """

    try:
        res = await fetch(
            sql_integration_prametres_objectif,
            donnees["params"]["titre"],
            int(donnees["params"]["annee"]),
            donnees["params"]["graphiques"],
            donnees["params"]["couleur"],
            region,
            donnees["params"]["derniereAnneeProjection"],
            donnees["params"]["description"],
            donnees["params"]["filter"],
            int(supra_goal_id) if supra_goal_id else None,
        )
        if supra_goal_id is None:
            supra_goal_id = res[0]["supra_goal_id"]
    except asyncpg.exceptions.UniqueViolationError:
        return {
            "message": "Il existe déjà un objectif associé à ce graphique qui porte ce nom",
            "status": 400,
        }

    for i in donnees_trajectoire:
        sql_integration_objectif = """
            INSERT INTO strategie_territoire.supra_goals_values
            (annee, valeur, annee_modifiee, supra_goal_id)
            VALUES ($1, $2, $3, $4);
        """
        await execute(
            sql_integration_objectif,
            int(i["annee"]),
            float(i["valeur"]),
            i.get("annee_modifiee", False),
            int(supra_goal_id),
        )

    for donnee in donnees_territoires:
        if not donnee.get("code_territoire"):
            donnee["code_territoire"] = ""
            donnee["type_territoire"] += "s"
    sql_assoctiation_territoire = """
        INSERT INTO strategie_territoire.supra_goals_links
        (type_territoire, code_territoire, supra_goal_id)
        values ($1, $2, $3);
    """
    await executemany(
        sql_assoctiation_territoire,
        [
            (i["type_territoire"], i["code_territoire"], int(supra_goal_id))
            for i in donnees_territoires
        ],
    )
    return {"message": "Objectif ajouté", "status": 200, "supra_goal_id": supra_goal_id}


async def allow_supra_goal_display_for_territory(
    region: str,
    supra_goal_id: int,
    type_territoire: str,
    code_territoire: str,
    nom_territoire: str,
    tout_type_territoire: bool,
):
    """Autorise l'affichage de l'objectif en mode valeur pour une échelle géographique / territoire spécifique à indiquer en paramètre

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    type_territoire : str
        Type de territoire sélectionné par l'utilisateur
    code_territoire : str
        code du territoire sélectionné par un utilisateur (si on autorise pour un type de territoire, le code du territoire est vide)
    nom_territoire : str
        Nom du territoire sélectionné par un utilisateur (si on autorise pour un type de territoire, le nom du territoire est vide)
    tout_type_territoire : Booléen
        Indique si on désactive l'affichage pour un type de territoire ou pour un territoire spécifique
    """
    req = """
          insert into strategie_territoire.supra_goals_links
          (type_territoire, code_territoire, supra_goal_id)
          values ($1, $2, $3);
          """
    try:
        await fetch(
            req,
            type_territoire,
            code_territoire,
            supra_goal_id,
        )
    except asyncpg.exceptions.UniqueViolationError:
        return {
            "message": "L'affichage des objectifs en mode valeur est déjà autorisé pour cette maille / ce territoire",
            "status": 400,
        }
    message = (
        "Cet objectif sera affiché en mode valeur pour le territoire " + nom_territoire
    )
    if tout_type_territoire:
        message = "Cet objectif sera affiché en mode valeur pour les " + type_territoire
    return {"message": message, "status": 200}


async def suppression_affectation_maille_affichage_objectifs_valeur(
    region: str,
    supra_goal_id: int,
    type_territoire: str,
    code_territoire: str,
    nom_territoire: str,
    tout_type_territoire: bool,
):
    """Désactive l'affichage de l'objectif en mode valeur pour les échelles géographiques / territoire spécifique à indiquer en paramètre

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    type_territoire : str
        Type de territoire sélectionné par l'utilisateur
    code_territoire : str
        code du territoire sélectionné par un utilisateur (si on autorise pour un type de territoire, le code du territoire est vide)
    nom_territoire : str
        Nom du territoire sélectionné par un utilisateur (si on autorise pour un type de territoire, le nom du territoire est vide)
    tout_type_territoire : Booléen
        Indique si on désactive l'affichage pour un type de territoire ou pour un territoire spécifique
    """
    req = """
          delete from strategie_territoire.supra_goals_links
          where supra_goal_id = $1
          AND (SELECT region FROM strategie_territoire.supra_goals WHERE supra_goal_id = $1) = $2
          and type_territoire = $3
          and code_territoire = $4;
          """

    await fetch(
        req,
        supra_goal_id,
        region,
        type_territoire,
        code_territoire,
    )
    message = (
        "Cet objectif ne sera plus affiché en mode valeur pour le territoire "
        + nom_territoire
    )
    if tout_type_territoire:
        message = (
            "Cet objectif ne sera plus affiché en mode valeur pour les "
            + type_territoire
        )
    return {"message": message, "status": 200}


async def liste_affectation_maille_affichage_objectifs_valeur(
    region: str, supra_goal_id: int
):
    """Retourne la liste des territoires / types de territoires pour lesquels on autorise l'affichage d'un objectif en mode valeur

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    titre : str
        Titre du graphique pour lequel on liste les territoires / types territoires concernés par l'autorisation
    graphique :
        Nom de la table à partir de laquelle on obtient les données de références la trajectoire qui définit l'objectif

    Returns
    --------
    Dictionnaire dont une clé est associée à la liste des affectations et une autre qui correspond au statut.
    """
    req = """
          SELECT l.*
          FROM strategie_territoire.supra_goals_links l
          LEFT JOIN strategie_territoire.supra_goals p
          ON l.supra_goal_id = p.supra_goal_id
          WHERE p.supra_goal_id = $1 AND p.region = $2
          """

    res = await fetch(req, int(supra_goal_id), region)
    res_liste = [dict(x) for x in res]
    return {"donnees": res_liste, "status": 200}
