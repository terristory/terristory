# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

# class DonneesPourRepresentations
# class DonneesPourRepresentationCartoIndicateurFiltre
# class DonneesPourRepresentationCartoIndicateurSimple
# class DonneesPourRepresentationCartoIndicateurFlux
# class DonneesPourRepresentationCartoIndicateurDataRatio
# class DonneesPourRepresentationCartoIndicateurAutreMaille
# class DonneesPourRepresentationCartoMoyennePonderee
# class DonneesPourRepresentationCartoIndicateurClimat
# class DonneesPourRepresentationDiagramme
# class DonneesPourRepresentationDiagrammeCategoriesIndependantes
# class DonneesPourRepresentationCourbesEmpilees
# class DonneesPourRepresentationJauge
# class DonneesPourRepresentationJaugeCirculaire
# class DonneesPourRepresentationHistogramme
# class DonneesPourRepresentationHistogrammeIndicateurUnique
# class DonneesPourRepresentationHistogrammeDataRatio
# class DonneesPourRepresentationCourbesHistoriques
# class DonneesPourRepresentationMarqueurSVG
# class DonneesPourRepresentationPixels
# class DonneesPourRepresentationPixelsCategory
# class DonneesPourRepresentationNomSimple
from dataclasses import dataclass

from terriapi.controller import execute, fetch


@dataclass
class GeoMesh:
    code: str


@dataclass
class GeoContext:
    region: str
    zone_id: str
    zone_type: str
    mesh: GeoMesh


@dataclass
class Indicator:
    id: int
    name: str
    data_type: str
    data_table: str
    formula: str

    @property
    def table(self) -> str:
        return self.data_table


@dataclass
class Years:
    year: int
    years: list


@dataclass
class Category:
    name: str
    modalities: list


async def get_list_mesh(geo_context: GeoContext) -> list[GeoMesh]:
    await fetch(f"""SELECT * FROM """)
    return


@dataclass
class GeoData:
    geo_context: GeoContext
    indicator: Indicator
    years: Years
    categories: list[Category]

    def _table(self, mesh: str) -> str:
        return self.indicator.table.replace("#mesh#", mesh).replace("#maille#", mesh)

    def build_query(self, mesh: str, list_mesh: list):
        return f"""SELECT {mesh}, valeur 
        FROM {self._table(mesh)}
        WHERE {mesh} = ANY($1)"""

    def get_data(self):
        query = self.list_mesh(self.geo_context)
        query = self.build_query(
            self.geo_context.mesh,
        )
