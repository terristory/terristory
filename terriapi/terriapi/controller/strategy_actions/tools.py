# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd


def _calcul_baisse_emission_et_conso_par_secteur(
    dataframe: pd.DataFrame,
    baisse_conso_secteur_coef_multiplicateur,
    column_as_key="nom",
    default_key=False,
):
    """Retourne la baisse des émissions GES ou de la consommation énergétique en fonction du secteur en entrée.

    Parameters
    ----------
    dataframe : DF
        Data Frame contenant les émissions de GES ou la consommation énergétique.
    baisse_conso_secteur_coef_multiplicateur : DF
        Coefficient multiplicateur à appliquer pour effectuer une baisse.

    Returns
    -------
    dict
    """
    if isinstance(dataframe, bool) and not dataframe:
        return {default_key: 0}
    column_as_key = str(column_as_key)
    dictionnaire = {}
    for i in range(dataframe.shape[0]):
        dictionnaire[dataframe.iloc[i][column_as_key]] = (
            dataframe.iloc[i]["valeur"] * baisse_conso_secteur_coef_multiplicateur
        )
    return dictionnaire


# cette fonction permet de calculer la consommation totale pour chaque année
# suivant la formule suivante totale[year] = totale[year-1] * (1 - (pourcentage à déduire))
# elle prend en paramètre l'année, consommation des énergie depuis la BD et les parmètres entrées par l'utilisateur (des pourcentages à déduire)
# et retourne le total pour l'année passée en paramètre
def calculate_conso_totale(year, user_params, conso_usage_energie):
    """Retourne la consommation totale pour chaque année (2019 => 2050)

    Parameters
    ----------
    year : int
        l'année qui correspond au pourcentage entré par l'utilisateur
    user_params : int
        le pourcentage de réduction entré par l'utilisateur
    conso_usage_energie : dataframe
        la consommation par type d'énergie

    Returns
    --------
    totale : float
        la consommation totale de l'année passé en paramétre

    """
    # récupérer la première année des paramétres entrés par l'utilisateur
    first_year = min(map(int, user_params.keys()))
    # récuperer la consommation totale qui correspond à la première année de disponibilité de données (année de référence)
    totale = conso_usage_energie.valeur.sum()
    # la consommation totale de l'année de référence
    totale_reference = totale
    while int(year) >= int(
        first_year
    ):  # calculer le total depuis la premiere année de disponibilité de données jusqu'à l'année passé en paramètre
        totale = totale - (
            totale_reference * (int(user_params[str(int(first_year))]) / 100)
        )
        first_year += 1

    return totale
