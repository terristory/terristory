# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
import inspect
import os
from pathlib import Path

from .constants import (
    Commodity,
    DataSet,
    ParamCommodity,
    ParamDataSet,
    RenewableProd,
    Sector,
    Usage,
)
from .impacts import AbstractImpact, Impacts
from .tests.testing import ActionsTester

here = Path(__file__).parent

CONSTANTS = {
    Commodity,
    Sector,
    Usage,
    RenewableProd,
    DataSet,
    ParamDataSet,
    ParamCommodity,
}


def get_file_content(action_key):
    if "." in action_key:
        raise ValueError("Impossible to have . in action key (security risks).")
    if "/" in action_key:
        action_key = action_key.split("/")[0]
    path = here / "actions/" / (action_key + ".py")
    if os.path.isfile(path):
        with open(path, "r") as f:
            return f.read()
    else:
        return None


def find_constants_usage(file):
    constants_used = set()
    # retrieve constants used in the file
    for constant in CONSTANTS:
        for val in constant:
            if str(val) in file:
                constants_used.add(str(val))
    return constants_used


def find_outputs(file):
    outputs = set()
    # show output elements guessed
    for name, impact in Impacts.__dict__.items():
        # if we have an impact class, we check it is in the file
        if (
            inspect.isclass(impact)
            and issubclass(impact, AbstractImpact)
            and "Impacts." + name in file
        ):
            outputs.add(name)

    return outputs
