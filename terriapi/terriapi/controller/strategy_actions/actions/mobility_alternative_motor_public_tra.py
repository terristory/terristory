# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    Requisite,
    Sector,
    VehicleFuel,
    VehicleType,
)
from terriapi.controller.strategy_actions.errors import StrategyException
from terriapi.controller.strategy_actions.loaders.specific_loaders import nb_bus_cars


class AlternativeMotorizationPublicTransports(AbstractAction):
    INPUT_ELEC_BUS = "20_bus_elec"
    INPUT_NGV_BUS = "20_bus_gnv"
    INPUT_DIESEL_BUS = "20_bus_diesel"

    PARAM_AVG_DIST_PER_BUS = "Distance moyenne parcourue par an par bus/car"
    PARAM_PERC_BIO_NGV = "Pourcentage d'intégration du BioGNV dans les véhicules GNV"

    BUS_DISTRIBUTION = "bus_car_distribution"

    def __init__(self):
        super().__init__("Alternative motorization of public transports", "20")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions ATMO",
            self.PARAM_FEGES_ATMO,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.ATMO_GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_AVG_DIST_PER_BUS,
            self.PARAM_AVG_DIST_PER_BUS,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_PERC_BIO_NGV,
            self.PARAM_PERC_BIO_NGV,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_ELEC_BUS,
            self.INPUT_ELEC_BUS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_NGV_BUS,
            self.INPUT_NGV_BUS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_DIESEL_BUS,
            self.INPUT_DIESEL_BUS,
            Requisite.Types.INPUT,
        )
        self.add_data_requisite("vehicles_fleet", DataSet.VEHICLE_FLEET, default=False)

    async def _pre_execute(self, *args, **kwargs):
        region = kwargs.get("region", None)
        zone = kwargs.get("zone", None)
        zone_id = kwargs.get("zone_id", None)
        if region is None or zone is None or zone_id is None:
            raise StrategyException(
                "No region, zone or zone_id specified for pre-execute function."
            )
        # TODO: use computation_function ? in params parameter
        self.data[self.BUS_DISTRIBUTION] = await nb_bus_cars(region, zone, zone_id)

    async def _execute(self, year, *args, **kwargs):
        """
        Calcul d'impact GES, NRJ et facture NRJ
        pour l'action de motorisation alternative des transports en commun
        """

        # parametre de configuration
        enable_air_pollutants_impacts = self.config.get(
            "enable_air_pollutants_impacts", False
        )
        vehicles_fleet = self.data["vehicles_fleet"]
        if not isinstance(vehicles_fleet, bool) and not vehicles_fleet.empty:
            # TODO: not used
            nb_bus_existant = vehicles_fleet.loc[
                (vehicles_fleet.VEHICLETYPE == VehicleType.BUSES_COACHES), "valeur"
            ]
            nb_bus_diesel_existant = float(
                vehicles_fleet.loc[
                    (vehicles_fleet.VEHICLETYPE == VehicleType.BUSES_COACHES)
                    & (vehicles_fleet.VEHICLEFUEL == VehicleFuel.DIESEL),
                    "valeur",
                ]
            )
        else:
            nb_buscars_territoire = self.data[self.BUS_DISTRIBUTION]
            total = next(
                (x["nb"] for x in nb_buscars_territoire if x["energie"] == "total"),
                None,
            )
            diesel = next(
                (x["nb"] for x in nb_buscars_territoire if x["energie"] == "Gazole"),
                None,
            )
            if total is None or diesel is None:
                raise StrategyException("Not enough bus/cars")

            # TODO: not used
            nb_bus_existant = total
            nb_bus_diesel_existant = diesel

        # les parametres d'entrées par l'utilisateur
        """
        A : Pourcentage de remplacement du parc de bus/cars existants par des bus électriques (%)
        B : Pourcentage de remplacement du parc de bus/cars existants par des bus GNV / bioGNV (%)
        C : Pourcentage de remplacement du parc de bus/cars initial par des bus/cars diesel nouvelle génération (%)
        """
        A = float(self.inputs[self.INPUT_ELEC_BUS][year]) / 100
        B = float(self.inputs[self.INPUT_NGV_BUS][year]) / 100
        C = float(self.inputs[self.INPUT_DIESEL_BUS][year]) / 100

        if A == 0 and B == 0 and C == 0:
            return {
                Impacts.EnergyConsumption(0.0, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.AvoidedEmissions(0.0, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        prix = prix[prix["secteur"] == "Indifférencié"]
        # facteurs d'émission polluants / ges / conso ATMO
        fe_atmo = self.params[self.PARAM_FEGES_ATMO]

        """
        E : Distance moyenne parcourue par an par bus/car (km)
        J : Pourcentage d'intégration du BioGNV dans les véhicules GNV (%)
        K : Durée de vie des bus et car (nombre d'années)
        
        """
        E = float(self.input_params[self.PARAM_AVG_DIST_PER_BUS])

        J = float(self.input_params[self.PARAM_PERC_BIO_NGV])

        """
        Depuis les variables d'émissions ATMO
        G : Consommation moyenne nouveau bus et car électrique (kWh/km)
        H : Consommation moyenne nouveau bus et car GNV/BioGNV (kWh/km)
        F : Consommation moyenne du parc de bus et car diesel existant (kWh/km)
        I : Consommation moyenne nouveau bus/car diesel (kWh/km)
        """
        F = fe_atmo.loc[
            (fe_atmo.categorie == "conso")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.DIESEL)
            & (fe_atmo.performance == "Actuel")
        ].iloc[0]["valeur"]
        G = fe_atmo.loc[
            (fe_atmo.categorie == "conso")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.ELECTRICITY)
        ].iloc[0]["valeur"]
        H = fe_atmo.loc[
            (fe_atmo.categorie == "conso")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.GAS)
        ].iloc[0]["valeur"]
        I = fe_atmo.loc[
            (fe_atmo.categorie == "conso")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.DIESEL)
            & (fe_atmo.performance == "Récent")
        ].iloc[0]["valeur"]

        """
        Depuis les variables d'émissions ATMO :
        M : Facteur d’émissions du diesel actuel (geqCO2/km)
        N : Facteur d'émissions du diesel nouvelle génération (geqCO2/km) 
        O : Facteur d’émissions de l’électricité (geqCO2/km)
        P : Facteur d’émissions du GNV (geqCO2/km)
        Q : Facteur d’émissions du BioGNV (geqCO2/km)
        """
        M = fe_atmo.loc[
            (fe_atmo.categorie == "GES")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.DIESEL)
            & (fe_atmo.performance == "Actuel")
        ].iloc[0]["valeur"]
        N = fe_atmo.loc[
            (fe_atmo.categorie == "GES")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.DIESEL)
            & (fe_atmo.performance == "Récent")
        ].iloc[0]["valeur"]
        O = fe_atmo.loc[
            (fe_atmo.categorie == "GES")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.ELECTRICITY)
        ].iloc[0]["valeur"]
        P = fe_atmo.loc[
            (fe_atmo.categorie == "GES")
            & (fe_atmo.type_veh_cha == "Autocars + Bus")
            & (fe_atmo.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe_atmo.PARAMCOMMODITY == ParamCommodity.GAS)
        ].iloc[0]["valeur"]
        # bioGNV 4.6 fois moins émetteur que GNV
        Q = P / 4.6

        """
        TODO
        R : Coût unitaire d'un bus ou car électrique (€)
        S : Coût unitaire d'un bus ou car au gaz (€)
        T : Coût unitaire d’un bus ou car diesel (€)
        """

        """
        TODO
        V = Nombre de nouveau bus électrique
        W = Nombre de nouveau bus GNV / BioGNV
        X = Nombre de nouveau car électrique
        Y = Nombre de nouveau car GNV / BioGNV
        """

        # – (("A" + "B") × "Nombre de bus existant" × "distance_parcourue" × 1 / 100)

        ### Calculs énergie et émissions pour les bus

        # TODO: not used
        (A + B + C) * nb_bus_diesel_existant
        ## Consommation énergétique
        # Changement de la consommation d’énergie des bus (vecteur électrique)
        conso_ener_bus_electrique = A * nb_bus_diesel_existant * E * G
        part_ener_bus_bio = B * nb_bus_diesel_existant * E * H
        conso_ener_nouveau_bus_diesel = C * nb_bus_diesel_existant * E * I
        conso_ener_bus_diesel_existant = (A + B + C) * nb_bus_diesel_existant * E * F

        # Changement de la consommation d’énergie des bus (vecteur produits pétroliers)
        conso_ener_bus_produit_petrolier = (
            conso_ener_nouveau_bus_diesel - conso_ener_bus_diesel_existant
        )
        # Changement de la consommation d’énergie des bus (vecteur gaz)
        conso_ener_bus_gaz = part_ener_bus_bio * (1 - J / 100)
        # Changement de la consommation d’énergie des bus (vecteur organo-carburants)
        conso_ener_bus_organic = part_ener_bus_bio * (J / 100)

        ##  Emissions de gaz à effet de serre
        # calcul des nombres km parcourus
        nbre_km_bus_electrique = A * nb_bus_diesel_existant * E
        nbre_km_bus_bio = B * nb_bus_diesel_existant * E
        nbre_km_nouveau_bus_diesel = C * nb_bus_diesel_existant * E
        nbre_km_bus_diesel_existant = (A + B + C) * nb_bus_diesel_existant * E

        nbre_km_bus_gaz = nbre_km_bus_bio * (1 - J / 100)
        nbre_km_bus_organic = nbre_km_bus_gaz * (J / 100)

        # Changement des émissions des GES des bus (vecteur produits pétroliers)
        emissions_ges_bus_produit_petrolier = -(
            (nbre_km_nouveau_bus_diesel * N) - (nbre_km_bus_diesel_existant * M)
        )
        # Changement des émissions des GES des bus (vecteur électrique)
        emissions_ges_bus_electrique = -(nbre_km_bus_electrique * O)
        # Changement des émissions des GES des bus (vecteur gaz)
        emissions_ges_bus_gaz = -(nbre_km_bus_gaz * P)
        # Changement des émissions des GES des bus (vecteur organo-carburants)
        emissions_ges_bus_organic = -(nbre_km_bus_organic * Q)

        ### Calculs énergie et émissions pour les cars :
        ## Consommation énergétique
        # Pour cette action, 100% du mix energétique sont des produits pétroliers (on remplace des bus à gazole ou essence par des énergies non poluantes, donc l'impact est uniquement sur les produits pétroliers)

        sum_eco_produit_petrolier = -float(conso_ener_bus_produit_petrolier) / 1000000
        sum_eco_elec = -float(conso_ener_bus_electrique) / 1000000
        sum_eco_organic = -float(conso_ener_bus_organic) / 1000000
        sum_eco_gaz = -float(conso_ener_bus_gaz) / 1000000

        eco_ener_tot = pd.DataFrame()
        eco_ener_tot["COMMODITY"] = [
            Commodity.OIL,
            Commodity.ELECTRICITY,
            Commodity.BIOFUELS,
            Commodity.GAS,
        ]
        eco_ener_tot["valeur"] = [
            sum_eco_produit_petrolier,
            sum_eco_elec,
            sum_eco_organic,
            sum_eco_gaz,
        ]

        prix.loc[prix.COMMODITY == Commodity.OIL, "valeur"] = float(
            prix[prix.PARAMCOMMODITY == ParamCommodity.DIESEL]["valeur"]
        )
        prix_diesel = (
            prix.groupby(["COMMODITY", "SECTOR", "unite", "annee"])["valeur"]
            .agg("mean")
            .reset_index()
        )

        facture = prix_diesel.merge(
            eco_ener_tot, left_on="COMMODITY", right_on="COMMODITY"
        )
        eco_ener_tot.set_index("COMMODITY", inplace=True)

        ## Emissions de gaz à effet de serre
        sum_emission_produit_petrolier = (
            float(emissions_ges_bus_produit_petrolier) / 1000000000
        )
        sum_emission_electrique = float(emissions_ges_bus_electrique) / 1000000000
        sum_emission_gaz = float(emissions_ges_bus_gaz) / 1000000000
        sum_emission_organic = float(emissions_ges_bus_organic) / 1000000000

        emissions_ges = pd.DataFrame()
        emissions_ges["COMMODITY"] = [
            Commodity.OIL,
            Commodity.ELECTRICITY,
            Commodity.BIOFUELS,
            Commodity.GAS,
        ]
        emissions_ges["valeur"] = [
            sum_emission_produit_petrolier,
            sum_emission_electrique,
            sum_emission_organic,
            sum_emission_gaz,
        ]
        emissions_ges.set_index("COMMODITY", inplace=True)
        facture["facture_totale"] = facture["valeur_x"] * facture["valeur_y"]
        red_fact_ener = (facture["valeur_x"] * facture["valeur_y"]).sum() * 10

        # Emissions de polluants atmosphériques
        if enable_air_pollutants_impacts:
            ## Les facteurs d'émissions de polluants atmosphériques par polluant
            def facteur_emission_polluant(pol):
                facteur_emission = {}
                for x in ["GNV", "Diesel_Actuel", "Diesel_Récent", "Elec"]:
                    facteur_emission[x] = fe_atmo.loc[
                        (fe_atmo.type_veh_cha == "Autocars + Bus")
                        & (fe_atmo.categorie == "{}".format(pol))
                        & (fe_atmo.type_energie == x)
                    ].iloc[0]["valeur"]

                return facteur_emission

            ## Caulculer les émissions de polluants atmosphériques par polluant
            def calculer_emission_polluants(facteur_emission):
                # Changement des émissions de polluants des bus (vecteur produits pétroliers)
                emissions_polluants_bus_produit_petrolier = -(
                    (nbre_km_nouveau_bus_diesel * facteur_emission["Diesel_Récent"])
                    - (nbre_km_bus_diesel_existant * facteur_emission["Diesel_Actuel"])
                )
                # Changement des émissions de polluants des bus (vecteur électrique)
                emissions_polluants_bus_electrique = -(
                    nbre_km_bus_electrique * facteur_emission["Elec"]
                )
                # Changement des émissions de polluants des bus (vecteur gaz)
                emissions_polluants_bus_gaz = -(
                    nbre_km_bus_gaz * facteur_emission["GNV"]
                )
                # Changement des émissions de polluants des bus (vecteur organo-carburants)
                emissions_polluants_bus_organic = -(
                    nbre_km_bus_organic * facteur_emission["GNV"]
                )

                sum_polluant_produit_petrolier = (
                    float(emissions_polluants_bus_produit_petrolier) / 1000000
                )
                sum_polluant_elec = float(emissions_polluants_bus_electrique) / 1000000
                sum_polluant_organic = float(emissions_polluants_bus_organic) / 1000000
                sum_polluant_gaz = float(emissions_polluants_bus_gaz) / 1000000

                eco_polluant_tot = pd.DataFrame()
                eco_polluant_tot["COMMODITY"] = [
                    Commodity.OIL,
                    Commodity.ELECTRICITY,
                    Commodity.BIOFUELS,
                    Commodity.GAS,
                ]
                eco_polluant_tot["valeur"] = [
                    sum_polluant_produit_petrolier,
                    sum_polluant_elec,
                    sum_polluant_organic,
                    sum_polluant_gaz,
                ]
                eco_polluant_tot.set_index("COMMODITY", inplace=True)

                return eco_polluant_tot["valeur"]

            ## Les facteurs d'émissions de polluants atmosphériques
            fe_nox = facteur_emission_polluant("NOx")
            fe_covnm = facteur_emission_polluant("COVNM")
            fe_pm10 = facteur_emission_polluant("PM10")
            fe_pm25 = facteur_emission_polluant("PM2.5")

            ## Les émissions évitées de polluants atmosphériques
            gain_nox = calculer_emission_polluants(fe_nox)
            gain_covnm = calculer_emission_polluants(fe_covnm)
            gain_pm10 = calculer_emission_polluants(fe_pm10)
            gain_pm25 = calculer_emission_polluants(fe_pm25)

            # Pas de facteurs d'émissions pour NH3 et SO2

            return {
                Impacts.EnergyConsumption(
                    eco_ener_tot["valeur"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.AvoidedEmissions(
                    emissions_ges["valeur"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsCOVNM(gain_covnm, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsNH3(None, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsSOX(None, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsPM10(gain_pm10, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsPM25(gain_pm25, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsNOX(gain_nox, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.EnergyBill(red_fact_ener, year),
                Impacts.TaxRevenue(None, year),
            }

        return {
            Impacts.EnergyConsumption(
                eco_ener_tot["valeur"], year, sector=Sector.ROAD_TRANSPORT
            ),
            Impacts.AvoidedEmissions(
                emissions_ges["valeur"], year, sector=Sector.ROAD_TRANSPORT
            ),
            Impacts.EnergyBill(red_fact_ener, year),
            Impacts.TaxRevenue(None, year),
        }
