# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
import pandas as pd

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamDataSet,
    Requisite,
    Sector,
)
from terriapi.controller.strategy_actions.tools import calculate_conso_totale


class EmissionsReductionNonEnergyIndustry(AbstractAction):
    INPUT_REDUCTION = "21_emissions_non_eneg_indus"

    PARAM_CHEMICAL = "Industrie chimique"
    PARAM_CCS_CHEMICAL = "Industrie chimique - CSC"
    PARAM_CEMENT = "Production de ciment"
    PARAM_CCS_CEMENT = "Production de ciment - CSC"
    PARAM_CCS_ALUMIN = "Production de l'aluminium - CSC"
    PARAM_CCS_AMMONIA = "Production d'ammoniac - CSC"
    PARAM_CCS_PROCESS = (
        "Procédés de l'industrie sidérurgique et de l'industrie pétrolière - CSC"
    )
    PARAM_WASTE_COMB = "Incinération des déchets"

    def __init__(self):
        super().__init__(
            "Emissions reduction for non energetic usage in industry", "21"
        )

    def add_requisites(self):
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_REDUCTION,
            self.INPUT_REDUCTION,
            Requisite.Types.INPUT,
        )

        self.add_mandatory_requisite(
            self.PARAM_CHEMICAL,
            self.PARAM_CHEMICAL,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_CCS_CHEMICAL,
            self.PARAM_CCS_CHEMICAL,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_CEMENT,
            self.PARAM_CEMENT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_CCS_CEMENT,
            self.PARAM_CCS_CEMENT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_CCS_ALUMIN,
            self.PARAM_CCS_ALUMIN,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_CCS_AMMONIA,
            self.PARAM_CCS_AMMONIA,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_CCS_PROCESS,
            self.PARAM_CCS_PROCESS,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_WASTE_COMB,
            self.PARAM_WASTE_COMB,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # données requises
        self.add_data_requisite(
            "emission_ges_industrie", DataSet.EMISSIONS, Sector.INDUSTRY_WITHOUT_ENERGY
        )

    async def _execute(self, year, *args, **kwargs):
        emission_ges_indus = self.data["emission_ges_industrie"]

        # Récuperer les entrées utilisateurs (%)
        user_entree = float(self.inputs[self.INPUT_REDUCTION][year]) / 100
        history_emissions = self.inputs[self.INPUT_REDUCTION]

        # si toutes les entrées utilisateurs sont nulle, pas nécessaire de calculer
        if user_entree == 0:
            return {
                Impacts.AvoidedEmissions(0.0, year),  # emiss_evitees,
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(0.0, year),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_industrie_chimique",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_industrie_chimique_csc",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_prod_ciment",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_prod_ciment_csc",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_prod_aluminium_csc",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_prod_ammoniac_csc",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_industrie_siderurgique_csc",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionsPerIndustry(
                    "taux_incineration_dechets",
                    0.0,
                    year,
                ),
            }

        # calcul des émisions GES non-énergétique (id : 4) totale pour chaque année précedant l'année passée en paramétres
        emission_ges_totale_annee_precedente = calculate_conso_totale(
            int(year) - 1,
            history_emissions,
            emission_ges_indus[emission_ges_indus.COMMODITY == Commodity.NON_ENERGETIC],
        )

        # calcul les émission GES non-énergétique (id : 4) totale pour chaque année passée en paramétres
        emission_ges_totale = calculate_conso_totale(
            int(year),
            history_emissions,
            emission_ges_indus[emission_ges_indus.COMMODITY == Commodity.NON_ENERGETIC],
        )

        # calculer les émisions GES non-énergétique totale évitée (id : 4)
        # équivalent à emission_ges_totale_annee_reference * (int(user_params['21_emissions_non_eneg_indus'][year])/100)
        emission_total_evitee = (
            emission_ges_totale_annee_precedente - emission_ges_totale
        )
        emission_ges = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        emission_ges.COMMODITY = emission_ges_indus.COMMODITY
        emission_ges["valeur"] = 0
        # 4 : non-énergétique
        emission_ges.loc[
            emission_ges.COMMODITY == Commodity.NON_ENERGETIC, "valeur"
        ] = emission_total_evitee

        # formater les données d'émission ges
        emission_ges = emission_ges.set_index("COMMODITY")["valeur"].to_dict()
        emission_ges = pd.Series(emission_ges)

        # % des sous-industries
        params = self.input_params
        taux_industrie_chimique = (
            float(params[self.PARAM_CHEMICAL]) / 100
        ) * emission_total_evitee
        taux_industrie_chimique_csc = (
            float(params[self.PARAM_CCS_CHEMICAL]) / 100
        ) * emission_total_evitee
        taux_prod_ciment = (
            float(params[self.PARAM_CEMENT]) / 100
        ) * emission_total_evitee
        taux_prod_ciment_csc = (
            float(params[self.PARAM_CCS_CEMENT]) / 100
        ) * emission_total_evitee
        taux_prod_aluminium_csc = (
            float(params[self.PARAM_CCS_ALUMIN]) / 100
        ) * emission_total_evitee
        taux_prod_ammoniac_csc = (
            float(params[self.PARAM_CCS_AMMONIA]) / 100
        ) * emission_total_evitee
        taux_industrie_siderurgique_csc = (
            float(params[self.PARAM_CCS_PROCESS]) / 100
        ) * emission_total_evitee
        taux_incineration_dechets = (
            float(params[self.PARAM_WASTE_COMB]) / 100
        ) * emission_total_evitee

        return {
            Impacts.AvoidedEmissions(
                emission_ges, year, sector=Sector.INDUSTRY_WITHOUT_ENERGY
            ),
            Impacts.EnergyBill(0.0, year),
            Impacts.TaxRevenue(None, year),
            Impacts.CO2EmissionsPerIndustry(
                "taux_industrie_chimique",
                taux_industrie_chimique,
                year,
            ),
            Impacts.CO2EmissionsPerIndustry(
                "taux_industrie_chimique_csc",
                taux_industrie_chimique_csc,
                year,
            ),
            Impacts.CO2EmissionsPerIndustry(
                "taux_prod_ciment",
                taux_prod_ciment,
                year,
            ),
            Impacts.CO2EmissionsPerIndustry(
                "taux_prod_ciment_csc",
                taux_prod_ciment_csc,
                year,
            ),
            Impacts.CO2EmissionsPerIndustry(
                "taux_prod_aluminium_csc",
                taux_prod_aluminium_csc,
                year,
            ),
            Impacts.CO2EmissionsPerIndustry(
                "taux_prod_ammoniac_csc",
                taux_prod_ammoniac_csc,
                year,
            ),
            Impacts.CO2EmissionsPerIndustry(
                "taux_industrie_siderurgique_csc",
                taux_industrie_siderurgique_csc,
                year,
            ),
            Impacts.CO2EmissionsPerIndustry(
                "taux_incineration_dechets",
                taux_incineration_dechets,
                year,
            ),
        }
