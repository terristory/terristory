# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd

from terriapi.controller import suivi_trajectoire as controller_suivi_trajectoire
from terriapi.controller.strategy_actions import (
    POLLUTANTS,
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    Requisite,
    Sector,
    Usage,
)
from terriapi.controller.strategy_actions.errors import StrategyException
from terriapi.controller.strategy_actions.tools import (
    _calcul_baisse_emission_et_conso_par_secteur,
)


class CommutingTimeReduction(AbstractAction):
    INPUT_PERC_REDUCTION = "18_reduction_dist_traj_domicile_travail"

    PARAM_PERC_PEOPLE_ALREADY_TELEWORKING = (
        "Pourcentage des actifs pratiquant déjà le télétravail"
    )
    PARAM_TELEWORK_DAYS_PER_WEEK = (
        "Nombre de jours de télétravail par semaine de 5 jours"
    )
    PARAM_DAYS_WORKED_BY_YEAR = "Nombre de jours travaillés par an"
    PARAM_NB_TRIPS_PER_DAY = "Nombre de trajets par jour"
    PARAM_THRESHOLD_MIN_DIST = (
        "Seuil minimum de distance considéré pour les trajets domicile-travail"
    )

    PARAM_CAR_FLEET = "action7_parc_voiture_com_men"

    DATA_NB_WORKERS_USING_CAR = "nombre_actifs_en_voiture"
    DATA_AVG_DIST_PER_WORKER_CAR = "distance_moyenne_par_actif_en_voiture"

    def __init__(self):
        super().__init__("Reduction of commuting time", "18")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions ATMO",
            self.PARAM_FEGES_ATMO,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.ATMO_GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )

        self.add_mandatory_requisite(
            "Parc de voitures",
            self.PARAM_CAR_FLEET,
            Requisite.Types.PARAM,
        )

        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_THRESHOLD_MIN_DIST,
            self.PARAM_THRESHOLD_MIN_DIST,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_PERC_PEOPLE_ALREADY_TELEWORKING,
            self.PARAM_PERC_PEOPLE_ALREADY_TELEWORKING,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TELEWORK_DAYS_PER_WEEK,
            self.PARAM_TELEWORK_DAYS_PER_WEEK,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_DAYS_WORKED_BY_YEAR,
            self.PARAM_DAYS_WORKED_BY_YEAR,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_NB_TRIPS_PER_DAY,
            self.PARAM_NB_TRIPS_PER_DAY,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_PERC_REDUCTION,
            self.INPUT_PERC_REDUCTION,
            Requisite.Types.INPUT,
        )

        # data
        self.add_data_requisite(
            "conso_transport",
            DataSet.CONSUMPTION,
            Sector.ROAD_TRANSPORT,
        )
        self.add_data_requisite(
            "emissions_transport",
            DataSet.EMISSIONS,
            Sector.ROAD_TRANSPORT,
        )

        for pollutant in POLLUTANTS:
            self.add_data_requisite(
                "emission_" + pollutant,
                DataSet.table_from_pollutant(pollutant),
                Sector.ROAD_TRANSPORT,
                default=False,
            )

    async def _pre_execute(self, *args, **kwargs):
        region = kwargs.get("region", None)
        zone = kwargs.get("zone", None)
        zone_id = kwargs.get("zone_id", None)
        if region is None or zone is None or zone_id is None:
            raise StrategyException(
                "No region, zone or zone_id specified for pre-execute function."
            )

        buffer_km = int(self.input_params[self.PARAM_THRESHOLD_MIN_DIST])

        # TOFIX: specify year here!
        (
            _,
            self.data[self.DATA_NB_WORKERS_USING_CAR],
        ) = await controller_suivi_trajectoire.nombre_actifs_en_voiture(
            region, zone, zone_id, buffer_km
        )

        (
            _,
            self.data[self.DATA_AVG_DIST_PER_WORKER_CAR],
        ) = await controller_suivi_trajectoire.distance_moyenne_par_actif_en_voiture(
            region, zone, zone_id, buffer_km
        )

    async def _execute(self, year, *args, **kwargs):
        """Retourne pour une année donnée, les impacts énergétiques, les émission ges et la baisse de la facteur énergétique (action 18).

        Parameters
        ----------
        region : str
            Nom de la région (nom officiel complet en minuscule, sans accent, e.g. auvergne-rhone-alpes)
        year : str
            Année pour laquelle on calcule les impacts
        user_params : dict
            Paramètres que l'utilisateur.trice doit saisir dans le formulaire de l'action
        distance_moyenne_par_actif_en_voiture : dict
            Dictionnaire contenant la distance moyenne parcourue par actif en voiture pour ses trajets domicile-travail
        nombre_actifs_en_voiture : dict
            Dictionnaire contenant le nombre d'actifs faisant un trajet domicile-travail en voiture.
        conso_usage_transport : DF
            Quantité de chaque énergie consommée dans le secteur des transports de personnes
        emission_ges_transport : DF
            Émission GES dans le transport de personnes

        Returns
        -------
        dict
        """
        enable_air_pollutants_impacts = self.config.get(
            "enable_air_pollutants_impacts", False
        )

        nombre_actifs_en_voiture = self.data[self.DATA_NB_WORKERS_USING_CAR]
        distance_moyenne_par_actif_en_voiture = self.data[
            self.DATA_AVG_DIST_PER_WORKER_CAR
        ]

        reduc_nb_km = int(self.inputs[self.INPUT_PERC_REDUCTION][year])

        if reduc_nb_km == 0:
            return {
                Impacts.EnergyConsumption(0.0, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.AvoidedEmissions(0.0, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        conso_usage_transport = self.data["conso_transport"]
        conso_usage_transport = (
            conso_usage_transport.groupby(["COMMODITY", "SECTOR"])[["valeur"]]
            .sum()
            .reset_index()
        )
        emission_ges_transport = self.data["emissions_transport"]
        emission_ges_transport = (
            emission_ges_transport.groupby(["COMMODITY", "SECTOR"])[["valeur"]]
            .sum()
            .reset_index()
        )

        # facteurs d'émissions polluants / ges / conso ATMO
        fe = self.params[self.PARAM_FEGES_ATMO]

        nb_jour_travail_par_an = int(self.input_params[self.PARAM_DAYS_WORKED_BY_YEAR])
        nb_jour_traj_par_jour = int(self.input_params[self.PARAM_NB_TRIPS_PER_DAY])
        pourcentage_actif_teletravail = int(
            self.input_params[self.PARAM_PERC_PEOPLE_ALREADY_TELEWORKING]
        )
        nb_jour_teletravail_par_sem = int(
            self.input_params[self.PARAM_TELEWORK_DAYS_PER_WEEK]
        )

        # consommation moyenne des véhicules/km ATMO
        conso_moy_vehicule = float(
            fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "conso")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]["valeur"]
        )

        # émissions moyenne g/km ATMO
        ges_moy_vehicule = float(
            fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "GES")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]["valeur"]
        )

        # nombre km évités
        nbre_km_evites = (
            (reduc_nb_km / 100)
            * nombre_actifs_en_voiture
            * distance_moyenne_par_actif_en_voiture
            * nb_jour_travail_par_an
            * nb_jour_traj_par_jour
            * (
                1
                - (
                    (pourcentage_actif_teletravail / 100)
                    * nb_jour_teletravail_par_sem
                    / 5
                )
            )
        )

        # baisse de la consommation énergétique
        baisse_conso_nrj = nbre_km_evites * conso_moy_vehicule

        # consommation énergétique pour le secteur du transport
        conso_nrj_secteur = float(
            conso_usage_transport["valeur"].sum(axis=0, skipna=True)
        )
        # retranscription en poucentage de la baisse de conso énergétique sur le secteur
        baisse_conso_secteur_coef_multiplicateur = (
            baisse_conso_nrj / 1000000
        ) / conso_nrj_secteur  # ok

        # dictionnaire baisse conso énergétique par vecteur
        dict_vecteurs_baisse_conso = _calcul_baisse_emission_et_conso_par_secteur(
            conso_usage_transport,
            baisse_conso_secteur_coef_multiplicateur,
            Commodity,
            Commodity.ALL,
        )

        # baisse des émissions ges pour le seteur transport
        baisse_emission_ges = nbre_km_evites * ges_moy_vehicule

        # émissions ges pour le secteur du transport
        emission_ges_secteur = float(
            emission_ges_transport["valeur"].sum(axis=0, skipna=True)
        )
        # retranscription en poucentage de la baisse de ges sur le secteur
        baisse_ges_secteur_coef_multiplicateur = (
            baisse_emission_ges / 1000000000
        ) / emission_ges_secteur  # ok

        # dictionnaire baisse émission ges par vecteur
        dict_vecteurs_baisse_emission = _calcul_baisse_emission_et_conso_par_secteur(
            emission_ges_transport,
            baisse_ges_secteur_coef_multiplicateur,
            Commodity,
            Commodity.ALL,
        )

        # facture énergétique
        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        # récupération de la répation des automobiles
        parc_voitures = self.params[self.PARAM_CAR_FLEET]

        # taux de voiture roulant à l'essence
        tx_voit_menag_commerc_essen = parc_voitures.tx_voit_menag_commerc_essen.iloc[0]
        # taux de voiture roulant au diesel
        tx_voit_menag_commerc_diesel = parc_voitures.tx_voit_menag_commerc_diesel.iloc[
            0
        ]
        # prix des vecteurs nécessaire pour le calcul de la facture énergétique
        prix_essence = prix.valeur[prix.PARAMCOMMODITY == ParamCommodity.GASOLINE].iloc[
            0
        ]
        prix_diesel = prix.valeur[prix.PARAMCOMMODITY == ParamCommodity.DIESEL].iloc[0]
        prix_organo_carbu = prix.valeur[
            prix.PARAMCOMMODITY == ParamCommodity.BIOFUEL
        ].iloc[0]

        # we handle the case where no biofuel consumption has been filled in
        conso_biofuels = conso_usage_transport.loc[
            conso_usage_transport.COMMODITY == Commodity.BIOFUELS, "valeur"
        ]
        if conso_biofuels.empty:
            conso_biofuels = 0
        # Baisse de la facture énergétique du territoire
        somme_vecteurs = prix_organo_carbu * float(
            conso_biofuels * baisse_conso_secteur_coef_multiplicateur
        )
        somme_vecteurs += float(
            conso_usage_transport.loc[
                conso_usage_transport.COMMODITY == Commodity.OIL, "valeur"
            ]
            * baisse_conso_secteur_coef_multiplicateur
        ) * (
            tx_voit_menag_commerc_essen * prix_essence
            + tx_voit_menag_commerc_diesel * prix_diesel
        )

        # passage des dictionnaires en tableau pandas pour le return
        gains_energetiques = pd.Series(dict_vecteurs_baisse_conso)
        emissions_evitees = pd.Series(dict_vecteurs_baisse_emission)

        if enable_air_pollutants_impacts:
            # Facteurs d'émissions des polluants atmosphériques
            fep = {}
            fep["nox"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "NOx")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]
            fep["covnm"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "COVNM")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]
            fep["pm10"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "PM10")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]
            fep["pm25"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "PM2.5")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]

            # facteur d'émission de polluatns atmosphérique

            # conversion g => tonne
            nbre_km_evites_coef_multiplicateur_polluants = nbre_km_evites / 1000000

            gain_pollutants = {}
            for pollutant in ["nox", "covnm", "pm10", "pm25"]:
                # Calcul emissions évitées Nox
                fep[pollutant]["valeur"] = fep[pollutant]["valeur"].multiply(
                    nbre_km_evites_coef_multiplicateur_polluants
                )
                facteur_emission = fep[pollutant].merge(
                    self.data["emission_" + pollutant],
                    left_on="COMMODITY#2",
                    right_on="COMMODITY",
                )

                # Create DataFrame
                gain_pollutants[pollutant] = pd.DataFrame(
                    columns=["COMMODITY", "valeur"], dtype=object
                )
                gain_pollutants[pollutant]["valeur"] = facteur_emission["valeur_x"]
                gain_pollutants[pollutant]["COMMODITY"] = facteur_emission["COMMODITY"]

            return {
                Impacts.EnergyConsumption(
                    gains_energetiques, year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.AvoidedEmissions(
                    emissions_evitees, year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsCOVNM(
                    gain_pollutants["covnm"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsNH3(None, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsPM10(
                    gain_pollutants["pm10"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsPM25(
                    gain_pollutants["pm25"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsNOX(
                    gain_pollutants["nox"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsSOX(None, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.EnergyBill(somme_vecteurs, year),
                Impacts.TaxRevenue(None, year),
            }

        return {
            Impacts.EnergyConsumption(
                gains_energetiques, year, sector=Sector.ROAD_TRANSPORT
            ),
            Impacts.AvoidedEmissions(
                emissions_evitees, year, sector=Sector.ROAD_TRANSPORT
            ),
            Impacts.EnergyBill(somme_vecteurs, year),
            Impacts.TaxRevenue(None, year),
        }
