# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)


class WoodEnergy(AbstractAction):
    PARAM_BOILER_PERF = "Rendement de la chaufferie"

    INPUT_TOTAL_CAP = "13_conso_totale"

    def __init__(self):
        super().__init__("Wood energy", "13")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_BOILER_PERF,
            self.PARAM_BOILER_PERF,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_TOTAL_CAP,
            self.INPUT_TOTAL_CAP,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

    async def _execute(self, year, *args, **kwargs):
        """Retourne pour une année donnée, les impacts énergétiques de l'action Chaufferies bois (action 13)

        Parameters
        ----------
        region : str
            Nom de la région (nom officiel complet en minuscule, sans accent, e.g. auvergne-rhone-alpes)
        year : str
            Année pour laquelle on calcule les impacts
        user_params dict :
            Paramètres que l'utilisateur.trice doit saisir dans le formulaire de l'action
        conso_chauffage DataFrame Pandas :
            Quantité de chaque énergie énergie consommée dans le secteur résidentiel

        Returns
        -------
        dict
        """
        energie_livree = (
            float(self.inputs[self.INPUT_TOTAL_CAP][year]) / 1000
        )  # Paramètre saisi par l'utilisateur.trice

        if energie_livree == 0:
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.BIOMASS_THER_CHAUFFERIE,
                    energy_vector=EnergyVector.HEAT,
                ),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(0.0, year),
            }

        conso_chauffage = self.data["conso_residentiel"]

        conso_chauffage = conso_chauffage.loc[
            (conso_chauffage.USAGE == Usage.HEATING), ["valeur", "COMMODITY"]
        ]  # Seul l'usage chauffage nous intéresse

        # scénario d'évolution du prix de l'énergie, par énergie et secteur entre 2016 et 2050
        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        # On sélectionne ici les lignes qui nous intéressent. La présence de nouvelles chaufferies au bois
        # n'a d'incidence que dans le secteur résidentiel ou alors indifférencié mais pour les énergies
        # Biomasse et Chaleur.

        prix = prix.loc[
            (prix.SECTOR == Sector.RESIDENTIAL)
            | (prix.PARAMCOMMODITY == ParamCommodity.BIOMASS)
            | (prix.COMMODITY == Commodity.URBAN_HEAT_COOL)
        ]

        prix_energie_bois = prix.loc[prix.PARAMCOMMODITY == ParamCommodity.BIOMASS][
            "valeur"
        ]  # On a besoin de disposer du prix spécifique à la biomasse

        prix = prix.groupby(["PARAMCOMMODITY", "COMMODITY", "annee"]).sum()
        conso_chauffage["valeur"] = (
            conso_chauffage["valeur"] / conso_chauffage["valeur"].sum()
        )  # Calcul de la proportion de chaque type d'énergie consommée dans le secteur résidentiel
        prix_mix = prix.merge(
            conso_chauffage, left_on="COMMODITY", right_on="COMMODITY"
        )  # On joint pour calculer le produit de la proportion de chaque énergie par leur prix
        prix_global_chauffage = (
            prix_mix["valeur_x"] * prix_mix["valeur_y"]
        ).sum()  # Calcul effectif. Vecteur colonne dont le nombre de lignes est égal au nombre de types d'énergie

        # paramètres avancés modifiables
        # Le rendement de chaufferie est nécessaire et doit être exprimé en proportion (et non en pourcentage)
        rendement_chaufferie = self.input_params[self.PARAM_BOILER_PERF] / 100

        baisse_fact_ener = energie_livree * prix_global_chauffage - (
            float(prix_energie_bois) * (energie_livree / float(rendement_chaufferie))
        )

        return {
            Impacts.EnergyProduction(
                energie_livree,
                year,
                prod_type=RenewableProd.BIOMASS_THER_CHAUFFERIE,
                energy_vector=EnergyVector.HEAT,
            ),
            Impacts.AvoidedEmissions(
                0.0,  # emiss_evitees,
                year,
                commodity=Commodity.ELECTRICITY,
                sector=Sector.ENERGY_INDUSTRY,
            ),
            Impacts.EnergyBill(baisse_fact_ener, year),
            Impacts.TaxRevenue(0.0, year),
        }
