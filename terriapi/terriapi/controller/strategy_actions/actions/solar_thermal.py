# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.analyse import get_global_irradiation
from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)


class SolarThermal(AbstractAction):
    INPUT_SUFFIX_NB_COLLEC_BUILDINGS = "_nb_batiment_collectif_renouv"

    @property
    def INPUT_NB_COLLEC_BUILDINGS(self):
        return self.action_number + self.INPUT_SUFFIX_NB_COLLEC_BUILDINGS

    INPUT_SUFFIX_SURFACE_SENSORS = "_surf_capteurs_bati"

    @property
    def INPUT_SURFACE_SENSORS(self):
        return self.action_number + self.INPUT_SUFFIX_SURFACE_SENSORS

    @property
    def PARAM_PERFORMANCE(self):
        return "rendement_" + self.plant_type

    @property
    def SUB_PARAM_PERFORMANCE(self):
        if self.plant_type == "ecs":
            return "Rendement ECS"
        elif self.plant_type == "combine":
            return "Rendement combiné"
        else:
            raise NotImplementedError(
                "Solar thermal action can only be ecs or combine type ("
                + self.plant_type
                + " given)"
            )

    def __init__(self, stype):
        self.plant_type = stype
        if stype == "ecs":
            code = "4"
            suffix = " (hot water)"
        elif stype == "combine":
            code = "4b"
            suffix = " (combined)"
        else:
            raise NotImplementedError(
                "Solar thermal action can only be ecs or combine type ("
                + stype
                + " given)"
            )
        super().__init__("Solar thermal" + suffix, code)

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # global irradiation
        self.add_mandatory_requisite(
            "Global irradiation",
            "Global irradiation",
            Requisite.Types.PARAM,
            computation_function=get_global_irradiation,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_PERFORMANCE,
            self.PARAM_PERFORMANCE,
            Requisite.Types.INPUT_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_NB_COLLEC_BUILDINGS,
            self.INPUT_NB_COLLEC_BUILDINGS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_SURFACE_SENSORS,
            self.INPUT_SURFACE_SENSORS,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

    async def _execute(self, year, *args, **kwargs):
        """
        Calcul d'impact GES, NRJ et facture NRJ
        pour l'action d'installation solaire pour le logement collectif combiné
        eau chaude / chauffage
        """
        # paramètres avancés des actions modifiables par l'utilisateur
        rendement = self.input_params[self.PARAM_PERFORMANCE][
            self.SUB_PARAM_PERFORMANCE
        ]

        conso_usage_energie = self.data["conso_residentiel"]

        nb_buildings = float(self.inputs[self.INPUT_NB_COLLEC_BUILDINGS][year])
        surface_per_building = float(self.inputs[self.INPUT_SURFACE_SENSORS][year])

        surface_installee = nb_buildings * surface_per_building

        if surface_installee == 0:
            return {
                Impacts.EnergyProduction(
                    0,
                    year,
                    prod_type=RenewableProd.SOLAR_THER,
                    energy_vector=EnergyVector.HEAT,
                ),
                Impacts.EnergyBill(0, year),
                Impacts.TaxRevenue(None, year),
            }

        # paramètres globaux
        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        feges = self.params[self.PARAM_FEGES]

        irrad_sol_an = self.params["Global irradiation"]["fixe"]

        rendement /= 100
        gain_energetique = irrad_sol_an * surface_installee * rendement / 1000

        # mix energetique chauffage
        conso_elec = conso_usage_energie[
            conso_usage_energie.COMMODITY == Commodity.ELECTRICITY
        ][["usage", "valeur"]]
        conso_elec["valeur"] = conso_elec["valeur"] / conso_elec["valeur"].sum()

        mix_energetique_ecs = conso_usage_energie[
            conso_usage_energie.USAGE == Usage.HOT_WATER
        ].assign(
            valeur=lambda x: x.valeur
            / conso_usage_energie[conso_usage_energie.USAGE == Usage.HOT_WATER][
                "valeur"
            ].sum()
        )

        mix_energetique_chauffage = conso_usage_energie[
            conso_usage_energie.USAGE == Usage.HEATING
        ].assign(
            valeur=lambda x: x.valeur
            / conso_usage_energie[conso_usage_energie.USAGE == Usage.HEATING][
                "valeur"
            ].sum()
        )

        # same filter than in action 1 residential refurbishment l.354
        feges_chauffage = feges.loc[
            (
                (
                    feges["COMMODITY#1"].isin(
                        (
                            Commodity.URBAN_HEAT_COOL,
                            Commodity.GAS,
                            Commodity.RENEWABLE_HEAT,
                            Commodity.COAL,
                            Commodity.OIL,
                        )
                    )
                )
                & (feges.USAGE == Usage.OTHERS)
            )
            | (
                (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
                & (feges.USAGE == Usage.HEATING)
                & (feges.SECTOR == Sector.RESIDENTIAL)
            )
        ]
        feges_chauffage = feges_chauffage.set_index("COMMODITY#1")
        feges_ecs = feges.loc[
            (
                (
                    (
                        feges["COMMODITY#1"].isin(
                            (
                                Commodity.URBAN_HEAT_COOL,
                                Commodity.GAS,
                                Commodity.RENEWABLE_HEAT,
                                Commodity.COAL,
                                Commodity.OIL,
                            )
                        )
                    )
                    & (feges.USAGE == Usage.OTHERS)
                )
                | (
                    (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
                    & (feges.USAGE == Usage.HOT_WATER)
                    & (feges.SECTOR == Sector.RESIDENTIAL)
                )
            )
        ]
        feges_ecs = feges_ecs.set_index("COMMODITY#1")

        # calcul terme 1 : gain des émissions de GES pour le chauffage
        fecm = feges_chauffage.merge(
            mix_energetique_chauffage, left_on="COMMODITY#2", right_on="COMMODITY"
        )
        somme_emission_chauffage = (fecm["valeur_x"] * fecm["valeur_y"]).sum()

        # calcul terme 2 : gains des émissions de GES pour l'ECS
        feecsm = feges_ecs.merge(
            mix_energetique_ecs, left_on="COMMODITY#2", right_on="COMMODITY"
        )
        somme_emission_ecs = (feecsm["valeur_x"] * feecsm["valeur_y"]).sum()

        # calcul final des émissions évitées
        if self.plant_type == "ecs":
            emissions_evitees = gain_energetique * somme_emission_ecs
        else:
            emissions_evitees = gain_energetique * (
                somme_emission_chauffage * 0.5 + somme_emission_ecs * 0.5
            )

        prix_mix_chauff = (
            prix[prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.UNDIFFERENCIATED))]
            .set_index("COMMODITY")
            .merge(mix_energetique_chauffage, left_on="COMMODITY", right_on="COMMODITY")
        )
        prix_global_chauffage = (
            prix_mix_chauff["valeur_x"] * prix_mix_chauff["valeur_y"]
        ).sum()

        # calcul du prix global pour l'ECS en €/100kWh en multipliant les prix par énergie et le mix énergetique du chauffage
        prix_mix_ecs = (
            prix[prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.UNDIFFERENCIATED))]
            .set_index("COMMODITY")
            .merge(mix_energetique_ecs, left_on="COMMODITY", right_on="COMMODITY")
        )
        prix_global_ecs = (prix_mix_ecs["valeur_x"] * prix_mix_ecs["valeur_y"]).sum()

        # Calcul final
        if self.plant_type == "ecs":
            baisse_facture_energetique = gain_energetique * prix_global_ecs / 100
        else:
            baisse_facture_energetique = (
                gain_energetique
                * (prix_global_chauffage * 0.5 + prix_global_ecs * 0.5)
                / 100
            )

        # Énergie en GWh
        gain_energetique /= 1000.0

        # Émission en ktCO2
        emissions_evitees /= 1000.0

        return {
            Impacts.EnergyProduction(
                gain_energetique,
                year,
                prod_type=RenewableProd.SOLAR_THER,
                energy_vector=EnergyVector.HEAT,
            ),
            # # TODO: fix this? what sector is counted for avoided emissions?
            # # problem is that these emissions are not used in original counts
            # # Should we include them now?
            # Impacts.AvoidedEmissions(
            #     0.0,  # emissions_evitees,
            #     year,
            #     sector=Sector.RESIDENTIAL,
            #     commodity=Commodity.RENEWABLE_HEAT,
            # ),
            Impacts.EnergyBill(baisse_facture_energetique, year),
            Impacts.TaxRevenue(None, year),
        }
