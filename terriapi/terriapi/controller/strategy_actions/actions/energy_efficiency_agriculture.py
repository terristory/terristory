# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
import pandas as pd

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    Requisite,
    Sector,
)
from terriapi.controller.strategy_actions.tools import calculate_conso_totale


class EnergyEfficiencyAgriculture(AbstractAction):
    INPUT_REDUCTION = "15_conso_agri_totale"

    PARAM_FORCED_MIX = "mix_energie_agriculture_region_2018_2050"

    def __init__(self):
        super().__init__("Energy efficiency in the agriculture", "15")

    def add_requisites(self):
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_optional_requisite(
            False,
            self.PARAM_FORCED_MIX,
            self.PARAM_FORCED_MIX,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.FORCED_MIX,
        )
        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_REDUCTION,
            self.INPUT_REDUCTION,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_agriculture", DataSet.CONSUMPTION, Sector.AGRICULTURE
        )
        self.add_data_requisite(
            "emission_ges_agriculture", DataSet.EMISSIONS, Sector.AGRICULTURE
        )

    async def _execute(self, year, *args, **kwargs):
        """Retourne les impacts énérgitique, les émission ges et la baisse dela facteur énergétique

        Parameters
        ----------
        csv_params : list
            liste des paramétres des actions

        year : int
            l'année qui correspond au pourcentage entré par l'utilisateur
        user_params : int
            le pourcentage de réduction entré par l'utilisateur
        conso_usage_energie : dataframe
            la consommation par type d'énergie

        Returns
        --------
        dict :
            Avec l'ensemble des simulations des actions (gain énergétique, émission ges, baisse de facteur énergétique, recette fiscale)

        """
        conso_usage_energie = self.data["conso_agriculture"]
        emission_ges_agri = (
            self.data["emission_ges_agriculture"]
            .groupby("COMMODITY")[["valeur"]]
            .sum()
            .reset_index()
        )

        consommation_agri = float(self.inputs[self.INPUT_REDUCTION][year]) / 100
        history_conso = self.inputs[self.INPUT_REDUCTION]
        # consommation agri nulle, pas de calcul possible
        if consommation_agri == 0:
            return {
                Impacts.EnergyConsumption(0.0, year),
                Impacts.AvoidedEmissions(0.0, year),  # emiss_evitees,
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(0.0, year),
            }

        # calcul du consommation totale pour chaque année précédant l'année passée en paramètres
        consommation_totale_annee_precedente = calculate_conso_totale(
            int(year) - 1, history_conso, conso_usage_energie
        )
        # calcul du consommation totale pour chaque année passée en paramétres
        consommation_totale = calculate_conso_totale(
            int(year), history_conso, conso_usage_energie
        )

        # calcul du mix énérgitique
        # division de la conso de chaque type d'energie par la consommation totale
        mix_energetique_df = pd.DataFrame(
            {
                "valeur_pourcentage": conso_usage_energie.groupby(["COMMODITY"])[
                    "valeur"
                ].sum()
                / conso_usage_energie["valeur"].sum()
            }
        ).reset_index()

        # Récuperer le mix énergétique fixe régional 2018 - 2050
        if (
            isinstance(self.params[self.PARAM_FORCED_MIX], pd.DataFrame)
            and not self.params[self.PARAM_FORCED_MIX].empty
        ):
            mix_energetique_df = self.params[self.PARAM_FORCED_MIX]
            mix_energetique_df = mix_energetique_df[
                mix_energetique_df.annee == int(year)
            ]

        # calculer la consommation totale à économiser
        # équivalent à consommation_totale_reference * (int(user_params['15_conso_agri_totale'][year])/100)
        consommation_total_economisee = (
            consommation_totale_annee_precedente - consommation_totale
        )

        # calcul du gain énergétique : multiplication de la consommation totale à économiser par le  mix energitique (pourcentage de chaque type d'energie)
        gain_df = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        gain_df.COMMODITY = mix_energetique_df.COMMODITY
        gain_df["valeur"] = mix_energetique_df["valeur_pourcentage"].multiply(
            consommation_total_economisee
        )
        # formatage des données
        gain_dict = gain_df.set_index("COMMODITY")["valeur"].to_dict()
        gain = pd.Series(gain_dict)

        # calcul des émissions GES évitées
        # pas d'impacts sur les émissions GES non-énergétique : 4
        emission_ges_agri.loc[
            emission_ges_agri.COMMODITY == Commodity.NON_ENERGETIC, "valeur"
        ] = 0
        # 1 : multiplier les facteurs d'émission de ges par les entrées utilisateur
        emission_ges = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        emission_ges["valeur"] = emission_ges_agri["valeur"].multiply(consommation_agri)
        emission_ges.COMMODITY = emission_ges_agri.COMMODITY
        # 2: formater les données d'émission ges
        emission_ges = emission_ges.set_index("COMMODITY")["valeur"].to_dict()
        emission_ges = pd.Series(emission_ges)

        # récupération des prix des énergies
        # scénario d'évolution du prix de l'énergie, par énergie et secteur
        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        # 4 : secteur Indifférencié : 4 et industriel : 3
        prix_agri = prix.loc[
            (
                prix.SECTOR.isin(
                    (Sector.INDUSTRY_WITHOUT_ENERGY, Sector.UNDIFFERENCIATED)
                )
            )
            & (
                ~prix.PARAMCOMMODITY.isin(
                    (
                        ParamCommodity.DIESEL,
                        ParamCommodity.GASOLINE,
                        ParamCommodity.LPG,
                        ParamCommodity.FUEL_OIL,
                    )
                )
            )
        ]

        # calculer la baisse de facteur énergétique
        prix_fact_gain_ener = prix_agri.merge(gain_df, on="COMMODITY")
        # 1 : multiplier les prix des énergies par les gains énergétiques
        prix_fact_gain_ener["baisse_fact_ener"] = prix_fact_gain_ener[
            "valeur_x"
        ].multiply(prix_fact_gain_ener["valeur_y"])
        # unité en K€
        baisse_fact_ener = (prix_fact_gain_ener["baisse_fact_ener"].sum()) * 10

        # calculer l'indice de consommation
        indice_consommation = consommation_totale / conso_usage_energie["valeur"].sum()

        return {
            Impacts.EnergyConsumption(gain, year, sector=Sector.AGRICULTURE),
            Impacts.AvoidedEmissions(emission_ges, year, sector=Sector.AGRICULTURE),
            Impacts.EnergyBill(baisse_fact_ener, year),
            Impacts.TaxRevenue(None, year),
            Impacts.ConsumptionIndex(
                indice_consommation,
                year,
                sector=Sector.AGRICULTURE,
                action=self.name,
            ),
        }
