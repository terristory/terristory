# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.strategy_actions import (
    POLLUTANTS,
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    Requisite,
    Sector,
    Usage,
)
from terriapi.controller.strategy_actions.tools import (
    _calcul_baisse_emission_et_conso_par_secteur,
)


class ResidentialRefurbishment(AbstractAction):
    PARAM_RENOV = "distribution_amongst_dpe_levels"
    PARAM_RENOV_NAME = (
        "Répartition des rénovations par niveau de performance énergétique"
    )
    PARAM_GAIN = "heating_gain"
    PARAM_GAIN_NAME = "Gains attribués au chauffage"
    PARAM_CONS_LVL = "action1_conso_max_etiquette"
    PARAM_REFURB_GAIN = "action1_taux_renovation_etiquette"

    INPUT_NB_HOUSING = "1_nb_logements"
    INPUT_AVG_SURFACE = "1_surface_moy_logement"

    def __init__(self):
        super().__init__("Residential Refurbishment", "1")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        self.add_mandatory_requisite(
            "Consommation maximale des logements par étiquette",
            self.PARAM_CONS_LVL,
            Requisite.Types.PARAM,
        )
        self.add_mandatory_requisite(
            "Gains associés aux rénovations par étiquette",
            self.PARAM_REFURB_GAIN,
            Requisite.Types.PARAM,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_RENOV_NAME,
            self.PARAM_RENOV,
            Requisite.Types.INPUT_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_GAIN_NAME,
            self.PARAM_GAIN,
            Requisite.Types.INPUT_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            "Nombre de logements",
            self.INPUT_NB_HOUSING,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            "Surface moyenne des logements",
            self.INPUT_AVG_SURFACE,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

        for pollutant in POLLUTANTS:
            self.add_data_requisite(
                "emission_" + pollutant,
                DataSet.table_from_pollutant(pollutant),
                Sector.RESIDENTIAL,
                default=False,
            )

    async def _execute(self, year, *args, **kwargs):
        conso_chauffage_ecs = self.data["conso_residentiel"]

        # paramètres d'actions
        nb_logements_renoves = float(
            self.inputs.get(self.INPUT_NB_HOUSING, {}).get(year, 0)
        )
        surface_moyenne_logement = float(
            self.inputs.get(self.INPUT_AVG_SURFACE, {}).get(year, 0)
        )
        # Calcul de la surface moyenne rénovée
        surf_renovee = surface_moyenne_logement * nb_logements_renoves

        enable_air_pollutants_impacts = self.config.get(
            "enable_air_pollutants_impacts", False
        )
        if surf_renovee == 0:
            return {
                Impacts.EnergyConsumption(0.0, year, sector=Sector.RESIDENTIAL),
                Impacts.AvoidedEmissions(0.0, year, sector=Sector.RESIDENTIAL),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        feges = self.params[self.PARAM_FEGES]
        taux_renovation = self.params[self.PARAM_REFURB_GAIN]
        conso_max = self.params[self.PARAM_CONS_LVL]

        gain_chauffage = self.input_params[self.PARAM_GAIN][self.PARAM_GAIN_NAME] / 100

        proportion_type_renovation = self.input_params[self.PARAM_RENOV]

        # Calcul du gain par niveau fin de rénovation - nouvelle colonne dans taux_renovation
        # Différence entre la conso énergetique max de l'étiquette avant rénovation et après rénovation
        # valeur.y : conso avant réno,
        # valeur : conso après réno
        diff_conso = taux_renovation.merge(
            conso_max,
            how="left",
            left_on="etiquette_avant_renovation",
            right_on="batiment_etiquette_energetique",
        ).merge(
            conso_max,
            how="left",
            left_on="etiquette_apres_renovation",
            right_on="batiment_etiquette_energetique",
        )

        taux_renovation["gain"] = diff_conso["valeur_y"] - diff_conso["valeur"]

        # stockage des pourcentages de logements rénovés par niveau grossier
        # de rénovation pour plus de lisibilité du calcul final
        pour_log_reno_faible = (
            proportion_type_renovation[
                "pourcentage de rénovations énergétiques faibles"
            ]
            / 100
        )
        pour_log_reno_moyen = (
            proportion_type_renovation[
                "pourcentage de rénovations énergétiques moyennes"
            ]
            / 100
        )
        pour_log_reno_performant = (
            proportion_type_renovation[
                "pourcentage de rénovations énergétiques performantes"
            ]
            / 100
        )
        pour_log_reno_tres_performant = (
            proportion_type_renovation[
                "pourcentage de rénovations énergétiques très performantes"
            ]
            / 100
        )

        # le gain pour le type de rénovation faible est /2 car sinon indentique à la classe "moyenne"
        taux_renovation.loc[
            taux_renovation.type_niveau_renovation == "faibles", "gain"
        ] /= 2

        # Calcul du gain énergétique par niveau de rénovation
        # pour un niveau de rénovation faible
        gain_tot_faibles = (
            taux_renovation[taux_renovation.type_niveau_renovation == "faibles"][
                "valeur"
            ]
            * taux_renovation[taux_renovation.type_niveau_renovation == "faibles"][
                "gain"
            ]
        ).sum() * pour_log_reno_faible

        gain_tot_moyennes = (
            taux_renovation[taux_renovation.type_niveau_renovation == "moyennes"][
                "valeur"
            ]
            * taux_renovation[taux_renovation.type_niveau_renovation == "moyennes"][
                "gain"
            ]
        ).sum() * pour_log_reno_moyen

        gain_tot_performantes = (
            taux_renovation[taux_renovation.type_niveau_renovation == "performantes"][
                "valeur"
            ]
            * taux_renovation[taux_renovation.type_niveau_renovation == "performantes"][
                "gain"
            ]
        ).sum() * pour_log_reno_performant

        gain_tot_tres_performantes = (
            taux_renovation[
                taux_renovation.type_niveau_renovation == "très performantes"
            ]["valeur"]
            * taux_renovation[
                taux_renovation.type_niveau_renovation == "très performantes"
            ]["gain"]
        ).sum() * pour_log_reno_tres_performant

        terme_1 = (
            gain_tot_faibles
            + gain_tot_moyennes
            + gain_tot_performantes
            + gain_tot_tres_performantes
        ) / 1000

        # calcul du pourcentage de consommation d'électricité pour le chauffage,
        # aussi appelé mix énergétique,
        # à partir des données de conso du scénario AURA-EE ICC
        # division de la conso en électricité pour le chauffage par la conso totale pour le chauffage
        mix_chauffage_elec = (
            conso_chauffage_ecs.loc[
                (conso_chauffage_ecs.USAGE == Usage.HEATING)
                & (conso_chauffage_ecs.COMMODITY == Commodity.ELECTRICITY),
                "valeur",
            ]
            .div(
                #  Usage = 1 : chauffage
                #  Énergie = 2 : Électricité
                conso_chauffage_ecs.loc[
                    conso_chauffage_ecs.USAGE == Usage.HEATING, "valeur"
                ].sum()
            )
            .iloc[0]
        )

        # calcul du pourcentage de consommation d'électricité pour l'Eau chaude sanitaire (ECS),
        # aussi appelé mix énergétique,
        # à partir des données de conso du scénario AURA-EE ICC
        # division de la conso en éléctricité pour l'ECS par la conso totale pour l'ECS
        mix_ecs_elec = (
            conso_chauffage_ecs.loc[
                (conso_chauffage_ecs.USAGE == Usage.HOT_WATER)
                & (conso_chauffage_ecs.COMMODITY == Commodity.ELECTRICITY),
                "valeur",
            ]
            .div(
                #  Énergie = 2 : électricité
                #  Usage = 2 : Eaux chaudes sanitaires
                conso_chauffage_ecs.loc[
                    conso_chauffage_ecs.USAGE == Usage.HOT_WATER, "valeur"
                ].sum()
            )
            .iloc[0]
        )

        # paramètre Electricité kWhep to kWh
        electricite_kwhep_kwh = 2.58 - 1
        # calcul final terme 2
        # ajustement énergie primaire -> énergie finale
        terme_2 = (
            gain_chauffage / (1 + mix_chauffage_elec * electricite_kwhep_kwh)
        ) + ((1 - gain_chauffage) / (1 + mix_ecs_elec * electricite_kwhep_kwh))
        # Calcul final pour obtenir le gain énergétique
        gain_energetique = surf_renovee * terme_1 * terme_2

        # étapes pour calculer les émissions évitées
        # Calcul du mix énergétique par type d'énergie à partir de la table conso_chauffage_ecs
        # soit calcul du pourcentage de la consommation de chaque énergie  pour le chauffage et l'ECS
        # à partir des données de conso du scénario AURA-EE ICC
        # calcul du mix pour l'ECS
        conso_chauffage = conso_chauffage_ecs[
            conso_chauffage_ecs.USAGE == Usage.HEATING
        ].copy()
        conso_chauffage = conso_chauffage.sort_values(by="COMMODITY")
        conso_chauffage["id_conso_energie"] = conso_chauffage.COMMODITY
        #  Calcul du mix énergétique dans le chauffage
        conso_chauffage["mix"] = (
            conso_chauffage["valeur"] / conso_chauffage["valeur"].sum()
        )
        #  On défini un identifiant
        conso_chauffage.set_index("id_conso_energie", inplace=True)

        conso_ecs = conso_chauffage_ecs[
            conso_chauffage_ecs.USAGE == Usage.HOT_WATER
        ].copy()

        conso_ecs = conso_ecs.sort_values(by="COMMODITY")
        #  On lui affecte l'identifiant énergie de la table des catégories énergie
        conso_ecs["id_conso_energie"] = conso_ecs.COMMODITY
        #  Calcul du mix énergétique pour les eaux chaudes sanitaires
        conso_ecs["mix"] = conso_ecs["valeur"] / conso_ecs["valeur"].sum()

        conso_ecs.set_index("id_conso_energie", inplace=True)

        mix_energetique_ecs = conso_chauffage_ecs[
            conso_chauffage_ecs.USAGE == Usage.HOT_WATER
        ].assign(
            valeur=lambda x: x.valeur
            / conso_chauffage_ecs[conso_chauffage_ecs.USAGE == Usage.HOT_WATER][
                "valeur"
            ].sum()
        )

        mix_energetique_chauffage = conso_chauffage_ecs[
            conso_chauffage_ecs.USAGE == Usage.HEATING
        ].assign(
            valeur=lambda x: x.valeur
            / conso_chauffage_ecs[conso_chauffage_ecs.USAGE == Usage.HEATING][
                "valeur"
            ].sum()
        )
        # gain d'énergie économisée pour les différents type d'énergie consommée.
        # La somme des valeurs de gain par type d'énergie doit valoir 'gain_energetique'
        gain = (
            gain_energetique * gain_chauffage * conso_chauffage["mix"]
            + gain_energetique * (1 - gain_chauffage) * conso_ecs["mix"]
        ) / 1000.0  # GWh
        gain = gain.to_frame("valeur")
        gain["COMMODITY"] = conso_ecs.COMMODITY
        gain = gain.set_index("COMMODITY")

        # Création des facteurs d'émission de GES pour le chauffage et
        # l'ECS à partir du jeu de données facteur_emission
        # pour le chauffage résidentiels
        # TODO: check that we take right element into account here!
        facteur_emission_chauffage = feges.loc[
            (
                (
                    (
                        feges["COMMODITY#1"].isin(
                            (
                                Commodity.URBAN_HEAT_COOL,
                                Commodity.GAS,
                                Commodity.RENEWABLE_HEAT,
                                Commodity.COAL,
                            )
                        )
                    )
                    & (feges.USAGE == Usage.OTHERS)
                )
                | (
                    (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
                    & (feges.USAGE == Usage.HEATING)
                    & (feges.SECTOR == Sector.RESIDENTIAL)
                )
                | (
                    (feges["COMMODITY#1"] == Commodity.OIL)
                    & (feges.PARAMCOMMODITY == ParamCommodity.FUEL_OIL)
                )
            )
        ]
        facteur_emission_chauffage.set_index("COMMODITY#1", inplace=True)

        facteur_emission_ecs = feges.loc[
            (
                (
                    (
                        feges["COMMODITY#1"].isin(
                            (
                                Commodity.URBAN_HEAT_COOL,
                                Commodity.GAS,
                                Commodity.RENEWABLE_HEAT,
                                Commodity.COAL,
                            )
                        )
                    )
                    & (feges.USAGE == Usage.OTHERS)
                )
                | (
                    (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
                    & (feges.USAGE == Usage.HOT_WATER)
                    & (feges.SECTOR == Sector.RESIDENTIAL)
                )
                | (
                    (feges["COMMODITY#1"] == Commodity.OIL)
                    & (feges.PARAMCOMMODITY == ParamCommodity.FUEL_OIL)
                )
            )
        ]
        facteur_emission_ecs.set_index("COMMODITY#1", inplace=True)

        # Calcul gain émissions ges
        emission_ges = (
            gain_energetique
            * gain_chauffage
            * conso_chauffage["mix"]
            * facteur_emission_chauffage["valeur"]
            + gain_energetique
            * (1 - gain_chauffage)
            * conso_ecs["mix"]
            * facteur_emission_ecs["valeur"]
        )
        # unité en ktco2
        emission_ges /= 1000.0
        emission_ges = emission_ges.to_frame("valeur")
        emission_ges["COMMODITY"] = conso_ecs.COMMODITY
        emission_ges = emission_ges.set_index("COMMODITY")
        emission_ges = emission_ges.dropna()

        # calcul du prix global pour le chauffage en €/100kWh en multipliant les prix par énergie et le mix énergetique du chauffage
        prix_mix_chauff = (
            prix[prix.SECTOR.isin([Sector.RESIDENTIAL, Sector.UNDIFFERENCIATED])]
            .set_index("COMMODITY")
            .merge(mix_energetique_chauffage, left_on="COMMODITY", right_on="COMMODITY")
        )
        prix_global_chauffage = (
            prix_mix_chauff["valeur_x"] * prix_mix_chauff["valeur_y"]
        ).sum()

        # calcul du prix global pour l'ECS en €/100kWh en multipliant les prix par énergie et le mix énergetique du chauffage
        prix_mix_ecs = (
            prix[prix.SECTOR.isin([Sector.RESIDENTIAL, Sector.UNDIFFERENCIATED])]
            .set_index("id_energie")
            .merge(mix_energetique_ecs, left_on="id_energie", right_on="energie")
        )
        prix_global_ecs = (prix_mix_ecs["valeur_x"] * prix_mix_ecs["valeur_y"]).sum()
        gain = gain.dropna()
        emission_ges = emission_ges.dropna()
        # Calcul final
        baisse_facture_energetique = (
            gain_energetique
            * (
                gain_chauffage * prix_global_chauffage
                + (1 - gain_chauffage) * prix_global_ecs
            )
            / 100
        )
        # Émission polluants atmosphérique
        if enable_air_pollutants_impacts:
            # Calcul des émissions de polluants évités

            # somme de consommation secteur résidentielle usage chauffage : 1 et ECS : 2
            sum_conso_chauffage_ecs = conso_chauffage_ecs.loc[
                (conso_chauffage_ecs.USAGE.isin((Usage.HEATING, Usage.HOT_WATER))),
                "valeur",
            ].sum()

            # retranscription en poucentage de la baisse énergétique
            baisse_polluant_coef_multiplicateur = (
                gain_energetique / sum_conso_chauffage_ecs
            ) / 1000

            pollutants_gains = {}
            for pollutant in POLLUTANTS:
                emission_pollutant = self.data.get("emission_" + pollutant, False)

                # impacts on each pollutant using the decrease by sector
                pollutants_gains[pollutant] = (
                    _calcul_baisse_emission_et_conso_par_secteur(
                        emission_pollutant,
                        baisse_polluant_coef_multiplicateur,
                        Commodity,
                        Commodity.ALL,
                    )
                )

            return {
                Impacts.EnergyConsumption(
                    gain["valeur"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.AvoidedEmissions(
                    emission_ges["valeur"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.PollutantsCOVNM(
                    pollutants_gains["covnm"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.PollutantsNH3(
                    pollutants_gains["nh3"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.PollutantsPM10(
                    pollutants_gains["pm10"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.PollutantsPM25(
                    pollutants_gains["pm25"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.PollutantsNOX(
                    pollutants_gains["nox"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.PollutantsSOX(
                    pollutants_gains["sox"], year, sector=Sector.RESIDENTIAL
                ),
                Impacts.EnergyBill(baisse_facture_energetique, year),
                Impacts.TaxRevenue(None, year),
            }

        return {
            Impacts.EnergyConsumption(gain["valeur"], year, sector=Sector.RESIDENTIAL),
            Impacts.AvoidedEmissions(
                emission_ges["valeur"], year, sector=Sector.RESIDENTIAL
            ),
            Impacts.EnergyBill(baisse_facture_energetique, year),
            Impacts.TaxRevenue(None, year),
        }
