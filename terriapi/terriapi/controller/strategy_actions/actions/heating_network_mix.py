# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from sanic.log import logger

from terriapi.controller.analyse import get_global_irradiation
from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)

from .solar_thermal import SolarThermal


class HeatingNetworkMix(SolarThermal):
    INPUT_HEAT_DELIVERED = "8_chaleur_livree"
    INPUT_PERC_SOLAR = "8_part_solaire"

    def __init__(self):
        # we are in ecs mode and not combined
        self.plant_type = "ecs"

        # we call __init__ of super from SolarThermal and not __init__ of SolarThermal
        super(SolarThermal, self).__init__("Heating network mix", "8")

    def add_requisites(self):
        # global irradiation
        self.add_mandatory_requisite(
            "Global irradiation",
            "Global irradiation",
            Requisite.Types.PARAM,
            computation_function=get_global_irradiation,
        )

        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_PERFORMANCE,
            self.PARAM_PERFORMANCE,
            Requisite.Types.INPUT_ADV_PARAM,
        )

        # entrées de l'utilisateur => optional here as it will be filled through pre_hook
        self.add_optional_requisite(
            None,
            self.INPUT_NB_COLLEC_BUILDINGS,
            self.INPUT_NB_COLLEC_BUILDINGS,
            Requisite.Types.INPUT,
        )
        self.add_optional_requisite(
            None,
            self.INPUT_SURFACE_SENSORS,
            self.INPUT_SURFACE_SENSORS,
            Requisite.Types.INPUT,
        )
        # proper parameters for current action
        self.add_mandatory_requisite(
            self.INPUT_HEAT_DELIVERED,
            self.INPUT_HEAT_DELIVERED,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_PERC_SOLAR,
            self.INPUT_PERC_SOLAR,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

    def mix_energetique(self, chaleur, part):
        """
        Calcul du mix énergétique pour une année
        https://gitlab.com/terristory/terristory/-/issues/219

        Parameters
        ----------
        chaleur : float
                Chaleur livrée en MWh
        part : float
            Part de solaire thermique à intégrer dans le mix énergétique

        Returns
        -------
        float
            surface de panneaux solaires
        float
            surface_sol: float,
        int
            équivalent nombre de logements chauffés
        """
        chaleur_thermique = chaleur * part / 100
        # production d'un panneau 450Kwh/m²
        surface_panneaux = chaleur_thermique / 450 * 1000
        # équivalent foncier en Hectare
        surface_sol = surface_panneaux * 2.5 / 10000
        # Équivalent logements chauffés par le solaire
        logements_chauffes = chaleur_thermique / 12

        return {
            "surface_panneaux": surface_panneaux,
            "surface_sol": surface_sol,
            "logements_chauffes": logements_chauffes,
        }

    async def _pre_execute(self, *args, **kwargs):
        """
        Cette action va calculer la surface en panneau solaire thermique qui serait
        nécessaire pour obtenir la part souhaitée dans le mix énergétique mais aussi l"impact
        sur l’emploi de la surface en panneau solaire à implanter
        (lien à faire avec les calculs de l’action solaire thermique)
        cf https://gitlab.com/terristory/terristory/-/issues/218
        """
        # on fait les calculs du mix énergétique
        last_part_mix = None
        has_done_anything = False
        impacts = Impacts()

        self.inputs[self.INPUT_NB_COLLEC_BUILDINGS] = {}
        self.inputs[self.INPUT_SURFACE_SENSORS] = {}

        for year in self.years():
            year = str(year)
            if (
                year not in self.inputs[self.INPUT_HEAT_DELIVERED]
                or year not in self.inputs[self.INPUT_PERC_SOLAR]
            ):
                logger.debug(
                    f"Skipping {self.name} for year {year} as no input has been specified"
                )
                continue
            # paramètres d'actions
            input_heat_delivered = float(self.inputs[self.INPUT_HEAT_DELIVERED][year])
            input_perc_solar = float(self.inputs[self.INPUT_PERC_SOLAR][year])

            params_mix_enr = self.mix_energetique(
                input_heat_delivered,
                input_perc_solar,
            )

            surface_panneaux = params_mix_enr["surface_panneaux"]
            surface_sol = params_mix_enr["surface_sol"]
            logements_chauffes = params_mix_enr["logements_chauffes"]

            if last_part_mix is not None:
                # la part de solaire n-1 est à soustraire pour l'impact emploi
                # on recalcule donc la surface de panneaux avec la part précédente
                sfp = self.mix_energetique(
                    input_heat_delivered,
                    input_perc_solar - last_part_mix,
                )

                nb_batiment_deduis = sfp["surface_panneaux"] / 100
            else:
                # nombre de logements pour l'action 4
                nb_batiment_deduis = surface_panneaux / 100

            self.inputs[self.INPUT_NB_COLLEC_BUILDINGS][year] = 100
            if nb_batiment_deduis > 0:
                # surface moyenne considérée pour l'action 4 : 100 m²
                self.inputs[self.INPUT_SURFACE_SENSORS][year] = nb_batiment_deduis
                has_done_anything = True
            else:
                self.inputs[self.INPUT_SURFACE_SENSORS][year] = 0.0

            # on stocke les paramètres pour l'année suivante
            # si et seulement si la valeur n'est pas nulle
            if float(input_perc_solar):
                last_part_mix = float(input_perc_solar)

            impacts += {
                Impacts.MixEnergy("surface_panneaux", round(surface_panneaux, 1), year),
                Impacts.MixEnergy("surface_sol", round(surface_sol, 2), year),
                Impacts.MixEnergy(
                    "logements_chauffes", round(logements_chauffes), year
                ),
            }

        if has_done_anything:
            self.mix_impacts = impacts
        else:
            self.disabled = True

    async def _execute(self, year, *args, **kwargs):
        impacts = await super()._execute(year, *args, **kwargs)
        return self.mix_impacts + impacts
