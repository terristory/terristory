# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)


class MethanizationInjection(AbstractAction):
    INPUT_NB_METHANIZERS = "6b_nb_methaniseur"
    INPUT_INJECT_CAP = "6b_capacite_injec_biometh"

    PARAM_PURIF_PERF = "Taux de rendement du purificateur"
    PARAM_DIGEST_HEAT_CONSUM_RATE = (
        "Taux de consommation pour le chauffage du digesteur"
    )
    PARAM_TIME_FULLPOWER = "Temps de fonctionnement en pleine puissance (heures)"
    PARAM_LOWER_CALORIFIC_VALUE_METHANE = "PCI du methane  (kWh / M3)"

    def __init__(self):
        super().__init__("Methanization by injection", "6b")

    def add_requisites(self):
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_PURIF_PERF,
            self.PARAM_PURIF_PERF,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_DIGEST_HEAT_CONSUM_RATE,
            self.PARAM_DIGEST_HEAT_CONSUM_RATE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TIME_FULLPOWER,
            self.PARAM_TIME_FULLPOWER,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_LOWER_CALORIFIC_VALUE_METHANE,
            self.PARAM_LOWER_CALORIFIC_VALUE_METHANE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_NB_METHANIZERS,
            self.INPUT_NB_METHANIZERS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_INJECT_CAP,
            self.INPUT_INJECT_CAP,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

    async def _execute(self, year, *args, **kwargs):
        """
        https://gitlab.com/terristory/terristory/-/issues/321
        """
        nb_methaniseur = int(self.inputs[self.INPUT_NB_METHANIZERS][year])
        capacite_injection = float(self.inputs[self.INPUT_INJECT_CAP][year])
        puissance_installee = nb_methaniseur * capacite_injection

        # TODO: avoid if no capacity?
        if nb_methaniseur == 0 or capacite_injection == 0:
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.BIOGAS_INJEC,
                    energy_vector=EnergyVector.BIOMETHANE,
                ),
                Impacts.AvoidedEmissions(0.0, year),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        adv_params = self.input_params

        feges = self.params[self.PARAM_FEGES]

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        # Stockage des variables ci-dessous pour plus de lisibilite des calculs finaux
        rendement_purif = adv_params[self.PARAM_PURIF_PERF] / 100

        conso_chauf_digesteur = adv_params[self.PARAM_DIGEST_HEAT_CONSUM_RATE] / 100

        tp_fonct_pleine_puiss = adv_params[self.PARAM_TIME_FULLPOWER]
        pci_methane = adv_params[self.PARAM_LOWER_CALORIFIC_VALUE_METHANE]

        facteur_emiss_gaz = feges[
            feges.PARAMCOMMODITY == ParamCommodity.GAS
        ].valeur.iloc[0]
        prix_moy_gaz = prix.loc[
            (prix.PARAMCOMMODITY == ParamCommodity.GAS)
            & (prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.INDUSTRY_WITHOUT_ENERGY))),
            "valeur",
        ].mean()

        # Energie livree en M3
        biometh_livree = puissance_installee * tp_fonct_pleine_puiss
        # Énergie livrée en MWh
        energie_livree = biometh_livree * pci_methane / 1000
        # Energie produite en KWH PCI
        energie_produite = (
            biometh_livree
            * pci_methane
            * ((1 - conso_chauf_digesteur) * rendement_purif)
            / 1000
        )

        # Emissions evitees en tCO2
        emiss_evitees = energie_produite * facteur_emiss_gaz
        # Gains lies a la vente de biomethane en k€
        # Baisse de la facture energetique en k€
        baisse_factur_ener = energie_produite * prix_moy_gaz / 100
        # Recettes fiscales  (CVAE supprimée)
        # Recette par maille (commune, communauté de commune, département, region)
        recette_fiscale = {
            "commune": 0,
            "epci": 0,
            "departement": 0,
            "region": 0,
        }

        # Énergie en GWh
        energie_livree /= 1000.0
        energie_produite /= 1000.0
        # Émission en ktCO2
        emiss_evitees /= 1000.0

        return {
            Impacts.EnergyProduction(
                energie_produite,
                year,
                prod_type=RenewableProd.BIOGAS_INJEC,
                energy_vector=EnergyVector.BIOMETHANE,
            ),
            # TODO: fix this? what sector is counted for avoided emissions?
            # problem is that these emissions are not used in original counts
            # Should we include them now?
            Impacts.AvoidedEmissions(
                0.0,  # emiss_evitees,
                year,
                sector=Sector.RESIDENTIAL,
                commodity=Commodity.ELECTRICITY,
            ),
            Impacts.EnergyBill(baisse_factur_ener, year),
            Impacts.TaxRevenue(recette_fiscale, year),
        }
