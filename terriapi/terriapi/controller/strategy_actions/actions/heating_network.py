# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)


class HeatingNetwork(AbstractAction):
    INPUT_HEAT_RESID_CONS = "5_conso_totale_chauffage_resi"
    INPUT_HEAT_SERV_CONS = "5_conso_totale_chauffage_tertiaire"
    INPUT_RATIO_CONNECTABLE_RESID = "5_part_conso_resi_racc"
    INPUT_RATIO_CONNECTABLE_SERV = "5_part_conso_tertiaire_racc"

    PARAM_LOWER_CALORIFIC_VALUE = "PCI anhydre"
    PARAM_HUMIDITY_RATE = "Taux d'humidité du bois"
    PARAM_PERF_HEATNETW = "Rendement réseaux de chaleur"
    PARAM_PERF_WOODBOIL = "Rendement chaufferie bois"
    PARAM_PERF_GASBOIL = "Rendement chaufferie gaz"
    PARAM_HEATING_ENERGY = "Énergie utilisée pour chauffage"
    PARAM_RATIO_WOOD_IN_HEAT = (
        "Part bois dans la chaleur produite par le réseau envisagé"
    )
    PARAM_RATIO_HEAT_PROD_WOODBOIL = (
        "Ratio de chaleur produite sur la puissance chaufferie bois"
    )
    PARAM_RATIO_GAS_WOOD_POWERS = (
        "Ratio de puissance chaufferie gaz sur la puissance chaufferie bois"
    )

    def __init__(self):
        super().__init__("Heating network", "5")

    def add_requisites(self):
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_LOWER_CALORIFIC_VALUE,
            self.PARAM_LOWER_CALORIFIC_VALUE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_HUMIDITY_RATE,
            self.PARAM_HUMIDITY_RATE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_PERF_HEATNETW,
            self.PARAM_PERF_HEATNETW,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_PERF_WOODBOIL,
            self.PARAM_PERF_WOODBOIL,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_PERF_GASBOIL,
            self.PARAM_PERF_GASBOIL,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_HEATING_ENERGY,
            self.PARAM_HEATING_ENERGY,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_RATIO_WOOD_IN_HEAT,
            self.PARAM_RATIO_WOOD_IN_HEAT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_optional_requisite(
            0.0,
            self.PARAM_RATIO_HEAT_PROD_WOODBOIL,
            self.PARAM_RATIO_HEAT_PROD_WOODBOIL,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_optional_requisite(
            0.0,
            self.PARAM_RATIO_GAS_WOOD_POWERS,
            self.PARAM_RATIO_GAS_WOOD_POWERS,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_HEAT_RESID_CONS,
            self.INPUT_HEAT_RESID_CONS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_HEAT_SERV_CONS,
            self.INPUT_HEAT_SERV_CONS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_RATIO_CONNECTABLE_RESID,
            self.INPUT_RATIO_CONNECTABLE_RESID,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_RATIO_CONNECTABLE_SERV,
            self.INPUT_RATIO_CONNECTABLE_SERV,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

    async def _execute(self, year, *args, **kwargs):
        """
        calcul des impacts sur les émissions de GES, le gain énergétique, la facture énergétique
        pour l'action 5 "réseau de chaleur"
        """
        # inputs
        conso_totale_chauffage_resi = int(
            round(float(self.inputs[self.INPUT_HEAT_RESID_CONS][year]), 0)
        )
        conso_totale_chauffage_tertiaire = int(
            round(float(self.inputs[self.INPUT_HEAT_SERV_CONS][year]), 0)
        )
        part_conso_resi_racc = (
            int(round(float(self.inputs[self.INPUT_RATIO_CONNECTABLE_RESID][year]), 0))
            / 100
        )
        part_conso_tertiaire_racc = (
            int(round(float(self.inputs[self.INPUT_RATIO_CONNECTABLE_SERV][year]), 0))
            / 100
        )

        # data
        conso_usage_energie = self.data["conso_residentiel"]

        if (conso_totale_chauffage_resi == 0 or part_conso_resi_racc == 0) and (
            conso_totale_chauffage_tertiaire == 0 or part_conso_tertiaire_racc
        ):
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.BIOMASS_THER_CHAUFFERIE,
                    energy_vector=EnergyVector.HEAT,
                ),
                Impacts.AvoidedEmissions(0.0, year),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        parametres_avances = self.input_params

        pci_anhydre = parametres_avances[self.PARAM_LOWER_CALORIFIC_VALUE]
        taux_humidite = parametres_avances[self.PARAM_HUMIDITY_RATE] / 100
        rendement_rdc = parametres_avances[self.PARAM_PERF_HEATNETW] / 100
        rendement_chaufferie_bois = parametres_avances[self.PARAM_PERF_WOODBOIL] / 100
        rendement_chaufferie_gaz = parametres_avances[self.PARAM_PERF_GASBOIL] / 100
        energie_chauffage = parametres_avances[self.PARAM_HEATING_ENERGY] / 100
        part_bois = parametres_avances[self.PARAM_RATIO_WOOD_IN_HEAT] / 100

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        feges = self.params[self.PARAM_FEGES]

        # PCI moyen plaquette forestière
        pci_moy_plaq_forest = (
            round(
                (
                    ((pci_anhydre * (1 - taux_humidite)) - 6.786 * taux_humidite * 100)
                    / 1000
                ),
                1,
            )
            * 1000
        )

        # Calcul de l'énergie livrée en MWh-th (MWh thermique)
        energie_livree = (
            conso_totale_chauffage_resi * part_conso_resi_racc
            + conso_totale_chauffage_tertiaire * part_conso_tertiaire_racc
        )
        # Calcul des émissions évitées tCO2
        # calculs préliminaires : gaz consommé MWh
        # reformatage de la table facteurs d'émissions
        # TODO: check logic?
        # same filter than in action 1 residential refurbishment l.354
        feges_chauffage = feges.loc[
            (
                (
                    feges["COMMODITY#1"].isin(
                        (
                            Commodity.URBAN_HEAT_COOL,
                            Commodity.GAS,
                            Commodity.RENEWABLE_HEAT,
                            Commodity.COAL,
                            Commodity.OIL,
                        )
                    )
                )
                & (feges.USAGE == Usage.OTHERS)
            )
            | (
                (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
                & (feges.USAGE == Usage.HEATING)
                & (feges.SECTOR == Sector.RESIDENTIAL)
            )
        ]
        feges_chauffage = feges_chauffage.set_index("COMMODITY#1")
        feges_ecs = feges.loc[
            (
                (
                    (
                        feges["COMMODITY#1"].isin(
                            (
                                Commodity.URBAN_HEAT_COOL,
                                Commodity.GAS,
                                Commodity.RENEWABLE_HEAT,
                                Commodity.COAL,
                                Commodity.OIL,
                            )
                        )
                    )
                    & (feges.USAGE == Usage.OTHERS)
                )
                | (
                    (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
                    & (feges.USAGE == Usage.HOT_WATER)
                    & (feges.SECTOR == Sector.RESIDENTIAL)
                )
            )
        ]
        feges_ecs = feges_ecs.set_index("COMMODITY#1")

        mix_energetique_ecs = conso_usage_energie[
            conso_usage_energie.USAGE == Usage.HOT_WATER
        ].assign(
            valeur=lambda x: x.valeur
            / conso_usage_energie[conso_usage_energie.USAGE == Usage.HOT_WATER][
                "valeur"
            ].sum()
        )

        mix_energetique_chauffage = conso_usage_energie[
            conso_usage_energie.USAGE == Usage.HEATING
        ].assign(
            valeur=lambda x: x.valeur
            / conso_usage_energie[conso_usage_energie.USAGE == Usage.HEATING][
                "valeur"
            ].sum()
        )

        # calcul terme 1 : gain des émissions de GES pour le chauffage
        fecm = feges_chauffage.merge(
            mix_energetique_chauffage, left_on="COMMODITY#2", right_on="COMMODITY"
        )
        somme_emission_chauffage = (fecm["valeur_x"] * fecm["valeur_y"]).sum()

        # calcul terme 2 : gains des émissions de GES pour l'ECS
        feecsm = feges_ecs.merge(
            mix_energetique_ecs, left_on="COMMODITY#2", right_on="COMMODITY"
        )
        somme_emission_ecs = (feecsm["valeur_x"] * feecsm["valeur_y"]).sum()

        # reformatage de la table facteurs d'émissions
        somme_emission_ecs = (feecsm["valeur_x"] * feecsm["valeur_y"]).sum()

        # Calcul final emission évitées en tCO2
        energie_produite = energie_livree / (rendement_rdc)

        energie_bois = energie_produite * part_bois

        bois_consom = energie_bois / (rendement_chaufferie_bois)

        energie_gaz = energie_produite * (1 - (part_bois))

        gaz_consom = energie_gaz / (rendement_chaufferie_gaz)

        emissions_evitees = energie_livree * (
            (energie_chauffage) * somme_emission_chauffage
            + (1 - energie_chauffage) * somme_emission_ecs
        ) - (
            gaz_consom
            * feges_chauffage[feges_chauffage.id_energie == 3]["valeur"].iloc[0]
        )

        # Baisse de la facture énergétique en k€ (ICAREE:
        # Net du cout de l'énergie (Bois et Gaz))
        # calculs préliminaires : bois consommé MWh

        # calcul du prix global pour le chauffage en €/100kWh en multipliant les prix par énergie et le mix énergetique du chauffage
        prix_mix_chauff = (
            prix[prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.UNDIFFERENCIATED))]
            .set_index("COMMODITY")
            .merge(mix_energetique_chauffage, left_on="COMMODITY", right_on="COMMODITY")
        )
        prix_global_chauffage = (
            prix_mix_chauff["valeur_x"] * prix_mix_chauff["valeur_y"]
        ).sum()

        # calcul du prix global pour l'ECS en €/100kWh en multipliant les prix par énergie et le mix énergetique du chauffage
        prix_mix_ecs = (
            prix[prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.UNDIFFERENCIATED))]
            .set_index("COMMODITY")
            .merge(mix_energetique_ecs, left_on="COMMODITY", right_on="COMMODITY")
        )
        prix_global_ecs = (prix_mix_ecs["valeur_x"] * prix_mix_ecs["valeur_y"]).sum()

        # Calcul final baisse de la facture énergétique en k€
        baisse_facture_energie = (
            energie_livree
            * (
                energie_chauffage * prix_global_chauffage
                + (1 - energie_chauffage) * prix_global_ecs
            )
            - gaz_consom
            * prix[
                (prix.COMMODITY == Commodity.GAS) & (prix.SECTOR == Sector.RESIDENTIAL)
            ]["valeur"].iloc[0]
            - bois_consom  # COMMODITY = 7 : Gaz
            * prix[prix.COMMODITY == Commodity.RENEWABLE_HEAT]["valeur"].iloc[0]
        ) / 100  # id_energie = 3 : Biomasse

        # Énergie en GWh
        energie_livree /= 1000.0

        # Émission en ktCO2
        emissions_evitees /= 1000.0

        return {
            Impacts.EnergyProduction(
                energie_livree,
                year,
                prod_type=RenewableProd.BIOMASS_THER_CHAUFFERIE,
                energy_vector=EnergyVector.HEAT,
            ),
            # TODO: fix this? what sector is counted for avoided emissions?
            # problem is that these emissions are not used in original counts
            # Should we include them now?
            Impacts.AvoidedEmissions(
                0.0,  # emissions_evitees,
                year,
                sector=Sector.RESIDENTIAL,
                commodity=Commodity.RENEWABLE_HEAT,
            ),
            Impacts.EnergyBill(baisse_facture_energie, year),
            Impacts.TaxRevenue(None, year),
        }
