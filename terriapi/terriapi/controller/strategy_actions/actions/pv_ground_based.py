# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.analyse import get_global_irradiation
from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
)


class GroundBasedPVPlant(AbstractAction):
    INPUT_PEAK_POWER = "3_puiss_crete"

    PARAM_LOSSES = (
        "Pertes liées au système (perte de température, réflectance angulaire,...)"
    )
    PARAM_RATIO_SURF_POWER = "Ratio surface/puissance installée"
    PARAM_LAND_PRICE = "Prix de la surface de terrain"
    PARAM_TAX_RATE_PROPERTY = "Taux d'imposition foncière"
    PARAM_TAX_RATE_CFE = "Taux d'imposition CFE"
    PARAM_TAX_RATE_DEVELOPMENT = "Taux d'imposition aménagement"
    PARAM_IFER = "ifer"

    def __init__(self):
        super().__init__("Ground-based photovoltaic plant", "3")

    def add_requisites(self):
        # global irradiation
        self.add_mandatory_requisite(
            "Global irradiation",
            "Global irradiation",
            Requisite.Types.PARAM,
            computation_function=get_global_irradiation,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_LOSSES,
            self.PARAM_LOSSES,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_RATIO_SURF_POWER,
            self.PARAM_RATIO_SURF_POWER,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_LAND_PRICE,
            self.PARAM_LAND_PRICE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_PROPERTY,
            self.PARAM_TAX_RATE_PROPERTY,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_CFE,
            self.PARAM_TAX_RATE_CFE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_DEVELOPMENT,
            self.PARAM_TAX_RATE_DEVELOPMENT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        # Paramètres statiques
        self.add_mandatory_requisite(
            "IFER",
            self.PARAM_IFER,
            Requisite.Types.PARAM,
        )
        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            "Puissance crête",
            self.INPUT_PEAK_POWER,
            Requisite.Types.INPUT,
        )
        # données requises
        self.add_data_requisite(
            "conso_industrielle", DataSet.CONSUMPTION, Sector.INDUSTRY_WITHOUT_ENERGY
        )
        self.add_data_requisite(
            "conso_residentielle", DataSet.CONSUMPTION, Sector.RESIDENTIAL
        )

    async def _execute(self, year, *args, **kwargs):
        """
        Calcul d'impact GES, NRJ et facture NRJ
        pour l'action d'ajout de centrale photovoltaïque au sol
        cf : https://gitlab.com/terristory/terristory/-/issues/299
        """
        puissance = float(self.inputs[self.INPUT_PEAK_POWER][year])
        # type_systeme = user_params['3_photov_type'][year]
        # pour l'instant fixe en AURA
        type_systeme = "fixe"

        if puissance == 0:
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.SOLAR_PV,
                    energy_vector=EnergyVector.ELECTRICITY,
                ),
                Impacts.AvoidedEmissions(0.0, year),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        params = self.input_params

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        feges = self.params[self.PARAM_FEGES]

        facteur_emiss_pv_va = feges[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.RENEWABLEPROD == RenewableProd.SOLAR_PV)
        ]["valeur"].iloc[0]

        # chargement du prix de l'électricité - moyenne ponderée du prix résidentiel et industriel
        # Extraire les valeurs de consommation
        consommation_industrielle = self.data["conso_industrielle"]["valeur"].sum()
        consommation_residentielle = self.data["conso_residentielle"]["valeur"].sum()

        # Extraire les prix de l'électricité pour les secteurs résidentiel et industriel
        prix = prix[
            (prix.COMMODITY == Commodity.ELECTRICITY)
            & (prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.INDUSTRY_WITHOUT_ENERGY)))
        ]

        # Séparer les prix et les associer à leur consommation correspondante
        prix_residentiel = prix[prix.SECTOR == Sector.RESIDENTIAL]["valeur"].mean()
        prix_industriel = prix[prix.SECTOR == Sector.INDUSTRY_WITHOUT_ENERGY][
            "valeur"
        ].mean()

        # Calculer la consommation totale
        consommation_totale = consommation_residentielle + consommation_industrielle

        # Calculer la moyenne pondérée
        prix_elec_pv_va = (
            (prix_residentiel * consommation_residentielle)
            + (prix_industriel * consommation_industrielle)
        ) / consommation_totale

        # valeur de la perte système
        perte_systeme = params[self.PARAM_LOSSES] / 100
        # Ratio surface/puissance installée
        ratio_surf_puiss_instal = params[self.PARAM_RATIO_SURF_POWER]
        # Prix du terrain en euros par hectare
        prix_surface_terrain = params[self.PARAM_LAND_PRICE]
        # Taux d'imposition fonciere
        taux_imp_fonciere = params[self.PARAM_TAX_RATE_PROPERTY] / 100
        # Taux d'imposition pour la Cotisation Fonciere des Entreprises (CFE)
        taux_imp_CFE = params[self.PARAM_TAX_RATE_CFE]
        # Taux de la taxe d'amenagement
        taux_imp_amenagemt = params[self.PARAM_TAX_RATE_DEVELOPMENT] / 100
        # Paramètre de l'IFER (Imposition Forfaitaire des Entreprises de Réseaux)
        ifer = self.params[self.PARAM_IFER]
        taux_IFER = ifer[ifer.category == "pv"]["value"].iloc[0]

        # Energie produite
        gain_energetique = (
            self.params["Global irradiation"][type_systeme]
            * puissance
            * (1 - perte_systeme)
        )
        # Emissions evitees
        emissions_evitees = gain_energetique * facteur_emiss_pv_va
        # Baisse de la facture énergétique du territoire
        baisse_facture = gain_energetique * prix_elec_pv_va / 100

        # Calculs retombées fiscales
        # Chiffre d'affaires
        chiffre_aff = 63.9 * gain_energetique / 1000
        # Charges
        charges = 26.2 * puissance
        # Surface du terrain
        surface_terrain = ratio_surf_puiss_instal * puissance
        # valeur locative cadastrale totale des projets
        valeur_loc_cadastr = prix_surface_terrain * surface_terrain / 1000
        # Taxe fonciere
        taxe_fonciere = 0.5 * valeur_loc_cadastr * taux_imp_fonciere
        # Imposition forfaitaire sur les entreprises de réseaux (ifer)
        valeur_ifer = puissance * taux_IFER
        # Énergie en GWh
        gain_energetique /= 1000.0

        # Émission en ktCO2
        emissions_evitees /= 1000.0

        # CFE sans degrevement
        cfe = 0.7 * taux_imp_CFE * valeur_loc_cadastr

        # calculs complémentaires à coder mais qui pour l'instant ne sont pas exploité
        # Taux d'imposition pour l'amenagment
        taxe_amenag = surface_terrain * 10 * 100 / 1000 * taux_imp_amenagemt
        # Part des recettes fiscales par type de territoire
        recette_fiscale = {
            "commune": 0.3 * taxe_fonciere
            + 0.5 * valeur_ifer
            + 0.5 * cfe
            + (5 / 7) * taxe_amenag,
            "epci": 0.2 * taxe_fonciere + 0.5 * cfe,
            "departement": 0.5 * taxe_fonciere
            + 0.5 * valeur_ifer
            + (2 / 7) * taxe_amenag,
            "region": 0,
        }

        return {
            Impacts.EnergyProduction(
                gain_energetique,
                year,
                prod_type=RenewableProd.SOLAR_PV,
                energy_vector=EnergyVector.ELECTRICITY,
            ),
            Impacts.AvoidedEmissions(
                0.0,  # emissions_evitees,
                year,
                commodity=Commodity.ELECTRICITY,
                sector=Sector.ENERGY_INDUSTRY,
            ),
            Impacts.EnergyBill(baisse_facture, year),
            Impacts.TaxRevenue(recette_fiscale, year),
        }
