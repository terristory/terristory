# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
from __future__ import annotations

import pandas as pd

from terriapi.controller.strategy_actions import (
    POLLUTANTS,
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    Requisite,
    Sector,
    Usage,
)
from terriapi.controller.strategy_actions.loaders import interpolators
from terriapi.controller.strategy_actions.loaders.specific_loaders import (
    heating_systems_distribution,
)


def calculer_diff_conso(
    user_params_equipement,
    repartition_equipement_reference,
    conso_totale_chaleur_enr_pac,
):
    """cette fonction permet de calculer la différence de consommation des équipements de chauffage
    nécessaire pour calculer l'investissement pour le secteur résidentiel et tertiaire
    """

    # pas de calcul des impacts économiques d'une réduction de la consommation d'énergie d'un équipement
    diff_conso_equipement = 0
    if user_params_equipement > repartition_equipement_reference:
        diff_conso_equipement = (
            user_params_equipement - repartition_equipement_reference
        ) * conso_totale_chaleur_enr_pac

    return diff_conso_equipement


class HeatingConversionServices(AbstractAction):
    INPUT_BIOMASS = "17_chauffage_biomasse"
    INPUT_HEATPUMP = "17_chauffage_pac"
    INPUT_DISTNET = "17_chauffage_rdc"
    INPUT_ELECTR = "17_chauffage_elec"
    INPUT_GAS = "17_chaudiere_gaz"
    INPUT_FIOUL_LPG = "17_chaudiere_fioul_gpl"

    CONSUMPTION_KEYS = [DataSet.CONSUMPTION, Sector.SERVICES, Usage.HEATING]

    @property
    def input_distrib_keys(self):
        return {
            "BIOMASS": self.INPUT_BIOMASS,
            "HEATPUMP": self.INPUT_HEATPUMP,
            "DISTNET": self.INPUT_DISTNET,
            "ELECTR": self.INPUT_ELECTR,
            "GAS": self.INPUT_GAS,
            "FIOUL_LPG": self.INPUT_FIOUL_LPG,
        }

    PARAM_PERF_COEF = "Coefficient de performance"
    PARAM_GAS_DISTRIB = (
        "Répartition de la consommation de gaz dans le chauffage tertiaire"
    )
    PARAM_OIL_DISTRIB = "Répartition de la consommation des produits pétroliers dans le chauffage tertiaire"
    PARAM_ELEC_DISTRIB = (
        "Répartition de la consommation elec dans le chauffage tertiaire"
    )
    PARAM_HEAT_DISTRIB = (
        "Répartition de la consommation de chaleur EnRt dans le chauffage tertiaire"
    )

    YEAR_PARAM_PERF_SYSTEMS = "Modifier les performances des appareils"

    def __init__(self):
        super().__init__("Heating system conversion in services", "17")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions ATMO",
            self.PARAM_FEGES_ATMO,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.ATMO_GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_PERF_COEF,
            self.PARAM_PERF_COEF,
            Requisite.Types.INPUT_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_GAS_DISTRIB,
            self.PARAM_GAS_DISTRIB,
            Requisite.Types.INPUT_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_OIL_DISTRIB,
            self.PARAM_OIL_DISTRIB,
            Requisite.Types.INPUT_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_ELEC_DISTRIB,
            self.PARAM_ELEC_DISTRIB,
            Requisite.Types.INPUT_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_HEAT_DISTRIB,
            self.PARAM_HEAT_DISTRIB,
            Requisite.Types.INPUT_ADV_PARAM,
        )
        # paramètres avancés annuels
        self.add_mandatory_requisite(
            self.YEAR_PARAM_PERF_SYSTEMS,
            self.YEAR_PARAM_PERF_SYSTEMS,
            Requisite.Types.INPUT_YEAR_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_BIOMASS,
            self.INPUT_BIOMASS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_HEATPUMP,
            self.INPUT_HEATPUMP,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_DISTNET,
            self.INPUT_DISTNET,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_ELECTR,
            self.INPUT_ELECTR,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_GAS,
            self.INPUT_GAS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_FIOUL_LPG,
            self.INPUT_FIOUL_LPG,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "emissions_ges_tertiaire",
            DataSet.EMISSIONS,
            Sector.SERVICES,
        )
        self.add_data_requisite(
            "conso_tertiaire",
            DataSet.CONSUMPTION,
            Sector.SERVICES,
            # TODO: use these?
            # Usage.HEATING,
            # Usage.HOT_WATER,
            # Usage.COOKING,
            # Usage.SPECIFIC_ELEC,
            # Usage.COOLING,
        )
        self.add_data_requisite("conso_chauffage_tertiaire", *self.CONSUMPTION_KEYS)

    async def _pre_execute(self, *args, **kwargs):
        conso_usage_tertiaire_chauffage = self.data["conso_chauffage_tertiaire"]
        COP = self.input_params["Coefficient de performance"]["COP"]

        repartition_conso_gaz = self.input_params[self.PARAM_GAS_DISTRIB]
        repartition_conso_pp = self.input_params[self.PARAM_OIL_DISTRIB]
        repartition_conso_EnRt = self.input_params[self.PARAM_HEAT_DISTRIB]
        repartition_conso_elec = self.input_params[self.PARAM_ELEC_DISTRIB]

        inputs_keys = {
            self.INPUT_BIOMASS,
            self.INPUT_HEATPUMP,
            self.INPUT_DISTNET,
            self.INPUT_ELECTR,
            self.INPUT_GAS,
            self.INPUT_FIOUL_LPG,
        }
        inputs = {k: self.inputs[k] for k in inputs_keys}

        self.heating_equipments_distrib = await heating_systems_distribution(
            conso_usage_tertiaire_chauffage,
            COP,
            repartition_conso_EnRt,
            repartition_conso_elec,
            repartition_conso_pp,
            repartition_conso_gaz,
            self.input_distrib_keys,
        )
        if len(inputs) == 0:
            return

        interpolating_results = interpolators.heating_system_distrib_interpolator(
            inputs,
            self.heating_equipments_distrib["coeffs_ref"],
            self.max_years.get(
                "conso_chauffage_tertiaire",
                # if we don't have a last year from data, we use first year configured
                # for current action - 1.
                int(self.settings.get("first_year", 2021)) - 1,
            ),
        )
        for k, interpolated in interpolating_results.items():
            self.inputs[k] = interpolated

    def get_input_by_cat(self, cat: str, year: str | int) -> float:
        return max(
            min(
                float(
                    self.inputs[cat].get(
                        str(year), self.heating_equipments_distrib["coeffs_ref"][cat]
                    )
                )
                / 100,
                1.0,
            ),
            0.0,
        )

    async def _execute(self, year, *args, **kwargs):
        """Retourne les répartitions d'énergies, les émission ges et la baisse dela facteur énergétique

        Parameters
        ----------
        csv_params : list
            liste des paramétres des actions
        year : int
            l'année qui correspond au pourcentage entré par l'utilisateur
        user_params : int
            le pourcentage de réduction entré par l'utilisateur
        conso_tertiaire : dataframe
            la consommation par type d'énergie dans le secteur tertaire
        conso_usage_tertiaire_chauffage: dataframe
            la consommation par type d'énergie dans le secteur tertaire usage chauffage
        emission_ges: dataframe
            les émissions de ges dans le secteur tertaire
        taux_equipement_chauffage_tertiaire :
            la répartitions des équipements de chauffage dans le secteur tertaire

        Returns
        --------
        dict :
            Avec l'ensemble des simulations des actions (répartitions des énergies, émission ges, baisse de facteur énergétique, recette fiscale)
        """
        # parametre de configuration
        enable_air_pollutants_impacts = self.config.get(
            "enable_air_pollutants_impacts", False
        )

        emission_ges = self.data["emissions_ges_tertiaire"]
        conso_tertiaire = self.data["conso_tertiaire"]
        conso_tertiaire = (
            conso_tertiaire.groupby("COMMODITY")[["valeur"]].sum().reset_index()
        )
        emission_ges = emission_ges.groupby("COMMODITY")[["valeur"]].sum().reset_index()
        conso_usage_tertiaire_chauffage = self.data["conso_chauffage_tertiaire"]

        # Récuperer les entrées utilisateurs (%)
        user_chauffage_elec_o = self.get_input_by_cat(self.INPUT_ELECTR, year)
        user_chaudiere_gaz_o = self.get_input_by_cat(self.INPUT_GAS, year)
        user_chaudiere_fioul_gpl_o = self.get_input_by_cat(self.INPUT_FIOUL_LPG, year)
        user_chauffage_biomasse_o = self.get_input_by_cat(self.INPUT_BIOMASS, year)
        user_chauffage_rdc_o = self.get_input_by_cat(self.INPUT_DISTNET, year)
        user_chauffage_pac_o = self.get_input_by_cat(self.INPUT_HEATPUMP, year)

        # taux de remplacement du parc initial de chauffage biomasse
        pn = (
            float(
                self.input_params[self.YEAR_PARAM_PERF_SYSTEMS]["1"].get(
                    str(year), 0.0
                )  # TODO: To fix to be able to use more than one parameter here
            )
            / 100
        )

        # Coefficient de performance pompe à chaleur : COP moyen
        COP = self.input_params["Coefficient de performance"]["COP"]

        # Récupérer les répartitions de la conso des énergies dans le chauffage résidentiel
        repartition_conso_gaz = self.input_params[self.PARAM_GAS_DISTRIB]
        repartition_conso_pp = self.input_params[self.PARAM_OIL_DISTRIB]
        repartition_conso_EnRt = self.input_params[self.PARAM_HEAT_DISTRIB]

        reference_year = int(
            self.max_years.get(
                "conso_tertiaire",
                # if we don't have a last year from data, we use first year configured
                # for current action - 1.
                int(self.settings.get("first_year", 2021)) - 1,
            )
        )
        if int(year) == reference_year:
            return {
                Impacts.EnergyConsumption(0.0, year, sector=Sector.SERVICES),
                Impacts.AvoidedEmissions(0.0, year, sector=Sector.SERVICES),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        # si toutes les entrées utilisateurs sont nulle, pas nécessaire de calculer
        if (
            user_chauffage_elec_o == 0
            and user_chaudiere_gaz_o == 0
            and user_chaudiere_fioul_gpl_o == 0
            and user_chauffage_biomasse_o == 0
            and user_chauffage_rdc_o == 0
            and user_chauffage_pac_o == 0
            and pn == 0
        ):
            return {
                Impacts.EnergyConsumption(0.0, year, sector=Sector.SERVICES),
                Impacts.AvoidedEmissions(0.0, year, sector=Sector.SERVICES),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        # facteurs d'émission polluants / ges / conso ATMO
        fe_atmo = self.params[self.PARAM_FEGES_ATMO]

        # ne prendre pas en compte l'energie "chaleur et froid urbain" temporairement - données manquantes pour GES côté AURA
        conso_usage_tertiaire_chauffage = conso_usage_tertiaire_chauffage.loc[
            conso_usage_tertiaire_chauffage.COMMODITY.isin(
                (
                    Commodity.ELECTRICITY,
                    Commodity.RENEWABLE_HEAT,
                    Commodity.OIL,
                    Commodity.GAS,
                )
            )
        ]
        conso_tertiaire = conso_tertiaire.loc[
            conso_tertiaire.COMMODITY.isin(
                (
                    Commodity.ELECTRICITY,
                    Commodity.RENEWABLE_HEAT,
                    Commodity.OIL,
                    Commodity.GAS,
                )
            )
        ]
        emission_ges = emission_ges.loc[
            emission_ges.COMMODITY.isin(
                (
                    Commodity.ELECTRICITY,
                    Commodity.RENEWABLE_HEAT,
                    Commodity.OIL,
                    Commodity.GAS,
                )
            )
        ]

        # Ajouter les intitulés des energies manquants
        # TODO : ajouter intitulé d'énergie (9 : "chauffage et froid urbain") manquant pour certains territoires en AURA
        if Commodity.GAS not in conso_usage_tertiaire_chauffage.COMMODITY.unique():
            conso_usage_tertiaire_chauffage.loc[
                len(conso_usage_tertiaire_chauffage), ["COMMODITY", "nom", "valeur"]
            ] = [
                Commodity.GAS,
                "Gaz",
                0,
            ]

        if Commodity.OIL not in conso_usage_tertiaire_chauffage.COMMODITY.unique():
            conso_usage_tertiaire_chauffage.loc[
                len(conso_usage_tertiaire_chauffage), ["COMMODITY", "nom", "valeur"]
            ] = [
                Commodity.OIL,
                "Produits pétroliers",
                0,
            ]

        # Récupérer les taux des équipements de l'année de référence
        repartition_rdc_reference = round(
            self.heating_equipments_distrib["coeffs_ref"][self.INPUT_DISTNET] / 100, 8
        )
        repartition_biomasse_reference = round(
            self.heating_equipments_distrib["coeffs_ref"][self.INPUT_BIOMASS] / 100, 8
        )

        # totale consommation + chaleur Enr PAC
        conso_totale_chaleur_enr_pac = (
            conso_usage_tertiaire_chauffage["valeur"].sum()
            + self.heating_equipments_distrib["chaleur_enr_pac"]
        )

        # Récupérer la consommation de gaz => id_energie : 7
        conso_gaz = conso_usage_tertiaire_chauffage.loc[
            conso_usage_tertiaire_chauffage.COMMODITY == Commodity.GAS, "valeur"
        ]
        if len(conso_gaz) > 0:
            conso_gaz = conso_gaz.iloc[0]
        else:
            conso_gaz = 0.0
        # Récupérer la consommation des produits pétroliers => id_energie : 5
        conso_pp = conso_usage_tertiaire_chauffage.loc[
            conso_usage_tertiaire_chauffage.COMMODITY == Commodity.OIL, "valeur"
        ]
        if len(conso_pp) > 0:
            conso_pp = conso_pp.iloc[0]
        else:
            conso_pp = 0.0
        # Récupérer la consommation de chaleur renouvelable EnRt => id_energie : 3
        conso_enrt = conso_usage_tertiaire_chauffage.loc[
            conso_usage_tertiaire_chauffage.COMMODITY == Commodity.RENEWABLE_HEAT,
            "valeur",
        ]
        if len(conso_enrt) > 0:
            conso_enrt = conso_enrt.iloc[0]
        else:
            conso_enrt = 0.0

        # calculer l'impact ener pou une année données pour calculer la gain ( = la différence entre l'année n et n - 1)
        def year_impact(_year):
            # récupérer les paramétres d'entrée de l'année passé en paramètre
            user_param_rdc = self.get_input_by_cat(self.INPUT_DISTNET, _year)
            user_param_biomasse = self.get_input_by_cat(self.INPUT_BIOMASS, _year)
            user_param_elec = self.get_input_by_cat(self.INPUT_ELECTR, _year)
            user_param_gaz = self.get_input_by_cat(self.INPUT_GAS, _year)
            user_param_pac = self.get_input_by_cat(self.INPUT_HEATPUMP, _year)
            user_param_fioul_gpl = self.get_input_by_cat(self.INPUT_FIOUL_LPG, _year)

            # Calculs intermidiares
            # Calculer le taux des équipements chauffage pour chaque année
            taux_equipement_elec = user_param_elec * conso_totale_chaleur_enr_pac
            taux_equipement_gaz = user_param_gaz * conso_totale_chaleur_enr_pac
            taux_equipement_fioul_gpl = (
                user_param_fioul_gpl * conso_totale_chaleur_enr_pac
            )
            taux_equipement_biomasse = (
                user_param_biomasse * conso_totale_chaleur_enr_pac
            )
            taux_chauffage_rdc = user_param_rdc * conso_totale_chaleur_enr_pac
            taux_chauffage_pac = user_param_pac * conso_totale_chaleur_enr_pac

            # Calculer les impacts énergetiques par vecteur énergétique (secteur tertiaire => usage chauffage)
            equi_chauffage_gaz_rdc = (
                conso_gaz * repartition_conso_gaz["Réseau de chaleur"] / 100
            )
            equip_pp_rdc = conso_pp * repartition_conso_pp["Réseau de chaleur"] / 100
            if user_param_rdc < repartition_rdc_reference:
                equi_chauffage_gaz_rdc *= user_param_rdc / repartition_rdc_reference
                equip_pp_rdc *= user_param_rdc / repartition_rdc_reference

            equip_enrt_rdc = taux_chauffage_rdc - equi_chauffage_gaz_rdc - equip_pp_rdc
            if user_param_rdc < repartition_rdc_reference:
                equip_enrt_rdc = (
                    (user_param_rdc / repartition_rdc_reference)
                    * conso_enrt
                    * repartition_conso_EnRt["Réseau de chaleur"]
                    / 100
                )

            equip_enrt_pac = taux_chauffage_pac - (taux_chauffage_pac / COP)
            # Resultat : impacts energitiques
            impact_electricite = taux_equipement_elec + (taux_chauffage_pac / COP)
            impact_gaz_naturel = taux_equipement_gaz + equi_chauffage_gaz_rdc
            impact_pp = taux_equipement_fioul_gpl + equip_pp_rdc
            impact_EnRT = taux_equipement_biomasse + equip_enrt_rdc + equip_enrt_pac
            # Retrancher la chaleur EnR initiale des PAC
            impact_EnRT -= self.heating_equipments_distrib["chaleur_enr_pac"]

            year_impact = {
                "impact_electricite": impact_electricite,
                "impact_EnRT": impact_EnRT,
                "impact_pp": impact_pp,
                "impact_gaz_naturel": impact_gaz_naturel,
            }

            return year_impact

        def gain_ener(currentYearImpact, previousYearImpact):
            gain_elec = float(currentYearImpact["impact_electricite"]) - float(
                previousYearImpact["impact_electricite"]
            )
            gain_enrt = float(currentYearImpact["impact_EnRT"]) - float(
                previousYearImpact["impact_EnRT"]
            )
            gain_pp = float(currentYearImpact["impact_pp"]) - float(
                previousYearImpact["impact_pp"]
            )
            gain_gaz = float(currentYearImpact["impact_gaz_naturel"]) - float(
                previousYearImpact["impact_gaz_naturel"]
            )

            data = {
                "COMMODITY": [
                    Commodity.ELECTRICITY,
                    Commodity.RENEWABLE_HEAT,
                    Commodity.OIL,
                    Commodity.GAS,
                ],
                "valeur": [-gain_elec, -gain_enrt, -gain_pp, -gain_gaz],
            }
            df = pd.DataFrame(data)
            db_inter = df.merge(
                conso_usage_tertiaire_chauffage,
                left_on="COMMODITY",
                right_on="COMMODITY",
            )

            return db_inter

        # calculer le gain énérgetique
        # calculer l'impact pour année n et n - 1
        currentYearImpact = year_impact(year)
        previousYearImpact = year_impact(str(int(year) - 1))
        # calculer le gain pour une année n
        db_gain = gain_ener(currentYearImpact, previousYearImpact)

        # Create DataFrame
        gain_df = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        gain_df["valeur"] = db_gain["valeur_x"]
        gain_df.COMMODITY = db_gain.COMMODITY

        # Formater les données d'impacts énergetiques
        gain_dict = gain_df.set_index("COMMODITY")["valeur"].to_dict()
        gain = pd.Series(gain_dict)

        # Calucler les émission GES
        def calculer_gain_enrt_ges(feges, _year):
            # facteur d'émission atmo - chauffage bois
            fe_bois_recent = (
                fe_atmo.loc[
                    # we use residential value as we don't have any more precise data
                    (fe_atmo.SECTOR == Sector.RESIDENTIAL)
                    & (fe_atmo.categorie == "GES")
                    & (fe_atmo.type_veh_cha == "Foyer fermé/Poële/Cuisinière")
                ].iloc[0]["valeur"]
                / 1000
            )  # g/Kwh

            # facteur d'émission calculé - vecteur ENR thermiques
            fe_enrt = feges.loc[(feges.COMMODITY == Commodity.RENEWABLE_HEAT)].iloc[0][
                "facteur_emission_ges"
            ]
            gain_enrt = 0
            user_param_rdc = self.get_input_by_cat(self.INPUT_DISTNET, _year)
            user_param_biomasse = self.get_input_by_cat(self.INPUT_BIOMASS, _year)

            # taux de remplacement du parc initial de chauffage biomasse
            pn = (
                float(
                    self.input_params[self.YEAR_PARAM_PERF_SYSTEMS]["1"].get(
                        str(_year), 0.0
                    )  # TODO: To fix to be able to use more than one parameter here
                )
                / 100
            )

            if user_param_rdc > repartition_rdc_reference:
                if user_param_biomasse > repartition_biomasse_reference:
                    gain_enrt = (
                        (
                            (user_param_rdc - repartition_rdc_reference)
                            * conso_totale_chaleur_enr_pac
                            * fe_enrt
                        )
                        + (
                            repartition_biomasse_reference
                            * conso_totale_chaleur_enr_pac
                            * pn
                            * (fe_bois_recent - fe_enrt)
                        )
                        + (
                            (user_param_biomasse - repartition_biomasse_reference)
                            * conso_totale_chaleur_enr_pac
                            * fe_bois_recent
                        )
                    )
                elif user_param_biomasse <= repartition_biomasse_reference:
                    gain_enrt = (
                        (
                            (user_param_rdc - repartition_rdc_reference)
                            * conso_totale_chaleur_enr_pac
                            * fe_enrt
                        )
                        + (
                            user_param_biomasse
                            * conso_totale_chaleur_enr_pac
                            * pn
                            * (fe_bois_recent - fe_enrt)
                        )
                        + (
                            (user_param_biomasse - repartition_biomasse_reference)
                            * conso_totale_chaleur_enr_pac
                            * fe_enrt
                        )
                    )
            elif user_param_rdc <= repartition_rdc_reference:
                terme_1 = (
                    (user_param_rdc - repartition_rdc_reference)
                    / repartition_rdc_reference
                    * conso_enrt
                    * repartition_conso_EnRt["Réseau de chaleur"]
                    / 100
                    * fe_enrt
                )
                terme_2 = conso_totale_chaleur_enr_pac * pn * (fe_bois_recent - fe_enrt)
                terme_3 = (
                    user_param_biomasse - repartition_biomasse_reference
                ) * conso_totale_chaleur_enr_pac
                if user_param_biomasse > repartition_biomasse_reference:
                    gain_enrt = (
                        terme_1
                        + (terme_2 * repartition_biomasse_reference)
                        + (terme_3 * fe_bois_recent)
                    )
                elif user_param_biomasse <= repartition_biomasse_reference:
                    gain_enrt = (
                        terme_1 + (terme_2 * user_param_biomasse) + (terme_3 * fe_enrt)
                    )

            return gain_enrt

        # 1 : calculer les facteurs d'émission de GES
        # divisier l'émission de GES par la consommation de chaque type d'energie
        merge_df = conso_tertiaire.merge(emission_ges, on="COMMODITY")
        facteur_emission_ges_df = pd.DataFrame(
            columns=["COMMODITY", "facteur_emission_ges"], dtype=object
        )
        facteur_emission_ges_df.COMMODITY = merge_df.COMMODITY
        facteur_emission_ges_df["facteur_emission_ges"] = merge_df["valeur_y"].div(
            merge_df["valeur_x"]
        )

        # 2 : Calculer les émission GES évitées
        merge_fac_gain = facteur_emission_ges_df.merge(
            gain_df, left_on="COMMODITY", right_on="COMMODITY"
        )

        emiss_evitees_df = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        emiss_evitees_df.COMMODITY = merge_fac_gain.COMMODITY
        emiss_evitees_df["valeur"] = merge_fac_gain["facteur_emission_ges"].multiply(
            merge_fac_gain["valeur"]
        )
        gain_ges_enrt = -calculer_gain_enrt_ges(facteur_emission_ges_df, year)
        if (int(year) - 1) > reference_year:
            gain_ges_enrt = float(
                calculer_gain_enrt_ges(facteur_emission_ges_df, str(int(year) - 1))
            ) - float(calculer_gain_enrt_ges(facteur_emission_ges_df, year))
        emiss_evitees_df.loc[
            emiss_evitees_df.COMMODITY == Commodity.RENEWABLE_HEAT, "id_energie"
        ] = gain_ges_enrt

        # 3 : formater les données des émissions GES évitées
        emiss_dict = emiss_evitees_df.set_index("COMMODITY")["valeur"].to_dict()
        emissions_evitees = pd.Series(emiss_dict)

        # récupération des prix des énergies
        # scénario d'évolution du prix de l'énergie, par énergie et secteur
        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        # 4 : secteur Indifférencié : 4 et résidentiel : 1
        prix_energie = prix.loc[
            # TODO: why residential here?
            (prix.SECTOR.isin([Sector.RESIDENTIAL, Sector.UNDIFFERENCIATED]))
            & (
                ~prix.PARAMCOMMODITY.isin(
                    (
                        ParamCommodity.GASOLINE,
                        ParamCommodity.DIESEL,
                        ParamCommodity.NON_ROAD_DIESEL,
                    )
                )
            )
        ]

        # calculer la baisse de facteur énergétique
        prix_fact_gain_ener = prix_energie.merge(gain_df, on="COMMODITY")
        # 1 : multiplier les prix des énergies par les gains énergétiques
        prix_fact_gain_ener["baisse_fact_ener"] = prix_fact_gain_ener[
            "valeur_x"
        ].multiply(prix_fact_gain_ener["valeur_y"])
        # unité en K€
        baisse_fact_ener = (prix_fact_gain_ener["baisse_fact_ener"].sum()) * 10

        # récupérer les répartitions des équipements pour l'année n-1
        params_annee_precedente_rdc = self.get_input_by_cat(
            self.INPUT_DISTNET, str(int(year) - 1)
        )
        params_annee_precedente_pac = self.get_input_by_cat(
            self.INPUT_HEATPUMP, str(int(year) - 1)
        )
        params_annee_precedente_elec = self.get_input_by_cat(
            self.INPUT_ELECTR, str(int(year) - 1)
        )
        params_annee_precedente_biomasse = self.get_input_by_cat(
            self.INPUT_BIOMASS, str(int(year) - 1)
        )
        params_annee_precedente_gaz = self.get_input_by_cat(
            self.INPUT_GAS, str(int(year) - 1)
        )
        params_annee_precedente_fioul_gpl = self.get_input_by_cat(
            self.INPUT_FIOUL_LPG, str(int(year) - 1)
        )

        # calculer la différence de consommation des équipements de chauffage (nécessaire pour calculer l'investissement)
        diff_chauffage_rdc = calculer_diff_conso(
            user_chauffage_rdc_o,
            params_annee_precedente_rdc,
            conso_totale_chaleur_enr_pac,
        )
        diff_chauffage_elec = calculer_diff_conso(
            user_chauffage_elec_o,
            params_annee_precedente_elec,
            conso_totale_chaleur_enr_pac,
        )
        diff_chaudiere_gaz = calculer_diff_conso(
            user_chaudiere_gaz_o,
            params_annee_precedente_gaz,
            conso_totale_chaleur_enr_pac,
        )
        diff_chaudiere_fioul_gpl = calculer_diff_conso(
            user_chaudiere_fioul_gpl_o,
            params_annee_precedente_fioul_gpl,
            conso_totale_chaleur_enr_pac,
        )
        diff_chauffage_biomasse = calculer_diff_conso(
            user_chauffage_biomasse_o,
            params_annee_precedente_biomasse,
            conso_totale_chaleur_enr_pac,
        )
        diff_chauffage_pac = calculer_diff_conso(
            user_chauffage_pac_o,
            params_annee_precedente_pac,
            conso_totale_chaleur_enr_pac,
        )

        # Emissions de polluants atmosphériques
        if enable_air_pollutants_impacts:
            ## Les facteurs d'émissions de polluants atmosphériques par polluant
            def facteur_emission_polluant(
                type, type_ener, performance=False, onlyForNox=False
            ):
                facteur_emission = {}
                for pol in ["NOx", "PM10", "PM2.5", "COVNM"]:
                    if performance and (
                        (onlyForNox and pol == "NOx") or (not onlyForNox)
                    ):
                        facteur_emission[pol] = fe_atmo.loc[
                            (fe_atmo.SECTOR == Sector.RESIDENTIAL)
                            & (fe_atmo.categorie == pol)
                            & (fe_atmo.type_veh_cha == "{}".format(type))
                            & (fe_atmo.type_energie == "{}".format(type_ener))
                            & (fe_atmo.performance == "{}".format(performance))
                        ].iloc[0]["valeur"]

                    elif (performance and onlyForNox and pol != "NOx") or (
                        not performance
                    ):
                        facteur_emission[pol] = fe_atmo.loc[
                            (fe_atmo.SECTOR == Sector.RESIDENTIAL)
                            & (fe_atmo.categorie == pol)
                            & (fe_atmo.type_energie == "{}".format(type_ener))
                            & (fe_atmo.type_veh_cha == "{}".format(type))
                        ].iloc[0]["valeur"]

                return facteur_emission

            ## calculer les émissions évitées vecteur énérgitique EnRT pour les polluants atmosphériques
            def calculer_gain_enert_polluant(
                year, pol, fe_rdc, fe_bois_recent, fe_bois_actuel
            ):
                emissions_polluants_enrt = 0
                user_param_rdc = self.get_input_by_cat(self.INPUT_DISTNET, str(year))
                user_param_biomasse = self.get_input_by_cat(
                    self.INPUT_BIOMASS, str(year)
                )

                # taux de remplacement du parc initial de chauffage biomasse
                pn = (
                    float(
                        self.input_params[self.YEAR_PARAM_PERF_SYSTEMS]["1"].get(
                            str(year), 0.0
                        )  # TODO: To fix to be able to use more than one parameter here
                    )
                    / 100
                )

                if user_param_rdc > repartition_rdc_reference:
                    if user_param_biomasse > repartition_biomasse_reference:
                        emissions_polluants_enrt = (
                            (
                                (user_param_rdc - repartition_rdc_reference)
                                * conso_totale_chaleur_enr_pac
                                * fe_rdc[pol]
                            )
                            + (
                                repartition_biomasse_reference
                                * conso_totale_chaleur_enr_pac
                                * pn
                                * (fe_bois_recent[pol] - fe_bois_actuel[pol])
                            )
                            + (
                                (user_param_biomasse - repartition_biomasse_reference)
                                * conso_totale_chaleur_enr_pac
                                * fe_bois_recent[pol]
                            )
                        )
                    elif user_param_biomasse <= repartition_biomasse_reference:
                        emissions_polluants_enrt = (
                            (
                                (user_param_rdc - repartition_rdc_reference)
                                * conso_totale_chaleur_enr_pac
                                * fe_rdc[pol]
                            )
                            + (
                                user_param_biomasse
                                * conso_totale_chaleur_enr_pac
                                * pn
                                * (fe_bois_recent[pol] - fe_bois_actuel[pol])
                            )
                            + (
                                (user_param_biomasse - repartition_biomasse_reference)
                                * conso_totale_chaleur_enr_pac
                                * fe_bois_actuel[pol]
                            )
                        )
                elif user_param_rdc <= repartition_rdc_reference:
                    terme_1 = (
                        (user_param_rdc - repartition_rdc_reference)
                        / repartition_rdc_reference
                        * conso_enrt
                        * repartition_conso_EnRt["Réseau de chaleur"]
                        / 100
                        * fe_rdc[pol]
                    )
                    terme_2 = (
                        repartition_biomasse_reference
                        * conso_totale_chaleur_enr_pac
                        * pn
                        * (fe_bois_recent[pol] - fe_bois_actuel[pol])
                    )
                    terme_3 = (
                        user_param_biomasse - repartition_biomasse_reference
                    ) * conso_totale_chaleur_enr_pac
                    if user_param_biomasse > repartition_biomasse_reference:
                        emissions_polluants_enrt = (
                            terme_1 + terme_2 + (terme_3 * fe_bois_recent[pol])
                        )
                    elif user_param_biomasse <= repartition_biomasse_reference:
                        emissions_polluants_enrt = (
                            terme_1 + terme_2 + (terme_3 * fe_bois_actuel[pol])
                        )

                return emissions_polluants_enrt

            ## Caulculer les émissions de polluants atmosphériques par polluant
            def calculer_emission_polluants(
                pol, fe_rdc, fe_gaz, fe_fioul, fe_bois_recent, fe_bois_actuel
            ):
                # gain produits pétroliers
                gain_polluants_pp = gain[Commodity.OIL] * fe_fioul[pol]
                # calculer gain gaz
                gain_polluants_gaz = gain[Commodity.GAS] * fe_gaz[pol]
                # calculer gain EnRt
                gain_polluants_enrt = float(
                    calculer_gain_enert_polluant(
                        int(year) - 1, pol, fe_rdc, fe_bois_recent, fe_bois_actuel
                    )
                ) - float(
                    calculer_gain_enert_polluant(
                        int(year), pol, fe_rdc, fe_bois_recent, fe_bois_actuel
                    )
                )

                # formatter les données par vecteur énergetique
                gain_polluant = pd.DataFrame()
                gain_polluant["COMMODITY"] = [
                    Commodity.OIL,
                    Commodity.RENEWABLE_HEAT,
                    Commodity.GAS,
                ]
                gain_polluant["valeur"] = [
                    gain_polluants_pp,
                    gain_polluants_enrt,
                    gain_polluants_gaz,
                ]
                gain_polluant.set_index("COMMODITY", inplace=True)

                return gain_polluant["valeur"]

            ## Les facteurs d'émissions de polluants atmosphériques
            fe_rdc = facteur_emission_polluant("Réseau de chaleur", "Toute énergie")
            fe_gaz = facteur_emission_polluant("Chaudière", "Gaz", "Ancien", True)
            fe_fioul = facteur_emission_polluant("Chaudière", "Fioul", "Ancien", True)
            fe_bois_recent = facteur_emission_polluant(
                "Chaudière", "Biomasse", "Récent"
            )
            fe_bois_actuel = facteur_emission_polluant(
                "Chaudière", "Biomasse", "Actuel"
            )

            ## Les émissions évitées de polluants atmosphériques
            gain_nox = calculer_emission_polluants(
                "NOx", fe_rdc, fe_gaz, fe_fioul, fe_bois_recent, fe_bois_actuel
            )
            gain_covnm = calculer_emission_polluants(
                "COVNM", fe_rdc, fe_gaz, fe_fioul, fe_bois_recent, fe_bois_actuel
            )
            gain_pm10 = calculer_emission_polluants(
                "PM10", fe_rdc, fe_gaz, fe_fioul, fe_bois_recent, fe_bois_actuel
            )
            gain_pm25 = calculer_emission_polluants(
                "PM2.5", fe_rdc, fe_gaz, fe_fioul, fe_bois_recent, fe_bois_actuel
            )

            return {
                Impacts.EnergyConsumption(gain, year, sector=Sector.SERVICES),
                Impacts.AvoidedEmissions(
                    emissions_evitees, year, sector=Sector.SERVICES
                ),
                Impacts.PollutantsCOVNM(gain_covnm, year, sector=Sector.SERVICES),
                Impacts.PollutantsNH3(0.0, year, sector=Sector.SERVICES),
                Impacts.PollutantsSOX(0.0, year, sector=Sector.SERVICES),
                Impacts.PollutantsPM10(gain_pm10, year, sector=Sector.SERVICES),
                Impacts.PollutantsPM25(gain_pm25, year, sector=Sector.SERVICES),
                Impacts.PollutantsNOX(gain_nox, year, sector=Sector.SERVICES),
                Impacts.EnergyBill(baisse_fact_ener, year),
                Impacts.TaxRevenue(None, year),
                Impacts.ConsumptionDiff(
                    "chauffage_elec",
                    diff_chauffage_elec,
                    year,
                    action=self.name,
                ),
                Impacts.ConsumptionDiff(
                    "chaudiere_gaz",
                    diff_chaudiere_gaz,
                    year,
                    action=self.name,
                ),
                Impacts.ConsumptionDiff(
                    "chaudiere_fioul_gpl",
                    diff_chaudiere_fioul_gpl,
                    year,
                    action=self.name,
                ),
                Impacts.ConsumptionDiff(
                    "chauffage_biomasse",
                    diff_chauffage_biomasse,
                    year,
                    action=self.name,
                ),
                Impacts.ConsumptionDiff(
                    "chauffage_rdc",
                    diff_chauffage_rdc,
                    year,
                    action=self.name,
                ),
                Impacts.ConsumptionDiff(
                    "chauffage_pac",
                    diff_chauffage_pac,
                    year,
                    action=self.name,
                ),
            }

        return {
            Impacts.EnergyConsumption(gain, year, sector=Sector.SERVICES),
            Impacts.AvoidedEmissions(emissions_evitees, year, sector=Sector.SERVICES),
            Impacts.EnergyBill(baisse_fact_ener, year),
            Impacts.TaxRevenue(None, year),
            Impacts.ConsumptionDiff(
                "chauffage_elec",
                diff_chauffage_elec,
                year,
                action=self.name,
            ),
            Impacts.ConsumptionDiff(
                "chaudiere_gaz",
                diff_chaudiere_gaz,
                year,
                action=self.name,
            ),
            Impacts.ConsumptionDiff(
                "chaudiere_fioul_gpl",
                diff_chaudiere_fioul_gpl,
                year,
                action=self.name,
            ),
            Impacts.ConsumptionDiff(
                "chauffage_biomasse",
                diff_chauffage_biomasse,
                year,
                action=self.name,
            ),
            Impacts.ConsumptionDiff(
                "chauffage_rdc",
                diff_chauffage_rdc,
                year,
                action=self.name,
            ),
            Impacts.ConsumptionDiff(
                "chauffage_pac",
                diff_chauffage_pac,
                year,
                action=self.name,
            ),
        }
