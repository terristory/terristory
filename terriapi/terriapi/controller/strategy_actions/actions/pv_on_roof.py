# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.analyse import get_global_irradiation
from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
)


class PVOnRoof(AbstractAction):
    PARAM_LOSSES = "Pertes systeme"
    PARAM_LAND_PRICE = "Prix de la surface de terrain"
    PARAM_TAX_RATE_CFE = "Taux d'imposition CFE"

    PARAM_IFER = "ifer"
    PARAM_PRICES_CRE = "action10_tarif_cre"
    PARAM_ADVANCED_STATIC = "action10_parametres_avances"

    INPUT_SUFFIX_PEAKPOWER = "_puiss_crete"

    @property
    def INPUT_PEAKPOWER(self):
        return self.action_number + self.INPUT_SUFFIX_PEAKPOWER

    @property
    def SUB_PARAM_PERFORMANCE(self):
        if self.plant_type == "petite_toiture":
            return "Rendement ECS"
        elif self.plant_type == "grande_toiture":
            return "Rendement combiné"
        elif self.plant_type == "ombriere":
            return "Rendement combiné"
        else:
            raise NotImplementedError(
                "Solar thermal action can only be petite_toiture, grande_toiture or ombriere type ("
                + self.plant_type
                + " given)"
            )

    def __init__(self, stype):
        self.plant_type = stype
        if stype == "petite_toiture":
            code = "10a"
            suffix = " (small roof)"
        elif stype == "grande_toiture":
            code = "10b"
            suffix = " (large roof)"
        elif stype == "ombriere":
            code = "10c"
            suffix = " (shading)"
        else:
            raise NotImplementedError(
                "PV on roof action can only be petite_toiture, grande_toiture or ombriere type ("
                + stype
                + " given)"
            )
        super().__init__("Roof based phovoltaic" + suffix, code)

    def add_requisites(self):
        # global irradiation
        self.add_mandatory_requisite(
            "Global irradiation",
            "Global irradiation",
            Requisite.Types.PARAM,
            computation_function=get_global_irradiation,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_LOSSES,
            self.PARAM_LOSSES,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_LAND_PRICE,
            self.PARAM_LAND_PRICE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_CFE,
            self.PARAM_TAX_RATE_CFE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # Paramètres statiques
        self.add_mandatory_requisite(
            "Paramètres avancés statiques",
            self.PARAM_ADVANCED_STATIC,
            Requisite.Types.PARAM,
        )
        self.add_mandatory_requisite(
            "IFER",
            self.PARAM_IFER,
            Requisite.Types.PARAM,
        )
        self.add_mandatory_requisite(
            "Tarifs de la CRÉ",
            self.PARAM_PRICES_CRE,
            Requisite.Types.PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_PEAKPOWER,
            self.INPUT_PEAKPOWER,
            Requisite.Types.INPUT,
        )

    async def _execute(self, year, *args, **kwargs):
        """Action photovoltaïque sur toiture

        https://gitlab.com/terristory/terristory/issues/451
        """
        puissance = float(self.inputs[self.INPUT_PEAKPOWER][year])
        if puissance == 0:
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.SOLAR_PV,
                    energy_vector=EnergyVector.ELECTRICITY,
                ),
                Impacts.AvoidedEmissions(0.0, year),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        prix_moy = prix[
            (prix.COMMODITY == Commodity.ELECTRICITY)
            & (prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.INDUSTRY_WITHOUT_ENERGY)))
        ][
            "valeur"
        ].mean()  # id_energie = 2 correspond à l'électricité

        feges = self.params[self.PARAM_FEGES]
        facteur_emiss_pv_va = feges[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.RENEWABLEPROD == RenewableProd.SOLAR_PV)
        ]["valeur"].iloc[0]

        adv_params = self.input_params

        # paramètres avancés non modifiables
        advanced_params = self.params[self.PARAM_ADVANCED_STATIC]

        # Paramètre de l'IFER (Imposition Forfaitaire des Entreprises de Réseaux)
        ifer = self.params[self.PARAM_IFER]
        taux_IFER = ifer[ifer.category == "pv"]["value"].iloc[0]
        # Paramètre du montant de rachat dans les appels à projet passé par la cre (commission de la régulation de l'énergie)
        cre = self.params[self.PARAM_PRICES_CRE]
        tarif_ombrieres = cre[cre.annee == int(year)]["valeur_ombriere"].iloc[0]
        tarif_toitures = cre[cre.annee == int(year)]["valeur_toiture"].iloc[0]

        # Ci-dessous les paramètres qui sont modifiables par l'utilisateur
        pertes = adv_params[self.PARAM_LOSSES]
        prix_surf_terrain = adv_params[self.PARAM_LAND_PRICE]
        taux_imposition_CFE = adv_params[self.PARAM_TAX_RATE_CFE]

        # Ci-dessous les paramètres qui sont non modifiable par l'utilisateur
        puiss_surf_toiture = advanced_params[(advanced_params.id == 1)]["valeur"].iloc[
            0
        ]
        puiss_surf_ombriere = advanced_params[(advanced_params.id == 2)]["valeur"].iloc[
            0
        ]
        coeff_cfe = advanced_params[(advanced_params.id == 3)]["valeur"].iloc[0]
        taux_VLC = advanced_params[(advanced_params.id == 4)]["valeur"].iloc[0]

        if self.plant_type == "petite_toiture" or self.plant_type == "grande_toiture":
            tarif = tarif_toitures
            puiss_surf = puiss_surf_toiture
        elif self.plant_type == "ombriere":
            tarif = tarif_ombrieres
            puiss_surf = puiss_surf_ombriere

        # Energie produite
        energie_produite = (
            puissance * (1 - pertes / 100) * self.params["Global irradiation"]["fixe"]
        )

        # Emissions evitees : rappel que le facteur d'émission du pv comprend le FE
        # de l'electricité lors des heures de production du PV / l'ACV n'est pas
        # prise en compte
        emission_ges = energie_produite * facteur_emiss_pv_va

        # Baisse de la facture énergétique du territoire
        baisse_facture = energie_produite * prix_moy / 100

        # Calculs retombées fiscales
        # Chiffre d'affaires - keuros
        chiffre_aff = tarif * energie_produite / 1000
        # Charges - keuros
        charges = 26.2 * puissance

        # Surface du terrain
        surface_terrain = puissance / puiss_surf * 1000

        # Imposition forfaitaire sur les entreprises de réseaux (ifer) - k euros
        valeur_ifer = puissance * taux_IFER

        # CFE -k euros
        prix_revient = prix_surf_terrain * surface_terrain

        # valeur locative cadastrale - k€
        valeur_loc_cadadstr = prix_revient * taux_VLC
        # cfe (cotisation foncière des entreprises) - keuros
        cfe = coeff_cfe * taux_imposition_CFE / 100 * valeur_loc_cadadstr

        taxe_fonciere = 0
        taxe_amenag = 0

        # Part des recettes fiscales par type de territoire
        recette_fiscale = {
            "commune": 0.3 * taxe_fonciere
            + 0.5 * valeur_ifer
            + 0.5 * cfe
            + (5 / 7) * taxe_amenag,
            "epci": 0.2 * taxe_fonciere + 0.5 * cfe,
            "departement": 0.5 * taxe_fonciere
            + 0.5 * valeur_ifer
            + (2 / 7) * taxe_amenag,
            "region": 0,
        }

        # Énergie en GWh
        energie_produite /= 1000.0

        # émissoin en ktCO2
        emission_ges /= 1000.0

        return {
            Impacts.EnergyProduction(
                energie_produite,
                year,
                prod_type=RenewableProd.SOLAR_PV,
                energy_vector=EnergyVector.ELECTRICITY,
            ),
            Impacts.AvoidedEmissions(
                0.0,  # TODO: why not applying emissions reduction
                year,
                commodity=Commodity.ELECTRICITY,
                sector=Sector.RESIDENTIAL,
            ),
            Impacts.EnergyBill(baisse_facture, year),
            Impacts.TaxRevenue(recette_fiscale, year),
        }
