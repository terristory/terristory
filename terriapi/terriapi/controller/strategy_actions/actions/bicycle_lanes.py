# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd
from sanic.log import logger
from scipy import stats

from terriapi.controller.strategy_actions import (
    POLLUTANTS,
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    Requisite,
    Sector,
    Usage,
)


class BicycleLanes(AbstractAction):
    INPUT_LEN_CYC_LANES = "7_longueur_bandes_cyclables"
    INPUT_LEN_CYC_PATHS = "7_longueur_pistes_cyclables"
    INPUT_LEN_EXIST_CYC = "7_longueur_pistes_cyclables_existantes"
    INPUT_POPULATION = "7_population"
    INPUT_TERRITORY_INDEX = "7_indice_territoire"

    PARAM_CARBON_ENERGY = "action7_indicateurs_carbon_energie"
    PARAM_LAMBDAS = "action7_lambdas"
    PARAM_CAR_FLEET = "action7_parc_voiture_com_men"

    MODAL_SHARE_LIMIT = "Plafond pour la part modale"
    IMMOBILE_PERSONS_RATE = "Taux de personnes immobiles"
    AVG_NB_TRIPS_PER_CAPITA_PER_DAY = (
        "Nombre moyen de déplacements par habitant par jour"
    )
    AVG_NB_DAYS_BIKING_PER_CYCLIST = (
        "Nombre de jours de déplacements à vélo par an par cycliste"
    )
    AVG_DIST_PER_TRIP_PER_CYCLIST = (
        "Distance par trajet parcourue par un cycliste en moyenne (Km)"
    )

    def __init__(self):
        super().__init__("Bicycle lanes", "7")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions ATMO",
            self.PARAM_FEGES_ATMO,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.ATMO_GES_FACTORS,
        )
        self.add_mandatory_requisite(
            "Consommation des transports en commun",
            self.PARAM_TRANSPORT_CONSUMPTION,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.MODAL_SHARE_LIMIT,
            self.MODAL_SHARE_LIMIT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.IMMOBILE_PERSONS_RATE,
            self.IMMOBILE_PERSONS_RATE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.AVG_NB_TRIPS_PER_CAPITA_PER_DAY,
            self.AVG_NB_TRIPS_PER_CAPITA_PER_DAY,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.AVG_NB_DAYS_BIKING_PER_CYCLIST,
            self.AVG_NB_DAYS_BIKING_PER_CYCLIST,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.AVG_DIST_PER_TRIP_PER_CYCLIST,
            self.AVG_DIST_PER_TRIP_PER_CYCLIST,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            "Indicateurs d'intensité carbone/énergie par mode",
            self.PARAM_CARBON_ENERGY,
            Requisite.Types.PARAM,
        )
        self.add_mandatory_requisite(
            "Valeurs lambda d'adaptation au vélo",
            self.PARAM_LAMBDAS,
            Requisite.Types.PARAM,
        )
        self.add_mandatory_requisite(
            "Répartition du parc de véhicules entre essence et diesel chez les ménages",
            self.PARAM_CAR_FLEET,
            Requisite.Types.PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_LEN_CYC_LANES,
            self.INPUT_LEN_CYC_LANES,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_LEN_CYC_PATHS,
            self.INPUT_LEN_CYC_PATHS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_LEN_EXIST_CYC,
            self.INPUT_LEN_EXIST_CYC,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_POPULATION,
            self.INPUT_POPULATION,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_TERRITORY_INDEX,
            self.INPUT_TERRITORY_INDEX,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )
        self.add_data_requisite(
            "emissions_residentiel",
            DataSet.EMISSIONS,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

        for pollutant in POLLUTANTS:
            self.add_data_requisite(
                "emission_" + pollutant,
                DataSet.table_from_pollutant(pollutant),
                Sector.RESIDENTIAL,
                default=False,
            )

    async def _execute(self, year, *args, **kwargs):
        """
        Calcul d'impact GES, NRJ et facture NRJ
        pour l'action de création de voies cyclables
        """
        conso_usage_energie = self.data["conso_residentiel"]
        emission_ges_secteur = self.data["emissions_residentiel"]

        # facteurs d'émissions polluant / ges / conso ATMO
        fe = self.params[self.PARAM_FEGES_ATMO]

        indice = self.inputs[self.INPUT_TERRITORY_INDEX][year]
        population = int(self.inputs[self.INPUT_POPULATION][year])

        # TODO: variable never used
        conso_transport_commun = self.params[self.PARAM_TRANSPORT_CONSUMPTION].copy()
        conso_transport_commun.set_index("type_transport", inplace=True)

        enable_air_pollutants_impacts = self.config.get(
            "enable_air_pollutants_impacts", False
        )

        # population nulle, pas de calcul possible
        if population == 0:
            return {
                Impacts.EnergyConsumption(0.0, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.AvoidedEmissions(0.0, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        voies = float(self.inputs[self.INPUT_LEN_CYC_LANES][year])
        voies += float(self.inputs[self.INPUT_LEN_CYC_PATHS][year])
        voies_existantes = float(self.inputs[self.INPUT_LEN_EXIST_CYC][year])

        parc_voitures = self.params[self.PARAM_CAR_FLEET]
        parc_voitures = parc_voitures[parc_voitures.annee == int(year)]
        indicateurs = self.params[self.PARAM_CARBON_ENERGY]
        lambdas = self.params[self.PARAM_LAMBDAS]

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        lambda_faible = lambdas[lambdas.indice_adap_velo == "Faible"]["lambda"].iloc[0]
        lambda_moy = lambdas[lambdas.indice_adap_velo == "Moyen"]["lambda"].iloc[0]
        lambda_fort = lambdas[lambdas.indice_adap_velo == "Fort"]["lambda"].iloc[0]

        plafond_part_modale = self.input_params[self.MODAL_SHARE_LIMIT] / 100
        voie_exist_par_hab = voies_existantes / population
        voies_par_hab_apres_install = (voies_existantes + voies) / population
        tx_pers_immobiles = self.input_params[self.IMMOBILE_PERSONS_RATE] / 100
        nbr_moy_deplct_par_hab_par_j = self.input_params[
            self.AVG_NB_TRIPS_PER_CAPITA_PER_DAY
        ]
        nbre_jours_deplac_annuel_cycliste = self.input_params[
            self.AVG_NB_DAYS_BIKING_PER_CYCLIST
        ]
        dist_moy_cycliste = self.input_params[self.AVG_DIST_PER_TRIP_PER_CYCLIST]

        intensite_energ_pass_km_voit = indicateurs[
            (indicateurs.nom_parametre == "intensite_energ_passager_km")
            & (indicateurs.type_transp == "voitures_camionnettes")
            & (indicateurs.type_indicateur == "energie")
        ].valeur.iloc[0]

        intensite_energ_pass_km_bus = indicateurs[
            (indicateurs.nom_parametre == "intensite_energ_passager_km")
            & (indicateurs.type_transp == "Bus")
            & (indicateurs.type_indicateur == "energie")
        ].valeur.iloc[0]

        intensite_carbon_passager_km_voit = indicateurs[
            (indicateurs.type_transp == "voitures_camionnettes")
            & (indicateurs.type_indicateur == "carbon")
        ].valeur.iloc[0]

        intensite_carbon_passager_km_bus = indicateurs[
            (indicateurs.type_transp == "Bus")
            & (indicateurs.type_indicateur == "carbon")
        ].valeur.iloc[0]

        tx_voit_menag_commerc_essen = parc_voitures.tx_voit_menag_commerc_essen.iloc[0]
        tx_voit_menag_commerc_diesel = parc_voitures.tx_voit_menag_commerc_diesel.iloc[
            0
        ]
        prix_essence = prix.valeur[prix.PARAMCOMMODITY == ParamCommodity.GASOLINE].iloc[
            0
        ]
        prix_diesel = prix.valeur[prix.PARAMCOMMODITY == ParamCommodity.DIESEL].iloc[0]

        # Part modale estimee avant travaux utilisant une fonction de répartition
        if indice == "Faible":
            part_mod_avt_trav = plafond_part_modale * stats.expon.cdf(
                voie_exist_par_hab, scale=1.0 / lambda_faible
            )
        elif indice == "Moyen":
            part_mod_avt_trav = plafond_part_modale * stats.expon.cdf(
                voie_exist_par_hab, scale=1.0 / lambda_moy
            )
        elif indice == "Fort":
            part_mod_avt_trav = plafond_part_modale * stats.expon.cdf(
                voie_exist_par_hab, scale=1.0 / lambda_fort
            )
        else:
            part_mod_avt_trav = 0.0

        # part modale estimée après travaux
        if indice == "Faible":
            part_mod_apr_trav = plafond_part_modale * stats.expon.cdf(
                voies_par_hab_apres_install, scale=1.0 / lambda_faible
            )
        elif indice == "Moyen":
            part_mod_apr_trav = plafond_part_modale * stats.expon.cdf(
                voies_par_hab_apres_install, scale=1.0 / lambda_moy
            )
        elif indice == "Fort":
            part_mod_apr_trav = plafond_part_modale * stats.expon.cdf(
                voies_par_hab_apres_install, scale=1.0 / lambda_fort
            )
        else:
            part_mod_apr_trav = 0.0
        # Nombre de deplacements avant installation
        nbr_dplc_avt_instal = (
            population
            * part_mod_avt_trav
            * (1 - tx_pers_immobiles)
            * nbre_jours_deplac_annuel_cycliste
            * nbr_moy_deplct_par_hab_par_j
        )

        # Nombre de deplacements après installation
        nbr_dplc_apr_instal = (
            population
            * part_mod_apr_trav
            * (1 - tx_pers_immobiles)
            * nbre_jours_deplac_annuel_cycliste
            * nbr_moy_deplct_par_hab_par_j
        )

        # Passagers-km avant et apres installation de nouvelles voies cyclables

        # TODO: variable not used
        pass_km_avt_instal = nbr_dplc_avt_instal * dist_moy_cycliste / 100_000
        pass_km_apr_instal = (
            (nbr_dplc_apr_instal - nbr_dplc_avt_instal) * dist_moy_cycliste / 100_000
        )

        # ############## ancienne méthodo conso ges #####################
        # # Mix énergétique à calculer par transport en commune
        # conso_transport_commun.loc["Bus", "mix"] = (
        #     conso_transport_commun.loc["Bus"]["valeur"]
        #     / conso_transport_commun.loc["Bus"]["valeur"].sum()
        # )

        # conso_transport_commun = conso_transport_commun.reset_index().set_index(
        #     ["type_transport", "id_energie"]
        # )

        # # ENERGIE ECONOMISEE
        # # Energie economisee par comparaison aux voitures
        # # Type d'énergie : produits pétroliers
        # eco_ener_vs_voit = (
        #     (0.67 + 0.04)
        #     * pass_km_apr_instal
        #     * intensite_energ_pass_km_voit
        #     / 3600
        #     * 1_000_000
        # )

        # # energie economisee comparee aux transports en commun
        # intensite_energ_pass = pd.DataFrame(
        #     {
        #         "valeur": [
        #             intensite_energ_pass_km_bus,
        #             intensite_energ_pass_km_bus,
        #         ],
        #         "id_energie": [5, 7],
        #     },
        #     index=pd.Index(["Bus", "Bus"], name="type_transport"),
        # )
        # intensite_energ_pass = intensite_energ_pass.reset_index().set_index(
        #     ["type_transport", "id_energie"]
        # )

        # eco_ener_vs_tc = (
        #     0.29
        #     * pass_km_apr_instal
        #     * intensite_energ_pass["valeur"]
        #     * conso_transport_commun["mix"]
        #     / 3600
        #     * 1_000_000
        # )
        # eco_ener_vs_tc.name = "valeur"
        # eco_ener_vs_tc = eco_ener_vs_tc.reset_index()
        # # Energie totale economisee
        # nom_energies = conso_usage_energie[["energie", "nom"]].drop_duplicates()
        # produits_petroliers = nom_energies.loc[nom_energies["energie"] == 5, "nom"].iloc[0]

        # gaz = ""
        # if pd.DataFrame(nom_energies.loc[nom_energies["energie"] == 7, "nom"]).to_dict(
        #     "records"
        # ):
        #     gaz = nom_energies.loc[nom_energies["energie"] == 7, "nom"].iloc[0]
        # elec = nom_energies.loc[nom_energies["energie"] == 2, "nom"].iloc[0]
        # eco_produits_petroliers = (
        #     eco_ener_vs_voit + eco_ener_vs_tc.query("id_energie == 5")["valeur"].sum()
        # )
        # eco_ener_tot = pd.Series(
        #     {
        #         produits_petroliers: eco_produits_petroliers.iloc[0],  # Produits pétroliers
        #         gaz: eco_ener_vs_tc.query("id_energie == 7")["valeur"].sum(),  # Gaz
        #         elec: eco_ener_vs_tc.query("id_energie == 2")["valeur"].sum(),
        #     }
        # )  # Électricité
        # # EMISSIONS EVITEES
        # # Emissions evitees par rapport aux voitures
        # # Type d'énergie : produits pétroliers
        # emiss_evit_vs_voit = (
        #     (0.67 + 0.04)
        #     * pass_km_apr_instal
        #     * intensite_carbon_passager_km_voit
        #     / 1000
        #     * 1_000_000
        # )
        # # Emissions evitees par rapport aux Transports en commun
        # emiss_evit_vs_tc = (
        #     pass_km_apr_instal * 0.29 * intensite_carbon_passager_km_bus / 1000 * 1000_000
        # )

        # intensite_carbon_passager = pd.DataFrame(
        #     {
        #         "valeur": [
        #             intensite_carbon_passager_km_bus,
        #             intensite_carbon_passager_km_bus,
        #         ],
        #         "id_energie": [5, 7],
        #     },
        #     index=pd.Index(["Bus", "Bus"], name="type_transport"),
        # )
        # intensite_carbon_passager = intensite_carbon_passager.reset_index().set_index(
        #     ["type_transport", "id_energie"]
        # )
        # emission_evit_transport_commun = (
        #     pass_km_apr_instal
        #     * 0.29
        #     * intensite_carbon_passager["valeur"]
        #     * conso_transport_commun["mix"]
        #     / 1000
        #     * 1_000_000
        # )

        # emission_evit_transport_commun.name = "valeur"
        # emission_evit_transport_commun = emission_evit_transport_commun.reset_index()
        # emissions_produits_petroliers = (
        #     emiss_evit_vs_voit
        #     + emission_evit_transport_commun.query("id_energie == 5")["valeur"].sum()
        # )
        # # Emissions totales evitees
        # emission_ges = pd.Series(
        #     {
        #         produits_petroliers: emissions_produits_petroliers.iloc[
        #             0
        #         ],  # Produits pétroliers
        #         gaz: emission_evit_transport_commun.query("id_energie == 7")[
        #             "valeur"
        #         ].sum(),  # Gaz
        #     }
        # )  # Électricité

        # # BAISSE DE LA FACTURE ENERGETIQUE
        # # Reduction de la facture energétique
        # red_fact_ener = (
        #     eco_ener_tot.sum()
        #     * (
        #         tx_voit_menag_commerc_essen * prix_essence
        #         + tx_voit_menag_commerc_diesel * prix_diesel
        #     )
        #     / 100
        # )

        # # Énergie en GWh
        # eco_ener_tot /= 1000.0

        # # Émission en ktCO2
        # emission_ges /= 1000.0

        ##################### fin ancienne méthodo #################

        # Taux de remplissage de bus
        taux_remplissage = 10

        # calcul du mix énérgitique
        # division de la conso de chaque type d'energie par la consommation totale
        mix_energetique_conso = pd.DataFrame(
            {
                "valeur_pourcentage": conso_usage_energie.groupby(["COMMODITY"])[
                    "valeur"
                ].sum()
                / conso_usage_energie["valeur"].sum()
            }
        ).reset_index()

        fe_conso_bus = fe.loc[
            (fe.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe.categorie == "conso")
            & (fe.type_veh_cha == "Bus moyen urbain/périurbain")
        ]
        fe_conso_voiture = fe.loc[
            (fe.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe.categorie == "conso")
            & (fe.type_veh_cha == "VL moyen urbain/périurbain")
        ]

        fe_conso = (
            pass_km_apr_instal
            * (
                (0.71 * fe_conso_voiture["valeur"].iloc[0])
                + (0.29 * fe_conso_bus["valeur"].iloc[0] / taux_remplissage)
            )
            / 10
        )

        # calcul du gain énergétique
        gain_df = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        gain_df.COMMODITY = mix_energetique_conso.COMMODITY
        gain_df["valeur"] = mix_energetique_conso["valeur_pourcentage"].multiply(
            fe_conso
        )

        # formatage des données
        gain_dict = gain_df.set_index("COMMODITY")["valeur"].to_dict()
        eco_ener_tot = pd.Series(gain_dict)

        # Emissions GES évitées

        # mix ener
        mix_energetique_ges = pd.DataFrame(
            {
                "valeur_pourcentage": emission_ges_secteur.groupby(["COMMODITY"])[
                    "valeur"
                ].sum()
                / emission_ges_secteur["valeur"].sum()
            }
        ).reset_index()

        fe_ges_bus = fe.loc[
            (fe.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe.categorie == "GES")
            & (fe.type_veh_cha == "Bus moyen urbain/périurbain")
        ]
        fe_conso_voiture = fe.loc[
            (fe.SECTOR == Sector.ROAD_TRANSPORT)
            & (fe.categorie == "GES")
            & (fe.type_veh_cha == "VL moyen urbain/périurbain")
        ]

        fe_ges = (
            pass_km_apr_instal
            * (
                (0.71 * fe_conso_voiture["valeur"].iloc[0])
                + (0.29 * fe_ges_bus["valeur"].iloc[0] / taux_remplissage)
            )
            / 10
        )

        # calcul des émissions ges évitées
        gain_ges_df = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        gain_ges_df.COMMODITY = mix_energetique_ges.COMMODITY
        gain_ges_df["valeur"] = mix_energetique_ges["valeur_pourcentage"].multiply(
            fe_ges / 1000  # émissions en ktCO2
        )

        # formatage des données
        gain_ges_dict = gain_ges_df.set_index("COMMODITY")["valeur"].to_dict()
        emission_ges = pd.Series(gain_ges_dict)

        # BAISSE DE LA FACTURE ENERGETIQUE
        # Reduction de la facture energétique
        red_fact_ener = (
            eco_ener_tot.sum()
            * (
                tx_voit_menag_commerc_essen * prix_essence
                + tx_voit_menag_commerc_diesel * prix_diesel
            )
            * 10
        )

        # Émission polluants atmosphérique
        if enable_air_pollutants_impacts:
            emissions_pollutants = {}
            for pollutant in POLLUTANTS:
                emissions_pollutants[pollutant] = self.data["emission_" + pollutant]
                if len(emissions_pollutants[pollutant]) == 0:
                    enable_air_pollutants_impacts = False
                    logger.warn(
                        f"Disabled air pollutants inside bike lanes action because {pollutant} data table is empty."
                    )
                    break
        if enable_air_pollutants_impacts:
            fep_voiture = {}
            fep_bus = {}
            # Facteurs d'émissions des polluants atmosphériques
            fep_voiture["nox"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "NOx")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]
            fep_voiture["covnm"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "COVNM")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]
            fep_voiture["pm10"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "PM10")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]
            fep_voiture["pm25"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "PM2.5")
                & (fe.type_veh_cha == "VL moyen urbain/périurbain")
            ]

            fep_bus["nox"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "NOx")
                & (fe.type_veh_cha == "Bus moyen urbain/périurbain")
            ]
            fep_bus["covnm"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "COVNM")
                & (fe.type_veh_cha == "Bus moyen urbain/périurbain")
            ]
            fep_bus["pm10"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "PM10")
                & (fe.type_veh_cha == "Bus moyen urbain/périurbain")
            ]
            fep_bus["pm25"] = fe.loc[
                (fe.SECTOR == Sector.ROAD_TRANSPORT)
                & (fe.categorie == "PM2.5")
                & (fe.type_veh_cha == "Bus moyen urbain/périurbain")
            ]

            # Calcul des émissions de polluants évités
            # Émission en tonne
            gains_pollutants_df = {}

            for pollutant in ["nox", "covnm", "pm10", "pm25"]:
                fe_pollutants = (
                    pass_km_apr_instal
                    * (
                        (0.71 * fep_voiture[pollutant]["valeur"].iloc[0])
                        + (
                            0.29
                            * fep_bus[pollutant]["valeur"].iloc[0]
                            / taux_remplissage
                        )
                    )
                    / 10
                )
                df = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
                df.COMMODITY = emissions_pollutants[pollutant].COMMODITY
                df["valeur"] = 0
                # 5 : Produits pétroliers
                df.loc[df.COMMODITY == Commodity.OIL, "valeur"] = fe_pollutants
                df_dict = df.set_index("COMMODITY")["valeur"].to_dict()
                gains_pollutants_df[pollutant] = pd.Series(df_dict)

            return {
                Impacts.EnergyConsumption(
                    eco_ener_tot, year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.AvoidedEmissions(
                    emission_ges, year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsCOVNM(
                    gains_pollutants_df["covnm"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsNH3(None, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsSOX(None, year, sector=Sector.ROAD_TRANSPORT),
                Impacts.PollutantsPM10(
                    gains_pollutants_df["pm10"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsPM25(
                    gains_pollutants_df["pm25"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.PollutantsNOX(
                    gains_pollutants_df["nox"], year, sector=Sector.ROAD_TRANSPORT
                ),
                Impacts.EnergyBill(red_fact_ener, year),
                Impacts.TaxRevenue(None, year),
            }

        return {
            Impacts.EnergyConsumption(eco_ener_tot, year, sector=Sector.ROAD_TRANSPORT),
            Impacts.AvoidedEmissions(emission_ges, year, sector=Sector.ROAD_TRANSPORT),
            Impacts.EnergyBill(red_fact_ener, year),
            Impacts.TaxRevenue(None, year),
        }
