# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
)


class WindPowerPlant(AbstractAction):
    INPUT_POWER_INSTALLED = "3b_puiss_install"

    PARAM_CHARGE_FACTOR = "Facteur de charge"
    PARAM_TAX_RATE_PROPERTY = "Taux d'imposition foncière"
    PARAM_TAX_RATE_CFE = "Taux d'imposition CFE"
    PARAM_AVG_POWER_WIND_TURB = "Puissance moyenne de l'éolien terrestre"
    PARAM_TAX_RATE_DEVELOPMENT = "Taux d'imposition d'aménagement"

    PARAM_ADVANCED_STATIC = "action3b_parametres_avances"
    PARAM_IFER = "ifer"
    PARAM_PRICES_CRE = "action3b_tarif_cre"

    def __init__(self):
        super().__init__("Wind power plant", "3b")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )

        # Paramètres statiques
        self.add_mandatory_requisite(
            "Paramètres avancés statiques",
            self.PARAM_ADVANCED_STATIC,
            Requisite.Types.PARAM,
        )
        self.add_mandatory_requisite(
            "IFER",
            self.PARAM_IFER,
            Requisite.Types.PARAM,
        )
        self.add_mandatory_requisite(
            "Tarifs de la CRÉ",
            self.PARAM_PRICES_CRE,
            Requisite.Types.PARAM,
        )

        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_CHARGE_FACTOR,
            self.PARAM_CHARGE_FACTOR,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_PROPERTY,
            self.PARAM_TAX_RATE_PROPERTY,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_CFE,
            self.PARAM_TAX_RATE_CFE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_AVG_POWER_WIND_TURB,
            self.PARAM_AVG_POWER_WIND_TURB,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_DEVELOPMENT,
            self.PARAM_TAX_RATE_DEVELOPMENT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            "Puissance installée",
            self.INPUT_POWER_INSTALLED,
            Requisite.Types.INPUT,
        )

    async def _execute(self, year, *args, **kwargs):
        """
        https://gitlab.com/terristory/terristory/issues/447
        """
        puissance = float(
            self.inputs.get(self.INPUT_POWER_INSTALLED, {}).get(year, 0.0)
        )

        if puissance == 0:
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.WIND,
                    energy_vector=EnergyVector.ELECTRICITY,
                ),
                Impacts.AvoidedEmissions(0.0, year),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        prix_moy = prix[
            (prix.COMMODITY == Commodity.ELECTRICITY)
            & prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.INDUSTRY_WITHOUT_ENERGY))
        ][
            "valeur"
        ].mean()  # id_energie = 2 correspond à l'électricité

        feges = self.params[self.PARAM_FEGES]
        facteur_emiss_eolien = feges.loc[(feges.usage == "Eolien")]["valeur"].iloc[0]

        # paramètres avancés modifiables
        user_advanced_params = self.input_params

        # paramètres avancés non modifiables
        advanced_params = self.params[self.PARAM_ADVANCED_STATIC]
        # Ci-dessous les paramètres qui sont modifiables par l'utilisateur
        facteur_charge = user_advanced_params[self.PARAM_CHARGE_FACTOR] / 100
        taux_imposition_fonciere = (
            user_advanced_params[self.PARAM_TAX_RATE_PROPERTY] / 100
        )
        taux_imposition_CFE = user_advanced_params[self.PARAM_TAX_RATE_CFE] / 100
        puiss_moy_eol_terrestre = user_advanced_params[self.PARAM_AVG_POWER_WIND_TURB]
        taux_imposition_amenagement = (
            user_advanced_params[self.PARAM_TAX_RATE_DEVELOPMENT] / 100
        )

        # Ci-dessous les paramètres qui sont non modifiable par l'utilisateur
        coefficient_VLC = float(
            advanced_params.loc[
                advanced_params["nom_parametre"] == "coefficient_VLC", "valeur"
            ]
        )
        ratio_investissement = float(
            advanced_params.loc[
                advanced_params["nom_parametre"] == "ratio_investissement", "valeur"
            ]
        )
        ratio_exploitation = float(
            advanced_params.loc[
                advanced_params["nom_parametre"] == "ratio_exploitation", "valeur"
            ]
        )

        # Paramètre de l'IFER (Imposition Forfaitaire des Entreprises de Réseaux)
        ifer = self.params[self.PARAM_IFER]
        taux_IFER = ifer[ifer.category == "eolien"]["value"].iloc[0]

        # Paramètre du montant de rachat dans les appels à projet passé par la cre (commission de la régulation de l'énergie)
        cre = self.params[self.PARAM_PRICES_CRE]
        tarif_moyen = cre[cre.annee == int(year)]["valeur_eolien"].iloc[0]

        # Energie produite
        energie_produite = puissance * facteur_charge * 8760

        # Emissions evitees :
        #   e facteur d'émission de l'éolien  comprend le FE de l'electricité lors des heures de production de l'éolien
        #   l'ACV n'est pas prise en compte
        emiss_evitees = energie_produite * facteur_emiss_eolien

        # Baisse de la facture énergétique du territoire
        baisse_factur_ener = energie_produite * prix_moy / 100

        # Calculs retombées fiscales
        # Chiffre d'affaires
        chiffre_aff = tarif_moyen * energie_produite / 1000

        # Charges
        charges = ratio_exploitation * puissance

        # CFE -k euro
        prix_revient = ratio_investissement * puissance

        # valeur locative cadastrale - k€
        valeur_loc_cadadstr = 8 / 100 * prix_revient

        # taxe foncière sur les propriétés baties
        assiette_tfpb = valeur_loc_cadadstr * coefficient_VLC
        taxe_fonciere = taux_imposition_fonciere * assiette_tfpb

        # cfe (cotisation foncière des entreprises) - keuros
        cfe = taux_imposition_CFE * valeur_loc_cadadstr * (1 - 0.3)

        # ifer (imposition forfaitaire sur les entreprises de réseau)
        ifer = puissance * taux_IFER

        # Énergie en GWh
        energie_produite /= 1000.0

        # Émission en ktCO2
        emiss_evitees /= 1000.0

        # taxe d'aménagement
        nb_eoliennes = puissance / puiss_moy_eol_terrestre
        taxe_amenagement = taux_imposition_amenagement * nb_eoliennes * 3000 / 1000

        # Part des recettes fiscales par type de territoire - à afficher par type d'échelle commune/epci/département/région
        recette_fiscale = {
            "commune": 0.3 * taxe_fonciere
            + 0.5 * ifer
            + 0.5 * cfe
            + (5 / 7) * taxe_amenagement,
            "epci": 0.2 * taxe_fonciere + 0.5 * cfe,
            "departement": 0.5 * taxe_fonciere
            + 0.5 * ifer
            + (2 / 7) * taxe_amenagement,
            "region": 0,
        }

        return {
            Impacts.EnergyProduction(
                energie_produite,
                year,
                prod_type=RenewableProd.WIND,
                energy_vector=EnergyVector.ELECTRICITY,
            ),
            Impacts.AvoidedEmissions(
                0.0,  # emiss_evitees,
                year,
                commodity=Commodity.ELECTRICITY,
                sector=Sector.ENERGY_INDUSTRY,
            ),
            Impacts.EnergyBill(baisse_factur_ener, year),
            Impacts.TaxRevenue(recette_fiscale, year),
        }
