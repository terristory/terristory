# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
import pandas as pd

from terriapi.controller.strategy_actions import (
    POLLUTANTS,
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)
from terriapi.controller.strategy_actions.tools import calculate_conso_totale


class EmissionsReductionNonEnergyAgriculture(AbstractAction):
    INPUT_REDUCTION = "22_emissions_non_eneg_agri"

    PARAM_DEV_METHANIZ = "Développer la méthanisation"
    PARAM_REDUCE_MIN_FERT = "Réduire la dose d'engrais minéral"
    PARAM_REPLACE_MIN_BY_SYN = (
        "Substituer l'azote minéral de synthèse par l'azote des produits organiques"
    )
    PARAM_REDUCE_NITROGEN_BY_2 = "Diminution de l'azote excrétée d'un rapport 2"
    PARAM_REPLACE_CARBO_BY_FAT = (
        "Substituer des glucides par des lipides insturés dans les rations"
    )
    PARAM_ADD_NITRATE_RATIONS = "Ajouter un additif (nitrate) dans les rations : addition de 1% de nitrate dans les rations"
    PARAM_DEV_AGROFOREST = "Développer l'agroforesterie à faible densité d'arbres"
    PARAM_DEV_HEDGES = "Développer les haies en périphérie des parcelles agricoles"

    def __init__(self):
        super().__init__(
            "Emissions reduction for non energetic usage in agriculture", "22"
        )

    def add_requisites(self):
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_DEV_METHANIZ,
            self.PARAM_DEV_METHANIZ,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_REDUCE_MIN_FERT,
            self.PARAM_REDUCE_MIN_FERT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_REPLACE_MIN_BY_SYN,
            self.PARAM_REPLACE_MIN_BY_SYN,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_REDUCE_NITROGEN_BY_2,
            self.PARAM_REDUCE_NITROGEN_BY_2,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_REPLACE_CARBO_BY_FAT,
            self.PARAM_REPLACE_CARBO_BY_FAT,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_ADD_NITRATE_RATIONS,
            self.PARAM_ADD_NITRATE_RATIONS,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_DEV_AGROFOREST,
            self.PARAM_DEV_AGROFOREST,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_DEV_HEDGES,
            self.PARAM_DEV_HEDGES,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_REDUCTION,
            self.INPUT_REDUCTION,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "emission_ges_agriculture", DataSet.EMISSIONS, Sector.AGRICULTURE
        )

    async def _execute(self, year, *args, **kwargs):
        # paramètres avancés des actions modifiables par l'utilisateur
        emission_ges_agri = self.data["emission_ges_agriculture"]

        # Récuperer les entrées utilisateurs (%)
        user_entree = float(self.inputs[self.INPUT_REDUCTION][year]) / 100
        history_emissions = self.inputs[self.INPUT_REDUCTION]

        # si toutes les entrées utilisateurs sont nulle, pas nécessaire de calculer
        if user_entree == 0:
            return {
                Impacts.AvoidedEmissions(0.0, year),  # emiss_evitees,
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(0.0, year),
                Impacts.CO2EmissionPerSubAction(
                    "taux_methanisation",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionPerSubAction(
                    "taux_reduction_engrais_mineral",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionPerSubAction(
                    "taux_substitution_azote_mineral",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionPerSubAction(
                    "taux_diminution_azote",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionPerSubAction(
                    "taux_substitution_glucides",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionPerSubAction(
                    "taux_nitrate",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionPerSubAction(
                    "taux_agroforesterie",
                    0.0,
                    year,
                ),
                Impacts.CO2EmissionPerSubAction(
                    "taux_haies_parcelles_agricoles",
                    0.0,
                    year,
                ),
            }

        # calcul des émisions GES non-énergétique (id : 4) totale pour chaque année précedant l'année passée en paramétres
        emission_ges_totale_annee_precedente = calculate_conso_totale(
            int(year) - 1,
            history_emissions,
            emission_ges_agri[emission_ges_agri.COMMODITY == Commodity.NON_ENERGETIC],
        )

        # calcul les émission GES non-énergétique (id : 4) totale pour chaque année passée en paramétres
        emission_ges_totale = calculate_conso_totale(
            int(year),
            history_emissions,
            emission_ges_agri[emission_ges_agri.COMMODITY == Commodity.NON_ENERGETIC],
        )

        # calculer les émisions GES non-énergétique totale évitée (id : 4)
        # équivalent à emission_ges_totale_annee_reference * (int(user_params['22_emissions_non_eneg_agri'][year])/100)
        emission_total_evitee = (
            emission_ges_totale_annee_precedente - emission_ges_totale
        )
        emission_ges = pd.DataFrame(columns=["COMMODITY", "valeur"], dtype=object)
        emission_ges.COMMODITY = emission_ges_agri.COMMODITY
        emission_ges["valeur"] = 0
        # 4 : non-énergétique
        emission_ges.loc[
            emission_ges.COMMODITY == Commodity.NON_ENERGETIC, "valeur"
        ] = emission_total_evitee

        # formater les données d'émission ges
        emission_ges = emission_ges.set_index("COMMODITY")["valeur"].to_dict()
        emission_ges = pd.Series(emission_ges)

        # % des sous-industries
        params = self.input_params
        taux_methanisation = (
            float(params[self.PARAM_DEV_METHANIZ]) / 100
        ) * emission_total_evitee
        taux_reduction_engrais_mineral = (
            float(params[self.PARAM_REDUCE_MIN_FERT]) / 100
        ) * emission_total_evitee
        taux_substitution_azote_mineral = (
            float(params[self.PARAM_REPLACE_MIN_BY_SYN]) / 100
        ) * emission_total_evitee
        taux_diminution_azote = (
            float(params[self.PARAM_REDUCE_NITROGEN_BY_2]) / 100
        ) * emission_total_evitee
        taux_substitution_glucides = (
            float(params[self.PARAM_REPLACE_CARBO_BY_FAT]) / 100
        ) * emission_total_evitee
        taux_nitrate = (
            float(params[self.PARAM_ADD_NITRATE_RATIONS]) / 100
        ) * emission_total_evitee
        taux_agroforesterie = (
            float(params[self.PARAM_DEV_AGROFOREST]) / 100
        ) * emission_total_evitee
        taux_haies_parcelles_agricoles = (
            float(params[self.PARAM_DEV_HEDGES]) / 100
        ) * emission_total_evitee

        return {
            Impacts.AvoidedEmissions(emission_ges, year, sector=Sector.AGRICULTURE),
            Impacts.EnergyBill(0.0, year),
            Impacts.TaxRevenue(None, year),
            Impacts.CO2EmissionPerSubAction(
                "taux_methanisation",
                taux_methanisation,
                year,
            ),
            Impacts.CO2EmissionPerSubAction(
                "taux_reduction_engrais_mineral",
                taux_reduction_engrais_mineral,
                year,
            ),
            Impacts.CO2EmissionPerSubAction(
                "taux_substitution_azote_mineral",
                taux_substitution_azote_mineral,
                year,
            ),
            Impacts.CO2EmissionPerSubAction(
                "taux_diminution_azote",
                taux_diminution_azote,
                year,
            ),
            Impacts.CO2EmissionPerSubAction(
                "taux_substitution_glucides",
                taux_substitution_glucides,
                year,
            ),
            Impacts.CO2EmissionPerSubAction(
                "taux_nitrate",
                taux_nitrate,
                year,
            ),
            Impacts.CO2EmissionPerSubAction(
                "taux_agroforesterie",
                taux_agroforesterie,
                year,
            ),
            Impacts.CO2EmissionPerSubAction(
                "taux_haies_parcelles_agricoles",
                taux_haies_parcelles_agricoles,
                year,
            ),
        }
