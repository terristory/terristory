# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import numpy as np
import pandas as pd

from terriapi.controller.strategy_actions import (
    POLLUTANTS,
    AbstractAction,
    Commodity,
    DataSet,
    Impacts,
    ParamCommodity,
    ParamDataSet,
    Requisite,
    Sector,
    Usage,
)
from terriapi.controller.strategy_actions.tools import (
    _calcul_baisse_emission_et_conso_par_secteur,
)


class TertiaryRefurbishment(AbstractAction):
    PARAM_GAIN = "heating_gain"
    PARAM_GAIN_LOW_NAME = "Gain énergétique pour des rénovations de niveau faible"
    PARAM_GAIN_HIGH_NAME = "Gain énergétique pour des rénovations performantes"
    PARAM_RENOV = "distribution_amongst_dpe_levels"
    PARAM_RENOV_NAME = "Répartition des rénovations par sous secteur"

    PARAM_AVG_CONS = "action2_consommation_moyenne_unitaire"

    INPUT_SURFACE = "2_surface_bat_tertiaire"
    INPUT_REFURB_LVL_LOW = "2_part_niveau_renov_faible"
    INPUT_REFURB_LVL_HIGH = "2_part_niveau_renov_perf"

    def __init__(self):
        super().__init__("Tertiary Refurbishment", "2")

    def add_requisites(self):
        self.add_optional_requisite(
            False,
            "Enable air pollutants impacts",
            "enable_air_pollutants_impacts",
            Requisite.Types.CONFIG,
        )

        # Paramètres statiques
        self.add_mandatory_requisite(
            "Consommation moyenne unitaire des différents types de tertiaire",
            self.PARAM_AVG_CONS,
            Requisite.Types.PARAM,
        )

        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_RENOV_NAME,
            self.PARAM_RENOV,
            Requisite.Types.INPUT_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            "Gain énergétique par niveau de rénovation",
            self.PARAM_GAIN,
            Requisite.Types.INPUT_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            "Surface",
            self.INPUT_SURFACE,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            "Part (%) de rénovations faibles",
            self.INPUT_REFURB_LVL_LOW,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            "Part (%) de rénovations fortes",
            self.INPUT_REFURB_LVL_HIGH,
            Requisite.Types.INPUT,
        )
        self.add_data_requisite(
            "conso_tertiary",
            DataSet.CONSUMPTION,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOLING,
            Sector.SERVICES,
        )

        for pollutant in POLLUTANTS:
            self.add_data_requisite(
                "emission_" + pollutant,
                DataSet.table_from_pollutant(pollutant),
                Sector.SERVICES,
                default=False,
            )

    async def _execute(self, year, *args, **kwargs):
        """
        Calcul d'impact GES, NRJ et facture NRJ
        pour l'action de rénovation tertiaire
        cf : https://gitlab.com/terristory/terristory/-/issues/299
        """
        conso_usage_energie = self.data["conso_tertiary"]

        # conversion milliers de m2 => m2
        surface_bat_tertiaire_va = (
            float(self.inputs.get(self.INPUT_SURFACE, {}).get(year, 0)) * 1000
        )
        reno_faible = float(self.inputs.get(self.INPUT_REFURB_LVL_LOW, {}).get(year, 0))
        reno_performante = float(
            self.inputs.get(self.INPUT_REFURB_LVL_HIGH, {}).get(year, 0)
        )

        enable_air_pollutants_impacts = self.config.get(
            "enable_air_pollutants_impacts", False
        )

        if surface_bat_tertiaire_va == 0:
            return {
                Impacts.EnergyConsumption(0.0, year, sector=Sector.SERVICES),
                Impacts.AvoidedEmissions(0.0, year, sector=Sector.SERVICES),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        # paramètres avancés des actions modifiables par l'utilisateur
        proportion_secteurs_renoves = pd.DataFrame.from_dict(
            self.input_params[self.PARAM_RENOV], orient="index"
        ).reset_index()
        proportion_secteurs_renoves.columns = ["nom", "valeur"]

        # gain energétique
        ge = self.input_params[self.PARAM_GAIN]

        # paramètres non modifiables de l'action
        conso_moy_unitaire_df = self.params[self.PARAM_AVG_CONS]
        # conso usage energie

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        feges = self.params[self.PARAM_FEGES]

        # gain énergétique en % de la conso pour les rénovations de niveau faible
        gain_energetique_reno_faible = (
            ge[self.PARAM_GAIN_LOW_NAME] / 100 * (reno_faible / 100)
        )

        # gain énergétique en % de la conso pour les rénovations de niveau performant
        gain_energetique_reno_performant = (
            ge[self.PARAM_GAIN_HIGH_NAME] / 100 * (reno_performante / 100)
        )

        conso_finale_secteurs_renoves = conso_moy_unitaire_df.merge(
            proportion_secteurs_renoves,
            how="inner",
            left_on="sous_secteurs",
            right_on="nom",
        )

        conso_finale_secteurs_renoves["valeur"] = (
            conso_finale_secteurs_renoves["valeur_x"]
            * conso_finale_secteurs_renoves["valeur_y"]
        )

        # paramètre Electricité kWhep to kWh
        electricite_kwhep_kwh = 2.58 - 1

        # ajustement énergie primaire -> énergie finale
        ## calcul du pourcentage de consommation d'électricité (mix énergétique) pour l'Eau chaude sanitaire (ECS) et climatication,
        ## division de la conso en éléctricité pour le chauffage, l'ECS et la climatisation par la conso totale pour le chauffage, l'ECS et la climatisation
        mix_chauffage_ecs_climatisation_elec = (
            conso_usage_energie.loc[
                (
                    conso_usage_energie.USAGE.isin(
                        (Usage.HEATING, Usage.HOT_WATER, Usage.COOLING)
                    )
                )
                & (conso_usage_energie.COMMODITY == Commodity.ELECTRICITY),
                "valeur",
            ].sum()
            / conso_usage_energie.loc[
                (
                    conso_usage_energie.USAGE.isin(
                        (Usage.HEATING, Usage.HOT_WATER, Usage.COOLING)
                    )
                ),
                "valeur",
            ].sum()
        )

        terme_2 = 1 / (1 + mix_chauffage_ecs_climatisation_elec * electricite_kwhep_kwh)

        gains_energetiques = (
            (
                (gain_energetique_reno_performant + gain_energetique_reno_faible)
                * (conso_finale_secteurs_renoves["valeur"] / 100)
                * surface_bat_tertiaire_va
            ).sum()
            * terme_2
            / 1000
        )

        # calcul du total de l'électricité consommée à partir de la conso par usage en Gwh

        conso_elec = conso_usage_energie[
            conso_usage_energie.COMMODITY == Commodity.ELECTRICITY
        ][["USAGE", "valeur"]]

        #  Énergie = 4 : Électricité
        conso_elec["valeur"] = conso_elec["valeur"] / conso_elec["valeur"].sum()

        # on rajoute la colonne qui va recevoir les valeur d'émission ges
        conso_elec["emission"] = np.nan

        # usage = 1 : chauffage
        conso_elec.loc[conso_elec.USAGE == Usage.HEATING, "emission"] = feges.loc[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.USAGE == Usage.HEATING)
            & (feges.SECTOR == Sector.SERVICES)
        ]["valeur"].iloc[0]
        # Eaux chaudes sanitaires => value from residential
        conso_elec.loc[conso_elec.USAGE == Usage.HOT_WATER, "emission"] = feges.loc[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.USAGE == Usage.HOT_WATER)
            & (feges.SECTOR == Sector.RESIDENTIAL)
        ]["valeur"].iloc[0]
        #  Usage = 3 : Cuisson => value from residential
        conso_elec.loc[conso_elec.USAGE == Usage.COOKING, "emission"] = feges.loc[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.USAGE == Usage.COOKING)
            & (feges.SECTOR == Sector.RESIDENTIAL)
        ]["valeur"].iloc[0]

        #  Usage = 11 : Climatisation
        conso_elec.loc[conso_elec.USAGE == Usage.COOLING, "emission"] = feges.loc[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.USAGE == Usage.COOLING)
            & (feges.SECTOR == Sector.SERVICES),
            "valeur",
        ].iloc[0]

        # Usage = 7 : électricité spécifique ou moyenne
        conso_elec.loc[conso_elec.USAGE == Usage.SPECIFIC_ELEC, "emission"] = feges.loc[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.USAGE == Usage.OTHERS)
            & (feges.SECTOR == Sector.UNDIFFERENCIATED),
            "valeur",
        ].iloc[0]

        emission_electrique_tot_va = (
            conso_elec["valeur"] * conso_elec["emission"]
        ).sum()

        # on ne garde que les lignes réseaux de chaleur, gaz, biomasse, charbon, fioul et les colonnes avec
        # le type d'énergie et la valeur
        nrj_filter = (
            Commodity.URBAN_HEAT_COOL,
            Commodity.GAS,
            Commodity.RENEWABLE_HEAT,
            Commodity.COAL,
        )
        #  1 : Combustibles Minéraux Solides
        #  3 : EnR thermiques
        #  7 : Gaz
        #  9 : Chauffage et froid urbain
        facteur_emission_action_2_df = feges[
            (feges["COMMODITY#1"].isin(nrj_filter))
            | (
                (feges["COMMODITY#1"] == Commodity.OIL)
                & (feges.PARAMCOMMODITY == ParamCommodity.FUEL_OIL)
            )
        ][["COMMODITY#2", "COMMODITY#1", "valeur"]]
        facteur_emission_action_2_df.columns = ["COMMODITY#2", "COMMODITY", "valeur"]
        facteur_emission_action_2_df = pd.concat(
            [
                facteur_emission_action_2_df,
                pd.DataFrame(
                    {
                        "COMMODITY#2": ParamCommodity.ELECTRICITY,
                        "COMMODITY": Commodity.ELECTRICITY,
                        "valeur": emission_electrique_tot_va,
                    },
                    index=[0],
                ),
            ],
            ignore_index=True,
        )

        facteur_emission_action_2_df = facteur_emission_action_2_df.groupby(
            "COMMODITY"
        )[["valeur"]].sum()

        # mix énergetique tertiaire
        valeur_tot_va = conso_usage_energie.valeur.sum()
        mix_energetique_df = pd.DataFrame(
            {
                "valeur": conso_usage_energie.groupby(["COMMODITY"])["valeur"].sum()
                / valeur_tot_va
            }
        ).reset_index()

        # Multiplication du mix énergetique par les facteurs d'émission par énergie
        emission_mixenergie_df = facteur_emission_action_2_df.join(
            mix_energetique_df.set_index("COMMODITY"),
            lsuffix="_emission",
            rsuffix="_mix",
        ).reset_index()

        # gain énergétique par type de conso d'énergie via les ratio du mix énergétique (GWh)
        gain = (
            emission_mixenergie_df["valeur_mix"] * gains_energetiques / 1000.0
        ).to_frame("valeur")
        gain["COMMODITY"] = emission_mixenergie_df.COMMODITY
        gain = gain.dropna()
        gain = gain.set_index("COMMODITY")
        # Émission GES évitées (en ktCO2)
        emission_ges = (
            gains_energetiques
            * emission_mixenergie_df["valeur_emission"]
            * emission_mixenergie_df["valeur_mix"]
            / 1000.0
        ).to_frame("valeur")
        emission_ges["COMMODITY"] = emission_mixenergie_df.COMMODITY
        emission_ges = emission_ges.set_index("COMMODITY")
        # prix moyen
        prix_moy_df = pd.DataFrame(
            {"valeur": prix.groupby(["PARAMCOMMODITY", "COMMODITY"])["valeur"].mean()}
        ).reset_index()
        somme_prix_va = prix_moy_df.set_index("COMMODITY").join(
            mix_energetique_df.set_index("COMMODITY"), lsuffix="_prix"
        )
        somme_prix_va["prix_pondere"] = (
            somme_prix_va["valeur_prix"] * somme_prix_va["valeur"]
        )
        somme_prix_va = somme_prix_va["prix_pondere"].sum()
        gain = gain.dropna()
        emission_ges = emission_ges.dropna()
        baisse_facture_energie = gains_energetiques * somme_prix_va / 100

        # Émission polluants atmosphérique
        if enable_air_pollutants_impacts:
            # Calcul des émissions de polluants évités

            # somme de consommation secteur tertiaire usage chauffage : 1 et ECS : 2  et Climatisation : 11
            sum_conso_chauffage_ecs = conso_usage_energie.loc[
                (
                    conso_usage_energie.USAGE.isin(
                        (Usage.HEATING, Usage.HOT_WATER, Usage.COOLING)
                    )
                ),
                "valeur",
            ].sum()

            # retranscription en poucentage de la baisse énergétique
            baisse_polluant_coef_multiplicateur = (
                gains_energetiques / sum_conso_chauffage_ecs
            ) / 1000

            pollutants_gains = {}
            for pollutant in POLLUTANTS:
                emission_pollutant = self.data.get("emission_" + pollutant, False)

                # impacts on each pollutant using the decrease by sector
                pollutants_gains[pollutant] = (
                    _calcul_baisse_emission_et_conso_par_secteur(
                        emission_pollutant,
                        baisse_polluant_coef_multiplicateur,
                        Commodity,
                        Commodity.ALL,
                    )
                )

            return {
                Impacts.EnergyConsumption(gain["valeur"], year, sector=Sector.SERVICES),
                Impacts.AvoidedEmissions(
                    emission_ges["valeur"], year, sector=Sector.SERVICES
                ),
                Impacts.PollutantsCOVNM(
                    pollutants_gains["covnm"], year, sector=Sector.SERVICES
                ),
                Impacts.PollutantsNH3(
                    pollutants_gains["nh3"], year, sector=Sector.SERVICES
                ),
                Impacts.PollutantsPM10(
                    pollutants_gains["pm10"], year, sector=Sector.SERVICES
                ),
                Impacts.PollutantsPM25(
                    pollutants_gains["pm25"], year, sector=Sector.SERVICES
                ),
                Impacts.PollutantsNOX(
                    pollutants_gains["nox"], year, sector=Sector.SERVICES
                ),
                Impacts.PollutantsSOX(
                    pollutants_gains["sox"], year, sector=Sector.SERVICES
                ),
                Impacts.EnergyBill(baisse_facture_energie, year),
                Impacts.TaxRevenue(None, year),
            }

        return {
            Impacts.EnergyConsumption(gain["valeur"], year, sector=Sector.SERVICES),
            Impacts.AvoidedEmissions(
                emission_ges["valeur"], year, sector=Sector.SERVICES
            ),
            Impacts.EnergyBill(baisse_facture_energie, year),
            Impacts.TaxRevenue(None, year),
        }
