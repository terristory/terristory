# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)


class MethanizationCogeneration(AbstractAction):
    INPUT_NB_METHANIZERS = "6_nb_methaniseur"
    INPUT_AVG_POWER = "6_puiss_moy_methaniseurs"

    PARAM_TIME_FULLPOWER = "Temps de fonctionnement en pleine puissance"
    PARAM_AUX_ELEC_CONSUMP = "Consommation électrique des auxiliaires"
    PARAM_ELEC_PERF = "Rendement électrique"
    PARAM_ENERGY_EFF = "Efficacité énergetique"

    def __init__(self):
        super().__init__("Methanization by cogeneration", "6")

    def add_requisites(self):
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_TIME_FULLPOWER,
            self.PARAM_TIME_FULLPOWER,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_AUX_ELEC_CONSUMP,
            self.PARAM_AUX_ELEC_CONSUMP,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_ELEC_PERF,
            self.PARAM_ELEC_PERF,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_ENERGY_EFF,
            self.PARAM_ENERGY_EFF,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )

        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_NB_METHANIZERS,
            self.INPUT_NB_METHANIZERS,
            Requisite.Types.INPUT,
        )
        self.add_mandatory_requisite(
            self.INPUT_AVG_POWER,
            self.INPUT_AVG_POWER,
            Requisite.Types.INPUT,
        )

        # données requises
        self.add_data_requisite(
            "conso_residentiel",
            DataSet.CONSUMPTION,
            Sector.RESIDENTIAL,
            Usage.HEATING,
            Usage.HOT_WATER,
            Usage.COOKING,
            Usage.SPECIFIC_ELEC,
            Usage.COOLING,
        )

    async def _execute(self, year, *args, **kwargs):
        """
        https://gitlab.com/terristory/terristory/-/issues/320
        """
        nb_methaniseur = int(self.inputs[self.INPUT_NB_METHANIZERS][year])
        puiss_moy_methaniseur = float(self.inputs[self.INPUT_AVG_POWER][year])

        # TODO: avoid if no power?
        if nb_methaniseur == 0 or puiss_moy_methaniseur == 0:
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.BIOGAS_ELEC,
                    energy_vector=EnergyVector.ELECTRICITY,
                ),
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.BIOGAS_THER,
                    energy_vector=EnergyVector.HEAT,
                ),
                Impacts.AvoidedEmissions(0.0, year),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        parametres_avances = self.input_params

        feges = self.params[self.PARAM_FEGES]

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]

        # Stockage des variables ci-dessous pour plus de lisibilite des calculs finaux

        tp_fonct_pleine_puiss = parametres_avances[self.PARAM_TIME_FULLPOWER]
        conso_elec_des_auxiliaires = (
            parametres_avances[self.PARAM_AUX_ELEC_CONSUMP] / 100.0
        )
        elec_performance = parametres_avances[self.PARAM_ELEC_PERF] / 100.0

        facteur_emiss_elec = feges[
            (feges["COMMODITY#1"] == Commodity.ELECTRICITY)
            & (feges.SECTOR == Sector.UNDIFFERENCIATED)
            & (feges.USAGE == Usage.OTHERS)
        ].valeur.iloc[0]

        facteur_emiss_chal = feges[
            (feges["COMMODITY#1"] == Commodity.GAS)
            & (feges.SECTOR == Sector.UNDIFFERENCIATED)
        ].valeur.iloc[0]

        # TODO: pourquoi ici ID = 1 et 2 ? 1 = charbon et 2 = électricité ?
        prix_moy_elec = prix[prix.COMMODITY == Commodity.ELECTRICITY]["valeur"].mean()
        prix_rdc = prix[prix.COMMODITY == Commodity.URBAN_HEAT_COOL]["valeur"].iloc[0]

        efficacite_ener = parametres_avances[self.PARAM_ENERGY_EFF] / 100
        # Puissance installée
        puiss_instal = nb_methaniseur * puiss_moy_methaniseur
        # Energie electrique produite
        ener_ele_prod = puiss_instal * tp_fonct_pleine_puiss
        # Electricite livree
        elec_livree = ener_ele_prod * (1 - conso_elec_des_auxiliaires)

        # 1.111500e+04
        # Chaleur livree
        chaleur_livree = ener_ele_prod * (
            efficacite_ener / elec_performance - 1 + conso_elec_des_auxiliaires
        )
        # Emissions electriques evitees
        emiss_elec_evitees = elec_livree * facteur_emiss_elec
        # Emission issues du gaz evitees
        emiss_chal_evitees = chaleur_livree * facteur_emiss_chal
        # Emissions totales evitees----------------
        emiss_tot_evitees = emiss_elec_evitees + emiss_chal_evitees

        # Gains lies a la vente d'electricite
        gain_vente_elec = elec_livree * prix_moy_elec / 100
        # gains lies a la vente de chaleur
        gain_vente_chaleur = chaleur_livree * prix_rdc / 100
        # Baisse de la facture energetique
        baisse_fact_ener = gain_vente_elec + gain_vente_chaleur
        # pas de retombées fiscales calculées (suppression cvae)
        # Recette par maille (commune, communauté de commune, département, region)
        recette_fiscale = {
            "commune": 0,
            "epci": 0,
            "departement": 0,
            "region": 0,
        }

        # Énergie en GWh
        elec_livree /= 1000.0
        chaleur_livree /= 1000.0
        # Émission en ktCO2
        emiss_tot_evitees /= 1000.0

        return {
            Impacts.EnergyProduction(
                elec_livree,
                year,
                prod_type=RenewableProd.BIOGAS_ELEC,
                energy_vector=EnergyVector.ELECTRICITY,
            ),
            Impacts.EnergyProduction(
                chaleur_livree,
                year,
                prod_type=RenewableProd.BIOGAS_THER,
                energy_vector=EnergyVector.HEAT,
            ),
            # TODO: fix this? what sector is counted for avoided emissions?
            # problem is that these emissions are not used in original counts
            # Should we include them now?
            Impacts.AvoidedEmissions(
                0.0,  # emiss_elec_evitees,
                year,
                sector=Sector.RESIDENTIAL,
                commodity=Commodity.ELECTRICITY,
            ),
            Impacts.AvoidedEmissions(
                0.0,  # emiss_chal_evitees,
                year,
                sector=Sector.RESIDENTIAL,
                commodity=Commodity.RENEWABLE_HEAT,
            ),
            Impacts.EnergyBill(baisse_fact_ener, year),
            Impacts.TaxRevenue(recette_fiscale, year),
        }
