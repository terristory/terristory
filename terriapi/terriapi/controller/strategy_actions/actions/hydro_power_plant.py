# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller.strategy_actions import (
    AbstractAction,
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    ParamDataSet,
    RenewableProd,
    Requisite,
    Sector,
    Usage,
)


class HydroPowerPlant(AbstractAction):
    INPUT_CAPACITY = "12_puiss_installee"

    PARAM_LOAD_FACTOR = "Facteur de charge"
    PARAM_TAX_RATE_PROPERTY = "Taux d'imposition foncière"
    PARAM_TAX_RATE_CFE = "Taux d'imposition CFE"

    PARAM_ADVANCED_STATIC = "action12_parametres_avances"
    PARAM_IFER = "ifer"

    def __init__(self):
        super().__init__("Hydro power plant", "12")

    def add_requisites(self):
        # paramètre globaux
        self.add_mandatory_requisite(
            "Facteurs d'émissions",
            self.PARAM_FEGES,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.GES_FACTORS,
        )
        # paramètres globaux des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            "Prix global de l'énergie",
            self.PARAM_PRICE,
            Requisite.Types.PARAM,
            passage_table=ParamDataSet.PRICES,
        )
        self.add_mandatory_requisite(
            "Paramètres avancés statiques",
            self.PARAM_ADVANCED_STATIC,
            Requisite.Types.PARAM,
        )
        # paramètres avancés des actions modifiables par l'utilisateur
        self.add_mandatory_requisite(
            self.PARAM_LOAD_FACTOR,
            self.PARAM_LOAD_FACTOR,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_PROPERTY,
            self.PARAM_TAX_RATE_PROPERTY,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        self.add_mandatory_requisite(
            self.PARAM_TAX_RATE_CFE,
            self.PARAM_TAX_RATE_CFE,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        )
        # Paramètres statiques
        self.add_mandatory_requisite(
            "IFER",
            self.PARAM_IFER,
            Requisite.Types.PARAM,
        )
        # entrées de l'utilisateur
        self.add_mandatory_requisite(
            self.INPUT_CAPACITY,
            self.INPUT_CAPACITY,
            Requisite.Types.INPUT,
        )

    async def _execute(self, year, *args, **kwargs):
        """ """
        puissance = float(self.inputs[self.INPUT_CAPACITY][year])
        if puissance == 0:
            return {
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.HYDRO_LOW,
                    energy_vector=EnergyVector.ELECTRICITY,
                ),
                Impacts.EnergyProduction(
                    0.0,
                    year,
                    prod_type=RenewableProd.HYDRO_HIGH,
                    energy_vector=EnergyVector.ELECTRICITY,
                ),
                Impacts.AvoidedEmissions(0.0, year),
                Impacts.EnergyBill(0.0, year),
                Impacts.TaxRevenue(None, year),
            }

        prix = self.params[self.PARAM_PRICE]
        prix = prix[prix.annee == int(year)]
        # TODO: Correction ici => prix du charbon et non de l'électricité précédemment ?
        prix_moy = prix[
            (prix.COMMODITY == Commodity.ELECTRICITY)
            & prix.SECTOR.isin((Sector.RESIDENTIAL, Sector.INDUSTRY_WITHOUT_ENERGY))
        ][
            "valeur"
        ].mean()  # id_energie = 2 correspond à l'électricité

        feges = self.params[self.PARAM_FEGES]
        facteur_emiss_hydro = feges.loc[
            (feges.RENEWABLEPROD == RenewableProd.HYDRO_HIGH)
            | (feges.RENEWABLEPROD == RenewableProd.HYDRO_LOW)
        ]["valeur"].iloc[0]

        # paramètres avancés modifiables
        user_advanced_params = self.input_params

        # paramètres avancés non modifiables
        advanced_params = self.params[self.PARAM_ADVANCED_STATIC]

        # Ci-dessous les paramètres qui sont modifiables par l'utilisateur
        facteur_charge = user_advanced_params[self.PARAM_LOAD_FACTOR] / 100
        taux_imposition_fonciere = (
            user_advanced_params[self.PARAM_TAX_RATE_PROPERTY] / 100
        )
        taux_imposition_CFE = user_advanced_params[self.PARAM_TAX_RATE_CFE] / 100

        # Ci-dessous les paramètres qui sont non modifiable par l'utilisateur
        # RQ: actuellement, plus d'obligations d'achats mais appel d'offres
        tarif_moyen = float(advanced_params.loc[advanced_params["id"] == 1, "valeur"])
        ratio_investissement = float(
            advanced_params.loc[advanced_params["id"] == 2, "valeur"]
        )
        ratio_exploitation = float(
            advanced_params.loc[advanced_params["id"] == 3, "valeur"]
        )
        taux_VLC = 0.08
        coefficient_VLC = float(
            advanced_params.loc[advanced_params["id"] == 5, "valeur"]
        )

        # Paramètre de l'IFER (Imposition Forfaitaire des Entreprises de Réseaux)
        ifer = self.params[self.PARAM_IFER]
        ratio_IFER = ifer[ifer.category == "hydro"]["value"].iloc[0]

        # Energie produite
        energie_produite = puissance * facteur_charge * 8760

        # Emissions evitees :
        #   le facteur d'émission de l'hydro  comprend le FE de l'electricité lors des heures de production de l'hydro
        #   l'ACV n'est pas prise en compte
        emiss_evitees = energie_produite * facteur_emiss_hydro

        # Baisse de la facture énergétique du territoire
        baisse_factur_ener = energie_produite * prix_moy / 100

        # Calculs retombées fiscales
        # Chiffre d'affaires
        chiffre_aff = tarif_moyen * energie_produite / 1000

        # Charges
        charges = ratio_exploitation * puissance

        # CFE -k euro
        prix_revient = ratio_investissement * puissance

        # valeur locative cadastrale - k€
        valeur_loc_cadadstr = taux_VLC * prix_revient

        # taxe foncière sur les propriétés baties
        assiette_tfpb = valeur_loc_cadadstr * coefficient_VLC

        taxe_fonciere = taux_imposition_fonciere * assiette_tfpb

        # cfe (cotisation foncière des entreprises) - keuros
        cfe = taux_imposition_CFE * valeur_loc_cadadstr * (1 - 0.3)

        # ifer (imposition forfaitaire sur les entreprises de réseau)
        ifer = puissance * ratio_IFER

        # Énergie en GWh
        energie_produite /= 1000.0
        # émissoin en ktCO2
        emiss_evitees /= 1000.0

        # recettes fiscales par type de territoire
        recette_fiscale = {
            "commune": 0.3 * taxe_fonciere + 0.5 * ifer + 0.5 * cfe,
            "epci": 0.2 * taxe_fonciere + 0.5 * cfe,
            "departement": 0.5 * taxe_fonciere + 0.5 * ifer,
            "region": 0,
        }

        return {
            Impacts.EnergyProduction(
                energie_produite,
                year,
                prod_type=RenewableProd.HYDRO_LOW,
                energy_vector=EnergyVector.ELECTRICITY,
            ),
            Impacts.EnergyProduction(
                0.0,
                year,
                prod_type=RenewableProd.HYDRO_HIGH,
                energy_vector=EnergyVector.ELECTRICITY,
            ),
            Impacts.AvoidedEmissions(
                0.0,  # emiss_evitees,
                year,
                commodity=Commodity.ELECTRICITY,
                sector=Sector.ENERGY_INDUSTRY,
            ),
            Impacts.EnergyBill(baisse_factur_ener, year),
            Impacts.TaxRevenue(recette_fiscale, year),
        }
