# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from ..actions_dispatcher import ActionsDispatcher
from .actions import *


class ActionsTester(ActionsDispatcher):
    def __init__(self, region, zone, zone_id):
        super().__init__(region, zone, zone_id)
        self.actions = {
            "residential_refurbishment": ResidentialRefurbishmentTest(),
            "tertiary_refurbishment": TertiaryRefurbishmentTest(),
            "pv_ground_based": GroundBasedPVPlantTest(),
            "wind_power_plant": WindPowerPlantTest(),
            # "solar_thermal/ecs": SolarThermal("ecs"),
            # "solar_thermal/combine": SolarThermal("combine"),
            # "heating_network": HeatingNetwork(),
            # "heating_network_mix": HeatingNetworkMix(),
            # "methanization_cogeneration": MethanizationCogeneration(),
            # "methanization_injection": MethanizationInjection(),
            # "bicycle_lanes": BicycleLanes(),
            "pv_on_roof/resid_small_roof": PVOnRoofTest("petite_toiture"),
            "pv_on_roof/serv_ind_big_roof": PVOnRoofTest("grande_toiture"),
            "pv_on_roof/shade": PVOnRoofTest("ombriere"),
            "hydro_power_plant": HydroPowerPlantTest(),
            # "wood_energy": WoodEnergy(),
            # "energy_efficiency_industry": EnergyEfficiencyIndustry(),
            # "energy_efficiency_agriculture": EnergyEfficiencyAgriculture(),
            # "emissions_reduction_industry": EmissionsReductionNonEnergyIndustry(),
            # "emissions_reduction_agriculture": EmissionsReductionNonEnergyAgriculture(),
            # "heating_conversion_resid": HeatingConversionResidential(),
            # "heating_conversion_services": HeatingConversionServices(),
            # "mobility_commuting_time_reduction": CommutingTimeReduction(),
            # "mobility_carpooling_awareness": CarPoolingAwareness(),
            # "mobility_alternative_motor_public_tra": AlternativeMotorizationPublicTransports(),
        }

    def get_mocked_inputs_by_action(self, mocked_inputs, actions_keys):
        all_mocked_inputs = {}
        for action_key in actions_keys:
            if action_key in self.actions:
                all_mocked_inputs = {
                    **all_mocked_inputs,
                    **self.actions[action_key].get_mocked_inputs(),
                }
            else:
                raise KeyError("This action doesn't exist.")
        mocked_inputs.load_mocked_inputs(all_mocked_inputs)

    def get_action_tests(self, action_key, default=False):
        if action_key in self.actions:
            return self.actions[action_key].get_tests()
        elif default != False:
            return default
        else:
            raise KeyError("This action has no tests yet.")
