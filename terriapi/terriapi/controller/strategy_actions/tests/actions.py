# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


from terriapi.controller.strategy_actions.actions import *


class ActionTests:
    def _tests(self, *args, **kwargs):
        raise NotImplementedError("_tests should be implemented by child classes")

    def get_tests(self, *args, **kwargs):
        return self._tests(*args, **kwargs)

    def _t(self, name, parameters, *args, **kwargs):
        return ActionTest(self, name, parameters, *args, **kwargs)


class ActionTest:
    def __init__(self, action, name, parameters):
        self.name = name
        self.parameters = parameters
        self.action = action

    def __str__(self):
        return str(self.action) + "/" + self.name

    def get_mocked_inputs(self, *args, **kwargs):
        for action, values in self.parameters.items():
            for year in self.action.years():
                if str(year) not in values:
                    self.parameters[action][str(year)] = 0.0
        return self.parameters

    def to_dict(self):
        return {
            "name": self.name,
            "parameters": self.parameters,
        }


class ResidentialRefurbishmentTest(ActionTests, ResidentialRefurbishment):
    def _mocked_results(self, *args, **kwargs):
        return {}

    def _tests(self, *args, **kwargs):
        return [
            self._t(
                "Basic test",
                {
                    self.INPUT_AVG_SURFACE: {
                        "2025": 70,
                        "2030": 150,
                    },
                    self.INPUT_NB_HOUSING: {
                        "2025": 20,
                        "2030": 7500,
                    },
                },
            ),
            self._t(
                "Second longer test",
                {
                    self.INPUT_AVG_SURFACE: {
                        "2025": 100,
                        "2030": 150,
                        "2035": 150,
                        "2040": 150,
                        "2045": 150,
                        "2050": 150,
                    },
                    self.INPUT_NB_HOUSING: {
                        "2025": 5000,
                        "2030": 7500,
                        "2035": 7500,
                        "2040": 7500,
                        "2045": 7500,
                        "2050": 7500,
                    },
                },
            ),
        ]


class TertiaryRefurbishmentTest(ActionTests, TertiaryRefurbishment):
    def _tests(self, *args, **kwargs):
        return [
            self._t(
                "Basic test",
                {
                    self.INPUT_SURFACE: {
                        "2025": 1000,
                        "2030": 1500,
                    },
                    self.INPUT_REFURB_LVL_LOW: {
                        "2025": 90,
                        "2030": 10,
                    },
                    self.INPUT_REFURB_LVL_HIGH: {
                        "2025": 10,
                        "2030": 90,
                    },
                },
            )
        ]


class WindPowerPlantTest(ActionTests, WindPowerPlant):
    def _tests(self, *args, **kwargs):
        return [
            self._t(
                "Basic test",
                {
                    self.INPUT_POWER_INSTALLED: {
                        "2025": 100,
                        "2030": 150,
                    },
                },
            )
        ]


class GroundBasedPVPlantTest(ActionTests, GroundBasedPVPlant):
    def _tests(self, *args, **kwargs):
        return [
            self._t(
                "Basic test",
                {
                    self.INPUT_PEAK_POWER: {
                        "2025": 100,
                        "2030": 150,
                    },
                },
            )
        ]


class HydroPowerPlantTest(ActionTests, HydroPowerPlant):
    def _tests(self, *args, **kwargs):
        return [
            self._t(
                "Basic test",
                {
                    self.INPUT_CAPACITY: {
                        "2025": 300,
                    },
                },
            )
        ]


class PVOnRoofTest(ActionTests, PVOnRoof):
    def _tests(self, *args, **kwargs):
        return [
            self._t(
                "Basic test",
                {
                    self.INPUT_PEAKPOWER: {
                        "2025": 100,
                        "2030": 150,
                    },
                },
            )
        ]
