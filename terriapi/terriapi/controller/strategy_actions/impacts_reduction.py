# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from sanic.log import logger

from . import DataKeys, DataSet
from .impacts import Impacts
from .loaders import SingleDataLoader


class ImpactsTrim:
    def __init__(self, data, passage_table, region, zone, zone_id):
        self.data = data
        self.passage_table = passage_table
        self.region = region
        self.zone = zone
        self.zone_id = zone_id

    async def load_missing_data(self, key):
        table_name, filters = self.passage_table.get_query_params(key)

        data_loader = SingleDataLoader(self.region)
        await data_loader.load(
            table_name,
            self.zone,
            self.zone_id,
            self.passage_table.get_columns(key.table),
            filters,
        )

        self.data[key] = self.passage_table.convert_data(key.table, data_loader.data)

    def trim_impacts_to_historical_threshold(self, impacts, thresholds):
        """Retourne les données ajustées selon un seuil d'impacts à ne pas dépasser

        Par exemple si au sein d'un EPCI, la consommation énergétique est de 100 GWh
        mais que l'impact est de 200 GWh, on limite cet impact à 100 GWh et on recalcule
        la répartition dans le graphique par énergie à partir du mix énergétique de
        l'impact initial.

        Il peut alors arriver de faire tomber certains type d'énergie en-dessous de zéro ce
        qui induit un total du graphique par énergie supérieur à celui par secteur.

        Dans ce cas, il faut de nouveau procéder à un ajustement, c'est-à-dire à répercuter
        cet écart (que nous calculons dans cette fonction) dans le secteur impacté.

        Ainsi, si au moyen d'une action de rénovation résidentielle on fait tomber les EnR
        thermiques 20GWh en-dessous de zéro et les CMS de 10GWh en-dessous de zéro (ce qui
        ne peut bien-sûr pas arriver dans la réalité), on enlève 30 GWh à l'impact initial
        sur la consommation énergétique dans le secteur résidentiel.

        impacts : DataFrame
            Tableau de données pandas qui donne l'impact total à appliquer sur chaque type d'énergie
        seuil : float
            Seuil par rapport auquel ajuster les impacts précédent (les impacts ne doivent pas dépasser le seuil)
        """

        data = impacts.result
        # we sum everything cumulatively
        summed_up = (
            data.groupby(["annee", "SECTOR"])[["valeur"]]
            .sum()
            .reset_index()
            .set_index(["annee", "SECTOR"])
            .cumsum()
            .reset_index()
        )

        for sector, threshold_values in thresholds.items():
            threshold = threshold_values["total"]
            year_over_threshold = summed_up.loc[
                (summed_up.SECTOR == sector) & (summed_up["valeur"] > threshold)
            ]
            if len(year_over_threshold) == 0:
                continue

            sector_data = data.loc[data.SECTOR == sector]
            first_year_over_threshold = min(year_over_threshold["annee"])
            sector_data_first_year = sector_data.loc[
                sector_data["annee"] == first_year_over_threshold
            ]
            difference = float(
                year_over_threshold.loc[
                    (year_over_threshold["annee"] == first_year_over_threshold),
                    "valeur",
                ]
                - threshold
            )

            #  Calcul du mix énergétique des impacts (i.e proportion d'énergie que représente chaque impact)
            #  Par exemple, la consommation d'électricité représente 30 % de mes impacts, les organo-carburant 4 % etc.
            mix_last_year = (
                sector_data_first_year["valeur"]
                / sector_data_first_year["valeur"].sum()
            )

            # we replace initial impact by new value
            sector_data["valeur_corrigee"] = sector_data["valeur"]
            sector_data.loc[
                sector_data["annee"] == first_year_over_threshold, "valeur_corrigee"
            ] = mix_last_year * (sector_data_first_year["valeur"].sum() - difference)
            # 0 for all years after year overpassing threshold
            sector_data.loc[
                sector_data["annee"] > first_year_over_threshold, "valeur_corrigee"
            ] = 0.0

            # we check we have not also overpassed COMMODITY values
            check_commodities = (
                sector_data[["annee", "COMMODITY", "valeur", "valeur_corrigee"]]
                .groupby(["annee", "COMMODITY"])
                .sum()
                # we group by "COMMODITY"
                .groupby(level=-1)
                # we cumsum
                .cumsum()
                .reset_index()
            )
            # we compute difference compared to threshold values
            check_commodities = check_commodities.merge(
                threshold_values["by_commodity"].reset_index(),
                how="left",
                left_on="COMMODITY",
                right_on="COMMODITY",
            ).set_index(["annee", "COMMODITY"])
            diff_commodities = (
                check_commodities["valeur_corrigee"] - check_commodities["valeur_y"]
            )

            # if we have anything relevant (> Wh)
            if len(diff_commodities.loc[diff_commodities > 1e-6]):
                # we filter it
                diff_to_put_back_to_sectors = diff_commodities.loc[
                    diff_commodities > 1e-6
                ]

                # we apply difference to first year overpassing
                sector_data_first_year = sector_data.loc[
                    sector_data["annee"] == first_year_over_threshold
                ]
                sector_data_first_year = sector_data_first_year.set_index(
                    ["annee", "COMMODITY", "SECTOR"]
                )
                sector_data_first_year.loc[
                    :, "valeur_corrigee"
                ] -= diff_to_put_back_to_sectors

                # we remove values that went below zero (shouldn't happen)
                sector_data_first_year.loc[
                    (sector_data_first_year["valeur_corrigee"] < 0),
                    "valeur_corrigee",
                ] = 0.0

                # we merge back
                sector_data = sector_data.set_index(["annee", "COMMODITY", "SECTOR"])
                sector_data.loc[sector_data_first_year.index, "valeur_corrigee"] = (
                    sector_data_first_year["valeur_corrigee"]
                )
                sector_data = sector_data.reset_index()

            # we rename the new value column
            sector_data["valeur"] = sector_data["valeur_corrigee"]
            sector_data = sector_data[["annee", "COMMODITY", "SECTOR", "valeur"]]
            data.loc[data.SECTOR == sector] = sector_data

        return data

    async def trim_impacts(self, impact_type, dataset):
        """
        Trim a specific impact type from the given dataset to respect sectors
        thresholds (ie. current emission/consumption levels).

        Parameters
        ----------
        impact_type : Impacts
            the impact type
        dataset : DataSet
            the type of dataset

        Returns
        -------
        Impacts
            new impacts trimmed if necessary

        Raises
        ------
        ValueError
            no data was provided for current element so unable to trim
        """
        if self.impacts is None:
            return None
        new_impacts = self.impacts.get(impact_type, None)
        if new_impacts is not None:
            if (
                new_impacts.result.valeur.max() == 0
                and new_impacts.result.valeur.min() == 0
            ):
                return None
            sectors_concerned = new_impacts.result.SECTOR.unique()
            thresholds = {}
            for sector in sectors_concerned:
                key = DataKeys([dataset, sector])
                if key not in self.data:
                    await self.load_missing_data(key)
                    logger.warning(
                        f"Loading additional data was required to trim current results as action hasn't provided data for {str(key)}."
                    )
                # we compute total threshold as well as threshold by commodity
                thresholds[sector] = {
                    "total": self.data[key]["valeur"].sum(),
                    "by_commodity": self.data[key]
                    .groupby(["SECTOR", "COMMODITY"])[["valeur"]]
                    .sum(),
                }
            new_impacts = self.trim_impacts_to_historical_threshold(
                new_impacts, thresholds
            )
        return new_impacts

    async def apply(self, impacts):
        """
        Reduce impacts that go beyond real sector and energy consumption.

        Parameters
        ----------
        impacts : Impacts
            the impacts given by one specific action
        """
        self.impacts = impacts

        type_impact = Impacts.EnergyConsumption
        dataset = DataSet.CONSUMPTION
        correction_gains = await self.trim_impacts(type_impact, dataset)
        if correction_gains is not None:
            logger.info("Trimming EnergyConsumption")
            # TODO: use setter here instead of direct
            self.impacts.get(type_impact).result = correction_gains

        type_impact = Impacts.AvoidedEmissions
        dataset = DataSet.EMISSIONS
        correction_gains = await self.trim_impacts(type_impact, dataset)
        if correction_gains is not None:
            logger.info("Trimming AvoidedEmissions")
            # TODO: use setter here instead of direct
            self.impacts.get(type_impact).result = correction_gains

        return self.impacts
