# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from __future__ import annotations

import tempfile
from collections import defaultdict

import pandas as pd
from sanic.log import logger

from terriapi import controller

from . import (
    KEYS_SEPARATOR,
    ColumnPassageType,
    DataKeys,
    DataSet,
    ParamDataSet,
    PassageType,
)


class PassageTable:
    def __init__(self, region, name=""):
        self.region = region
        self.name = name
        self.data = {}
        self._conv = {}

        self.custom_filters_data = {}

    def debug(self):
        logger.warn("Current region for passage table is " + str(self.region))
        logger.warn("Tables data available are " + str(self.tables_data))
        logger.warn("Values data available are " + str(self.values_data))
        logger.warn("Columns data available are " + str(self.columns_data))

    @property
    def dataset_type(self):
        return DataSet

    @property
    def _no_confid_query(self):
        return """
        SELECT key, match, association_type, aggregation 
        FROM strategie_territoire.passage_table 
        WHERE region = $1 AND 
        (
            (association_type = 'table' AND key::text = ANY( $2 ))
            OR
            (key::text SIMILAR TO $3 AND association_type <> 'table')
        )"""

    @property
    def _confid_query(self):
        return """
        SELECT p.key, p.match, p.association_type, p.aggregation,
        COALESCE(pc.is_confidential, false) as is_confidential
        FROM strategie_territoire.passage_table p
        LEFT JOIN strategie_territoire.passage_table_confidentiality pc
        ON p.key = pc.key AND p.region = pc.region
        WHERE p.region = $1 AND 
        (
            (p.association_type = 'table' AND p.key::text = ANY( $2 ))
            OR
            (p.key::text SIMILAR TO $3 AND p.association_type <> 'table')
        )
        """

    @property
    def _whole_query(self):
        return """
        SELECT p.key, p.match, p.association_type, p.aggregation,
        COALESCE(pc.is_confidential, false) as is_confidential, p.data_unit
        FROM strategie_territoire.passage_table p
        LEFT JOIN strategie_territoire.passage_table_confidentiality pc
        ON p.key = pc.key AND p.region = pc.region
        WHERE p.region = $1
        """

    @property
    def _filter_by_type_query(self):
        return self._whole_query + "AND p.association_type = ANY($2)"

    @property
    def empty(self):
        return len(self.data) == 0

    async def whole(self):
        res = await controller.fetch(self._whole_query, self.region)
        return [dict(r) for r in res]

    async def filter_by_type(self, association_type):
        if not isinstance(association_type, (tuple, list, set)):
            association_type = [association_type]
        res = await controller.fetch(
            self._filter_by_type_query, self.region, association_type
        )
        return [dict(r) for r in res]

    async def query(self, tables, with_confid=False):
        if with_confid:
            query_filters = self._confid_query
        else:
            query_filters = self._no_confid_query
        res = await controller.fetch(
            query_filters, self.region, list(tables), "(" + "|".join(tables) + ")%"
        )
        return [dict(r) for r in res]

    def filter_load_params(self, keys):
        """
        Convert a list of DataKeys to a list of tables strings to be used to retrieve
        all passage table information about these tables.

        Parameters
        ----------
        keys : list of DataKeys
            all passage keys useful

        Returns
        -------
        list
            all tables names as strings
        """
        tables = set()
        for key in keys:
            if not isinstance(key, tuple):
                tables.add(str(key))

        logger.debug("Initial keys given for passage table definition: " + str(keys))
        logger.debug("Passage table content retrieval query: " + str(tables))
        return tables

    async def load(self, keys: list | tuple, with_confid: bool = False):
        """
        Load passage table elements from a list of keys.
        Can load confidentiality elements if asked.

        Parameters
        ----------
        keys : DataKeys
            the list of keys asked
        with_confid : bool, optional
            if we want to retrieve confidential information, by default False
        """
        tables = self.filter_load_params(keys)

        res = await self.query(tables, with_confid=with_confid)

        self.data = res
        self.tables_data = {
            r["key"]: r["match"] for r in res if r["association_type"] == "table"
        }

        # if we need to handle confidentiality
        if with_confid:
            self.confid_data = {
                self.dataset_type[r["key"]]: {}
                for r in res
                if r["association_type"] == "table" and r.get("is_confidential", False)
            }

        self.columns_data = defaultdict(dict)
        self.values_data = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        self.custom_filters_data = defaultdict(dict)
        self.agg_rules = defaultdict(dict)
        for r in res:
            if r["association_type"] == "table":
                continue

            table_name = self.dataset_type[r["key"].split(KEYS_SEPARATOR)[0]]

            key = r["key"].split(KEYS_SEPARATOR)[1]
            if r["association_type"] == "column":
                column_name = ColumnPassageType.column_type(key)
                if not column_name:
                    continue
                self.columns_data[table_name][column_name] = r["match"]

                # if we need to handle confidentiality
                if with_confid and table_name in self.confid_data:
                    self.confid_data[table_name][column_name] = r.get(
                        "is_confidential", False
                    )
            elif r["association_type"] == "value":
                val_key = ColumnPassageType[key]
                # if no corresponding type
                if not val_key:
                    continue

                if val_key.is_custom_filter:
                    self.custom_filters_data[table_name][val_key] = r["match"]

                # else, we handle normal rules
                suffix = False
                if "#" in key:
                    suffix = key[key.index("#") : key.index(".")]

                    self.values_data[table_name][val_key][suffix].append(r["match"])
                else:
                    self.values_data[table_name][val_key][False].append(r["match"])
                if "aggregation" in r:
                    self.agg_rules[r["key"]][r["match"]] = r["aggregation"]

        return len(self.tables_data) > 0

    def convert_table_keys(self, keys: list[DataSet | str]) -> dict[DataSet | str, str]:
        return {k: self.convert_table_key(k) for k in keys}

    def convert_table_key(self, key: DataSet | str) -> str:
        """
        Convert a table key to its real table name.

        Parameters
        ----------
        key: DataSet|str
            a table key (ex: "DataSet.CONSUMPTION")

        Returns
        -------
        str
            the corresponding table name (ex: "conso_energetique")
        """
        if isinstance(key, str) or key.passage_type() == PassageType.TABLE:
            return self.tables_data.get(str(key), False)
        else:
            raise ValueError("Key (" + str(key) + ") is not a table")

    def convert_column_key(self, key, table_key):
        """
        Convert a table key to its real table name.

        Parameters
        ----------
        key : str
            a table key (ex: "DataSet.CONSUMPTION")

        Returns
        -------
        str
            the corresponding table name (ex: "conso_energetique")
        """
        if key.passage_type() == PassageType.TABLE:
            raise ValueError("Key (" + str(key) + ") is a table")
        if not table_key or table_key is None:
            self.debug()
            raise ValueError(
                "Table key is required for key "
                + str(key)
                + " and table "
                + str(table_key)
            )
        # as columns are stored based on their string representation,
        # we need to convert the key when we have a ColumnPassageType class
        if not isinstance(table_key, str):
            key = str(key)
        # get the column
        column_res = self.columns_data.get(table_key, {}).get(key, {})
        if len(column_res) == 0:
            raise ValueError(
                "Column not found for key " + str(key) + " and table " + str(table_key)
            )
        return column_res

    def convert_key(self, key, table_key):
        """
        Convert a key from a specific table to its real column and value.

        Parameters
        ----------
        key : str
            the key name (ex: "Sector.RESIDENTIAL")
        table_key : str
            the table name (ex: "DataSet.CONSUMPTION")

        Returns
        -------
        str
            column name (ex: secteur)
        str
            value (ex: 1)
        """
        # check incoming parameters
        if key.passage_type() == PassageType.TABLE:
            raise ValueError("Key (" + str(key) + ") is a table")
        if not table_key or table_key is None:
            self.debug()
            raise ValueError(
                "Table key is required for key "
                + str(key)
                + " and table "
                + str(table_key)
            )

        # get the value
        value_res = self.values_data.get(table_key, {}).get(key, {})
        if len(value_res) == 0:
            self.debug()
            raise ValueError(
                "Value not found for key " + str(key) + " and table " + str(table_key)
            )

        # get the column
        column_res = self.columns_data.get(table_key, {}).get(key.column_type(), {})
        if len(column_res) == 0:
            self.debug()
            raise ValueError(
                f"Column not found for key {key} ({key.column_type()}) and table {table_key}"
            )

        # returns converted elements
        return column_res, value_res

    def contains_key(self, key, table_key):
        """
        Check if a specified column or value key is in the passage table

        Parameters
        ----------
        key: ColumnPassageType|type
            the key or column enum (ex: Sector.RESIDENTIAL, Usage)
        table_key: DataSet
            the table enum (ex: DataSet.CONSUMPTION)

        Returns
        -------
        bool
        """
        if not table_key:
            self.debug()
            raise ValueError(
                f"Table key is required for key {key} and table {table_key}"
            )
        if key.passage_type() == PassageType.TABLE:
            raise ValueError("Key (" + str(key) + ") is a table")

        # Get the value
        value_res = self.values_data.get(table_key, {}).get(key, {})
        if value_res:
            return True
        # Get the column
        column_res = self.columns_data.get(table_key, {}).get(key, {})
        if column_res:
            return True
        return False

    def save_key(self, table, key, val):
        if table not in self._conv:
            self._conv[table] = {}
        self._conv[table][key] = val

    def get_query_params(self, keys):
        """
        Build a query corresponding to a set of keys

        Parameters
        ----------
        keys : DataKeys object
            the keys useful to build the query

        Returns
        -------
        str
            table name
        str
            filters applied using real columns and values
        """
        table_name = self.convert_table_key(keys.table)

        keys += list(self.custom_filters_data.get(keys.table, {}).keys())

        if not table_name:
            logger.error("Table name not found for key " + str(keys.table))
            raise ValueError("Table name not found for key " + str(keys.table))

        filters = {}
        for column in keys.non_dataset_types():
            for subkey in keys.types[column]:
                matches = self.convert_key(subkey, table_key=keys.table)
                # nothing found
                if not matches:
                    continue
                col_filter, all_matches = matches
                # we loop if we have suffixes
                for _, val_matches in all_matches.items():
                    # we loop if we have more than one matching value
                    for param in val_matches:
                        self.save_key(
                            keys.table,
                            subkey,
                            {"column": col_filter, "value": param},
                        )
                        filters[col_filter] = filters.get(col_filter, []) + [param]
        return table_name, filters

    def convert_data(
        self, table: DataSet, data: pd.DataFrame, keep_duplicates: bool = False
    ) -> pd.DataFrame:
        """
        Convert data from database to a DataKeys object

        Parameters
        ----------
        data : list
            the data to convert
        table : DataKeys object
            the keys useful to build the query

        Returns
        -------
        DataKeys object
            the converted data
        """
        # columns to replace
        for val_key, val_data in self.values_data.get(table, {}).items():
            # if we are in simple case, we simulate complex case
            if not isinstance(val_data, dict):
                val_data = {False: val_data}

            for suffix, val_matches in val_data.items():
                if val_key.is_custom_filter:
                    continue
                # col name
                new_col_key = val_key.column_type()
                if suffix:
                    new_col_key += suffix
                # col real name in the table
                col_match = self.columns_data.get(table, {}).get(new_col_key)
                # if we don't have the column inside the table, we don't need to convert it
                if col_match not in data.columns:
                    continue
                # new column name
                if new_col_key not in data.columns:
                    data[new_col_key] = ""
                if data[col_match].dtype != "object":
                    data[col_match] = data[col_match].astype(str)
                # we replace all values
                for val_match in val_matches:
                    matching_rows = data[col_match] == val_match

                    # if we have potential multiple outputs and we want to keep them
                    # for example:
                    #       * we have two hydro power outputs, LOW and HIGH
                    #       * but local region only has one category for hydro power
                    #       * we don't want to lose one of both LOW/HIGH values
                    if keep_duplicates:
                        rows = data.loc[
                            matching_rows
                            & (data.loc[matching_rows, new_col_key] != ""),
                            :,
                        ]
                        rows[new_col_key] = val_key
                        data = pd.concat([data, rows], ignore_index=True)
                        matching_rows = data[col_match] == val_match

                    data.loc[
                        matching_rows & (data.loc[matching_rows, new_col_key] == ""),
                        new_col_key,
                    ] = val_key

        # get not na data
        return data

    def get_dataframe(self, table_key: DataSet) -> pd.DataFrame:
        """
        Returns a dataframe of all value keys for a specified table.
        If there are several columns, they will be concatenated.

        Parameters
        ----------
        table_key : DataSet
            The table enum (ex: DataSet.CONSUMPTION)
        """
        data = pd.DataFrame()
        for val_key, val_data in self.values_data.get(table_key, {}).items():
            if not isinstance(val_data, dict):
                val_data = {False: val_data}
            for suffix, val_matches in val_data.items():
                if val_key.is_custom_filter:
                    continue
                # col name
                new_col_key = val_key.column_type()
                if suffix:
                    new_col_key += suffix
                # col real name in the table
                col_match = self.columns_data.get(table_key, {}).get(new_col_key)
                # we replace all values
                for val_match in val_matches:
                    data = pd.concat(
                        [
                            data,
                            pd.DataFrame(
                                {new_col_key: val_key, col_match: val_match}, index=[0]
                            ),
                        ],
                        ignore_index=True,
                    )
        return data

    def fill_unknown_val_in_columns(self, table, data, column):
        """
        Fill a specific column with a data key if the column is empty, ie. if
        no association has been made in passage table for some original values.

        Parameters
        ----------
        table : DataSet
            table considered
        data : pd.DataFrame
            data to alter
        column : ColumnType or str
            the column name

        Returns
        -------
        bool
            whether an edit has been made
        """
        new_col = str(column)
        if new_col not in data.columns:
            return False
        if "" in data[new_col].unique():
            col_match = self.columns_data.get(table, {}).get(new_col)
            data.loc[data[new_col] == "", new_col] = (
                "__MISSING DATA FOR #" + data[col_match] + "__"
            )

        return True

    def get_columns(self, table_key, only_confid=False):
        """
        Get the columns of a table

        Parameters
        ----------
        table_key : str
            the table key

        Returns
        -------
        list
            the columns of the table
        """
        if only_confid:
            # we use the keys to check that no confidentiality is enabled
            return [
                col
                for key, col in self.columns_data.get(table_key, {}).items()
                if self.confid_data.get(table_key, {}).get(key, False)
            ]
        else:
            return self.columns_data.get(table_key, {}).values()

    def get_custom_real_conditions(self, table: DataSet) -> dict[str, str]:
        custom_filters = self.custom_filters_data.get(table, {})
        table_columns_data = self.columns_data.get(table, {})
        final_custom_filters = {
            table_columns_data[key_col.column_type()]: match
            for key_col, match in custom_filters.items()
            if key_col.column_type() in table_columns_data
        }
        return final_custom_filters

    def revert_impacts(self, table_key, data):
        """
        Reconvert impacts from general data to regional specific names

        Parameters
        ----------
        table_key : DataKeys object
            the table key
        data : dict of DataFrames or DataFrame
            the data to convert
        """
        # we handle the case of a dict => recursively, we parse all elements
        if isinstance(data, dict):
            output = {}
            for k, val in data.items():
                if isinstance(val, pd.DataFrame):
                    output[k] = self.revert_impacts(table_key, val)
            return output

        # columns to replace
        for val_key, val_data in self.values_data.get(table_key, {}).items():
            # if we are in simple case, we simulate complex case
            if not isinstance(val_data, dict):
                val_data = {False: val_data}

            for suffix, val_matches in val_data.items():
                if val_key.is_custom_filter:
                    continue
                # col name
                old_col_key = val_key.column_type()
                if suffix:
                    old_col_key += suffix
                # col real name in the output
                col_match = self.columns_data.get(table_key, {}).get(old_col_key)
                # if we don't have the column inside the table, we don't need to convert it
                if old_col_key not in data.columns:
                    continue
                # new column name
                if col_match not in data.columns:
                    data[col_match] = ""
                if data[old_col_key].dtype != "object":
                    data[old_col_key] = data[old_col_key].astype(str)
                if len(val_matches) > 1:
                    if f"{table_key}/{val_key}" in self.agg_rules:
                        aggregation_rules = self.agg_rules[f"{table_key}/{val_key}"]
                        priority_vals = [
                            k for k, v in aggregation_rules.items() if v == "priority"
                        ]
                        if len(priority_vals) == 0:
                            logger.error(
                                f"Need at least one priority value for key {val_key} ({table_key}) for reverting impacts"
                            )
                            # we need at least one value => we take original data
                            priority_vals = val_matches

                        if len(priority_vals) > 1:
                            logger.error(
                                f"Cannot handle more than one priority value for key {val_key} ({table_key}) for reverting impacts"
                            )
                        val_matches = priority_vals
                        logger.debug(
                            f"Apply priority for key {val_key} ({table_key}) with {val_matches[0]} for reverting impacts"
                        )
                    else:
                        logger.error(
                            f"Need at least one priority value for key {val_key} ({table_key}) for reverting impacts. Will randomly use one corresponding key."
                        )
                val_match = val_matches[0]
                data.loc[data[old_col_key] == val_key, col_match] = val_match
        # get not na data
        return data

    async def update_from_csv(self, filename):
        """Update passage table from csv file.

        Parameters
        ----------
        filename : str
            CSV file path
        """
        df = pd.read_csv(filename, sep=";")
        if set(df.columns) != {
            "key",
            "match",
            "association_type",
            "is_confidential",
            "aggregation",
            "data_unit",
        }:
            raise ValueError(
                "Les colonnes fournies doivent être key, match, association_type, aggregation, data_unit et is_confidential."
            )
        # we add the region column
        df["region"] = self.region

        # we create both passage table csv, one with confid for passage_table_confidentiality
        df_with_confid = df.loc[
            df["association_type"].isin(("table", "column")),
            ["region", "key", "is_confidential"],
        ]
        # and one without for passage_table
        df_without_confid = df[
            ["region", "key", "match", "association_type", "aggregation", "data_unit"]
        ]

        # we create a temporary file
        with tempfile.NamedTemporaryFile(mode="w+b") as fp:
            async with controller.db.acquire() as conn:
                async with conn.transaction():
                    # we upload main passage_table
                    df_without_confid.to_csv(fp.name, sep=";", header=True, index=False)
                    await conn.fetch(
                        f"""DELETE FROM strategie_territoire.passage_table
                        WHERE region = $1""",
                        self.region,
                    )
                    await conn.copy_to_table(
                        "passage_table",
                        source=fp.name,
                        schema_name="strategie_territoire",
                        header=True,
                        format="csv",
                        delimiter=";",
                        columns=list(df_without_confid.columns),
                    )
                    # we upload confid passage table
                    df_with_confid.to_csv(fp.name, sep=";", header=True, index=False)
                    await conn.fetch(
                        f"""DELETE FROM strategie_territoire.passage_table_confidentiality
                        WHERE region = $1""",
                        self.region,
                    )
                    await conn.copy_to_table(
                        "passage_table_confidentiality",
                        source=fp.name,
                        schema_name="strategie_territoire",
                        header=True,
                        format="csv",
                        delimiter=";",
                        columns=list(df_with_confid.columns),
                    )


class ParamPassageTable(PassageTable):
    @property
    def dataset_type(self):
        return ParamDataSet

    async def query(self, table_key, with_confid=False):
        query_filters = """
        SELECT key, match, association_type
        FROM strategie_territoire.passage_table 
        WHERE region = $1 AND 
        (
            (association_type = 'table' AND key::text = $2)
            OR
            (key::text SIMILAR TO $3 AND association_type <> 'table')
        )"""
        res = await controller.fetch(
            query_filters, self.region, str(table_key), str(table_key) + "%"
        )
        return [dict(r) for r in res]

    def filter_load_params(self, table_key):
        logger.debug(
            "Initial keys given for parameters passage table definition: "
            + str(table_key)
        )

        return table_key
