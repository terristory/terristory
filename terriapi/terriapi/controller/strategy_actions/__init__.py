# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import enum

import pandas as pd
from sanic.log import logger

from .constants import *
from .impacts import *

KEYS_SEPARATOR = "/"


class RequisiteError(BaseException):
    pass


class Requisite:
    class Types(enum.Enum):
        """
        Differentiating four external elements required to compute actions results:
        * CONFIG: configuration parameters, related to current region
        * DATA: variable data (e.g. consumption data), usually stored in database
        * PARAM: static values, usually stored in database
        * INPUTS: inputs, given by the user and compared with default values
        * INPUT_PARAM: inputs, given by the user and compared with default values
        """

        CONFIG = 0
        DATA = 1
        PARAM = 2
        INPUT = 3
        INPUT_ADV_PARAM = 4
        INPUT_ECO_PARAM = 5
        INPUT_YEAR_PARAM = 6
        INPUT_RAW_ADV_PARAM = 7  # handle case without any sub category

        def __str__(self):
            if self.name == "CONFIG":
                return "Configuration"
            elif self.name == "DATA":
                return "Données"
            elif self.name == "PARAM":
                return "Paramètres statiques"
            elif self.name == "INPUT":
                return "Informations entrées par l'utilisateur"
            elif self.name == "INPUT_ADV_PARAM":
                return "Paramètres avancés entrés par l'utilisateur"
            elif self.name == "INPUT_ECO_PARAM":
                return "Paramètres économiques entrés par l'utilisateur"
            elif self.name == "INPUT_YEAR_PARAM":
                return "Paramètres annualisés entrés par l'utilisateur"
            elif self.name == "INPUT_RAW_ADV_PARAM":
                return "Paramètres avancés 'bruts' entrés par l'utilisateur"
            else:
                raise NotImplementedError(
                    "This type of requisite has no string association"
                )

    def __init__(self, name, key_to_test, r_type, behavior_when_missing, **kwargs):
        # TODO: clarify current name vs key_to_test roles
        # TODO: add function giving final key => either name or key_to_test depending on requisite type?
        self.name = name
        self.key_to_test = key_to_test
        self.r_type = r_type
        self.behavior_when_missing = behavior_when_missing

        self.passage_table = kwargs.get("passage_table", False)
        self.computation_function = kwargs.get("computation_function", False)

    @property
    def is_optional(self):
        return (
            self.behavior_when_missing.b_type == BehaviorWhenMissing.Types.DEFAULT_VALUE
        )

    def __str__(self):
        return "Dépendance " + self.name + " (" + str(self.key_to_test) + ")"

    def input_key(self):
        if self.r_type in (
            Requisite.Types.INPUT_ADV_PARAM,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        ):
            return self.name
        return self.key_to_test

    def final_key(self):
        if self.r_type in (
            Requisite.Types.INPUT_ADV_PARAM,
            Requisite.Types.INPUT_RAW_ADV_PARAM,
        ):
            return self.key_to_test
        return self.key_to_test

    def check(self, array, ref_array):
        """
        Fill array at self.key_to_test key with data from ref_array at self.key_to_test key
        if available or with default value if not. If current Requisite doesn't
        allow default value, will fail.

        Parameters
        ----------
        array : dict
            future array to fill
        ref_array : dict
            reference array with loaded data

        Raises
        ------
        RequisiteError
            when missing requisite (was supposed to have data and was not allowed
            to use default value)
        """
        if self.input_key() not in ref_array:
            if (
                self.behavior_when_missing.b_type
                == BehaviorWhenMissing.Types.DEFAULT_VALUE
            ):
                array[self.final_key()] = self.behavior_when_missing.default_value
            else:
                raise RequisiteError("Missing requisite: " + str(self))
        else:
            found_data = ref_array[self.input_key()]
            # we check we have anything worth using
            if isinstance(found_data, (pd.DataFrame, list, dict)):
                if len(found_data) == 0:
                    # if we have a default, we send the default otherwise
                    if (
                        self.behavior_when_missing.b_type
                        == BehaviorWhenMissing.Types.DEFAULT_VALUE
                    ):
                        array[self.final_key()] = (
                            self.behavior_when_missing.default_value
                        )
                        return
                    # or we raise an error because we need some data
                    else:
                        raise RequisiteError("Empty requisite: " + str(self))
            logger.debug("\t Loaded " + str(self) + " in array.")
            array[self.final_key()] = found_data


class DataRequisite(Requisite):
    def __init__(
        self,
        key_name,
        behavior_when_missing,
        *args,
        **kwargs,
    ):
        self.init_keys(args)
        self.behavior_when_missing = behavior_when_missing
        self.r_type = Requisite.Types.DATA
        self.name = key_name

        self.computation_function = False

    def init_keys(self, keys):
        self.keys = DataKeys(keys)
        if DataSet not in self.keys.types:
            raise ValueError("No dataset provided")
        if len(self.keys.types[DataSet]) > 1:
            raise ValueError(
                "Too many datasets provided (only one allowed): "
                + str(self.keys.types[DataSet])
            )
        self.key_to_test = self.keys

    def check(self, array, ref_array):
        """
        Fill array at self.name key with data from ref_array at self.key_to_test key
        if available or with default value if not. If current Requisite doesn't
        allow default value, will fail.

        Parameters
        ----------
        array : dict
            future array to fill
        ref_array : dict
            reference array with loaded data

        Raises
        ------
        RequisiteError
            when missing requisite (was supposed to have data and was not allowed
            to use default value)
        """
        # TODO: check that other data that include our data are not already
        # loaded
        if str(self.key_to_test) not in ref_array:
            if (
                self.behavior_when_missing.b_type
                == BehaviorWhenMissing.Types.DEFAULT_VALUE
            ):
                array[self.name] = self.behavior_when_missing.default_value
            else:
                raise RequisiteError("Missing requisite: " + str(self))
        else:
            logger.debug(
                "\t Loaded "
                + str(self)
                + " in array at key "
                + self.name
                + " (from "
                + str(self.key_to_test)
                + ")."
            )
            array[self.name] = ref_array[str(self.key_to_test)]

    def get_passage_keys(self):
        return [self.keys.table] + [
            (self.keys.table, _ty)
            for t in self.keys.non_dataset_types()
            for _ty in self.keys.types.get(t, [])
        ]


# TODO: add yearly requisite for some inputs?
class YearlyInput(Requisite):
    pass


class DataKeys:
    def __init__(self, keys):
        self.types = {}
        for k in keys:
            self.types[type(k)] = self.types.get(type(k), set())
            self.types[type(k)].add(k)
        if DataSet not in self.types:
            raise ValueError("No dataset provided for keys " + str(keys))
        self.table = list(self.types[DataSet])[0]
        self.columns = {
            type_key: self.parse_column(self.types[type_key])
            for type_key in self.non_dataset_types()
        }
        self.keys = list(keys)

    def __str__(self):
        key = str(self.table)
        for type_key in self.columns:
            key += KEYS_SEPARATOR + "-".join(
                map(lambda x: str(x), self.columns[type_key]["values"])
            )
        return key

    def __hash__(self):
        # used to be able to put DataKeys in a set
        # the string should be enough to determine unicity
        return hash(str(self))

    def __eq__(self, other):
        # used to be able to put DataKeys in a set
        # the string should be enough to determine unicity
        return str(self) == str(other)

    def parse_column(self, keys):
        values = list(keys)
        # we take the first value for current column
        key = str(values[0]).split(".")[0]
        return {
            "key": key,
            "values": values,
        }

    def non_dataset_types(self):
        return (k for k in self.types if k != DataSet)

    def merge(self, other_keys, main_column):
        """
        We want to be able to merge another data keys inside current data keys.

        Parameters
        ----------
        other_keys : DataKeys
            the other data keys to merge
        """
        if other_keys.table != self.table:
            raise ValueError("Cannot merge datakeys not related to the same table.")
        # we merge only sector
        if main_column in self.columns and main_column in other_keys.columns:
            # we create new datakeys with only columns related to main column
            keys = (
                [self.table]
                + self.columns.get(main_column, {}).get("values", [])
                + other_keys.columns.get(main_column, {}).get("values", [])
            )
            return DataKeys(keys)
        return self

    def with_only(self, col_filter):
        """
        Get subset of current keys with only column given and all other keys.
        For example, if we give Sector, we will received two datakeys, one with
        only current datakeys regarding Sector and another will all other datakeys.

        Parameters
        ----------
        col_filter : PassageType
            column used to separate

        Returns
        -------
        tuple
            two datakeys, one with relevant keys and another with the rest
        """
        keys = [self.table] + self.columns.get(col_filter, {}).get("values", [])
        other_keys = [self.table] + [
            col
            for k, cols in self.columns.items()
            if k != col_filter
            for col in cols.get("values", [])
        ]
        datakey = DataKeys(keys)
        other_datakey = DataKeys(other_keys)
        return datakey, other_datakey

    def __add__(self, other):
        if not isinstance(other, (tuple, list)):
            raise ValueError(
                "Cannot add DataKeys with format " + str(type(other)) + "."
            )
        if len(other) == 0:
            return self
        return DataKeys(self.keys + list(other))


class BehaviorWhenMissing:
    class Types(enum.Enum):
        """
        Differentiating two behaviors when a requisite is missing:
        * DEFAULT_VALUE: use a default value
        * FAIL: raise an error that can disable the action concerned
        """

        DEFAULT_VALUE = 0
        FAIL = 1

    def __init__(self, b_type, default_value=None):
        self.b_type = b_type
        self.default_value = default_value


class AbstractAction:
    PARAM_FEGES = "global_facteur_emission"
    PARAM_FEGES_ATMO = "facteur_emission_polluants_ges_conso_atmo"
    PARAM_PRICE = "user_global_prix_energie"
    PARAM_TRANSPORT_CONSUMPTION = "global_conso_transport_commun"

    # TODO: handle versions
    # TODO: handle pre_execute hooks (e.g. to check if any action must be performed
    # because data are outdated)

    def __init__(self, name, action_number, **kwargs):
        self.name = name
        self.action_number = action_number
        # settings related to general behavior of the action
        self.settings = kwargs.get("settings", {})

        # requisites are used to load data and params
        self.requisites = kwargs.get("requisites", [])

        # following variables contain external parameters used to compute action's results
        self.config = kwargs.get("config", {})
        self.data = kwargs.get("data", {})
        self.params = kwargs.get("params", {})
        self.inputs = kwargs.get("inputs", {})
        self.optional_inputs = {}
        self.input_params = kwargs.get("input_params", {})

        # enabled years
        self.enabled_years = set()

        # max years for data
        self.max_years = {}

        # if disabled, won't run
        self.disabled = False

        # didn't fail yet
        self.failed = False

        # we try to add self registered requisites
        try:
            self.add_requisites()
        except ValueError as e:
            logger.error(
                "Disabling action "
                + self.name
                + " due to error while adding requisites: "
                + str(e)
            )
            self.disabled = True

    def __str__(self):
        return "Action #" + self.action_number + " " + self.name

    def add_requisites(self):
        raise NotImplementedError("Method add_requisites not implemented")

    def set_settings(self, settings):
        self.settings = settings

    def add_mandatory_requisite(self, *args, **kwargs):
        behavior = BehaviorWhenMissing(BehaviorWhenMissing.Types.FAIL)
        kwargs["behavior_when_missing"] = behavior
        self.requisites.append(Requisite(*args, **kwargs))

    def add_data_requisite(self, key_name, *args, **kwargs):
        if "default" in kwargs:
            # TODO: be able to add default value for some datasets
            type_behavior = BehaviorWhenMissing.Types.DEFAULT_VALUE
            behavior = BehaviorWhenMissing(type_behavior, kwargs["default"])
        else:
            behavior = BehaviorWhenMissing(BehaviorWhenMissing.Types.FAIL)
        self.requisites.append(DataRequisite(key_name, behavior, *args, **kwargs))

    # TODO: do optional requisites exist without a default value?
    def add_optional_requisite(self, default_value, *args, **kwargs):
        behavior = BehaviorWhenMissing(
            BehaviorWhenMissing.Types.DEFAULT_VALUE, default_value
        )
        kwargs["behavior_when_missing"] = behavior
        self.requisites.append(Requisite(*args, **kwargs))

    def check_requisites(self, config, data, params, inputs, max_years):
        for requisite in self.requisites:
            logger.debug(
                f"Checking requisite ({requisite.r_type}): {requisite.name} on action: {self.name}"
            )
            if requisite.r_type == Requisite.Types.CONFIG:
                requisite.check(self.config, config)
            elif requisite.r_type == Requisite.Types.DATA:
                requisite.check(self.data, data)
                if requisite.key_to_test in max_years:
                    self.max_years[requisite.name] = max_years[requisite.key_to_test]
            elif requisite.r_type == Requisite.Types.PARAM:
                requisite.check(self.params, params)
            elif requisite.r_type == Requisite.Types.INPUT_ADV_PARAM:
                adv_inputs = inputs.get(self.action_number, {})
                if adv_inputs:
                    adv_inputs = (
                        adv_inputs.get("advanced", {})
                        .get("advanced", {})
                        .get(self.action_number, {})
                    )
                    requisite.check(
                        self.input_params,
                        adv_inputs,
                    )
                else:
                    raise RequisiteError("Missing requisite: " + str(requisite))
            elif requisite.r_type == Requisite.Types.INPUT_RAW_ADV_PARAM:
                adv_inputs = inputs.get(self.action_number, {})
                if adv_inputs:
                    adv_inputs = (
                        adv_inputs.get("advanced", {})
                        .get("advanced", {})
                        .get(self.action_number, {})
                        .get("parametres_avances", {})
                    )
                    requisite.check(
                        self.input_params,
                        adv_inputs,
                    )
                else:
                    raise RequisiteError("Missing requisite: " + str(requisite))
            elif requisite.r_type == Requisite.Types.INPUT_ECO_PARAM:
                adv_inputs = inputs.get(self.action_number, {})
                if adv_inputs:
                    adv_inputs = adv_inputs.get("advanced", {}).get("economique", {})
                    requisite.check(
                        self.input_params,
                        adv_inputs,
                    )
                else:
                    raise RequisiteError("Missing requisite: " + str(requisite))
            elif requisite.r_type == Requisite.Types.INPUT_YEAR_PARAM:
                adv_inputs = inputs.get(self.action_number, {})
                if adv_inputs:
                    adv_inputs = (
                        adv_inputs.get("advanced", {})
                        .get("yearAdvancedParams", {})
                        .get(self.action_number, {})
                    )
                    requisite.check(
                        self.input_params,
                        adv_inputs,
                    )
                else:
                    raise RequisiteError("Missing requisite: " + str(requisite))
        return True

    def _fill_in_optional_input(self, def_val):
        return {str(year): def_val for year in self.years()}

    def check_inputs(self, inputs):
        for requisite in self.requisites:
            if requisite.r_type == Requisite.Types.INPUT:
                logger.warning(
                    f"Checking input ({requisite.r_type}): {requisite.name} on action: {self.name}"
                )
                inputs_params = inputs.get(self.action_number, {})
                if not inputs_params:
                    raise RequisiteError("Missing requisite: " + str(requisite))

                subactions = inputs_params.get("subactions", {})
                # if input is optional, we fill in the inputs with default values
                if requisite.is_optional and requisite.name not in subactions:
                    subactions[requisite.name] = self._fill_in_optional_input(
                        requisite.behavior_when_missing.default_value
                    )

                requisite.check(self.inputs, subactions)
        return True

    def get_required_data_keys(self):
        if self.disabled:
            return []
        keys = [
            requisite.get_passage_keys()
            for requisite in self.requisites
            if requisite.r_type == Requisite.Types.DATA
        ]
        return list(set([k for l in keys for k in l]))

    def _get_required(self, requisite_type):
        if self.disabled:
            return []
        return [
            requisite
            for requisite in self.requisites
            if requisite.r_type == requisite_type
        ]

    def get_required_params(self):
        return self._get_required(Requisite.Types.PARAM)

    def get_required_data(self):
        return self._get_required(Requisite.Types.DATA)

    async def _pre_execute(self, *args, **kwargs):
        pass

    async def pre_execute(self, *args, **kwargs):
        await self._pre_execute(*args, **kwargs)

    async def _execute(self, year, *args, **kwargs):
        raise NotImplementedError("Method _execute not implemented")

    def years(self):
        if not self.settings or not self.settings.get("first_year", False):
            logger.error(
                "Disabling action " + self.name + " due to missing first_year settings"
            )
            self.disabled = True
            raise ValueError("Missing first_year settings")

        first_year = int(self.settings.get("first_year"))
        last_year = int(self.settings.get("last_year", 2050))
        return range(first_year, last_year + 1)

    def check_inputs_for_year(self, year):
        for p in self.inputs:
            if str(year) not in self.inputs[p]:
                logger.debug(
                    "Skipping "
                    + p
                    + " for year "
                    + str(year)
                    + " as no input has been specified"
                )
                return False
        return True

    async def execute(self, *args, **kwargs):
        impacts = Impacts()
        for year in self.years():
            logger.debug("### HANDLE YEAR ### " + str(year))
            # if some inputs are missing, we skip
            # TODO: is it the right behaviour?
            if not self.check_inputs_for_year(year):
                continue
            _res = await self._execute(str(year), *args, **kwargs)
            impacts += _res
            self.enabled_years.add(year)
        return impacts

    def _mocked_inputs(self, *args, **kwargs):
        raise NotImplementedError(
            f"Mocked inputs must be implemented in action {self.name} to be able to test it."
        )
