# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from abc import ABCMeta, abstractmethod

import pandas as pd
from sanic.log import logger


class AbstractImpact(metaclass=ABCMeta):
    @property
    @abstractmethod
    def key(self):
        raise NotImplementedError

    @key.setter
    @abstractmethod
    def key(self, val):
        return self.key + val

    @property
    @abstractmethod
    def constraints(self):
        raise NotImplementedError

    @constraints.setter
    @abstractmethod
    def constraints(self, val):
        return self.constraints + val

    def __init__(self, result, year=None, **kwargs):
        self.result = self.post_load(self._load(result, year), **kwargs)

    def __len__(self):
        return len(self.result) if isinstance(self.result, pd.DataFrame) else 0

    def __iter__(self):
        if isinstance(self.result, pd.DataFrame):
            dict_results = self.result.to_dict("index")
            for k, v in dict_results.items():
                yield k, v
        return

    def get_default(self):
        return {"valeur": 0.0}

    def get_constraints(self):
        """
        Yields all internal constraints. Can be overriden when other columns are
        required.
        """
        for constraint in self.constraints:
            yield constraint

    def _load(self, result, year=None):
        if result is None:
            if year is not None:
                return pd.DataFrame(
                    {**{"annee": int(year)}, **self.get_default()}, index=[0]
                )
            return pd.DataFrame({"valeur": 0.0}, index=[0])
        if isinstance(result, pd.DataFrame):
            if year is not None and "annee" not in result.columns:
                result["annee"] = int(year)
            return result
        elif isinstance(result, (tuple, list)):
            df = pd.DataFrame(result)
            df.columns = ["valeur"]
            if year is not None:
                df["annee"] = int(year)
            return df
        elif isinstance(result, (pd.Series, dict)):
            # we prepare the table so it can be used by the Impacts class
            # thus, it requires that the main column is named "valeur"
            # and that there is a column named "COMMODITY" containing the previous indexes
            index = result.index if isinstance(result, pd.Series) else result.keys()
            values = result if isinstance(result, pd.Series) else result.values()
            df = pd.DataFrame(values, index=index)
            df.columns = ["valeur"]
            if year is not None:
                df["annee"] = int(year)
            return df
        else:
            df = pd.DataFrame({"valeur": result}, index=[0])
            if year is not None:
                df["annee"] = int(year)
            return df

    def post_load(self, df, **kwargs):
        return df

    def create_self(self, *args, **kwargs):
        """
        Used to create a new impact value from summed values. Can be overriden
        when necessary (ie., constructor requires more than just result values).
        """
        return self.__class__(*args, **kwargs)

    def __add__(self, other_impact):
        if not isinstance(other_impact, self.__class__):
            raise ValueError(
                "Cannot add impacts of different types (" + str(self) + ")"
            )
        if (
            "annee" not in self.result.columns
            or "annee" not in other_impact.result.columns
        ):
            raise ValueError("Year is required for current impact (" + str(self) + ")")
        df = pd.concat([self.result, other_impact.result], axis=0)
        cols = ["annee"]
        for constraint in self.get_constraints():
            if constraint in df.columns:
                cols.append(constraint)
        # we need to handle specifically by_action case
        by_action = issubclass(self.__class__, AbstractByActionImpact)
        # if we have at least on year in common, we must handle differently
        if (
            by_action
            and len(
                set(self.result["annee"]).intersection(other_impact.result["annee"])
            )
            != 0
        ):
            # if we don't have any common action, we can just merge by concatenating
            if (
                len(
                    set(self.result["__ACTION__"]).intersection(
                        other_impact.result["__ACTION__"]
                    )
                )
                == 0
            ):
                df = pd.concat([self.result, other_impact.result], axis=0)
            else:
                logger.error(
                    "Cannot handle such cases with one common action (should't happen => it would mean we have to merge for one year twice the same action)"
                )
        elif by_action:
            pass
        else:
            df = df.groupby(cols).sum().reset_index()

        return self.create_self(df)

    def get(self, key, default_value=0.0, default_columns=[]):
        if self.result is None:
            return pd.DataFrame()
        elif isinstance(key, (list, tuple)) and set(map(str, key)) <= set(
            self.result.columns
        ):
            return (
                self.result.groupby(["annee"] + list(map(str, key)))[["valeur"]]
                .sum()
                .reset_index()
            )
        elif str(key) in self.result.columns:
            return (
                self.result.groupby(["annee", str(key)])[["valeur"]].sum().reset_index()
            )
        else:
            # we return null values for each year
            df_years = pd.DataFrame(self.result["annee"].unique())
            df_cols = pd.DataFrame(default_columns)

            df = df_years.merge(df_cols, how="cross")
            if df.empty:
                return df
            df["valeur"] = default_value
            df.columns = ["annee", str(key), "valeur"]
            return df

    def to_dict(self):
        return self.result.to_dict(orient="records")

    def __str__(self):
        if self.result is not None:
            return f"Impact <{self.key}>\n{self.result.describe()}"
        else:
            return f"Impact <{self.key}>\nEmpty results"

    def homogenize_years(self, final_year):
        """
        Add all years that have no value yet

        Parameters
        ----------
        final_year : int
            the final year of the period simulated
        """
        if len(self.result) == 0:
            logger.info("No data to homogenize here.")
            return
        min_year = self.result["annee"].min()
        missing_years = set(range(min_year, final_year + 1)).difference(
            self.result["annee"].unique()
        )
        if len(missing_years) > 0:
            # Create a dataframe with missing years at NaN values
            df_missing_years = pd.DataFrame(
                {"annee": list(missing_years)}, index=list(missing_years)
            )

            # if we have relevant columns, we need to have missing years for each combination
            constraints = list(self.get_constraints())

            # we need to use only common constraints when we have empty columns for example
            common_constraints = []
            if len(constraints) > 0:
                common_constraints = list(
                    set(constraints).intersection(self.result.columns)
                )
                relevant_combinations = (
                    self.result[common_constraints].value_counts().index
                )
            else:
                relevant_combinations = []

            df_missing_years_combined = []
            for combination in relevant_combinations:
                # we create a temporary dataframe for each combination
                _df = df_missing_years.copy()
                _df[common_constraints] = combination
                df_missing_years_combined.append(_df)

            df_missing_years = (
                pd.concat(df_missing_years_combined)
                if len(df_missing_years_combined) > 0
                else df_missing_years
            )

            # we append the missing years to the result
            result = pd.concat([self.result, df_missing_years]).sort_values("annee")
            # we replace all empty values (that were created) by 0.0
            self.result = result.fillna(0.0)


class AbstractByActionImpact(AbstractImpact):
    def post_load(self, df, action=None, **kwargs):
        if "__ACTION__" not in df.columns and action is not None:
            df["__ACTION__"] = action
        elif action is not None:
            raise NotImplementedError(
                "Can not handle case with an action specified but column __ACTION__ already filled."
            )
        return df

    def get_constraints(self):
        """
        Override normal `get_constraints` function to append __ACTION__ column.
        """
        super().get_constraints()
        yield "__ACTION__"

    def __getitem__(self, action):
        # if we have managed by action, we get specific action
        if "__ACTION__" in self.result.columns:
            filtering_val = action

            return (
                self.result[
                    self.result.apply(
                        lambda x: x["__ACTION__"] == filtering_val, axis=1
                    )
                ][
                    ["annee", "valeur"]
                ]  # we filter on right action
                .set_index("annee")["valeur"]  # we use year as index
                .to_dict()  # we convert to dict year -> value
            )
        else:
            raise NotImplementedError("Not implemented")

    def get_result_by_action(self, action):
        # if we have managed by action, we get specific action
        if "__ACTION__" in self.result.columns:
            filtering_val = action

            return self.result[
                self.result.apply(lambda x: x["__ACTION__"] == filtering_val, axis=1)
            ][
                ["annee", "valeur"]
            ]  # we filter on right action
        else:
            raise NotImplementedError("Not implemented")

    def get_all_actions_impacting(self):
        return set(self.result["__ACTION__"].unique())


class AbstractSubImpact(AbstractImpact):
    def __init__(self, key, result, year=None, **kwargs):
        self.sub_key = key
        self.key += "_" + key
        super().__init__(result, year, **kwargs)

    def create_self(self, *args, **kwargs):
        return self.__class__(self.sub_key, *args, **kwargs)


class CommoditySectorImpact(AbstractImpact):
    constraints = ["COMMODITY", "SECTOR"]

    def post_load(self, df, sector=None, commodity=None, **kwargs):
        # if we have specified a sector, we use it
        if sector is not None:
            df["SECTOR"] = sector
        # if we have specified a commodity, we use it
        if commodity is not None:
            df["COMMODITY"] = commodity
        if (
            sector is not None
            and commodity is None
            and (len(df.index) != 1 or df.index[0] != 0)
        ):
            # if we still don't have any COMMODITY column => index contains comm
            if "COMMODITY" not in df.columns:
                df.index.name = "COMMODITY"
                df = df.reset_index()
        if (
            sector is None
            and commodity is not None
            and (len(df.index) != 1 or df.index[0] != 0)
        ):
            # if we still don't have any SECTOR column => index contains sector
            if "SECTOR" not in df.columns:
                df.index.name = "SECTOR"
                df = df.reset_index()
        return df


class RenewableProdImpact(AbstractImpact):
    constraints = ["RENEWABLEPROD", "ENERGYVECTOR"]

    def post_load(self, df, prod_type=None, energy_vector=None, **kwargs):
        if prod_type is not None:
            df["RENEWABLEPROD"] = prod_type
        if energy_vector is not None:
            df["ENERGYVECTOR"] = energy_vector
        return df


class Impacts:
    class Empty(AbstractImpact):
        key = "empty_impact"
        constraints = []

        def __init__(self, *args, **kwargs):
            self.result = None

        def is_empty(self):
            return (
                self.result is None
                or self.result == ""
                or self.result == []
                or self.result == {}
                or self.result == ()
            )

    class EnergyConsumption(CommoditySectorImpact):
        key = "gains_energetiques"

    class MixEnergy(AbstractSubImpact):
        key = "mix_energetique"
        constraints = []

    class EnergyProduction(RenewableProdImpact):
        key = "energie_produite"

    class AvoidedEmissions(CommoditySectorImpact):
        key = "emissions_evitees"

    class PollutantsCOVNM(CommoditySectorImpact):
        key = "gain_polluants_covnm"

    class PollutantsNH3(CommoditySectorImpact):
        key = "gain_polluants_nh3"

    class PollutantsPM10(CommoditySectorImpact):
        key = "gain_polluants_pm10"

    class PollutantsPM25(CommoditySectorImpact):
        key = "gain_polluants_pm25"

    class PollutantsNOX(CommoditySectorImpact):
        key = "gain_polluants_nox"

    class PollutantsSOX(CommoditySectorImpact):
        key = "gain_polluants_sox"

    class EnergyBill(AbstractImpact):
        key = "baisse_fact_ener"
        constraints = []

        def to_dict(self, *args):
            self.result["valeur"] = round(self.result["valeur"], 0)
            return self.result.set_index("annee")["valeur"].to_dict(*args)

        def homogenize_years(self, final_year):
            super().homogenize_years(final_year)
            self.result["valeur"] = self.result["valeur"].cumsum()

    class TaxRevenue(AbstractImpact):
        key = "recette_fiscale"
        # TODO: use db here?
        constraints = ["commune", "epci", "departement", "region"]

        def post_load(self, df, **kwargs):
            # we handle the "dict" case => we need to transpose the element here
            # and to set it properly to correct format
            if set(self.constraints) == set(df.index):
                years = df["annee"].unique()
                # TODO: generalize here to be able to have more than one year?
                if len(years) > 1:
                    raise NotImplementedError(
                        "Not able to handle more than one year here?"
                    )
                # we transpose to have constraints as columns and we add column year
                df = df.round({"valeur": 2})
                df = df[["valeur"]].transpose()
                df["annee"] = years[0]
            # we don't have the constraints columns
            elif set(df.columns) == {"annee", "valeur"}:
                df = df.round({"valeur": 2})
                for constraint in self.constraints:
                    df[constraint] = df["valeur"]
                df = df.drop(columns="valeur")
            return df

        def get_default(self):
            return {"commune": 0.0, "epci": 0.0, "departement": 0.0, "region": 0.0}

        def to_dict(self):
            return self.result.groupby("annee").sum().to_dict(orient="index")

        def homogenize_years(self, final_year):
            super().homogenize_years(final_year)

    class ConsumptionIndex(AbstractByActionImpact):
        key = "indice_consommation"
        constraints = []

    class ConsumptionDiff(AbstractSubImpact, AbstractByActionImpact):
        key = "difference_conso"
        constraints = []

    class CO2EmissionsPerIndustry(AbstractSubImpact):
        key = "emission_co2_par_industrie"
        constraints = []

    class CO2EmissionPerSubAction(AbstractSubImpact):
        key = "emission_co2_par_sous_action"
        constraints = []

    def __init__(self, impacts=None):
        if impacts is None:
            impacts = {}
        self.impacts = impacts

    def items(self):
        return self.impacts.items()

    def get(self, key, default=None):
        if issubclass(key, AbstractImpact):
            return self.impacts.get(key.key, default)
        else:
            return self.impacts.get(key, default)

    def get_all_actions(self, key, default=None):
        if issubclass(key, AbstractByActionImpact):
            # we get unique actions for this key
            all_actions = set().union(
                *[
                    i.get_all_actions_impacting()
                    for k, i in self.impacts.items()
                    if k.startswith(key.key)
                ]
            )
            # if no action actually
            if len(all_actions) == 0:
                return None
            # for each action, we gather corresponding results
            results = {}
            for action in all_actions:
                df = pd.concat(
                    [
                        i.get_result_by_action(action)
                        for k, i in self.impacts.items()
                        if k.startswith(key.key)
                    ]
                )
                results[action] = {
                    str(y): df.loc[df["annee"] == y, "valeur"]
                    for y in df["annee"].unique()
                }

            return results
        else:
            return self.get(key, default)

    def get_all_sub(self, key, default={}):
        if issubclass(key, AbstractImpact):
            _key = key.key
        else:
            _key = key

        by_action = issubclass(key, AbstractByActionImpact)
        # if we have a by action impact
        # then we need to gather by action resulting dicts
        if by_action:
            # we get unique actions for this key
            all_actions = set().union(
                *[
                    i.get_all_actions_impacting()
                    for k, i in self.impacts.items()
                    if k.startswith(_key)
                ]
            )
            # for each action, we gather corresponding results
            results = {}
            for action in all_actions:
                results[action] = self.parse_results(
                    [
                        # we do include sub_keys (e.g., heating systems id for ConsumptionDiff)
                        i.get_result_by_action(action).assign(sub_key=i.sub_key)
                        for k, i in self.impacts.items()
                        if k.startswith(_key)
                    ]
                )
            return results
        # otherwise, we just have to group by subkeys
        else:
            results = [
                # we assign sub key to a new column
                i.result.assign(sub_key=i.sub_key)
                for k, i in self.impacts.items()
                if k.startswith(_key)
            ]
            if len(results) == 0:
                return default
            return self.parse_results(results)

    def parse_results(self, results):
        """
        Format a dict with subkeys results to have following format:

        ```
            year: {
                all subkey: ...data
            }
        ```

        Parameters
        ----------
        results : list
            list of current results, one dataframe per sub key with year, subkey and value columns

        Returns
        -------
        dict
        """
        # we format the dict to have year: {all subkey: data}
        df = pd.concat(results)
        data_yearly = {
            str(y): df.loc[df["annee"] == y, :].set_index("sub_key")["valeur"].to_dict()
            for y in df["annee"].unique()
        }
        return data_yearly

    def __add__(self, other_impacts):
        if isinstance(other_impacts, (dict, self.__class__)):
            for key, impact in other_impacts.items():
                if key not in self.impacts:
                    self.impacts[key] = impact
                else:
                    self.impacts[key] += impact
            return self
        elif isinstance(other_impacts, set):
            for impact in other_impacts:
                key = impact.key
                if key not in self.impacts:
                    self.impacts[key] = impact
                else:
                    self.impacts[key] += impact
            return self
        else:
            raise ValueError(
                "Cannot add impacts which are not stored in a set or a real class"
            )

    @staticmethod
    def impact_from_pollutant(pollutant):
        if pollutant == "covnm":
            return Impacts.PollutantsCOVNM
        elif pollutant == "nh3":
            return Impacts.PollutantsNH3
        elif pollutant == "pm10":
            return Impacts.PollutantsPM10
        elif pollutant == "pm25":
            return Impacts.PollutantsPM25
        elif pollutant == "nox":
            return Impacts.PollutantsNOX
        elif pollutant == "sox":
            return Impacts.PollutantsSOX
        else:
            raise NotImplementedError("Unknown pollutant " + str(pollutant))

    def homogenize_years(self, final_year):
        for key, impact in self.impacts.items():
            logger.info("Homogenizing %s", key)
            impact.homogenize_years(final_year)
