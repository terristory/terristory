import enum

from sanic.log import logger


class PassageType(enum.Enum):
    TABLE = "table"
    COLUMN = "column"
    VALUE = "value"


class EnumMeta(enum.EnumMeta):
    def __getitem__(self, label):
        """
        Transform a string into an enum value.

        Allow to use:
            * DataSet.XXX
            * Commodity.XXX
            * Sector.XXX
            * Usage.XXX
            * RenewableProd.XXX

        Parameters
        ----------
        label : str
            a string representing an enum value

        Returns
        -------
        enum
            an enum corresponding to the value and type requested

        Raises
        ------
        NotImplementedError
            when the string concerns an unknown type of enum
        """
        _label = label
        if "DataSet." in label:
            _label = label.split(".")[1]
        elif "." in label:
            type_object, _label = label.split(".", 1)
            # we remove any kind of indicator related to multiple column
            # (e.g. "Commodity#1.GAS" becomes "Commodity.GAS")
            if "#" in type_object:
                type_object = type_object[: type_object.index("#")]

            if type_object == "Commodity":
                return Commodity[_label]
            elif type_object == "Sector":
                return Sector[_label]
            elif type_object == "Usage":
                return Usage[_label]
            elif type_object == "RenewableProd":
                return RenewableProd[_label]
            elif type_object == "ParamCommodity":
                return ParamCommodity[_label]
            elif type_object == "VehicleType":
                return VehicleType[_label]
            elif type_object == "VehicleFuel":
                return VehicleFuel[_label]
            elif type_object == "EnergyVector":
                return EnergyVector[_label]
            elif type_object == "Filter":
                return Filter[_label]
            elif type_object == "PrincipalMode":
                return PrincipalMode[_label]
            elif type_object == "CustomFilter":
                return CustomFilter(_label)
            else:
                logger.error("Unknown type object: " + type_object)
                return False
        return super().__getitem__(_label)

    def __str__(cls):
        return cls.__name__.upper()


class ColumnPassageType(enum.Enum, metaclass=EnumMeta):
    @classmethod
    def passage_type(cls):
        return PassageType.COLUMN

    @classmethod
    def column_name(cls):
        return cls.__name__

    @classmethod
    def column_type(cls, column_name=None):
        if column_name is None:
            column_name = cls.column_name()
        # if we have a multiple column name (e.g. "Commodity#1")
        # we remove the suffixe to decide column type and then we return
        # the name with the suffixe
        suffixe = ""
        if "#" in column_name:
            suffixe = column_name[column_name.index("#") :]
            column_name = column_name[: column_name.index("#")]
        # we handle custom filter columns
        elif "." in column_name:
            suffixe = column_name[column_name.index(".") + 1 :]
            column_name = column_name[: column_name.index(".")]
        if column_name == "Commodity":
            return str(Commodity) + suffixe
        elif column_name == "Sector":
            return str(Sector) + suffixe
        elif column_name == "Usage":
            return str(Usage) + suffixe
        elif column_name == "RenewableProd":
            return str(RenewableProd) + suffixe
        elif column_name == "ParamCommodity":
            return str(ParamCommodity) + suffixe
        elif column_name == "VehicleType":
            return str(VehicleType) + suffixe
        elif column_name == "VehicleFuel":
            return str(VehicleFuel) + suffixe
        elif column_name == "EnergyVector":
            return str(EnergyVector) + suffixe
        elif column_name == "Filter":
            return str(Filter) + suffixe
        elif column_name == "PrincipalMode":
            return str(PrincipalMode) + suffixe
        elif column_name == "PrincipalMode":
            return str(PrincipalMode) + suffixe
        elif column_name == "CustomFilter":
            return "CUSTOMFILTER_" + suffixe.upper()
        else:
            logger.error("Unknown type object: " + column_name)
            return False

    def __lt__(self, other):
        return str(self) < str(other)

    @property
    def is_custom_filter(self):
        return False


class TablePassageType(enum.Enum, metaclass=EnumMeta):
    @classmethod
    def passage_type(cls):
        return PassageType.TABLE


# enum for commodities
class Commodity(ColumnPassageType):
    ALL = 999
    TOTAL = 0
    COAL = 1  # Combustibles Minéraux Solides
    ELECTRICITY = 2  # Électricité
    RENEWABLE_HEAT = 3  # ENR thermiques
    NON_ENERGETIC = 4  # Non-énergétique
    OIL = 5  # Produits pétroliers
    BIOFUELS = 6  # Organo-carburants
    GAS = 7  # Gaz
    UNKNOWN = 8  # Non identifié
    URBAN_HEAT_COOL = 9  # Chauffage et froid urbain
    WASTES = 10  # Déchets
    NON_RENEWABLE_HEAT = 11  # ENR thermiques


class Sector(ColumnPassageType):
    ALL = 999
    TOTAL = 0
    RESIDENTIAL = 1  # Résidentiel
    AGRICULTURE = 2  # Agriculture, sylviculture et aquaculture
    SERVICES = 3  # Tertiaire
    WASTE_MANAGEMENT = 4  # Gestion des déchets
    INDUSTRY_WITHOUT_ENERGY = 5  # Industrie hors branche énergie
    ROAD_TRANSPORT = 6  # Transport routier
    OTHER_TRANSPORT = 7  # Autres transports
    ENERGY_INDUSTRY = 8  # Industrie branche énergie
    UNDIFFERENCIATED = 9  # Non différencié


class Usage(ColumnPassageType):
    ALL = 999
    TOTAL = 0
    HEATING = 1  # Chauffage
    HOT_WATER = 2  # Eau Chaude Sanitaire
    COOKING = 3  # Cuisson
    SPECIFIC_ELEC = 4  # Electricité spécifique
    OTHERS = 5  # Autres usages
    COOLING = 11  # Climatisation
    PASSENGER_TRANSPORT = 12
    GOODS_TRANSPORT = 13


class RenewableProd(ColumnPassageType):
    SOLAR_THER = 2  # Production du solaire thermique
    SOLAR_PV = 3  # Production photovoltaïque
    WIND = 4  # Production éolienne
    HYDRO_LOW = 5  # Production hydroélectrique - puiss inf 4,5 MW
    BIOMASS_ELEC = 6  # Cogénération bois-biomasse : Production électrique
    BIOGAS_ELEC = 7  # Valorisation électrique du biogaz
    BIOGAS_THER = 8  # Valorisation thermique du biogaz
    WASTE_VALORIZATION_ELEC = 9
    WASTE_VALORIZATION_THER = 10
    HYDRO_HIGH = 12  # Production hydroélectrique - puiss sup 4,5 MW
    BIOGAS_INJEC = 13  # Valorisation par injection
    SOLAR_THERMODYNAMIC = 15
    GEOTHERMY_ELEC = 16  # Géothermie
    GEOTHERMY_THER = 17  # Géothermie
    HEAT_PUMP = 18
    BIOMASS_THER_COGENERATION = 19  # Cogénération bois-biomasse : Production Thermique
    BIOMASS_THER_CHAUFFERIE = 20  # Chaufferie bois-biomasse : Production Thermique
    BIOMASS_THER_DOMESTIC = 21  # Bois domestique : Valorisation Thermique
    BIOFUEL = 22
    WASTE_HEAT_VALORIZATION = 23  # Valorisation de la chaleur fatale
    AEROTHERMAL_PAC = 24  # Production nette des PAC aérothermique
    GEOTHERMAL_PAC = 14  # Production nette des PAC géothermique


class EnergyVector(ColumnPassageType):
    ELECTRICITY = 1
    HEAT = 2
    BIOMETHANE = 3


class Filter(ColumnPassageType):
    pass


class PrincipalMode(ColumnPassageType):
    TWO_WHEEL_MOTORIZED = "TWO_WHEEL_MOTORIZED"
    CAR_TRUCK_VAN = "CAR_TRUCK_VAN"
    WALK = "WALK"
    PUBLIC_TRANSPORT = "PUBLIC_TRANSPORT"
    BIKE = "BIKE"


class VehicleType(ColumnPassageType):
    PASSENGER_CARS = 1  # Cars
    TRUCKS = 2  # Trucks
    BUSES_COACHES = 3  # Buses and coaches
    LIGHT_COMM = 4  # Light commercial vehicles


class VehicleFuel(ColumnPassageType):
    DIESEL = 1  # Gazole
    GASOLINE = 2  # Essence
    GAS = 3  # Gaz
    ELEC_H2 = 4  # Electrique et hydrogène
    PLUG_HYBRID_DIESEL = 5  # Gazole hybride rechargeable
    PLUG_HYBRID_GASOLINE = 6  # Essence hybride rechargeable
    ELECTRIC = 7  # Electrique
    UNKNOWN = 8  # Inconnu


class DataSet(TablePassageType):
    CONSUMPTION = 1
    EMISSIONS = 2
    PRODUCTION = 3
    POLLUTANT_COVNM = 5
    POLLUTANT_NH3 = 6
    POLLUTANT_NOX = 7
    POLLUTANT_PM10 = 8
    POLLUTANT_PM25 = 9
    POLLUTANT_SOX = 10

    VEHICLE_FLEET = 11
    MOBILITY = 12
    ENERGY_BILL = 13
    POPULATION = 14

    @staticmethod
    def table_from_pollutant(pollutant):
        if pollutant == "covnm":
            return DataSet.POLLUTANT_COVNM
        elif pollutant == "nh3":
            return DataSet.POLLUTANT_NH3
        elif pollutant == "nox":
            return DataSet.POLLUTANT_NOX
        elif pollutant == "pm10":
            return DataSet.POLLUTANT_PM10
        elif pollutant == "pm25":
            return DataSet.POLLUTANT_PM25
        elif pollutant == "sox":
            return DataSet.POLLUTANT_SOX
        else:
            raise NotImplementedError("Unknown pollutant: " + str(pollutant))


POLLUTANTS = ["nox", "covnm", "pm10", "pm25", "nh3", "sox"]


class ParamDataSet(TablePassageType):
    GES_FACTORS = 1
    PRICES = 2
    FORCED_MIX = 3
    ATMO_GES_FACTORS = 4


# enum for commodities
class ParamCommodity(ColumnPassageType):
    ALL = 1
    TOTAL = 0
    COAL = "Charbon"  # Charbon
    ELECTRICITY = "Electricité"  # Electricité
    BIOMASS = "Biomasse"  # Biomasse
    DIESEL = "Diesel"  # Diesel
    GASOLINE = "Essence"  # Essence
    FUEL_OIL = "Fioul"  # Fioul
    NON_ROAD_DIESEL = "Gazole Non Routier"
    LPG = "Gpl"  # Gpl
    BIOFUEL = "Biocarburant"  # Biocarburant
    BIOGAS = "BioGNV"  # BioGNV
    GAS = "Gaz"  # Gaz
    CNG = "GNV"  # GNV
    HEAT = "Chaleur"  # Chaleur
    WASTE = "Déchets"  # Chaleur


class CustomFilter:
    @classmethod
    def passage_type(cls):
        return PassageType.COLUMN

    def __init__(self, key):
        if "." not in key:
            raise ValueError("Unknown type object when creating custom filter: " + key)
        self.type_object, self.label = key.split(".")
        self.key = key

    @property
    def is_custom_filter(self):
        return True

    def __str__(self):
        return self.type_object + "." + self.label

    def column_type(self):
        return "CUSTOMFILTER_" + self.type_object.upper()
