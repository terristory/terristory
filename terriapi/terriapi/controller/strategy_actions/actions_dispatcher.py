# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import asyncio
import traceback

import pandas as pd
from sanic.log import logger

from terriapi import controller
from terriapi.controller.strategy_actions import Commodity, DataSet, Sector
from terriapi.controller.suivi_trajectoire import _suivi_trajectoire_analyze

from . import RequisiteError
from .actions import *
from .constants import POLLUTANTS, DataSet
from .impacts import Impacts
from .impacts_reduction import ImpactsTrim
from .loaders import DataLoader, ParametersGroup
from .passage_table import PassageTable


class ActionsDispatcher:
    # TODO: reorganize functions to be chronological

    def __init__(self, region, zone, zone_id, limited_to_actions=None):
        self.actions = {
            "residential_refurbishment": ResidentialRefurbishment(),
            "tertiary_refurbishment": TertiaryRefurbishment(),
            "pv_ground_based": GroundBasedPVPlant(),
            "wind_power_plant": WindPowerPlant(),
            "solar_thermal/ecs": SolarThermal("ecs"),
            "solar_thermal/combine": SolarThermal("combine"),
            "heating_network": HeatingNetwork(),
            "heating_network_mix": HeatingNetworkMix(),
            "methanization_cogeneration": MethanizationCogeneration(),
            "methanization_injection": MethanizationInjection(),
            "bicycle_lanes": BicycleLanes(),
            "pv_on_roof/resid_small_roof": PVOnRoof("petite_toiture"),
            "pv_on_roof/serv_ind_big_roof": PVOnRoof("grande_toiture"),
            "pv_on_roof/shade": PVOnRoof("ombriere"),
            "hydro_power_plant": HydroPowerPlant(),
            "wood_energy": WoodEnergy(),
            "energy_efficiency_industry": EnergyEfficiencyIndustry(),
            "energy_efficiency_agriculture": EnergyEfficiencyAgriculture(),
            "emissions_reduction_industry": EmissionsReductionNonEnergyIndustry(),
            "emissions_reduction_agriculture": EmissionsReductionNonEnergyAgriculture(),
            "heating_conversion_resid": HeatingConversionResidential(),
            "heating_conversion_services": HeatingConversionServices(),
            "mobility_commuting_time_reduction": CommutingTimeReduction(),
            "mobility_carpooling_awareness": CarPoolingAwareness(),
            "mobility_alternative_motor_public_tra": AlternativeMotorizationPublicTransports(),
        }
        self.region = region
        self.zone = zone
        self.zone_id = zone_id
        self.config = {}
        self.data = {}
        if limited_to_actions is not None:
            self.actions = {
                k: v
                for k, v in self.actions.items()
                if v.action_number in limited_to_actions
            }
        # used to store max years for each data
        self.max_years = {}
        self.fixed_years = None
        self.params = {}
        self.inputs = {}
        self.passage_table = None

    def get_action_by_action_number(self, action_number):
        corresponding_actions = [
            v for v in self.actions.values() if v.action_number == action_number
        ]
        return corresponding_actions[0] if len(corresponding_actions) > 0 else None

    def load_and_check_inputs(self):
        for action in self.actions:
            if self.actions[action].disabled:
                continue
            try:
                self.actions[action].check_inputs(self.inputs)
                logger.info("Action " + action + " enabled successfully!")
            except RequisiteError as e:
                logger.warning("Action " + action + " disabled: " + str(e))
                self.actions[action].disabled = True
                continue

    def load_and_check_requisites(self):
        for action in self.actions:
            if self.actions[action].disabled:
                continue
            try:
                self.actions[action].check_requisites(
                    self.config, self.data, self.params, self.inputs, self.max_years
                )
                logger.info("Action " + action + " enabled successfully!")
            except RequisiteError as e:
                logger.warning("Action " + action + " disabled: " + str(e))
                self.actions[action].disabled = True
                continue

    def set_config(self, regional_config):
        self.config = regional_config

    def set_actions_settings(self, actions_settings):
        for action in actions_settings:
            if action["action_key"] in self.actions:
                self.actions[action["action_key"]].set_settings(action)

    def merge_inputs(self, defaults, inputs, *args, **kwargs):
        self.inputs = defaults.merge(inputs, *args, **kwargs)

    def get_required_data_keys(self):
        # use minimal data set of keys
        data_list = set((DataSet.EMISSIONS, DataSet.CONSUMPTION, DataSet.PRODUCTION))
        for action in self.actions:
            if not self.actions[action].disabled:
                data_list.update(self.actions[action].get_required_data_keys())
        return data_list

    def get_required_data(self):
        data_list = set()
        for action in self.actions:
            if not self.actions[action].disabled:
                data_list.update(self.actions[action].get_required_data())
        return data_list

    async def load_required_data(self, passage_table):
        data_list = self.get_required_data()

        # we create a dictionary containing, for each main table, all sectors
        # values that might be interesting
        data_loader = DataLoader(passage_table)
        await data_loader.load(self.region, self.zone, self.zone_id, data_list)

        self.data = data_loader.data
        self.max_years = data_loader.max_years
        return True

    async def load_required_params(self):
        params_list = set()
        for action in self.actions:
            if not self.actions[action].disabled:
                params_list.update(set(self.actions[action].get_required_params()))

        for param in params_list:
            param_group = ParametersGroup(self.region, self.zone, self.zone_id, param)
            try:
                test_is_valid = await param_group.is_valid()
                if not test_is_valid:
                    logger.warning("Invalid table name: " + param.key_to_test)
                    continue
            except ValueError as e:
                logger.warning("Parameter " + param + " disabled: " + str(e))
                continue

            await param_group.load()
            self.params[param.key_to_test] = param_group.data

    async def get_passage_table(self):
        keys = self.get_required_data_keys()
        self.passage_table = PassageTable(self.region, "Main passage table")
        await self.passage_table.load(keys)
        return self.passage_table

    async def execute_all(self):
        impacts = Impacts()
        for action in self.actions:
            if not self.actions[action].disabled:
                logger.info("Executing pre-action " + action)

                try:
                    # we execute any pre-action necessary
                    await self.actions[action].pre_execute(
                        region=self.region, zone=self.zone, zone_id=self.zone_id
                    )
                except Exception as e:
                    self.actions[action].failed = (
                        "Action " + action + " failed during pre-execution: " + str(e)
                    )
                    self.actions[action].disabled = True
                    logger.error(
                        "Action " + action + " failed during pre-execution: " + str(e)
                    )
                    print(traceback.format_exc())
                    # TODO: save error somewhere to know which action failed
                    # TODO: return the fact that the action failed
                    continue

        impacts_trimmer = ImpactsTrim(
            self.data,
            self.passage_table,
            self.region,
            self.zone,
            self.zone_id,
        )
        # full execution
        for action in self.actions:
            if not self.actions[action].disabled:
                logger.info("Executing action " + action)

                try:
                    _res = await self.actions[action].execute(
                        region=self.region, zone=self.zone, zone_id=self.zone_id
                    )
                    _res = await impacts_trimmer.apply(_res)
                    impacts += _res
                except Exception as e:
                    self.actions[action].failed = (
                        "Action " + action + " failed: " + str(e)
                    )
                    self.actions[action].disabled = True
                    logger.error("Action " + action + " failed: " + str(e))
                    logger.error(traceback.format_exc())
                    # TODO: save error somewhere to know which action failed
                    # TODO: return the fact that the action failed
                    continue
        await self.save_errors()
        return impacts

    async def get_data_history(self, passage_table, table):
        table_name = passage_table.convert_table_key(table)
        sector_column = passage_table.convert_column_key(
            Sector,
            table,
        )
        commodity_column = passage_table.convert_column_key(Commodity, table)
        custom_filters = passage_table.get_custom_real_conditions(table)

        # TODO: optimize? data already retrieved?
        history_sector, history_commodity = await asyncio.gather(
            _suivi_trajectoire_analyze(
                self.region,
                table_name,
                self.zone,
                self.zone_id,
                sector_column,
                custom_filters,
            ),
            _suivi_trajectoire_analyze(
                self.region,
                table_name,
                self.zone,
                self.zone_id,
                commodity_column,
                custom_filters,
            ),
        )
        return history_sector, history_commodity

    async def merge_outputs(self, outputs, remove_passage_data: bool = True):
        # contains relevant passage table datasets
        passage_table_datasets = {
            DataSet.PRODUCTION,
            DataSet.CONSUMPTION,
            DataSet.EMISSIONS,
        }

        res = await controller.fetch(
            """
                SELECT MIN(first_year) AS first_year, MAX(last_year) AS last_year
                FROM strategie_territoire.action_status
                WHERE region=$1
            """,
            self.region,
        )
        reference_year = res[0]["first_year"] - 1
        final_year = res[0]["last_year"]
        outputs.homogenize_years(final_year)

        energie_economisee = outputs.get(Impacts.EnergyConsumption, Impacts.Empty())
        emission_ges = outputs.get(Impacts.AvoidedEmissions, Impacts.Empty())

        pollutants_atmo = {}
        if self.config.get("enable_air_pollutants_impacts", False):
            for pollutant in POLLUTANTS:
                pollutants_atmo[pollutant] = outputs.get(
                    Impacts.impact_from_pollutant(pollutant), Impacts.Empty()
                )
                passage_table_datasets.add(DataSet.table_from_pollutant(pollutant))

        # TODO: use main passage table here?
        passage_table = PassageTable(self.region, "Results passage table")
        await passage_table.load(
            passage_table_datasets,
            with_confid=True,
        )
        if passage_table.empty:
            raise Exception("Passage table is empty")

        # pour les actions industrie : 14 et agriculture : 15
        indice_consommation = outputs.get_all_actions(Impacts.ConsumptionIndex, None)
        # pour les actions de conversion des équipements de chauffage résidentiel : 16 et tertiaire : 17
        difference_conso = outputs.get_all_sub(Impacts.ConsumptionDiff, None)
        # pour l'action de réduction des émission GES non-energitique dans l'industrie : 21
        emission_co2_par_industrie = outputs.get_all_sub(
            Impacts.CO2EmissionsPerIndustry
        )
        # pour l'action de réduction des émission GES non-energitique dans l'agriculture : 22
        emission_co2_par_sous_action = outputs.get_all_sub(
            Impacts.CO2EmissionPerSubAction
        )

        # On somme la facture énergétique venant des actions de type réduction de conso
        # et des actions de type production d'énergie renouvelable.
        energie_produite = outputs.get(Impacts.EnergyProduction, Impacts.Empty())
        mix_energetique = outputs.get(Impacts.MixEnergy, {})
        facture_energetique = outputs.get(Impacts.EnergyBill, pd.DataFrame())

        # Seules les actions de type "production d'énergie renouvelable" ont un impact sur les retombées fiscales
        retombees_fiscales = outputs.get(Impacts.TaxRevenue, pd.DataFrame())

        # Handling energy spared
        result_energie_produite = (
            await controller.suivi_trajectoire.historique_et_impact_prod(
                self.region,
                self.zone,
                self.zone_id,
                reference_year,
                final_year,
                energie_produite,
                passage_table,
                remove_passage_data,
            )
        )

        energie_economisee_finale = {}
        history_conso_sector, history_conso_commodity = await self.get_data_history(
            passage_table, DataSet.CONSUMPTION
        )
        # Handling energy spared
        if not history_conso_sector.empty and not history_conso_commodity.empty:
            energie_economisee_finale = (
                await controller.suivi_trajectoire.historique_et_impact_reduction_conso(
                    self.region,
                    self.zone,
                    self.zone_id,
                    reference_year,
                    final_year,
                    energie_economisee,
                    passage_table,
                    self.config.get("is_national"),
                    remove_passage_data,
                )
            )
        result_emission_ges = {}
        history_ges_sector, history_ges_commodity = await self.get_data_history(
            passage_table, DataSet.EMISSIONS
        )
        if not history_ges_sector.empty and not history_ges_commodity.empty:
            result_emission_ges = (
                await controller.suivi_trajectoire.historique_et_impact_emission_ges(
                    self.region,
                    self.zone,
                    self.zone_id,
                    reference_year,
                    final_year,
                    emission_ges,
                    passage_table,
                    self.config.get("is_national"),
                    remove_passage_data,
                )
            )

        result_pollutants = {}

        for pollutant in POLLUTANTS:
            impact_pollutant = pollutants_atmo.get(pollutant, False)
            if not impact_pollutant:
                result_pollutants[pollutant] = {}
                logger.warning("Skipping pollutant " + pollutant + " (no impact)")
                continue
            result_pollutants[pollutant] = (
                await controller.suivi_trajectoire.historique_et_impact_polluants_atmo(
                    self.region,
                    self.zone,
                    self.zone_id,
                    reference_year,
                    final_year,
                    impact_pollutant,
                    DataSet.table_from_pollutant(pollutant),
                    passage_table,
                    self.config.get("is_national"),
                    remove_passage_data,
                )
            )

        return {
            "energie_economisee": energie_economisee_finale,
            "energie_produite": result_energie_produite,
            "emission_ges": result_emission_ges,
            "covnm": result_pollutants["covnm"],
            "nh3": result_pollutants["nh3"],
            "nox": result_pollutants["nox"],
            "sox": result_pollutants["sox"],
            "pm10": result_pollutants["pm10"],
            "pm25": result_pollutants["pm25"],
            "facture_energetique": facture_energetique,
            "retombees_fiscales": retombees_fiscales,
            "mix_energetique": mix_energetique,
            "indice_consommation": indice_consommation,
            "difference_conso": difference_conso,
            "emission_co2_par_industrie": emission_co2_par_industrie,
            "emission_co2_par_sous_action": emission_co2_par_sous_action,
        }

    def get_failed_actions(self):
        return [
            {"action_key": action_key, "name": a.settings.get("name", a.name)}
            for action_key, a in self.actions.items()
            if a.failed
        ]

    def set_fixed_years(self, initial_years, actions_settings):
        if len(actions_settings) == 0:
            return []
        min_year = min((action["first_year"] for action in actions_settings))
        max_year = max((action["last_year"] for action in actions_settings))
        self.fixed_years = list(
            filter(lambda year: min_year <= int(year) <= max_year, initial_years)
        )
        return self.fixed_years

    async def save_errors(self):
        for action_key in self.actions:
            action = self.actions[action_key]
            if action.failed:
                await controller.execute(
                    """INSERT INTO strategie_territoire.action_errors 
                (region, action_number, action_key, zone_type, zone_id, error_message)
                VALUES($1, $2, $3, $4, $5, $6)""",
                    self.region,
                    action.action_number,
                    action_key,
                    self.zone,
                    self.zone_id,
                    action.failed,
                )
