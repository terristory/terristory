# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd
from sanic.log import logger
from slugify import slugify


class DefaultInputsLoader:
    """
    Default inputs values for subactions and advanced params (mostly).
    """

    def __init__(
        self,
        region,
        zone,
        zone_id,
    ):
        self.region = region
        self.zone = zone
        self.zone_id = zone_id

    def set_defaults(
        self,
        actions,
        subactions,
        advanced_params,
    ):
        """
        Save default values for actions enabled, subactions and advanced params
        inside current object.

        Parameters
        ----------
        actions : dict
            the default values related to actions from database
        subactions : dict
            the default values related to subactions from database
        advanced_params : dict
            the default values related to advanced params from database
        """
        self.actions = actions
        self.subactions = subactions
        self.advanced_params = advanced_params

    def merge(self, inputs, mocked=False, *args, **kwargs):
        """
        Merge default values with user inputs.

        Parameters
        ----------
        inputs : InputsLoader
            the inputs from user

        Returns
        -------
        dict
            merged subactions and advanced params values for each action
        """
        actions = {}
        for action_data in self.actions:
            action_number = action_data["numero"]
            logger.debug("Handling inputs for action " + action_number)
            if not mocked and not inputs.is_action_filled(action_number):
                logger.debug("\t Action disabled")
                actions[action_number] = False
                continue
            logger.debug("\t Merge inputs and defaults")
            def_subactions = self.get_subaction_by_action(action_number)
            def_advanced = self.get_advanced_params_by_action(action_number)
            subactions = inputs.get_action_params(action_number, def_subactions)
            advanced = inputs.get_advanced_params(action_number, def_advanced)

            actions[action_number] = {
                "subactions": subactions,
                "advanced": advanced,
            }
        return actions

    def get_subaction_by_action(self, action_number):
        return [
            subaction_data
            for subaction_data in self.subactions
            if subaction_data["action"] == action_number
        ]

    def get_advanced_params_by_action(self, action_number):
        """
        Generate a dictionary containing economic and advanced params for
        a specific action.

        Parameters
        ----------
        action_number : str
            the action number allowing to identify related values inside both
            default values and user inputs.

        Returns
        -------
        dict
            resulting dictionary containing economic and advanced params as follows:
            {
                "economique": {
                },
                "advanced": {
                    "1": {
                        "param1": {
                            "1": 1,
                            "2": 2,
                        },
                    },
                },
                "yearAdvancedParams": {},
            }
        """
        advanced_params = [
            param_data["params_avances"]
            for param_data in self.advanced_params["actions"]
            if param_data["action"] == action_number
        ]
        if len(advanced_params) == 0:
            return {}

        return {
            "economique": {
                str(param["id"]): param["part"]
                for param in advanced_params[0].get("economique", {})
            },
            "advanced": {
                action_number: {
                    param_name: {
                        str(j[params["columns"].index("nom")]): j[
                            params["columns"].index("valeur")
                        ]
                        for j in params["data"]
                    }
                    for param_name, params in advanced_params[0]
                    .get("autres", {})
                    .items()
                }
            },
            "yearAdvancedParams": {
                action_number: {
                    param_name: {str(j): {} for j in params["index"]}
                    for param_name, params in advanced_params[0]
                    .get("others_years", {})
                    .items()
                }
            },
        }


class InputsLoader:
    def __init__(
        self,
        region,
        zone,
        zone_id,
    ):
        self.region = region
        self.zone = zone
        self.zone_id = zone_id

    def load_inputs(
        self,
        years,
        actions_by_year,
        subactions,
        advanced_params,
        advanced_economic_params,
        advanced_params_years,
        regional_config,
    ):
        self.years = years
        self.actions_by_year = actions_by_year
        self.subactions = subactions
        self.advanced_params = advanced_params
        self.advanced_economic_params = advanced_economic_params
        self.advanced_params_years = advanced_params_years
        self.regional_config = regional_config

    def load_mocked_inputs(
        self,
        subactions,
    ):
        # TODO: autocomplete the rest?
        self.years = []
        self.actions_by_year = {}
        self.subactions = subactions
        self.advanced_params = {}
        self.advanced_economic_params = {}
        self.advanced_params_years = {}
        self.regional_config = {}

    def is_action_filled(self, action_number):
        """
        Whether or not the action is filled by user inputs.

        Parameters
        ----------
        action_number : str
            the action number identifying action inside user inputs

        Returns
        -------
        boolean
            testing if any value exists for current action
        """
        if len(self.years) == 0:
            return False

        for actions in self.actions_by_year.values():
            if action_number in actions:
                return True
        return False

    def get_action_params(self, action_number, subactions_config):
        """
        Get sub action params that are given by inputs based on subactions
        related to current action. The subactions are indicated by the default
        values.

        Parameters
        ----------
        action_number : str
            current action number (1, 10a, 3b, etc.)
        subactions_config : dict
            containing at least two keys, `action` and `nom` to access subaction
            configuration

        Returns
        -------
        dict
            current action subactions inputs
        """
        # we retrieve subactions associated with current action
        associated_keys = set(
            sub["nom"] for sub in subactions_config if sub["action"] == action_number
        )

        # we filter to only have corresponding inputs
        def _is_right(sub):
            key, val = sub
            return key in associated_keys

        return dict(filter(_is_right, self.subactions.items()))

    def get_advanced_params(self, action_number, default_params):
        """
        Get advanced params that are given by inputs based on action config.
        The default values are given by external parameter.

        Parameters
        ----------
        action_number : str
            current action number (1, 10a, 3b, etc.)
        default_params : dict
            containing default values for advanced params

        Returns
        -------
        dict
            containing three keys: `economique`, `advanced` and `yearAdvancedParams`
        """
        current_adv_params = self.advanced_params.get(action_number, {})
        current_years_adv_params = self.advanced_params_years.get(action_number, {})

        return {
            "economique": {
                # if available, we take current value
                # otherwise, in the last get, we take default value
                str(key): self.advanced_economic_params.get(str(key), default_value)
                for key, default_value in default_params.get("economique", {}).items()
            },
            "advanced": {
                action_number: {
                    param_name: {
                        # if available, we take current value
                        # otherwise, in the last get, we take default value
                        id: current_adv_params.get(param_name, {}).get(
                            id, default_value
                        )
                        for id, default_value in params.items()
                    }
                    for param_name, params in default_params.get("advanced", {})
                    .get(action_number, {})
                    .items()
                }
            },
            "yearAdvancedParams": {
                action_number: {
                    param_name: {
                        # if available, we take current value
                        # otherwise, in the last get, we take default value
                        id: current_years_adv_params.get(param_name, {}).get(
                            id, default_value
                        )
                        for id, default_value in params.items()
                    }
                    for param_name, params in default_params.get(
                        "yearAdvancedParams", {}
                    )
                    .get(action_number, {})
                    .items()
                }
            },
        }
