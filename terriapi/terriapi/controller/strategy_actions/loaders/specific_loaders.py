# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi.controller import fetch
from terriapi.controller.strategy_actions.constants import Commodity


async def heating_systems_distribution(
    heating_consumption,
    COP,
    taux_equipement_chaleur_enrt,
    taux_equipement_elec,
    taux_equipement_pp,
    taux_equipement_gaz,
    keys,
):
    """Retourne les répartitions des équipements de chauffage dans le secteur résidentiel / tertaire.

    Parameters
    ----------
    region : str
        Nom de la région (schéma)
    zone : str
        Nom du territoire
    zone_id : str
        Identifiant du territoire
    action : float
        numéro de l'action
    nom_secteur: str
        Nom du secteur
    id_secteur: float
        id de secteur

    Returns
    ----------
    dict  : %
        Avec l'ensemble des répartitions des équipements de chauffage pour la région Occitanie
    """

    conso_residentiel_usage_chauffage = heating_consumption.loc[
        heating_consumption.COMMODITY.isin(
            (
                Commodity.ELECTRICITY,
                Commodity.RENEWABLE_HEAT,
                Commodity.OIL,
                Commodity.GAS,
            )
        )
    ]
    # Calculer la consommation totale de l'année de référence
    conso_totale = conso_residentiel_usage_chauffage["valeur"].sum()

    # Répartition de la consommation des énergies dans le chauffage résidentiel/tertiaire
    taux_biomasse = float(taux_equipement_chaleur_enrt["Chauffage biomasse"]) / 100
    taux_enRt_rdc = float(taux_equipement_chaleur_enrt["Réseau de chaleur"]) / 100

    taux_elec_joule = float(taux_equipement_elec["Elec joule"]) / 100
    taux_pac = float(taux_equipement_elec["PAC"]) / 100

    taux_chaud_fioul_gpl = float(taux_equipement_pp["Chaudière fioul/GPL"]) / 100
    taux_pp_rdc = float(taux_equipement_pp["Réseau de chaleur"]) / 100

    taux_chaud_gaz = float(taux_equipement_gaz["Chaudière gaz"]) / 100
    taux_gaz_rdc = float(taux_equipement_gaz["Réseau de chaleur"]) / 100

    # Calculer chaleur EnR PAC
    # à retirer des résultats finaux pour conserver la même conso de chauffage
    # 1 : Récupérer la consommation d'éléctricté => id_energie : 2
    conso_elec = float(
        conso_residentiel_usage_chauffage.loc[
            conso_residentiel_usage_chauffage.COMMODITY == Commodity.ELECTRICITY,
            "valeur",
        ].iloc[0]
    )
    # 2 : Récupérer le taux de PAC
    # 3 : Calculer la chaleur Enr PAC
    chaleur_enr_pac = conso_elec * taux_pac * (COP - 1)

    # Calculer le totale + Chaleur Enr PAC
    conso_totale_chaleur_enr_pac = conso_totale + chaleur_enr_pac

    # heating network values
    chauffage_rdc = 0.0
    # if we have no coefficient for heating network rates -> we take non renewable heat
    if taux_gaz_rdc == 0 and taux_pp_rdc == 0 and taux_enRt_rdc == 0:
        conso_rdc = heating_consumption.loc[
            heating_consumption.COMMODITY == Commodity.NON_RENEWABLE_HEAT, "valeur"
        ]
        if not conso_rdc.empty:
            conso_totale_chaleur_enr_pac += conso_rdc.sum()
            chauffage_rdc = conso_rdc.sum() / conso_totale_chaleur_enr_pac

    # calculer les répartitions des équipements de l'année de référence dans le chauffage résidentiel/tertiaire
    # Taux d'équipement chauffage elec joule
    chauffage_elec = (conso_elec * taux_elec_joule) / conso_totale_chaleur_enr_pac

    # Récupérer la consommation de gaz => id_energie : 7
    conso_chaud_gaz = conso_residentiel_usage_chauffage.loc[
        conso_residentiel_usage_chauffage.COMMODITY == Commodity.GAS, "valeur"
    ]
    if conso_chaud_gaz.empty:
        conso_chaud_gaz = 0.0
    else:
        conso_chaud_gaz = conso_chaud_gaz.iloc[0]

    # Taux d'équipement chaudière gaz
    chaudiere_gaz = (conso_chaud_gaz * taux_chaud_gaz) / conso_totale_chaleur_enr_pac

    # Récupérer la consommation produit pétroliers => id_energie : 5
    conso_fioul_gpl = conso_residentiel_usage_chauffage.loc[
        conso_residentiel_usage_chauffage.COMMODITY == Commodity.OIL, "valeur"
    ]
    if conso_fioul_gpl.empty:
        conso_fioul_gpl = 0.0
    else:
        conso_fioul_gpl = conso_fioul_gpl.iloc[0]

    # Taux d'équipement fioul-gpl
    chaudiere_fioul_gpl = (
        conso_fioul_gpl * taux_chaud_fioul_gpl
    ) / conso_totale_chaleur_enr_pac

    # Récupérer la consommation de biomasse : EnRt => id_energie : 3
    conso_biomasse = conso_residentiel_usage_chauffage.loc[
        conso_residentiel_usage_chauffage.COMMODITY == Commodity.RENEWABLE_HEAT,
        "valeur",
    ]
    if conso_biomasse.empty:
        conso_biomasse = 0.0
    else:
        conso_biomasse = conso_biomasse.iloc[0]
    # Taux d'équipement chauffage biomasse
    chauffage_biomasse = (conso_biomasse * taux_biomasse) / conso_totale_chaleur_enr_pac

    # Taux d'équipement chauffage RDC
    if taux_gaz_rdc != 0 or taux_pp_rdc != 0 or taux_enRt_rdc != 0:
        chauffage_rdc = (
            ((conso_chaud_gaz * taux_gaz_rdc) / conso_totale_chaleur_enr_pac)
            + ((conso_fioul_gpl * taux_pp_rdc) / conso_totale_chaleur_enr_pac)
            + ((conso_biomasse * taux_enRt_rdc) / conso_totale_chaleur_enr_pac)
        )

    # Taux d'équipement chauffage pompes à chaleur
    chauffage_pac = (conso_elec * taux_pac) / conso_totale_chaleur_enr_pac * COP

    return {
        "coeffs_ref": {
            keys["ELECTR"]: chauffage_elec * 100,
            keys["GAS"]: chaudiere_gaz * 100,
            keys["FIOUL_LPG"]: chaudiere_fioul_gpl * 100,
            keys["BIOMASS"]: chauffage_biomasse * 100,
            keys["DISTNET"]: chauffage_rdc * 100,
            keys["HEATPUMP"]: chauffage_pac * 100,
        },
        "chaleur_enr_pac": chaleur_enr_pac,
    }


async def nb_bus_cars(region, zone, zone_id):
    """Récupère le nb de bus et cars pour la zone sélectionnée

    Parameters
    ----------
    region : str
        Nom de la région (schéma)
    zone : str
        Nom du territoire
    zone_id : str
        Identifiant du territoire
    """
    sql = """
            with data_terr as (
                select *
                from strategie_territoire.parc_tcp as p
                join {schema}.territoire t on t.commune = p.commune
                and t.type_territoire = $1
                and t.code = $2
            ),
            gaz as (
                select 'Gazole' as energie, coalesce(sum(valeur),0) as nb
                from data_terr
                where energie = 'Gazole'
            ),
            essence as (
                select 'Essence' as energie, coalesce(sum(valeur),0) as nb
                from data_terr
                where energie = 'Essence'
            ),
            total as (
                select 'total' as energie, coalesce(sum(valeur),0) as nb
                from data_terr
                where energie in ('Gazole','Essence')
            )
            select * from gaz
            union
            select * from essence
            union
            select * from total
        """.format(
        schema=region.replace("-", "_")
    )

    res = await fetch(sql, zone, zone_id)
    valeurs = [dict(x) for x in res]
    return valeurs
