# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd
from sanic.log import logger
from slugify import slugify

from terriapi import controller
from terriapi.controller.strategy_actions.passage_table import ParamPassageTable
from terriapi.utils import check_is_valid_table


def _parse_name(name):
    return slugify(name).replace("-", "_").lower()


class ParametersGroup:
    """
    This class handles static parameters for a given region and table name.
    It is used to load the parameters from the database.
    """

    stored_passage_tables = {}

    def __init__(self, region, zone, zone_id, param):
        # all tables are prefixed with 'params_' to avoid conflicts with other tables
        self.table_name = "params_" + _parse_name(param.key_to_test)
        self.region = region
        self.zone = zone
        self.zone_id = zone_id
        self.param = param
        self.data = []

    async def is_valid(self):
        if self.param.computation_function:
            return True
        try:
            is_valid = await check_is_valid_table(
                "strategie_territoire", self.table_name
            )
            return is_valid
        except Exception as e:
            logger.warn(
                "Error while checking if table "
                + self.table_name
                + " is valid: "
                + str(e)
            )
            raise ValueError("Invalid table name: " + self.table_name)

    async def load(self):
        if self.param.computation_function:
            logger.debug(
                "Load param for " + str(self.param) + " through computation function"
            )
            self.data = await self.param.computation_function(
                self.region, self.zone, self.zone_id
            )
        else:
            req = f"""
            select * from strategie_territoire.{self.table_name}
            where region = $1
            """
            try:
                resp = await controller.fetch(req, self.region)
            except Exception as e:
                raise e
            self.data = pd.DataFrame(dict(row) for row in resp)
        if self.param.passage_table:
            logger.debug(
                "Param passage "
                + str(self.param.passage_table)
                + " for table "
                + self.table_name
            )
            if self.param.passage_table not in ParametersGroup.stored_passage_tables:
                _passage_table = ParamPassageTable(
                    self.region, "Param passage table for " + self.table_name
                )
                await _passage_table.load(self.param.passage_table)
                ParametersGroup.stored_passage_tables[self.param.passage_table] = (
                    _passage_table
                )
            passage_table = ParametersGroup.stored_passage_tables[
                self.param.passage_table
            ]
            self.data = passage_table.convert_data(self.param.passage_table, self.data)
