# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json

import pandas as pd

from terriapi import controller

ACTION_1_RENOV_LABEL = (
    "Répartition des rénovations par niveau de performance énergétique"
)
ACTION_1_GAIN_LABEL = "Gains attribués au chauffage"
ACTION_2_GAIN_LABEL = "Gain énergétique par niveau de rénovation"
ACTION_2_RENOV_LABEL = "Répartition des rénovations par sous secteur"

# contraintes
action_params_descr = {
    ACTION_1_RENOV_LABEL: """Un niveau de rénovation correspond à un
changement de classes DPE : niveau faible et moyen correspond à un passage à la classe supérieure (ex. : de G à F),
niveau performant à un passage à deux classes au dessus (ex. : de G à E), niveau très performant à un passage à
trois classes au dessus (ex: de G à B)"""
}

action_params_constraint = {
    ACTION_1_RENOV_LABEL: (
        "La somme des pourcentages par différent "
        "niveau de rénovation doit faire 100%"
    ),
    ACTION_2_RENOV_LABEL: (
        "la somme des pourcentages des rénovations par sous-secteur doit faire 100%"
    ),
}


async def get_actions_list(region, *args, **kwargs):
    sql = """
        select
            row_number() over (order by d.action_number) as id,
            d.action_number as numero,
            d.action_key,
            d.name,
            d.categorie,
            d.category_class,
            s.impact_eco,
            s.avec_params,
            s.first_year,
            s.last_year,
            COALESCE(s.description, '') as description
        from strategie_territoire.action d
        LEFT JOIN strategie_territoire.action_status s ON d.action_number = s.action_number
        where s.region = $1 AND s.enabled = true
        order by category_order, action_order;
    """
    res = await controller.fetch(sql, region)
    return [dict(row) for row in res]


async def get_subactions_list(region, *args, **kwargs):
    res = await controller.fetch(
        """
        SELECT
            s.id,
            s.action_number as action,
            concat(s.action_number, '_', s.nom) as nom,
            s.label,
            s.unite,
            s.choices
        FROM strategie_territoire.action_subactions s
        LEFT JOIN strategie_territoire.action_subactions_status sb 
        ON sb.action_number = s.action_number AND sb.nom = s.nom
        WHERE sb.region = $1 AND sb.enabled = true
        ORDER BY ordre""",
        region,
    )
    return [dict(row) for row in res]


async def get_advanced_params_list(region, zone, zone_id, *args, **kwargs):
    res = await controller.fetch(
        """
        with tmp as (
            select
                a.numero
                , json_build_object('id', a.id, 'phase_projet', a.phase_projet,
                                    'maillon', a.maillon, 'part',
                                    round(c.part_terr, 1), 'part_france', a.part_francaise * 100, 'action_eco', a.action_eco) as content
            from strategie_territoire.action_details a
            join strategie_territoire.naf b on b.id = a.naf or a.action_eco = false
            join {schema}.part_territoire($1, $2) c on c.a88 = b.a88
            where a.phase_projet is not null or a.action_eco = false
            order by a.phase_projet, a.maillon
        )
        select numero as action, json_agg(content) as params_avances
        from tmp
        group by numero""".format(
            schema=region.replace("-", "_")
        ),
        zone,
        zone_id,
    )

    # ajout des paramètres économiques
    user_action_params = await get_user_action_params(region)
    user_action_params_years = await get_user_action_params_years(region)

    results = [dict(row) for row in res]
    for line in results:
        # réécriture dans une sous catégorie
        # pour les paramètres propres aux impacts emplois
        convert_json = json.loads(line["params_avances"])
        # si l'action est ne contient pas d'impacts éco
        if convert_json[0]["action_eco"] == False:
            line["params_avances"] = {"economique": []}
        else:
            line["params_avances"] = {"economique": json.loads(line["params_avances"])}

        if line["action"] in user_action_params:
            # ajout des paramètres non économiques de l'action
            line["params_avances"]["autres"] = {
                key: json.loads(value.to_json(orient="split"))
                for key, value in user_action_params[line["action"]].items()
            }

            for key, value in user_action_params[line["action"]].items():
                if key in action_params_descr:
                    line["params_avances"]["autres"][key]["description"] = (
                        action_params_descr[key]
                    )
                if key in action_params_constraint:
                    line["params_avances"]["autres"][key]["contrainte"] = (
                        action_params_constraint[key]
                    )

        # ajout des paramètres no économiques avec des années jusqu'à 2050
        if line["action"] in user_action_params_years:
            # ajout des paramètres non économiques de l'action
            line["params_avances"]["others_years"] = {
                key: json.loads(value.to_json(orient="split"))
                for key, value in user_action_params_years[line["action"]].items()
            }

            for key, value in user_action_params_years[line["action"]].items():
                if key in action_params_descr:
                    line["params_avances"]["others_years"][key]["description"] = (
                        action_params_descr[key]
                    )
                if key in action_params_constraint:
                    line["params_avances"]["others_years"][key]["contrainte"] = (
                        action_params_constraint[key]
                    )
                line["params_avances"]["others_years"][key]["valeur_annee"] = {}

    global_params = await get_user_global_params(region)
    return {
        "global_params": {
            key: json.loads(value.to_json(orient="split"))
            for key, value in global_params.items()
        },
        "actions": results,
    }


async def get_user_action_params(region: str) -> dict[str, dict[str, pd.DataFrame]]:
    """
    Retrieves advanced parameters associated with all actions that users can filled in
    under main action parameters.
    """

    res = await controller.fetch(
        """SELECT action, libelle, nom, unite, valeur 
        FROM strategie_territoire.params_user_action_params
        WHERE region = $1
        order by order_param
        """,
        region,
    )
    if len(res) == 0:
        return {}
    df = pd.DataFrame(res, columns=res[0].keys())

    user_action_params = {}
    colsName = ["nom", "unite", "valeur"]
    for action in df["action"].unique():
        df_action = df.query("action == '{}'".format(action))
        user_action_params[action] = {}
        for libelle in df_action["libelle"].unique():
            df_action_libelle = df_action.query("libelle == '{}'".format(libelle))
            user_action_params[action][libelle] = df_action_libelle[colsName]
    return user_action_params


async def get_user_action_params_years(
    region: str,
) -> dict[str, dict[str, pd.DataFrame]]:
    """
    Retrieves yearly advanced parameters associated with all actions that users can
    filled in under main action parameters.
    """

    res = await controller.fetch(
        """SELECT * FROM strategie_territoire.params_user_action_params_years
        WHERE region = $1
        order by order_param
        """,
        region,
    )
    if len(res) == 0:
        return {}
    df = pd.DataFrame(res, columns=res[0].keys())

    user_action_params = {}
    colsName = ["nom"]

    for action in df["action"].unique():
        df_action = df.query("action == {}".format(action))
        user_action_params[str(action)] = {}
        for libelle in df_action["libelle"].unique():
            df_action_libelle = df_action.query("libelle == '{}'".format(libelle))
            user_action_params[str(action)][libelle] = df_action_libelle[colsName]

    return user_action_params


async def get_user_global_params(region: str) -> dict[str, pd.DataFrame]:
    """
    [UNUSED] Retrieves global parameters that users should be able to edit.

    Provides following list:

    - energy prices
    """
    # paramètres globaux modifiables
    res = await controller.fetch(
        """SELECT * FROM strategie_territoire.params_user_global_prix_energie
        WHERE region = $1
        """,
        region,
    )
    return {"prix_usage_energie": pd.DataFrame([dict(r) for r in res])}

    # suppression des années > 2030 pour le prix de l'énergie
    # user_global_params['prix_usage_energie'] = user_global_params['prix_usage_energie'][user_global_params['prix_usage_energie'].annee < 2031]
    # TODO est-ce que ça servait à qqchose ?


async def get_single_action_params(region: str, param_table_name: str) -> pd.DataFrame:
    """
    Retrieves static parameters associated with one action and that are used internally
    when computing an action's results.
    """
    authorized_table_names = [
        "action1_conso_max_etiquette",
        "action1_taux_renovation_etiquette",
        "action2_consommation_moyenne_unitaire",
        "action3b_parametres_avances",
        "action3b_ifer",
        "action3b_tarif_cre",
        "action7_parc_voiture_com_men",
        "action7_lambdas",
        "action7_indicateurs_carbon_energie",
        "action10_parametres_avances",
        "action10_ifer",
        "action10_tarif_cre",
        "action12_parametres_avances",
        "global_facteur_emission",
        "facteur_emission_polluants_ges_conso_atmo",
        "global_conso_transport_commun",
        "parts_investissement_VA_ETP",
    ]
    if param_table_name not in authorized_table_names:
        return pd.DataFrame()
    # paramètres globaux modifiables
    res = await controller.fetch(
        f"""SELECT * FROM strategie_territoire."params_{param_table_name}"
        WHERE region = $1
        """,
        region,
    )
    return pd.DataFrame([dict(r) for r in res])
