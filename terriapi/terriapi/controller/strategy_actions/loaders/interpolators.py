# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from collections import defaultdict

from scipy import interpolate

from terriapi.controller.strategy_actions.constants import Commodity, Sector

# all interpolators parameters must start with
# requisite, inputs, data, max_years,


def heating_system_distrib_interpolator(
    original_inputs, initial_distrib, ref_year, input_factor=1.0, *args, **kwargs
):
    ref_year = int(ref_year)

    # we get initial ratios and all years
    years = set((ref_year,))
    not_null_years = set((ref_year,))
    for input in original_inputs.values():
        years = years.union(set(map(lambda x: int(x), input.keys())))
        for year, val in input.items():
            if float(val) > 0.0:
                not_null_years.add(int(year))

    not_null_years = sorted(list(not_null_years))

    new_values = defaultdict(dict)
    for commodity, input in original_inputs.items():
        x_f = [ref_year]
        y_f = [float(initial_distrib[commodity])]
        # we add ref year value
        new_values[commodity][str(ref_year)] = y_f[0]

        # if year was specified by inputs
        for year in not_null_years:
            # and if it is not ref year
            if year != ref_year:
                # we need to use it to interpolate other years
                x_f.append(year)
                y_f.append(float(input[str(year)]) * input_factor)
        if len(x_f) <= 1 and len(y_f) <= 1:
            continue
        f = interpolate.interp1d(x_f, y_f)

        for year in input.keys():
            # if we are further than last year with input => last year value
            if int(year) > x_f[-1]:
                new_values[commodity][year] = y_f[-1]
            # if we are before first year with input => first year
            elif int(year) < x_f[0]:
                new_values[commodity][year] = y_f[0]
            else:
                # TODO: optimize here to run all years at once?
                new_val = f(year)
                new_values[commodity][year] = float(new_val)

    return new_values
