# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from functools import reduce

import pandas as pd
from sanic.log import logger

from terriapi import controller

from .. import DataKeys
from ..constants import Sector


class DataLoader:
    def __init__(self, passage_table):
        self.data = {}
        self.max_years = {}
        self.passage_table = passage_table

    def get(self, data_requisite, default=None):
        key_to_load = data_requisite.key_to_test
        return self.data[str(key_to_load)]

    async def load(self, region, zone, zone_id, data_requisites, all_years=False):
        # we create a dictionary containing, for each main table, all sectors
        # values that might be interesting
        unique_tables = dict()
        for data in data_requisites:
            key_to_load = data.key_to_test
            unique_tables[key_to_load.table] = unique_tables.get(
                key_to_load.table, key_to_load
            ).merge(key_to_load, Sector)

        for table, keys_to_load in unique_tables.items():
            try:
                table_name, filters = self.passage_table.get_query_params(keys_to_load)
            except ValueError as e:
                logger.error("Error in data loader " + str(e))
                continue
            data_loader = SingleDataLoader(region)
            await data_loader.load(
                table_name,
                zone,
                zone_id,
                self.passage_table.get_columns(table),
                filters,
                all_years=all_years,
            )
            current_data = self.passage_table.convert_data(table, data_loader.data)
            self.data[str(keys_to_load)] = current_data
            self.max_years[str(keys_to_load)] = data_loader.max_year

            if (
                len(current_data) > 0
                and "SECTOR" in current_data.columns
                and Sector in keys_to_load.columns
            ):
                for sector in keys_to_load.columns[Sector]["values"]:
                    sector_key = DataKeys([table, sector])
                    self.data[str(sector_key)] = current_data.loc[
                        current_data.SECTOR == sector
                    ]
                    self.max_years[str(sector_key)] = data_loader.max_year

        for data in data_requisites:
            key_to_load = data.key_to_test

            # we already have the data downloaded
            if str(key_to_load) in self.data:
                continue
            logger.debug(
                "\t Loading data "
                + str(key_to_load)
                + "("
                + str(type(key_to_load))
                + ")"
            )

            key_with_only_sector, other_keys = key_to_load.with_only(Sector)

            # if we have more general values already downloaded, we can filter them
            if str(key_with_only_sector) in self.data:
                general_data = self.data[str(key_with_only_sector)]
                filters = []
                for k, val in other_keys.columns.items():
                    filters.append(general_data[str(k)].isin(val["values"]))

                # we filter the data to only retrieve relevant data
                self.data[str(key_to_load)] = general_data.loc[
                    reduce(lambda a, b: a | b, filters, 0)
                ]
                # we have the same max year for the data as we are just taking a subset
                self.max_years[str(key_to_load)] = self.max_years[
                    str(key_with_only_sector)
                ]
            else:
                try:
                    # TODO: optimize by table to avoid multiple queries?
                    table_name, filters = self.passage_table.get_query_params(
                        key_to_load
                    )
                    data_loader = SingleDataLoader(region)
                    await data_loader.load(
                        table_name,
                        zone,
                        zone_id,
                        self.passage_table.get_columns(key_to_load.table),
                        filters,
                        all_years=all_years,
                    )

                    # we save data inside current array
                    self.data[str(key_to_load)] = self.passage_table.convert_data(
                        key_to_load.table, data_loader.data
                    )
                    self.max_years[str(key_to_load)] = data_loader.max_year

                    logger.debug(
                        "Data " + str(key_to_load) + " was successfully saved!"
                    )
                except ValueError as e:
                    logger.error(
                        "Data " + str(key_to_load) + " was not retrieved: " + str(e)
                    )
                    continue


class SingleDataLoader:
    data: pd.DataFrame

    def __init__(self, region, key=None):
        # all tables are prefixed with 'params_' to avoid conflicts with other tables
        self.key = key
        self.region = region
        self.schema = region.replace("-", "_")
        self.data = pd.DataFrame()
        self.max_year = None

    async def load(
        self, table_name, zone, zone_id, columns, filters={}, all_years=False
    ):
        logger.info(
            "\t Loading data from table "
            + table_name
            + " in schema "
            + self.schema
            + " with columns "
            + str(columns)
            + " and filters "
            + str(filters)
        )

        # we add parameters to current query
        final_params = []
        query_filters = []
        for col_filter, params in filters.items():
            # we create the query filters
            params_tests = ", ".join(
                f"${i+len(final_params)+3}" for i in range(len(params))
            )
            query_filters.append(col_filter + "::varchar IN (" + params_tests + ")")
            final_params += params
        # if we have at least one element, we add first AND
        # we add all filters
        filters_query = " AND " * (len(query_filters) > 0) + " AND ".join(query_filters)

        # group by elements
        columns_selection = ", " * (len(columns) > 0) + ", ".join(
            ("a." + c for c in columns)
        )

        # we add group by
        group_by_query = " GROUP BY " * (len(columns) > 0) + ", ".join(columns)

        if not all_years:
            # have access to multiple years here?
            query = f"""
                SELECT MAX(a.annee) as max_year, SUM(a.valeur) as valeur{columns_selection}
                FROM {self.schema}.{str(table_name)} a
                LEFT JOIN {self.schema}.territoire b ON a.commune = b.commune
                WHERE b.code = $1 AND b.type_territoire = $2 
                AND a.annee = (
                    SELECT MAX(annee) FROM {self.schema}.{str(table_name)}
                )
                {filters_query}
                {group_by_query}"""
        else:
            group_by_query += ", a.annee"
            # have access to multiple years here?
            query = f"""
                SELECT a.annee, SUM(a.valeur) as valeur{columns_selection}
                FROM {self.schema}.{str(table_name)} a
                LEFT JOIN {self.schema}.territoire b ON a.commune = b.commune
                WHERE b.code = $1 AND b.type_territoire = $2 
                {filters_query}
                {group_by_query}"""

        # we execute and save output
        try:
            resp = await controller.fetch(query, zone_id, zone, *final_params)
        except Exception as e:
            logger.error("Error while loading data: " + str(e))
            raise e
        df = pd.DataFrame([dict(row) for row in resp])
        if len(df) > 0:
            if not all_years:
                self.max_year = float(df["max_year"].iloc[0])
                df = df.drop(columns="max_year")
            else:
                self.max_year = float(df["annee"].max())
        self.data = df
