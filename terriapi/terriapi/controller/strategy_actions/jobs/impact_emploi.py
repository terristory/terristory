﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""
module permettant de stocker toutes les étapes de
calcul concernant l'impact emploi
"""
from abc import ABC, abstractmethod


class ImpactEmploi(ABC):
    """Cette calsse perment d'instancier la classe apropriéé pour calculer les impacts économiques selon le type de l'action :
    - action avec paramétres économique (cout global, ....)
    - action sans parameétres économique
    """

    @abstractmethod
    def calcule_impact_emploi(self):
        return NotImplemented
