# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
import asyncio
import math
from collections import defaultdict
from decimal import Decimal

import numpy as np
import pandas as pd
from sanic.log import logger

from terriapi import controller, here
from terriapi.controller.strategy_actions.loaders import actions_data, specific_loaders

from .impact_emploi import ImpactEmploi


class ImpactEmploiAvecParam(ImpactEmploi):
    """
    Cette classe hérite de la classe Impact Emploi, elle permet de calculer les impacts économiques pour les actions
    avec paramétres économique stocké dans strategie_territoire.action_details (cout global, ...)
    """

    action_when_ponctuel = {
        "1": """when numero = '1'
        then cout_global * {1_nb_logements} * {1_surface_moy_logement} / 1000""",
        "2": """when numero = '2'
        then cout_global * {2_surface_bat_tertiaire} / 1000""",
        "3": """when numero = '3'
        then cout_global * {3_puiss_crete} * 1000""",
        "4": """when numero = '4'
        then cout_global * {4_nb_batiment_collectif_renouv}*{4_surf_capteurs_bati} / 1000""",
        "4b": """when numero = '4b'
        then cout_global * {4b_nb_batiment_collectif_renouv}*{4b_surf_capteurs_bati} / 1000""",
        "5": """when numero = '5' and id between 58 and 68
            then cout_global * ((({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0) +
            {5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0)) /
            ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0)) *
            ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0))/
            {reseau_chaleur[Ratio de chaleur produite sur la puissance chaufferie bois]}
        when numero = '5' and id between 69 and 71
            then cout_global * ((({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0) +
            {5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0))/
            ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0))*
            ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0))/
            {reseau_chaleur[Ratio de chaleur produite sur la puissance chaufferie bois]} *
            {reseau_chaleur[Ratio de puissance chaufferie gaz sur la puissance chaufferie bois]}
        when numero = '5' and id between 72 and 73
            then cout_global * (({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0) +
            {5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0)) /
            {reseau_chaleur[Ratio énergie livrée par mètre linéaire]}) / 1000.0
        when numero = '5' and id between 74 and 75
            then cout_global * ((({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0)+
            {5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0))/({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0))*
            ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0))/
            {reseau_chaleur[Ratio de chaleur produite sur la puissance chaufferie bois]} +
            (((({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0) +
            {5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0))/
            ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0))*
            ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0))/
            {reseau_chaleur[Ratio de chaleur produite sur la puissance chaufferie bois]}) *
            {reseau_chaleur[Ratio de puissance chaufferie gaz sur la puissance chaufferie bois]}
            """,
        "6": """when numero = '6'
        then cout_global * {6_nb_methaniseur} * {6_puiss_moy_methaniseurs}""",
        "7": """when numero = '7' and id between 111 and 112
            then cout_global * {7_longueur_bandes_cyclables}
        when numero = '7' and id between 113 and 114
            then cout_global * {7_longueur_pistes_cyclables}""",
        "3b": """when numero = '3b'
        then cout_global * {3b_puiss_install} * 1000""",
        "6b": """when numero = '6b'
        then cout_global * {6b_nb_methaniseur} * {6b_capacite_injec_biometh}""",
        "10a": """when numero = '10a'
        then cout_global * {10a_puiss_crete} * 1000""",
        "10b": """when numero = '10b'
        then cout_global * {10b_puiss_crete} * 1000""",
        "10c": """when numero = '10c'
        then cout_global * {10c_puiss_crete} * 1000""",
        "12": """when numero = '12'
    then cout_global * {12_puiss_installee} * 1000""",
        "13": """when numero = '13' then cout_global * ({13_conso_totale}  /
                                    {bois_energie[Ratio de chaleur produite sur la puissance chaufferie bois]})""",
        "16": """when numero = '16' and maillon like '%chauffage_elec'
            then cout_global * ({difference_conso[chauffage_elec]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_elec' and numero='16')) * 1000
            when numero = '16' and maillon like '%chaudiere_gaz'
            then cout_global * ({difference_conso[chaudiere_gaz]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chaudiere_gaz' and numero='16')) * 1000
            when numero = '16' and maillon like '%chaudiere_fioul_gpl'
            then cout_global * ({difference_conso[chaudiere_fioul_gpl]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chaudiere_fioul_gpl' and numero='16')) * 1000
            when numero = '16' and maillon like '%chauffage_biomasse'
            then cout_global * ({difference_conso[chauffage_biomasse]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_biomasse' and numero='16')) * 1000
            when numero = '16' and maillon like '%chauffage_rdc'
            then cout_global * ({difference_conso[chauffage_rdc]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_rdc' and numero='16')) * 1000
            when numero = '16' and maillon like '%chauffage_pac'
            then cout_global * ({difference_conso[chauffage_pac]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_pac' and numero='16')) * 1000""",
        "17": """when numero = '17' and maillon like '%chauffage_elec'
            then cout_global * ({difference_conso[chauffage_elec]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_elec' and numero='17')) * 1000
            when numero = '17' and maillon like '%chaudiere_gaz'
            then cout_global * ({difference_conso[chaudiere_gaz]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chaudiere_gaz' and numero='17')) * 1000
            when numero = '17' and maillon like '%chaudiere_fioul_gpl'
            then cout_global * ({difference_conso[chaudiere_fioul_gpl]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chaudiere_fioul_gpl' and numero='17')) * 1000
            when numero = '17' and maillon like '%chauffage_biomasse'
            then cout_global * ({difference_conso[chauffage_biomasse]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_biomasse' and numero='17')) * 1000
            when numero = '17' and maillon like '%chauffage_rdc'
            then cout_global * ({difference_conso[chauffage_rdc]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_rdc' and numero='17')) * 1000
            when numero = '17' and maillon like '%chauffage_pac'
            then cout_global * ({difference_conso[chauffage_pac]} / (select valeur::numeric from strategie_territoire.action_details
            where parametres ilike 'Heures de fonctionnement chauffage_pac' and numero='17')) * 1000
                            """,
        "20": """when numero = '20' and id = 219 then cout_global * ({buscars[nb_bus]} * {20_bus_elec} / 100)
             when numero = '20' and id = 220 then cout_global * ({buscars[nb_bus]} * {20_bus_gnv} / 100)
             when numero = '20' and id = 329 then cout_global * ({buscars[nb_bus]} * {20_bus_diesel} / 100)
          """,
        "21": """when numero = '21' and id between 315 and 317 then cout_global * ({emission_co2_par_industrie[taux_industrie_siderurgique_csc]} + {emission_co2_par_industrie[taux_prod_aluminium_csc]})
             when numero = '21' and id between 318 and 320 then cout_global * {emission_co2_par_industrie[taux_industrie_chimique_csc]}
             when numero = '21' and id between 321 and 323 then cout_global * {emission_co2_par_industrie[taux_prod_ciment_csc]}
             when numero = '21' and id between 324 and 326 then cout_global * {emission_co2_par_industrie[taux_prod_ammoniac_csc]}
    """,
    }

    action_when_perenne = {
        "1": """when numero = '1'
            then cout_global * {1_nb_logements} * {1_surface_moy_logement} / 1000.""",
        "2": """when numero = '2'
            then cout_global * {2_surface_bat_tertiaire} / 1000.""",
        "3": """when numero = '3'
            then cout_global * {3_puiss_crete} * 1000""",
        "4": """when numero = '4'
            then cout_global * {4_nb_batiment_collectif_renouv}*{4_surf_capteurs_bati} / 1000.""",
        "4b": """when numero = '4b'
            then cout_global * {4b_nb_batiment_collectif_renouv}*{4b_surf_capteurs_bati} / 1000.""",
        "5": """when numero = '5' and id between 76 and 79
            then cout_global * (((({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0)) +
                            ({5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0)) /
                            ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0)) *
                            ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0) * 1000) /
                            ({reseau_chaleur[PCI anhydre]} *
                            (1 - ({reseau_chaleur[Taux d'humidité du bois]} / 100.0))) - (6.786 * ({reseau_chaleur[Taux d'humidité du bois]} / 100.0))) / 1000
        when numero = '5' and id = 80 or id = 83
            then cout_global * (((({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0)) +
                                ({5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0)) /
                                ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0)) *
                                (1 - ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0))) /
                                ({reseau_chaleur[Rendement chaufferie gaz]} / 100.0)) / 1000
        when numero = '5' and id between 81 and 82
            then
            cout_global * ((({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0)) +
            ({5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0)) /
            ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0)) *
            ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0)) / 1000
        when numero = '5' and id = 84
            then cout_global * ((({5_conso_totale_chauffage_resi} *
            ({5_part_conso_resi_racc} / 100.0)) + ({5_conso_totale_chauffage_tertiaire} *
            ({5_part_conso_tertiaire_racc} / 100.0)) /
            ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0)) *
            (1 - ({reseau_chaleur[Part bois dans la chaleur produite par le réseau envisagé]} / 100.0))) / 1000
        when numero = '5' and id = 85
            then cout_global * (({5_conso_totale_chauffage_resi} * ({5_part_conso_resi_racc} / 100.0)) +
            ({5_conso_totale_chauffage_tertiaire} * ({5_part_conso_tertiaire_racc} / 100.0)) /
            ({reseau_chaleur[Rendement réseaux de chaleur]} / 100.0)) / 1000 """,
        "6": """when numero = '6'
            then (cout_global * {6_nb_methaniseur} * {6_puiss_moy_methaniseurs} * act.tpsfonc) / 1000.""",
        "6b": """when numero = '6b'
            then (cout_global * {6b_nb_methaniseur} * {6b_capacite_injec_biometh} * 0.96 * 11.07 * act.tpsfonc_b) / 1000.""",
        "7": """
        when numero = '7' and id = 115
            then cout_global * {7_longueur_bandes_cyclables}
        when numero = '7' and id = 116
            then cout_global * {7_longueur_pistes_cyclables}""",
        "3b": """
        when numero = '3b'
            then cout_global * {3b_puiss_install} * 1000""",
        "10a": """when numero = '10a'
            then cout_global * {10a_puiss_crete} * 1000""",
        "10b": """when numero = '10b'
            then cout_global * {10b_puiss_crete} * 1000""",
        "10c": """when numero = '10c'
            then cout_global * {10c_puiss_crete} * 1000""",
        "12": """when numero = '12'
            then cout_global * {12_puiss_installee} * 1000""",
        "13": """when numero = '13' and id between 213 and 216 then cout_global * (((({13_conso_totale}) /
                                ({bois_energie[Rendement de la chaufferie]} / 100.0))) * 1000) /
                                (({bois_energie[PCI anhydre]} * (1 - ({bois_energie[Taux d'humidité du bois]} /100.0)) - ((6.786 *
                                ({bois_energie[Taux d'humidité du bois]} / 100.0)) * 100.0)))/ 1000.
                when numero = '13' and id between 217 and 218 then cout_global * {13_conso_totale} / 1000""",
        "20": """when numero = '20' and id = 219 then 0
                when numero = '20' and id = 220 then 0
                 when numero = '20' and id = 329 then 0
            """,
        "16": """ when numero = '16' then 0""",
        "17": """ when numero = '17' then 0""",
        "21": """ when numero = '21' then 0""",
    }

    prod_terr_ponctuel = """
        select
            a.id
            , a.numero
            , a.maillon
            , concat(a.type, ',', a.type_hyp, ',', a.maillon) as type
            , b.id as naf
            , case when a.cout_global is not null
            then
                a.cout_global * a.part_francaise * (coalesce(pl.part_terr, c.part_terr) / 100.0)
            else
                (d.taux_mo * e.sum_invest) / (1 - d.taux_mo)
                * a.part_francaise * (coalesce(pl.part_terr, c.part_terr) / 100.0)
            end as cout_global
            , a.unite
        from strategie_territoire.action_details a
        join strategie_territoire.naf b on b.id = a.naf
        join {schema}.part_territoire($1, $2) c on c.a88 = b.a88
        -- part locale
        left join (values {part_locale}) as pl(id, part_terr) on a.id = pl.id
        left join lateral (
            select strategie_territoire.tonumeric(valeur) as taux_mo
            from strategie_territoire.action_details
            where type_hyp = 'générales'
                and type = a.type
                and parametres ilike 'rémunération maitre d''œuvre'
        ) as d on true
        left join lateral (
            select sum(cout_global) as sum_invest
            from strategie_territoire.action_details
            where type = a.type
                and type_hyp = 'investissement'
        ) as e on true
        where type_hyp = 'investissement'
        order by a.id
    """

    prod_terr_perenne = """
        select
            a.id
            , a.numero
            , concat(a.type, ',', a.type_hyp, ',', a.maillon) as type
            , b.id as naf
            , case
                when a.cout_global is not null
                then a.cout_global * a.part_francaise * coalesce(pl.part_terr, c.part_terr) / 100.0
                when a.cout_global is null and maillon ilike '%bande%'
                then e.sum_invest * entretien_bande.ratio * a.part_francaise * coalesce(pl.part_terr, c.part_terr) / 10000
                when a.cout_global is null and maillon ilike '%piste%'
                then e.sum_invest * entretien_piste.ratio * a.part_francaise * coalesce(pl.part_terr, c.part_terr) / 10000
            end as cout_global
            , a.unite
        from strategie_territoire.action_details a
        join strategie_territoire.naf b on b.id = a.naf
        join {schema}.part_territoire($1, $2) c on c.a88 = b.a88
        -- part locale
        left join (values {part_locale}) as pl(id, part_terr) on a.id = pl.id
        -- join part_territoire_user si existant
        left join lateral (
            select sum(cout_global) as sum_invest
            from strategie_territoire.action_details
            where type = a.type
                and type_hyp = 'investissement'
                and (
                    (position('bande' in a.phase_projet) > 0 and position('bande' in phase_projet) > 0)
                    or (position('piste' in a.phase_projet) > 0 and position('piste' in phase_projet) > 0)
                )
        ) as e on true
        , lateral (
            select strategie_territoire.tonumeric(replace(valeur, '%', ''))  as ratio
            from strategie_territoire.action_details where parametres ilike '%ratio entretien%bande%'
        ) as entretien_bande
        , lateral (
            select strategie_territoire.tonumeric(replace(valeur, '%', ''))  as ratio
            from strategie_territoire.action_details where parametres ilike '%ratio entretien%piste%'
        ) as entretien_piste
        where type_hyp = 'maintenance'
        UNION ALL
        select
            id
            , numero
            , concat(type, ',', type_hyp, ',', maillon) as type
            , naf
            , cout_global * part_francaise
            , unite
        from strategie_territoire.action_details
        where remuneration = 1 or (numero = '20' and remuneration = 0)
    """

    # étape 6 - ponctuel
    # https://gitlab.com/terristory/terristory/-/issues/60
    bilan_cout_prod_ponctuel = """
        select
            *,
            case
            {when}
            end as bilan_cout_prod
        from (
            {prod_terr}
        ) _
        where numero in {action_filter}
    """

    # étape 6 - pérenne
    # https://gitlab.com/terristory/terristory/-/issues/60
    bilan_cout_prod_perenne = """
        with act as (
            select
                round((({reseau_chaleur[PCI anhydre]}
                    * (1 - {reseau_chaleur[Taux d'humidité du bois]} / 100.0)
                    - 6.786 * {reseau_chaleur[Taux d'humidité du bois]}) / 1000.)::numeric, 1) * 1000 as pcimoyen
                , (({reseau_chaleur[Rendement chaufferie bois]} + {reseau_chaleur[Rendement chaufferie gaz]}) / 2) / 100.0 as chauff
                , {reseau_chaleur[Rendement réseaux de chaleur]} / 100.0 as reseau
                , tpsfonc.valeur as tpsfonc
                , tpsfonc_b.valeur as tpsfonc_b
            from (values (NULL)) _
            , lateral (
                select valeur::numeric
                from strategie_territoire.action_details where parametres ilike '%Temps de fonctionnement annuel en pleine puissance%'
                and numero = '6'
            ) as tpsfonc
            , lateral (
                select valeur::numeric
                from strategie_territoire.action_details where parametres ilike '%Temps de fonctionnement annuel en pleine puissance%'
                and numero = '6b'
            ) as tpsfonc_b
        )
        select
            ptp.*,
            case
            {when}
            end as bilan_cout_prod
        from (
            {prod_terr}
        ) ptp
        cross join act
        where ptp.numero in {action_filter}
    """

    def build_case(
        self,
        actions,
        year_params,
        reseau_chaleur,
        bois_energie,
        buscars,
        difference_conso,
        emission_co2_par_industrie,
        year,
        ponctuel=True,
    ):
        """
        Contruction du case sql pour calculer le coût
        associé à chaque action renseignée par l'utilisateur

        Parameters
        ----------
        actions: list
            liste des numéros d'actions
        year_params: dict
            association nom de paramètre -> valeur pour l'année considérée
        ponctuel: bool
            si vrai alors impact ponctuel sinon impact pérenne

        Returns
        -------
        str
            Partie de SQL à insérer dans un CASE
        """

        if ponctuel:
            result = ""
            difference_conso_action = None
            for action in actions:
                # here we use new system with old id and action type to retrieve consumption diff
                action_number = action.action_number
                conso_action_key = action.name

                if action_number in ["16", "17"]:
                    difference_conso_action = difference_conso.get(
                        conso_action_key, {}
                    ).get(
                        year,
                        {
                            "chauffage_elec": 0.0,
                            "chaudiere_gaz": 0.0,
                            "chaudiere_fioul_gpl": 0.0,
                            "chauffage_biomasse": 0.0,
                            "chauffage_rdc": 0.0,
                            "chauffage_pac": 0.0,
                        },
                    )
                elif action_number == "21":
                    emission_co2_par_industrie = emission_co2_par_industrie[year]
                result += (
                    "\n".join([self.action_when_ponctuel[action_number]]).format(
                        **year_params,
                        bois_energie=bois_energie,
                        reseau_chaleur=reseau_chaleur,
                        difference_conso=difference_conso_action,
                        buscars=buscars,
                        emission_co2_par_industrie=emission_co2_par_industrie,
                    )
                    + "\n"
                )
            return result
        else:
            get_action_id = lambda x: x.action_number

            return "\n".join(
                [self.action_when_perenne[get_action_id(action)] for action in actions]
            ).format(
                **year_params,
                bois_energie=bois_energie,
                reseau_chaleur=reseau_chaleur,
                buscars=buscars,
                year=year,
            )

    def impact_direct(
        self,
        region,
        part_locale,
        actions,
        year_params,
        action_filter,
        reseau_chaleur,
        bois_energie,
        buscars,
        ponctuel,
        difference_conso,
        emission_co2_par_industrie,
        year,
    ):
        """Requête finale de calcul d'impact emploi direct
        https://gitlab.com/terristory/terristory/-/issues/67

        Parameters
        ----------
        region : str
            nom de la région (schéma en base)
        part_locale : str
            Chaine contenant des enregistrements temporaires pour la part locale
            ex: '(id_action, 100), (...)...'
        actions : str
            liste des numéros d'actions
        year_params: dict
            association nom de paramètre -> valeur pour l'année considérée
        action_filter: str
            filtre SQL pour filtrer via les numéros d'actions
        ponctuel: bool
            si vrai alors impact ponctuel sinon impact pérenne
        reseau_chaleur: dict
            paramètres avancés de l'action 5 réseau de chaleur
        Returns
        -------
        str
            requête SQL complète à exécuter pour récupérer le nombre d'emplois
            directs ainsi que la valeur ajoutée brute associée
        """
        sql = """
    with bilan_cout as ({bilan_cout_prod}), va_brute as (
        select
            n.a732
            , sum(bilan_cout_prod) * 1000 * v.part_va_prod as va_brute
        from bilan_cout t
            left join strategie_territoire.naf n on t.naf = n.id
            left join strategie_territoire.va_prod v on v.a732 = n.a732
            group by n.a732, v.part_va_prod
    )
    select
        round(sum(va_brute) / 1e6, 3) as va_totale
        , round(sum(va_brute * ratio_emploi_va / 1e6))::int as nb_emploi_total
    from va_brute v
    left join strategie_territoire.emploi_va e on e.a732 = v.a732
        """
        if ponctuel:
            return sql.format(
                bilan_cout_prod=self.bilan_cout_prod_ponctuel.format(
                    prod_terr=self.prod_terr_ponctuel.format(
                        schema=region, part_locale=part_locale
                    ),
                    when=self.build_case(
                        actions,
                        year_params,
                        reseau_chaleur,
                        bois_energie,
                        buscars,
                        difference_conso,
                        emission_co2_par_industrie,
                        year,
                        ponctuel,
                    ),
                    action_filter=action_filter,
                    reseau_chaleur=reseau_chaleur,
                    bois_energie=bois_energie,
                    difference_conso=difference_conso,
                    emission_co2_par_industrie=emission_co2_par_industrie,
                    year=year,
                )
            )
        else:
            return sql.format(
                bilan_cout_prod=self.bilan_cout_prod_perenne.format(
                    schema=region,
                    prod_terr=self.prod_terr_perenne.format(
                        schema=region, part_locale=part_locale
                    ),
                    when=self.build_case(
                        actions,
                        year_params,
                        reseau_chaleur,
                        bois_energie,
                        buscars,
                        difference_conso,
                        emission_co2_par_industrie,
                        year,
                        ponctuel,
                    ),
                    action_filter=action_filter,
                    reseau_chaleur=reseau_chaleur,
                    bois_energie=bois_energie,
                    difference_conso=difference_conso,
                    emission_co2_par_industrie=emission_co2_par_industrie,
                    year=year,
                )
            )

    def demande_indirecte(
        self,
        region,
        part_locale,
        actions,
        year_params,
        action_filter,
        ponctuel,
        reseau_chaleur,
        bois_energie,
        buscars,
        difference_conso,
        emission_co2_par_industrie,
        year,
    ):
        """
        Étape 2 emplois indirects - consommation inter branches
        https://gitlab.com/terristory/terristory/-/issues/74

        Parameters
        ----------
        part_locale : str
            Chaine contenant des enregistrements temporaires pour la part locale
            ex: '(id_action, 100), (...)...'
        actions : str
            liste des numéros d'actions
        year_params: dict
            association nom de paramètre -> valeur pour l'année considérée
        action_filter: str
            filtre SQL pour filtrer via les numéros d'actions
        ponctuel: bool
            si vrai alors impact ponctuel sinon impact pérenne
        reseau_chaleur: dict
            paramètres avancés de l'action 5 réseau de chaleur
        Returns
        -------
        str
            requête SQL complète à exécuter pour récupérer le nombre d'emplois
            directs ainsi que la valeur ajoutée brute associée
        """

        sql = """
    with tmp as (
        {bilan_cout_prod}
    ), demande as (
    select
        a732,
        sum(bilan_cout_prod) * 1000 as demande_init
    from tmp t
    join strategie_territoire.naf n on t.naf = n.id
    group by n.a732
    )
    select
        n.cpa
        , sum(demande_init * c.part_ci_prod) as conso_inter
    from demande d
    join strategie_territoire.ci_prod c on c.a732 = d.a732
    join strategie_territoire.naf n on n.a732 = c.a732
    group by n.cpa
        """
        if ponctuel:
            return sql.format(
                bilan_cout_prod=self.bilan_cout_prod_ponctuel.format(
                    prod_terr=self.prod_terr_ponctuel.format(
                        schema=region, part_locale=part_locale
                    ),
                    when=self.build_case(
                        actions,
                        year_params,
                        reseau_chaleur,
                        bois_energie,
                        buscars,
                        difference_conso,
                        emission_co2_par_industrie,
                        year,
                        ponctuel,
                    ),
                    action_filter=action_filter,
                    reseau_chaleur=reseau_chaleur,
                    bois_energie=bois_energie,
                    difference_conso=difference_conso,
                    emission_co2_par_industrie=emission_co2_par_industrie,
                    year=year,
                )
            )

        return sql.format(
            bilan_cout_prod=self.bilan_cout_prod_perenne.format(
                schema=region,
                prod_terr=self.prod_terr_perenne.format(
                    schema=region, part_locale=part_locale
                ),
                when=self.build_case(
                    actions,
                    year_params,
                    reseau_chaleur,
                    bois_energie,
                    buscars,
                    difference_conso,
                    emission_co2_par_industrie,
                    year,
                    ponctuel,
                ),
                action_filter=action_filter,
                reseau_chaleur=reseau_chaleur,
                bois_energie=bois_energie,
                difference_conso=difference_conso,
                emission_co2_par_industrie=emission_co2_par_industrie,
                year=year,
            )
        )

    def calcul_investissement(
        self,
        actions,
        year_params,
        action_filter,
        reseau_chaleur,
        bois_energie,
        buscars,
        difference_conso,
        emission_co2_par_industrie,
        year,
    ):
        """Calcul de l'investissement pour une année donnée en prenant
        en compte les paramètres utilisateurs de chaque action
        cf : https://gitlab.com/terristory/terristory/issues/376

        Parameters
        ----------
        actions : str
            liste des numéros d'actions
        year_params: dict
            association nom de paramètre -> valeur pour l'année considérée
        action_filter: str
            filtre SQL pour filtrer les numéros d'actions
        Returns
        -------
        float
            somme des investissements en
        """

        sql = """
        select
            round(sum(cout_global), 2) as invest
        from (
            select
            a.numero
            , case when d.taux_mo is not null
                then a.sum_invest + (a.sum_invest * d.taux_mo / (1 - d.taux_mo))
                else a.sum_invest
            end as cout_global
            from (
                select numero, sum(case {when} end) as sum_invest
                from strategie_territoire.action_details
                where type_hyp = 'investissement'
                group by numero
            ) as a
            left join lateral (
                select strategie_territoire.tonumeric(valeur) as taux_mo
                from strategie_territoire.action_details
                where type_hyp = 'générales'
                    and numero = a.numero
                    and parametres ilike 'rémunération maitre d''œuvre'
            ) as d on true
        ) _
        where numero in {action_filter}
    """
        sql = sql.format(
            when=self.build_case(
                actions,
                year_params,
                reseau_chaleur,
                bois_energie,
                buscars,
                difference_conso,
                emission_co2_par_industrie,
                year,
                ponctuel=True,
            ),
            action_filter=action_filter,
        )

        return sql

    effectif_cpa_terr = """
        with tmp as (
            select
                sum(nbter.nb_emploi) nbter
                , n.a88::int
                , a.emploi_interieur_salarie_nb_pers as eisnp
                , a.emploi_interieur_total_nb_pers * 1000 as eitnb
                , (a.emploi_interieur_salarie_nb_pers / a.emploi_interieur_total_nb_pers) as pse
                , n.cpa
            from {schema}.nb_emploi_territoire_view nbter
            join strategie_territoire.naf n on n.a88 = nbter.a88
            join strategie_territoire.a88 a on a.naf = n.id
            where nbter.territoire = $1 and nbter.territoire_id = $2
            group by n.a88, n.cpa, a.emploi_interieur_salarie_nb_pers, a.emploi_interieur_total_nb_pers
        )
        select
            cpa,
            sum(nbter / pse) as effectif_cpa
        from tmp
        group by cpa
        """

    # matrice des entrées sorties (économie)

    # nettoyage pour historique:
    # >>> div.drop(columns=['libelle'], inplace=True)
    # >>> div.loc['P1'] = div.loc['P1'].str.replace(' ', '').str.replace(',', '.')
    # >>> div.drop(columns=['CPA_U'], inplace=True)
    # >>> div.fillna(0).to_csv('tesdiv.csv', sep=';')

    mio = pd.read_csv(
        str((here / "data" / "tableau-entrees-sorties.csv").resolve()),
        delimiter=";",
        decimal=",",
    )
    mio.set_index(mio.columns, inplace=True)
    mio = mio.astype(np.float64)

    # Matric des consos intermédiaires ...
    # Étape 2
    mconso = pd.read_csv(
        str((here / "data" / "tesdiv.csv").resolve()),
        delimiter=";",
        thousands=" ",
        decimal=",",
    )
    mconso.set_index("Code", inplace=True)
    mconso = mconso.astype(np.float64)

    def compute_ciq(self, mterr, mfr, sum_terr, sum_fr):
        """
        Étape 4 : matrice CIQ_LIQ
        https://gitlab.com/terristory/terristory/-/issues/76
        """
        index = self.mio.index.tolist()
        smfr = pd.Series(mfr.values.ravel(), index=mfr.index)
        smterr = pd.Series(mterr.values.ravel(), index=mterr.index)
        missing_fr = set(index).difference(set(smfr.index))
        for k in missing_fr:
            smfr[k] = np.nan
        missing_terr = set(index).difference(set(mterr.index))
        for k in missing_terr:
            smterr[k] = np.nan
        cpa = pd.DataFrame({"mfr": smfr, "mterr": smterr})
        # diagonale
        diag = (cpa["mterr"] / sum_terr) / (cpa["mfr"] / sum_fr)
        diag.fillna(0.0, inplace=True)

        cpa["ratio"] = (cpa["mterr"] / cpa["mfr"]).astype(np.float64)
        row = cpa["ratio"].values
        col = cpa["ratio"].values

        col[col == 0] = np.nan
        row[row == 0] = np.nan

        ciq = np.divide.outer(row, col)
        ciq[np.diag_indices(64)] = diag.values
        ciq = pd.DataFrame(ciq, index=cpa.index, columns=cpa.index)
        ciq.fillna(0.0, inplace=True)
        return ciq

    def impact_indirect(self, cpa_terr, cpa_fr, conso_inter, emploi_par_cpa_df):
        # Étape 3
        # https://gitlab.com/terristory/terristory/-/issues/75
        mrci = self.mio.divide(self.mconso.loc["TOT_CA"])
        mrci.fillna(0, inplace=True)

        mterr = pd.DataFrame.from_dict([dict(row) for row in cpa_terr])
        mfr = pd.DataFrame.from_dict([dict(row) for row in cpa_fr])
        mterr.set_index("cpa", inplace=True)
        mfr.set_index("cpa", inplace=True)
        sum_terr = mterr["effectif_cpa"].sum()
        sum_fr = mfr["effectif_cpa"].sum()

        # Étape 4 : matrice CIQ_LIQ
        # https://gitlab.com/terristory/terristory/-/issues/76
        ciq = self.compute_ciq(mterr, mfr, sum_terr, sum_fr)

        # Étape 5: matrice des coefficients de territorialisation des rétombées économiques
        # https://gitlab.com/terristory/terristory/-/issues/77
        flq = ciq * math.pow(math.log2(1 + (sum_terr / sum_fr)), 0.3)

        # Étape 6: conso intermédiaires NS adressées aux branches
        # https://gitlab.com/terristory/terristory/-/issues/78
        conso_inter = pd.DataFrame.from_dict([dict(row) for row in conso_inter])
        conso_inter.set_index("cpa", inplace=True)
        # création de la table des conso intermédiaires des branches
        # avec tous les code CPA
        conso = pd.DataFrame(
            np.zeros((self.mio.shape[0], 1)),
            index=self.mio.index,
            columns=["conso_inter"],
        )
        conso.update(conso_inter)
        # conversion de numeric (provenant de pg) à flottant
        conso = conso.astype(np.float64)
        cib = mrci * flq[flq < 1].fillna(1) @ conso

        # Étape 7:
        # https://gitlab.com/terristory/terristory/-/issues/79
        # matrice des coeffs techniques restreints aux consommations
        # issues de la production nationale...à vos souhaits !
        mct = self.mio.divide(self.mconso.loc["P1"])
        mct.fillna(0, inplace=True)

        # Étape 8:
        # Matrice des coefficients technique du territoire uniquement
        # https://gitlab.com/terristory/terristory/-/issues/80
        mcterr = flq[flq < 1] * mct
        mcterr[flq >= 1] = mct

        # Étape 9:
        # Matrice des coefficients territoriaux
        # https://gitlab.com/terristory/terristory/-/issues/81
        ident = pd.DataFrame(
            np.identity(self.mio.shape[0]),
            index=self.mio.index,
            columns=self.mio.columns,
        )
        leontief = ident - mcterr
        mcoeff = pd.DataFrame(
            np.linalg.inv(leontief.values), leontief.columns, leontief.index
        )

        # Étape 10 : demande totale produit
        # https://gitlab.com/terristory/terristory/-/issues/82
        dtp = mcoeff.dot(cib)
        # https://gitlab.com/terristory/terristory/-/issues/83
        div = self.mconso.loc["B1G"].divide(self.mconso.loc["P1"])
        # https://gitlab.com/terristory/terristory/-/issues/84
        # VA brute indirecte
        dtp = (dtp["conso_inter"] * div).to_frame()

        dtp.rename(index=str, columns={0: "ratio"}, inplace=True)
        return {
            "va_totale": round((dtp.sum() / 1e6)[0], 3),
            "nb_emploi_total": int(round((emploi_par_cpa_df * dtp / 1e6).sum())),
        }

    def calcul_investissement_indus_non_ener(self, emission_co2_par_industrie):
        """Cette fonction permet de calculer l'investissement pour les industries sans CSC
            pour l'action 21 : Réduction des émissions GES non-ener dans l'industrie

        Paramètres
        ----------
        emission_co2_par_industrie : str
            le taux des émissions CO2 pour les industries sans CSC

        Returns
        -------
        float
            investissement de l'année courante
        """

        # unité €/teqCO2
        cout_industrie_chimique = (
            emission_co2_par_industrie["taux_industrie_chimique"] * 250
        )
        cout_industrie_ciment = emission_co2_par_industrie["taux_prod_ciment"] * 625.042
        cout_industrie_incineration_dechets = (
            emission_co2_par_industrie["taux_incineration_dechets"] * 2298883.418
        )

        inves = (
            cout_industrie_chimique
            + cout_industrie_ciment
            + cout_industrie_incineration_dechets
        )

        return Decimal(round(inves, 3))

    def calcul_ETP_VA_indus_non_ener(self, emission_co2_par_industrie):
        """Cette fonction permet de calculer la valeur ajoutée et le nombre d'emplois générés
              pour les industries sans CSC pour l'action 21 : Réduction des émissions GES non-ener dans l'industrie

        Paramètres
         ----------
         emission_co2_par_industrie : str
             le taux des émissions CO2 pour les industries sans CSC

         Returns
         -------
         dict
             la valeur ajoutée et le nombre d'emplois générés
        """

        inves = self.calcul_investissement_indus_non_ener(emission_co2_par_industrie)

        # calculer la valeur ajoutée
        va_totale = float(inves) * 0.46

        # calculer le nombre d'emplois généré
        nb_emploi_total = float(inves) * 12.42

        return {
            "va_totale": Decimal(va_totale),
            "nb_emploi_total": Decimal(nb_emploi_total),
        }

    async def calcule_impact_emploi(
        self,
        region,
        zone,
        zone_id,
        years,
        mix_energetique,
        indice_consommation,
        difference_conso,
        emission_co2_par_industrie,
        action_year,
        inputs,
        actions,
    ):
        """Calcule l'impact des actions sur les emplois, la valeur ajoutée et l'investissement

        Parameters
        ----------
        region : str
            Nom de la région (schéma)
        zone : str
            Nom du territoire
        zone_id : str
            Identifiant du territoire
        years : set
            Ensemble des années
        action_year : dict
            Liste des identifiants des actions par année
        action_params : dict
            Paramètres des actions
        advanced_params : dict
            Paramètres avancés des actions

        """
        if action_year == {}:  # Si aucune action n'a été sélectionnée,
            return {}  # On retourne un dictionnaire vide

        schema_region = region.replace("-", "_")

        # liste des actions cochés format pour les requetes sql
        action_filter_str = "('{}')".format(
            "','".join([action.action_number for action in actions])
        )

        # indicateurs pérennes à conserver pour l'année n+1
        nb_emploi_perenne_direct = defaultdict(list)
        va_totale_perenne_direct = defaultdict(list)
        nb_emploi_perenne_indirect = defaultdict(list)
        va_totale_perenne_indirect = defaultdict(list)

        # tableau des résultats finaux pour toutes les années
        results = {}
        # XXX dag kézako cpa ?
        cpa_terr = {}

        code_region = await controller.fetch(f"select code from {schema_region}.region")
        code_region = code_region[0]["code"]

        # calculs indépendants de l'année :
        if zone != "région":
            cpa_terr["region"] = await controller.fetch(
                self.effectif_cpa_terr.format(schema=schema_region),
                "region",
                code_region,
            )

        cpa_terr[zone], cpa_france, emploi_par_cpa = await asyncio.gather(
            controller.fetch(
                self.effectif_cpa_terr.format(schema=schema_region), zone, zone_id
            ),
            controller.fetch("select * from strategie_territoire.effectif_cpa_france"),
            controller.fetch(
                "select cpa, ratio_emploi_va::float as ratio from strategie_territoire.emploi_va_cpa"
            ),
        )

        emploi_par_cpa_df = pd.DataFrame.from_dict(
            [dict(row) for row in emploi_par_cpa]
        )
        emploi_par_cpa_df.set_index("cpa", inplace=True)

        if not inputs or not isinstance(inputs, dict):
            return "(0,0) limit 0"
        output = []
        for action, input_params in inputs.items():
            if not input_params:
                continue
            specific_elements = input_params.get("advanced", {}).get("economique", None)
            if not specific_elements:
                continue
            output += ["({},{})".format(i, p) for i, p in specific_elements.items()]
        if not output:
            return "(0,0) limit 0"

        local_share = ",".join(output)

        def get_user_param_for_year(year):
            if not inputs or not isinstance(inputs, dict):
                return {}
            output = {}
            for action, input_params in inputs.items():
                if not input_params:
                    continue
                output = {
                    **output,
                    **{
                        key: value
                        for key, values in input_params.get("subactions", {}).items()
                        for cyear, value in values.items()
                        if input_params and cyear == year
                    },
                }
            return output

        user_action_params = await actions_data.get_user_action_params(region)

        # paramètres avancés de l'action 5 que l'on souhaite utiliser dans l'impact emploi
        reseau_chaleur = (
            inputs["5"]["advanced"]["advanced"]["5"]["parametres_avances"]
            if inputs.get("5", False)
            else False
        )
        bois_energie = (
            inputs["13"]["advanced"]["advanced"]["13"]["parametres_avances"]
            if inputs.get("13", False)
            else False
        )

        if not reseau_chaleur:
            reseau_chaleur = (
                user_action_params["5"]["parametres_avances"]
                .set_index("nom")["valeur"]
                .to_dict()
            )

        if not bois_energie:
            bois_energie = (
                user_action_params["13"]["parametres_avances"]
                .set_index("nom")["valeur"]
                .to_dict()
            )

        for cle in bois_energie:
            bois_energie[cle] = float(bois_energie[cle])

        # Calculs spécifique pour l'action 20
        nb_buscars_territoire = await specific_loaders.nb_bus_cars(
            region, zone, zone_id
        )
        total = next(
            (x["nb"] for x in nb_buscars_territoire if x["energie"] == "total"), None
        )

        buscars = {}
        """
        Pour l'action 20 (Motorisation alternative), l'utilisateur saisit des pourcentages. On a donc par année :
            {20_bus_elec}
            {20_bus_gnv}
        qui contiennent des pourcentages.
        A partir de ces pourcentages, on retrouve le Nb de bus et le nombre de cars en plus (fabriqués) à insérer dans la requête SQL (selon le territoire)
        """
        buscars["nb_bus"] = total  # on n'a pas le détail donc on fait 50 / 50

        # TODO il faut également faire l'interpolation sur les années non renseignées

        """
        TODO  :
        et si 10 bus en 2020,  et si en 2040 on en veux 20. Il faudra en construire 20 (et non 10, car les 10 premiers seront obsolètes et il faudra les renouveler)
        fonction pour récupérer le nb de bus en fonction du pourcentage saisie par l'utilisateur
        """

        # Cache pour ne pas lancer une même requête x fois
        cache_requetes = {
            zone: defaultdict(),
            "region": defaultdict(),
        }

        # XXX dag question : comprends pas bien encore cette variable "year_params"
        for index, year in enumerate(sorted(years)):
            # on semble récupérer les valeurs des paramètres d'actions pour l'année en cours (de la boucle)
            year_params = get_user_param_for_year(year)
            if not year_params:
                logger.error(
                    "No year params for year {} (case without parameters)".format(year)
                )
                continue

            year_results = {
                "direct": defaultdict(dict),
                "indirect": defaultdict(dict),
            }

            direct_ponctuel = {}
            direct_perenne = {}
            demande_ponctuelle = {}
            demande_perenne = {}
            investissement = {}
            # Préparation des requêtes
            regional_share = "(0,0) limit 0"

            direct_ponctuel_sql = self.impact_direct(
                schema_region,
                local_share,
                actions,
                year_params,
                action_filter_str,
                reseau_chaleur,
                bois_energie,
                buscars,
                True,
                difference_conso,
                emission_co2_par_industrie,
                year,
            )
            direct_perenne_sql = self.impact_direct(
                schema_region,
                local_share,
                actions,
                year_params,
                action_filter_str,
                reseau_chaleur,
                bois_energie,
                buscars,
                False,
                None,
                None,
                None,
            )
            demande_ponctuelle_sql = self.demande_indirecte(
                schema_region,
                local_share,
                actions,
                year_params,
                action_filter_str,
                True,
                reseau_chaleur,
                bois_energie,
                buscars,
                difference_conso,
                emission_co2_par_industrie,
                year,
            )
            demande_perenne_sql = self.demande_indirecte(
                schema_region,
                local_share,
                actions,
                year_params,
                action_filter_str,
                False,
                reseau_chaleur,
                bois_energie,
                buscars,
                None,
                None,
                None,
            )
            direct_ponctuel_region_sql = self.impact_direct(
                schema_region,
                regional_share,
                actions,
                year_params,
                action_filter_str,
                reseau_chaleur,
                bois_energie,
                buscars,
                True,
                difference_conso,
                emission_co2_par_industrie,
                year,
            )
            direct_perenne_region_sql = self.impact_direct(
                schema_region,
                regional_share,
                actions,
                year_params,
                action_filter_str,
                reseau_chaleur,
                bois_energie,
                buscars,
                False,
                None,
                None,
                None,
            )
            demande_ponctuelle_region_sql = self.demande_indirecte(
                schema_region,
                regional_share,
                actions,
                year_params,
                action_filter_str,
                True,
                reseau_chaleur,
                bois_energie,
                buscars,
                difference_conso,
                emission_co2_par_industrie,
                year,
            )
            demande_perenne_region_sql = self.demande_indirecte(
                schema_region,
                regional_share,
                actions,
                year_params,
                action_filter_str,
                False,
                reseau_chaleur,
                bois_energie,
                buscars,
                None,
                None,
                None,
            )
            calcul_investissement_sql = self.calcul_investissement(
                actions,
                year_params,
                action_filter_str,
                reseau_chaleur,
                bois_energie,
                buscars,
                difference_conso,
                emission_co2_par_industrie,
                year,
            )

            # Lancement des requêtes
            direct_ponctuel[zone] = (
                cache_requetes[zone][direct_ponctuel_sql]
                if direct_ponctuel_sql in cache_requetes[zone]
                else await controller.fetch(direct_ponctuel_sql, zone, zone_id)
            )
            direct_perenne[zone] = (
                cache_requetes[zone][direct_perenne_sql]
                if direct_perenne_sql in cache_requetes[zone]
                else await controller.fetch(direct_perenne_sql, zone, zone_id)
            )
            demande_ponctuelle[zone] = (
                cache_requetes[zone][demande_ponctuelle_sql]
                if demande_ponctuelle_sql in cache_requetes[zone]
                else await controller.fetch(demande_ponctuelle_sql, zone, zone_id)
            )
            demande_perenne[zone] = (
                cache_requetes[zone][demande_perenne_sql]
                if demande_perenne_sql in cache_requetes[zone]
                else await controller.fetch(demande_perenne_sql, zone, zone_id)
            )
            investissement[year] = (
                cache_requetes[zone][calcul_investissement_sql]
                if calcul_investissement_sql in cache_requetes[zone]
                else await controller.fetch(calcul_investissement_sql)
            )
            if zone != "region":
                # lors d'un choix de territoire (hors région) les impacts
                # sont également calculés pour la région mais nous ne devons pas utiliser
                # les parts locales qui proviennent de l'interface (destinées au territoire sélectionné et non à la région)
                direct_ponctuel["region"] = (
                    cache_requetes["region"][direct_ponctuel_region_sql]
                    if direct_ponctuel_region_sql in cache_requetes["region"]
                    else await controller.fetch(
                        direct_ponctuel_region_sql, "region", code_region
                    )
                )
                direct_perenne["region"] = (
                    cache_requetes["region"][direct_perenne_region_sql]
                    if direct_perenne_region_sql in cache_requetes["region"]
                    else await controller.fetch(
                        direct_perenne_region_sql, "region", code_region
                    )
                )
                demande_ponctuelle["region"] = (
                    cache_requetes["region"][demande_ponctuelle_region_sql]
                    if demande_ponctuelle_region_sql in cache_requetes["region"]
                    else await controller.fetch(
                        demande_ponctuelle_region_sql, "region", code_region
                    )
                )
                demande_perenne["region"] = (
                    cache_requetes["region"][demande_perenne_region_sql]
                    if demande_perenne_region_sql in cache_requetes["region"]
                    else await controller.fetch(
                        demande_perenne_region_sql, "region", code_region
                    )
                )

            # On met en cache les résultats par requête
            cache_requetes[zone][direct_ponctuel_sql] = direct_ponctuel[zone]
            cache_requetes[zone][direct_perenne_sql] = direct_perenne[zone]
            cache_requetes[zone][demande_ponctuelle_sql] = demande_ponctuelle[zone]
            cache_requetes[zone][demande_perenne_sql] = demande_perenne[zone]
            cache_requetes["region"][direct_ponctuel_region_sql] = direct_ponctuel[
                "region"
            ]
            cache_requetes["region"][direct_perenne_region_sql] = direct_perenne[
                "region"
            ]
            cache_requetes["region"][demande_ponctuelle_region_sql] = (
                demande_ponctuelle["region"]
            )
            cache_requetes["region"][demande_perenne_region_sql] = demande_perenne[
                "region"
            ]
            cache_requetes[zone][calcul_investissement_sql] = investissement[year]

            for current_zone, current_zone_id in {
                (zone, zone_id),
                ("region", code_region),
            }:
                rdp = {"va_totale": 0, "nb_emploi_total": 0}
                indirect_perenne = {"va_totale": 0, "nb_emploi_total": 0}
                # ponctuel
                year_results["direct"][current_zone]["ponctuel"] = [
                    dict(r) for r in direct_ponctuel[current_zone]
                ][0]
                year_results["indirect"][current_zone]["ponctuel"] = (
                    self.impact_indirect(
                        cpa_terr[current_zone],
                        cpa_france,
                        demande_ponctuelle[current_zone],
                        emploi_par_cpa_df,
                    )
                )

                for x in actions:
                    if x == "21":
                        # on ajoute les ETP et VA  des indutries sans CSC pour l'action 21 : réduction des émissions GES non-ener dans l'industrie
                        year_results["direct"][current_zone]["ponctuel"][
                            "va_totale"
                        ] += self.calcul_ETP_VA_indus_non_ener(
                            emission_co2_par_industrie[year]
                        )[
                            "va_totale"
                        ]
                        year_results["direct"][current_zone]["ponctuel"][
                            "nb_emploi_total"
                        ] += self.calcul_ETP_VA_indus_non_ener(
                            emission_co2_par_industrie[year]
                        )[
                            "nb_emploi_total"
                        ]

                # résultat direct perenne
                if demande_perenne[
                    current_zone
                ]:  # NOTE : !!! A voir si on le supprime ou pas !
                    rdp = [dict(r) for r in direct_perenne[current_zone]][0]
                    indirect_perenne = self.impact_indirect(
                        cpa_terr[current_zone],
                        cpa_france,
                        demande_perenne[current_zone],
                        emploi_par_cpa_df,
                    )

                if index == 0:
                    # la première année pas d'emploi pérennes
                    year_results["indirect"][current_zone]["perenne"] = {
                        "va_totale": 0,
                        "nb_emploi_total": 0,
                    }
                    year_results["direct"][current_zone]["perenne"] = {
                        "va_totale": 0,
                        "nb_emploi_total": 0,
                    }
                else:
                    # on cumule les 20 dernières années de pérenne sans ajouter l'année en cours
                    year_results["direct"][current_zone]["perenne"] = {
                        "nb_emploi_total": sum(
                            nb_emploi_perenne_direct[current_zone][-30:]
                        ),
                        "va_totale": sum(va_totale_perenne_direct[current_zone][-30:]),
                    }
                    year_results["indirect"][current_zone]["perenne"] = {
                        "va_totale": sum(
                            va_totale_perenne_indirect[current_zone][-30:]
                        ),
                        "nb_emploi_total": sum(
                            nb_emploi_perenne_indirect[current_zone][-30:]
                        ),
                    }

                # on augmente les indicateurs pérennes pour l'année suivante
                nb_emploi_perenne_direct[current_zone].append(rdp["nb_emploi_total"])
                va_totale_perenne_direct[current_zone].append(rdp["va_totale"])
                nb_emploi_perenne_indirect[current_zone].append(
                    indirect_perenne["nb_emploi_total"]
                )
                va_totale_perenne_indirect[current_zone].append(
                    indirect_perenne["va_totale"]
                )
            results[year] = year_results

            # on ajoute la somme des investissements réalisés
            results[year]["investissement"] = investissement[year][0][0]

            # on ajoute l'invesstissement des indutries sans CSC pour l'action 21 : réduction des émissions GES non-ener dans l'industrie
            for x in actions:
                if x == "21":
                    results[year][
                        "investissement"
                    ] += self.calcul_investissement_indus_non_ener(
                        emission_co2_par_industrie[year]
                    )

            # ajout du mix énergétique pour l'année en cours
            if year in mix_energetique:
                results[year]["mix"] = mix_energetique[year]

        return results
