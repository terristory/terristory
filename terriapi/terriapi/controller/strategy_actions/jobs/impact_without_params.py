# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
import asyncio
import math
from collections import defaultdict
from decimal import Decimal

import pandas as pd
from sanic.log import logger

from terriapi import controller
from terriapi.controller.strategy_actions import (
    BehaviorWhenMissing,
    DataRequisite,
    DataSet,
    Sector,
)
from terriapi.controller.strategy_actions.loaders import DataLoader, actions_data
from terriapi.controller.strategy_actions.passage_table import PassageTable

from .impact_emploi import ImpactEmploi


class ImpactEmploiSansParam(ImpactEmploi):
    """
    Cette classe hérite de la classe Impact Emploi, elle permet de calculer les impacts économiques pour les actions
    sans paramétres économique stocké dans strategie_territoire.action_details (cout global, ...)
    """

    async def capital_net_fixe(self, region, zone, zone_id):
        """
        Returns the fixed capital.

        Parameters
        ----------
        region : str
            Name of region
        zone : str
            Territory type
        zone_id : str
            Territory id

        Returns
        -------
        float
        """
        schema = region.replace("-", "_")
        if zone != "region":
            sql = """
                select code from {schema}.region;
            """.format(
                schema=schema
            )
            id = await controller.fetch(sql)
            region_id = id[0]["code"]
        else:
            region_id = zone_id

        keys_list = [
            DataSet.CONSUMPTION,
            Sector.AGRICULTURE,
            Sector.INDUSTRY_WITHOUT_ENERGY,
        ]
        passage_table = PassageTable(region, "Main passage table")
        await passage_table.load(keys_list)
        data_loader = DataLoader(passage_table)
        requisite = DataRequisite(
            "region_energy_consumpton", BehaviorWhenMissing.Types.FAIL, *keys_list
        )
        await data_loader.load(region, zone, zone_id, [requisite])
        data_territory = data_loader.get(requisite)
        if data_territory[["SECTOR", "valeur"]].empty:
            total_conso_territory = 0
        else:
            total_conso_territory = (
                data_territory[["SECTOR", "valeur"]].groupby("SECTOR").sum()
            )

        await data_loader.load(region, "region", region_id, [requisite])
        data_region = data_loader.get(requisite)
        total_conso_region = data_region[["SECTOR", "valeur"]].groupby("SECTOR").sum()

        # get regional fixed capital
        request = """
            select 
            * from strategie_territoire.fixed_capital
            where region = $1;
            """

        result = await controller.fetch(request, region)

        ## regional fixed capital for industry
        capital_industry = next(
            (x["value"] for x in result if x["category"] == "industry"), 0
        )
        ## regional fixed capital for agriculture
        capital_agriculture = next(
            (x["value"] for x in result if x["category"] == "agriculture"), 0
        )

        # calculate fixed capital for territory
        industry = (
            capital_industry
            * total_conso_territory.loc[Sector.INDUSTRY_WITHOUT_ENERGY, "valeur"]
            / total_conso_region.loc[Sector.INDUSTRY_WITHOUT_ENERGY, "valeur"]
        )
        agriculture = (
            capital_agriculture
            * total_conso_territory.loc[Sector.AGRICULTURE, "valeur"]
            / total_conso_region.loc[Sector.AGRICULTURE, "valeur"]
        )

        return {"industry": industry, "agriculture": agriculture}

    def find_last_investissement(self, action, year_investissement, year, year_params):
        """Cette fonction permet de récupérer l'investissement cumulé des années précédents
            l'année passée en paramètres

        Paramètres
        ----------
        action : str
        year_investissement : dict
            dataframe des investissement par année jusqu'à l'année passé en paramètres
        Returns
        -------
        float
            investissement cumulé des années précédents l'année passée en paramètres
        """
        valeur = 0
        user_parameter = 0
        if action == "15":
            user_parameter = float(year_params["15_conso_agri_totale"])
        if action == "14":
            user_parameter = float(year_params["14_conso_indus_totale"])

        # si le parametre est nulle, inutile de calculer
        if user_parameter == 0:
            return Decimal(0)

        # parcourir la liste des investissement des années précédents l'année passée en paramètres
        # pour récupérer l'investissement cumulé s'il existe
        for key in year_investissement:
            valeur += year_investissement[key][0]

        return valeur

    async def calcul_investissement_agriculture(
        self,
        action,
        year_investissement,
        year_params,
        year,
        indice_consommation,
        capital_net_fixe,
    ):
        """Cette fonction permet de calculer l'investissement dans le secteur agriculture

        Paramètres
        ----------
        action : str
        year : str
        year_params: dict
        year_investissement : dict
            dataframe des investissement par année jusqu'à l'année passé en paramètres
        indice_consommation: float
            la consommation totale de l'année en cours divisé par la consommation totale de dernière donées
            de disponibilité de données en BD

        Returns
        -------
        float
            investissement de l'année passé en paramètres
        """

        user_parameter = float(year_params["15_conso_agri_totale"])
        # si le parametre est nulle, inutile de calculer
        if user_parameter == 0:
            return Decimal(0)

        # Capital fixe net total estimé en AURA & Projection suivant le scénario AURA2030 (2018)
        CFNAURA = float(capital_net_fixe)

        # Hypothèses pour le scénario AURA2030
        # Elasticité de substitution K-E
        ESAURA = 0.46

        # Param. de distrib. coûts (K/K+E)
        ParamDistAURA = 0.8337

        # Hypothèses pour le scénario de référence
        # Elasticité de substitution K-E
        ESRef = 0.45
        # Param. de distrib. coûts (K/K+E)
        ParamDistRef = 0.8470

        # calculer l'indice d'évolution du capital utilisé scénario AURA 2030
        # etape 1 : calculer la puissance pour scénarion AURA 2030
        puissance_hyp_aura = (ESAURA - 1) / ESAURA
        puissance_hyp_aura_inverse = ESAURA / (ESAURA - 1)
        # l'indice de production est fixé à 1
        # NOTE: cette indice de prodution est le même pour le scénario AURA 2030 et le scénario référence
        indice_prod_AURA = 1
        # etape 2 : calculer l'indice d'évolution
        partI = math.pow(indice_prod_AURA, puissance_hyp_aura)
        partII = math.pow(indice_consommation.get(str(year), 1.0), puissance_hyp_aura)
        result1 = (1 - ParamDistAURA) * partII
        result2 = (partI - result1) / ParamDistAURA
        indice_evolution_aura = math.pow(result2, puissance_hyp_aura_inverse)

        # calculer l'indice d'évolution du capital utilisé scénario Référence
        # etape 1 : calculer la puissance pour scénarion Référence
        puissance_hyp_ref = (ESRef - 1) / ESRef
        puissance_hyp_ref_inverse = ESRef / (ESRef - 1)
        # NOTE on utilise indice de production scénario AURA 2030
        # etape 2 : calculer l'indice d'évolution
        result1_ref = math.pow(indice_prod_AURA, puissance_hyp_ref)
        result2_ref = (1 - ParamDistRef) * result1_ref
        resultFinal = (result1_ref - result2_ref) / ParamDistRef
        indice_evolution_ref = math.pow(resultFinal, puissance_hyp_ref_inverse)

        # calculer l'indice d'évolution
        # Différence de capital mobilisé entre les deux scénarios (Indice)
        indice_evolution = indice_evolution_aura - indice_evolution_ref

        # calculer l'investissement cumulé
        # Différence de capital mobilisé entre les deux scénarios (Stock en Milliers €)
        inves_cumulee = Decimal(indice_evolution * CFNAURA * 1000000)

        # récupérer le dernier investissement de la dernière année de réduction
        inves_precedant = self.find_last_investissement(
            action, year_investissement, year, year_params
        )

        # soustraction entre l'investissement cumulé et l'investissement de la dernière année de réduction s'il existe
        inves = inves_cumulee - inves_precedant

        return Decimal(round(inves, 3))

    async def calcul_investissement_indus(
        self,
        action,
        year_investissement,
        year_params,
        year,
        indice_consommation,
        capital_net_fixe,
    ):
        """Cette fonction permet de calculer l'investissement dans le secteur industriel

        Paramètres
        ----------
        action : str
        year : str
        year_params: dict
        year_investissement : dict
            dataframe des investissement par année jusqu'à l'année passé en paramètres
        indice_consommation: float
            la consommation totale de l'année en cours divisé par la consommation totale de dernière donées
            de disponibilité de données en BD

        Returns
        -------
        float
            investissement de l'année passé en paramètres
        """
        user_parameter = float(year_params["14_conso_indus_totale"])
        # si le parametre est nulle, inutile de calculer
        if user_parameter == 0:
            return Decimal(0)

        # Capital fixe net total estimé en AURA (2015)
        CFNAURA = float(capital_net_fixe)

        # Hypothèses pour le scénario AURA2030
        # Elasticité de substitution K-E
        ESAURA = 0.46

        # Param. de distrib. coûts (K/K+E)
        ParamDistAURA = 0.7187

        # Hypothèses pour le scénario de référence
        # Elasticité de substitution K-E
        ESRef = 0.46
        # Param. de distrib. coûts (K/K+E)
        ParamDistRef = 0.7187

        # calculer l'indice d'évolution du capital utilisé scénario AURA 2030
        # etape 1 : calculer la puissance pour scénarion AURA 2030
        puissance_hyp_aura = (ESAURA - 1) / ESAURA
        puissance_hyp_aura_inverse = ESAURA / (ESAURA - 1)

        # l'indice de production est fixé à 1
        # NOTE: cette indice de prodution est le même pour le scénario AURA 2030 et le scénario référence
        indice_prod_AURA = 1
        # etape 2 : calculer l'indice d'évolution
        partI = math.pow(indice_prod_AURA, puissance_hyp_aura)
        partII = math.pow(indice_consommation.get(str(year), 1.0), puissance_hyp_aura)
        result1 = (1 - ParamDistAURA) * partII
        result2 = (partI - result1) / ParamDistAURA
        indice_evolution_aura = math.pow(result2, puissance_hyp_aura_inverse)

        # calculer l'indice d'évolution du capital utilisé scénario Référence
        # etape 1 : calculer la puissance pour scénarion Référence
        puissance_hyp_ref = (ESRef - 1) / ESRef
        puissance_hyp_ref_inverse = ESRef / (ESRef - 1)
        # NOTE : on utilise indice de production scénario AURA 2030
        # etape 2 : calculer l'indice d'évolution
        result1_ref = math.pow(indice_prod_AURA, puissance_hyp_ref)
        result2_ref = (1 - ParamDistRef) * result1_ref
        resultFinal = (result1_ref - result2_ref) / ParamDistRef
        indice_evolution_ref = math.pow(resultFinal, puissance_hyp_ref_inverse)

        # calculer l'indice d'évolution
        # différence de capital mobilisé entre les deux scénarios (Indice)
        indice_evolution = indice_evolution_aura - indice_evolution_ref

        # calculer l'investissement cumulé
        # différence de capital mobilisé entre les deux scénarios (Stock en Milliers €)
        inves_cumulee = Decimal(indice_evolution * CFNAURA * 1000000)

        # récupérer l'investissement de la dernière année de réduction
        inves_precedant = self.find_last_investissement(
            action, year_investissement, year, year_params
        )

        # soustraction entre l'investissement cumulé et l'investissement de la dernière année de réduction s'il existe
        inves = inves_cumulee - inves_precedant

        return Decimal(round(inves, 3))

    async def calcul_valeur_ajoutee_nbre_emploi(self, region, zone, investissement):
        """Cette fonction permet de calculer la valeur ajoutée et le nombre d'emplois générés
            pour la région et le territoire pour les actions 14 et 15: industrie et agriculture

        Paramètres
        ----------
        region : str
        zone : str
        investissement: float

        Returns
        -------
        dict
            la valeur ajoutée et le nombre d'emplois générés pour la region et le territoire
        """

        # Ratio valeur ajoutée pour les actions industrie : 14 et agriculture : 15
        RatioVA = 0.5
        # Ratio nombre d'emplois générés pour les actions industrie : 14 et agriculture : 15
        RationETP = 13.52

        # récupérer les parts local par code CPA
        part_local_par_CPA = await actions_data.get_single_action_params(
            region, "parts_investissement_VA_ETP"
        )

        # calculer la valeur ajoutée et le nombre d'emplois générés
        part_locale_territoire_VA = []
        part_locale_territoire_ETP = []

        # k€ => Millions €
        investissement = investissement / 1000

        # etape 1 : calculer la valeur ajoutée pour la région
        VA_region = float(investissement) * RatioVA
        ETP_region = VA_region * RationETP

        # si la zone est différente de la région
        # calculer la part local
        if zone != "region":
            for index, row in part_local_par_CPA.iterrows():
                # calculer la part d'investissement par CPA
                inves_par_cpa = float(investissement) * row["part_invess"]

                # calculer la valeur ajoutée par CPA
                VA_region_par_cpa = inves_par_cpa * RatioVA
                # calculer la part local de la valeur ajoutée par CPA
                VA_territoire_par_cpa = VA_region_par_cpa * row["part_VA_ETP"]

                # calculer le nombre d'emploi par CPA
                ETP_region_par_cpa = inves_par_cpa * RationETP
                # calculer la part local de nombre d'emplois crées par CPA
                ETP_territoire_par_cpa = ETP_region_par_cpa * row["part_VA_ETP"]

                # stocker les valeurs ajoutées et les emplois crées par CPA
                part_locale_territoire_VA.append(VA_territoire_par_cpa)
                part_locale_territoire_ETP.append(ETP_territoire_par_cpa)

                # NOTE : la valeur ajoutée local égale la somme des parts locals par CPA
                # NOTE : le nombre d'emplois locals égale la somme des parts locals par CPA

        resultat = {
            "region": {
                "va_totale": Decimal(VA_region),
                "nb_emploi_total": Decimal(ETP_region),
            },
            "territoire": {
                "va_totale": Decimal(sum(part_locale_territoire_VA)),
                "nb_emploi_total": Decimal(sum(part_locale_territoire_ETP)),
            },
        }

        return resultat

    async def calcul_investissement_agriculture_non_ener(
        self, emissions_co2_par_sous_action
    ):
        """Cette fonction permet de calculer l'investissement dans le secteur agriculture

        Paramètres
        ----------
        emissions_co2_par_sous_action : dict
            Taux d'émission de co2

        Returns
        -------
        float
            investissement de l'année passé en paramètres
        """

        A = emissions_co2_par_sous_action["taux_methanisation"] * 1000
        B = emissions_co2_par_sous_action["taux_reduction_engrais_mineral"] * 1000
        C = emissions_co2_par_sous_action["taux_substitution_azote_mineral"] * 1000
        D = emissions_co2_par_sous_action["taux_diminution_azote"] * 1000
        E = emissions_co2_par_sous_action["taux_substitution_glucides"] * 1000
        F = emissions_co2_par_sous_action["taux_nitrate"] * 1000
        G = emissions_co2_par_sous_action["taux_agroforesterie"] * 1000
        H = emissions_co2_par_sous_action["taux_haies_parcelles_agricoles"] * 1000

        inves = (
            A * 88854.70279
            + B * 29.52380952
            + C * 6.726457399
            + D * 1323.100968
            + E * 519.047619
            + F * 35.11688312
            + G * 13.24324324
            + H * 62.5
        )
        inves /= 1000.0

        return Decimal(round(inves, 3))

    def calcul_part_local(self, cpa_terr, cpa_fr):
        """permet de calculer la part régional et la part local
        Paramètres
        ----------
        cpa_terr : dict
            effectifs territoire par code CPA
        cpa_fr : dict
            effectifs France par code CPA
        Returns
        -------
        dataframe : part local par code CPA
        """

        mterr = pd.DataFrame.from_dict([dict(row) for row in cpa_terr])
        mfr = pd.DataFrame.from_dict([dict(row) for row in cpa_fr])
        sum_terr = mterr["effectif_cpa"].sum()
        sum_fr = mfr["effectif_cpa"].sum()

        df = mterr.merge(mfr, on="cpa")
        df["effectif_cpa_x"] = df["effectif_cpa_x"].div(sum_terr)
        df["effectif_cpa_y"] = df["effectif_cpa_y"].div(sum_fr)
        df["ratio"] = df["effectif_cpa_x"].div(df["effectif_cpa_y"])

        df["part_local"] = df["ratio"].multiply(
            Decimal(math.pow(math.log2(1 + (sum_terr / sum_fr)), 0.3))
        )

        df.loc[df["part_local"] > 1, "part_local"] = 1

        return df

    effectif_cpa_terr = """
        with tmp as (
            select
                sum(nbter.nb_emploi) nbter
                , n.a88::int
                , a.emploi_interieur_salarie_nb_pers as eisnp
                , a.emploi_interieur_total_nb_pers * 1000 as eitnb
                , (a.emploi_interieur_salarie_nb_pers / a.emploi_interieur_total_nb_pers) as pse
                , n.cpa
            from {schema}.nb_emploi_territoire_view nbter
            join strategie_territoire.naf n on n.a88 = nbter.a88
            join strategie_territoire.a88 a on a.naf = n.id
            where nbter.territoire = $1 and nbter.territoire_id = $2
            group by n.a88, n.cpa, a.emploi_interieur_salarie_nb_pers, a.emploi_interieur_total_nb_pers
        )
        select
            cpa,
            sum(nbter / pse) as effectif_cpa
        from tmp
        group by cpa
        """

    async def calcul_va_etp_agriculture_non_ener(
        self, region, zone, investissement, part_local_va_par_CPA
    ):
        """Cette fonction permet de calculer la valeur ajoutée et le nombre d'emplois générés
            pour la région et le territoire pour l'action 18 : Réduction des émission GES non énergétiques dans l'agriculture

        Paramètres
        ----------
        region : str
        zone : str
        investissement: float

        Returns
        -------
        dict
            la valeur ajoutée et le nombre d'emplois générés pour la region et le territoire
        """

        # Ratio M€ VA par M€ Investissement
        RatioVA = 0.579421270987696

        # Ratio Emploi par M€ VA
        RationETP = 17.836702957634

        # calculer la valeur ajoutée et le nombre d'emplois générés
        part_locale_territoire_VA = []
        part_locale_territoire_ETP = []

        # k€ => Millions €
        investissement = investissement / 1000

        # etape 1 : calculer la valeur ajoutée pour la région
        VA_national = float(investissement) * RatioVA
        ETP_national = VA_national * RationETP

        # calculer ETP et VA par CPA
        for index, row in part_local_va_par_CPA.iterrows():
            # calculer la part local de la valeur ajoutée par CPA
            VA_territoire_par_cpa = (
                float(row["repartition_valeur_ajoutee_ponctuel"])
                * float(row["part_local"])
                * VA_national
            )

            # calculer la part local de nombre d'emplois crées par CPA
            ETP_territoire_par_cpa = (
                float(row["repartition_emploi_ponctuel"])
                * float(row["part_local"])
                * ETP_national
            )

            # stocker les valeurs ajoutées et les emplois crées par CPA
            part_locale_territoire_VA.append(VA_territoire_par_cpa)
            part_locale_territoire_ETP.append(ETP_territoire_par_cpa)

            # NOTE : la valeur ajoutée local égale la somme des parts locals par CPA
            # NOTE : le nombre d'emplois locals égale la somme des parts locals par CPA

        resultat = {
            zone: {
                "va_totale": Decimal(sum(part_locale_territoire_VA)),
                "nb_emploi_total": Decimal(sum(part_locale_territoire_ETP)),
            }
        }

        return resultat

    async def calcule_impact_emploi(
        self,
        region,
        zone,
        zone_id,
        years,
        indice_consommation,
        emission_co2_par_sous_action,
        action_year,
        inputs,
        actions,
    ):
        """Calcule l'impact des actions sur les emplois, la valeur ajoutée et l'investissement

        Parameters
        ----------
        region : str
            Nom de la région (schéma)
        zone : str
            Nom du territoire
        zone_id : str
            Identifiant du territoire
        years : set
            Ensemble des années
        action_year : dict
            Liste des identifiants des actions par année
        action_params : dict
            Paramètres des actions
        advanced_params : dict
            Paramètres avancés des actions

        """
        inve_indus = (
            {}
        )  # pour stocker l'investissement cumulé de l'action 14, qui sert à calculer l'investissement ponctuel
        inve_agriculture = (
            {}
        )  # pour stocker l'investissement cumulé de l'action 15, qui sert à calculer l'investissement ponctuel
        inve_agriculture_non_ener = (
            {}
        )  # pour stocker l'investissement cumulé de l'action 22 qui sert à calculer l'investissement ponctuel
        investissement_indus_agri = defaultdict(
            lambda: Decimal(0)
        )  # pour sommer l'investissement des actions 14, 15 et 22 si'elles sont cochées ensemble dans la même année
        VA_ETP_indus = defaultdict(
            lambda: float
        )  # pour action 14 et 15 pour stocker l'investissement cumulé
        VA_ETP_agriculture = defaultdict(lambda: float)
        VA_ETP_agriculture_non_ener = defaultdict(lambda: float)  # pour l'action 22

        if action_year == {}:  # Si aucune action n'a été sélectionnée,
            return {}  # On retourne un dictionnaire vide

        schema_region = region.replace("-", "_")

        # tableau des résultats finaux pour toutes les années
        results = {}
        cpa_terr = {}

        code_region = await controller.fetch(f"select code from {schema_region}.region")
        code_region = code_region[0]["code"]

        # capitale net fixe estimé en aura pour l'industrie et l'agriculture
        capital_net_fixe = await self.capital_net_fixe(region, zone, zone_id)

        # récupérer les répartitions VA et ETP par code CPA
        request = "select * from strategie_territoire.cpa"
        res = await controller.fetch(request)
        repartition_VA_par_CPA = pd.DataFrame.from_dict([dict(row) for row in res])

        # calculs indépendants de l'année :
        cpa_terr[zone], cpa_france = await asyncio.gather(
            controller.fetch(
                self.effectif_cpa_terr.format(schema=schema_region), zone, zone_id
            ),
            controller.fetch("select * from strategie_territoire.effectif_cpa_france"),
        )

        part_local_par_CPA = self.calcul_part_local(cpa_terr[zone], cpa_france)
        part_local_va_par_CPA = part_local_par_CPA.merge(
            repartition_VA_par_CPA, left_on="cpa", right_on="code"
        )

        if zone != "region":
            cpa_terr["region"] = await controller.fetch(
                self.effectif_cpa_terr.format(schema=schema_region),
                "region",
                code_region,
            )
            part_region_par_CPA = self.calcul_part_local(cpa_terr["region"], cpa_france)
            part_region_va_par_CPA = part_region_par_CPA.merge(
                repartition_VA_par_CPA, left_on="cpa", right_on="code"
            )
        else:
            part_region_par_CPA = part_local_par_CPA
            part_region_va_par_CPA = part_local_va_par_CPA

        def get_user_param_for_year(year):
            if not inputs or not isinstance(inputs, dict):
                return {}
            output = {}
            for action, input_params in inputs.items():
                if not input_params:
                    continue
                output = {
                    **output,
                    **{
                        key: value
                        for key, values in input_params.get("subactions", {}).items()
                        for cyear, value in values.items()
                        if input_params and cyear == year
                    },
                }
            return output

        # XXX dag question : comprends pas bien encore cette variable "year_params"
        for index, year in enumerate(sorted(years)):
            # on semble récupérer les valeurs des paramètres d'actions pour l'année en cours (de la boucle)
            year_params = get_user_param_for_year(year)
            if not year_params:
                logger.error(
                    "No year params for year {} (case without parameters)".format(year)
                )
                continue
            year_results = {
                "direct": defaultdict(dict),
                "indirect": defaultdict(dict),
            }

            for action in actions:
                action_number = action.action_number
                # if no indice has been given, we can't compute investments
                # it must be because action is actually empty
                if (
                    action_number == "14"
                    and indice_consommation is not None
                    and len(indice_consommation) > 0
                ):
                    investissement_indus = await asyncio.gather(
                        self.calcul_investissement_indus(
                            action_number,
                            inve_indus,
                            year_params,
                            int(year),
                            indice_consommation[action.name],
                            capital_net_fixe["industry"],
                        )
                    )
                    inve_indus[year] = investissement_indus
                    investissement_indus_agri[year] += inve_indus[year][0]
                # if no indice has been given, we can't compute investments
                # it must be because action is actually empty
                elif (
                    action_number == "15"
                    and indice_consommation is not None
                    and len(indice_consommation) > 0
                ):
                    investissement_agriculture = await asyncio.gather(
                        self.calcul_investissement_agriculture(
                            action_number,
                            inve_agriculture,
                            year_params,
                            int(year),
                            indice_consommation[action.name],
                            capital_net_fixe["agriculture"],
                        )
                    )
                    inve_agriculture[year] = investissement_agriculture
                    investissement_indus_agri[year] += inve_agriculture[year][0]
                elif action_number == "22":
                    investissement_agriculture_non_ener = await asyncio.gather(
                        self.calcul_investissement_agriculture_non_ener(
                            emission_co2_par_sous_action[year]
                        )
                    )
                    inve_agriculture_non_ener[year] = (
                        investissement_agriculture_non_ener
                    )
                    investissement_indus_agri[year] += inve_agriculture_non_ener[year][
                        0
                    ]

            for current_zone, current_zone_id in {
                (zone, zone_id),
                ("region", code_region),
            }:
                year_results["direct"][current_zone]["ponctuel"] = {
                    "va_totale": 0,
                    "nb_emploi_total": 0,
                }
                year_results["indirect"][current_zone]["ponctuel"] = {
                    "va_totale": 0,
                    "nb_emploi_total": 0,
                }

                # ponctuel
                for action in actions:
                    action_number = action.action_number
                    # if no investment are available, nothing to do
                    # it must be because action is actually empty
                    if action_number == "14" and inve_indus.get(year, False):
                        VA_ETP_indus[year] = (
                            await self.calcul_valeur_ajoutee_nbre_emploi(
                                region, current_zone, inve_indus[year][0]
                            )
                        )
                        if current_zone == "region":
                            year_results["direct"]["region"]["ponctuel"][
                                "va_totale"
                            ] += VA_ETP_indus[year]["region"]["va_totale"]
                            year_results["direct"]["region"]["ponctuel"][
                                "nb_emploi_total"
                            ] += VA_ETP_indus[year]["region"]["nb_emploi_total"]
                        else:
                            year_results["direct"][current_zone]["ponctuel"][
                                "va_totale"
                            ] += VA_ETP_indus[year]["territoire"]["va_totale"]
                            year_results["direct"][current_zone]["ponctuel"][
                                "nb_emploi_total"
                            ] += VA_ETP_indus[year]["territoire"]["nb_emploi_total"]
                    # if no investment are available, nothing to do
                    # it must be because action is actually empty
                    elif action_number == "15" and inve_agriculture.get(year, False):
                        VA_ETP_agriculture[year] = (
                            await self.calcul_valeur_ajoutee_nbre_emploi(
                                region, current_zone, inve_agriculture[year][0]
                            )
                        )
                        if current_zone == "region":
                            year_results["direct"]["region"]["ponctuel"][
                                "va_totale"
                            ] += VA_ETP_agriculture[year]["region"]["va_totale"]
                            year_results["direct"]["region"]["ponctuel"][
                                "nb_emploi_total"
                            ] += VA_ETP_agriculture[year]["region"]["nb_emploi_total"]
                        else:
                            year_results["direct"][current_zone]["ponctuel"][
                                "va_totale"
                            ] += VA_ETP_agriculture[year]["territoire"]["va_totale"]
                            year_results["direct"][current_zone]["ponctuel"][
                                "nb_emploi_total"
                            ] += VA_ETP_agriculture[year]["territoire"][
                                "nb_emploi_total"
                            ]
                    elif action_number == "22":
                        if current_zone == "region":
                            VA_ETP_agriculture_non_ener[year] = (
                                await self.calcul_va_etp_agriculture_non_ener(
                                    region,
                                    current_zone,
                                    inve_agriculture_non_ener[year][0],
                                    part_region_va_par_CPA,
                                )
                            )
                        else:
                            VA_ETP_agriculture_non_ener[year] = (
                                await self.calcul_va_etp_agriculture_non_ener(
                                    region,
                                    current_zone,
                                    inve_agriculture_non_ener[year][0],
                                    part_local_va_par_CPA,
                                )
                            )
                        year_results["direct"][current_zone]["ponctuel"][
                            "va_totale"
                        ] += VA_ETP_agriculture_non_ener[year][current_zone][
                            "va_totale"
                        ]
                        year_results["direct"][current_zone]["ponctuel"][
                            "nb_emploi_total"
                        ] += VA_ETP_agriculture_non_ener[year][current_zone][
                            "nb_emploi_total"
                        ]

                # pas d'impacts indirect pour les actions 14 et 15
                year_results["indirect"][current_zone]["ponctuel"] = {
                    "va_totale": 0,
                    "nb_emploi_total": 0,
                }
                # pas d'impact pérennes pour les actions industrie : 14 et agriculture : 15
                year_results["indirect"][current_zone]["perenne"] = {
                    "va_totale": 0,
                    "nb_emploi_total": 0,
                }
                year_results["direct"][current_zone]["perenne"] = {
                    "va_totale": 0,
                    "nb_emploi_total": 0,
                }

            results[year] = year_results
            # on ajoute la somme des investissements réalisés
            results[year]["investissement"] = investissement_indus_agri[year]

        return results
