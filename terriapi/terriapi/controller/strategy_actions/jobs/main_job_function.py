# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""
module permettant de stocker toutes les étapes de
calcul concernant l'impact emploi
"""
import copy

from terriapi import controller

from .impact_emploi import ImpactEmploi
from .impact_with_params import ImpactEmploiAvecParam
from .impact_without_params import ImpactEmploiSansParam


class FabriqueImpactEmploi(ImpactEmploi):
    """Cette classe permet d'instancier dynamiquement les classes apropriées pour calculer les impacts emplois

    Paramètres
    ----------
    Cf. attributs de la classe mère
    representation : dict
        - l'invesstissmet
        - la valeur ajoutée
        - le nombre des emplois générée
    """

    async def meta_actions(self, actions):
        """
        DEPRECATED
        Cette fonction permet de récuperer les caractéristiques des actions
        """
        sql = """
        select numero as action, impact_eco, avec_params
        from strategie_territoire.meta_action where numero in ({action})""".format(
            action=actions
        )
        res = await controller.fetch(sql)
        meta_actions = [dict(row) for row in res]

        return meta_actions

    async def initialize_dict(self, region, zone, years):
        """Calcule l'impact des actions sur les emplois, la valeur ajoutée et l'investissement
        Parameters
        ----------
        current_zone : str
            Nom du territoire

        """

        schema_region = region.replace("-", "_")
        code_region = await controller.fetch(f"select code from {schema_region}.region")
        code_region = code_region[0]["code"]
        results = {}

        va_etp_dict = {key: 0 for key in ["va_totale", "nb_emploi_total"]}
        pp_dict = {key: copy.deepcopy(va_etp_dict) for key in ["ponctuel", "perenne"]}
        zone_dict = {key: copy.deepcopy(pp_dict) for key in [zone, "region"]}
        year_results = {key: copy.deepcopy(zone_dict) for key in ["direct", "indirect"]}
        year_results["investissement"] = 0

        # NOTE : à voir si je dois remplacer quelque #results[year] = year_results# par la ligne suivante
        results = {
            key: copy.deepcopy(year_results) for index, key in enumerate(sorted(years))
        }

        return results

    async def calcule_impact_emploi(
        self,
        region,
        zone,
        zone_id,
        years,
        mix_energetique,
        indice_consommation,
        difference_conso,
        emission_co2_par_industrie,
        emission_co2_par_sous_action,
        action_year,
        inputs,
    ):
        """Permet d'instancier la classe appropriée pour calculer l'impact des actions sur les emplois, la valeur ajoutée et l'investissement

        Parameters
        ----------
        region : str
            Nom de la région (schéma)
        zone : str
            Nom du territoire
        zone_id : str
            Identifiant du territoire
        years : set
            Ensemble des années
        action_year : dict
            Liste des identifiants des actions par année
        action_params : dict
            Paramètres des actions
        advanced_params : dict
            Paramètres avancés des actions

        """

        if action_year == {}:  # Si aucune action n'a été sélectionnée,
            return {}  # On retourne un dictionnaire vide

        schema_region = region.replace("-", "_")
        first_year = sorted(action_year.keys())[0]

        # on retire l'action de mix energétique non utilisée dans l'impact énergie
        only_energie_action = [
            act for act in action_year[first_year] if act.action_number != "8"
        ]

        actions_avec_params = [
            action
            for action in only_energie_action
            if action.settings.get("impact_eco", False) == True
            and action.settings.get("avec_params", True) == True
        ]
        # actions_avec_params = []
        actions_sans_params = [
            action
            for action in only_energie_action
            if action.settings.get("impact_eco", False) == True
            and action.settings.get("avec_params", False) == False
        ]
        # actions_sans_params = []

        code_region = await controller.fetch(f"select code from {schema_region}.region")
        code_region = code_region[0]["code"]

        # liste pour stocker les résultats de chaque type d'action
        year_results = []

        # tableau des résultats pour chaque type d'actions pour toutes les années
        results_actions_avec_params = {}
        results_actions_sans_params = {}

        # tableau des résultats finaux pour toutes les années
        results = {}
        # initialiser resultats avec des vaeurs 0 pour pouvoir sommer les résultats finaux
        results = await self.initialize_dict(region, zone, years)

        if len(actions_avec_params) > 0:
            donnees_representation_action = ImpactEmploiAvecParam()
            results_actions_avec_params = (
                await donnees_representation_action.calcule_impact_emploi(
                    region,
                    zone,
                    zone_id,
                    years,
                    mix_energetique,
                    indice_consommation,
                    difference_conso,
                    emission_co2_par_industrie,
                    action_year,
                    inputs,
                    actions_avec_params,
                )
            )
            year_results.append(results_actions_avec_params)

        if len(actions_sans_params) > 0:
            donnees_representation_action = ImpactEmploiSansParam()
            results_actions_sans_params = (
                await donnees_representation_action.calcule_impact_emploi(
                    region,
                    zone,
                    zone_id,
                    years,
                    indice_consommation,
                    emission_co2_par_sous_action,
                    action_year,
                    inputs,
                    actions_sans_params,
                )
            )
            year_results.append(results_actions_sans_params)

        # sommer les résultats de toutes les actions cochées
        for type_action in year_results:
            if type_action:
                for cle, val in type_action.items():
                    results[cle]["investissement"] += val["investissement"]
                    for current_zone, current_zone_id in {
                        (zone, zone_id),
                        ("region", code_region),
                    }:
                        results[cle]["direct"][current_zone]["ponctuel"][
                            "va_totale"
                        ] += val["direct"][current_zone]["ponctuel"]["va_totale"]
                        results[cle]["direct"][current_zone]["ponctuel"][
                            "nb_emploi_total"
                        ] += val["direct"][current_zone]["ponctuel"]["nb_emploi_total"]
                        results[cle]["direct"][current_zone]["perenne"][
                            "va_totale"
                        ] += val["direct"][current_zone]["perenne"]["va_totale"]
                        results[cle]["direct"][current_zone]["perenne"][
                            "nb_emploi_total"
                        ] += val["direct"][current_zone]["perenne"]["nb_emploi_total"]
                        results[cle]["indirect"][current_zone]["ponctuel"][
                            "va_totale"
                        ] += val["indirect"][current_zone]["ponctuel"]["va_totale"]
                        results[cle]["indirect"][current_zone]["ponctuel"][
                            "nb_emploi_total"
                        ] += val["indirect"][current_zone]["ponctuel"][
                            "nb_emploi_total"
                        ]
                        results[cle]["indirect"][current_zone]["perenne"][
                            "va_totale"
                        ] += val["indirect"][current_zone]["perenne"]["va_totale"]
                        results[cle]["indirect"][current_zone]["perenne"][
                            "nb_emploi_total"
                        ] += val["indirect"][current_zone]["perenne"]["nb_emploi_total"]

        return results
