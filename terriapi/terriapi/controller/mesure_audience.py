﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from __future__ import annotations

import datetime

from terriapi import controller


class MesureDAudience:
    """Obtention et insersion en base des données de navigation des utilisateur.rices.

    Parameters
    ----------
    region : str
        Nom de la région sans majuscule ni accent (ex : auvergne-rhone-alpes)
    ip : str
        Adresse ip du client
    """

    def __init__(self, region: str):
        self.region = region

        self.donnees_ip = {}

    @property
    def nom_schema(self):
        return self.region.replace("-", "_")

    @property
    def date(self):
        date = datetime.datetime.today()
        return date

    @property
    def ip_anonymisee(self):
        if not self.donnees_ip:
            return "Inconnu"

        ip = self.donnees_ip["geoplugin_request"].split(".")
        return (
            ip[0] + "." + ip[1] + "." + ip[2] + "." + controller.user.hash_pass(ip[3])
        )

    async def commune(self):
        """Retourne le nom de la commune depuis laquelle l'utilisateur.trice utilise TerriSTORY"""
        # Si ladresse IP est reconnue par l'API
        if not self.donnees_ip:
            return "Inconnu"
        longitude = self.donnees_ip["geoplugin_longitude"]
        latitude = self.donnees_ip["geoplugin_latitude"]
        # Requête qui retourne le nom de la commune à partir des coordonnées de l'ip
        # et des géométries des communes de France disponibles en base.
        try:
            conversion_proj = """
                            select nom
                            from {region}.commune_fr
                            where st_within(st_transform(st_setsrid(st_point({lon}, {lat}), 4326), 3857), geom)
                            """.format(
                region=self.nom_schema, lon=longitude, lat=latitude
            )
            commune = await controller.fetch(conversion_proj)
        except:
            return "Inconnu"
        if commune:
            return commune[0]["nom"]
        return "Inconnu"

    async def inserer_donnees_localisation(self, is_excluded: bool = False):
        """Insère les données de localisation de l'utilisateur.rice"""
        # On n'insère pas les données de longitude, latitude car il est interdit de stocker
        # des coordonnées précises (la maille la plus fine que nous sommes autorisé.es à stocker)
        # est la commune).
        req = "insert into consultations.ip_localisation(ip, commune, region_selectionnee, is_excluded) values($1, $2, $3, $4) returning id"
        commune = await self.commune()
        id_utilisateur = await controller.fetch(
            req, self.ip_anonymisee, commune, self.region, is_excluded
        )
        return id_utilisateur[0]["id"]


class MesureDAudienceTerritoriale(MesureDAudience):
    """
    Classe intermédiaire pour l'ensemble des mesures d'audience qui s'appuient
    sur des territoires et nécessitent donc de spécifier le type et le code de
    territoire lors de la consultation.

    Parameters
    ----------
    code_territoire : str
        Code du territoire sélectionné
    type_territoire : str
        Type de territoire sélectionné
    donnees_ip : str
        Données sur l'IP (géolocalisation approximative)
    """

    def __init__(
        self,
        region: str,
        code_territoire: str,
        type_territoire: str,
        donnees_ip: str | None = "",
    ):
        super().__init__(region)
        self.code_territoire = code_territoire
        self.type_territoire = type_territoire
        self.donnees_ip = donnees_ip


class MesureDAudienceIndicateurs(MesureDAudienceTerritoriale):
    """Insère les données de navigation dans la table propre à la consultation des indicateurs

    Parameters
    ----------
    provenance : str
        Page depuis laquelle l'indicateur est consulté (depuis l'inteface carto ou la page de création des tableau de bord)
    id_indicateur : int
        Identifiant unique de l'indicateur consulté
    """

    def __init__(
        self,
        provenance: str,
        region: str,
        code_territoire: str,
        type_territoire: str,
        id_indicateur,
    ):
        super().__init__(region, code_territoire, type_territoire)
        self.provenance = provenance
        self.id_indicateur = int(id_indicateur)

    async def inserer_donnees_consultations(self, id_utilisateur):
        """Insère les données de navigation propres aux indicateurs"""
        requete = """
                  INSERT INTO consultations.consultations_indicateurs (id_utilisateur, provenance, region, code_territoire, type_territoire, id_indicateur, date)
                  VALUES ($1, $2, $3, $4, $5, $6, $7)
                  """
        if (
            self.provenance != "undefined"
            and self.provenance != "test"
            and self.provenance != "integration_donnees_territorialsynthesis"
        ):
            await controller.fetch(
                requete,
                int(id_utilisateur),
                self.provenance,
                self.region,
                self.code_territoire,
                self.type_territoire,
                self.id_indicateur,
                self.date,
            )

        return {"id": id_utilisateur}


class MesureDAudiencePoi(MesureDAudienceTerritoriale):
    """Insère les données de consultation des couches POI

    Parameters
    ----------

    nom_couche : str
        Nom de la couche POI. Est préféré à l'ID pour ne pas être dépendant de l'état
        de la BDD à un instant donné
    cochee : booléen
        Renvoie True si la couche a été activée et False si elle a été désactivée
    """

    def __init__(
        self,
        region: str,
        code_territoire: str,
        type_territoire: str,
        nom_couche: str,
        cochee: str,
    ):
        super().__init__(region, code_territoire, type_territoire)
        self.nom_couche = nom_couche
        if cochee == "true":
            self.cochee = True
        else:
            self.cochee = False

    async def inserer_donnees_consultations(self, id_utilisateur):
        """Insère les données de navigation propres aux couches d'entités ponctuelles"""
        requete = """
                  INSERT INTO consultations.poi (id_utilisateur, region, code_territoire, type_territoire, nom_couche, cochee,  date)
                  VALUES ($1, $2, $3, $4, $5, $6, $7)
                  returning id
                  """
        await controller.fetch(
            requete,
            int(id_utilisateur),
            self.region,
            self.code_territoire,
            self.type_territoire,
            self.nom_couche,
            self.cochee,
            self.date,
        )
        return {"id": id_utilisateur}


class MesureDAudienceActions(MesureDAudienceTerritoriale):
    """Insère les données de navigation dans la table propre à la consultation des actions et trajectoires cibles

    Parameters
    ----------
    liste_actions : liste
        liste des actions cochées
    liste_trajectoires_cibles : liste
        liste des trajectoires cibles modifiées par l'utilisateur.rice
    type_action : str
        Précision quant à la manière dont les calculs sont lancés : lancement de calculs ou export ADEME ?
    """

    def __init__(
        self,
        region,
        code_territoire,
        type_territoire,
        liste_actions,
        liste_trajectoires_cibles,
        type_action,
    ):
        super().__init__(region, code_territoire, type_territoire)
        self.liste_actions = liste_actions
        self.liste_trajectoires_cibles = liste_trajectoires_cibles
        self.type_action = type_action

    async def inserer_donnees_consultations(self, id_utilisateur):
        """Insère les données de navigation propres aux actions et trajectoires cibles"""
        # On commence par insérer les données de localisation de l'IP (méthode héritée de MesureDAudience)
        requete = """
                  INSERT INTO consultations.actions_cochees (region, code_territoire, type_territoire, liste_actions, liste_trajectoires_cibles, type_action, date, id_utilisateur)
                  VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                  """
        if self.type_action != "test":
            await controller.fetch(
                requete,
                self.region,
                self.code_territoire,
                self.type_territoire,
                self.liste_actions,
                self.liste_trajectoires_cibles,
                self.type_action,
                self.date,
                int(id_utilisateur),
            )
        return {"id": "id_utilisateur"}


class MesureDAudienceAnalysesTerritoriales(MesureDAudienceTerritoriale):
    """Insère les données de navigation propres aux pages d'analyses territoriales

    Parameters
    ----------

    page : str
        Page consultée (suivi des indicateurs territoriaux ou analyses territoriales)
    """

    def __init__(self, region, code_territoire, type_territoire, page):
        super().__init__(region, code_territoire, type_territoire)
        self.page = page

    async def inserer_donnees_consultations(self, id_utilisateur):
        """Insère les données de navigation propres aux analyses territoriales"""
        # On commence par insérer les données de localisation de l'IP (méthode héritée de MesureDAudience)
        requete = """
                  INSERT INTO consultations.analyses_territoriales (id_utilisateur, region, code_territoire, type_territoire, page, date)
                  VALUES ($1, $2, $3, $4, $5, $6)
                  """
        await controller.fetch(
            requete,
            int(id_utilisateur),
            self.region,
            self.code_territoire,
            self.type_territoire,
            self.page,
            self.date,
        )
        return {"id": id_utilisateur}


class MesureDAudienceTableauxBords(MesureDAudienceTerritoriale):
    """Insère les données de consultation des tableaux de bords

    Parameters
    ----------
    tableau_bord_id : id
        ID du tableau de bord visionné
    tableau_bord_nom : str
        Nom du tableau de bord
    """

    def __init__(
        self,
        region,
        code_territoire,
        type_territoire,
        tableau_bord_id,
        tableau_bord_nom,
    ):
        super().__init__(region, code_territoire, type_territoire)
        self.tableau_bord_id = tableau_bord_id
        self.tableau_bord_nom = tableau_bord_nom

    async def inserer_donnees_consultations(self, id_utilisateur):
        """Insère les données de navigation propres aux couches d'entités ponctuelles"""
        requete = """
                  INSERT INTO consultations.tableaux_bords 
                  (id_utilisateur, region, code_territoire, type_territoire,
                    tableau_bord_id, tableau_bord_nom, date)
                  VALUES ($1, $2, $3, $4,
                    $5, $6, $7)
                  returning id
                  """
        await controller.fetch(
            requete,
            int(id_utilisateur),
            self.region,
            self.code_territoire,
            self.type_territoire,
            self.tableau_bord_id,
            self.tableau_bord_nom,
            self.date,
        )
        return {"id": id_utilisateur}


class MesureDAudienceAutresPages(MesureDAudience):
    """Insère les données de navigation propres aux autres pages qui ne sont pas
    déjà gérées par d'autres classes (A propos, contact, etc.).

    Parameters
    ----------
    region : str
        Région sélectionnée
    page : str
        Page consultée
    details : str (default='')
        Détails sur la page consultée
    """

    def __init__(self, region, page, details=""):
        super().__init__(region)
        self.page = page
        self.details = details

    async def inserer_donnees_consultations(self, id_utilisateur):
        """
        Insère les données de navigation propres aux analyses territoriales

        Parameters
        ----------
        id_utilisateur : int
            Le token de l'utilisateur qui visite
        """
        requete = """
                  INSERT INTO consultations.autres_pages (id_utilisateur, region, page, date, details)
                  VALUES ($1, $2, $3, $4, $5)
                  """
        await controller.fetch(
            requete,
            int(id_utilisateur),
            self.region,
            self.page,
            self.date,
            self.details,
        )
        return {"id": id_utilisateur}
