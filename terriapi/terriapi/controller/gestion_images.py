﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import base64
import os

# import shutil


def ecriture_image_base64(image, output_path):
    """Fonction qui écrit des images en base 64 sur le serveur.

    Parameters
    ----------
    image : image en base64
    output_path : le chemin de sortie
    """
    img = image.encode()
    with open(r"" + output_path + "", "wb") as fh:
        fh.write(base64.decodebytes(img))


def list_path_file(path_folder, extension_file):
    """Fonction qui renvoie une liste de path file.

    Parameters
    ----------
    path_folder : le chemin du dossier
    extension_image : l'extension du type d'image qu'on recherche

    Returns
    -------
    list
    """
    list_path = []
    for images in os.listdir(path_folder):
        if images.endswith("." + extension_file):
            list_path.append(images)

    return list_path


class SuppressionElement:
    """Classe pour supprimer un ou plusieurs éléments d'un dossier.

    Parameters
    ----------
    path_folder : le chemin du dossier
    """

    def __init__(self, path_folder):
        self.path_folder = path_folder


# Classe créée dans le cas où il serait utile de supprimer un ensemble d'éléments dans un dossier
# class SuppressionEnsembleFichiersDossier(SuppressionElement):
#     """Classe pour supprimer plusieurs éléments d'un dossier.

#     Paramètres
#     ----------
#     path_folder : le chemin du dossier
#     """
#     def supprimer_ensemble_fichiers_dossier(self):
#         "Fonction qui supprime les éléments d'un dossier."
#         shutil.rmtree(self.path_folder)


class SuppressionUniqueFichierDossier(SuppressionElement):
    """Classe pour supprimer un élément d'un dossier.

    Parameters
    ----------
    path_folder : le chemin du dossier
    """

    def supprimer_unique_fichier_dossier(self, path_file):
        """Fonction qui supprime un élément précis d'un dossier"""
        os.remove(self.path_folder + path_file)
