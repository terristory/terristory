﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Gestion des couches de Point d'Intérêt aka POI

- Installation méthanisation
- Installation réseaux de chaleur
- bornes d'infra de recherche de véhicules électriques (IRVE)
"""
from __future__ import annotations

import json
from datetime import datetime

from sanic.log import logger

from terriapi import controller
from terriapi.controller import execute, fetch
from terriapi.exceptions import ForceUpdateAllowedException, ValidationError
from terriapi.integration import enregistrerPdfMethodo
from terriapi.integration.validation import (
    existence_table,
    lecture_headers,
    normalize,
    verifie_headers,
)


async def update_poi_contribution(region, user, layer, record_id, properties, geom):
    """Mise à jour d'un objet dans une couche POI

    Parameters
    ----------
    region : str
        region key
    user : terriapi.controller.user.User
        Utilisateur
    layer : str
        Nom de la couche POI (i.e. nom de la table)
    record_id : int
        Identifiant de l'élément à modifier
    properties : dict
        Données attributaires (JSON)
    geom : tuple
        Coordonnées x (lon), y (lat)
    """
    schema = region.replace("-", "_")
    x, y = geom
    update_query = f"""INSERT INTO meta.poi_proposed_changes 
        (region, poi_table, user_mail, poi_id, new_properties, old_properties, action, state, new_geometry, old_geometry)
        VALUES($1, $2, $3, $4, $5, $6, 'edit', 'pending', ST_SetSRID(ST_MakePoint($7, $8), 3857), $9) returning edit_id"""
    # pour récupérer la valeur courante
    select_query = f"""select properties, geom, ST_X(geom) as x, ST_Y(geom) as y
    from {schema}_poi.{layer}
    where id = $1
    """
    properties_json = json.dumps(properties, ensure_ascii=False)
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            existing_data_rset = await conn.fetch(select_query, record_id)
            existing_data = existing_data_rset[0]
            courant = existing_data["properties"]
            geom_courant = existing_data["geom"]

            if x is None and y is None:
                x, y = existing_data["x"], existing_data["y"]

            rset = await conn.fetch(
                update_query,
                region,
                layer,
                user.mail,
                record_id,
                properties_json,
                courant,
                float(x),
                float(y),
                geom_courant,
            )
    if rset:
        return rset[0]["edit_id"]
    return None


async def create_poi_contribution(region, user, layer, properties, geom):
    """Création d'une POI dans une couche donnée

    Parameters
    ----------
    region : str
        region key
    user : terriapi.controller.user.User
        Utilisateur
    layer : str
        Nom de la couche POI (i.e. nom de la table)
    properties : dict
        Données attributaires (JSON)
    geom : tuple
        Coordonnées x (lon), y (lat)
    """
    x, y = geom
    addition_query = f"""INSERT INTO meta.poi_proposed_changes 
        (region, poi_table, user_mail, poi_id, new_properties, action, state, new_geometry)
        VALUES($1, $2, $3, $4, $5, 'add', 'pending', ST_SetSRID(ST_MakePoint($6, $7), 3857)) returning edit_id"""
    properties_json = json.dumps(properties, ensure_ascii=False)
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            rset = await conn.fetch(
                addition_query,
                region,
                layer,
                user.mail,
                0,
                properties_json,
                float(x),
                float(y),
            )
            if rset:
                return rset[0]["edit_id"]
            return None


async def delete_poi_object_contribution(region, user, layer, record_id):
    """Suppression d'une POI dans une couche donnée

    Parameters
    ----------
    region : str
        region key
    user : terriapi.controller.user.User
        Utilisateur
    layer : str
        Nom de la couche POI (i.e. nom de la table)
    id : integer
        ID de l'objet à supprimer
    """
    schema = region.replace("-", "_")
    delete_query = f"""INSERT INTO meta.poi_proposed_changes 
        (region, poi_table, user_mail, poi_id, old_properties, action, state, old_geometry)
        VALUES($1, $2, $3, $4, $5, 'delete', 'pending', $6) returning edit_id"""
    # pour récupérer la valeur courante
    select_query = (
        f"""SELECT properties, geom FROM {schema}_poi.{layer} WHERE id = $1"""
    )
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            current_data = await conn.fetch(select_query, record_id)
            current_props = current_data[0]["properties"]
            current_geom = current_data[0]["geom"]
            rset = await conn.fetch(
                delete_query,
                region,
                layer,
                user.mail,
                record_id,
                current_props,
                current_geom,
            )

            if rset:
                return rset[0]["edit_id"]
            return None


async def update_poi(region, user, layer, record_id, properties, geom):
    """UPDATE a POI object in a layer

    Parameters
    ----------
    region : str
        region key
    user : terriapi.controller.user.User
        Utilisateur
    layer : str
        Nom de la couche POI (i.e. nom de la table)
    record_id : int
        Identifiant de l'élément à modifier
    properties : dict
        Données attributaires (JSON)
    geom : tuple
        Coordonnées x (lon), y (lat)
    """
    schema = region.replace("-", "_")
    x, y = geom
    update_query = f"""update {schema}_poi.{layer} set
      properties = $2,
      geom = ST_SetSRID(ST_MakePoint($3, $4), 3857)
    where id = $1"""
    # pour récupérer la valeur courante
    select_query = f"""select properties
      ,geom
    from {schema}_poi.{layer}
    where id = $1
    """
    # mettre à jour la table d'historique des modifs
    historique_query = f"""insert into {schema}_poi.historique
    (id, layer, mail, action, properties_precedent, properties_courant, geom_precedent, geom_courant, mise_a_jour, region)
    values ($1, $2, $3, $4, $5, $6, $7, ST_SetSRID(ST_MakePoint($8, $9), 3857), NOW(), $10)
    """
    properties_json = json.dumps(properties, ensure_ascii=False)
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            courant_rset = await conn.fetch(select_query, record_id)
            courant = courant_rset[0]["properties"]
            geom_courant = courant_rset[0]["geom"]
            rset = await conn.execute(
                update_query, record_id, properties_json, float(x), float(y)
            )
            await conn.execute(
                historique_query,
                record_id,
                layer,
                user.mail,
                "modification",
                courant,
                properties_json,
                geom_courant,
                float(x),
                float(y),
                user.region,
            )
    return rset != "UPDATE 0"


async def create_poi(region, user, layer, properties, geom):
    """Création d'une POI dans une couche donnée

    Parameters
    ----------
    region : str
        region key
    user : terriapi.controller.user.User
        Utilisateur
    layer : str
        Nom de la couche POI (i.e. nom de la table)
    properties : dict
        Données attributaires (JSON)
    geom : tuple
        Coordonnées x (lon), y (lat)
    """
    schema = region.replace("-", "_")
    x, y = geom
    query = f"""INSERT INTO {schema}_poi.{layer} (properties, geom) values
    ($1, ST_SetSRID(ST_MakePoint($2, $3), 3857)) RETURNING id"""
    properties_json = json.dumps(properties, ensure_ascii=False)
    # mettre à jour la table d'historique des modifs
    historique_query = f"""insert into {schema}_poi.historique
    (id, layer, mail, action, properties_precedent, properties_courant, geom_precedent, geom_courant, mise_a_jour, region)
    values ($1, $2, $3, $4, $5, $6, $7, ST_SetSRID(ST_MakePoint($8, $9), 3857), NOW(), $10)
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            rset = await conn.fetch(query, properties_json, float(x), float(y))
            await conn.execute(
                historique_query,
                rset[0]["id"],
                layer,
                user.mail,
                "creation",
                None,
                properties_json,
                None,
                float(x),
                float(y),
                user.region,
            )
    if rset:
        return rset[0]["id"]
    return None


async def delete_poi_object(region, user, layer, id):
    """Suppression d'une POI dans une couche donnée

    Parameters
    ----------
    region : str
        region key
    user : terriapi.controller.user.User
        Utilisateur
    layer : str
        Nom de la couche POI (i.e. nom de la table)
    id : integer
        ID de l'objet à supprimer
    """
    schema = region.replace("-", "_")
    query = f"""delete from {schema}_poi.{layer} where id=$1"""
    # mettre à jour la table d'historique des modifs
    historique_query = f"""insert into {schema}_poi.historique
    (id, layer, mail, action, mise_a_jour, region)
    values ($1, $2, $3, $4, NOW(), $5)
    """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            rset = await conn.fetch(query, id)
            await conn.execute(
                historique_query,
                id,
                layer,
                user.mail,
                "suppression",
                user.region,
            )
    return rset != "DELETE 0"


async def update_poi_edition_rights(region, layer, droit, user):
    """Mise à jour des droits (modifiable) pour une couche POI

    Parameters
    ----------
    region : str
        region key
    layer : str
        Nom de la couche POI (i.e. nom de la table)
    droit : boolean
        Modifiable ou non
    """
    schema = region.replace("-", "_")
    query = f"""update {schema}_poi.layer
            set modifiable = $2
            where nom = $1"""

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            rset = await conn.execute(query, layer, droit)
            historique_query = f"""insert into {schema}_poi.historique
            (id, layer, mail, action, properties_precedent, properties_courant,
                geom_precedent, geom_courant, mise_a_jour, region)
            values (0, $1, $2, $3, $4, $5,
                NULL, NULL, NOW(), $6)
            """
            if droit:
                etat = json.dumps({"etat": "Modification activée"}, ensure_ascii=False)
            else:
                etat = json.dumps(
                    {"etat": "Modification désactivée"}, ensure_ascii=False
                )
            await conn.execute(
                historique_query,
                layer,
                user.mail,
                "Changement du droit de modification",
                json.dumps({}),
                etat,
                user.region,
            )
    return rset != "UPDATE 0"


def _diff_dict(left, right):
    """Différence entre deux dictionnaires (à plat)"""
    result = []
    for key in left:
        old, new = left.get(key, ""), right.get(key, "")
        if old != new:
            if isinstance(old, list):
                diff_list = _diff_list(old, new)
                if diff_list:
                    result.append(f"{key} : {diff_list}")
            elif old is None or old == "":
                result.append(f"{key} : nouvelle valeur ({new})\n")
            else:
                result.append(f"{key} : précédente ({old}) / courante ({new})\n")
    return result


def linear_dict(dic):
    """Flattens a dict to a list of strings."""
    return [f"{key} : {val}" for key, val in dic.items()]


def _diff_list(left, right):
    """Différence entre deux listes"""
    longueur_left, longueur_right = len(left), len(right)
    if longueur_left != longueur_right:
        if longueur_right - longueur_left > 0:
            return "ajout de {} élément(s)".format(longueur_right - longueur_left)
        else:
            return "suppression de {} élément(s)".format(longueur_left - longueur_right)
    if left != right:
        return "contenu différent"


async def historique_poi(region):
    """Récupère et traite l'historique des modifications des couches POI

    Parameters
    ----------
    region : str
        region key
    """
    schema = region.replace("-", "_")
    query = f"""select id
      ,layer
      ,action
      ,mail
      ,properties_precedent
      ,properties_courant
      ,case when geom_precedent is not null then
         st_distance(geom_precedent, geom_courant)
       else null end as distance
      ,st_x(st_transform(geom_courant, 4326)) || ' ' || st_y(st_transform(geom_courant, 4326)) as geom_courant
      ,st_x(st_transform(geom_precedent, 4326)) || ' ' || st_y(st_transform(geom_precedent, 4326)) as geom_precedent
      ,to_char(mise_a_jour, 'YYYY-MM-DD HH24:MI:SS') as mise_a_jour
    from {schema}_poi.historique
    order by mise_a_jour DESC
    """
    rset = await fetch(query)
    data = [dict(x) for x in rset]
    # ressort les différences entre precedent et suivant
    historique = []
    for record in data:
        result = {
            "id": record["id"],
            "layer": record["layer"],
            "action": record["action"],
            "utilisateur": record["mail"],
            "changement": None,
            "geolocalisation": None,
            "position_courante": record["geom_courant"],
            "position_precedente": record["geom_precedent"],
            "mise_a_jour": record["mise_a_jour"],
        }
        if record["action"] == "modification":
            precedent = json.loads(record["properties_precedent"])
            courant = json.loads(record["properties_courant"])
            difference = _diff_dict(precedent, courant)
            if difference:
                result["changement"] = difference
        if "droit de modification" in record["action"]:
            result["changement"] = json.loads(record["properties_courant"]).get(
                "etat", ""
            )
        if record["distance"] is not None:
            result["geolocalisation"] = "distance (m) : {:.2f}".format(
                record["distance"]
            )
        historique.append(result)
    return historique


async def get_poi_contributions(region, layers: str | list = "*", edit_id=None):
    """Retrieve contributions list for current region

    Parameters
    ----------
    region : str
        region key
    """
    query = f"""SELECT 
            edit_id, region, poi_table, user_mail, poi_id, new_properties, old_properties,
            action, state, to_char(submitted_date, 'YYYY-MM-DD HH24:MI:SS') as submitted_date, 
            ARRAY[ST_X(new_geometry), ST_Y(new_geometry)] as geometry,
            CASE WHEN old_geometry IS NOT NULL
                THEN st_distance(old_geometry, new_geometry)
                ELSE NULL 
            END AS distance,
            st_x(st_transform(new_geometry, 4326)) || ' ' || st_y(st_transform(new_geometry, 4326)) AS geom_courant,
            st_x(st_transform(old_geometry, 4326)) || ' ' || st_y(st_transform(old_geometry, 4326)) AS geom_precedent
        FROM meta.poi_proposed_changes
        WHERE region = $1 
        {"AND $2 = '*'" if isinstance(layers, str) else "AND poi_table = ANY($2)"}
        {"" if edit_id is None else "AND edit_id = $3"}
        ORDER BY submitted_date DESC
    """
    if edit_id:
        rset = await fetch(query, region, layers, edit_id)
    else:
        rset = await fetch(query, region, layers)
    data = [dict(x) for x in rset]
    # ressort les différences entre precedent et suivant
    historique = []
    for record in data:
        result = {
            "edit_id": record["edit_id"],
            "poi_id": record["poi_id"],
            "poi_table": record["poi_table"],
            "action": record["action"],
            "state": record["state"],
            "utilisateur": record["user_mail"],
            "changement": None,
            "geolocalisation": None,
            "position_courante": record["geom_courant"],  # for display
            "position_precedente": record["geom_precedent"],  # for display
            "new_properties": record["new_properties"],  # for display & save
            "old_properties": record["old_properties"],  # for display
            "new_geometry": record["geometry"],  # for save
            "mise_a_jour": record["submitted_date"],
        }
        old_properties = json.loads(record["old_properties"] or "{}")
        new_properties = json.loads(record["new_properties"] or "{}")
        if record["action"] == "edit":
            difference = _diff_dict(old_properties, new_properties)
            if difference:
                result["changement"] = difference
        if record["action"] == "add":
            result["changement"] = linear_dict(new_properties)
        if record["action"] == "delete":
            result["changement"] = linear_dict(old_properties)
        if record["distance"] is not None:
            result["geolocalisation"] = "distance (m) : {:.2f}".format(
                record["distance"]
            )
        historique.append(result)
    return historique


async def get_poi_contribution(region, edit_id):
    """Retrieve a specific contribution for current region

    Parameters
    ----------
    region : str
        region key
    edit_id : int
        contribution ID
    """
    contributions = await get_poi_contributions(region, edit_id=edit_id)
    if len(contributions) == 0:
        return None
    return contributions[0]


async def update_contribution(region, edit_id, action):
    """Update a contribution status inside meta table

    Parameters
    ----------
    region : str
        region key
    """
    update_query = f"""UPDATE meta.poi_proposed_changes SET state = $1 WHERE edit_id = $2 AND region = $3"""
    return await controller.execute(
        update_query,
        action,
        edit_id,
        region,
    )


async def obtenir_installation_et_geom(
    schema, liste_tables, main_schema="", zone=None, zone_id=None
):
    """Permet d'obtenir la liste des propriétés et des geométries des équipement

    Parameters
    ----------

    schema : chaîne de caractères
        schéma régional consacré aux équipements
    liste_tables : liste
        liste de couches qui contiennent des données équipements
    main_schema : chaîne de caractères
        shéma régional
    zone : chaîne de caractères
        type de zone dans laquelle récupérer les équipements
    zone_id : chaîne de caractères
        id de la zone. Si aucune zone n'est spécifiée, tous les équipements de la région sont retournés

    Returns
    --------

    Dictionnaire contenant les géométries et les propriétés des couches
    """
    dict_par_couche_equipement = {}
    for table in liste_tables:
        if zone is None or zone_id is None:
            res = await fetch(
                f"""
                with tmp as
                (
                    select st_astext(geom) as points, id, properties
                    from {schema}.{table["nom"]}
                )
                select points, id, properties from tmp;
            """
            )
        else:
            res = await fetch(
                f"""
                with tmp as
                (
                    select st_astext(d.geom) as points, d.id, d.properties
                    from {schema}.{table["nom"]} as d
                    left join {main_schema}.{zone} as t
                    on st_intersects(t.geom, d.geom)
                    where t.code = $1
                )
                select points, id, properties from tmp;
            """,
                zone_id,
            )

        dict_par_entite = []
        for a in [dict(i) for i in res]:
            if a["points"]:
                elem = {
                    "geom": a["points"],
                    "proprietes": a["properties"],
                    "id": a["id"],
                }
                if elem not in dict_par_entite:
                    dict_par_entite.append(elem)
        dict_par_couche_equipement[table["nom"]] = dict_par_entite
    return dict_par_couche_equipement


async def obtenir_tables_equipements_autres_que_points(schema):
    """Obtention des nom des tables qui contiennent des données d'équipements dont la géométrie n'est pas le point

    Parameters
    ----------
    schema : Chaîne de caractères
        Nom du schéma régional

    Returns
    --------
    Liste des tables qui contiennent les données des équipements représentés par d'autres géométries que le point.
    """
    req = """
          select nom, type_geom
          from {schema}.layer
          where type_geom != 'Point'
          """.format(
        schema=schema
    )
    res = await fetch(req)
    return [dict(x) for x in res]


async def delete_poi_layer(schema, layer, table_name):
    """Delete a POI layer.

    Parameters
    ----------
    region : str
        region key
    layer : int
        poi layer key inside regional poi layer table
    table_name : str
        poi layer table name
    """

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            query = f"""DELETE FROM {schema}.layer WHERE id = $1"""
            await conn.execute(query, layer)
            check_exists = """SELECT EXISTS (
                SELECT FROM information_schema.tables 
                WHERE  table_schema = $1
                AND    table_name   = $2
            );"""
            exists = await conn.fetchval(check_exists, schema, table_name)
            if exists:
                query = f"""DROP TABLE {schema}.{table_name}"""
                await conn.execute(query)
                return True
            else:
                return "Table manquante"


async def update_order_within_themes(data, region):
    """Update layers' order within their theme.

    Parameters
    ----------
    data : json
        {themes : {id_analyse: ordre} ... }
    """
    schema = region.replace("-", "_")
    for theme in data:
        for analyse_id in data[theme]:
            ordre = data[theme][analyse_id]
            sql = f"""
                update {schema}_poi.layer
                set order_in_theme = $1,
                    theme = $2
                where id = $3
                """
            await execute(sql, int(ordre), theme, int(analyse_id))


async def rename_theme(old_name, new_name, region):
    """Rename indicators' theme.

    Parameters
    ----------
    old_name : str
        Old theme name
    new_name : str
        New theme name
    region : str
        Region name
    """
    schema = region.replace("-", "_")
    sql = f"""
        update {schema}_poi.layer
        set theme = $1
        where theme = $2
    """
    await execute(sql, new_name, old_name)


async def update_theme_order(data, region: str):
    """Update themes order inside a region.

    Parameters
    ----------
    data : json
        {themes : {id_analyse: ordre} ... }
    """
    schema = region.replace("-", "_")
    ordre = 1
    for theme in data:
        sql = f"""
                update {schema}_poi.layer
                set theme_order = $1
                where theme = $2
                """
        await execute(sql, ordre, theme)
        ordre += 1


async def get_themes(region: str):
    """Get POI themes for a region.

    Parameters
    ----------
    region : str
        Region name
    """
    schema = region.replace("-", "_")
    sql = f"""
        select theme as label from {schema}_poi.layer
        GROUP BY theme, theme_order
        ORDER BY theme_order
    """
    return await fetch(sql)


async def get_layers_constraints(region: str, layers: str | list):
    """Get POI themes for a region.

    Parameters
    ----------
    region : str
        Region name
    """
    schema = region.replace("-", "_")
    if isinstance(layers, str):
        layers = [layers]
    return await controller.fetch(
        f"""
        SELECT layer_name, json_object_agg(
            field_name, json_build_object(
                'field_type', field_type,
                'details', details
            )
        ) as structure_constraints
        FROM {schema}_poi.layers_structure
        WHERE layer_name = ANY($1)
        GROUP BY layer_name
        """,
        layers,
    )


async def validate_contribution(constraints: list, properties: dict):
    """Get POI themes for a region.

    Parameters
    ----------
    region : str
        Region name
    """
    if len(constraints) == 0:
        return True
    structure_constraints = json.loads(constraints[0]["structure_constraints"])

    for key, val in properties.items():
        if key not in structure_constraints:
            continue
        constraint = structure_constraints[key]
        if constraint["field_type"] == "text":
            continue
        elif constraint["field_type"] == "integer":
            try:
                properties[key] = int(val)
            except ValueError:
                raise ValueError(key)
        elif constraint["field_type"] == "float":
            try:
                properties[key] = float(val)
            except ValueError:
                raise ValueError(key)
        elif constraint["field_type"] == "select":
            if val == "":
                continue
            if val not in constraint["details"]:
                raise ValueError(key)
        elif constraint["field_type"] == "date":
            if val == "":
                continue
            # we test if it is a valid date
            try:
                datetime.strptime(val, "%Y-%m-%d")
            except ValueError:
                raise ValueError(key)
    return True


async def import_layer(
    region: str, layer_id: int, layer: str, user, force_update: bool = False
):
    """
    Import a POI layer from national scheme
    """
    schema = region.replace("-", "_")
    table_exists = await controller.fetch(
        f"""SELECT EXISTS (
            SELECT FROM information_schema.tables 
            WHERE  table_schema = $1
            AND    table_name   = $2
        ) UNION SELECT EXISTS (
            SELECT FROM {schema}_poi.layer
            WHERE nom = $2
        )""",
        f"{schema}_poi",
        layer,
    )
    if any(t["exists"] for t in table_exists) and not force_update:
        raise ForceUpdateAllowedException()

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            # Clean existing layers
            await conn.execute(f"DROP TABLE IF EXISTS {schema}_poi.{layer}")
            await conn.execute(f"DELETE FROM {schema}_poi.layer WHERE nom = $1", layer)
            # Insert new layers
            await conn.execute(
                f"""CREATE TABLE {schema}_poi.{layer} (LIKE france_poi.{layer} INCLUDING ALL)"""
            )
            await conn.execute(
                f"""INSERT INTO {schema}_poi.layer (
                    nom, label, couleur, modifiable, theme, type_geom,
                    statut, ancrage_icone, donnees_exportables, credits_data_sources, credits_data_producers,
                    type_installation, theme_order, order_in_theme)
                (
                    SELECT nom, label, couleur, modifiable, theme, type_geom,
                    statut, ancrage_icone, donnees_exportables, credits_data_sources, credits_data_producers,
                    type_installation, theme_order, order_in_theme
                    FROM france_poi.layer WHERE id = $1
                );""",
                layer_id,
            )
            await conn.execute(
                f"""insert into {schema}_poi.historique
                    (id, layer, mail, action, mise_a_jour, region)
                    values (0, $1, $2, $3, NOW(), $4)""",
                layer,
                user.mail,
                "import",
                user.region,
            )
    return None


async def create_layer_table(schema, table, columns, mode, type_geom):
    """On crée la structure d'une table pour une nouvelle couhe équipements.

    Parameters
    ----------
    schema : str
    table : str
    """
    if mode != "maj":
        is_valid = await existence_table(schema, table)
        if not is_valid:
            raise ValidationError(is_valid.message)

    sql_columns = []
    for column in columns:
        if column != "x" and column != "y":
            sql_columns.append("{} varchar".format(column))

    return f"""create table {schema}.{table} (
        id serial,
        x double precision,
        y double precision,
        properties json,
        geom geometry({type_geom}, 3857),
        {",".join(sql_columns)}
    )"""


async def create_layer(region, schema, table, metadata, userdata, fpath):
    """Création d'une nouvelle couche équipement

    Parameters
    ----------
    schema : str
        Nom du schéma
    table : str
        Nom de la table de l'équipement
    metadata : dict
        Méta-données pour cette couche équipements
    fpath : Path
        Chemin du fichier CSV
    """
    colonnes_obligatoires = ["x", "y"]
    if metadata["type_geom"] != "Point":
        colonnes_obligatoires = ["geom_origine"]
    is_valid = verifie_headers(fpath, colonnes_obligatoires)
    if not is_valid:
        raise ValidationError(is_valid.message)
    headers = lecture_headers(fpath, original=True)

    # normalisation des headers
    headers_normalized = [normalize(x) for x in headers]

    query_create = await create_layer_table(
        schema, table, headers_normalized, "ajout", metadata["type_geom"]
    )
    # insertion dans <region>_poi.layer
    query_insertion = f"""
    insert into {schema}.layer (nom, label, couleur, modifiable, theme, type_geom,
    statut, ancrage_icone, donnees_exportables, credits_data_sources, credits_data_producers,
    type_installation, theme_order, order_in_theme)
    values (
        $1, $2, $3, $4, $5, $6, 
        $7, $8, $9, $10, $11, 
        $12, $13, $14
    )
    returning id
    """

    # Enregistrement du fichier methodo
    if metadata["fichierPdfMethodo"]:
        await enregistrerPdfMethodo(
            region,
            metadata["fichierPdfMethodo"],
            final_name=metadata["nom_table"],
        )

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            logger.info(f"insertion dans '{schema}.layer'")
            # compute theme order (depends if it is a new theme or an existing theme)
            rset = await conn.fetch(
                f"SELECT theme_order FROM {schema}.layer WHERE theme = $1 ",
                metadata["theme"],
            )
            if len(rset) > 0:
                theme_order = rset[0]["theme_order"]
            else:
                rset = await conn.fetch(
                    f"SELECT COUNT(DISTINCT theme) as max_order FROM {schema}.layer",
                )
                theme_order = int(rset[0]["max_order"]) + 1

            # compute order in theme for new layer
            rset = await conn.fetch(
                f"SELECT COALESCE(MAX(order_in_theme), 0) as max_order FROM {schema}.layer WHERE theme = $1",
                metadata["theme"],
            )
            if len(rset) > 0:
                order_in_theme = int(rset[0]["max_order"]) + 1
            else:
                order_in_theme = 1

            rset = await conn.fetch(
                query_insertion,
                metadata["nom_table"],
                metadata["nom"],
                metadata["couleur"],
                False,
                metadata["theme"],
                metadata["type_geom"],
                metadata["statut"],
                metadata["ancrage_icone"],
                metadata["donnees_exportables"],
                metadata["credits_data_sources"],
                metadata["credits_data_producers"],
                metadata.get("typologies_modalities", ""),
                theme_order,
                order_in_theme,
            )
            indicateur_id = rset[0]["id"]
            logger.info("nouvelle couche équipement avec id=%d", indicateur_id)

            # créer la table
            logger.info("création de la table '%s'", table)
            await conn.fetch(query_create)
            # insérer les données
            logger.info("insertion des données '%s'", fpath)
            await conn.copy_to_table(
                table,
                source=fpath,
                schema_name=schema,
                header=True,
                format="csv",
                delimiter=";",
                escape="\\",
                columns=headers_normalized,
            )

            # Remplissage de la colonne properties
            logger.info("Génération du JSON")
            json_sql = []
            for idx, val in enumerate(headers):
                if (
                    val.strip() != "x"
                    and val.strip() != "y"
                    and val.strip() != "geom_origine"
                ):
                    json_sql.append("'{}'".format(val))
                    json_sql.append(headers_normalized[idx])
            await conn.fetch(
                "update {schema}.{table} set properties = json_build_object({json})".format(
                    schema=schema, table=table, json=",".join(json_sql)
                )
            )

            # Génération de la géométrie à partir de x et y
            logger.info("Génération de la géométrie")
            req_geom = "update {schema}.{table} set geom = st_transform(st_setsrid(st_makepoint(x, y), 4326), 3857)".format(
                schema=schema, table=table
            )
            if metadata["type_geom"] != "Point":
                req_geom = "update {schema}.{table} set geom = st_transform(ST_GeomFromText(geom_origine, 2154), 3857)".format(
                    schema=schema, table=table, type_geom=metadata["type_geom"]
                )

            await conn.fetch(req_geom)

            # Supression des colonnes inutiles
            logger.info("Supression des colonnes inutiles")
            for idx, val in enumerate(headers_normalized):
                await conn.fetch(
                    "alter table {schema}.{table} drop column {column}".format(
                        schema=schema, table=table, column=val
                    )
                )

            # Index sur les géométries
            logger.info("Création d'index géométrique")
            await conn.fetch(
                "create index on {schema}.{table} using gist(geom);".format(
                    schema=schema, table=table
                )
            )

            historique_query = f"""insert into {schema}.historique
                (id, layer, mail, action, properties_precedent, properties_courant,
                    geom_precedent, geom_courant, mise_a_jour, region)
                values (0, $1, $2, $3, NULL, NULL,
                    NULL, NULL, now(), $4)
                """
            await conn.execute(
                historique_query,
                metadata["nom_table"],
                userdata.mail,
                "Création de la couche",
                userdata.region,
            )


async def update_layer(region, schema, table, metadata, userdata, fpath=None):
    """Mise à jour d'une couche équipement

    Parameters
    ----------
    schema : str
        Nom du schéma
    table : str
        Nom de la table de l'équipement
    metadata : dict
        Méta-données pour cette couche équipements
    fpath : Path
        Chemin du fichier CSV
    """
    colonnes_obligatoires = ("x", "y")
    if metadata["type_geom"] != "Point":
        colonnes_obligatoires = ["geom_origine"]

    # Enregistrement du fichier methodo
    if metadata["fichierPdfMethodo"]:
        await enregistrerPdfMethodo(
            region,
            metadata["fichierPdfMethodo"],
            final_name=metadata["nom_table"],
        )

    # if we have to update data
    if fpath is not None:
        is_valid = verifie_headers(fpath, colonnes_obligatoires)
        if not is_valid:
            raise ValidationError(is_valid.message)
        headers = lecture_headers(fpath, original=True)
        # normalisation des headers
        headers_normalized = [normalize(x) for x in headers]

        query_drop = """
            drop table {schema}.{table}
        """.format(
            schema=schema, table=table
        )

        query_create = await create_layer_table(
            schema, table, headers_normalized, "maj", metadata["type_geom"]
        )

        async with controller.db.acquire() as conn:
            async with conn.transaction():
                # supprimer la table existante
                logger.info("Suppression de la table existante '%s'", table)
                await conn.fetch(query_drop)
                # créer la table
                logger.info("Création de la table '%s'", table)
                await conn.fetch(query_create)
                # insérer les données
                logger.info("insertion des données '%s'", fpath)
                await conn.copy_to_table(
                    table,
                    source=fpath,
                    schema_name=schema,
                    header=True,
                    format="csv",
                    delimiter=";",
                    escape="\\",
                    columns=headers_normalized,
                )

                # Remplissage de la colonne properties
                logger.info("Génération du JSON")
                json_sql = []
                for idx, val in enumerate(headers):
                    if val != "x" and val != "y" and val != "geom_origine":
                        json_sql.append("'{}'".format(val))
                        json_sql.append(headers_normalized[idx])
                await conn.fetch(
                    "update {schema}.{table} set properties = json_build_object({json})".format(
                        schema=schema, table=table, json=",".join(json_sql)
                    )
                )

                # Génération de la géométrie à partir de x et y
                logger.info("Génération de la géométrie")
                req_geom = "update {schema}.{table} set geom = st_transform(st_setsrid(st_makepoint(x, y), 4326), 3857)".format(
                    schema=schema, table=table
                )

                if metadata["type_geom"] != "Point":
                    req_geom = "update {schema}.{table} set geom = st_transform(ST_GeomFromText(geom_origine, 2154), 3857)".format(
                        schema=schema, table=table, type_geom=metadata["type_geom"]
                    )
                await conn.fetch(req_geom)

                # Supression des colonnes inutiles
                logger.info("Supression des colonnes inutiles")
                for idx, val in enumerate(headers_normalized):
                    await conn.fetch(
                        "alter table {schema}.{table} drop column {column}".format(
                            schema=schema, table=table, column=val
                        )
                    )

                # Index sur les géométries
                logger.info("Création d'index géométrique")
                await conn.fetch(
                    "create index on {schema}.{table} using gist(geom);".format(
                        schema=schema, table=table
                    )
                )

    # Dans tous les cas, on met à jour les métadonnées
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            # Mise à jour des metadonnées
            logger.info("Mise à jour des metadonnées de la couche équipement")
            await conn.fetch(
                """update {schema}.layer set
                couleur = $1, theme = $2, statut = $3, ancrage_icone = $4, donnees_exportables = $5,
                    label = $6, credits_data_sources = $7, credits_data_producers = $8,
                    type_installation = $10
                where nom = $9""".format(
                    schema=schema
                ),
                metadata["couleur"],
                metadata["theme"],
                metadata["statut"],
                metadata["ancrage_icone"],
                metadata["donnees_exportables"],
                metadata["label"],
                metadata["credits_data_sources"],
                metadata["credits_data_producers"],
                table,
                metadata.get("typologies_modalities", ""),
            )

            historique_query = f"""insert into {schema}.historique
                (id, layer, mail, action, properties_precedent, properties_courant,
                    geom_precedent, geom_courant, mise_a_jour, region)
                values (0, $1, $2, $3, NULL, NULL,
                    NULL, NULL, now(), $4)
                """
            await conn.execute(
                historique_query,
                table,
                userdata.mail,
                (
                    "Mise à jour de la couche"
                    if fpath is None
                    else "Mise à jour de la couche et de ses données"
                ),
                userdata.region,
            )
