# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi import controller

STAT_TYPES = [
    "territoires",
    "indicateurs",
    "poi",
    "tableaux_de_bords",
    "autres_pages",
    "utilisateurs",
    "analyses",
    "strategies_territoriales",
]


async def perform_query_stats(query, *params):
    """
    Perform a simple query and return results as list of dict

    Parameters
    ----------
    query : str
        the query to perform
    *params : mixed
        all parameters sent to fetch function

    Returns
    -------
    list of dict
        results
    """
    rset = await controller.fetch(query, *params)
    return [dict(x) for x in rset]


async def get_territorial_stats(region, from_date, to_date):
    """
    Get stats related to territorial visits in specified time interval. Will
    consider filters applied, territorial analyses page, analyses on main map,
    dashboards...

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    schema = region.replace("-", "_")
    query = f"""select sum(a.count) as nbre_selection, t.code, a.type_territoire, t.nom 
        from (
            select count(code_territoire), code_territoire, type_territoire 
            from consultations.actions_cochees 
            where REPLACE(region, '_', '-') = $1 
            and date >= $2 and date < $3
            group by code_territoire, type_territoire 
            
            union all 
            
            select count(code_territoire), code_territoire, type_territoire 
            from consultations.analyses_territoriales 
            where REPLACE(region, '_', '-') = $1 
            and date >= $2 and date < $3
            group by code_territoire, type_territoire 
            
            union all 
            
            select count(code_territoire), code_territoire, type_territoire 
            from consultations.tableaux_bords
            where REPLACE(region, '_', '-') = $1 
            and date >= $2 and date < $3
            group by code_territoire, type_territoire 
            
            union all 
            
            select count(code_territoire), code_territoire, type_territoire 
            from consultations.consultations_indicateurs 
            where provenance not like 'Focalisation territoriale%' 
            and date >= $2 and date < $3
            and provenance not like 'Filtre%' 
            and REPLACE(region, '_', '-') = $1 
            group by code_territoire, type_territoire
        ) a 
        join (
            select distinct code, nom 
            from {schema}.territoire
        ) t 
        on t.code = a.code_territoire group by t.nom, t.code, a.type_territoire order by t.nom"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_strategies_stats(region, from_date, to_date):
    """
    Get stats related to territorial strategies in specified time interval.

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    schema = region.replace("-", "_")
    query = f"""select a.type_action, sum(a.count) as nbre_selection, t.code, a.type_territoire, t.nom 
        from (
            select type_action, count(code_territoire), code_territoire, type_territoire 
            from consultations.actions_cochees 
            where region = $1 
            and date >= $2 and date < $3
            group by code_territoire, type_territoire, type_action
        ) a 
        join (
            select distinct code, nom 
            from {schema}.territoire
        ) t 
        on t.code = a.code_territoire 
        group by a.type_action, t.nom, t.code, a.type_territoire
        order by t.nom"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_analyses_page_stats(region, from_date, to_date):
    """
    Get stats related to territorial analyses page in specified time interval.

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    schema = region.replace("-", "_")
    query = f"""select page as type_page, sum(a.count) as nbre_selection, t.code, a.type_territoire, t.nom 
        from (
            select page, count(code_territoire), code_territoire, type_territoire 
            from consultations.analyses_territoriales 
            where (region = $1 OR REPLACE(region, '_', '-') = $1)
            and date >= $2 and date < $3
            group by code_territoire, type_territoire, page
        ) a 
        join (
            select distinct code, nom 
            from {schema}.territoire
        ) t 
        on t.code = a.code_territoire group by a.page, t.nom, t.code, a.type_territoire order by t.nom"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_dashboards_stats(region, from_date, to_date):
    """
    Get stats related to dashboard visits in specified time interval.

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    query = f"""select a.tableau_bord_id, a.tableau_bord_nom, 
            (count(a.id) / (
                select count(*) from consultations.tableaux_bords where REPLACE(region, '_', '-') = $1 and date >= $2 and date < $3
                )::numeric) * 100 as proportion,
            count(a.id) as nbre
        from consultations.tableaux_bords a
        where REPLACE(a.region, '_', '-') = $1 and date >= $2 and date < $3
        group by a.tableau_bord_id, a.tableau_bord_nom
        order by proportion"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_poi_stats(region, from_date, to_date):
    """
    Get stats related to POI layers visits in specified time interval.

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    query = f"""select a.nom_couche as nom_poi, 
            (count(a.id) / (
                select count(*) from consultations.poi where REPLACE(region, '_', '-') = $1 and date >= $2 and date < $3
                )::numeric) * 100 as proportion,
            count(a.id) as nbre
        from consultations.poi a
        where REPLACE(a.region, '_', '-') = $1 and date >= $2 and date < $3
        group by a.nom_couche
        order by proportion"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_analyses_stats(region, from_date, to_date):
    """
    Get stats related to any analysis visits in specified time interval.

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    query = f"""select m.nom as nom_indicateur, 
            (count(id_indicateur) / (
                select count(*) from consultations.consultations_indicateurs 
                where provenance = 'carto' and REPLACE(region, '_', '-') = $1 and date >= $2 and date < $3
                )::numeric) * 100 as proportion,
            count(id_indicateur) as nbre
        from consultations.consultations_indicateurs a
        join meta.indicateur m 
        on m.id = a.id_indicateur
        where REPLACE(a.region, '_', '-') = $1 and provenance = 'carto' and date >= $2 and date < $3
        group by id_indicateur, m.nom
        order by proportion"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_other_pages_stats(region, from_date, to_date):
    """
    Get stats related to other pages visits in specified time interval.

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    query = f"""select a.page, a.details, 
            (count(a.id) / (
                select count(*) from consultations.autres_pages where REPLACE(region, '_', '-') = $1 and date >= $2 and date < $3
                )::numeric) * 100 as proportion,
            count(a.id) as nbre
        from consultations.autres_pages a
        where REPLACE(a.region, '_', '-') = $1 and date >= $2 and date < $3
        group by a.page, a.details
        order by proportion"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_users_stats(region, from_date, to_date):
    """
    Get stats related to unique and all users visits in specified time interval.
    Will return two values, one called unique and the other called doublon.

    Parameters
    ----------
    region : str
        region name
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    query = f"""select 'unique' as type, count(*) from (select distinct a.ip from consultations.ip_localisation a
                join (
                            select distinct id_utilisateur from (
                            select distinct id_utilisateur, date from consultations.consultations_indicateurs where date >= $2 and date < $3
                            union all
                            select distinct id_utilisateur, date from consultations.analyses_territoriales where date >= $2 and date < $3
                            union all
                            select distinct id_utilisateur, date from consultations.poi where date >= $2 and date < $3
                            union all
                            select distinct id_utilisateur, date from consultations.autres_pages where date >= $2 and date < $3
                            union all
                            select distinct id_utilisateur, date from consultations.tableaux_bords where date >= $2 and date < $3
                            union all
                            select distinct id_utilisateur, date from consultations.actions_cochees where date >= $2 and date < $3
                        ) cv
                    ) ci on ci.id_utilisateur = a.id
                        where a.region_selectionnee = $1
            ) f
        UNION
            select 'doublon' as type, SUM(n) as n from (
                select COUNT(*) AS n from consultations.consultations_indicateurs where date >= $2 and date < $3
                AND REPLACE(region, '_', '-') = $1
                union all
                select COUNT(*) AS n from consultations.analyses_territoriales where date >= $2 and date < $3
                AND REPLACE(region, '_', '-') = $1
                union all
                select COUNT(*) AS n from consultations.poi where date >= $2 and date < $3
                AND REPLACE(region, '_', '-') = $1
                union all
                select COUNT(*) AS n from consultations.autres_pages where date >= $2 and date < $3
                AND REPLACE(region, '_', '-') = $1
                union all
                select COUNT(*) AS n from consultations.tableaux_bords where date >= $2 and date < $3
                AND REPLACE(region, '_', '-') = $1
                union all
                select COUNT(*) AS n from consultations.actions_cochees where date >= $2 and date < $3
                AND REPLACE(region, '_', '-') = $1
            ) cv"""
    return await perform_query_stats(query, region, from_date, to_date)


async def get_stats_by_type(region, stat_type, from_date, to_date):
    """
    Get stats by type for region, from and to date.

    Parameters
    ----------
    region : str
        the region considered (must be valid)
    stat_type : str
        contains the type of stats asked. Must be among following list:

        * analyses
        * autres_pages
        * indicateurs
        * poi
        * strategies_territoriales
        * tableaux_de_bords
        * territoires
        * utilisateurs
    from_date : datetime
        starting date of the interval to consider
    to_date : datetime
        end date of the interval to consider

    Returns
    -------
    list of dict
        data resulting from corresponding query
    """
    if stat_type == "territoires":
        return await get_territorial_stats(region, from_date, to_date)
    elif stat_type == "indicateurs":
        return await get_analyses_stats(region, from_date, to_date)
    elif stat_type == "poi":
        return await get_poi_stats(region, from_date, to_date)
    elif stat_type == "tableaux_de_bords":
        return await get_dashboards_stats(region, from_date, to_date)
    elif stat_type == "autres_pages":
        return await get_other_pages_stats(region, from_date, to_date)
    elif stat_type == "utilisateurs":
        return await get_users_stats(region, from_date, to_date)
    elif stat_type == "strategies_territoriales":
        return await get_strategies_stats(region, from_date, to_date)
    elif stat_type == "analyses":
        return await get_analyses_page_stats(region, from_date, to_date)
    else:
        return []
