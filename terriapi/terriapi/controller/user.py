﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module de gestion des utilisateurs
"""
from __future__ import annotations

import binascii
import hashlib
import json
import smtplib
from email.mime.text import MIMEText
from email.utils import formataddr, formatdate
from typing import NamedTuple

from sanic.log import logger

from terriapi import settings
from terriapi.controller import execute, fetch


class User(NamedTuple):
    user_id: str
    exp: int
    mail: str
    profil: str
    publication: str
    est_admin: bool
    region: str
    territoire: str
    acces_indicateurs_desactives: bool
    global_admin: bool
    prenom: str
    nom: str
    organisation: str
    fonction: str
    territoire_predilection: str
    utiliser_territoire_predilection: bool
    code_postal: str
    can_validate_poi_contributions: bool
    poi_contributions: list
    national_region: str

    def to_dict(self):
        return dict(self._asdict())


def hash_pass(password):
    """Return a hashed password"""
    hpwd = hashlib.pbkdf2_hmac(
        "sha256",
        bytes(password, "utf-8"),
        bytes(settings.get("api", "secret_key"), "utf-8"),
        100000,
    )
    return binascii.hexlify(hpwd).decode()


async def send_mail(
    sender,
    sender_label,
    reply_to: str | None,
    receiver,
    subject,
    content,
    settings,
    typemail="plain",
    encoding="utf-8",
    content_plain="",
):
    """
    Envoi de mail asynchrone

    :returns: un message de statut
    """
    status = "ok"
    smtp = None
    try:
        msg = MIMEText(content, typemail, encoding)
        msg["From"] = formataddr(("TerriSTORY " + sender_label, sender))
        if reply_to and reply_to != sender:
            msg["Reply-to"] = formataddr(("TerriSTORY " + sender_label, reply_to))
        msg["To"] = receiver
        msg["Date"] = formatdate(localtime=True)
        msg["Subject"] = subject

        with smtplib.SMTP(
            settings["api"]["smtp_server"], settings["api"]["smtp_port"]
        ) as server:
            server.starttls()
            server.login(settings["api"]["smtp_user"], settings["api"]["smtp_password"])
            server.sendmail(sender, receiver, msg.as_string())
        logger.info("Mail sent from " + sender)
    except smtplib.SMTPResponseException as e:
        status = "Impossible de se connecter au serveur de mail"
        logger.error(
            f"Couldn't not send mail (error: {e.smtp_code} / {e.smtp_error}) from {sender}: {str(e)}"
        )
    except Exception as e:
        status = "Erreur, impossible d'envoyer le mail"
        logger.error(f"Couldn't not send mail from {sender}: {str(e)}")
    finally:
        if smtp and smtp.is_connected:
            await smtp.quit()

    return status


async def is_active_user(region: str, login: str) -> bool:
    """Check is the specified login corresponds to an active user in the database"""
    query = """SELECT actif FROM utilisateur WHERE region = $1 AND mail = $2"""
    rset = await fetch(query, region, login.lower())
    return bool(rset and rset[0]["actif"])


async def get_user_password(region: str, login: str):
    """Récupère le hash du mot de passe d'un·e utilisateur·trice en fonction de son login.

    Parameters
    ----------
    login : str
        Login de l'utilisateur / utilisatrice, i.e. son email

    Returns
    -------
    str (ou None si l'utilisateur n'a pas été trouvé)
    """
    query = """SELECT password
    FROM utilisateur
    WHERE mail = $2
    AND region = $1
    """
    rset = await fetch(query, region, login.lower())
    if rset:
        return rset[0]["password"]
    return None


async def user(region, login):
    """Récupère toutes les infos d'un.e utilisateur/utilisatrice.

    Parameters
    ----------
    login : str
        Login de l'utilisateur / utilisatrice, i.e. son email

    Returns
    -------
    dict
    """
    query = """select mail
      , prenom
      , nom
      , organisation
      , fonction
      , territoire
      , territoire_predilection
      , utiliser_territoire_predilection
      , code_postal
      , profil
      , coalesce(publication, false) as publication
      , actif
      , region
      , acces_indicateurs_desactives
      , global_admin
    from utilisateur
    where mail = $2 AND region = $1
    """
    rset = await fetch(query, region, login.lower())
    query_poi_contrib = """SELECT layer_name FROM meta.poi_contributions_contacts 
    WHERE email = $2 AND region = $1
    """
    rset_poi_contrib = await fetch(query_poi_contrib, region, login.lower())
    if rset is not None and rset:
        data = dict(rset[0])
        data["poi_contributions"] = [x["layer_name"] for x in rset_poi_contrib]
        data["can_validate_poi_contributions"] = len(rset_poi_contrib) > 0
        return data
    return None


async def users(region=""):
    """Récupère toutes les infos de tous les utilisateurs/utilisatrices.

    Returns
    -------
    dict
    """
    condition_region = ""
    if region != "":
        condition_region = "where region = $1"
    query = f"""select mail
      , prenom
      , nom
      , organisation
      , fonction
      , territoire
      , territoire_predilection
      , utiliser_territoire_predilection
      , code_postal
      , profil
      , coalesce(publication, false) as publication
      , actif
      , connexion
      , to_char(derniere_connexion, 'YYYY-MM-DD HH24:MI') as derniere_connexion
      , to_char(date_creation, 'YYYY-MM-DD HH24:MI') as date_creation
      , region
      , acces_indicateurs_desactives
      , global_admin
    from utilisateur
    {condition_region}
    order by mail
    """

    if condition_region != "":
        rset = await fetch(query, region)
    else:
        rset = await fetch(query)

    if rset is not None and rset:
        return [dict(x) for x in rset]
    return []


async def admins(with_national=False):
    """Récupère la liste de tous les admins

    Returns
    -------
    dict
    """
    query = """SELECT mail, prenom, nom, organisation, region, profil
    FROM utilisateur 
    WHERE profil = 'admin' and actif=true 
    AND EXISTS (SELECT FROM regions_configuration WHERE id = region AND env = $1 AND (NOT is_national OR is_national = $2))
    ORDER BY mail
    """
    rset = await fetch(query, settings.get("api", "env"), with_national)

    if rset is not None and rset:
        return [dict(x) for x in rset]
    else:
        return []


async def create_user(
    prenom,
    nom,
    password,
    mail,
    organisation,
    fonction,
    territoire_predilection,
    utiliser_territoire_predilection,
    territoire,
    region,
    profil,
    actif,
    rgpd="Pas d'accord",
    is_national=False,
):
    """Crée un·e utilisateur·rice en base de données.

    Parameters
    ----------
    prenom : str
        Prénom.
    nom : str
        Nom.
    password : str
        Mot de passe. Il est hashé dans cette fonction. Peut être `None`.
    mail : str
        Email.
    organisation : str
        Structure / organisation.
    territoire_predilection : str
        Territoire représenté par un json { zoneType, zoneId, zoneMaille }.
    utiliser_territoire_predilection : bool
        Utiliser le territoire comme territoire par défaut à la connexion.
    code_postal : str
        Code postal
    profil : str
        'admin' ou 'utilisateur'.
    actif : boolean
        Compte actif ou non.
    rgpd : string
        Case cochée ou non.
    """

    # Check validity of prefered territory
    prefered_territory = json.loads(territoire_predilection)
    if (
        "zoneType" in prefered_territory
        and "zoneMaille" in prefered_territory
        and ("zoneId" in prefered_territory or "zoneType" == "region")
    ):
        schema = region.replace("-", "_")
        valid_zones = await fetch(
            f"SELECT nom FROM {schema}.zone WHERE nom=$1 AND maille=$2",
            prefered_territory["zoneType"],
            prefered_territory["zoneMaille"],
        )
        if not valid_zones:
            territoire_predilection = '{"zoneType":"region", "zoneMaille":"epci"}'
        elif "zoneId" in prefered_territory:
            valid_territories = await fetch(
                f"SELECT nom FROM {schema}.{prefered_territory['zoneType']} WHERE code=$1",
                prefered_territory["zoneId"],
            )
            if not valid_territories:
                territoire_predilection = '{"zoneType":"region", "zoneMaille":"epci"}'
    else:
        territoire_predilection = '{"zoneType":"region", "zoneMaille":"epci"}'

    query = """insert into utilisateur
        (prenom, nom, mail, password, organisation, fonction, territoire_predilection, utiliser_territoire_predilection, territoire, region, profil, actif, rgpd)
     values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13);
    """
    password = hash_pass(password) if password is not None else None
    await execute(
        query,
        prenom,
        nom,
        mail.lower(),
        password,
        organisation,
        fonction,
        territoire_predilection,
        utiliser_territoire_predilection,
        territoire,
        region,
        profil,
        actif,
        rgpd,
    )

    if is_national:
        query = """INSERT INTO meta.user_national_regions (region_id, mail) VALUES($1, $2)"""
        await execute(
            query,
            region,
            mail.lower(),
        )


async def activate_user(region, login, password):
    """Active le compte associé au login.

    Parameters
    ----------
    login : str
        Login (i.e. mail) du compte.
    password : str
        Mot de passe générer de manière aléatoire. Le mot de passe est hashé
        dans cette fonction.
    """
    query = """UPDATE utilisateur
    SET actif=true,
        password=$3
    WHERE mail = $2 AND region = $1
    """
    rset = await fetch(query, region, login.lower(), hash_pass(password))
    return rset


async def deactivate_user(region, login):
    """Désactive le compte associé au login.

    Parameters
    ----------
    login : str
        Login (i.e. mail) du compte
    """
    query = """UPDATE utilisateur
    SET actif=false,
        password=null
    WHERE mail = $2 AND region = $1
    """
    rset = await fetch(query, region, login.lower())
    return rset


async def update_user(region: str, login: str, userdata: dict):
    if userdata["territoire_predilection"] is not None:
        # Check validity of prefered territory
        prefered_territory = json.loads(userdata["territoire_predilection"])
        if (
            "zoneType" in prefered_territory
            and "zoneMaille" in prefered_territory
            and ("zoneId" in prefered_territory or "zoneType" == "region")
        ):
            # TODO: also validate content
            """valid_zones = await fetch(
                f"SELECT nom FROM {region}.zone WHERE nom=$1 AND maille=$2",
                prefered_territory["zoneType"],
                prefered_territory["zoneMaille"],
            )
            if not valid_zones:
                userdata["territoire_predilection"] = None
            elif "zoneId" in prefered_territory:
                valid_territories = await fetch(
                    f"SELECT nom FROM {region}.{prefered_territory['zoneType']} WHERE code=$1",
                    prefered_territory["zoneId"],
                )
                if not valid_territories:
                    userdata["territoire_predilection"] = None"""
        else:
            userdata["territoire_predilection"] = None

    keys = [key for (key, val) in userdata.items() if val is not None]
    values = [val for val in userdata.values() if val is not None]
    query = f"""UPDATE utilisateur SET
    {", ".join(f"{key}=${i+3}" for (i, key) in enumerate(keys))}
    WHERE mail = $2 AND region=$1"""
    await fetch(query, region, login.lower(), *values)


async def delete_user(region, login):
    """Efface le compte associé au login.

    Parameters
    ----------
    login : str
        Login (i.e. mail) du compte
    """
    query = """DELETE FROM utilisateur
    WHERE mail = $2 AND region = $1
    """
    rset = await fetch(query, region, login.lower())
    return rset


async def set_user_password(region, login, password):
    """Stocke un nouveau mot de passe associé au login.

    Parameters
    ----------
    login : str
        Login (i.e. mail) du compte.
    password : str
        Mot de passe générer de manière aléatoire. Le mot de passe est hashé
        dans cette fonction.
    """
    query = """UPDATE utilisateur
    SET password=$3
    WHERE mail = $2 AND region = $1
    """
    rset = await fetch(query, region, login.lower(), hash_pass(password))
    return rset


async def update_connexion_info(region, login):
    """Met à jour les données de connexion associées à un compte.

    Parameters
    ----------
    loging : str
        Login (i.e. mail) du compte
    """
    update = """
    UPDATE utilisateur
    SET connexion = connexion + 1, derniere_connexion = now()
    WHERE region = $1 AND mail = $2
    """
    await execute(update, region, login.lower())


async def store_refresh_token(region, login, token):
    """Stocke le refresh token JWT associé à un login.

    Parameters
    ----------
    login : str
         Login (i.e. mail) du compte
    token : str
         Token généré par sanic-jwt.
    """
    query = """insert into refresh_token
     (region, mail, token)
     values ($1, $2, $3);
    """
    await fetch(query, region, login.lower(), token)


async def get_refresh_token(region, login):
    """Récupère le refresh token JWT associé à un login.

    Parameters
    ----------
    login : str
         Login (i.e. mail) du compte

    Returns
    -------
    dict
        keys: token, creation_date
    """
    query = """select token, creation_date
    from refresh_token
    where mail = $2 AND region = $1;
    """
    rset = await fetch(query, region, login.lower())
    return dict(rset[0])


async def delete_refresh_token(region, login):
    """Efface le refresh token JWT associé à un login.

    Quand le token est invalidé par sa date d'expiration par exemple, on
    l'efface. Pour ensuite en créer un nouveau avec une nouvelle date.

    Parameters
    ----------
    login : str
         Login (i.e. mail) du compte
    """
    query = """delete from refresh_token
    where mail = $2 AND region = $1;
    """
    await fetch(query, region, login.lower())


async def auth_user(request, region):
    """
    Returns information about the authenticated user

    Parameters
    ----------
    region : str
        region key

    Returns
    -------
    User
        object with user information from database
    """
    payload = await request.app.ctx.auth.extract_payload(request, region)
    return await request.app.ctx.auth.retrieve_user(request, payload)


async def get_regions_national_user(usermail):
    """
    Returns information about the authenticated user

    Parameters
    ----------
    region : str
        region key

    Returns
    -------
    User
        object with user information from database
    """
    query = """SELECT DISTINCT region_id as id, r.label FROM meta.user_national_regions u
    LEFT JOIN regions_configuration r ON r.id = u.region_id
    WHERE region_id <> 'national' AND mail = $1 AND r.is_national = true
    """
    return await fetch(query, usermail)


async def is_user_in_national_region(usermail, region):
    """
    Checks whether a user is associated to a national region.

    Parameters
    ----------
    usermail : str
    region : str

    Returns
    -------
    boolean
        whether there is a user associated with a region in user_national_regions
        table.
    """
    query = """SELECT EXISTS(
        SELECT 1 FROM meta.user_national_regions u
        WHERE region_id = $1 AND mail = $2
    )
    """
    res = await fetch(query, region, usermail)
    return res[0]["exists"]
