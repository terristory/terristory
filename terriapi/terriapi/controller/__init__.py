﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module de gestion des requêtes SQL utilisées par l'API.
"""

from __future__ import annotations

import os
import re
import statistics
import string
from datetime import datetime
from time import perf_counter

import pandas as pd
from sanic.log import logger
from sqlalchemy import create_engine
from sqlalchemy.sql import text

from terriapi import settings
from terriapi.exceptions import ForceUpdateAllowedException

alphabet = list(string.ascii_lowercase)

# une instance de asyncpg.Pool initalisée par l'application Sanic
db = None


async def fetch(*args):
    """
    Fonction permettant de récupérer les données en base
    en utilisant le pool de connection.

    Parameters
    ----------
    args: list
        liste des arguments de la fonction asyncpg fetch

    Returns
    -------
        une promesse de résultat
    """
    async with db.acquire() as conn:
        async with conn.transaction():
            t1 = perf_counter()
            try:
                res = await conn.fetch(*args)
            except Exception as exc:
                logger.error(
                    "request ({:.2f}s), parameters:{} : \n\t{}".format(
                        perf_counter() - t1, args[1:], args[0]
                    )
                )
                raise exc
            logger.debug(
                "request ({:.2f}s), parameters:{} : \n\t{}".format(
                    perf_counter() - t1, args[1:], args[0]
                )
            )
            return res


async def execute(*args):
    """Exécute une requete en utilisant le pool de connection.

    Parameters
    ----------
    args: list
        liste des arguments de la fonction asyncpg execute

    Returns
    -------
        None
    """

    async with db.acquire() as conn:
        async with conn.transaction():
            t1 = perf_counter()
            rset = await conn.execute(*args)

            logger.debug(
                "request ({:.2f}s), parameters:{} : \n\t{}".format(
                    perf_counter() - t1, args[1:], args[0]
                )
            )
            return rset


async def executemany(*args):
    """Exécute une requete en utilisant le pool de connection.

    Parameters
    ----------
    args: list
        liste des arguments de la fonction asyncpg execute

    Returns
    -------
        None
    """
    async with db.acquire() as conn:
        async with conn.transaction():
            t1 = perf_counter()
            rset = await conn.executemany(*args)
            logger.debug(
                "request ({:.2f}s), parameters:{} : \n\t{}".format(
                    perf_counter() - t1, args[1:], args[0]
                )
            )
            return rset


def get_pg_engine():
    """Return the sqlAlchemy engine to access the postgresql database"""
    pg_user = settings.get("api", "pg_user")
    pg_password = settings.get("api", "pg_password")
    pg_port = settings.get("api", "pg_port")
    pg_host = settings.get("api", "pg_host")
    pg_name = settings.get("api", "pg_name")
    engine = create_engine(
        f"postgresql://{pg_user}:{pg_password}@{pg_host}:{pg_port}/{pg_name}"
    )
    return engine


async def copy_to_table(
    schema: str,
    table: str,
    source,
    columns: list[str] | None = None,
    is_national: bool = False,
    replace: bool = True,
    recreate_table_query: str | None = None,
) -> int:
    """Copie des données tabulaires issues d'un fichier (.csv) dans une table.

    La table doit exister.

    Parameters
    ----------
    schema : str
        Nom du schéma
    table : str
        Nom de la table où vont être copiées les données
    source : Path
        Nom du fichier CSV
    columns : list
        Colonnes à préciser si en-tête à un ordre différent de l'ordre des colonnes
    replace : boolean
        Efface les données de la table avant la copie. Si Faux, ajoute les données

    Returns
    -------
    int
        Nombre de lignes insérées
    """

    async with db.acquire() as conn:
        async with conn.transaction():
            if replace:
                await conn.fetch("truncate table " + schema + "." + table)
            if recreate_table_query is not None:
                await conn.fetch("drop table " + schema + "." + table)
                await conn.fetch(recreate_table_query)
            # Handling pixels data that contain geom column
            if columns and "geom" in columns:
                await conn.execute(
                    f"""
                    ALTER TABLE {schema}.{table}
                    ALTER COLUMN geom TYPE text;
                    """
                )
                await conn.execute(
                    f"""
                    ALTER TABLE {schema}.{table}
                    DROP COLUMN id;
                    """
                )
            result = await conn.copy_to_table(
                table,
                source=source,
                schema_name=schema,
                header=True,
                format="csv",
                delimiter=";",
                columns=columns,
            )
            # Handling pixels data that contain geom column
            if columns and "geom" in columns:
                await conn.execute(
                    f"""
                    ALTER TABLE {schema}.{table}
                    ALTER COLUMN geom TYPE geometry(Polygon,3857)
                    USING ST_GeomFromText(geom, 3857)
                    """
                )
                await conn.execute(
                    f"""
                    ALTER TABLE {schema}.{table}
                    ADD COLUMN id SERIAL PRIMARY KEY
                    """
                )

                await conn.fetch(
                    f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_geom_idx ON {schema}.{table} USING GIST(geom);"""
                )

            if columns and "commune" in columns:
                await conn.fetch(
                    f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_idx ON {schema}.{table} (commune)""",
                )
            if columns and "code" in columns:
                await conn.fetch(
                    f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_code_idx ON {schema}.{table} (code)""",
                )
            if columns and "annee" in columns:
                await conn.fetch(
                    f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_year_idx ON {schema}.{table} (annee)""",
                )
    return int(result.split()[-1])


async def table_datatypes(schema, table):
    """Récupère les noms et types des champs d'une table donnée.

    Si le résultat renvoie un dictionnaire vide, la table n'existe sans doute pas (ou
    inaccessible en lecture par le pguser).

    Parameters
    ----------
    schema : str
    table : str

    Returns
    -------
    dict
        nom du champs: type
    """
    query = """select column_name
      ,data_type
    from information_schema.columns
    where table_schema=$1
      and table_name=$2
    order by ordinal_position
    """
    rset = await fetch(query, schema, table)
    return {x["column_name"]: x["data_type"] for x in rset}


async def list_categories(region):
    """Récupère la liste des catégories pour une région donnée.

    Parameters
    ----------
    region : str
        Nom de la région sous la forme 'nom-de-la-region'.

    Returns
    -------
    dict
        dict {nom: [list modalite ids]}
    """
    query = """select nom
      , array_agg(modalite_id) as modalites
    from meta.categorie
    where region = $1
    group by nom
    order by nom
    """
    rset = await fetch(query, region)
    return {x["nom"]: x["modalites"] for x in rset}


async def get_categories_full_contents(region):
    """Récupère la liste des catégories pour une région donnée.

    Parameters
    ----------
    region : str
        Nom de la région sous la forme 'nom-de-la-region'.

    Returns
    -------
    dict
        dict {name: [all data]}
    """
    query = """select nom, modalite, modalite_id, couleur, ordre
    from meta.categorie
    where region = $1
    """
    rset = await fetch(query, region)
    output = {}
    for x in rset:
        if x["nom"] not in output:
            output[x["nom"]] = []
        useful_data = dict(x)
        del useful_data["nom"]
        output[x["nom"]].append(useful_data)
    return output


async def list_categories_modalites(region):
    """Récupère la liste des catégories et des modalites pour une région donnée.

    Parameters
    ----------
    region : str
        Nom de la région sous la forme 'nom-de-la-region'.

    Returns
    -------
    dict
        dict {nom: [list categories modalites ids]}
    """
    query = """select modalite_id, nom||'.'||modalite as cat_mod from meta.categorie
        where region = $1
        order by nom, modalite
    """
    rset = await fetch(query, region)
    return [{"label": x["cat_mod"], "value": x["modalite_id"]} for x in rset]


async def check_category_exists(region: str, category_name: str) -> bool:
    query = (
        """SELECT EXISTS (SELECT FROM meta.categorie WHERE nom = $1 AND region = $2)"""
    )
    rset = await fetch(query, category_name, region)
    return rset[0]["exists"]


async def list_tables(region):
    """Récupère la liste des tables de données (indicateurs) pour une région donnée.

    Parameters
    ----------
    region : str
        Nom de la région sous la forme 'nom-de-la-region'.

    Returns
    -------
    dict
        dict {nom: [list modalite ids]}
    """
    schema = region.replace("-", "_")
    query = """
    with table_names as (
        select t.table_name as nom, p.date_perimetre, p.only_for_zone
        from information_schema.tables t
        join meta.perimetre_geographique p on p.nom_table = t.table_name
        where t.table_schema = $1
        and p.region = $2
    )
    select t.nom, string_agg(i.nom, ', ') as indicateurs, date_perimetre, t.only_for_zone
    from table_names t
    left join meta.indicateur i on (
        i.data ~ ('.*?[^a-zA-Z_]' || t.nom || '[^a-zA-Z_].*?') 
        OR i.data ~ ('^' || t.nom || '[^a-zA-Z_].*?') 
        OR i.data ~ ('.*?[^a-zA-Z_]' || t.nom || '$') 
        OR TRIM(BOTH FROM i.data) = TRIM(BOTH FROM t.nom)
    ) and region = $2
    group by t.nom, date_perimetre, t.only_for_zone
    order by t.nom
    """
    rset = await fetch(query, schema, region)
    return [dict(x) for x in rset]


async def get_data_tables_headers(region, list_tables):
    """
    Get the first 5 rows of each data table for a given region.

    Parameters
    ----------
    region : str
        Name of the region written in the form 'name-of-the-region'.

    Returns
    -------
    dict
        dict {nom: [list modalite ids]}
    """
    schema = region.replace("-", "_")
    header_dict = {}
    # TODO : optimize the selection of the first 5 rows
    for table_name in list_tables:
        query = f"""
        SELECT * FROM {schema}.{table_name} LIMIT 5
        """
        rset = await fetch(query)
        header_dict[table_name] = [dict(element) for element in rset]
    return header_dict


async def get_update_date(region, list_tables):
    """
    Get update date of each data table for a given region.

    Parameters
    ----------
    region : str
    list_tables : list

    Returns
    -------
    dict
    """
    schema = region.replace("-", "_")
    date_dict = {}
    for table_name in list_tables:
        query = f"""
                SELECT nom_donnee, to_char(date_maj, 'YYYY-MM-DD HH24:MI:SS') as date_maj
                FROM {schema}.historique_indicateurs 
                WHERE nom_donnee = $1; """

        rset = await fetch(query, table_name)
        date_dict[table_name] = [dict(element) for element in rset]
    return date_dict


async def get_last_update_date(region, list_tables):
    """
    Get update date of each data table for a given region.

    Parameters
    ----------
    region : str
    list_tables : list

    Returns
    -------
    dict
    """
    schema = region.replace("-", "_")
    date_dict = {}
    for table_name in list_tables:
        query = f"""
                SELECT nom_donnee, to_char(max(date_maj), 'YYYY-MM-DD HH24:MI:SS') as date_maj
                FROM {schema}.historique_indicateurs 
                WHERE nom_donnee = $1
                GROUP BY nom_donnee"""

        rset = await fetch(query, table_name)
        date_dict[table_name] = [dict(element) for element in rset]
    return date_dict


async def liste_tables_jdd_communs(region):
    """
    Récupère la liste des tables de données (indicateurs) pour une région donnée.

    Parameters
    ----------
    region : str
        Nom de la région sous la forme 'nom-de-la-region'.

    Returns
    -------
    dict
        dict {nom: [list modalite ids]}
    """
    schema = region.replace("-", "_")
    query = """
    select t.table_name as nom, p.date_perimetre
    from information_schema.tables t
    join meta.perimetre_geographique p on p.nom_table = t.table_name
    where t.table_schema = $1
    and p.region = $2
    """
    rset = await fetch(query, schema, region)
    return rset


async def get_common_dataset_table(global_region, table_name):
    """
    Retrieve the content from a table of common data set.

    Parameters
    ----------
    global_region : str
        Global region key used to load data from.
    table_name : str
        The table name that we wish to retrieve

    Returns
    -------
    pd.DataFrame
        the dataframe of all existing rows (if the table exists)
    boolean
        False if the table is not found in common PostgreSQL schema
    """
    common_schema = global_region.replace("-", "_")
    common_region = global_region

    query = """
    select t.table_name as nom, p.date_perimetre
    from information_schema.tables t
    join meta.perimetre_geographique p on p.nom_table = t.table_name
    where t.table_schema = $1
    and p.region = $2
    and t.table_name = $3
    """
    rset = await fetch(query, common_schema, common_region, table_name)
    if len(rset) != 1:
        return False
    else:
        query = f"""
        select * FROM {common_schema}.{table_name}
        """
        rset = await fetch(query)
        return pd.DataFrame(map(dict, rset))


async def get_data_table(region, table_name):
    """
    Get the full content of a table.

    Parameters
    ----------
    region : str
        Name of the region written in the form 'name-of-the-region'.
    table_name : str
        Table name in the region

    Returns
    -------
    dict
        [list of rows]
    """
    schema = region.replace("-", "_")
    check_query = """
    select t.table_name as nom
    from information_schema.tables t
    where t.table_schema = $1
    and t.table_name = $2
    """
    rset = await fetch(check_query, schema, table_name)
    if len(rset) != 1:
        return False
    else:
        query = f"""
        SELECT * FROM {schema}.{table_name}
        """
        rset = await fetch(query)
        return pd.DataFrame(map(dict, rset))


def conversion_chaine_en_binaire(chaine):
    """convertit une chaine de caractères en binaire (un octet par lettre).

    Parameters
    ----------
    chaine : chaîne de caractères
        chaîne de caractères à convertir en binaire

    Returns
    --------
    Chaîne de caractères
        Chaîne de caractères au format binaire : suite d'octets séparés à chaque fois par une espace
    """
    return " ".join(format(ord(c), "b") for c in chaine)


def conversion_binaire_en_chaine(binaire):
    """Décode une chaîne de caractères en binaire.

    Parameters
    ----------
    chaine : chaîne de caractères en binaire
        chaîne de caractères décodée

    Returns
    --------
    Chaîne de caractères
        Chaîne de caractères décodée et donc lisible
    """
    if binaire == "":
        return binaire
    liste_chaines_binaire = binaire.split(" ")
    return "".join([chr(int(x, 2)) for x in liste_chaines_binaire])


async def chercher_fichier_des_communes(chemin, nom, l93=True):
    """Retourne le chemin absolu vers le fichier COMMUNES.shp au sein d'une arborescence dont la racine est à indiquer en paramètre

    Parameters
    ----------

    chemin: str
        Chemin d'accès au répertoire depuis lequel lancer la recherche
    """
    for root, dirs, files in os.walk(chemin, topdown=False):
        for name in files:
            fichier = os.path.join(root, name)
            liste_fichiers = fichier.split("/")
            if (
                l93 and liste_fichiers[-1] == nom and "LAMB93_FR" in liste_fichiers[-2]
            ):  # On cherche la couche des communes projetées en Lambert 93
                return fichier
            elif not l93 and liste_fichiers[-1] == nom:
                return fichier


class QueryBuilder:
    """
    Classe permettant de construire des requêtes SQL complexes à l'aide de méthodes
    simples. L'intérêt principal est de pouvoir gérer automatiquement les alias des
    relations.
    """

    def __init__(self, from_relation, schema=None):
        self._schema = schema
        self.from_relation = (
            from_relation if schema is None else ".".join([schema, from_relation])
        )
        self.select = []
        self.join = []
        self.where = []
        self.groupby = []
        self.having = []
        self.orderby = []
        # assocation nom de table -> alias sql
        self.aliases = {(self.from_relation, 0): "a"}
        self.alphabet = iter(alphabet[1:])  # l'alphabet sans le a
        # paramètres d'interpolation pour injecter dans les requêtes asyncpg
        self.params = []

    def add_select(
        self, expression=None, schema=None, table=None, column=None, index=0
    ):
        if expression and not table:
            self.select.append(expression)
        elif expression and table:
            # on spécifie la table dont il faut injecter l'alias dans l'expression
            if schema:
                table = ".".join([schema, table])
            else:
                table = (
                    table if self._schema is None else ".".join([self._schema, table])
                )
            self.select.append(expression.format(self.aliases[(table, index)]))
        else:
            if schema:
                table = ".".join([schema, table])
            else:
                table = (
                    table if self._schema is None else ".".join([self._schema, table])
                )
            self.select.append("{}.{}".format(self.aliases[(table, index)], column))

    def add_join(
        self, table, schema=None, jointype="join", on="", index=0, use_join_alias=False
    ):
        # use_join_alias : sert pour les requpete ayant plusieurs fois l'alias à remplacer dans la chaine de caractère
        if schema:
            table = ".".join([schema, table])
        else:
            table = table if self._schema is None else ".".join([self._schema, table])

        if (table, index) not in self.aliases:
            self.aliases[(table, index)] = next(self.alphabet)

        if use_join_alias:
            # Cette condition est nécessaire, sinon le simple format sans préciser l'alias ne peut fonctionner que s'il n'y a
            # qu'une seule occurence à remplacer. Or on a maintenant le cas où il y en a plusieurs (car d'un join sur plusieurs colonnes)
            on = on.format(join_alias=self.aliases[(table, index)])
        else:
            on = on.format(self.aliases[(table, index)])
        self.join.append(
            "{} {} as {} on {}".format(
                jointype, table, self.aliases[(table, index)], on
            )
        )
        return self.aliases[(table, index)]

    def add_filter(
        self, table, column, value, schema=None, operator="=", index=0, as_param=True
    ):
        # as_param : défini si un paramètre doit être fourni à la requête au moment de l'exécution
        #            ou si c'est le code qui s'occupe de compléter le filtre
        if as_param:
            self.params.append(value)
        if schema:
            table = ".".join([schema, table])
        else:
            table = table if self._schema is None else ".".join([self._schema, table])

        if as_param:
            condition = "{}.{} {} ${}".format(
                self.aliases[(table, index)], column, operator, len(self.params)
            )
        else:
            condition = "{}.{} {} {}".format(
                self.aliases[(table, index)], column, operator, value
            )

        if self.where:
            self.where.append("and {}".format(condition))
        else:
            self.where.append("where {}".format(condition))

    def add_complex_filter(self, tables, values=None, expression=None):
        """
        exemple d'expression à remplir : '{0}.id = ${{}} or {1}.id = ${{}}'
        """
        tables_with_schema = []
        for t, alias_id in tables:
            t = ".".join([self._schema, t]) if self._schema is not None else t
            tables_with_schema.append((t, alias_id))
        condition = expression.format(*[self.aliases[t] for t in tables_with_schema])
        condition = condition.format(
            *[len(self.params) + idx for idx, v in enumerate(values, start=1)]
        )
        self.params.extend(values)

        if self.where:
            self.where.append("and ({})".format(condition))
        else:
            self.where.append("where ({})".format(condition))

    def add_complex_having(self, tables, values=None, expression=None):
        tables_with_schema = []
        for t, alias_id in tables:
            t = ".".join([self._schema, t]) if self._schema is not None else t
            tables_with_schema.append((t, alias_id))
        condition = expression.format(*[self.aliases[t] for t in tables_with_schema])
        condition = condition.format(
            *[len(self.params) + idx for idx, v in enumerate(values, start=1)]
        )
        self.params.extend(values)

        if self.having:
            self.having.append("and ({})".format(condition))
        else:
            self.having.append("having ({})".format(condition))

    def add_groupby(self, table, column, schema=None, index=0):
        if schema:
            table = ".".join([schema, table])
        else:
            table = table if self._schema is None else ".".join([self._schema, table])
        if self.groupby:
            self.groupby.append("{}.{}".format(self.aliases[(table, index)], column))
        else:
            self.groupby.append(
                "group by {}.{}".format(self.aliases[(table, index)], column)
            )

    def add_orderby(self, table, column, schema=None, index=0):
        if schema:
            table = ".".join([schema, table])
        else:
            table = table if self._schema is None else ".".join([self._schema, table])

        if self.orderby:
            self.orderby.append("{}.{}".format(self.aliases[(table, index)], column))
        else:
            self.orderby.append(
                "order by {}.{}".format(self.aliases[(table, index)], column)
            )

    def get_alias(self, table, index=0):
        """
        Récupère l'alias créé pour la table et l'index correspondant
        index > 0 si plusieurs références à la même table
        """
        table = table if self._schema is None else ".".join([self._schema, table])
        return self.aliases[(table, index)]

    @property
    def query(self):
        return "\nselect {} \nfrom {} as a \n{}\n{}\n{}\n{}\n{}\n".format(
            "\n,".join(self.select),
            self.from_relation,
            "\n".join(self.join),
            "\n".join(self.where),
            ",".join(self.groupby),
            ",".join(self.having),
            ",".join(self.orderby),
        )


async def add_to_data_history(region, schema, user, action, data_name, details=""):
    """
    Adds action to data history table for current region.

    Parameters
    ----------
    region : string
        current region key
    schema : string
        the schema in which to insert the event
    user : object
        the user object
    action : string
        the action performed
    data_name : string
        the data name or a string representing the data
    details : string, optional
        additionnal details, either pure string or formatted json, by default ""
    """
    table_name = schema + ".historique_indicateurs"
    req = (
        """
        insert into """
        + table_name
        + """(nom_donnee, mail, action, date_maj, 
        autres_informations, region) values($1, $2, $3, $4, $5, $6);
        """
    )
    return await execute(
        req, data_name, user.mail, action, datetime.now(), details, user.region
    )


async def import_data_table_from_common_base(region, table, maj):
    """Copies a data table from the 'france' schema to the regional schema.
    Keeps only the communes of the specified region.

    A commune belongs to a region if the first two characters of its code correspond to a department code in <region>.departement.
    With this definition, even the communes of the region that do not exist in the current perimeter (in <region>.commune) are kept.

    If the table contains a 'commune_dest' column (flow data), the filtering is also done on this second column.

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    table : str
        table name
    maj : any
        Truthy if updating an existing table, falsy if importing a new table
    """
    schema = region.replace("-", "_")

    # Check if "commune_dest" column exists. If exists also filter on this second commune column.
    req = """
        SELECT EXISTS (
        SELECT FROM information_schema.columns 
        WHERE  table_schema = 'france'
        AND    table_name   = $1
        AND    column_name = 'commune_dest'
        )
    """
    res = await fetch(req, table)
    col_commune_dest_exists = res[0]["exists"]
    commune_dest_filter = ""
    if col_commune_dest_exists:
        commune_dest_filter = f"OR left(p.commune_dest,2) in (SELECT distinct left(code,2) FROM {schema}.departement)"

    async with db.acquire() as conn:
        async with conn.transaction():
            if maj:
                req_suppr = """
                            DROP TABLE IF EXISTS {schema}.{table};
                            """.format(
                    schema=schema, table=table
                )

                await conn.execute(req_suppr)

            req = """
                CREATE TABLE {schema}.{table} AS (
                    SELECT p.*
                    FROM france.{table} p
                    LEFT JOIN {schema}.commune c 
                    ON c.code = p.commune 
                    WHERE left(p.commune,2) in (SELECT distinct left(code,2) FROM {schema}.departement)
                    {commune_dest_filter}
                )
            """.format(
                schema=schema, table=table, commune_dest_filter=commune_dest_filter
            )

            await conn.execute(req)

            # Create indexes on the new table
            await conn.execute(
                f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_idx ON {schema}.{table} (commune)""",
            )

            await conn.execute(
                f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_year_idx ON {schema}.{table} (annee)""",
            )

            if col_commune_dest_exists:
                await conn.execute(
                    f"""CREATE INDEX IF NOT EXISTS {schema}_{table}_commune_dest_idx ON {schema}.{table} (commune_dest)""",
                )


async def import_category_from_common_base(
    region: str, category: str, force: bool = False
) -> None:
    """Copies a category from the 'france' to the regional data.

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    category : str
        category name
    force : bool
        Will remove existing category with this name before importing
    """
    async with db.acquire() as conn:
        async with conn.transaction():
            if force:
                deletion_query = """
                            DELETE FROM meta.categorie WHERE region = $1 AND nom = $2;
                            """
                await conn.execute(deletion_query, region, category)

            req = """
                INSERT INTO meta.categorie (
                    SELECT nom, modalite, modalite_id, couleur, ordre, $1 
                    FROM meta.categorie 
                    WHERE nom = $2 AND region = 'france'
                )
            """
            await conn.execute(req, region, category)


async def import_indicator_from_common_base(
    region: str, source_indicator_id: int, force_update: bool = False
) -> None:
    """Copies a indicator from the 'france' schema to the regional data.

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    indicator : int
        indicator ID
    force_update : bool
        Will remove existing category with this name before importing
    """

    schema = region.replace("-", "_")

    async with db.acquire() as conn:
        async with conn.transaction():
            metadata = await conn.fetch(
                f"""SELECT data, type FROM meta.indicateur
                    WHERE id = $1 AND region = 'france'""",
                source_indicator_id,
            )
            if len(metadata) != 1:
                raise ValueError("No valid indicator found.")
            metadata = metadata[0]
            formula = compute_formula(metadata["data"])
            dependencies = [formula["indicateur"]]
            if formula["ratio"] != "":
                dependencies.append(formula["ratio"].replace("/maille.", ""))

            table_exists = await conn.fetch(
                f"""(
                    SELECT table_name AS name, 'table' AS exist_type FROM information_schema.tables 
                    WHERE table_schema = $1
                    AND table_name = ANY($3)
                ) UNION (
                    SELECT nom AS name, 'indicator' AS exist_type FROM meta.indicateur
                    WHERE region = $1 AND nom = (
                        SELECT nom FROM meta.indicateur
                        WHERE region = 'france' 
                        AND id = $2
                    )
                ) UNION (
                    SELECT nom AS name, 'category' AS exist_type FROM meta.categorie WHERE region = $1 AND nom = ANY(
                        SELECT categorie FROM meta.chart 
                        WHERE indicateur = $2
                    )
                )""",
                schema,
                source_indicator_id,
                dependencies,
            )
            if len(table_exists) > 0 and not force_update:
                raise ForceUpdateAllowedException(
                    "existing: "
                    + (
                        ", ".join(
                            [
                                f"{t['name']} (type: {t['exist_type']})"
                                for t in table_exists
                            ]
                        )
                    )
                )

            if force_update:
                query = f"""DELETE FROM meta.chart
                            WHERE indicateur = (
                                SELECT id FROM meta.indicateur
                                WHERE region = $1 
                                AND nom = (
                                    SELECT nom FROM meta.indicateur
                                    WHERE region = 'france' 
                                    AND id = $2
                                )
                            )"""
                await conn.execute(query, region, source_indicator_id)
                query = f"""DELETE FROM meta.indicateur
                            WHERE region = $1 AND nom = (
                                SELECT nom FROM meta.indicateur
                                WHERE region = 'france' 
                                AND id = $2
                            )"""
                await conn.execute(query, region, source_indicator_id)

            query_insertion = """
                            insert into meta.indicateur (
                                nom, data, type, color_start, color_end,
                                unit, decimals, ui_theme, years, region,
                                isratio, filter, confidentiel, only_for_zone, disabled_for_zone,
                                disabled_for_macro_level, donnees_exportables,
                                credits_analysis_producers, credits_data_sources, credits_data_producers,
                                estimated_years, disabled_in_dashboard, active)
                            (
                                SELECT nom, data, type, color_start, color_end,
                                unit, decimals, 'Importation socle commun', years, $2,
                                isratio, filter, confidentiel, only_for_zone, disabled_for_zone,
                                disabled_for_macro_level, donnees_exportables,
                                credits_analysis_producers, credits_data_sources, credits_data_producers,
                                estimated_years, disabled_in_dashboard, false
                                FROM meta.indicateur
                                WHERE id = $1 AND region = 'france')
                            returning id
                            """

            logger.info("insertion dans 'meta.indicateur'")
            rset = await conn.fetch(query_insertion, source_indicator_id, region)
            new_indicator_id = rset[0]["id"]

            # copy data and their perimeter configuration
            for dependency in dependencies:
                if force_update:
                    query = f"""DROP TABLE {schema}.{dependency}"""
                    await conn.execute(query)
                    query = f"""DELETE FROM meta.perimetre_geographique
                                WHERE region = $1 AND nom_table = $2"""
                    await conn.execute(query, region, dependency)

                insert_perimeter = """
                                        INSERT INTO meta.perimetre_geographique
                                        (SELECT nom_table, $1, date_perimetre, true, only_for_zone 
                                        FROM meta.perimetre_geographique 
                                        WHERE nom_table = $2 AND region = 'france')
                                        """
                await conn.fetch(insert_perimeter, region, dependency)

                insert_data = f"""
                    CREATE TABLE {schema}.{dependency} (LIKE france.{dependency} INCLUDING ALL)
                """
                await conn.execute(insert_data)
                commune_column_exists = await conn.fetch(
                    """SELECT EXISTS (
                        SELECT FROM information_schema.columns
                        WHERE column_name='commune' AND table_schema=$1 AND table_name=$2
                    )""",
                    schema,
                    dependency,
                )
                if commune_column_exists and commune_column_exists[0].get("exists"):
                    await conn.execute(
                        f"CREATE INDEX IF NOT EXISTS {schema}_{dependency}_idx ON {schema}.{dependency} (commune)"
                    )
                await conn.execute(
                    f"CREATE INDEX IF NOT EXISTS {schema}_{dependency}_year_idx ON {schema}.{dependency} (annee);"
                )

            if metadata["type"] != "wms_feed":
                logger.info("nouvel indicateur avec id=%d", new_indicator_id)
                logger.info("insertion dans 'meta.chart'")
                query_charts = """INSERT INTO meta.chart 
                (SELECT $1, titre, type, categorie, visible, ordre, data_type
                FROM meta.chart
                WHERE indicateur = $2)
                """
                await conn.execute(query_charts, new_indicator_id, source_indicator_id)

                if force_update:
                    deletion_query = """
                        DELETE FROM meta.categorie WHERE region = $1 AND nom = ANY(
                            SELECT categorie FROM meta.chart 
                            WHERE indicateur = $2
                        );
                        """
                    await conn.execute(deletion_query, region, new_indicator_id)

                req = """
                    INSERT INTO meta.categorie (
                        SELECT nom, modalite, modalite_id, couleur, ordre, $1 
                        FROM meta.categorie 
                        WHERE nom = ANY(
                            SELECT categorie FROM meta.chart 
                            WHERE indicateur = $2
                        ) AND region = 'france'
                    )
                """
                await conn.execute(req, region, new_indicator_id)

            await conn.fetch(
                """INSERT INTO meta.indicateur_representation_details
                    (SELECT $1, name, value
                    FROM meta.indicateur_representation_details
                    WHERE indicateur_id = $2)""",
                new_indicator_id,
                source_indicator_id,
            )

            return new_indicator_id


def compute_formula(raw_formula):
    """Retourne la définition de l'indicateur (colonne data) décomposée dans un dictionnaire

    Returns
    --------
    dict
    """
    definition_analyse = raw_formula
    # Récupération des opérations de la formule de calcul.
    # On obtient ainsi une liste qui contient chacune des opérations.
    # Exemple : air_polluant_covnm*1000000/maille.population donne
    # ['*', '/']
    liste_operations = re.findall("[/+*-]", definition_analyse)
    # Un indicateur est composée par un jeu de données initial (production EnR,
    # émissions de GES, consommation énergétique etc.), d'un coefficient par
    # lequel on multiplie les valeurs de ce premier jeu de données (*1000 par
    # exemple) et enfin d'un ratio (autre jeu de données dont les valeurs servent
    # à diviser l'indicateur initial).
    definition_analyse_decortiquee = {
        "indicateur": "",  # Initialisation à une chaine vide par défaut.
        "coeff": "",  # Chaine vide
        "ratio": "",  # Chaine vide
        "coeff_details": {},
    }

    if not liste_operations:
        definition_analyse_decortiquee["indicateur"] = raw_formula
        return definition_analyse_decortiquee

    # Position du premier caractère qui sépare le nom de l'indicateur
    # du reste de la formule de calcul
    pos = definition_analyse.find(liste_operations[0])
    indicateur = definition_analyse[:pos]  # Récupération du nom de l'analyse
    # we try to securize the formula
    check_reg_indicateur_formulae = re.compile("^([a-zA-Z]+)([a-zA-Z0-9_]*)")
    right_formulas = check_reg_indicateur_formulae.search(indicateur)
    if right_formulas is None:
        return definition_analyse_decortiquee
    indicateur = right_formulas.group()

    definition_analyse_decortiquee["indicateur"] = indicateur
    # Position du signe qui permet de distinguer la présence d'un éventuel coefficient
    pos_coeff_mult = definition_analyse.find("*")
    # Notons que nous partons de l'hypothèse que le signe '*' est toujours situé avant le coefficient.
    # À faire : faire en sorte que le calcul soit correct quelle que soit la manière dont on l'écrit.
    # Expression régulière afin de ne conserver que les chiffres (détection coefficient)
    # Recherche à partir de cette expression régulière
    coeff = ""
    if pos_coeff_mult > -1:
        exp_reg_coeff = re.compile("\*[0-9]+[,.]?[0-9]*")
        coeff_exp = exp_reg_coeff.search(definition_analyse[pos_coeff_mult:])
        if coeff_exp is not None:
            # Récupération du coefficient
            # Détection du caractère qui permet de distinguer
            # la présence de la donnée
            # par laquelle on divise (ratio)
            coeff = coeff_exp.group().replace(",", ".")
            definition_analyse_decortiquee["coeff_details"]["mul"] = float(coeff[1:])
    # Position du signe qui permet de distinguer la présence d'un éventuel ratio
    pos_coeff_div = definition_analyse.find("/")
    # Notons que nous partons de l'hypothèse que le signe '/' est toujours situé avant le coefficient.
    # Expression régulière afin de ne conserver que les chiffres (détection coefficient)
    # Recherche à partir de cette expression régulière
    if pos_coeff_div > -1:
        exp_reg_coeff = re.compile("\/[0-9]+[,.]?[0-9]*")
        coeff_division = exp_reg_coeff.search(definition_analyse[pos_coeff_div:])
        if coeff_division is not None:
            # Récupération du coefficient
            # Détection du caractère qui permet de distinguer
            # la présence de la donnée
            # par laquelle on divise (ratio)
            div_coeff = coeff_division.group().replace(",", ".")
            definition_analyse_decortiquee["coeff_details"]["div"] = float(
                div_coeff[1:]
            )
            coeff += div_coeff
    definition_analyse_decortiquee["coeff"] = coeff

    pos_ratio = definition_analyse.find("/maille.")
    # if we didn't find any ratio, we return as is
    if pos_ratio <= -1:
        return definition_analyse_decortiquee

    # otherwise, we handle the ratio
    pos_ratio += len("/maille.")
    # On sélectionne toutes les lettres, le signe _ mais aussi les chiffres (pop_1565 par exemple)
    exp_reg = re.compile("[a-z]{1,}[a-z_0-9]+")
    ratio_exp = exp_reg.search(
        definition_analyse[pos_ratio:]
    )  # Recherche (Cf. plus haut)
    # Récupération du ratio (exemple : population, employé, superficie, etc.)
    if ratio_exp is not None:
        ratio = ratio_exp.group()
        if ratio != "":
            ratio = "/maille." + ratio
            definition_analyse_decortiquee["ratio"] = ratio
    return definition_analyse_decortiquee


def guess_frequency_update(history):
    if len(history) < 2:
        return f"unknown (only {len(history)} update available)"
    parsed_history = list(
        map(lambda d: datetime.strptime(d["date_maj"], "%Y-%m-%d %H:%M:%S"), history)
    )
    parsed_history.sort(reverse=True)
    diffs = [(i - j).days for i, j in zip(parsed_history, parsed_history[1:])]
    return f"every {int(statistics.fmean(diffs))} day(s) in average (considering {len(parsed_history)} updates)"
