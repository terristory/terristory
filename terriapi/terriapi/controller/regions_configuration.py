﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Fonctions et requêtes SQL sur la gestion des configurations des régions
"""
import json
from collections import defaultdict

import slugify

from terriapi import controller, settings
from terriapi.controller import suivi_trajectoire


async def get_impacts_configurations(region):
    """
    Returns the regional strategy impacts configuration from the database.
    """
    region_condition = ""
    if region is not None:
        region_condition = "where region = $1"
    sql = f"""
        select *
        from strategie_territoire.impacts_configuration
        {region_condition}
        """

    if region is not None:
        res = await controller.fetch(sql, region)
    else:
        res = await controller.fetch(sql)

    impacts_configuration_by_region = {}
    for impact in res:
        if impact["region"] not in impacts_configuration_by_region:
            impacts_configuration_by_region[impact["region"]] = []
        impacts_configuration_by_region[impact["region"]].append(dict(impact))

    return impacts_configuration_by_region


async def get_sankeys_configuration(id=None):
    region_condition = ""
    if id is not None:
        region_condition = "where region = $1"

    sql = f"""
        select region, data_table, sankey_name,
        geographical_levels_enabled
        from meta.sankey_metadata
        {region_condition}
    """

    if id is not None:
        rset = await controller.fetch(sql, id)
    else:
        rset = await controller.fetch(sql)

    # we then aggregate all metadata by region
    meta_data_sankey_by_region = {}
    for meta_data_sankey in rset:
        meta_data_sankey = dict(meta_data_sankey)
        meta_data_sankey["geographical_levels_enabled"] = meta_data_sankey.get(
            "geographical_levels_enabled", ""
        ).split(",")
        if meta_data_sankey["region"] not in meta_data_sankey_by_region:
            meta_data_sankey_by_region[meta_data_sankey["region"]] = []
        meta_data_sankey_by_region[meta_data_sankey["region"]].append(meta_data_sankey)
    return meta_data_sankey_by_region


async def get_seo_configuration(region):
    """
    Returns the regional SEO configuration from the database.
    """
    region_condition = ""
    if region is not None:
        region_condition = "where region = $1"
    sql = f"""
        select *
        from meta.seo
        {region_condition}
        """

    if region is not None:
        res = await controller.fetch(sql, region)
    else:
        res = await controller.fetch(sql)

    seo_configuration_by_region = defaultdict(list)
    for meta_data in res:
        if meta_data["region"] not in seo_configuration_by_region:
            seo_configuration_by_region[meta_data["region"]] = []
        seo_configuration_by_region[meta_data["region"]].append(dict(meta_data))

    return seo_configuration_by_region


async def is_national_feature_enabled():
    """
    Check if the national feature is enabled.
    """
    res = await controller.fetch(
        """
                                    SELECT *
                                    FROM meta.national
                                """
    )
    return res[0]["enabled"]


async def get_configuration(id=None, with_private=False, allow_national_regions=False):
    """
    Return main regions configuration from database.
    """

    # Récupération des configurations depuis la base de données
    region_condition = " and id = $2" if id is not None else ""
    deployed_condition = (
        " AND (is_national = false OR id = 'france' OR id = 'national') "
        if not allow_national_regions
        else ""
    )
    private_data = "contact_email_response_to," if with_private else ""

    sql = f"""
        select  id,
                url,
                label,
                theme,
                valeur_defaut_territoire,
                titre_graph_ges,
                ges_export_excel,
                export_excel_results,
                export_excel_update_date,
                source_suivi_traj,
                cesba_notes,
                cesba_elements,
                lien_a_propos,
                lien_faq,
                map_init_center,
                map_init_zoom,
                map_min_zoom,
                map_extent,
                map_init_extent,
                footer::text,
                ui_show_login,
                ui_show_poi,
                ui_show_analyse,
                ui_show_cesba,
                ui_show_plan_actions,
                ui_show_simulators,
                enable_history_actions_plan,
                enable_past_inputs_for_strategy,
                enable_air_pollutants_impacts,
                enable_newsletter_national,
                enable_newsletter_regional,
                ui_show_tableau_bord,
                ui_show_sankey,
                allow_sharing_dashboard_by_url,
                splash_screen_custom,
                actu,
                contact_email,
                {private_data}
                contact_resp_rgpd,
                env,
                relative_pcaet,
                auto_select_maille,
                is_national,
                enabled
        from regions_configuration
        where env = $1
        {region_condition}
        {deployed_condition}
        ORDER BY ordre_affichage
    """

    env = settings.get("api", "env")
    if id is not None:
        rset = await controller.fetch(sql, env, id)
    else:
        rset = await controller.fetch(sql, env)
    main_data = [dict(x) for x in rset]

    meta_data_sankey_by_region = await get_sankeys_configuration(id)
    analyses_pages_enabled = await suivi_trajectoire.get_pages_enabled(env, id)
    impacts_configurations = await get_impacts_configurations(id)
    other_links = await get_other_links_for_navbar(id)
    seo_configurations = await get_seo_configuration(id)
    is_national_enabled = await is_national_feature_enabled()

    for i, reg in enumerate(main_data):
        reg["sankeys"] = meta_data_sankey_by_region.get(reg["id"], [])
        reg["analyses_pages"] = analyses_pages_enabled.get(reg["id"], [])
        reg["impacts"] = impacts_configurations.get(reg["id"], [])
        reg["other_links"] = other_links.get(reg["id"], [])
        reg["seo"] = seo_configurations.get(reg["id"], [])
        reg["is_national_enabled"] = is_national_enabled
        reg["ui_show_simulators"] = json.loads(reg.get("ui_show_simulators", "{}"))
    return main_data


async def get_other_links_for_navbar(region=None):
    region_condition = ""
    if region is not None:
        region_condition = "WHERE region_id = $1"
    sql = f"""
        SELECT region_id, category, label, url, zones_types, show_external,
        replace_epci_group_names, zones_excluded
        FROM meta.navbar_other_links
        {region_condition}
        ORDER BY links_order
    """
    if region is not None:
        rset = await controller.fetch(sql, region)
    else:
        rset = await controller.fetch(sql)

    # we then aggregate all metadata by region
    other_links_by_group = defaultdict(lambda: defaultdict(list))
    for link in rset:
        zones_excluded = (
            json.loads(link["zones_excluded"]) if link["zones_excluded"] else []
        )
        zones_excluded_by_type = defaultdict(list)
        for zone in zones_excluded:
            zones_excluded_by_type[zone["type"]].append(zone["id"])
        other_links_by_group[link["region_id"]][link["category"]].append(
            {
                **link,
                "slug": slugify.slugify(link["category"] + "-" + link["label"]),
                "zones_excluded": zones_excluded_by_type,
            }
        )
    return other_links_by_group


async def get_data_units(region):
    """
    Returns the list of units available in current region.
    """
    sql = f"""
        SELECT * FROM meta.data_units
        WHERE region = $1
        """
    res = await controller.fetch(sql, region)
    return [dict(r) for r in res]


async def get_poi_contributions_managers(region, layer):
    """
    Returns the list of users who are supposed to manage POIs contributions.
    """
    sql = f"""
        SELECT email, is_receiving_mails FROM meta.poi_contributions_contacts
        WHERE region = $1 AND (layer_name = $2 OR layer_name = '*')
        """
    res = await controller.fetch(sql, region, layer)
    return [dict(r) for r in res]


async def update_configuration(region, key, value):
    env = settings.get("api", "env")
    sql = f"""
        UPDATE regions_configuration SET {key}=$1 WHERE id=$2 and env=$3
        """
    await controller.execute(sql, value, region, env)


async def check_is_valid_region(schema_name):
    env = settings.get("api", "env")
    # récupération du dictionnaire.
    res = await controller.fetch(
        """SELECT EXISTS (
            SELECT FROM regions_configuration 
            WHERE id = $1 AND env = $2
        )""",
        schema_name,
        env,
    )
    return res[0]["exists"]


async def check_is_valid_region_not_national(schema_name):
    env = settings.get("api", "env")
    # récupération du dictionnaire.
    res = await controller.fetch(
        """SELECT EXISTS (
            SELECT FROM regions_configuration 
            WHERE id = $1 AND env = $2 AND is_national = false
        )""",
        schema_name,
        env,
    )
    return res[0]["exists"]


async def get_non_national_regions(env):
    sql = """SELECT DISTINCT id as nom from regions_configuration 
    WHERE env = $1 AND id <> 'france' AND is_national = false"""
    rset = await controller.fetch(sql, env)
    return [r["nom"] for r in rset]
