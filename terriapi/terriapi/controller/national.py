# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import asyncio
import json
from collections import defaultdict
from functools import reduce

import pandas as pd
from sanic.log import logger

from terriapi import controller
from terriapi.controller.strategy_actions import actions_dispatcher
from terriapi.utils import normalize_text


def replace_modalities_with_ids(dataFrame, df_categories):
    """
    Replaces modality names with their corresponding category IDs in the given DataFrame.
    Raises an error if any modality names cannot be mapped.

    Args:
        dataFrame : DataFrame containing modality name columns to be replaced.
        df_categories : DataFrame containing category and modality mappings.

    Returns:
        DataFrame with modality names replaced by category IDs.
    """
    # Create copies to avoid modifying original DataFrames
    df = dataFrame.copy()
    df_cat = df_categories.copy()

    # Normalize 'nom' and 'modalite' columns in df_categories
    df_cat["nom_normalized"] = df_cat["nom"].apply(normalize_text)
    df_cat["modalite_normalized"] = df_cat["modalite"].apply(normalize_text)

    # Identify categories to replace based on matching column names
    categories_to_replace = [
        col
        for col in df.columns
        if normalize_text(col) in df_cat["nom_normalized"].unique()
    ]

    for category in categories_to_replace:
        category_normalized = normalize_text(category)

        # Filter df_cat for the current category
        df_cat_filtered = df_cat[df_cat["nom_normalized"] == category_normalized][
            ["modalite_normalized", "modalite_id"]
        ]

        if df_cat_filtered.empty:
            raise ValueError(
                f"No mapping found for category '{category}' in df_categories."
            )

        # Normalize the corresponding column in df
        df[category + "_normalized"] = df[category].apply(normalize_text)

        # Merge to map modality names to IDs
        df = df.merge(
            df_cat_filtered,
            left_on=category + "_normalized",
            right_on="modalite_normalized",
            how="left",
        )

        # Check for unmapped modalities
        unmapped = df[df["modalite_id"].isna()][category].unique()
        if len(unmapped) > 0:
            raise ValueError(
                f"Unmapped modality values found in category '{category}': {unmapped}"
            )

        # Replace the original category column with modality_id
        df[category] = df["modalite_id"].astype(int)

        # Drop auxiliary columns
        df = df.drop(
            columns=[category + "_normalized", "modalite_normalized", "modalite_id"]
        )

    return df


def replace_ids_with_modalities(dataFrame, df_categories):
    """
    Replaces category IDs with their corresponding modality names in the given DataFrame.

    Args:
        dataFrame : DataFrame containing category ID columns to be replaced.
        df_categories : DataFrame containing category and modality mappings.

    Returns:
        DataFrame with category IDs replaced by modality names.

    Raises:
        ValueError: If any NaN values are found after replacement.
    """
    # Get unique categories present in df_categories
    categories = [
        col for col in dataFrame.columns if col in df_categories["nom"].values
    ]

    # Iterate over each category to replace IDs with modalities
    for category in categories:
        # Filter df_categories for the current category and rename columns
        df_categories_filtered = df_categories[df_categories["nom"] == category].rename(
            columns={"modalite_id": category, "modalite": f"{category}_name"}
        )

        # Merge dataFrame with df_categories_filtered to replace category ID with modality name
        dataFrame = pd.merge(
            dataFrame,
            df_categories_filtered[[category, f"{category}_name"]],
            on=category,
            how="left",
        )

        # Drop the original category ID column and rename category_name to category
        dataFrame.drop(columns=[category], inplace=True)
        dataFrame.rename(columns={f"{category}_name": category}, inplace=True)

        # Check for NaN values after the merge
        if dataFrame[category].isnull().any():
            raise ValueError(
                f"NaN values found in category '{category}' after replacement."
            )

    return dataFrame


async def convert_modality_to_id(region, dataFrame, export=False) -> pd.DataFrame:
    """
    Converts modality names to IDs or vice versa in the given DataFrame based on the export flag.

    Args:
        dataFrame : DataFrame containing category IDs or modality names to be converted.
        export (bool): If True, convert IDs to modality names. If False, convert modality names to IDs.

    Returns:
        DataFrame with converted category IDs or modality names.
    """
    query_category = """
        select * from meta.categorie WHERE region=$1
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            # Select all categories
            categories = await conn.fetch(query_category, region)
            df_categories = pd.DataFrame([dict(x) for x in categories])

            if export:
                data = replace_ids_with_modalities(dataFrame, df_categories)
                return data
            else:
                data = replace_modalities_with_ids(dataFrame, df_categories)
                return data
    return pd.DataFrame()


async def get_required_categories_name(region, column_names):
    """
    Fetches required categories based on provided column names.

    Args:
        column_names: List of column names to filter categories.

    Returns:
        dictionary where keys are category names and values are lists of modalities.
        Returns an empty dictionary if no matching categories are found or if an error occurs.
    """
    query_category = """
        SELECT * FROM meta.categorie WHERE region=$1
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            try:
                # Fetch all categories
                categories = await conn.fetch(query_category, region)

                # Convert the fetched categories to a DataFrame
                df_categories = pd.DataFrame([dict(x) for x in categories])

                # Filter category names from column_names
                category_columns = [
                    col for col in column_names if col in df_categories["nom"].values
                ]

                # If there are matching categories, return the grouped categories
                if category_columns:
                    # Filter the DataFrame
                    filtered_categories = df_categories[
                        df_categories["nom"].isin(category_columns)
                    ]

                    # Group by 'nom'
                    grouped_categories = defaultdict(list)
                    for _, row in filtered_categories.iterrows():
                        row_dict = row.to_dict()
                        nom = row_dict.pop("nom")
                        grouped_categories[nom].append(row_dict)

                    grouped_categories = dict(grouped_categories)
                    return grouped_categories

                # If no matching categories, return an empty dictionary
                return {}

            except Exception as e:
                # Log the error
                logger.error(f"An error occurred while fetching categories: {e}")
                return {}


async def update_metadata_simulator(data, region):
    """
    Update metadata for the simulator based on provided data and region.

    This function updates metadata related to the simulator in the database.
    It activates various impacts and lever categories based on the provided data
    and ensures that the region and corresponding level are inserted if they do not exist.

    Parameters:
        data (dict): A dictionary containing metadata information.
            It should include values for the following keys:
            - 'conso_energetique': Energy consumption data
            - 'emission_ges': Greenhouse gas emission data
            - 'mobilite_insee': Mobility data
            - 'parc_auto_statique': Static car park data
            - 'facture_energetique' (optional): Energy invoice data
        region (str): The region for which metadata is being updated.

    Returns:
        bool: A boolean indicating whether the metadata update was successful.
        Returns True if successful, False otherwise.
    """
    # Define SQL queries
    query_level = """
        INSERT INTO simulator.simulator_level (region, thematic, level)
        SELECT
            $1::text AS region,
            $3::text AS thematic,
            CASE
                WHEN (SELECT COUNT(DISTINCT match)
                    FROM strategie_territoire.passage_table
                    WHERE Key IN ('DataSet.CONSUMPTION/Usage.PASSENGER_TRANSPORT', 'DataSet.CONSUMPTION/Usage.GOODS_TRANSPORT')
                    AND region = $1::text) = 2
                    AND 
                    (SELECT COUNT(DISTINCT match)
                    FROM strategie_territoire.passage_table
                    WHERE Key IN ('DataSet.EMISSIONS/Usage.PASSENGER_TRANSPORT', 'DataSet.EMISSIONS/Usage.GOODS_TRANSPORT')
                    AND region = $1::text) = 2
                    AND 
                    (SELECT CASE 
                                WHEN $2 THEN 
                                    (SELECT COUNT(DISTINCT match)
                                    FROM strategie_territoire.passage_table
                                    WHERE Key IN ('DataSet.ENERGY_BILL/Usage.PASSENGER_TRANSPORT', 'DataSet.ENERGY_BILL/Usage.GOODS_TRANSPORT')
                                    AND region = $1::text)
                                ELSE 2
                            END
                    ) = 2 THEN 1
                ELSE 0
            END
        ON CONFLICT (region, level, thematic)
        DO UPDATE SET level = EXCLUDED.level
        WHERE simulator.simulator_level.level != EXCLUDED.level
        RETURNING *;
    """

    query_impact = """
        WITH region_data AS (
            SELECT unnest(regions) AS region
            FROM simulator.simulator_impact
            WHERE type = $1
            AND thematic = $2
        )
        UPDATE simulator.simulator_impact
        SET regions = array(
            SELECT DISTINCT region
            FROM region_data
            UNION
            SELECT $3 
        )
        WHERE type = $1
        AND NOT EXISTS (
            SELECT 1
            FROM region_data
            WHERE region = $3
        );
    """

    query_lever_category = """
        WITH region_data AS (
            SELECT unnest(regions) AS region, id
            FROM simulator.category_lever
            WHERE thematic = $1
        )
        UPDATE simulator.category_lever
        SET regions = array(
            SELECT DISTINCT region
            FROM region_data
            UNION ALL
            SELECT $2
        )
        WHERE NOT EXISTS (
            SELECT 1
            FROM region_data
            WHERE region = $2
        )
        RETURNING id;
    """

    query_basic_lever = """
        WITH region_data AS (
            SELECT unnest(regions) AS region
            FROM simulator.lever
        )
        UPDATE simulator.lever
        SET regions = array(
            SELECT DISTINCT region
            FROM region_data
            UNION ALL
            SELECT $1
        )
        WHERE key <> ALL($2) 
        AND NOT EXISTS (
            SELECT 1
            FROM region_data
            WHERE region = $1
        )
        AND category_lever_id = ANY($3);
    """

    # Define required keys for mobility
    required_keys_mobility = {
        "conso_energetique",
        "emission_ges",
        "mobilite_insee",
        "parc_auto_statique",
    }

    extra_lever_keys = (
        "deplacements_personnels_hors_travail",
        "transport_de_marchandises",
        "poids_lourds",
    )

    # Check if all required keys for mobility have values
    mobility_simulator_enabled = all(
        key in data and data[key] for key in required_keys_mobility
    )

    # Check for energy bill data presence
    energy_bill_exists = bool(data.get("facture_energetique"))

    # TODO: Check if all required keys for EnR have values
    enr_simulator_enabled = False

    # Default to False unless we confirm success
    mobility_status = False
    enr_status = False

    # If all required keys for mobility are present, proceed with database operations
    if mobility_simulator_enabled:
        try:
            async with controller.db.acquire() as conn:
                async with conn.transaction():
                    # INSERT Or UPDATE Level 0 or 1 (mobility)
                    await conn.fetch(
                        query_level, region, energy_bill_exists, "mobility"
                    )

                    # Activate impacts related to mobility
                    await conn.fetch(query_impact, "impact-ener", "mobility", region)
                    await conn.fetch(query_impact, "impact-carbone", "mobility", region)

                    # Activate energy-bill impact if data exists
                    if energy_bill_exists:
                        await conn.fetch(
                            query_impact, "impact-facture-ener", "mobility", region
                        )

                    # Update lever category and retrieve updated category_lever_ids
                    updated_category_levers = await conn.fetch(
                        query_lever_category, "mobility", region
                    )

                    # Extract category_lever_ids
                    category_lever_ids = [
                        record["id"] for record in updated_category_levers
                    ]

                    # Perform basic lever update
                    await conn.fetch(
                        query_basic_lever, region, extra_lever_keys, category_lever_ids
                    )

                    mobility_status = True

        except Exception as e:
            raise Exception(f"Error during mobility update: {e}")

    # For EnR
    if enr_simulator_enabled:
        try:
            async with controller.db.acquire() as conn:
                async with conn.transaction():
                    # TODO: Add logic to process simulator EnR queries and updates
                    print(f"Updates EnR")
        except Exception as e:
            raise Exception(f"Error during energy update: {e}")

    # Return both results: Mobility and EnR
    return {
        "mobility_simulator_status": mobility_status,
        "enr_simulator_status": enr_status,
    }


async def check_data_validity_simulator(region, data):
    # Define the SQL queries
    query_level = """
        SELECT EXISTS (
            SELECT 1
            FROM simulator.simulator_level
            WHERE region = $1::text
            AND thematic = $2::text
        );
    """

    query_impact = """
        WITH region_data AS (
            SELECT thematic, unnest(regions) AS region
            FROM simulator.simulator_impact
            WHERE type = $1
            AND thematic = $3
        )
        SELECT EXISTS (
            SELECT 1
            FROM region_data
            WHERE region = $2::text
            AND thematic = $3
        );
    """

    query_lever_category = """
        WITH region_data AS (
            SELECT id, thematic, unnest(regions) AS region
            FROM simulator.category_lever
        )
        SELECT id
        FROM region_data
        WHERE region = $1::text
        AND thematic = $2::text;
    """

    query_basic_lever = """
        WITH region_data AS (
            SELECT unnest(regions) AS region, category_lever_id
            FROM simulator.lever
        )
        SELECT EXISTS (
            SELECT 1
            FROM region_data
            WHERE region = $1::text
            AND category_lever_id = ANY($2::int[])
        );
    """

    # Define required keys for mobility
    required_keys_mobility = {
        "conso_energetique",
        "emission_ges",
        "mobilite_insee",
        "parc_auto_statique",
    }

    # TODO : Define required keys for EnR (Renewable Energy) ?
    # required_keys_enr = {
    # }

    # Check if all required keys for mobility have values
    enable_simulator_mobility = all(
        key in data and data[key] for key in required_keys_mobility
    )

    # Check if all required keys for EnR have values
    # enable_simulator_enr = all(key in data and data[key] for key in required_keys_enr)
    enable_simulator_enr = False

    # If all required keys for mobility have values, proceed with database operations
    if enable_simulator_mobility:
        async with controller.db.acquire() as conn:
            async with conn.transaction():
                # Check if level exists for this region
                level_result = await conn.fetch(query_level, region, "mobility")

                # Check if impacts are activated
                impact_ener_result = await conn.fetch(
                    query_impact, "impact-ener", region, "mobility"
                )
                impact_carbone_result = await conn.fetch(
                    query_impact, "impact-carbone", region, "mobility"
                )

                # Check if category levers are activated
                lever_category_result = await conn.fetch(
                    query_lever_category, region, "mobility"
                )
                category_lever_ids = [row["id"] for row in lever_category_result]

                # Check if levers are activated, using category_lever_id if present
                if category_lever_ids:
                    basic_lever_result = await conn.fetch(
                        query_basic_lever, region, category_lever_ids
                    )
                else:
                    basic_lever_result = [
                        {"exists": False}
                    ]  # Default to False if no lever categories

                # Set enable_simulator_mobility to True if all checks pass
                enable_simulator_mobility = (
                    level_result[0]["exists"]
                    and impact_ener_result[0]["exists"]
                    and impact_carbone_result[0]["exists"]
                    and lever_category_result
                    and basic_lever_result[0]["exists"]
                )

    # If all required keys for EnR have values, proceed with EnR specific checks
    if enable_simulator_enr:
        async with controller.db.acquire() as conn:
            async with conn.transaction():
                # Check if level exists for EnR for this region
                enr_level_result = await conn.fetch(query_level, region, "EnR")
                # Check if impacts are activated for EnR
                enr_impact_result = await conn.fetch(
                    query_impact, "impact-ener", region, "EnR"
                )
                enr_carbone_result = await conn.fetch(
                    query_impact, "impact-carbone", region, "EnR"
                )
                # Check if category levers for EnR are activated
                enr_lever_category_result = await conn.fetch(
                    query_lever_category, region, "EnR"
                )
                enr_category_lever_ids = [
                    row["category_lever_id"] for row in enr_lever_category_result
                ]

                # Check if levers for EnR are activated
                if enr_category_lever_ids:
                    enr_basic_lever_result = await conn.fetch(
                        query_basic_lever, region, enr_category_lever_ids
                    )
                else:
                    enr_basic_lever_result = [{"exists": False}]

                # Set enable_simulator_enr to True if all EnR checks pass
                enable_simulator_enr = (
                    enr_level_result[0]["exists"]
                    and enr_impact_result[0]["exists"]
                    and enr_carbone_result[0]["exists"]
                    and enr_lever_category_result
                    and enr_basic_lever_result[0]["exists"]
                )

    # Return the result for both mobility and Renewable Energy
    return {
        "mobility_simulator_enabled": enable_simulator_mobility,
        "enr_simulator_enabled": enable_simulator_enr,
    }


async def check_and_update_actions_status(region, data):
    """
    Checks the status of actions based on provided data and updates the action status in the database.

    Args:
        region (str): The region to check.
        data (dict): The data containing keys for various parameters.

    Returns:
        bool: True if territorial strategy can be enabled, False otherwise.
    """

    if not await check_essential_data(region):
        return False
    missing_actions = await find_missing_actions(region, data)
    update_query = """
        UPDATE strategie_territoire.action_status
        SET enabled = CASE
            WHEN action.action_number = ANY($2) THEN false
            ELSE true
        END
        FROM strategie_territoire."action"
        WHERE action_status.action_number = action.action_number
        AND action_status.region = $1;
        """
    result = await controller.execute(update_query, region, missing_actions)

    return int(result.split()[1]) > 0


async def check_essential_data(region):
    """
    Check if essential data is present for the given region.

    Args:
        region (str): The region to check.

    Returns:
        bool: True if all essential data is present, False otherwise.
    """
    essential_queries = {
        "global_factor": "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_global_facteur_emission WHERE region = $1)",
        "user_action_params": "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_user_action_params WHERE region = $1)",
        "global_energy_prices": "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_user_global_prix_energie WHERE region = $1)",
        "IFER": "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_ifer WHERE region = $1)",
    }

    for name, query in essential_queries.items():
        result = await controller.execute(query, region)
        if not result[0]:  # result[0] is True if data exists
            print(f"Essential data missing: {name}")
            return False
    return True


async def find_missing_actions(region, data_tables):
    """
    Identify actions that are missing required data or parameters.

    Args:
        region (str): The region to check.
        data_tables (dict): The data containing keys for various parameters.

    Returns:
        list: A list of actions that are missing required data.
    """
    missing_actions = ["7"]  # Start with predefined missing actions
    dispatcher = actions_dispatcher.ActionsDispatcher(None, None, None)

    enabled_tables = check_required_keys(data_tables)

    for key, action in dispatcher.actions.items():
        if has_missing_requisites(action, enabled_tables["consumption"]):
            missing_actions.append(str(action.action_number))

    if not enabled_tables["mobility"]:
        missing_actions.extend(["18", "19"])

    if not enabled_tables["transport"]:
        missing_actions.append("20")

    # Call the async function with await
    specific_missing_actions = await find_specific_missing_actions(region)
    missing_actions.extend(specific_missing_actions)

    return missing_actions


def check_required_keys(data_tables):
    """
    Determine if the necessary data for different action categories is present.

    Args:
        data_tables (dict): The data containing keys for various parameters.

    Returns:
        dict: A dictionary with boolean parameters indicating if required data is present.
    """
    required_keys = {
        "consumption": {"conso_energetique", "emission_ges", "prod_enr"},
        "mobility": {"mobilite_insee"},
        "transport": {"parc_auto_statique"},
    }

    return {
        "consumption": all(
            data_tables.get(key) for key in required_keys["consumption"]
        ),
        "mobility": all(data_tables.get(key) for key in required_keys["mobility"]),
        "transport": all(data_tables.get(key) for key in required_keys["transport"]),
    }


def has_missing_requisites(action, enable_consumption):
    """
    Check if an action involves DataSet.CONSUMPTION or DataSet.EMISSIONS and if enable_consumption is False.

    Args:
        action: The action to check.
        enable_consumption (bool): Flag indicating if consumption data is enabled.

    Returns:
        bool: True if the action involves DataSet.CONSUMPTION or DataSet.EMISSIONS and enable_consumption is False.
              False otherwise.
    """
    for req in action.requisites:
        if str(req.r_type) == "Données":
            if (
                "DataSet.CONSUMPTION" in str(req) or "DataSet.EMISSIONS" in str(req)
            ) and not enable_consumption:
                return True
    return False


async def find_specific_missing_actions(region):
    """
    Identify specific actions that are missing required data in the database.

    Args:
        region (str): The region to check.

    Returns:
        list: A list of specific action numbers that are missing data.
    """
    action_queries = {
        "1": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action1_conso_max_etiquette WHERE region = $1)",
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action1_taux_renovation_etiquette WHERE region = $1)",
        ],
        "2": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action2_consommation_moyenne_unitaire WHERE region = $1)"
        ],
        "10": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action10_parametres_avances WHERE region = $1)",
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action10_tarif_cre WHERE region = $1)",
        ],
        "3b": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action3b_parametres_avances WHERE region = $1)",
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action3b_tarif_cre WHERE region = $1)",
        ],
        "7": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action7_indicateurs_carbon_energie WHERE region = $1)",
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action7_lambdas WHERE region = $1)",
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action7_parc_voiture_com_men WHERE region = $1)",
        ],
        "12": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_action12_parametres_avances WHERE region = $1)"
        ],
        "14": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.fixed_capital WHERE category = 'industry' AND region = $1)"
        ],
        "15": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.fixed_capital WHERE category = 'agriculture' AND region = $1)"
        ],
        "16": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_user_action_params_years WHERE action='16' AND region = $1)"
        ],
        "17": [
            "SELECT EXISTS (SELECT 1 FROM strategie_territoire.params_user_action_params_years WHERE action='17' AND region = $1)"
        ],
    }

    missing_actions = []

    for action_number, queries in action_queries.items():
        # Collect results asynchronously
        results = await asyncio.gather(
            *[controller.execute(query, region) for query in queries]
        )

        # Check if all queries returned True (meaning data exists)
        if not all(result[0] for result in results):  # result[0] is True if data exists
            missing_actions.append(str(action_number))

    return missing_actions


def construct_update_query(missing_actions):
    """
    Construct the SQL query to update action statuses based on missing actions.

    Args:
        missing_actions (list): A list of action numbers that should be disabled.

    Returns:
        str: The SQL query string.
    """
    if missing_actions:
        return (
            f"""
        UPDATE strategie_territoire.action_status
        SET enabled = CASE
            WHEN action.action_number IN $2 THEN false
            ELSE true
        END
        FROM strategie_territoire."action"
        WHERE action_status.action_number = action.action_number
        AND action_status.region = $1;
        """,
            missing_actions,
        )
    else:
        return """
        UPDATE strategie_territoire.action_status
        SET enabled = true
        FROM strategie_territoire."action"
        WHERE action_status.action_number = action.action_number
        AND action_status.region = $1;
        """


async def fill_in_uploaded_data(region, table_name, data, datatypes):
    missing_keys = set(datatypes.keys()) - set(data.columns)
    if len(missing_keys) == 0:
        return data

    result = await controller.fetch(
        f"""SELECT * FROM strategie_territoire.national_hypotheses
        WHERE region = $1 AND table_name = $2 AND column_name = ANY($3)""",
        region,
        table_name,
        missing_keys,
    )

    for key in missing_keys:
        data[key] = "Autres usages"

    # no operation available
    if len(result) == 0:
        return data

    # binary join operation
    concat_and = lambda x, y: x & y
    new_rows = []
    filters_to_delete = []
    for operation in result:
        # filter according to db elements
        conditions = [data[k] == v for k, v in json.loads(operation["filters"]).items()]
        filters = reduce(concat_and, conditions)

        # compute the new row based on the value
        _row = data.loc[filters].copy()
        _row.loc[:, operation["column_name"]] = operation["key"]
        _row.loc[:, "valeur"] *= float(operation["value"])

        # we save these elements to perform the operations at the end to avoid
        # interfering with other operations
        filters_to_delete.append(filters)
        new_rows.append(_row)

    # delete previous rows
    for filters in filters_to_delete:
        data = data.loc[~filters]

    # insert new rows
    data = pd.concat([data] + new_rows)

    return data


async def get_metadata(region, table_key=None):
    if table_key is not None:
        result = await controller.fetch(
            f"""SELECT table_name, description, template_file, categories,
            requirements, optional
            FROM meta.national_import_metadata
            WHERE region = $1 and table_key = $2
            ORDER BY table_order""",
            region,
            table_key,
        )
        if len(result) == 0:
            raise ValueError("Couldn't find table metadata for " + table_key)
        return dict(result[0])
    else:
        result = await controller.fetch(
            f"""SELECT table_key, table_name, description, template_file, categories,
            requirements, optional
            FROM meta.national_import_metadata
            WHERE region = $1
            ORDER BY table_order""",
            region,
        )
        return list(map(dict, result))
