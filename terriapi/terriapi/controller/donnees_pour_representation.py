# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module pour les classes dont le rôle est de formatter la donnée pour
les visualiser selon une représentation spécifique
"""

import json
from abc import ABC, abstractmethod

import asyncpg
import numpy as np
import pandas as pd
from sanic.log import logger
from sklearn.linear_model import LinearRegression

from terriapi.controller import QueryBuilder, fetch
from terriapi.controller.indicateur_tableau_bord import IndicateurTableauBord
from terriapi.controller.stations_mesures import (
    get_stations_mesures,
    get_value_par_station,
)
from terriapi.controller.suivi_trajectoire import (
    _graphe_format,
    _rendre_donnees_confidentielles,
    _zero_annee_manquant_a_gauche,
)


async def get_confidential_values(
    region: str,
    zone: str,
    zone_id: str,
    indicator: int,
    category: str,
    all_categories: list,
) -> list[str]:
    schema = region.replace("-", "_")
    table = "confid_maille"
    sql = QueryBuilder(table, schema)
    sql.add_select("annee", table=table)
    sql.add_select(f"conditions->>'{category}' as {category}", table=table)
    sql.add_join(zone, on="{}.code = a.code")
    sql.add_select(table=zone, column="code")
    sql.add_filter(zone, "code", zone_id)
    sql.add_filter(table, "type_analyse", indicator)
    # as the data displayed are only distributed along one single category
    # we filter on other categories to select confidentiality rules that are not
    # filtered along these other categories (ie., value should be "*")
    for cat in all_categories:
        if cat == category:
            continue
        sql.add_filter(table, f"conditions->>'{cat}'", "*")
    rset = await fetch(sql.query, *sql.params)
    if not rset:
        return []
    df = pd.DataFrame([dict(x) for x in rset]).drop_duplicates()
    return df[category].unique().tolist()


class Representations(ABC):
    """Obtention des données formattées en fonction de la manière dont on souhaite les représenter.
    On peut représenter les données :
    - De manière cartographique ;
    - Sous formes de graphiques restitués avec react-chart-js

    Cette classe contient une méthode abstraite obtenir_donnees redéfinies dans les classes filles et des méthodes
    dont on ne se sert que dans la méthode finale (considérée comme privée).

    Parmi les méthodes annexes, certaines permettent de générer les bouts de requêtes grâce auxquels
    on gère la confidentialité ou encore les filtres.

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    specific_year : integer, optional
        Specific year to retrieve, if not specified will take max of available years, default None.
    specific_unit_params : dict, optional
        Specific unit to use, default {}.


    """

    def __init__(
        self,
        caracteristiques_indicateur,
        maille,
        type_territoire,
        code_territoire,
        specific_year=None,
        specific_unit_params={},
    ):
        self.identifiant_indicateur = caracteristiques_indicateur["id"]
        self.filtre_initial_categorie = caracteristiques_indicateur[
            "filtre_initial_modalites_par_categorie"
        ]
        self.caracteristiques_indicateur = {
            "disabled_for_macro_level": "",
            "disabled_for_zone": "",
            "only_for_zone": "",
            "moyenne_ponderee": False,
            "charts": [],
            "data_deuxieme_representation": None,
            "data_type": "",
            "data": "",
            "data_ratio": False,
            "afficher_calcul_et_donnees_table": False,
            "data_deuxieme_representation": None,
            "concatenation": False,
            "afficher_proportion": False,
            **caracteristiques_indicateur,
        }

        self.region = caracteristiques_indicateur["region"]
        self.schema = caracteristiques_indicateur["schema_region"]
        self.jeu_de_donnees = caracteristiques_indicateur["definition_indicateur"][
            "indicateur"
        ]
        self.confid_layer = caracteristiques_indicateur.get("confidentiel", None)
        self.ratio = caracteristiques_indicateur["definition_indicateur"]["ratio"]
        self.coeff = caracteristiques_indicateur["definition_indicateur"]["coeff"]
        self.coeff_details = caracteristiques_indicateur["definition_indicateur"].get(
            "coeff_details", {}
        )
        if specific_year is not None:
            self.annee = int(specific_year)
        else:
            self.annee = (
                max(caracteristiques_indicateur["years"])
                if caracteristiques_indicateur["years"]
                else None
            )
        self.unit_params = specific_unit_params
        self.maille = maille
        self.type_territoire = type_territoire
        self.code_territoire = code_territoire

        self.ratio_best_year = False

    @property
    def afficher_indicateur_maille(self):
        if self.caracteristiques_indicateur["disabled_for_macro_level"]:
            if "," in self.caracteristiques_indicateur["disabled_for_macro_level"]:
                territoire = (
                    self.caracteristiques_indicateur["disabled_for_macro_level"]
                ).split(",")
                for i in territoire:
                    if self.type_territoire == i:
                        return False
            elif (
                self.type_territoire
                == self.caracteristiques_indicateur["disabled_for_macro_level"]
            ):
                return False
        if self.caracteristiques_indicateur["disabled_for_zone"]:
            if "," in self.caracteristiques_indicateur["disabled_for_zone"]:
                territoire = (
                    self.caracteristiques_indicateur["disabled_for_zone"]
                ).split(",")
                for i in territoire:
                    if self.maille == i:
                        return False
            elif self.maille == self.caracteristiques_indicateur["disabled_for_zone"]:
                return False
        if self.caracteristiques_indicateur["only_for_zone"]:
            if "," in self.caracteristiques_indicateur["only_for_zone"]:
                territoire = (self.caracteristiques_indicateur["only_for_zone"]).split(
                    ","
                )
                for i in territoire:
                    if self.maille == i:
                        return True
                return False
            elif self.maille != self.caracteristiques_indicateur["only_for_zone"]:
                return False
        return True

    @property
    def unit_factor(self):
        val = self.unit_params.get("conversion_factor", 1.0)
        return val if isinstance(val, (float, int)) else 1.0

    def _hide_confidential_values(self, result, provenance):
        # Si les données sont confidentielles, on remplacer la valeur par le mot clé : "confidentielle"
        if (
            "confidentiel" in result.columns
            and provenance != "integration_donnees_territorialsynthesis"
        ):
            result.loc[result["confidentiel"] == True, "val"] = "confidentielle"
            result = result.where(result.notnull(), None)
        return result.fillna(0.0).to_dict("records")

    def _apply_territorial_confid(self, data, confid, merge_on_categories=False):
        """On résoud l'ensemble des territoires confidentiels

        Paramètres
        ----------
        data : list
            Indicateurs pour un ensemble de territoires
        confid : list
            Territoires confidentiels

        Returns
        --------
        Liste des territoires confidentiels (nom, code, val et centroïde en longitude, latitude)
        """
        if not confid:
            return data
        conf = pd.DataFrame(confid)
        conf["confidentiel"] = True
        df = pd.DataFrame(data)
        if "x" in list(conf) or "y" in list(conf):
            conf = conf.drop(columns=["x", "y"])
        if merge_on_categories:
            df_conditions = conf["conditions"].apply(pd.Series)
            conf = pd.concat(
                [conf.drop(["conditions"], axis=1), df_conditions],
                axis=1,
            )
            join_columns = list(df_conditions.columns)
            join_columns.append("code")

            if (
                self.caracteristiques_indicateur["filter"]
                and "." in self.caracteristiques_indicateur["filter"]
            ):
                additional_category, custom_value = self.caracteristiques_indicateur[
                    "filter"
                ].split(".", 1)
                # just checking we are not adding duplicated categories
                if additional_category not in df.columns:
                    df[additional_category] = custom_value

            result = pd.merge(df, conf, on=join_columns, how="outer")
        else:
            result = pd.merge(df, conf, on="code", how="outer")
        result["confidentiel"] = result["confidentiel"].fillna(False)
        # On supprime les valeurs NaN lorsqu'elles sont présentes dans les colonnes '[nom', 'x', 'y']
        result = result.dropna(subset=["nom", "x", "y"])

        result = result.to_dict("records")
        # on veut changer tous les NaN en None.
        # NaN n'est pas sérialisable en JSON
        for territoire in result:
            territoire["val"] = (
                None if pd.isna(territoire["val"]) else territoire["val"]
            )
            territoire["nom"] = (
                None if pd.isna(territoire["val"]) else territoire["nom"]
            )
        return result

    def _apply_diagram_confid(self, data, confid):
        """On résoud l'ensemble des territoires confidentiels

        Paramètres
        ----------
        data : list
            Indicateurs pour un ensemble de territoires
        confid : list
            Territoires confidentiels

        Returns
        --------
        Liste des territoires confidentiels (nom, code, val et centroïde en longitude, latitude)
        """
        if not confid or "charts" not in confid:
            return data

        if confid["charts"] == "B":
            for i, chart in enumerate(data):
                if chart["name"] == "energie" or chart["name"] == "usage":
                    chart["data"] = ["confidentiel"] * len(chart["data"])
                    data[i] = chart
        elif confid["charts"] == "C":
            for i, chart in enumerate(data):
                if chart["name"] == "secteur" or chart["name"] == "usage":
                    chart["data"] = ["confidentiel"] * len(chart["data"])
                    data[i] = chart
        elif confid["charts"] == "D":
            for i, chart in enumerate(data):
                chart["data"] = ["confidentiel"] * len(chart["data"])
                data[i] = chart

        return data

    async def _modalites_disponibles_pour_territoire_et_categorie(self, categorie):
        """Retourne la liste des modalités disponibles pour un territoire donné et une catégorie

        Par exemple, pour la catégorie energie, la modalité « chauffage et froid urbain » n'est pas
        disponible pour le territoire CC de l'Oisans (EPCI). Cette méthode permet d'obtenir la liste
        des seules modalités dispobles pour le territoire en attribut de la méthode et la catégorie
        en argument de la méthode.

        Paramètres
        ----------
        categorie : chaine de caractères
            Nom de la catégorie

        Returns
        --------
        Liste
            Liste des modalités disponibles pour le territoire sélectionné et la catégorie.
        """
        condition_annee = ""
        categorie_pond = categorie
        if self.annee:
            condition_annee = "where annee = $4"
        if self.caracteristiques_indicateur["moyenne_ponderee"]:
            categorie_pond = "modalite_id"
        req = f"""
              select distinct cat.modalite, cat.modalite_id, cat.couleur, cat.ordre
              from meta.categorie cat
              join {self.schema}.{self.jeu_de_donnees} a on a.{categorie_pond} = cat.modalite_id
              join {self.schema}.territoire t on t.commune = a.commune
              {condition_annee}
              and t.code = $1
              and cat.nom = $2
              and cat.region = $3
              ORDER BY cat.ordre
              """
        if self.annee:
            res = await fetch(
                req, self.code_territoire, categorie, self.region, self.annee
            )
        else:
            res = await fetch(req, self.code_territoire, categorie, self.region)
        return [dict(x) for x in res]

    async def _supprimer_modalites_filtrees_inutiles(self, filtres_categorie):
        """Supprime du filtre par catégorie initial, les modalités non disponibles au sein d'un territoire donné

        Paramètre
        ---------
        filtres_categorie : dictionnaire
            Filtre appliqué sur chaque catégorie. Les clé sont les catégories accompagnées de l'identifiant
            de l'indicateur et les valeurs des listes qui peuvent contenir plusieurs modalités.
            Exemple : {
                        "1energie" : [{"filtre_categorie" : 'Gaz'}, {"filtre_categorie" : 'Électricité'}],
                        "1usage" : [...],
                        .
                        .
                        .
                    }


        Returns
        --------
        Dictionnaire
            Filtre par catégorie débarassé des modalités non disponibles pour le territoire sélectionné
        """
        f_final = {}
        for categorie in filtres_categorie:
            liste_modalites = (
                await self._modalites_disponibles_pour_territoire_et_categorie(
                    categorie
                )
            )
            liste_modalites = [x["modalite"] for x in liste_modalites]
            f_final[categorie] = []

            for elem in filtres_categorie[categorie]:
                if elem["filtre_categorie"].split(".", 1)[1] in liste_modalites:
                    f_final[categorie].append(elem)
        return f_final

    async def get_columns_concerned_by_confid(self):
        # we get real column names
        # TODO: change this to have a general tool not only available for sector and energy categories
        params = [self.confid_layer, self.type_territoire, self.maille]
        requete_annee = ""
        if self.annee:
            requete_annee = "and a.annee = $4"
            params.append(self.annee)
        req = f"""
              SELECT DISTINCT jsonb_object_keys(conditions) as col
              FROM {self.schema}.confid_maille AS a
              WHERE a.type_analyse = $1
              AND (a.type_territoire = $2
              OR a.type_territoire = $3)
              {requete_annee};
              """
        rset = await fetch(req, *params)

        return [x["col"] for x in rset]

    async def _construction_conditions_confid(self, filtres_categorie={}):
        """Construit la clause WHERE de la requête sur la table confid_maille afin d'obtenir les données de confidentialité

        Paramètres
        ----------
        filtres_categorie : dictionnaire
        Filtre appliqué sur chaque catégorie. Les clé sont les catégories accompagnées de l'identifiant
            de l'indicateur et les valeurs des listes qui peuvent contenir plusieurs modalités.
            Exemple : {
                        "1energie" : [{"filtre_categorie" : 'Gaz'}, {"filtre_categorie" : 'Électricité'}],
                        "1usage" : [...],
                        .
                        .
                        .
                    }
        Returns
        --------
        Chaine de caractères
             Clause WHERE d'une requête qui permet d'obtenir les données de confidentialité sur un territoire

        """
        filtres_categorie = await self._supprimer_modalites_filtrees_inutiles(
            filtres_categorie
        )
        # Liste des graphiques
        graphiques = self.caracteristiques_indicateur["charts"]

        # we create categories associated to confid rules
        confid_categories = [graphique["categorie"] for graphique in graphiques]

        # if we prefiltering is configured for this analysis, we add it to confid rules categories
        if (
            self.caracteristiques_indicateur["filter"]
            and "." in self.caracteristiques_indicateur["filter"]
        ):
            additional_category = self.caracteristiques_indicateur["filter"].split(
                ".", 1
            )[0]
            # just checking we are not adding duplicated categories
            if additional_category not in confid_categories:
                confid_categories.append(additional_category)

        # we get real column names
        categories_concernees_par_confidentialite = (
            await self.get_columns_concerned_by_confid()
        )

        requete_filtres_finale = ""
        l_requete_finale = []
        masquer_tout_camembert = False
        d_liste_modalites_filtrees = {}
        for confid_category in confid_categories:
            if confid_category in filtres_categorie.keys():
                liste_modalites_filtrees = [
                    f["filtre_categorie"].split(".", 1)[1]
                    for f in filtres_categorie[confid_category]
                ]
                d_liste_modalites_filtrees[confid_category] = liste_modalites_filtrees
                if len(liste_modalites_filtrees) == 1:
                    masquer_tout_camembert = True

        if filtres_categorie == {}:
            return ""
        for confid_category in confid_categories:
            liste_mod = await self._modalites_disponibles_pour_territoire_et_categorie(
                confid_category
            )
            ensemble_mod = set([x["modalite"] for x in liste_mod])
            liste_modalites_filtrees = []
            if confid_category in d_liste_modalites_filtrees.keys():
                liste_modalites_filtrees = d_liste_modalites_filtrees[confid_category]
            liste_par_groupe_filtres = []
            if (
                not confid_category
            ):  # S'il n'y a pas de graphiques associés à l'indicateur,
                return ""  # On retourne une chaine de caractères vide
            # Si la catégorie du graphique est concernée par la confidentialité
            if confid_category in categories_concernees_par_confidentialite:
                # Si toutes les modalités sont disponibles, on filtre sur les lignes dont la confidentialité
                if (
                    len(liste_modalites_filtrees) != 0
                    and ensemble_mod == set(liste_modalites_filtrees)
                    and self.region != "nouvelle-aquitaine"
                ):
                    filtre_a_ajouter = "a.conditions->>'{nom_categorie}' = '*'".format(
                        nom_categorie=confid_category
                    )
                    l_filtres_a_ajouter = [
                        "a.conditions->>'{nom_categorie}' = '{x}'".format(
                            x=x, nom_categorie=confid_category
                        )
                        for x in liste_modalites_filtrees
                    ]
                    if filtre_a_ajouter not in liste_par_groupe_filtres:
                        liste_par_groupe_filtres.append(filtre_a_ajouter)
                        if masquer_tout_camembert:
                            liste_par_groupe_filtres += l_filtres_a_ajouter
                else:  # Si un filtre a été appliqué sur la catégorie en cours d'étude
                    if (
                        confid_category in filtres_categorie.keys()
                        and len(filtres_categorie[confid_category]) > 0
                    ):
                        for f in filtres_categorie[confid_category]:
                            # Alors on ajoute dans le filtre les modalités précises concernées
                            nom_categorie = f["filtre_categorie"].split(".", 1)[0]
                            nom_modalite = f["filtre_categorie"].split(".", 1)[1]
                            filtre_a_ajouter = "a.conditions->>'{nom_categorie}' = '{nom_modalite}'".format(
                                nom_categorie=nom_categorie,
                                nom_modalite=nom_modalite,
                            )
                            # Et on l'ajoute à la liste des modalités filtrées si elle ne s'y trouve pas déjà
                            if (
                                filtre_a_ajouter not in liste_par_groupe_filtres
                                and self.region != "nouvelle-aquitaine"
                            ):
                                liste_par_groupe_filtres.append(filtre_a_ajouter)
                            if self.region == "nouvelle-aquitaine":
                                liste_modalites_filtrees_sep = "||".join(
                                    [
                                        f["filtre_categorie"].split(".", 1)[1]
                                        for f in filtres_categorie[confid_category]
                                    ]
                                )

                                liste_par_groupe_filtres.append(
                                    "tri_chaine_caracteres(a.conditions->>'{nom_categorie}') = tri_chaine_caracteres('{nom_modalite}')".format(
                                        nom_categorie=f["filtre_categorie"].split(
                                            ".", 1
                                        )[0],
                                        nom_modalite=liste_modalites_filtrees_sep,
                                    )
                                )

                if liste_par_groupe_filtres:
                    if self.region == "paysdelaloire":
                        keyword = "or"
                    else:
                        keyword = "and"
                    requete_finale_groupe_filtres = (
                        " and (a.conditions->>'{nom_categorie}' = '*' or (".format(
                            nom_categorie=confid_category
                        )
                        + f" {keyword} ".join(liste_par_groupe_filtres)
                        + "))"
                    )  # Construction de la clause WHERE
                    if requete_finale_groupe_filtres not in l_requete_finale:
                        l_requete_finale.append(requete_finale_groupe_filtres)
                requete_filtres_finale = " ".join(l_requete_finale)
        return requete_filtres_finale

    async def _total_confidentiel(self, condition_confid):
        """Retourne un booléen qui indique si le total est - ou non - confidentiel

        Paramètres
        ----------
        condition_confid : chaine de caractères
            Bout de requête qui définit les conditions de confidentialité pour un territoire (clause WHERE)

        Returns
        --------
        Booléen
            True si la valeur est confidentielle, false sinon
        """
        if self.confid_layer is None:
            return False

        requete_annee = ""
        params = [self.confid_layer, self.code_territoire, self.type_territoire]
        if self.annee:
            requete_annee = "and a.annee = $4"
            params.append(self.annee)
        req = f"""
              select *
              from {self.schema}.confid_maille as a
              where a.type_analyse = $1
              and a.code = $2
              and a.type_territoire = $3
              {requete_annee}
              {condition_confid};
              """

        rset = await fetch(req, *params)
        est_confidentiel = [dict(x) for x in rset]

        if est_confidentiel:
            return True
        return False

    async def _confid_camembert_apres_filtre(self, condition_confid):
        """Retourne un booléen qui indique s'il est nécessaire - ou non - de rendre le diagramme confidentiel

        Paramètres
        ----------
        condition_confid : chaine de caractères
            Bout de requête qui définit les conditions de confidentialité pour un territoire (clause WHERE)

        Returns
        --------
        Booléen
            True s'il faut appliquer la confidentialité sur le diagramme circulaire, false sinon
        """
        if self.confid_layer is None:
            return False

        condition_annee = ""
        condition_confid_finale = ""
        if condition_confid:
            condition_confid_finale = "AND ({condition_confid})".format(
                # Il suffit de filtrer sur une seule modalité confidentielle pour que les valeurs confidentielles soient accessibles.
                condition_confid=condition_confid[4:]
            )
        else:
            return False
        params = [self.confid_layer, self.code_territoire, self.type_territoire]
        if self.annee:
            condition_annee = "AND annee = $4"
            params.append(self.annee)
        req = f"""
              select *
              from {self.schema}.confid_maille a
              WHERE type_analyse = $1
              AND code = $2
              AND type_territoire = $3
              {condition_annee}
              {condition_confid_finale};
              """

        rset = await fetch(req, *params)

        if rset:
            return True
        else:
            return False

    async def _confid_camembert_apres_filtre_pourcentage_du_total(
        self, donnees_par_categorie
    ):
        """Renvoie True s'il faut masquer les camembert dans le cas où une modalité concernée par la confidentialité représente plus de 80 % du total.

        On s'appuie pour cela sur l'algrorithme suivant :

        Quel que soit le graphique :
            Quel que soit la modalité de ce graphique :
                Si cette modalité est concernée par la confidentialité :
                    Si cette modalité est active :
                        Vérifier qu'elle représente moins de 80 % du total
                            Si oui :
                                On ne masque pas les camemberts (on retourne False)
                            Sinon :
                                On les masque (on retourne True)
        Paramètres
        ----------

        donnees_par_categorie : dictionnaire liste de dictionnaires
            Pour chaque catégorie, liste des valeurs pour chaque modalités

        Retourne
        --------
        Booléen
            True si on laisse les camemberts affichés, False sinon.

        """
        graphiques = self.caracteristiques_indicateur["charts"]
        #  À faire : factoriser l'obtention des données confidentielles avec la requête de la méthode _confid_camembert_apres_filtre
        condition_annee = ""
        params = [self.code_territoire, self.confid_layer, self.type_territoire]
        if self.annee:
            condition_annee = "AND annee = $4"
            params.append(self.annee)
        req = f"""
              select *
              from {self.schema}.confid_maille
              where code = $1
              {condition_annee}
              AND type_analyse = $2
              AND type_territoire = $3
              """
        res = await fetch(req, *params)

        modalites_concernees_par_confid = pd.DataFrame([dict(x) for x in res])
        for graphique in graphiques:  #  Pour chaue graphique
            modalites_pour_categorie = pd.DataFrame(
                donnees_par_categorie[graphique["categorie"]]
            )  #  Liste des modalités du graphique
            if (
                graphique["categorie"] in modalites_concernees_par_confid.keys()
            ):  #  Si la catégorie est bien dans le DataFRame
                for modalite in list(
                    modalites_pour_categorie["modalite"]
                ):  #  Pour chaque modalité de cette catégorie
                    if modalite in list(
                        modalites_concernees_par_confid[graphique["categorie"]]
                    ):  #  Si la modalte est concernée par la confidentialité
                        proportion = (
                            float(
                                modalites_pour_categorie[
                                    modalites_pour_categorie["modalite"] == modalite
                                ]["val"]
                                / modalites_pour_categorie["val"].sum()
                            )
                            * 100
                        )  #  Calcul de la proportion que représente cette modalité par rapport au total
                        if (
                            proportion >= 80
                        ):  #  Si cette proportion est supérieure à 80 %
                            return True  #  On masque les camemberts
        return False

    async def get_territorial_confid_rules(
        self, condition_confid, only_territories=True
    ):
        """Retourne les territoires où la donnée est confidentielle pour un indicateur donné.

        Paramètres
        ----------
        condition_confid : chaine de caractères
            Bout de requête qui définit les conditions de confidentialité pour un territoire (clause WHERE)

        Returns
        --------
        Liste
            Liste des données dont la structure est :
                [
                    {'code': '01010', 'x': 263931.0, 'y': 4721930.0},
                    {'code': '01016', 'x': 141975.0, 'y': 4839001.0},
                    .
                    .
                    .
                ]

        """
        if self.confid_layer is None:
            return []

        params = [self.confid_layer, self.maille]

        requete_annee = ""
        if self.annee:
            requete_annee = "AND a.annee = $3"
            params.append(self.annee)

        req = f"""
           select distinct a.code
          , trunc(x) as x
          , trunc(y) as y
          {", a.conditions" if not only_territories else ""}
          from {self.schema}.confid_maille as a
          join {self.schema}.{self.maille} as b on b.code = a.code
          where a.type_analyse = $1
          AND a.type_territoire = $2
          {requete_annee}
          {condition_confid}
          """

        rset = await fetch(req, *params)

        if not only_territories:
            return [
                {**dict(x), "conditions": json.loads(x["conditions"])} for x in rset
            ]
        else:
            return [dict(x) for x in rset]

    async def _donnees_confidentielles_diagramme(self):
        """Retourne le type de confidentialité d'un diagramme circulaire

        Paramètres
        ----------
        caracteristiques_indicateur : dict
            métadonnées d'un indicateur
        annee : int
            Dernière année pour lesquelles les données sont disponibles

        Returns
        --------
        dict
        """
        if not self.confid_layer:
            return {"charts": None}

        requete_annee = ""
        if self.annee:
            requete_annee = "and annee = $3"
        recup_confid_diagramme = f"""
                                 SELECT type_confid
                                 FROM {self.schema}.confid_camembert
                                 WHERE type_analyse = $1
                                 {requete_annee}
                                 AND code = $2
                                 """

        if self.annee:
            confid = await fetch(
                recup_confid_diagramme,
                self.confid_layer,
                self.code_territoire,
                self.annee,
            )
        else:
            confid = await fetch(
                recup_confid_diagramme, self.confid_layer, self.code_territoire
            )

        if confid:
            confid = confid[0]["type_confid"]

        return {"charts": confid}

    async def _obtenir_modalites(self, cat_name=None, out_format="dataframe"):
        """Retourne dans un dataframe toutes les modalités disponibles en base

        Retourne
        --------
        dataframe :
            Ensemble des modalités
        """
        cat_name_filter = ""
        params = [self.region]
        if cat_name is not None:
            cat_name_filter = "AND nom = $2"
            params.append(cat_name)
        requete = f"""   
                SELECT modalite_id, nom, modalite, couleur, region, ordre
                FROM meta.categorie
                WHERE region=$1
                {cat_name_filter}
                ORDER BY ordre
                """
        modalites = await fetch(requete, *params)
        modalites_dict = [dict(x) for x in modalites]
        if out_format == "list":
            return modalites_dict
        df = pd.DataFrame(modalites_dict)

        return df

    async def _obtenir_identifiant_modalite(self, modalites, categorie, modalite):
        """Retourne l'identifiant d'une modalité à partir de la catégorie à laquelle elle appartient de son nom

        Paramètres
        ----------
        modalites : dataframe
            listes de toutes les modalités
        catégorie : str
            nom de la catégorie
        modalité : str
            nom de la modalité

        Retourne
        --------
        entier :
            Identifiant de la modalité
        """

        # Filtre sur la catégorie et la modalité
        modalites_filtrees = modalites[
            (modalites.nom == categorie) & (modalites.modalite == modalite)
        ]
        # Distinct sur les valeurs
        modalites_filtrees.drop_duplicates()
        return modalites_filtrees.modalite_id.iloc[0]

    def _construction_filtre_valeur(self, filtre):
        """Construit le filtre lorsqu'il concerne les valeurs (indicateur distance - mobilité en AURA)

        Paramètre
        ---------
        filtre : liste
            Intervalle de deux valeurs entre lesquelles on filtre

        Returns
        --------
        Chaine de caractères
            Clause WHERE de la requête finale

        """
        condition_filtre_sup = ""
        clause = "and"
        op = ">="
        valeurs_a_filtrer = self.caracteristiques_indicateur[
            "data_deuxieme_representation"
        ]
        if self.maille == "commune":
            if not self.annee:
                clause = "where"

        if (
            not self.caracteristiques_indicateur["data_deuxieme_representation"]
            and self.caracteristiques_indicateur["type"] == "flow"
        ):
            valeurs_a_filtrer = "valeur"
        if (
            self.caracteristiques_indicateur["data_deuxieme_representation"]
            and self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi"
            and filtre != 0
        ):
            op = "="
        condition_filtre = "{clause} {valeurs_a_filtrer} {op} {filtre}"
        condition_filtre_inf = condition_filtre.format(
            valeurs_a_filtrer=valeurs_a_filtrer, filtre=filtre, op=op, clause=clause
        )
        # 0 n'est pas une valeur possible pour l'indicateur accessibilité emploi, on lance l'indicateur sans filtre
        if (
            self.caracteristiques_indicateur["data_deuxieme_representation"]
            and self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi"
            and filtre == 0
        ):
            condition_filtre_inf = ""
            condition_filtre_sup = ""
        # Si le filtre n'est pas un nombre, c'est qu'il est un intervalle ex : '50,200'
        if (
            not str(filtre).isdigit()
            and self.caracteristiques_indicateur["data_type"] != "accessibilite_emploi"
        ):
            #  On définit alors un filtre sup et un filtre inf
            condition_filtre_inf = condition_filtre.format(
                valeurs_a_filtrer=valeurs_a_filtrer,
                filtre=filtre.split(",")[0],
                op=">=",
                clause=clause,
            )
            condition_filtre_sup = condition_filtre.format(
                valeurs_a_filtrer=valeurs_a_filtrer,
                filtre=filtre.split(",")[1],
                op="<",
                clause=clause,
            )
            # Si la borne supérieure est supérieure à 100, alors on se contente du filtre valur < borne sup
            if int(filtre.split(",")[1]) >= 100:
                condition_filtre_sup = ""
        return {"inf": condition_filtre_inf, "sup": condition_filtre_sup}

    async def _construire_requetes_filtres_categories(
        self, filtres_categorie={}, type_filtre="identifiants"
    ):
        """Construit les bouts de requêtes SQL pour appliquer les filtres sélectionnés par l'utilisateur.rice.

        Paramètres
        ----------
        filtres_categorie : dictionnaire
            Filtre appliqué sur chaque catégorie. Les clé sont les catégories accompagnées de l'identifiant
            de l'indicateur et les valeurs des listes qui peuvent contenir plusieurs modalités.
            Exemple : {
                        "1energie" : [{"filtre_categorie" : 'Gaz'}, {"filtre_categorie" : 'Électricité'}],
                        "1usage" : [...],
                        .
                        .
                        .
                      }
        type_filtre : str (intitules|identifiants)
            Permet de choisir entre les identifiants de la modalité (identifiants)
            ou bien les valeurs (intitules)
        """
        modalites = await self._obtenir_modalites()
        if self.caracteristiques_indicateur[
            "filter"
        ]:  # Si un filtre doit être appliqué d'emblée,
            f_categorie_initiale = self.caracteristiques_indicateur["filter"]
            id_groupe_filtres = f_categorie_initiale.split(".", 1)[0]
            # Si le filtre en question ne se trouve pas dans le dictionnaire des filtres
            if not id_groupe_filtres in filtres_categorie.keys():
                filtres_categorie[id_groupe_filtres] = [
                    {"filtre_categorie": f_categorie_initiale}
                ]  # On l'y ajoute
        l_requete_finale = []
        for id_groupe_filtres in filtres_categorie:
            liste_par_groupe_filtres = []
            for f in filtres_categorie[id_groupe_filtres]:
                categorie = f["filtre_categorie"].split(".", 1)[0]
                modalite = f["filtre_categorie"].split(".", 1)[1]
                # On récupère l'identifiant de la modalité
                id_modalite = await self._obtenir_identifiant_modalite(
                    modalites, categorie, modalite
                )
                #  On ajoute ces composants de requête dans une liste
                if type_filtre == "intitules":
                    liste_par_groupe_filtres.append('"' + modalite + '"')
                else:
                    liste_par_groupe_filtres.append(str(id_modalite))
            if filtres_categorie[id_groupe_filtres]:
                if not self.caracteristiques_indicateur["moyenne_ponderee"]:
                    requete_finale_groupe_filtres = (
                        "and {categorie} in (".format(categorie=categorie)
                        + ",".join(liste_par_groupe_filtres)
                        + ")"
                    )  # Construction du bout de requête pour les filtres sur une catégorie
                else:
                    requete_finale_groupe_filtres = (
                        "(a.modalite_id in ("
                        + ",".join(liste_par_groupe_filtres)
                        + ") and a.type_categorie = '{categorie}')".format(
                            categorie=categorie
                        )
                    )  # Construction du bout de requête pour les filtres sur une catégorie
                l_requete_finale.append(requete_finale_groupe_filtres)
            else:
                for graphique in self.caracteristiques_indicateur["charts"]:
                    requete_finale_groupe_filtres = "and {categorie} in (9999)".format(
                        categorie=graphique["categorie"]
                    )
                    if self.caracteristiques_indicateur["moyenne_ponderee"]:
                        requete_finale_groupe_filtres = "(a.modalite_id in (9999) and a.type_categorie = '{categorie}')".format(
                            categorie=graphique["categorie"]
                        )  # Construction du bout de requête pour les filtres sur une catégorie
                    l_requete_finale.append(requete_finale_groupe_filtres)
            # On ajoute ces bouts de requêtes pour chaque catégorie dans une liste

        # Construction de la requête finale
        requete_filtres_finale = " ".join(l_requete_finale)
        if self.caracteristiques_indicateur["moyenne_ponderee"]:
            requete_filtres_finale = " or ".join(l_requete_finale)
        if (
            l_requete_finale
        ):  # Si la liste n'est pas vide, cela signifie qu'il faut appliquer un filtre
            return requete_filtres_finale
        else:
            return ""  # Sinon, on retourne une chaine vide

    async def _obtenir_total_ratio(self, ratio):
        """Calcule le total de la donnée par laquelle on divise une autre données

        Paramètres
        ----------
        ratio : str
            Nom de la table qui représente le ratio (ex : population, superficie etc.)

        Returns
        --------
        flottant (nombre par lequel on divise les valeurs d'une analyse)
        """
        condition_na = ""
        condition_year = ""
        if (
            self.caracteristiques_indicateur["data"] == "prod_enr"
            and self.region == "nouvelle-aquitaine"
        ):
            if self.caracteristiques_indicateur["data_ratio"]:
                condition_na = "and ratio_filiere_enr is not null"

        # we specify main geographical parameters that will be sent to the query
        params = [
            self.type_territoire,
            self.code_territoire,
            self.maille,
        ]

        if self.annee:
            annee = self.ratio_best_year or self.annee
            # we add year to parameters
            params.append(annee)
            condition_year = "AND r.annee = $4"

        if self.maille == "commune":
            requete_total_ratio = f"""
                      SELECT sum(r.valeur)
                      FROM {self.schema}.{ratio} r
                      JOIN {self.schema}.commune c ON c.code = r.commune
                      JOIN {self.schema}.territoire t on t.commune = c.code
                      WHERE t.type_territoire = $1
                      AND t.code = $2
                      AND $3 = 'commune' -- to avoid any issue with params sent to fetch function
                      {condition_year}
                      {condition_na} ;
                      """
        else:
            requete_total_ratio = f"""
                  SELECT sum(r.valeur)
                  FROM {self.schema}.{ratio} r
                  JOIN {self.schema}.commune c ON c.code = r.commune
                  JOIN {self.schema}.territoire tmaille on tmaille.commune = c.code
                  JOIN {self.schema}.{self.maille} tmaille1 on tmaille1.code = tmaille.code
                  JOIN {self.schema}.territoire tzone on tzone.commune = c.code
                  JOIN {self.schema}.{self.type_territoire} {self.type_territoire} 
                  on {self.type_territoire}.code = tzone.code
                  WHERE tmaille.type_territoire = $3
                  AND tzone.type_territoire = $1
                  AND tzone.code = $2
                  {condition_year}
                  {condition_na}
                  """
        total_ratio = await fetch(requete_total_ratio, *params)
        return total_ratio[0]["sum"]

    async def _total(self, filtre=0, filtres_categorie=None, year_found_for_ratio=None):
        """Retourne le total d'un indicateur pour le territoire sélectionné (à afficher dans la légende)

        Paramètres
        ----------
        filtre : entier
            Seuil qu'on définit au moyen d'une réglette et avec lequel on filtre les valeurs sur une colonne à spécifier
        filtres_categorie : dict
            Cf. documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------
        Valeur réelle
            total de l'indicateur
        """
        if filtres_categorie is None:
            filtres_categorie = {}
        filtre_inf = ""
        filtre_sup = ""
        having_sum = ""
        condition_na = ""
        if (
            self.caracteristiques_indicateur["data"] == "prod_enr"
            and self.region == "nouvelle-aquitaine"
        ):
            if not self.caracteristiques_indicateur["data_ratio"]:
                condition_na = "and filiere_enr is not null"
            else:
                condition_na = "and ratio_filiere_enr is not null"

        # TOFIX: is this code used? Can you enter inside this function with only_for_zone not empty?
        if self.caracteristiques_indicateur["only_for_zone"]:
            val = await self._total_indicateur_zone_unique()
            return {"total": val}

        filtres_categorie = await self._construire_requetes_filtres_categories(
            filtres_categorie
        )
        #  S'il faut combiner plusieurs représentations carto, cela signifie qu'il est possible d'ajouter
        #  un filtre sur les valeurs d'une colonne qui est en fait un intervalle de valeurs.
        # S'il est nécessaire de combiner deux représentations distinctes
        if self.caracteristiques_indicateur["data_deuxieme_representation"]:
            filtre_inf = self._construction_filtre_valeur(filtre)["inf"]
            filtre_sup = self._construction_filtre_valeur(filtre)["sup"]

        if self.caracteristiques_indicateur["type"] == "flow":
            having_sum = "having (sum(a.valeur) >= {filtre})".format(filtre=filtre)

        condition_annee = ""
        if self.annee:
            annee = self.annee
            condition_annee = f"where a.annee = {annee}"

        coeff = self.coeff
        if self.caracteristiques_indicateur["afficher_calcul_et_donnees_table"]:
            coeff = ""

        condition_somme = "sum(a.valeur{coeff})::float".format(coeff=coeff)
        condition_somme_commune = "sum(valeur{coeff})::float".format(coeff=coeff)

        if self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi":
            condition_somme = (
                "(sum(a.valeur * a.nbre_actifs) / sum(a.nbre_actifs))::float"
            )
            condition_somme_commune = (
                "(sum(a.valeur * a.nbre_actifs) / sum(a.nbre_actifs))::float"
            )

        if self.caracteristiques_indicateur["data_type"] == "depenses_carburant_menage":
            condition_somme = "(sum(a.valeur * a.nb_menage) / sum(a.nb_menage))::float"
            condition_somme_commune = (
                "(sum(a.valeur * a.nb_menage) / sum(a.nb_menage))::float"
            )

        if (
            self.type_territoire == "region"
        ):  # Si l'échelle est la région, il est possible d'optimiser la requête en évitant une jointure
            req = """
                select {condition_somme} as val
                from {schema_region}.{jeu_de_donnees} as a
                join {schema_region}.territoire t on t.commune = a.commune
                {condition_annee}
                {condition_na}
                {filtre_inf}
                {filtre_sup}
                {filtres_categorie}
                and t.type_territoire = '{maille}'
                {having_sum};
                """.format(
                schema_region=self.schema,
                maille=self.maille,
                jeu_de_donnees=self.jeu_de_donnees,
                coeff=coeff,
                condition_somme=condition_somme,
                filtre_inf=filtre_inf,
                filtre_sup=filtre_sup,
                filtres_categorie=filtres_categorie,
                condition_annee=condition_annee,
                having_sum=having_sum,
                condition_na=condition_na,
            )

            if self.maille == "commune":
                req = """
                    select {condition_somme_commune} as val
                    from {schema_region}.{jeu_de_donnees} as a
                    {condition_annee}
                    {condition_na}
                    {filtre_inf}
                    {filtre_sup}
                    {filtres_categorie}
                    {having_sum};
                    """.format(
                    schema_region=self.schema,
                    jeu_de_donnees=self.jeu_de_donnees,
                    condition_somme_commune=condition_somme_commune,
                    filtre_inf=filtre_inf,
                    filtre_sup=filtre_sup,
                    filtres_categorie=filtres_categorie,
                    condition_annee=condition_annee,
                    having_sum=having_sum,
                    condition_na=condition_na,
                )

        else:
            req = """
                  select {condition_somme} as val
                  from {schema_region}.{jeu_de_donnees} as a
                  join {schema_region}.territoire t on t.commune = a.commune
                  join {schema_region}.territoire tmaille on tmaille.commune = a.commune
                  join {schema_region}.{maille} maille on maille.code = tmaille.code
                  {condition_annee}
                  {condition_na}
                  {filtre_inf}
                  {filtre_sup}
                  {filtres_categorie}
                  and t.type_territoire = '{type_territoire}'
                  and tmaille.type_territoire = '{maille}'
                  and t.code = '{code_territoire}'
                  {having_sum};
                  """.format(
                type_territoire=self.type_territoire,
                maille=self.maille,
                code_territoire=self.code_territoire,
                schema_region=self.schema,
                jeu_de_donnees=self.jeu_de_donnees,
                condition_somme=condition_somme,
                filtre_inf=filtre_inf,
                filtre_sup=filtre_sup,
                filtres_categorie=filtres_categorie,
                condition_annee=condition_annee,
                condition_na=condition_na,
                having_sum=having_sum,
            )

            if self.maille == "commune":
                req = """
                      select {condition_somme} as val
                      from {schema_region}.{jeu_de_donnees} as a
                      join {schema_region}.territoire t on t.commune = a.commune
                      {condition_annee}
                      {condition_na}
                      {filtre_inf}
                      {filtre_sup}
                      {filtres_categorie}
                      and t.type_territoire = '{type_territoire}'
                      and t.code = '{code_territoire}'
                      {having_sum};
                      """.format(
                    type_territoire=self.type_territoire,
                    code_territoire=self.code_territoire,
                    schema_region=self.schema,
                    jeu_de_donnees=self.jeu_de_donnees,
                    condition_somme=condition_somme,
                    filtre_inf=filtre_inf,
                    filtre_sup=filtre_sup,
                    filtres_categorie=filtres_categorie,
                    condition_annee=condition_annee,
                    condition_na=condition_na,
                    having_sum=having_sum,
                )

        res = await fetch(req)
        valeur = res[0]["val"]
        if valeur is None:
            valeur = 0
        d = {"total": valeur, "total_ratio": 1}

        ratio_table_name = self.ratio.replace("/maille.", "")
        if (
            ratio_table_name
            and not self.caracteristiques_indicateur["afficher_calcul_et_donnees_table"]
        ):  # S'il s'agit d'un ratio
            if not self.ratio_best_year:
                query_closest_year = f"""SELECT DISTINCT annee, ABS(annee - $1)
                    FROM {self.schema}.{ratio_table_name} ORDER BY ABS(annee - $1)"""
                values_closest_year = await fetch(query_closest_year, self.annee)
                best_year = values_closest_year[0]["annee"]
                self.ratio_best_year = best_year

            # Calcul de la valeur totale par laquelle on doit diviser
            total_ratio = await self._obtenir_total_ratio(ratio_table_name)

            if self.ratio_best_year and self.ratio_best_year != self.annee:
                d["ratio_best_year"] = self.ratio_best_year

            if total_ratio is not None:
                d["total_ratio"] = total_ratio
                d["total"] = float(valeur) / float(total_ratio)
                # Retour de la division du total de l'indicateur par celui du ratio
                # Sinon, on retourne simplement la somme sur toutes les communes
        return d

    async def _total_indicateur_zone_unique(self):
        """Retourne la valeur d'un indicateur disponible pour un seul type de territoire.

        Returns
        --------
            Réel : valeur de l'indicateur pour le territoire sélectionné
        """
        req = f"""SELECT * FROM {self.schema}.{self.jeu_de_donnees} LIMIT 1"""
        res = await fetch(req)
        res = [dict(r) for r in res]

        nombre_decimales = self.caracteristiques_indicateur["decimals"]
        # if we have a communal data table
        if "commune" in res[0]:
            req = f"""
              select round({self.unit_factor}*cast(valeur as numeric), {nombre_decimales}) as val
              from {self.schema}.{self.jeu_de_donnees} as a
              LEFT JOIN {self.schema}.territoire as t ON t.commune = a.commune
              WHERE t.type_territoire = $1 AND t.code = $2
              """
            res = await fetch(req, self.type_territoire, self.code_territoire)
        # if we have another geographical level of data
        else:
            req = f"""
                select round({self.unit_factor}*cast(valeur as numeric), {nombre_decimales}) as val
                from {self.schema}.{self.jeu_de_donnees} as a
                LEFT JOIN {self.schema}.territoire as t ON t.code = a.{self.maille} AND t.type_territoire = $3
                LEFT JOIN {self.schema}.territoire as tr ON tr.commune = t.commune AND tr.type_territoire = $1
                WHERE tr.code = $2
                """
            res = await fetch(
                req, self.type_territoire, self.code_territoire, self.maille
            )
        return res[0]["val"]

    def _min_max_valeurs(self, resultats, donnees_territoire_confidentielles):
        """Retourne les valeurs minimale et maximale de l'indicateur pour le territoire sélectionné

        Paramètres
        ----------
        resultats : dictionnaire
            Résultats retournés par _donnees_par_territoire de la classe fille DonneesPourRepresentationCarto

        Returns
        --------
        dictionnaire
            {"min" : valeur minimale, "max" : valeur maximale}

        """
        # on récupère toutes les valeurs qui ne sont pas confidentielles
        if donnees_territoire_confidentielles:
            liste_valeurs = [
                float(x["val"]) for x in resultats if x["val"] and not x["confidentiel"]
            ]
        else:
            liste_valeurs = [float(x["val"]) for x in resultats if x["val"]]

        # s'il n'y a aucune valeur, on envoie finalement zéro
        if not liste_valeurs or len(liste_valeurs) == 0:
            return {"min": 0, "max": 0}
        return {"min": min(liste_valeurs), "max": max(liste_valeurs)}

    def obtenir_liste_categories(self):
        return [a["categorie"] for a in self.caracteristiques_indicateur["charts"] if a]

    def obtenir_donnees_par_territoire(self, donnees, liste_categories):
        """Agrège les données par territoires et catégorie afin de ne conserver que les données par territoires

        Paramètres
        ----------

        donnees : Liste
            Liste de dictionnaires contenant les données par territoire et catégorie
        liste_categories : Liste
            Liste des catégories disponibles pour l'indicateur exécuté

        Returns
        --------

        Liste de dictionnaires avec uniquement les données par territoire
        """
        df = pd.DataFrame(donnees)
        if (
            "orig" in df.keys() and "dest" in df.keys()
        ):  #  Cas particulier pour les migrations pendulaires
            df[["orig", "dest"]] = df[["orig", "dest"]].astype(
                str
            )  # Liste de coordonnées non agrégeables à convertir en chaîne
        ratio = self.ratio.replace("maille.", "")[1:]
        df_somme = df

        liste_col = [
            x
            for x in df.keys()
            if x not in liste_categories
            and x != "val"
            and x != "val_applats"
            and x != "val_filtre"
            and x != "rapport"
        ]
        df_somme[liste_categories] = df_somme[liste_categories].fillna(
            "inc"
        )  #  L'aggrégation supprime les lignes de certaines communes si pas de valeur
        df_somme = df_somme.replace([np.inf, -np.inf], 0)
        if "val" in df.keys():
            # During the groupby, a group with no values should be kept as NaN and not changed to 0: use min_count=1 (skip_na parameter would be more explicit but is broken : https://github.com/pandas-dev/pandas/issues/15675)
            df_somme = df.groupby(liste_col)["val"].sum(min_count=1).reset_index()
        if "val_applats" in df.keys():
            df_somme = (
                df.groupby(liste_col)[["val", "val_applats"]]
                .sum(min_count=1)
                .reset_index()
            )
            df_somme["val_applats"] = df_somme["val_applats"].astype(float)
            df_somme["val_applats"] = df_somme["val_applats"] / df_somme[ratio]

        if (
            ratio in df.keys()
            and not self.caracteristiques_indicateur["afficher_calcul_et_donnees_table"]
        ):
            if len(liste_categories) == 0:
                df_val = df[df[ratio] != 0]
                df_val = df_val[["code", "val"]].drop_duplicates()
                df_ratio = df[["code", ratio]]
                df_ratio = df_ratio.groupby("code")[[ratio]].agg("sum").reset_index()
                df_somme = df_ratio.merge(
                    df_val, left_on="code", right_on="code", how="outer"
                )

            df_somme[["val", ratio]] = df_somme[["val", ratio]].astype(float)
            df_somme["val"] = df_somme["val"] / df_somme[ratio]
            df_somme["val"] = df_somme["val"].where(df_somme["val"].notnull(), None)
            df_somme = df_somme.replace([np.inf, -np.inf], 0)

        if self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi":
            lc = [x for x in liste_col if x != "nbre_actifs"]
            df_somme_nbre_actif = df.groupby(lc)["nbre_actifs"].agg("sum").reset_index()
            df_somme_val = df.groupby(lc)["val_filtre"].agg("sum").reset_index()
            df_somme = df_somme_nbre_actif.merge(
                df_somme_val, right_on="code", left_on="code"
            )
            df_moyenne = (
                df.drop("nbre_actifs", axis=1)
                .groupby(lc)["rapport"]
                .agg("mean")
                .reset_index()
            )
            df_agg = df_somme.merge(df_moyenne, left_on="code", right_on="code")
            df_somme = df_agg
            df_somme["val"] = df_somme["val_filtre"]
            if self.maille != "commune":
                df_somme = df[liste_col].drop_duplicates()
                df_somme["num"] = df["val_filtre"].astype(float) * df[
                    "nbre_actifs"
                ].astype(float)
                df_somme_val = df_somme.groupby(lc)["num"].agg("sum").reset_index()
                df_somme_nbre_actif = (
                    df_somme.groupby("code")["nbre_actifs"].agg("sum").reset_index()
                )
                df_moy = df.groupby("code")["rapport"].agg("mean").reset_index()
                df_somme_ratio = df_somme_val.merge(
                    df_somme_nbre_actif, left_on="code", right_on="code"
                )
                df_somme_ratio["val"] = df_somme_ratio["num"] / df_somme_ratio[
                    "nbre_actifs"
                ].replace(0, np.nan)
                df_final = df_somme_ratio.merge(df_moy, left_on="code", right_on="code")
                df_somme = df_final

        df_somme = df_somme.where(df_somme.notnull(), None)
        df_somme.replace({np.NaN: None}, inplace=True)

        # rounding
        if "val_applats" in df_somme.keys():
            df_somme["val_applats"] = (
                self.unit_factor * df_somme["val_applats"].fillna(0.0)
            ).round(self.caracteristiques_indicateur["decimals"])
        if "val" in df_somme.keys():
            df_somme["val"] = (self.unit_factor * df_somme["val"].astype(float)).round(
                self.caracteristiques_indicateur["decimals"]
            )

        d = df_somme.to_dict("records")

        for i in d:
            if "val" in i and str(i["val"]) == "nan":
                i["val"] = None

        if (
            "orig" in df.keys() and "dest" in df_somme.keys()
        ):  #  Cas particulier pour les migrations pendulaires
            for donnee in d:
                donnee["dest"] = json.loads(
                    donnee["dest"]
                )  #  Conversion des chaînes en JSON
                donnee["orig"] = json.loads(donnee["orig"])
        return d

    async def obtenir_donnees_par_categorie(
        self, donnees, liste_categories, total_ratio
    ):
        """Agrège les données par territoires et catégorie afin de ne conserver que les données par catégorie

        Paramètres
        ----------

        donnees : Liste
            Liste de dictionnaires contenant les données par territoire et catégorie
        liste_categories : Liste
            Liste des catégories disponibles pour l'indicateur exécuté

        Returns
        --------

        Liste de dictionnaires avec uniquement les données par catégorie
        """

        modalites = (
            await self._obtenir_modalites()
        )  # Obtention de toutes les modalité de toutes les catégorie pour la région sélectionnée
        df = pd.DataFrame(donnees)
        ratio = self.ratio.replace("maille.", "")[1:]
        liste_graphiques = []
        d = {}
        for categorie in liste_categories:
            df_categorie = df.merge(
                modalites[modalites["nom"] == categorie],
                right_on="modalite",
                left_on=categorie,
                how="outer",
            )  #  Jointure avec un DataFrame ne contenant que les modalités de la catégorie conernée
            modalites_disponibles = (
                await self._modalites_disponibles_pour_territoire_et_categorie(
                    categorie
                )
            )
            modalites_disponibles = [x["modalite"] for x in modalites_disponibles]
            val = "val"
            if "val" in df.keys():
                df_somme_categorie = (
                    df_categorie.groupby(
                        [
                            "modalite",
                            "modalite_id",
                            "couleur",
                            "ordre",
                        ]
                    )["val"]
                    .agg("sum")
                    .reset_index()
                )

            unit_factor_applied = False
            if ratio in df.keys():
                df_somme = df_somme_categorie
                df_somme["val"] = df_somme["val"].astype(float)
                df_somme["val"] = df_somme["val"] / total_ratio
                df_somme = df_somme.replace([np.inf, -np.inf], 0)
                unit_factor_applied = True
                df_somme["val"] = (self.unit_factor * df_somme["val"]).round(
                    self.caracteristiques_indicateur["decimals"]
                )
                df_somme_categorie = df_somme.where(df_somme.notnull(), None)

            if self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi":
                df_categorie["val_filtre"] = df_categorie["val_filtre"].astype(
                    float
                ) * df_categorie["nbre_actifs"].astype(float)
                df_somme_categorie = (
                    df_categorie.groupby(
                        ["modalite", "modalite_id", "couleur", "ordre"]
                    )["val_filtre"]
                    .agg("sum")
                    .reset_index()
                )
                df_somme_categorie_actifs = (
                    df_categorie.groupby(
                        ["modalite", "modalite_id", "couleur", "ordre"]
                    )["nbre_actifs"]
                    .agg("sum")
                    .reset_index()
                )
                df_moyenne = (
                    df_categorie.groupby(
                        ["modalite", "modalite_id", "couleur", "ordre"]
                    )["rapport"]
                    .agg("mean")
                    .reset_index()
                )
                df_agg = df_somme_categorie.merge(
                    df_moyenne, left_on="modalite", right_on="modalite"
                )
                df_somme_categorie = df_agg
                df_f = df_somme_categorie.merge(
                    df_somme_categorie_actifs, left_on="modalite", right_on="modalite"
                )
                df_f["couleur"] = df_f["couleur_x"]
                df_f["rapport"] = df_f["rapport"].fillna(0)

                val = "val_filtre"
                df_f["val_filtre"] = df_f["val_filtre"].astype(float) / df_f[
                    "nbre_actifs"
                ].astype(float)

                df_somme_categorie = df_f.fillna(0)

            if "ordre" in df_somme_categorie.columns:
                df_somme_categorie = df_somme_categorie.sort_values("ordre")

            final_factor = self.unit_factor if not unit_factor_applied else 1.0

            liste_graphiques.append(
                {
                    "indicateur": self.identifiant_indicateur,
                    "name": categorie,
                    "data": [
                        (
                            round(
                                final_factor * float(x),
                                self.caracteristiques_indicateur["decimals"],
                            )
                            if str(x) != str(np.NaN)
                            else None
                        )
                        for x in df_somme_categorie[val]
                    ],
                    "labels": [
                        x if x in modalites_disponibles else "indisponible"
                        for x in df_somme_categorie["modalite"]
                    ],
                    "modalite_id": [x for x in df_somme_categorie["modalite_id"]],
                    "colors": [x for x in df_somme_categorie["couleur"]],
                    "modalites_disponibles": modalites_disponibles,
                }
            )

            d[categorie] = df_somme_categorie.to_dict("records")
        graphiques = {
            "charts": liste_graphiques,
            "unite": self.caracteristiques_indicateur["unit"],
            "donnees_par_categorie": d,
        }

        return graphiques


class DonneesPourRepresentations(Representations):
    """Hérite des attributs et méthodes de Representation grâce auxquelles la méthode de cette classe
    retourne les données dont on a besoin pour afficher la visualisation cartographique des indica-
    teurs. Instancie également dynamiquement les classes filles selon la situation.
    """

    async def _donnees_par_territoire(self, provenance, filtre, filtres_categorie):
        """Retourne les données par territoire (nom, valeur, longitude et latitude du centroïde, code du territoire)

        Paramètres
        ----------

        filtre : entier
            valeur de seuil avec laquelle on filtre les valeurs d'une des colonnes de la table à partir de laquelle on construit l'indicateur
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categories
        Indicateur : str
            Nom de la table à partir de laquelle l'indicateur est construit
        coeff : entier
            Coefficient avec lequel on multiplie les valeurs de l'indicateur, généralement dans le cas d'une conversion entre unités
        ratio : flottant

        Returns
        --------
        Dictionnaire :
            {
            map : [{"val" : valeur, "code": code du territoire, "nom" : nom du territoire, "x": longitude, "y" latitude}, {...}]
            }

        """
        if (
            not self.afficher_indicateur_maille
            and provenance not in "tableau_de_bord_restitue"
        ):
            return []

        liste_categories = self.obtenir_liste_categories()
        conditions_categorie = ""
        categories_selectionnees = ""
        selection_finale_categories = ""
        categories_group_by = ""
        categories_jointures = ""
        conditions_categorie_region = ""
        if liste_categories:
            liste_conditions = []
            liste_selections = []
            liste_selection_finale = []
            liste_categories_jointures = []
            liste_categories_group_by = []
            liste_condition_categorie_region = []
            for categorie in liste_categories:
                liste_conditions.append(f" {categorie}.nom = '{categorie}'")
                liste_selections.append(f" {categorie}.modalite as {categorie}")
                liste_selection_finale.append(f" res_finaux.{categorie} as {categorie}")
                liste_categories_jointures.append(
                    f"join meta.categorie {categorie} on {categorie}.modalite_id = a.{categorie}"
                )
                liste_categories_group_by.append(f"{categorie}.modalite")
                liste_condition_categorie_region.append(
                    f"{categorie}.region = '{self.region}'"
                )
            conditions_categorie = "and " + " and ".join(liste_conditions)
            categories_selectionnees = ", " + ", ".join(liste_selections)
            selection_finale_categories = ", " + ", ".join(liste_selection_finale)
            categories_group_by = ", " + ", ".join(liste_categories_group_by)
            categories_jointures = " ".join(liste_categories_jointures)
            conditions_categorie_region = "and " + " and ".join(
                liste_condition_categorie_region
            )

        params = [self.code_territoire]

        #  Bout de requête à ajouter dans la requête finale pour appliquer les filtres sur les modalités que l'utilsiateur.rice a sélectionnées
        requete_filtres_categories = await self._construire_requetes_filtres_categories(
            filtres_categorie
        )
        version_simple_indicateur = ""
        version_simple_indicateur_finale = ""
        condition_rapport = ""
        condition_rapport_finale = ""
        alias_val = "val"
        if self.caracteristiques_indicateur["afficher_calcul_et_donnees_table"]:
            alias_val = "val_applats"
            version_simple_indicateur = "sum(a.valeur) as valeur_simple,"
            version_simple_indicateur_finale = (
                "max(res_finaux.valeur_simple::numeric) as val,"
            )
        ratio = self.ratio.replace("maille", self.maille)

        selection_finale = """
            case when max(res_finaux.val_filtre) < 0 then -1 * cast(max(abs(res_finaux.val_filtre)) as numeric)
            else cast(max(abs(res_finaux.val_filtre)) as numeric)
            end
        """
        if "mobilite_insee" in self.caracteristiques_indicateur["data"]:
            selection_finale = (
                "cast(coalesce(max(res_finaux.val_filtre), 0) as numeric)"
            )

        condition_somme_valeur = "sum(a.valeur{coeff}) as val_filtre,".format(
            coeff=self.coeff
        )

        condition_annee = ""
        if self.annee:
            condition_annee = "AND a.annee = {annee}".format(annee=self.annee)

        condition_filtre_sup = ""
        condition_filtre_inf = ""
        selection_moyenne_ponderee = ""

        # S'il est nécessaire de combiner deux représentations distinctes
        if (
            self.caracteristiques_indicateur["data_deuxieme_representation"]
            and self.caracteristiques_indicateur["data_type"] != "accessibilite_emploi"
        ):
            condition_filtre_inf = self._construction_filtre_valeur(filtre)["inf"]
            condition_filtre_sup = self._construction_filtre_valeur(filtre)["sup"]

        if self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi":
            condition_filtre_inf = self._construction_filtre_valeur(filtre)["inf"]
            condition_rapport = "avg(a.rapport)::float as rapport,"
            condition_rapport_finale = "max(res_finaux.rapport) as rapport,"
            selection_moyenne_ponderee = "max(res_finaux.nbre_actifs) as nbre_actifs,"
            alias_val = "val_filtre"
            condition_somme_valeur += "sum(a.nbre_actifs) as nbre_actifs,"
            if self.maille != "commune":
                condition_somme_valeur = (
                    "a.valeur::float as val_filtre, a.nbre_actifs as nbre_actifs,"
                )
                condition_rapport = "a.rapport as rapport,"

        liste_categories_ = ""
        liste_cats = self.obtenir_liste_categories()
        if liste_cats:
            liste_categories_ = ", " + ", ".join(liste_cats)
        if self.maille == "commune":
            req_valeurs_carte_filtrees = f"""
                SELECT DISTINCT {selection_finale} AS {alias_val}, {selection_moyenne_ponderee} {condition_rapport_finale} {version_simple_indicateur_finale}
                    {self.maille}.code, {self.maille}.nom, trunc({self.maille}.x) as x, trunc({self.maille}.y) as y {selection_finale_categories}
                FROM (
                    SELECT {condition_somme_valeur} {condition_rapport} {version_simple_indicateur} a.commune as code {categories_selectionnees}
                    FROM {self.schema}.{self.jeu_de_donnees} as a
                    FULL OUTER JOIN {self.schema}.territoire tzone ON tzone.commune = a.commune
                    FULL OUTER JOIN {self.schema}.{self.type_territoire} zone ON zone.code = tzone.code
                    JOIN {self.schema}.commune commune ON commune.code = a.commune
                    {categories_jointures}
                    WHERE tzone.code = $1 AND tzone.type_territoire = '{self.type_territoire}'
                        {condition_annee}
                        {condition_filtre_inf}
                        {condition_filtre_sup}
                        {requete_filtres_categories}
                        {conditions_categorie}
                        {conditions_categorie_region}
                    GROUP BY a.commune, {self.maille}.nom, {self.maille}.x, {self.maille}.y
                        {categories_group_by}
                ) AS res_finaux
                FULL OUTER JOIN {self.schema}.territoire tfinal ON tfinal.commune = res_finaux.code
                FULL OUTER JOIN {self.schema}.{self.maille} {self.maille} ON {self.maille}.code = tfinal.commune
                WHERE tfinal.code = $1
                GROUP BY {self.maille}.code, {self.maille}.nom, {self.maille}.x, {self.maille}.y {liste_categories_}
                ORDER BY {self.maille}.code, {self.maille}.nom
            """
        else:
            if (
                self.caracteristiques_indicateur["data_type"]
                == "depenses_carburant_menage"
            ):
                condition_somme_valeur = "(sum(a.valeur * a.nb_menage) / sum(a.nb_menage))::float as val_filtre, "
            group_by = ""
            if self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi":
                group_by = ", a.nbre_actifs, a.valeur, a.rapport"

            req_valeurs_carte_filtrees = f"""
                SELECT DISTINCT {selection_finale} AS {alias_val}, {selection_moyenne_ponderee} {condition_rapport_finale} {version_simple_indicateur_finale}
                    {self.maille}.code, {self.maille}.nom, trunc({self.maille}.x) AS x, trunc({self.maille}.y) AS y {selection_finale_categories}
                FROM (
                    SELECT {condition_somme_valeur} {condition_rapport} {version_simple_indicateur} tmaille.code AS code {categories_selectionnees}
                    FROM {self.schema}.{self.jeu_de_donnees} AS a
                    FULL OUTER JOIN {self.schema}.territoire tzone ON tzone.commune = a.commune
                    FULL OUTER JOIN {self.schema}.{self.type_territoire} zone ON zone.code = tzone.code
                    FULL OUTER JOIN {self.schema}.territoire tmaille ON tmaille.commune = tzone.commune
                    {categories_jointures}
                    WHERE tmaille.type_territoire = '{self.maille}'
                        AND tzone.code = $1 AND tzone.type_territoire = '{self.type_territoire}'
                        {condition_annee}
                        {condition_filtre_inf}
                        {condition_filtre_sup}
                        {requete_filtres_categories}
                        {conditions_categorie}
                        {conditions_categorie_region}
                    GROUP BY tmaille.code, tzone.code {group_by} {categories_group_by}
                ) AS res_finaux
                FULL OUTER JOIN (
                    SELECT tmaille.code AS maille, tzone.code AS code
                    FROM {self.schema}.territoire tzone
                    FULL OUTER JOIN {self.schema}.territoire tmaille ON tmaille.commune = tzone.commune
                    WHERE tzone.type_territoire = '{self.type_territoire}'
                        AND tmaille.type_territoire = '{self.maille}'
                ) AS tfinal ON tfinal.maille = res_finaux.code
                FULL OUTER JOIN {self.schema}.{self.maille} {self.maille} ON {self.maille}.code = tfinal.maille
                WHERE tfinal.code = $1
                GROUP BY {self.maille}.code, {self.maille}.nom, {self.maille}.x, {self.maille}.y {liste_categories_}
                ORDER BY {self.maille}.code, {self.maille}.nom
            """

        valeurs_carte_filtrees = await fetch(req_valeurs_carte_filtrees, *params)
        valeurs_carte_filtrees_liste_dict = [dict(x) for x in valeurs_carte_filtrees]
        valeurs_carte_filtrees_df = pd.DataFrame(valeurs_carte_filtrees_liste_dict)

        if ratio:
            ratio_table_name = self.ratio.split(".")[1]
            params = [self.maille, self.code_territoire, self.type_territoire]

            if self.annee:
                if not self.ratio_best_year:
                    query_closest_year = f"""SELECT DISTINCT annee, ABS(annee - $1)
                        FROM {self.schema}.{ratio_table_name} ORDER BY ABS(annee - $1)"""
                    values_closest_year = await fetch(query_closest_year, self.annee)
                    best_year = values_closest_year[0]["annee"]
                    self.ratio_best_year = best_year

                # we add the condition for year
                query_year_condition = "AND ratio.annee = $4"
                params.append(self.ratio_best_year)
            else:
                query_year_condition = ""

            if self.maille == "commune":
                query_ratio = f"""SELECT sum(ratio.valeur) AS {ratio_table_name},
                            ratio.commune AS code,
                            tzone.code AS code_zone
                            FROM {self.schema}.territoire tzone
                            FULL OUTER JOIN {self.schema}.{ratio_table_name} ratio ON ratio.commune = tzone.commune
                            WHERE tzone.code = $2
                                AND tzone.type_territoire = $3
                                AND $1 = 'commune'
                                {query_year_condition}
                            GROUP BY ratio.commune,
                                    tzone.code;"""
            else:
                query_ratio = f"""SELECT sum(ratio.valeur) AS {ratio_table_name},
                            tmaille.code AS code,
                            tzone.code AS code_zone
                            FROM {self.schema}.territoire tzone
                            FULL OUTER JOIN {self.schema}.{self.type_territoire} zone ON zone.code = tzone.code
                            FULL OUTER JOIN {self.schema}.territoire tmaille ON tmaille.commune = tzone.commune
                            FULL OUTER JOIN {self.schema}.{ratio_table_name} ratio ON ratio.commune = tmaille.commune
                            WHERE tmaille.type_territoire = $1
                                AND tzone.code = $2
                                AND tzone.type_territoire = $3
                                {query_year_condition}
                            GROUP BY tmaille.code,
                                    tzone.code;"""

            values_ratio = await fetch(query_ratio, *params)
            values_ratio_dict = [dict(x) for x in values_ratio]
            values_ratio_df = pd.DataFrame(values_ratio_dict)
            if len(values_ratio_df) == 0:
                logger.error(
                    f"""Aucune valeur trouvée dans la table {ratio_table_name} dans les conditions demandées."""
                )
                return []
            valeurs_carte_filtrees_df = (
                valeurs_carte_filtrees_df.set_index("code")
                .join(values_ratio_df[["code", ratio_table_name]].set_index("code"))
                .reset_index()
            )
            columns = list(valeurs_carte_filtrees_df.columns)
            valeurs_carte_filtrees_df = valeurs_carte_filtrees_df[
                [ratio_table_name] + [c for c in columns if c != ratio_table_name]
            ]

        valeurs_carte_filtrees_df = valeurs_carte_filtrees_df[
            valeurs_carte_filtrees_df["x"].notnull()
        ]
        valeurs_carte_filtrees_df = valeurs_carte_filtrees_df[
            valeurs_carte_filtrees_df["y"].notnull()
        ]
        valeurs_carte_filtrees_liste_dict = valeurs_carte_filtrees_df.to_dict("records")
        return valeurs_carte_filtrees_liste_dict

    async def _donnees_par_categories(self, provenance, filtre=0, filtres_categorie={}):
        """Retourne les données par catégories (nom de la catégorie, valeurs, libellés des modalités etc.)

        Paramètres
        ----------
        filtre : entier
            valeur de seuil avec laquelle on filtre les valeurs d'une des colonnes de la table à partir de laquelle on construit l'indicateur
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------
        Dictionnaire :
            {
                "charts" : [{'indicateur': 1,
                'name': 'secteur',
                'data': [Decimal('54'), Decimal('4'), Decimal('421'), Decimal('878'), Decimal('161'), Decimal('40'), Decimal('124')],
                'labels': ['Agriculture, sylviculture et aquaculture', 'Gestion des déchets', 'Résidentiel', 'Transport routier', 'Industrie hors branche énergie', 'Autres transports', 'Tertiaire'],
                'colors': ['#1fa22E', '#933588', '#fecf44', '#a74d47', '#cfd1d2', '#91d4f2', '#f29400']},
                {...},
                .
                .
                .
                ]
                "unit" : unité dans laquelle les valeurs de l'indicateurs sont exprimées
            }

        """
        if self.caracteristiques_indicateur["data_type"] != "climat":
            requete_filtres_categories = (
                await self._construire_requetes_filtres_categories(filtres_categorie)
            )
            unite = self.caracteristiques_indicateur["unit"]
            graphiques = self.caracteristiques_indicateur["charts"]
            having_sum = ""
            if (
                not self.afficher_indicateur_maille
                and provenance not in "tableau_de_bord_restitue"
            ):
                return {"charts": []}
            # Il faut récupérer le nom de la colonne de la maille pour ensuite la proposer dans la CTE **final**
            # afin qu'elle soit disponible dans la requête finale
            ratio = self.ratio.replace("maille.", "")
            if self.annee:
                requete_annee = "where a.annee = {annee}".format(annee=self.annee)
            nombre_decimales = self.caracteristiques_indicateur["decimals"]

            filtre_inf = ""
            filtre_sup = ""
            #  S'il faut combiner plusieurs représentations carto, cela signifie qu'il est possible d'ajouter
            #  un filtre sur les valeurs d'une colonne qui est en fait un intervalle de valeurs.
            #  S'il est nécessaire de combiner deux représentations distinctes
            if self.caracteristiques_indicateur["data_deuxieme_representation"]:
                filtre_inf = self._construction_filtre_valeur(filtre)["inf"]
                filtre_sup = self._construction_filtre_valeur(filtre)["sup"]

            if self.caracteristiques_indicateur["type"] == "flow":
                having_sum = "having (sum(val) >= {filtre})".format(filtre=filtre)
            calcul = "{coeff}".format(coeff=self.coeff)
            if self.caracteristiques_indicateur["afficher_calcul_et_donnees_table"]:
                calcul = ""
            condition_valeur = "a.valeur"
            group_by = ""
            jointure_territoire = """
                                   join {schema}.territoire as b on a.commune = b.commune
                                   join {schema}.commune as commune on commune.code = b.commune
                                   join {schema}.{type_territoire} as {type_territoire} on {type_territoire}.code = b.code
                                   """.format(
                schema=self.schema, type_territoire=self.type_territoire
            )
            condition_territoire = """
                                   and b.type_territoire='{zone}'
                                   and b.code = '{code_insee_territoire}'
                                   """.format(
                zone=self.type_territoire, code_insee_territoire=self.code_territoire
            )
            if self.maille != "commune":
                jointure_territoire = """
                                      join {schema}.territoire as b on a.commune = b.commune
                                      join {schema}.{maille} as maille on maille.code = b.code
                                      join {schema}.territoire as d on a.commune = d.commune
                                      join {schema}.{type_territoire} as {type_territoire} on {type_territoire}.code = d.code
                                      """.format(
                    schema=self.schema,
                    maille=self.maille,
                    type_territoire=self.type_territoire,
                )
                condition_territoire = """
                                       and b.type_territoire = '{maille}'
                                       and d.type_territoire = '{zone}'
                                       and d.code = '{code_insee_territoire}'
                                       """.format(
                    maille=self.maille,
                    zone=self.type_territoire,
                    code_insee_territoire=self.code_territoire,
                )
            elif self.maille == "commune" and self.type_territoire == "commune":
                jointure_territoire = f"""join {self.schema}.commune as commune on commune.code = a.commune"""
                condition_territoire = """
                                       and a.commune = '{code_insee_territoire}'
                                       """.format(
                    maille=self.maille,
                    zone=self.type_territoire,
                    code_insee_territoire=self.code_territoire,
                )
            req = """
                  with tmp_final as (
                      with e_final as (
                          select * from meta.categorie where nom = '{categorie}' and region = '{region}'
                      ),
                      final as (
                          select {condition_valeur} as val, a.{categorie}
                          from {schema}.{jeu_de_donnees} as a
                          {jointure_territoire}
                          {requete_annee}
                          {filtre_inf}
                          {filtre_sup}
                          {requete_filtres_categories}
                          {condition_territoire}
                          {group_by}
                      )
                      select round(cast(sum({unit_factor}*val{calcul}) as numeric), {nombre_decimales}) as val, e_final.modalite, e_final.couleur, e_final.modalite_id, e_final.ordre
                      from final
                      full outer join e_final on e_final.modalite_id = final.{categorie}
                      group by modalite, couleur, ordre, e_final.modalite_id
                      {having_sum}
                      order by ordre, modalite, couleur
                  )
                  select val, ef.modalite, ef.modalite_id, ef.couleur from tmp_final
                  full outer join (select * from meta.categorie where nom = '{categorie}' and region = '{region}') ef on ef.modalite_id = tmp_final.modalite_id
                  group by ef.modalite, ef.modalite_id, ef.couleur, tmp_final.val, ef.ordre
                  order by ef.ordre, ef.modalite, ef.modalite_id, ef.couleur;
                  """

            value_ratio = 1.0
            if (
                ratio
                and not self.caracteristiques_indicateur[
                    "afficher_calcul_et_donnees_table"
                ]
            ):
                ratio_table_name = self.ratio.split(".")[1]
                params = [self.maille, self.code_territoire, self.type_territoire]

                if self.annee:
                    if not self.ratio_best_year:
                        query_closest_year = f"""SELECT DISTINCT annee, ABS(annee - $1)
                            FROM {self.schema}.{ratio_table_name} ORDER BY ABS(annee - $1)"""
                        values_closest_year = await fetch(
                            query_closest_year, self.annee
                        )
                        best_year = values_closest_year[0]["annee"]
                        self.ratio_best_year = best_year

                    # we add the condition for year
                    query_year_condition = "AND ratio.annee = $4"
                    params.append(self.ratio_best_year)
                else:
                    query_year_condition = ""

                if self.maille == "commune" and self.type_territoire == "commune":
                    query_ratio = f"""SELECT sum(ratio.valeur) AS {ratio_table_name}
                                FROM {self.schema}.{ratio_table_name} ratio
                                WHERE ratio.commune = $2
                                    AND $1 = 'commune'
                                    AND $3 = 'commune'
                                    {query_year_condition};"""
                elif self.maille == "commune":
                    query_ratio = f"""SELECT sum(ratio.valeur) AS {ratio_table_name}
                                FROM {self.schema}.territoire tzone
                                FULL OUTER JOIN {self.schema}.{ratio_table_name} ratio ON ratio.commune = tzone.commune
                                WHERE tzone.code = $2
                                    AND tzone.type_territoire = $3
                                    AND $1 = 'commune'
                                    {query_year_condition};"""
                else:
                    query_ratio = f"""SELECT sum(ratio.valeur) AS {ratio_table_name}
                                FROM {self.schema}.territoire tzone
                                FULL OUTER JOIN {self.schema}.{self.type_territoire} zone ON zone.code = tzone.code
                                FULL OUTER JOIN {self.schema}.territoire tmaille ON tmaille.commune = tzone.commune
                                FULL OUTER JOIN {self.schema}.{ratio_table_name} ratio ON ratio.commune = tmaille.commune
                                WHERE tmaille.type_territoire = $1
                                    AND tzone.code = $2
                                    AND tzone.type_territoire = $3
                                    {query_year_condition};"""

                value_ratio_res = await fetch(query_ratio, *params)
                value_ratio_dict = [dict(x) for x in value_ratio_res]
                if len(value_ratio_dict) == 0:
                    logger.error(
                        f"""Aucune valeur trouvée dans la table {ratio_table_name} dans les conditions demandées."""
                    )
                    return []
                value_ratio = float(value_ratio_dict[0][ratio_table_name])

            liste_graphiques = []
            for graphique in graphiques:
                if not graphique:
                    return {"charts": []}

                if (
                    self.caracteristiques_indicateur["data_type"]
                    == "accessibilite_emploi"
                ):
                    condition_valeur = (
                        "sum(a.valeur * a.nbre_actifs) / sum(a.nbre_actifs) "
                    )
                    group_by = "group by a.{categorie}".format(
                        categorie=graphique["categorie"]
                    )

                req_finale = req.format(
                    identifiant_indicateur=self.identifiant_indicateur,
                    calcul=calcul,
                    jointure_territoire=jointure_territoire,
                    condition_territoire=condition_territoire,
                    nombre_decimales=nombre_decimales,
                    jeu_de_donnees=self.jeu_de_donnees,
                    categorie=graphique["categorie"],
                    requete_annee=requete_annee,
                    requete_filtres_categories=requete_filtres_categories,
                    filtre_inf=filtre_inf,
                    filtre_sup=filtre_sup,
                    maille=self.maille,
                    type_territoire=self.type_territoire,
                    zone=self.type_territoire,
                    code_insee_territoire=self.code_territoire,
                    region=self.region,
                    schema=self.schema,
                    having_sum=having_sum,
                    condition_valeur=condition_valeur,
                    group_by=group_by,
                    unit_factor=self.unit_factor,
                )

                valeurs_par_categorie = await fetch(req_finale)

                modalites_disponibles = (
                    await self._modalites_disponibles_pour_territoire_et_categorie(
                        graphique["categorie"]
                    )
                )
                modalites_disponibles = [x["modalite"] for x in modalites_disponibles]
                valeurs_par_categorie_liste_dict = [
                    dict(x)
                    for x in valeurs_par_categorie
                    if x["modalite"] and x["modalite"] in modalites_disponibles
                ]
                if ratio:
                    for x in valeurs_par_categorie_liste_dict:
                        if value_ratio == 0.0:
                            x["val"] = "NaN"
                        elif x["val"] is not None:
                            x["val"] = round(
                                self.unit_factor * float(x["val"]) / value_ratio,
                                nombre_decimales,
                            )

                liste_graphiques.append(
                    {
                        "indicateur": self.identifiant_indicateur,
                        "name": graphique["categorie"],
                        "data": [x["val"] for x in valeurs_par_categorie_liste_dict],
                        "labels": [
                            x["modalite"] for x in valeurs_par_categorie_liste_dict
                        ],
                        "colors": [
                            x["couleur"] for x in valeurs_par_categorie_liste_dict
                        ],
                    }
                )
            graphiques = {"charts": liste_graphiques}
            graphiques["unite"] = unite
            return graphiques

    async def donnees_finales(
        self, provenance, filtre, filtres_categorie, reinitialiser_filtres=False
    ):
        """Permet d'obtenir les données qu'on souhaite restituer à l'interface cartographique
        On y intègre les données relatives à la confidentialité des graphiques et des territoires

        Paramètres
        ----------
        filtre : entier
            Seuil qu'on définit au moyen d'une réglette et avec lequel on filtre les valeurs sur une colonne à spécifier
        filtres_categorie : dict
            Cf. documentation de la méthode _construire_requetes_filtres_categories
        """
        if (
            not self.afficher_indicateur_maille
            and provenance not in "tableau_de_bord_restitue"
        ):
            return {}
        if self.caracteristiques_indicateur["type"] == "wms_feed":
            return {"wms_feed": "success", "map": [], "charts": []}

        useful_initial_filters = {
            k: v
            for k, v in self.filtre_initial_categorie.items()
            if not k.startswith("____all_")
        }
        if reinitialiser_filtres:
            filtres_categorie = useful_initial_filters
        else:
            filtres_categorie = {**useful_initial_filters, **filtres_categorie}

        condition_confid = ""
        if (
            self.caracteristiques_indicateur["data_type"] != "climat"
            and self.caracteristiques_indicateur["charts"]
            and self.caracteristiques_indicateur["charts"][0] is not None
        ):
            condition_confid = await self._construction_conditions_confid(
                filtres_categorie
            )

        confid_apres_filtre = await self._confid_camembert_apres_filtre(
            condition_confid
        )
        donnees_territoire_confidentielles = await self.get_territorial_confid_rules(
            condition_confid
        )
        total_confid = await self._total_confidentiel(condition_confid)
        donnees_finales = {}
        confid = False

        donnees_graphiques = {"charts": []}
        if self.caracteristiques_indicateur["moyenne_ponderee"]:
            logger.info(
                f"Utilise DonneesPourRepresentationDiagrammeCategoriesIndependantes pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            donnees_pour_representations_graphiques = (
                DonneesPourRepresentationDiagrammeCategoriesIndependantes(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_graphiques = (
                await donnees_pour_representations_graphiques.obtenir_donnees(
                    provenance, filtres_categorie
                )
            )
        else:
            confid = await self._donnees_confidentielles_diagramme()
            if (
                confid["charts"] == "A"
            ):  #  Si la confidentialité est de type 'A', on désative tous les camemberts à l'application du moindre filtre
                for cle in self.caracteristiques_indicateur[
                    "filtre_initial_modalites_par_categorie"
                ]:
                    # skip case of custom filters
                    if (
                        cle.startswith("____all_")
                        or "____all_" + cle
                        in self.caracteristiques_indicateur[
                            "filtre_initial_modalites_par_categorie"
                        ]
                    ):
                        continue
                    if (
                        cle in filtres_categorie.keys()
                    ):  #  Comparaison de deux dictionnaires, donc on regarde si la clé de l'un existe bien dans l'autre
                        #  Si une modalité a été décochée
                        if set(
                            [x["filtre_categorie"] for x in filtres_categorie[cle]]
                        ) != set(
                            [
                                x["filtre_categorie"]
                                for x in self.caracteristiques_indicateur[
                                    "filtre_initial_modalites_par_categorie"
                                ][cle]
                            ]
                        ):
                            confid = {"charts": "D"}
                    else:
                        confid = {"charts": "D"}

        if confid_apres_filtre:
            confid = {"charts": "D"}

        if self.caracteristiques_indicateur["concatenation"]:
            donnees_finales = {}
            donnees_finales["total"] = {"val": None, "divider": None}
        elif self.caracteristiques_indicateur["type"] == "pixels":
            logger.info(
                f"Utilise DonneesPourRepresentationPixels pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            donnees_pour_representations_graphiques = DonneesPourRepresentationPixels(
                self.caracteristiques_indicateur,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
            donnees_finales = (
                await donnees_pour_representations_graphiques.obtenir_donnees()
            )
        elif self.caracteristiques_indicateur["type"] == "pixels_cat":
            logger.info(
                f"Utilise DonneesPourRepresentationPixelsCategory pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            donnees_pour_representations_graphiques = (
                DonneesPourRepresentationPixelsCategory(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_finales = (
                await donnees_pour_representations_graphiques.obtenir_donnees()
            )
        elif self.caracteristiques_indicateur["type"] == "flow":
            logger.info(
                f"Utilise DonneesPourRepresentationCartoIndicateurFlux pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            donnees_pour_representation_carto = (
                DonneesPourRepresentationCartoIndicateurFlux(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_finales_indicateur = (
                await donnees_pour_representation_carto.obtenir_donnees(
                    provenance, filtre, filtres_categorie
                )
            )
            donnees_finales_indicateur["confid"] = confid
            donnees_finales_indicateur["filtre_initial_modalites_par_categorie"] = (
                self.filtre_initial_categorie
            )
            donnees_finales = donnees_finales_indicateur
        elif self.caracteristiques_indicateur["only_for_zone"]:
            if self.caracteristiques_indicateur["data_type"] == "climat":
                logger.info(
                    f"Utilise DonneesPourRepresentationCartoIndicateurClimat pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
                )
                donnees_pour_representation_carto = (
                    DonneesPourRepresentationCartoIndicateurClimat(
                        self.caracteristiques_indicateur,
                        self.maille,
                        self.type_territoire,
                        self.code_territoire,
                        self.annee,
                    )
                )
                donnees_finales_indicateur = (
                    await donnees_pour_representation_carto.obtenir_donnees(provenance)
                )
                donnees_finales = donnees_finales_indicateur
            else:
                logger.info(
                    f"Utilise DonneesPourRepresentationCartoIndicateurAutreMaille pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
                )
                donnees_pour_representation_carto = (
                    DonneesPourRepresentationCartoIndicateurAutreMaille(
                        self.caracteristiques_indicateur,
                        self.maille,
                        self.type_territoire,
                        self.code_territoire,
                        self.annee,
                    )
                )
                donnees_finales_indicateur = (
                    await donnees_pour_representation_carto.obtenir_donnees(provenance)
                )
                donnees_finales = donnees_finales_indicateur
        elif self.caracteristiques_indicateur["moyenne_ponderee"]:
            logger.info(
                f"Utilise DonneesPourRepresentationCartoMoyennePonderee pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            donnees_pour_representation_carto = (
                DonneesPourRepresentationCartoMoyennePonderee(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_finales_indicateur = (
                await donnees_pour_representation_carto.obtenir_donnees(
                    provenance, filtres_categorie
                )
            )
            donnees_finales_indicateur["confid"] = {"charts": "A"}
            donnees_finales_indicateur["filtre_initial_modalites_par_categorie"] = (
                self.filtre_initial_categorie
            )
            donnees_finales = donnees_finales_indicateur
        elif self.caracteristiques_indicateur["data_ratio"]:
            logger.info(
                f"Utilise DonneesPourRepresentationCartoIndicateurDataRatio pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            donnees_pour_representation_carto = (
                DonneesPourRepresentationCartoIndicateurDataRatio(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_carto = await donnees_pour_representation_carto.obtenir_donnees(
                provenance, filtres_categorie
            )
            donnees_carto["confid"] = confid
            donnees_carto["filtre_initial_modalites_par_categorie"] = (
                self.filtre_initial_categorie
            )
            donnees_finales = donnees_carto
        elif (
            self.caracteristiques_indicateur["data_deuxieme_representation"]
            or self.caracteristiques_indicateur["afficher_proportion"]
        ):
            #  S'il s'agit d'un indicateur avec combinaison de deux types de filtres (catégorie / modalités et seuil) et de deux représentations (cercles et aplats)
            logger.info(
                f"Utilise DonneesPourRepresentationCartoIndicateurFiltre pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            #  On instancie la classe qui permet d'obtenir les données pour ce type d'indicateurs.
            donnees_pour_representation_carto = (
                DonneesPourRepresentationCartoIndicateurFiltre(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_carto = await donnees_pour_representation_carto.obtenir_donnees(
                provenance, filtre, filtres_categorie
            )
            donnees_carto["confid"] = confid
            donnees_carto["filtre_initial_modalites_par_categorie"] = (
                self.filtre_initial_categorie
            )
            #  Pour le moment, la confidentialité ne concerne jamais ce genre d'indicateurs
            donnees_finales = donnees_carto
        else:
            logger.info(
                f"Utilise DonneesPourRepresentationCartoIndicateurSimple pour l'indicateur {self.caracteristiques_indicateur['nom']} sur le territoire {self.type_territoire} de code {self.code_territoire} et la maille {self.maille}"
            )
            #  Instanciation de la classe qui permet d'obtenir les données pour les autres types d'indicateurs (une représentation et un seul filtre).
            donnees_pour_representation_carto = (
                DonneesPourRepresentationCartoIndicateurSimple(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_carto = await donnees_pour_representation_carto.obtenir_donnees(
                provenance, filtres_categorie
            )
            if len(donnees_carto) == 0:
                return {}
            if donnees_territoire_confidentielles or confid:
                donnees_carto["map"] = self._apply_territorial_confid(
                    donnees_carto["map"], donnees_territoire_confidentielles
                )
                donnees_carto["charts"] = self._apply_diagram_confid(
                    donnees_carto["charts"], confid
                )
                donnees_carto["map"] = self._hide_confidential_values(
                    pd.DataFrame(donnees_carto["map"]), provenance
                )
                export_confid_rules = await self.get_territorial_confid_rules(
                    "", only_territories=False
                )
                donnees_carto["export"] = self._apply_territorial_confid(
                    donnees_carto["export"],
                    export_confid_rules,
                    merge_on_categories=True,
                )
                donnees_carto["export"] = self._hide_confidential_values(
                    pd.DataFrame(donnees_carto["export"]), provenance
                )
            min_max_valeurs_filtrees = self._min_max_valeurs(
                donnees_carto["map"], donnees_territoire_confidentielles
            )
            donnees_carto["min_max_valeurs"] = min_max_valeurs_filtrees
            donnees_carto["confid"] = confid
            donnees_carto["filtre_initial_modalites_par_categorie"] = (
                self.filtre_initial_categorie
            )
            if total_confid:
                donnees_carto["total"] = {"val": "confidentiel", "divider": None}
            donnees_finales = donnees_carto

        donnees_finales["filtre_initial"] = filtres_categorie
        donnees_finales["afficher_calcul_et_donnees_table"] = (
            self.caracteristiques_indicateur["afficher_calcul_et_donnees_table"]
        )
        donnees_finales["moyenne_ponderee"] = self.caracteristiques_indicateur[
            "moyenne_ponderee"
        ]

        df_confid = pd.DataFrame(
            await self.get_territorial_confid_rules(
                "and conditions->>'secteur' = '*' and conditions->>'energie' = '*'"
            )
        )
        df_confid["confidentiel"] = True
        if (
            len(donnees_finales.get("export", [])) > 0
            and "confidentiel" not in donnees_finales["export"][0]
            and not df_confid.empty
        ):
            export_df = pd.DataFrame(donnees_finales["export"])
            export_df = export_df.merge(
                df_confid, left_on="code", right_on="code", how="left"
            )
            export_df[["x", "y"]] = export_df[["x_x", "y_x"]]
            if "confidentiel_y" in export_df:
                export_df["confidentiel"] = export_df["confidentiel_y"].fillna(False)
            export_df.drop(
                ["x_y", "y_y", "x_x", "y_x", "confidentiel_y"], axis=1, inplace=True
            )
            export_df.loc[export_df["confidentiel"], "val"] = "confidentielle"
            donnees_finales["export"] = export_df.to_dict("records")

        if "charts" not in donnees_finales:
            donnees_finales["charts"] = donnees_graphiques["charts"]
        masquer_tout_camembert = False
        if (
            self.caracteristiques_indicateur["charts"]
            and self.caracteristiques_indicateur["charts"][0] is not None
            and "donnees_par_categorie" in donnees_finales
        ):
            masquer_tout_camembert = (
                await self._confid_camembert_apres_filtre_pourcentage_du_total(
                    donnees_finales["donnees_par_categorie"]
                )
            )

        if masquer_tout_camembert:
            donnees_finales["confid"] = {"charts": "D"}
            donnees_finales["total"] = {"val": "confidentiel", "divider": None}
        return donnees_finales


class DonneesPourRepresentationCartoIndicateurFiltre(DonneesPourRepresentations):
    """Cf. Documentation de la classe mère DonneesPourRepresentationCarto"""

    async def _min_max_seuil(self):
        """Retourne l'intervalle de valeurs entre lesquelles on peut filtrer sur une colonne.

        Pour les indicateurs dont les valeurs d'une colonne peuvent être filtrées par un seuil,
        il faut définir l'intervalle de valeurs entre lesquelles le filtre peut être sélectionné.
        On considère qu'on peut filtrer entre le min et le max des valeurs de la colonnes en question.

        Returns
        --------
        dict
            {
                "min" : valeur en-dessous de laquelle on ne peut plus faire descendre le filtre (généralement 0),
                "max" : valeur au-dessus de laquelle on ne peut plus faire descendre le filtre
            }
        """
        if self.caracteristiques_indicateur["data_deuxieme_representation"] is None:
            logger.error(
                f"Attention, le paramètre data_deuxieme_representation n'a pas été spécifié pour l'indicateur {self.jeu_de_donnees}. La colonne valeur sera donc utilisée par défaut"
            )
            valeurs_a_filtrer = "valeur"
        else:
            valeurs_a_filtrer = self.caracteristiques_indicateur[
                "data_deuxieme_representation"
            ]

        req_min_max = """
                    select min({valeurs_a_filtrer}) as min, max({valeurs_a_filtrer}) as max
                    from {schema}.{jeu_de_donnees};
                    """.format(
            valeurs_a_filtrer=valeurs_a_filtrer,
            jeu_de_donnees=self.jeu_de_donnees,
            schema=self.schema,
        )
        min_max = await fetch(req_min_max)
        return {"min": min_max[0]["min"], "max": min_max[0]["max"]}

    def _moyenne_proportion(self, df_valeurs_filtrees, df_valeurs_non_filtrees):
        """Retourne le pourcentage entre la somme des valeurs filtrées et non filtrées.

        Paramètres
        ----------
        df_valeurs_filtrees : DataFrama Pandas
            Cadre de données pandas issus des données retournées par _obtenir_donnees_par_territoire filtrées
        df_valeurs_non_filtrees : DataFrama Pandas
            Cadre de données pandas issus des données retournées par _obtenir_donnees_par_territoire non filtrées

        Returns
        --------
        Valeur réelle
            Pourcentage de la somme des valeurs filtrées par rapport à la somme des données non filtrées.
        """
        somme_valeurs_non_filtrees = df_valeurs_non_filtrees["val"].sum()
        somme_valeurs_filtrees = df_valeurs_filtrees["val"].sum()
        if somme_valeurs_filtrees == 0 or somme_valeurs_non_filtrees == 0:
            return 0
        return (float(somme_valeurs_filtrees) / float(somme_valeurs_non_filtrees)) * 100

    @staticmethod
    def _ajouter_pourcentage_des_donnees_non_filtrees(
        df_valeurs_filtrees, df_valeurs_non_filtrees
    ):
        """Retourne le même type d'objet que celui retourné par _donnees_par_territoire avec en plus le pourcentage des données filtrées par rapport à l'ensemble des données.

        df_valeurs_filtrees contient les même territoires que df_valeurs_non_filtrees.
        Si la valeur ("val") des données non filtrées est nan, alors une valeur nan dans les données filtrées est interprétée comme une donnée manquante.
        Si la valeur ("val") des données non filtrées n'est pas nan, alors une valeur nan dans les données filtrées est interprétée comme une valeur à 0.

        Paramètres
        ----------
        df_valeurs_filtrees : DataFrama Pandas
            Cadre de données pandas issus des données retournées par _obtenir_donnees_par_territoire filtrées
        df_valeurs_non_filtrees : DataFrama Pandas
            Cadre de données pandas issus des données retournées par _obtenir_donnees_par_territoire non filtrées

        Returns
        --------

        Dictionnaire
            Cf. structure dans la documentation _obtenir_donnees_par_territoire. Elle est identique à une valeur près
            qu'on ajoute à chaque dictionnaire de la liste associée à la clé "map" qui représente la proportion entre
            la valeur filtrée et non filtrée multipliée par 100 (pourcentage).
        """
        df_filtered_data_with_percentage = df_valeurs_non_filtrees.merge(
            df_valeurs_filtrees[["code", "val"]],
            how="left",
            on="code",
            suffixes=("_non_filtrees", "_filtrees"),
        )

        # A nan value in the filtered data is interpreted as a 0 if there is a value (not nan) in the unfiltered data and as a missing value otherwise.
        df_filtered_data_with_percentage.loc[
            ~df_filtered_data_with_percentage["val_non_filtrees"].isna(), "val_filtrees"
        ] = df_filtered_data_with_percentage.loc[
            ~df_filtered_data_with_percentage["val_non_filtrees"].isna(), "val_filtrees"
        ].fillna(
            0
        )

        # add a "val_applat" column that is the percentage of the filtered data compared to the non filtered data
        df_filtered_data_with_percentage["val_applats"] = (
            df_filtered_data_with_percentage["val_filtrees"]
            / df_filtered_data_with_percentage["val_non_filtrees"]
            * 100
        )
        df_filtered_data_with_percentage["val_applats"] = np.round(
            df_filtered_data_with_percentage["val_applats"].astype(float)
        )

        # Select only the desired columns and keep the filtered data as the 'val' column
        df_filtered_data_with_percentage = df_filtered_data_with_percentage[
            ["code", "nom", "x", "y", "val_filtrees", "val_applats"]
        ].rename(columns={"val_filtrees": "val"})

        # nan values are set to None (to have "null" values in the json response)
        df_filtered_data_with_percentage[["val", "val_applats"]] = (
            df_filtered_data_with_percentage[["val", "val_applats"]].replace(
                {np.nan: None}
            )
        )

        return df_filtered_data_with_percentage.to_dict("records")

    async def obtenir_donnees(self, provenance, filtre=0, filtres_categorie={}):
        """Retourne les données finales par territoire en ajoutant la confidentialité pour les graphiques et autres informations nécessaires

        Paramètres
        ----------

        filtre : entier
            valeur de seuil avec laquelle on filtre les valeurs d'une des colonnes de la table à partir de laquelle on construit l'indicateur
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------

        Dictionnaire :
            {"map": données retournées par _obtenir_donnees_par_territoire,
            confid: {'charts': Type de confidentialité 'A', 'B', 'C', ou 'D'},
            total: {'val': total, 'val_proportion': proportion pour le territoire sélectionné, 'divider': None},
            min_max_valeurs: {'min': minimum, 'max': maximum},
            bornes_filtre: {'min': minimum, 'max': maximum} Cf. documentation de la méthode _moyenne_proportion
            }
        """
        resultats_carte = []
        charts = {}
        liste_categories = self.obtenir_liste_categories()
        donnees_pour_export = []

        deuxieme_representation = resultats_carte
        total = await self._total(filtre, filtres_categorie)
        total_proportion = None

        if self.caracteristiques_indicateur["data_type"] == "accessibilite_emploi":
            resultats = await self._donnees_par_territoire(
                provenance, filtre, filtres_categorie
            )
            deuxieme_representation = self.obtenir_donnees_par_territoire(
                resultats, liste_categories
            )
            charts = await self.obtenir_donnees_par_categorie(
                resultats, liste_categories, total["total_ratio"]
            )
            donnees_pour_export = resultats

        # Le cas de l'indicateur domicile-travail
        if self.caracteristiques_indicateur["data_type"] != "accessibilite_emploi":
            resultats_carte = await self._donnees_par_territoire(provenance, 0, {})
            requete_filtres_categories = (
                await self._construire_requetes_filtres_categories(
                    filtres_categorie, "intitules"
                )
            )
            df = pd.DataFrame(resultats_carte)
            l_cols = [
                x
                for x in df.keys()
                if x != self.caracteristiques_indicateur["data_deuxieme_representation"]
            ]
            if self.caracteristiques_indicateur["afficher_proportion"]:
                if requete_filtres_categories != "":
                    df = df.query(requete_filtres_categories[3:])

                df = df[l_cols].drop_duplicates()
                valeurs_filtrees = df.to_dict("records")
            else:
                valeurs_filtrees = await self._donnees_par_territoire(
                    provenance, filtre, filtres_categorie
                )
                valeurs_filtrees_finales = self.obtenir_donnees_par_territoire(
                    pd.DataFrame(valeurs_filtrees), liste_categories
                )

            resultats_carte_finales = self.obtenir_donnees_par_territoire(
                pd.DataFrame(resultats_carte)[l_cols]
                .drop_duplicates()
                .to_dict("records"),
                liste_categories,
            )
            valeurs_filtrees_finales = self.obtenir_donnees_par_territoire(
                valeurs_filtrees, liste_categories
            )
            df_valeurs_filtrees = pd.DataFrame(valeurs_filtrees_finales)
            total = await self._total(filtre, filtres_categorie)
            total_proportion = self._moyenne_proportion(
                df_valeurs_filtrees, pd.DataFrame(resultats_carte)
            )
            deuxieme_representation = (
                self._ajouter_pourcentage_des_donnees_non_filtrees(
                    df_valeurs_filtrees, pd.DataFrame(resultats_carte_finales)
                )
            )
            donnees_pour_export = valeurs_filtrees
            charts = await self.obtenir_donnees_par_categorie(
                valeurs_filtrees, liste_categories, total["total_ratio"]
            )

        results = {
            "map": deuxieme_representation,
            "confid": {"charts": None},
            "charts": charts["charts"],
            "donnees_par_categorie": charts["donnees_par_categorie"],
            "export": donnees_pour_export,
        }
        results["total"] = {
            "val": round(
                self.unit_factor * total["total"],
                self.caracteristiques_indicateur["decimals"],
            ),
            "val_proportion": total_proportion,
            "divider": None,
        }
        min_max_filtre = await self._min_max_seuil()
        # Le cas de l'indicateur domicile-travail
        # Si la borne supérieure est au-dessus de 100 km,
        if (
            min_max_filtre["max"] >= 100
            and self.caracteristiques_indicateur["data_type"] != "accessibilite_emploi"
        ):
            # On fait comme si elle était égale à 100
            min_max_filtre["max"] = 100
        min_max_valeurs_filtrees = self._min_max_valeurs(results["map"], False)
        results["donnees_deuxieme_representation"] = deuxieme_representation

        results["min_max_valeurs"] = min_max_valeurs_filtrees
        results["bornes_filtre"] = min_max_filtre

        return results


class DonneesPourRepresentationCartoIndicateurSimple(DonneesPourRepresentations):
    async def obtenir_donnees(self, provenance, filtres_categorie={}):
        """Retourne les données finales par territoire en ajoutant la confidentialité pour les graphiques et autres informations nécessaires

        Paramètres
        ----------
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------

        Dictionnaire :
            {"map": données retournées par _obtenir_donnees_par_territoire,
            total: {'val': total, 'divider': None},
            min_max_valeurs: {'min': minimum, 'max': maximum}
            }
        """
        resultats_carte = await self._donnees_par_territoire(
            provenance, 0, filtres_categorie
        )
        if len(resultats_carte) == 0:
            return {}
        liste_categories = self.obtenir_liste_categories()
        total = await self._total(filtres_categorie=filtres_categorie)
        charts = await self.obtenir_donnees_par_categorie(
            resultats_carte, liste_categories, total["total_ratio"]
        )
        resultats_carte_finaux = self.obtenir_donnees_par_territoire(
            resultats_carte, liste_categories
        )

        # if we have a ratio, we have to divide the final value by the ratio parameter.
        if self.ratio != "":
            ratio_column_name = self.ratio.split(".")[-1]
            for res in resultats_carte:
                if ratio_column_name in res and res["val"] is not None:
                    if np.isnan(res[ratio_column_name]):
                        res[ratio_column_name] = 0
                    if res[ratio_column_name] != 0:
                        res["val"] = float(res["val"]) / res[ratio_column_name]
                    else:
                        res["val"] = "NaN"

        results = {
            "map": resultats_carte_finaux,
            "export": resultats_carte,
            "confid": {"charts": None},
            "charts": charts["charts"],
            "donnees_par_categorie": charts["donnees_par_categorie"],
        }

        if self.ratio_best_year and self.ratio_best_year != self.annee:
            results["ratio_best_year"] = self.ratio_best_year
        results["total"] = {
            "val": round(
                self.unit_factor * total["total"],
                self.caracteristiques_indicateur["decimals"],
            ),
            "divider": None,
        }
        return results


class DonneesPourRepresentationCartoIndicateurFlux(DonneesPourRepresentations):
    async def obtenir_donnees(self, provenance, filtre, filtres_categorie={}):
        """Retourne les données finales par territoire pour les données de flux (exemple : migrations pendulaires)

        Paramètres
        ----------
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------
        Dictionnaires :
            {"map": [{'val': 392.0, 'orig': [x_origine, y_origine], 'dest': [x_dest, y_dest], 'country_dest': '99999', 'lab_o': nom du territoire de départ, 'lab_d': Nom du territoire d'arrivée}, ..., {}],
            confid: {'charts': Type de confidentialité 'A', 'B', 'C', ou 'D'},
            total: {'val': total, 'divider': None},
            min_max_valeurs: {'min': minimum, 'max': maximum}
            }
        """
        maille = self.maille
        # Alias de la table dans laquelle on obtient les coordonnées de départ
        alias_orig = "commune_fr_premiere_jointure"
        # Alias de la table dans laquelle on obtient les coordonnées de destination
        alias_dest = "commune_fr_dest_premiere_jointure"
        # Initialement, on considère qu'on est à la maille commune. Ces deux variables changent de valeur en fonction
        # des différents cas.
        liste_categories = self.obtenir_liste_categories()
        (
            liste_conditions,
            liste_selections,
            liste_selection_finale,
            liste_categories_jointures,
            liste_categories_group_by,
            liste_condition_categorie_region,
        ) = ([] for i in range(6))
        for x in liste_categories:
            req_condition = " {categorie}.nom = '{categorie}'".format(categorie=x)
            req_selection = " {categorie}.modalite as {categorie}".format(categorie=x)
            req_selection_finale = " tmp.{categorie}".format(categorie=x)
            req_jointure = "join meta.categorie {categorie} on {categorie}.modalite_id = a.{categorie}".format(
                categorie=x
            )
            req_group_by = "{categorie}.modalite".format(categorie=x)
            req_condition_categorie_region = "{categorie}.region = '{region}'".format(
                categorie=x, region=self.region
            )
            liste_conditions.append(req_condition)
            liste_selections.append(req_selection)
            liste_selection_finale.append(req_selection_finale)
            liste_categories_jointures.append(req_jointure)
            liste_categories_group_by.append(req_group_by)
            liste_condition_categorie_region.append(req_condition_categorie_region)
        conditions_categorie = ""
        categories_selectionnees = ""
        selection_finale_categories = ""
        categories_group_by = ""
        categories_jointures = ""
        conditions_categorie_region = ""
        if liste_categories:
            conditions_categorie = "and" + " and ".join(liste_conditions)
            categories_selectionnees = ", " + ", ".join(liste_selections)
            selection_finale_categories = ", " + ", ".join(liste_selection_finale)
            categories_group_by = ", " + ", ".join(liste_categories_group_by)
            categories_jointures = " ".join(liste_categories_jointures)
            conditions_categorie_region = "and " + " and ".join(
                liste_condition_categorie_region
            )

        condition = ""
        jointures = ""
        requete_filtres_categories = await self._construire_requetes_filtres_categories(
            filtres_categorie
        )
        territoire_fr = False
        premieres_jointures = """
                              join {schema}.territoire t on t.commune = a.commune
                              join {schema}.territoire t_dest on t_dest.commune = a.commune_dest
                              """.format(
            schema=self.schema
        )

        if (
            self.maille == "commune"
            or self.maille == "epci"
            or self.maille == "departement"
        ):
            maille = self.maille + "_fr"
            # Ce paramètre indique si pour la maille sélectionnée, on dispose des données pour toute la France
            territoire_fr = True

        if (
            not territoire_fr
        ):  # Si on ne dispose pas des données géographiques de la maille sélectionnée pour toute la France,
            premieres_jointures = """
                                  join {schema}.territoire tmaille on a.commune = tmaille.commune
                                  join {schema}.territoire tmaille_dest on a.commune_dest = tmaille_dest.commune
                                  join {schema}.{maille} {maille} on {maille}.code = tmaille.code
                                  join {schema}.{maille} {maille}_dest on {maille}_dest.code = tmaille_dest.code
                                  """.format(
                schema=self.schema, maille=self.maille
            )
            condition = "where tmaille.type_territoire = '{maille}' and tmaille_dest.type_territoire = '{maille}'".format(
                maille=self.maille
            )

        if self.type_territoire == "region":
            alias_orig = "{maille}".format(maille=maille)
            alias_dest = "{maille}_dest".format(maille=maille)
            if territoire_fr:
                colonne_code = "commune"
                if self.maille == "commune":
                    colonne_code = "code"
                premieres_jointures = """
                                      join {schema}.{maille} as {maille} on {maille}.{colonne_code} = a.commune
                                      join {schema}.{maille} as {maille}_dest on {maille}_dest.{colonne_code} = a.commune_dest
                                      """.format(
                    schema=self.schema, maille=maille, colonne_code=colonne_code
                )

        elif self.type_territoire != "region" and self.maille == "commune":
            premieres_jointures = """
                                  join {schema}.territoire t on t.commune = a.commune
                                  """.format(
                schema=self.schema
            )
            condition = "where t.type_territoire = '{type_territoire}'  and (t.code = '{code}')".format(
                code=self.code_territoire, type_territoire=self.type_territoire
            )

        elif self.type_territoire != "region" and self.maille != "commune":
            alias_orig = "{maille}".format(maille=maille)
            alias_dest = "{maille}_dest".format(maille=maille)
            jointures = """
                        join {schema}.territoire as t on t.commune = a.commune
                        join {schema}.territoire as t_dest on a.commune_dest = t_dest.commune
                        """.format(
                schema=self.schema
            )
            condition = """
                        and tmaille.type_territoire = '{maille}'
                        and tmaille_dest.type_territoire = '{maille}'
                        and (t.code = '{code}' or t_dest.code = '{code}')
                        """.format(
                maille=maille, code=self.code_territoire
            )

            if territoire_fr:
                premieres_jointures = """
                                      join {schema}.{maille} as {maille} on a.commune = {maille}.commune
                                      join {schema}.{maille} as {maille}_dest on a.commune_dest = {maille}_dest.commune
                                      join {schema}.territoire t on t.commune = a.commune
                                      join {schema}.territoire t_dest on t_dest.commune = a.commune
                                      """.format(
                    maille=maille, schema=self.schema
                )

                jointures = """
                            join {schema}.{type_territoire} as {type_territoire} on {type_territoire}.code = t.code
                            join {schema}.{type_territoire} as {type_territoire}_dest on {type_territoire}_dest.code = t_dest.code
                            """.format(
                    type_territoire=self.type_territoire, schema=self.schema
                )

                condition = """
                            where t.type_territoire = '{type_territoire}'
                            and t_dest.type_territoire = '{type_territoire}'
                            and ({type_territoire}.code = '{code}' or {type_territoire}_dest.code = '{code}')
                            """.format(
                    type_territoire=self.type_territoire, code=self.code_territoire
                )

        req = """
              with tmp as (
              select round({unit_factor}*sum(a.valeur)) as val
              , ARRAY[{alias_orig}.x, {alias_orig}.y] as orig
              , ARRAY[{alias_dest}.x, {alias_dest}.y] as dest
              , a.country_dest
              , {alias_orig}.nom as lab_o
              , {alias_dest}.nom as lab_d
              , {alias_dest}.code as code_d
              {categories_selectionnees}
              from {schema}.{jeu_de_donnees} as a
              join {schema}.commune_fr as commune_fr_premiere_jointure on commune_fr_premiere_jointure.code = a.commune
              join {schema}.commune_fr as commune_fr_dest_premiere_jointure on commune_fr_dest_premiere_jointure.code = a.commune_dest
              {categories_jointures}
              {premieres_jointures}
              {jointures}
              {condition}
              {conditions_categorie}
              {conditions_categorie_region}
              {requete_filtres_categories}
              group by {alias_orig}.x, {alias_orig}.y, {alias_dest}.x, {alias_dest}.y, a.country_dest, {alias_orig}.nom, {alias_dest}.nom, {alias_dest}.code {categories_group_by}
              order by {alias_orig}.nom, {alias_dest}.nom, {alias_dest}.code
              )
              select sum(tmp.val) as val, tmp.orig, tmp.dest, tmp.lab_o, tmp.lab_d, tmp.code_d {selection_finale_categories}
              from tmp
              join (
                    select sum(tmp.val) as val, tmp.orig, tmp.dest, tmp.lab_o, tmp.lab_d, tmp.code_d
                    from tmp
                    group by tmp.orig, tmp.dest, tmp.lab_o, tmp.lab_d, tmp.code_d
                    having(sum(tmp.val) >= {filtre})
              ) as tmp1
              on tmp1.orig = tmp.orig and tmp1.dest = tmp.dest and tmp1.lab_o = tmp.lab_o
              and tmp.lab_d = tmp1.lab_d and tmp1.code_d = tmp.code_d
              group by tmp.orig, tmp.dest, tmp.lab_o, tmp.lab_d, tmp.code_d {selection_finale_categories};
              """.format(
            schema=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
            premieres_jointures=premieres_jointures,
            jointures=jointures,
            condition=condition,
            alias_orig=alias_orig,
            alias_dest=alias_dest,
            requete_filtres_categories=requete_filtres_categories,
            categories_jointures=categories_jointures,
            conditions_categorie=conditions_categorie,
            categories_selectionnees=categories_selectionnees,
            categories_group_by=categories_group_by,
            conditions_categorie_region=conditions_categorie_region,
            selection_finale_categories=selection_finale_categories,
            filtre=filtre,
            unit_factor=self.unit_factor,
        )

        mapset = await fetch(req)

        liste_val = [x["val"] for x in mapset]
        total = await self._total(filtre, filtres_categorie)
        min_max_valeurs = {"min": 0, "max": 0}
        if len(liste_val) > 0:
            min_max_valeurs = {"min": min(liste_val), "max": max(liste_val)}
        mapset = [dict(x) for x in mapset]
        map = self.obtenir_donnees_par_territoire(mapset, liste_categories)
        charts = await self.obtenir_donnees_par_categorie(
            mapset, liste_categories, total["total_ratio"]
        )

        return {
            "map": map,
            "export": mapset,
            "min_max_valeurs": min_max_valeurs,
            "total": {
                "val": round(
                    self.unit_factor * total["total"],
                    self.caracteristiques_indicateur["decimals"],
                ),
                "divider": None,
            },
            "charts": charts["charts"],
            "donnees_par_categorie": charts["donnees_par_categorie"],
        }


class DonneesPourRepresentationCartoIndicateurDataRatio(DonneesPourRepresentations):
    """Retourne les données spécifiquement structurées pour les indicateurs qui consistent à diviser un indicateur par un autre

    Cf. Documentation des attributs de la classe mère Representations.
    À faire : définir deux attributs supplémentaires :
    - Les données associées à l'indicateur courant,
    - Les données associées à l'indicateur par lequel on divise
    Puis factoriser ensuite la gestion de la confidentialité.
    """

    async def obtenir_donnees(self, provenance, filtres_categorie={}):
        """Retourne les données pour les indicateurs de type 'data_ratio'

        Returns
        --------
        donnees_carto : dict
            {"map": données retournées par _obtenir_donnees_par_territoire,
            confid: {'charts': Type de confidentialité 'A', 'B', 'C', ou 'D'},
            total: {'val': total, 'divider': None},
            min_max_valeurs: {'min': minimum, 'max': maximum}
            }
        """
        nombre_decimales = self.caracteristiques_indicateur["decimals"]
        id_indicateur_ratio = self.caracteristiques_indicateur["data_ratio"]
        donnees_pour_representation_carto = (
            DonneesPourRepresentationCartoIndicateurSimple(
                self.caracteristiques_indicateur,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
        )
        donnees_carto = await donnees_pour_representation_carto.obtenir_donnees(
            provenance, filtres_categorie
        )
        indicateur_ratio = IndicateurTableauBord(self.region, id_indicateur_ratio)
        caracteristiques_indicateur_ratio = (
            await indicateur_ratio.meta_donnees_indicateur()
        )
        donnees_representation_carto_ratio = (
            DonneesPourRepresentationCartoIndicateurSimple(
                caracteristiques_indicateur_ratio,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
        )
        donnees_carto_ratio = await donnees_representation_carto_ratio.obtenir_donnees(
            provenance
        )
        condition_confid = (
            "and conditions->>'secteur' = '*' and conditions->>'energie' = '*'"
        )

        donnees_territoire_confidentielles_ratio = (
            await donnees_representation_carto_ratio.get_territorial_confid_rules(
                condition_confid
            )
        )

        tot = await donnees_pour_representation_carto._total()
        tot_ratio = await donnees_representation_carto_ratio._total()
        total_ratio = 0
        tot["total"] *= self.unit_factor

        if tot_ratio["total"] != 0:
            total_ratio = round(
                tot["total"] / tot_ratio["total"] * 100, nombre_decimales
            )

        total_quotient = tot_ratio["total"]

        if not self.afficher_indicateur_maille:
            donnees_carto["map"] = []
            donnees_carto["total"] = {"val": total_ratio, "divider": total_quotient}
            return donnees_carto

        if donnees_territoire_confidentielles_ratio:
            donnees_carto_ratio["map"] = (
                donnees_representation_carto_ratio._apply_territorial_confid(
                    donnees_carto_ratio["map"], donnees_territoire_confidentielles_ratio
                )
            )

        df = pd.DataFrame(donnees_carto["map"])

        df_ratio = pd.DataFrame(donnees_carto_ratio["map"])
        jointure_df_final = df.merge(df_ratio, on="code", how="outer")

        if "confidentiel" not in list(jointure_df_final):
            jointure_df_final["confidentiel"] = False

        jointure_df_final["val"] = (
            jointure_df_final["val_x"].astype(float)
            / jointure_df_final["val_y"].astype(float)
            * 100
        )
        jointure_df_final = jointure_df_final.replace([np.inf, -np.inf], 0)

        jointure_df_final["val"] = jointure_df_final["val"].astype(float)
        jointure_df_final["x"] = jointure_df_final["x_x"]
        jointure_df_final["nom"] = jointure_df_final["nom_x"]
        jointure_df_final["y"] = jointure_df_final["y_y"]
        jointure_df_final["val"] = jointure_df_final["val"].round(nombre_decimales)
        jointure_df_final = pd.DataFrame(
            self._hide_confidential_values(jointure_df_final, provenance)
        )

        donnees_carto_ratio_finales = (
            jointure_df_final[["code", "nom", "val", "x", "y", "confidentiel"]]
            .dropna()
            .to_dict("records")
        )
        donnees_carto["map"] = donnees_carto_ratio_finales
        donnees_carto["export"] = donnees_carto_ratio_finales

        donnees_carto["total"] = {"val": total_ratio, "divider": total_quotient}

        return donnees_carto


class DonneesPourRepresentationCartoIndicateurAutreMaille(DonneesPourRepresentations):
    async def obtenir_donnees(self, provenance):
        ratio = self.ratio.replace("maille", self.maille)
        maille_la_plus_fine = self.caracteristiques_indicateur["only_for_zone"]
        # Dans le cas de plusieurs mailles disponibles, on prend la première
        if "," in maille_la_plus_fine:
            maille_la_plus_fine = maille_la_plus_fine.split(",")[0].strip()
        nombre_decimales = self.caracteristiques_indicateur["decimals"]
        if (
            not self.afficher_indicateur_maille
            and provenance not in "tableau_de_bord_restitue"
        ):
            return {
                "map": [],
                "confid": {"charts": None},
                "total": {
                    "val": "Cet indicateur n'est pas disponible à la maille sélectionnée",
                    "divider": None,
                },
            }

        # What additional columns are displayed in the popup when hovering the territory
        popup_columns = await fetch(
            """select value from meta.indicateur_representation_details
            where indicateur_id=$1 and name='popup_columns'""",
            self.identifiant_indicateur,
        )
        if popup_columns and popup_columns[0]["value"]:
            popup_columns = json.loads(popup_columns[0]["value"]).keys()
            # Adding a prefix in case of conflicting column names
            popup_columns = "".join(", a." + col for col in popup_columns)
        else:
            popup_columns = ""

        has_succeeded = False
        while not has_succeeded:
            try:
                req = """
                    select {maille_la_plus_fine}.code
                    , {maille_la_plus_fine}.nom
                    , trunc({maille_la_plus_fine}.x) as x
                    , trunc({maille_la_plus_fine}.y) as y
                    , round(cast(sum(a.valeur{coeff}{ratio}) / count(*) as numeric) ,{nombre_decimales}) as val
                    {popup_columns}
                    from {schema_region}.{jeu_de_donnees} as a
                    join {schema_region}.{maille_la_plus_fine} as {maille_la_plus_fine} on {maille_la_plus_fine}.code = a.{maille_la_plus_fine}
                    join {schema_region}.territoire as tmaille on tmaille.code = a.{maille_la_plus_fine}
                    join {schema_region}.territoire as tzone on tzone.commune = tmaille.commune
                    join {schema_region}.{type_territoire} maille_{type_territoire} on maille_{type_territoire}.code = tzone.code
                    where a.annee = {annee}
                    and tmaille.type_territoire = '{maille_la_plus_fine}'
                    and tzone.type_territoire = '{type_territoire}'
                    and tzone.code = '{code_territoire}'
                    group by {maille_la_plus_fine}.code,{maille_la_plus_fine}.x,{maille_la_plus_fine}.y,{maille_la_plus_fine}.nom {popup_columns}
                """.format(
                    maille_la_plus_fine=maille_la_plus_fine,
                    schema_region=self.schema,
                    type_territoire=self.type_territoire,
                    jeu_de_donnees=self.jeu_de_donnees,
                    coeff=self.coeff,
                    ratio=ratio,
                    annee=self.annee,
                    code_territoire=self.code_territoire,
                    nombre_decimales=nombre_decimales,
                    popup_columns=popup_columns,
                )

                valeurs_par_territoire = await fetch(req)
                has_succeeded = True
            except asyncpg.exceptions.UndefinedColumnError as e:
                if maille_la_plus_fine == "commune":
                    raise e
                maille_la_plus_fine = "commune"

        valeurs_par_territoire = [dict(x) for x in valeurs_par_territoire]
        resultats = {
            "map": valeurs_par_territoire,
            "confid": {"charts": None},
            "total": {"val": None, "divider": None},
            "export": valeurs_par_territoire,
        }
        return resultats


class DonneesPourRepresentationCartoMoyennePonderee(DonneesPourRepresentations):
    """Cette classe permet d'obtenir les données cartographiques (par territoire) pour un indicateur de type moyenne pondérée

    Cf. Documentation des attributs de la classe mère Representations.
    """

    async def obtenir_donnees(self, provenance, filtres_categorie):
        """Obtention des données moyenne pondérée (d'une colonne par l'autre)

        paramètres
        ----------
        provenance : str
            page depuis laquelle l'indicateur est lancé (ex : carto, suivi_energetique etc.)        )
        filtres_categorie : dictionnaire
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------
        Dictionnaire comprenant un tableau de valeurs par territoire
        """
        requete_filtres_categories_brutes = (
            await self._construire_requetes_filtres_categories(filtres_categorie)
        )
        requete_filtres_categories = "and (" + requete_filtres_categories_brutes + ")"
        if len(filtres_categorie.keys()) == 0:
            requete_filtres_categories = ""

        req = """
              select round((sum(a.valeur * c.population) / sum(c.population))::numeric, {nombre_decimales}) as val,
                  sum(a.valeur * c.population) as numerateur,
                  sum(c.population) as denominateur,
                  c.code, trunc(c.x) as x, trunc(c.y) as y, c.nom as nom
              from {schema}.{jeu_de_donnees} a
              join {schema}.commune c on c.code = a.commune
              join {schema}.territoire t on t.commune = c.code
              where t.code = '{code_insee_territoire}'
              and annee={annee}
              {requete_filtres_categories}
              group by c.code, c.x, c.y, c.nom;
              """

        if self.maille != "commune":
            req = """
                  select round((sum(a.valeur * c.population) / sum(c.population))::numeric, {nombre_decimales}) as val,
                                    sum(a.valeur * c.population) as numerateur,
                                    sum(c.population) as denominateur,
                                    a.type_categorie,
                                    t.code, trunc({maille}.x) as x, trunc({maille}.y) as y, {maille}.nom as nom
                  from {schema}.{jeu_de_donnees} a
                  join {schema}.commune c on c.code = a.commune
                  join {schema}.territoire t on t.commune = c.code
                  join {schema}.{maille} {maille} on {maille}.code = t.code
                  join {schema}.territoire ter on ter.commune = a.commune
                  join {schema}.{type_territoire} t_{type_territoire} on t_{type_territoire}.code = ter.code
                  where ter.code = '{code_insee_territoire}'
                  and annee={annee}
                  {requete_filtres_categories}
                  group by t.code, a.type_categorie, {maille}.x, {maille}.y, {maille}.nom;
                  """

        req_finale = req.format(
            jeu_de_donnees=self.jeu_de_donnees,
            schema=self.schema,
            code_insee_territoire=self.code_territoire,
            maille=self.maille,
            type_territoire=self.type_territoire,
            annee=self.annee,
            nombre_decimales=self.caracteristiques_indicateur["decimals"],
            requete_filtres_categories=requete_filtres_categories,
        )

        valeurs_par_territoire_brutes = await fetch(req_finale)
        valeurs_par_territoire = [dict(x) for x in valeurs_par_territoire_brutes]
        df_valeurs = pd.DataFrame(valeurs_par_territoire)
        tot_numerateur = df_valeurs["numerateur"].sum()
        tot_denominateur = df_valeurs["denominateur"].sum()
        df_valeurs = df_valeurs.drop(["numerateur", "denominateur"], axis=1)
        map_values = (
            df_valeurs[["code", "x", "y", "nom", "val"]]
            .groupby(["code", "x", "y", "nom"])
            .mean()
            .reset_index()
            .to_dict("records")
        )
        valeurs_par_territoire = df_valeurs.to_dict("records")
        resultats = {
            "map": map_values,
            "export": valeurs_par_territoire,
            "confid": {"charts": None},
        }
        resultats["total"] = {
            "val": round(
                tot_numerateur / tot_denominateur,
                self.caracteristiques_indicateur["decimals"],
            ),
            "divider": None,
        }
        return resultats


class DonneesPourRepresentationCartoIndicateurClimat(DonneesPourRepresentations):
    async def min_max_year(self):
        """Selection des années minimale et maximale de disponibilité de données climat"""

        req = """
                select min(annee) as min_year, max(annee) as max_year
                from {schema_region}.{jeu_de_donnees}
         """.format(
            schema_region=self.schema, jeu_de_donnees=self.jeu_de_donnees
        )
        min_max_ye = await fetch(req)
        min_max_ye = [dict(x) for x in min_max_ye]
        return min_max_ye

    async def define_periods(self, max_min_year):
        """Cette fonction permet de calculer les périodes ancienne et récente d'un jeu de données climat

        Parameters
        ----------
        max_min_year :
            [{min_year : annee_minimal, max_year : annee_maximal}]
        """
        trentaine = 30
        reduc_annee = 0
        if self.caracteristiques_indicateur["data"] == "data_enneigement":
            trentaine = 29
            reduc_annee = 1
        periode_recente_min = max_min_year[0]["max_year"] - trentaine
        periode_recente_max = max_min_year[0]["max_year"] - 1

        if periode_recente_min - trentaine > max_min_year[0]["min_year"]:
            periode_ancienne_min = periode_recente_min - trentaine - reduc_annee
            periode_ancienne_max = periode_recente_min - 1
        else:
            periode_ancienne_min = max_min_year[0]["min_year"]
            periode_ancienne_max = max_min_year[0]["min_year"] + 29

        periods = [
            {
                "periode_ancienne": [periode_ancienne_min, periode_ancienne_max],
                "periode_recente": [periode_recente_min, periode_recente_max],
            }
        ]

        return periods

    async def _regression_lineraire(self, df, explicative, expliquee):
        nombre_annees = len(df["annee"].unique())
        x = np.array(df[explicative])
        y = np.array(df[expliquee])
        x = x.reshape(-1, 1)
        y = y.reshape(-1, 1)
        reg = LinearRegression()
        reg.fit(x, y)
        return round(
            reg.coef_[0][0] * nombre_annees,
            self.caracteristiques_indicateur["decimals"],
        )

    async def obtenir_donnees(self, provenance):
        """
        Cette fonction permet de:
           - calculer l'évolution global de l'indicateur climat par territoire
           - retourner les données map de l'indicateur
           - retourner les données des stations de mesures
           - retourner les données charts par station sur une période allant de l'année minimale jusqu'à l'année maximal
        """
        nombre_decimales = self.caracteristiques_indicateur["decimals"]
        unite = self.caracteristiques_indicateur["unit"]
        graphique = self.caracteristiques_indicateur["charts"] or [None]
        min_max_year = await self.min_max_year()
        periodes = await self.define_periods(min_max_year)
        if (
            not self.afficher_indicateur_maille
            and provenance not in "tableau_de_bord_restitue"
        ):
            return {
                "map": [],
                "confid": {"charts": None},
                "total": {
                    "val": "Cet indicateur n'est pas disponible à la maille sélectionnée",
                    "divider": None,
                },
            }
        selection_agregat = "sum(a.valeur)"
        # L'Occitanie souhaite calculer la moyenne pour tous les indicateurs sauf les données de précipitations, nous calculons le total annuel moyen (cf. https://gitlab.com/terristory/terristory/-/issues/1914)
        if self.jeu_de_donnees == "data_enneigement" or (
            self.region == "occitanie" and self.jeu_de_donnees != "data_precipitation"
        ):
            selection_agregat = "avg(a.valeur)"

        sous_sous_req_ancien = """
                               select {selection_agregat} as valeur, a.station_id, a.nom, ancien.min_periode_ancienne,
                               ancien.max_periode_ancienne, ancien.min_periode_recente, ancien.max_periode_recente
                               from (
                                   select station_id, nom,
                                   case when max(annee) - 29 - 30 < min(annee)
                                   then min(annee)
                                   else max(annee) - 29 - 30
                                   end as min_periode_ancienne,
                                   case when max(annee) - 29 < min(annee)
                                   then min(annee) + 29 else max(annee) - 30
                                   end as max_periode_ancienne,
                                   max(annee) - 29 as min_periode_recente,
                                   max(annee) as max_periode_recente
                                   from {schema_region}.{jeu_de_donnees}
                                   group by station_id, nom
                                   ) ancien
                                join {schema_region}.{jeu_de_donnees} a on a.station_id = ancien.station_id
                                where a.annee between ancien.min_periode_ancienne and ancien.max_periode_ancienne
                                group by annee, a.station_id, a.nom, ancien.min_periode_ancienne,
                                ancien.max_periode_ancienne, ancien.min_periode_recente, ancien.max_periode_recente
                            """.format(
            schema_region=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
            selection_agregat=selection_agregat,
        )
        sous_sous_req_recent = """
                               select {selection_agregat} as valeur, a.station_id, a.nom, recent.min_periode_ancienne,
                               recent.max_periode_ancienne, recent.min_periode_recente, recent.max_periode_recente
                               from (
                                   select station_id, nom,
                                   case when max(annee) - 29 - 30 < min(annee)
                                   then min(annee)
                                   else max(annee) - 29 - 30
                                   end as min_periode_ancienne,
                                   case when max(annee) - 29 < min(annee)
                                   then min(annee) + 29 else max(annee) - 30
                                   end as max_periode_ancienne,
                                   max(annee) - 29 as min_periode_recente,
                                   max(annee) as max_periode_recente
                                   from {schema_region}.{jeu_de_donnees}
                                   group by station_id, nom
                                   ) recent
                                join {schema_region}.{jeu_de_donnees} a on a.station_id = recent.station_id
                                where a.annee between recent.min_periode_recente and recent.max_periode_recente
                                group by annee, a.station_id, a.nom, recent.min_periode_ancienne,
                                recent.max_periode_ancienne, recent.min_periode_recente, recent.max_periode_recente
                                """.format(
            schema_region=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
            selection_agregat=selection_agregat,
        )

        sous_req = """
                   select avg(f.valeur) as valeur, f.station_id, f.nom, f.min_periode_ancienne,
                   f.max_periode_ancienne, f.min_periode_recente, f.max_periode_recente, 2 as periode
                   FROM ({sous_sous_req_ancien}) as f
                   group by f.station_id, f.nom, f.min_periode_ancienne,
                   f.max_periode_ancienne, f.min_periode_recente, f.max_periode_recente
                   union all
                   select avg(f.valeur) as valeur, f.station_id, f.nom, f.min_periode_ancienne,
                   f.max_periode_ancienne, f.min_periode_recente, f.max_periode_recente, 1 as periode
                   FROM ({sous_sous_req_recent}) as f
                   group by f.station_id, f.nom, f.min_periode_ancienne,
                   f.max_periode_ancienne, f.min_periode_recente, f.max_periode_recente
                   """.format(
            sous_sous_req_ancien=sous_sous_req_ancien,
            sous_sous_req_recent=sous_sous_req_recent,
        )

        req = """
              select round(cast(valeur as numeric), {nombre_decimales}) as val,
              d.nom as nom_station, d.station_id, e.code_territoire as code,
              min_periode_ancienne, max_periode_ancienne,
              min_periode_recente, max_periode_recente,
              trunc({maille}.x) as x, trunc({maille}.y) as y, {maille}.nom
              FROM (SELECT nom, a.station_id, min_periode_ancienne, max_periode_ancienne,
              min_periode_recente, max_periode_recente,
              valeur - LEAD(valeur) OVER (PARTITION BY station_id order by periode) as valeur
              FROM (
              SELECT nom, valeur, station_id, min_periode_ancienne, max_periode_ancienne,
              min_periode_recente, max_periode_recente, periode,
              COUNT(*) OVER (PARTITION BY station_id, periode order by periode) AS cnt,
              ROW_NUMBER() OVER (PARTITION BY station_id, periode order by periode) AS rn
              FROM ({sous_req}) b) a) d
              join {schema_region}.mapping_{jeu_de_donnees} e on e.station_id = d.station_id
              join {schema_region}.{maille} on {maille}.code = e.code_territoire
              and d.valeur is not null;
              """.format(
            maille=self.maille,
            schema_region=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
            sous_req=sous_req,
            nombre_decimales=nombre_decimales,
        )

        stations_mesures = await get_stations_mesures(self.schema, self.jeu_de_donnees)
        valeurs_par_station_par_annee = await get_value_par_station(
            self.schema,
            self.jeu_de_donnees,
            stations_mesures,
            min_max_year[0]["min_year"],
            min_max_year[0]["max_year"],
        )
        resultats = {"map": None, "confid": {"charts": None}, "nb_valeurs": 0}
        resultats["unite"] = unite
        resultats["intervalle_temps"] = [
            min_max_year[0]["min_year"],
            min_max_year[0]["max_year"],
        ]
        resultats["total"] = {"val": None, "divider": None}
        resultats["stations_mesures"] = stations_mesures
        resultats["intervalles_temps_recent_ancien"] = periodes[0]
        resultats["charts"] = [
            {
                "indicateur": self.identifiant_indicateur,
                "name": graphique[0]["categorie"],
                "data": [valeurs_par_station_par_annee],
                "labels": list(
                    range(min_max_year[0]["min_year"], min_max_year[0]["max_year"] + 1)
                ),
            }
        ]
        if self.jeu_de_donnees != "data_temperature" or self.region == "occitanie":
            valeurs_par_territoire = await fetch(req)
            valeurs_par_territoire = [dict(x) for x in valeurs_par_territoire]
            resultats["map"] = valeurs_par_territoire
            resultats["nb_valeurs"] = len(
                pd.DataFrame(valeurs_par_territoire)["val"].unique()
            )
        else:
            req = """
                  select avg(a.valeur) as valeur, annee, a.nom as nom_station, d.station_id, d.code_territoire as code, trunc({maille}.x) as x , trunc({maille}.y) as y, {maille}.nom
                  from {schema_region}.{jeu_de_donnees} a
                  join {schema_region}.mapping_{jeu_de_donnees} d on d.station_id = a.station_id
                  join {schema_region}.{maille} {maille} on {maille}.code = d.code_territoire
                  group by a.nom, annee, d.station_id, d.code_territoire, {maille}.x, {maille}.y, {maille}.nom;
                  """.format(
                maille=self.maille,
                schema_region=self.schema,
                jeu_de_donnees=self.jeu_de_donnees,
            )
            valeurs_par_territoire_et_annee = await fetch(req)
            valeurs_par_territoire_et_annee = [
                dict(x) for x in valeurs_par_territoire_et_annee
            ]
            df_valeurs_par_territoire = pd.DataFrame(valeurs_par_territoire_et_annee)
            valeurs_regroupees_par_territoire = {}
            valeurs_finales_par_station = []
            for d in valeurs_par_territoire_et_annee:
                if d["nom_station"] not in valeurs_regroupees_par_territoire.keys():
                    valeurs_regroupees_par_territoire[d["nom_station"]] = []
                valeurs_regroupees_par_territoire[d["nom_station"]].append(d)

            for d in valeurs_regroupees_par_territoire:
                df = pd.DataFrame(valeurs_regroupees_par_territoire[d])
                coefficient_regression = await self._regression_lineraire(
                    df, "annee", "valeur"
                )
                valeurs_finales_par_station.append(
                    {"valeur": coefficient_regression, "nom_station": d}
                )
            df_valeurs_par_station = pd.DataFrame(valeurs_finales_par_station)
            df_valeurs_par_territoire = pd.DataFrame(valeurs_par_territoire_et_annee)
            df_final = df_valeurs_par_territoire.merge(
                df_valeurs_par_station, on="nom_station", how="inner"
            )
            df_final["val"] = df_final["valeur_y"]
            resultats["map"] = df_final.to_dict("records")
            resultats["nb_valeurs"] = len(df_final["val"].unique())
        return resultats


class DonneesPourRepresentationDiagramme(DonneesPourRepresentations):
    """Obtention des données formattées pour créer des diagrammes circulaires."""

    async def obtenir_donnees(self, provenance, filtre=0, filtres_categorie={}):
        """Retourne les données par catégories (nom de la catégorie, valeurs, libellés des modalités etc.)

        Paramètres
        ----------
        filtre : entier
            valeur de seuil avec laquelle on filtre les valeurs d'une des colonnes de la table à partir de laquelle on construit l'indicateur
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categorie
        """
        donnees = await self._donnees_par_categories(
            provenance, filtre, filtres_categorie
        )
        return donnees


class DonneesPourRepresentationDiagrammeCategoriesIndependantes(
    DonneesPourRepresentationDiagramme
):
    """Retourne les données par catégories pour les indicateurs pour lesquelles les catégories sont indépendantes les unes par rapport aux autres

    Parameters
    ----------
    Cf. Documentation des attributs de la classe mère Representations
    """

    async def obtenir_donnees(self, provenance, filtres_categorie):
        """
        Obtention des données moyenne pondérée (d'une colonne par l'autre) pour chaque catégorie

        paramètres
        ----------
        provenance : str
            page depuis laquelle l'indicateur est lancé (ex : carto, suivi_energetique etc.)        )
        filtres_categorie : dictionnaire
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------
        Dictionnaire comprenant un tableau de valeurs par catégorie
        """
        requete_filtres_categories_brutes = (
            await self._construire_requetes_filtres_categories(filtres_categorie)
        )
        requete_filtres_categories = "and (" + requete_filtres_categories_brutes + ")"
        if len(filtres_categorie.keys()) == 0:
            requete_filtres_categories = ""
        requete_annee = ""
        if self.annee:
            requete_annee = "and a.annee = {annee}".format(annee=self.annee)
        nombre_decimales = self.caracteristiques_indicateur["decimals"]
        # TODO: generalize this function?
        #       - only using population (what if another data should be used?)
        #       - still using geographical layers for population data
        req = """
              with e_final as (
                  select * from meta.categorie where nom = $2 and region = $1
              ),
              final as (
              select round((sum(a.valeur * c.population) / sum(c.population))::numeric, $4) as val, h.modalite_id, h.couleur
              from {schema}.{jeu_de_donnees} a
              join {schema}.commune c on c.code = a.commune
              join {schema}.territoire t on t.commune = c.code
              join meta.categorie h on h.modalite_id = a.modalite_id
              where h.region = $1
              {requete_annee}
              and h.nom = $2
              and t.code = $3
              and a.type_categorie = $2
              {requete_filtres_categories}
              group by h.modalite_id, h.couleur
              )
              select final.val, e_final.modalite, e_final.couleur, e_final.ordre
              from final
              full outer join e_final on e_final.modalite_id = final.modalite_id
              order by e_final.ordre;
              """

        liste_graphiques = []
        for graphique in self.caracteristiques_indicateur["charts"]:
            if not graphique:
                return {"charts": []}
            req_finale = req.format(
                coeff=self.coeff,
                jeu_de_donnees=self.jeu_de_donnees,
                categorie=graphique["categorie"],
                requete_annee=requete_annee,
                requete_filtres_categories=requete_filtres_categories,
                schema=self.schema,
            )

            valeurs_par_categorie = await fetch(
                req_finale,
                self.region,
                graphique["categorie"],
                self.code_territoire,
                nombre_decimales,
            )
            modalites_disponibles = (
                await self._modalites_disponibles_pour_territoire_et_categorie(
                    graphique["categorie"]
                )
            )
            modalites_disponibles = [x["modalite"] for x in modalites_disponibles]
            valeurs_par_categorie_liste_dict = [
                dict(x)
                for x in valeurs_par_categorie
                if x["modalite"] and x["modalite"] in modalites_disponibles
            ]
            liste_graphiques.append(
                {
                    "indicateur": self.identifiant_indicateur,
                    "name": graphique["categorie"],
                    "data": [x["val"] for x in valeurs_par_categorie_liste_dict],
                    "labels": [x["modalite"] for x in valeurs_par_categorie_liste_dict],
                    "colors": [x["couleur"] for x in valeurs_par_categorie_liste_dict],
                }
            )
        graphiques = {"charts": liste_graphiques}
        graphiques["unite"] = self.caracteristiques_indicateur["unit"]
        return graphiques


class DonneesPourRepresentationCourbesEmpilees(DonneesPourRepresentations):
    """Obtention des données formattées pour créer des diagrammes circulaires.

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.forced_years = None

    def force_years(self, years):
        self.forced_years = years

    async def _obtenir_donnees_brutes(self, categorie, filtres_categorie):
        params = [self.type_territoire, categorie, self.code_territoire, self.region]
        condition_filtre_categorie = await self._construire_requetes_filtres_categories(
            filtres_categorie
        )
        condition_specific_years = ""
        if self.forced_years is not None:
            params.append(self.forced_years)
            condition_specific_years = f" AND a.annee = ANY(${len(params)})"
        req = f"""
              select a.annee
              , c.modalite as nom
              , couleur
              , sum(valeur) as valeur
              from {self.schema}.{self.jeu_de_donnees} as a
              join {self.schema}.territoire as b on b.commune = a.commune
              join meta.categorie c on a.{categorie} = c.modalite_id
              where b.type_territoire = $1
              and c.nom = $2
              and b.code = $3
              and region = $4
              {condition_filtre_categorie}
              {condition_specific_years}
              group by a.annee, c.modalite, c.ordre, c.couleur
              order by a.annee, c.ordre
              """

        rset = await fetch(req, *params)
        data = [dict(x) for x in rset]
        if self.forced_years is not None:
            distinct_years = set(row["annee"] for row in data)
            # we add missing years with null values
            forced_years = set(self.forced_years)
            if distinct_years != forced_years:
                missing_years = forced_years.difference(distinct_years)
                for year in missing_years:
                    data.append(
                        {
                            "annee": year,
                            "nom": "",
                            "couleur": "#FFF",
                            "valeur": 0.0,
                        }
                    )

        return data

    async def _get_years_associations_for_ratio(self, ratio, distinct_years):
        # first we get the distinct years for self.schema ratio table
        query_distinct_years_ratio = f"""
                  SELECT DISTINCT annee
                  FROM {self.schema}.{ratio} r"""
        distinct_years_ratio = await fetch(query_distinct_years_ratio)
        distinct_years_ratio = [x["annee"] for x in distinct_years_ratio]

        # we find close year in distinct_years_ratio for each in distinct_years
        years_associations = {}
        for year in distinct_years:
            years_associations[year] = min(
                distinct_years_ratio, key=lambda x: abs(x - year)
            )
        years_to_use = set(years_associations.values())
        return years_associations, years_to_use

    async def _obtenir_total_ratio(
        self, ratio, distinct_years, years_associations, years_to_use
    ):
        """Calcule le total de la donnée par laquelle on divise une autre données

        Paramètres
        ----------
        ratio : str
            Nom de la table qui représente le ratio (ex : population, superficie etc.)

        Returns
        --------
        flottant (nombre par lequel on divise les valeurs d'une analyse)
        """
        condition_na = ""
        if (
            self.caracteristiques_indicateur["data"] == "prod_enr"
            and self.region == "nouvelle-aquitaine"
        ):
            if self.caracteristiques_indicateur["data_ratio"]:
                condition_na = "and ratio_filiere_enr is not null"

        # we specify main geographical parameters that will be sent to the query
        params = [
            self.type_territoire,
            self.code_territoire,
            self.maille,
            years_to_use,
        ]

        if self.maille == "commune":
            requete_total_ratio = f"""
                      SELECT annee, sum(r.valeur)
                      FROM {self.schema}.{ratio} r
                      JOIN {self.schema}.commune c ON c.code = r.commune
                      JOIN {self.schema}.territoire t on t.commune = c.code
                      WHERE t.type_territoire = $1
                      AND t.code = $2
                      AND $3 = 'commune' -- to avoid any issue with params sent to fetch function
                      AND r.annee = any($4)
                      {condition_na}
                      GROUP BY r.annee ;
                      """
        else:
            requete_total_ratio = f"""
                  SELECT annee, sum(r.valeur)
                  FROM {self.schema}.{ratio} r
                  JOIN {self.schema}.commune c ON c.code = r.commune
                  JOIN {self.schema}.territoire tmaille on tmaille.commune = c.code
                  JOIN {self.schema}.{self.maille} tmaille1 on tmaille1.code = tmaille.code
                  JOIN {self.schema}.territoire tzone on tzone.commune = c.code
                  JOIN {self.schema}.{self.type_territoire} {self.type_territoire} 
                  on {self.type_territoire}.code = tzone.code
                  WHERE tmaille.type_territoire = $3
                  AND tzone.type_territoire = $1
                  AND tzone.code = $2
                  AND r.annee = any($4)
                  {condition_na}
                  GROUP BY r.annee
                  """
        total_ratio = await fetch(requete_total_ratio, *params)
        total_ratio = {x["annee"]: x["sum"] for x in total_ratio}

        final_total = {}
        for year in distinct_years:
            final_total[year] = total_ratio[years_associations[year]]
        return pd.DataFrame.from_dict(final_total, orient="index", columns=["total"])

    async def obtenir_donnees(self, filtres_categorie):
        """Retourne les données formattées pour les courbes empilées.

        Returns
        --------
        dict
            Un dictionnaire avec deux clés : unite et analyse.
            La valeur associée à la clé analyse est en fait un dictionnaire
            qui contient les données formattées par catégorie.
        """
        coeff = self.coeff
        resultats = {}
        if not self.caracteristiques_indicateur["charts"]:
            return {"unite": self.caracteristiques_indicateur["unit"]}
        representation_details = self.caracteristiques_indicateur.get(
            "representation_details", {}
        )

        categories_concernees_par_confidentialite = (
            await self.get_columns_concerned_by_confid()
        )
        for cat in self.caracteristiques_indicateur["charts"]:
            years_associations = {}
            trajDonnees = await self._obtenir_donnees_brutes(
                cat["categorie"], filtres_categorie
            )
            traj_donnees_non_filtrees = await self._obtenir_donnees_brutes(
                cat["categorie"], {}
            )
            traj = pd.DataFrame(trajDonnees)
            if traj.empty:
                continue
            traj_donnees_non_filtrees = pd.DataFrame(traj_donnees_non_filtrees)
            traj["valeur"] = round(
                self.unit_factor * traj["valeur"],
                self.caracteristiques_indicateur["decimals"],
            )
            traj_donnees_non_filtrees["valeur"] = round(
                self.unit_factor * traj_donnees_non_filtrees["valeur"],
                self.caracteristiques_indicateur["decimals"],
            )
            if (
                self.caracteristiques_indicateur["isratio"]
                and not self.caracteristiques_indicateur["data_ratio"]
            ):
                ratio = self.ratio.replace("/maille.", "")
                distinct_years = traj["annee"].unique()
                (
                    years_associations,
                    years_to_use,
                ) = await self._get_years_associations_for_ratio(ratio, distinct_years)

                ratio_by_year = await self._obtenir_total_ratio(
                    ratio, distinct_years, years_associations, years_to_use
                )

                traj = traj.join(ratio_by_year, on="annee")
                traj["valeur"] = traj["valeur"] / traj["total"]

            # we apply the coeff which can both correspond to mul or div
            if coeff and self.coeff_details:
                traj["valeur"] *= float(self.coeff_details.get("mul", 1))
                if self.coeff_details.get("div", 1) != 0:
                    traj["valeur"] /= float(self.coeff_details.get("div", 1))

            traj = _zero_annee_manquant_a_gauche(traj)
            traj_donnees_non_filtrees = _zero_annee_manquant_a_gauche(
                traj_donnees_non_filtrees
            )
            if cat["categorie"] in categories_concernees_par_confidentialite:
                cat_confidentielle = await get_confidential_values(
                    self.region,
                    self.type_territoire,
                    self.code_territoire,
                    self.confid_layer,
                    cat["categorie"],
                    categories_concernees_par_confidentialite,
                )
                traj = _rendre_donnees_confidentielles(traj, cat_confidentielle)
                traj_donnees_non_filtrees = _rendre_donnees_confidentielles(
                    traj_donnees_non_filtrees, cat_confidentielle
                )
            if type(traj) == str:
                resultats[cat["categorie"]] = {
                    "traj": "Confidentiel",
                    "categories": "Confidentiel",
                    "evolution": False,
                }
            else:
                resultats[cat["categorie"]] = {
                    "traj": _graphe_format(traj),
                    "categories": _graphe_format(traj_donnees_non_filtrees),
                }

                # do we have to add a summary on evolution
                summary = representation_details.get(f"summary", False)
                if summary:
                    available_years = traj["annee"].unique()
                    min_year = min(available_years)
                    max_year = max(available_years)
                    ref_year = int(representation_details.get(f"ref_year", min_year))
                    resultats[cat["categorie"]]["evolution"] = {
                        "ref_year": int(ref_year),
                        "max_year": int(max_year),
                    }
                    # we add color information to metadata for current traj
                else:
                    resultats[cat["categorie"]]["evolution"] = False

            # if we had to estimate any year for ratio computation, send it
            if (
                self.caracteristiques_indicateur["isratio"]
                and not self.caracteristiques_indicateur["data_ratio"]
                and len(years_associations) > 0
            ):
                # find element in years_associations where key is different from value
                # and add it to results
                estimated_ratio_years = [
                    {"original": str(year), "used": str(estimate)}
                    for year, estimate in years_associations.items()
                    if estimate != year
                ]
                resultats[cat["categorie"]][
                    "estimated_years_for_ratio"
                ] = estimated_ratio_years
        resultats = {"analyse": resultats}
        resultats["unite"] = self.caracteristiques_indicateur["unit"]
        return resultats


class DonneesPourRepresentationJauge(DonneesPourRepresentations):
    """Obtention des données formattées pour créer des pictos proportionnels en forme du territoire sélectionné.

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    """

    def build_territory_boundaries_query(
        self,
        couleur_contour_picto: str,
        couleur_fond_picto: str,
        couleur_remplissage_picto: str,
        valeur: float,
        simplify: float = 0,
    ):
        geom_column = "geom"
        if simplify > 0:
            geom_column = f"ST_SimplifyPreserveTopology(geom , {simplify}) as geom"
        sql = f"""with
                -- On diminue la taille du polygon car on est en L93, et le texte ne pourra pas être aggrandi suffisament si on reste à cette échelle
                scaled_territoire as (
                    select st_scale(st_transform(geom, 2154), 0.01, 0.01) as geom
                    from {self.schema}.{self.type_territoire} where code = $1
                ),
                territoire as (
                    select {geom_column},
                    ST_YMax(geom) as ymax,
                    ST_YMin(geom) as ymin,
                    ST_XMax(geom) as xmax,
                    ST_XMin(geom) as xmin,
                    -- we compute the border size based on the envelope rectangle
                    (ST_XMax(geom) - ST_XMin(geom)) / 100 as border_size
                    from scaled_territoire
                ),
            -- On dessine un rectangle faisant le poucentage désiré de la hauteur du territoire
            square_over as (
                select ST_MakeEnvelope(xmin, ymin, xmax, $2 * (ymax - ymin) / 100 + ymin, 2154) as geom
                from territoire
            ),
            -- Intersection avec la geom du territoire pour ne récupérer que la bonne forme
            territoire_over as (
                select st_intersection(t.geom, s.geom) as geom, t.ymax, t.xmax, t.xmin, t.border_size
                from territoire t, square_over s
            ),
            -- Recalage en 0
            territoire_on_00 as (
                select ST_Translate(geom, -xmin, -ymax) as geom, border_size
                from territoire
            ),
            territoire_over_on_00 as (
                select ST_Translate(geom, -xmin, -ymax) as geom, border_size
                from territoire_over
            ),
            paths as (
                -- Terristoire complet
                select concat(
                    '<path d= "',
                    ST_AsSVG(st_transform(geom, 2154),0, 4), '" ',
                    'stroke="{couleur_contour_picto}" stroke-width="', border_size,'" fill="{couleur_fond_picto}" '
                    ' />') as path
                from territoire_on_00
                union all
                -- Pourcentage du territoire (calculé sur la hauteur, et non sur l'aire, ce n'est pas très gênant)
                select concat(
                    '<path d= "',
                    ST_AsSVG(st_transform(geom, 2154),0, 4), '" ',
                    'stroke="{couleur_contour_picto}" stroke-width="', border_size,'" fill="{couleur_remplissage_picto}" '
                    ' />')  as path
                from territoire_over_on_00
            ),
            paths_concat as (
                select array_to_string(array_agg(path),'') as path
                from paths
            )
            select concat(
                    '<svg height="200" width="200" preserveAspectRatio="xMidYMid meet" viewBox="0 0 '|| xmax - xmin ||' '|| ymax - ymin ||'">',
                    path,
                    '<text x="'|| (xmax - xmin) / 3 ||'" y="'|| (ymax - ymin) / 2 ||'" font-size="'|| (xmax-xmin) * 30 / 4011 ||'em" fill="#000000">{valeur} %</text>',
                    '</svg>') as jauge
            from paths_concat,
            territoire;

        """
        return sql

    async def obtenir_donnees(self):
        """Retourne les données formattées pour les jauges de territoires.

        Returns
        -------
        dict
        """
        if not self.afficher_indicateur_maille:
            return {
                "disabled": True,
                "error": "Cet indicateur n'est pas disponible à la maille sélectionnée",
            }
        couleur_contour_picto = "#0571bf"
        couleur_fond_picto = "#b9e1fd"
        couleur_remplissage_picto = "#3dabfa"
        valeur_tot = await self._total()
        valeur = self.unit_factor * float(valeur_tot["total"])
        if self.caracteristiques_indicateur["data_ratio"]:
            indicateur_tableau_bord_ratio = IndicateurTableauBord(
                self.region, self.caracteristiques_indicateur["data_ratio"]
            )
            caracteristiques_indicateur_ratio = (
                await indicateur_tableau_bord_ratio.meta_donnees_indicateur()
            )
            ratio = DonneesPourRepresentations(
                caracteristiques_indicateur_ratio,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
            total_ratio = await ratio._total()
            valeur = round(
                valeur / total_ratio["total"] * 100,
                self.caracteristiques_indicateur["decimals"],
            )
        else:
            valeur = round(
                valeur,
                self.caracteristiques_indicateur["decimals"],
            )

        tolerance_steps = [0.0, 1.0, 5.0, 10.0]
        for tolerance_step in tolerance_steps:
            sql = self.build_territory_boundaries_query(
                couleur_contour_picto,
                couleur_fond_picto,
                couleur_remplissage_picto,
                valeur,
                tolerance_step,
            )
            res = await fetch(sql, self.code_territoire, valeur)
            if res:
                res = res[0]["jauge"]
            if len(res) <= 1500000:
                break

        return {"svg": res}


class DonneesPourRepresentationJaugeCirculaire(DonneesPourRepresentations):
    """Obtention des données formattées pour créer des proportionnels en forme des diagrammes semi circulaire.

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    """

    async def obtenir_donnees(self):
        """Retourne les données formattées (pourcentage) pour les jauges semi circulaire."""
        if not self.afficher_indicateur_maille:
            return {
                "disabled": True,
                "error": "Cet indicateur n'est pas disponible à la maille sélectionnée",
            }
        valeur = await self._total()
        if self.caracteristiques_indicateur["data_ratio"]:
            indicateur_tableau_bord_ratio = IndicateurTableauBord(
                self.region, self.caracteristiques_indicateur["data_ratio"]
            )
            caracteristiques_indicateur_ratio = (
                await indicateur_tableau_bord_ratio.meta_donnees_indicateur()
            )
            ratio = DonneesPourRepresentations(
                caracteristiques_indicateur_ratio,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
            total_ratio = await ratio._total()
            valeur = round(
                self.unit_factor * float(valeur["total"]) / total_ratio["total"] * 100,
                self.caracteristiques_indicateur["decimals"],
            )
        else:
            valeur = round(
                self.unit_factor * float(valeur["total"]),
                self.caracteristiques_indicateur["decimals"],
            )
        return valeur


class DonneesPourRepresentationHistogramme(DonneesPourRepresentations):
    """Obtention des données formatées pour créer des proportionnels en forme des histogrammes."""

    async def obtenir_donnees(self):
        """Retourne les données formattées pour l'histogramme cumulé."""
        unit = self.caracteristiques_indicateur["unit"]
        req_list_indicateurs = """
            select nom, data
            from meta.indicateur as i
            where i.id = ANY($1)
            """
        list_indicateurs = await fetch(
            req_list_indicateurs,
            tuple(self.caracteristiques_indicateur["concatenation"]),
        )

        category_metadata = await self._obtenir_modalites("secteur", "list")
        modalities_order = [c["modalite"] for c in category_metadata]
        modalities_colors = {c["modalite"]: c["couleur"] for c in category_metadata}

        dict_list_indicateurs = [dict(x) for x in list_indicateurs]
        list_air_polluants = {}
        for x in dict_list_indicateurs:
            # TOFIX: here, we use sector category and do not apply to relevant category
            req = """
                SELECT ROUND(CAST(SUM(i.valeur{coeff}) as numeric), $4) AS val, c.modalite, c.couleur
                FROM {schema}.{data} as i
                JOIN meta.categorie as c ON c.modalite_id = i.secteur
                JOIN {schema}.territoire t ON t.commune = i.commune
                WHERE c.nom = 'secteur'
                AND t.code = $1
                AND c.region = $2
                AND i.annee = $3
                GROUP BY c.modalite, c.couleur, c.ordre
                ORDER BY c.ordre
                """.format(
                schema=self.schema, data=x["data"], coeff=self.coeff
            )
            result = await fetch(
                req,
                self.code_territoire,
                self.region,
                self.annee,
                self.caracteristiques_indicateur["decimals"],
            )
            list_results = [dict(x) for x in result]
            modalities = [x["modalite"] for x in list_results]
            missing_modalities = set(modalities_order) - set(modalities)
            for modality in missing_modalities:
                list_results.append(
                    {
                        "val": 0.0,
                        "modalite": modality,
                        "couleur": str(modalities_colors.get(modality, "blue")),
                    }
                )

            # we save the order of the first indicator to use the same order everywhere
            list_results.sort(key=lambda x: modalities_order.index(x["modalite"]))
            list_air_polluants[x["nom"]] = list_results
        list_air_polluants["unit"] = unit
        return list_air_polluants


class DonneesPourRepresentationHistogrammeIndicateurUnique(DonneesPourRepresentations):
    """Obtention des données formatées pour créer des proportionnels en forme des histogrammes."""

    async def obtenir_donnees(self, filtre_categorie={}):
        """Retourne les données formatées d'un seul indicateur pour la construction d'un histogramme."""

        ratio = self.ratio.replace("maille", self.maille)
        if ratio != "":
            ratio = "/nullif({ratio}, 0)".format(ratio=ratio.replace("/", ""))
        requete_filtres_categories = await self._construire_requetes_filtres_categories(
            filtre_categorie
        )
        req = """
              select round(sum(cast(i.valeur{coeff}{ratio} as numeric)), {nombre_decimales}) as valeur, annee
              from {schema}.{jeu_de_donnees} i
              join {schema}.territoire t on t.commune = i.commune
              {requete_filtres_categories}
              and t.code = $1
              group by annee
              order by annee;
              """.format(
            coeff=self.coeff,
            ratio=ratio,
            nombre_decimales=self.caracteristiques_indicateur["decimals"],
            schema=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
            requete_filtres_categories=requete_filtres_categories,
        )
        resultats = await fetch(req, self.code_territoire)
        donnees = {"traj": [dict(x) for x in resultats]}
        for categorie in filtre_categorie:
            liste_modalites = (
                await self._modalites_disponibles_pour_territoire_et_categorie(
                    categorie
                )
            )
            donnees[categorie] = liste_modalites
        return donnees


class DonneesPourRepresentationHistogrammeDataRatio(DonneesPourRepresentations):
    """Obtention des données formatées pour créer des proportionnels en forme des histogrammes."""

    async def obtenir_donnees(self, provenance, filtre_categorie={}):
        id_indicateur_ratio = self.caracteristiques_indicateur["data_ratio"]
        donnees_representation_indicateur = (
            DonneesPourRepresentationHistogrammeIndicateurUnique(
                self.caracteristiques_indicateur,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
        )
        resultats_indicateur = await donnees_representation_indicateur.obtenir_donnees(
            filtre_categorie
        )

        indicateur_ratio = IndicateurTableauBord(self.region, id_indicateur_ratio)
        caracteristiques_indicateur_ratio = (
            await indicateur_ratio.meta_donnees_indicateur()
        )

        donnees_representation_indicateur_ratio = (
            DonneesPourRepresentationHistogrammeIndicateurUnique(
                caracteristiques_indicateur_ratio,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
        )
        resultats_indicateur_ratio = (
            await donnees_representation_indicateur_ratio.obtenir_donnees()
        )

        annees_indicateur = [x["annee"] for x in resultats_indicateur["traj"]]
        annees_indicateur_ratio = [
            x["annee"] for x in resultats_indicateur_ratio["traj"]
        ]

        annees_commune = sorted(
            list(set(annees_indicateur).intersection(set(annees_indicateur_ratio)))
        )
        if len(annees_indicateur) == 0:
            annees_commune = sorted(annees_indicateur_ratio)
        if len(annees_indicateur_ratio) == 0:
            annees_commune = sorted(annees_indicateur)

        resultats_indicateur["traj"] = [
            x for x in resultats_indicateur["traj"] if x["annee"] in annees_commune
        ]
        resultats_indicateur_ratio["traj"] = [
            x
            for x in resultats_indicateur_ratio["traj"]
            if x["annee"] in annees_commune
        ]

        resultats_indicateur["nom"] = self.caracteristiques_indicateur["nom"]
        resultats_indicateur["unite"] = self.caracteristiques_indicateur["unit"]
        resultats_indicateur_ratio["nom"] = caracteristiques_indicateur_ratio["nom"]
        resultats_indicateur_ratio["unite"] = caracteristiques_indicateur_ratio["unit"]

        return {
            "indicateur": resultats_indicateur,
            "indicateur_ratio": resultats_indicateur_ratio,
        }


class DonneesPourRepresentationCourbesHistoriques(DonneesPourRepresentations):
    """Obtention des données formatées pour créer des courbes historiques."""

    async def obtenir_donnees(self, provenance, filtre_categorie={}):
        associated_indicators = self.caracteristiques_indicateur["concatenation"]
        associated_indicators_through_data_ratio = self.caracteristiques_indicateur[
            "data_ratio"
        ]
        if not associated_indicators and not associated_indicators_through_data_ratio:
            raise ValueError(
                "Not able to perform historical lines on non-concatenated indicator."
            )
        # if we are handling data ratio indicator and not group of indicators
        elif associated_indicators_through_data_ratio and not associated_indicators:
            associated_indicators = [
                self.identifiant_indicateur,
                associated_indicators_through_data_ratio,
            ]
        results = {}
        years = self.caracteristiques_indicateur["years"]
        representation_details = self.caracteristiques_indicateur.get(
            "representation_details", {}
        )
        for indicator in associated_indicators:
            indicateur_pour_tableau_de_bord = IndicateurTableauBord(
                self.region, indicator
            )
            caracteristiques_indicateur = (
                await indicateur_pour_tableau_de_bord.meta_donnees_indicateur()
            )
            # we only need one category system to compute total
            if len(caracteristiques_indicateur["charts"]) > 0:
                caracteristiques_indicateur["charts"] = [
                    caracteristiques_indicateur["charts"][0]
                ]
            donnees_representation_indicateur = (
                DonneesPourRepresentationCourbesEmpilees(
                    caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            resultats_indicateur = (
                await donnees_representation_indicateur.obtenir_donnees(
                    filtre_categorie
                )
            )
            data = resultats_indicateur["analyse"]
            # we sum in one category
            if len(data.keys()) > 0:
                first_cat = list(data.keys())[0]
                total = resultats_indicateur["analyse"][first_cat]["traj"]
                # if confidential, we do not need anything
                if total == "Confidentiel":
                    resultats_indicateur["evolution"] = False
                    resultats_indicateur["confidentiel"] = True
                    resultats_indicateur["analyse"] = {}
                else:
                    # we compute traj
                    traj = {}
                    for cat in total:
                        for year_data in cat["data"]:
                            if year_data["annee"] in years:
                                traj[int(year_data["annee"])] = (
                                    traj.get(year_data["annee"], 0.0)
                                    + year_data["valeur"]
                                )
                    # do we have to add a summary on evolution
                    summary = representation_details.get(f"summary#{indicator}", False)
                    if summary:
                        min_year = min(traj.keys())
                        max_year = max(traj.keys())
                        ref_year = int(
                            representation_details.get(
                                f"ref_year#{indicator}", min_year
                            )
                        )
                        resultats_indicateur["evolution"] = {
                            "ref_year": ref_year,
                            "max_year": max_year,
                        }
                        # we add color information to metadata for current traj
                    else:
                        resultats_indicateur["evolution"] = False
                    resultats_indicateur["analyse"] = traj
                caracteristiques_indicateur["representation_details"]["color"] = (
                    representation_details.get(f"color#{indicator}", False)
                )
            else:
                resultats_indicateur["analyse"] = {}
            resultats_indicateur["meta"] = caracteristiques_indicateur
            results[indicator] = resultats_indicateur

        return results


class DonneesPourRepresentationMarqueurSVG(DonneesPourRepresentations):
    """Obtention des données formatées pour créer des Marqueurs SVG

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    """

    async def obtenir_donnees(self, filters):
        """Retourne les données formatées(Total)."""

        unite = self.caracteristiques_indicateur["unit"]
        if self.caracteristiques_indicateur["type"] == "wms_feed":
            return {"valeur": "Non définie", "unite": unite}

        valeur = await self._total(filtres_categorie=filters)
        if self.caracteristiques_indicateur["data_ratio"]:
            indicateur_tableau_bord_ratio = IndicateurTableauBord(
                self.region, self.caracteristiques_indicateur["data_ratio"]
            )
            caracteristiques_indicateur_ratio = (
                await indicateur_tableau_bord_ratio.meta_donnees_indicateur()
            )
            ratio = DonneesPourRepresentations(
                caracteristiques_indicateur_ratio,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
            total_ratio = await ratio._total()
            valeur["total"] = round(
                self.unit_factor * valeur["total"] / total_ratio["total"] * 100,
                self.caracteristiques_indicateur["decimals"],
            )
        else:
            valeur["total"] = round(
                self.unit_factor * valeur["total"],
                self.caracteristiques_indicateur["decimals"],
            )
        return {
            "valeur": valeur["total"],
            "unite": self.unit_params.get("unit_name", unite),
        }


class DonneesPourRepresentationPixels(DonneesPourRepresentations):
    """Obtention des données formatées pour une carte de pixels

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    """

    async def obtenir_donnees(self):
        """Retourne les données finales par territoire en ajoutant la confidentialité pour les graphiques et autres informations nécessaires

        Paramètres
        ----------

        filtre : entier
            valeur de seuil avec laquelle on filtre les valeurs d'une des colonnes de la table à partir de laquelle on construit l'indicateur
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------

        Dictionnaire :
            {"map": données retournées par _obtenir_donnees_par_territoire,
            confid: {'charts': Type de confidentialité 'A', 'B', 'C', ou 'D'},
            total: {'val': total, 'val_proportion': proportion pour le territoire sélectionné, 'divider': None},
            min_max_valeurs: {'min': minimum, 'max': maximum},
            bornes_filtre: {'min': minimum, 'max': maximum} Cf. documentation de la méthode _moyenne_proportion
            }
        """
        if not self.afficher_indicateur_maille:
            return {
                "map": [],
                "confid": {"charts": None},
                "total": {
                    "val": "Cet indicateur n'est pas disponible à la maille sélectionnée",
                    "divider": None,
                },
            }
        total = await self._total()

        # we give access to raw data
        req = """
              SELECT a.commune, a.valeur as val
              FROM {schema}.{jeu_de_donnees} as a
              LEFT JOIN {schema}.territoire as t ON t.commune = a.commune AND t.type_territoire = $2
              WHERE t.code = $1;
              """.format(
            nombre_decimales=self.caracteristiques_indicateur["decimals"],
            schema=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
            type_territoire=self.type_territoire,
        )
        res = await fetch(req, self.code_territoire, self.type_territoire)

        results = {
            "map": [dict(x) for x in res],
            "confid": {"charts": None},
            "charts": {},
            "donnees_par_categorie": {},
            "export": [],
        }
        results["total"] = {
            "val": total["total"],
            "val_proportion": 1.0,
            "divider": None,
        }
        min_max_valeurs_filtrees = await self.get_stats_on_indicator()
        results["min_max_valeurs"] = min_max_valeurs_filtrees

        return results

    async def get_stats_on_indicator(self):
        """Retourne les valeurs minimale et maximale de l'indicateur pour le territoire sélectionné

        Paramètres
        ----------
        resultats : dictionnaire
            Résultats retournés par _donnees_par_territoire de la classe fille DonneesPourRepresentationCarto

        Returns
        --------
        dictionnaire
            {"min" : valeur minimale, "max" : valeur maximale}

        """

        req = """
              SELECT MIN(valeur) as min, MAX(valeur) as max, COUNT(valeur) AS count
              FROM {schema}.{jeu_de_donnees} as a
              LEFT JOIN {schema}.territoire as t ON t.commune = a.commune AND t.type_territoire = $2
              WHERE t.code = $1;
              """.format(
            nombre_decimales=self.caracteristiques_indicateur["decimals"],
            schema=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
            type_territoire=self.type_territoire,
        )
        res = await fetch(req, self.code_territoire, self.type_territoire)
        return dict(res[0])

    async def _total_indicateur_zone_unique(self):
        """Retourne la valeur d'un indicateur disponible pour un seul type de territoire.

        Returns
        --------
            Réel : valeur de l'indicateur pour le territoire sélectionné
        """
        req = """
              SELECT round(cast(valeur as numeric), {nombre_decimales}) as val
              FROM {schema}.{jeu_de_donnees} as a
              LEFT JOIN {schema}.territoire as t 
              ON t.commune = a.commune 
              AND t.type_territoire = $2
              WHERE t.code = $1;
              """.format(
            nombre_decimales=self.caracteristiques_indicateur["decimals"],
            schema=self.schema,
            jeu_de_donnees=self.jeu_de_donnees,
        )
        res = await fetch(req, self.code_territoire, self.type_territoire)
        return res[0]["val"]


class DonneesPourRepresentationPixelsCategory(DonneesPourRepresentations):
    """Obtention des données formatées pour une carte de pixels qualitatifs

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    """

    async def obtenir_donnees(self):
        """Retourne les données finales par territoire en ajoutant la confidentialité pour les graphiques et autres informations nécessaires

        Paramètres
        ----------

        filtre : entier
            valeur de seuil avec laquelle on filtre les valeurs d'une des colonnes de la table à partir de laquelle on construit l'indicateur
        filtres_categorie :
            Cf. la documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------

        Dictionnaire :
            {"map": données retournées par _obtenir_donnees_par_territoire,
            confid: {'charts': Type de confidentialité 'A', 'B', 'C', ou 'D'},
            total: {'val': total, 'val_proportion': proportion pour le territoire sélectionné, 'divider': None},
            min_max_valeurs: {'min': minimum, 'max': maximum},
            bornes_filtre: {'min': minimum, 'max': maximum} Cf. documentation de la méthode _moyenne_proportion
            }
        """
        if not self.afficher_indicateur_maille:
            return {
                "map": [],
                "confid": {"charts": None},
                "total": {
                    "val": "Cet indicateur n'est pas disponible à la maille sélectionnée",
                    "divider": None,
                },
            }

        results = {
            "map": [],
            "confid": {"charts": None},
            "charts": {},
            "donnees_par_categorie": {},
            "export": [],
        }
        results["total"] = {}
        results["distinct_values"] = await self.get_distinct_values()

        return results

    async def get_distinct_values(self):
        """Retourne les valeurs distinctes des catégories pour le territoire sélectionné

        Paramètres
        ----------
        resultats : dictionnaire
            Résultats retournés par _donnees_par_territoire de la classe fille DonneesPourRepresentationCarto

        Returns
        --------
        dictionnaire
            {"min" : valeur minimale, "max" : valeur maximale}

        """

        req = """
              SELECT DISTINCT a.valeur, COUNT(*) AS count, c.modalite, c.couleur
              FROM {schema}.{jeu_de_donnees} as a
              LEFT JOIN {schema}.territoire as t ON t.commune = a.commune AND t.type_territoire = $2
              LEFT JOIN meta.categorie AS c ON c.modalite_id = a.valeur AND c.region = $3 AND c.nom = $4
              WHERE t.code = $1
              GROUP BY a.valeur, c.modalite, c.couleur;
              """.format(
            schema=self.schema, jeu_de_donnees=self.jeu_de_donnees
        )
        res = await fetch(
            req,
            self.code_territoire,
            self.type_territoire,
            self.region,
            self.jeu_de_donnees,
        )
        return [dict(x) for x in res]


class DonneesPourRepresentationNomSimple(DonneesPourRepresentations):
    """Obtention des données formatées pour créer des étiquettes avec le nom d'indicateurs

    Parameters
    ----------
    indicateur_tableau_bord : IndicateurTableauBord
        Objet IndicateurTableauBord dont les attributs permettent de définir l'indicateur à représenter
    maille : str
        Nom de la maille territoriale telle qu'écrite en base
    type_territoire : str
        Zone territoriale telle qu'écrite en base
    code_territoire : str
        Code du territoire
    """

    async def obtenir_donnees(self):
        """Retourne les données formatées(Nom, unité, id, total)."""

        id_indicateur = self.caracteristiques_indicateur["id"]
        nom = self.caracteristiques_indicateur["nom"]
        unite = self.caracteristiques_indicateur["unit"]
        if self.caracteristiques_indicateur["type"] in ("wms_feed", "choropleth_cat"):
            return {
                "valeur": "Non définie",
                "unite": unite,
                "id_indicateur": id_indicateur,
                "nom": nom,
            }

        valeur = await self._total()
        return {
            "valeur": self.unit_factor * valeur["total"],
            "unite": self.unit_params.get("unit_name", unite),
            "id_indicateur": id_indicateur,
            "nom": nom,
        }


class FabriqueRepresentations(Representations):
    """Instancie dynamiquement les classes filles de Representations
    En fonction de l'indicateur sélectionné par l'utilisateur.rice, on a besoin d'instancier la classe appropriée

    Parameters
    ----------

    Cf. Documentation de la classe mère Représentations.
    """

    @abstractmethod
    def donnees_finales(self):
        return NotImplemented


class FabriqueRepresentationsTableauBord(FabriqueRepresentations):
    """Instancie dynamiquement les représentations d'indicateurs pour les afficher dans les tableaux de bord

    Parameters
    ----------
    Cf. attributs de la classe mère
    representation : str
        Représentation sélectionnée par l'utilisateur.rice (courbes empilées, diagrammes circulaires, jauge etc.)

    """

    def __init__(
        self,
        indicateur_tableau_bord,
        maille,
        type_territoire,
        code_territoire,
        representation,
        **kwargs,
    ):
        super().__init__(
            indicateur_tableau_bord, maille, type_territoire, code_territoire, **kwargs
        )
        self.representation = representation

    async def donnees_finales(self, provenance, filtres_categorie={}):
        """Retourne les données restituées dans les tableaux de bord

        Paramètres
        ----------
        filtres_categorie : dict
            Cf. documentation de la méthode _construire_requetes_filtres_categories

        Returns
        --------

        Dictionnaire
            Données retournées par les méthodes obtenir_donnees associées à chaque classe fille instanciées dans cette méthode.
        """
        if self.representation == "line" or self.representation == "bar":
            logger.info("Use CourbesEmpilees for line or bar")
            donnees_representation_indicateur = (
                DonneesPourRepresentationCourbesEmpilees(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees(
                filtres_categorie
            )
        elif self.representation == "line-years" or self.representation == "bar-years":
            years = self.caracteristiques_indicateur.get("years", False)
            logger.info("Use CourbesEmpilees for line-years or bar-years")
            donnees_representation_indicateur = (
                DonneesPourRepresentationCourbesEmpilees(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            donnees_representation_indicateur.force_years(years)
            resultats = await donnees_representation_indicateur.obtenir_donnees(
                filtres_categorie
            )
        elif self.representation == "pie" or self.representation == "radar":
            confid = await self._donnees_confidentielles_diagramme()
            donnees_representation_indicateur = {}
            if self.caracteristiques_indicateur["moyenne_ponderee"]:
                logger.info(
                    "Use DonneesPourRepresentationDiagrammeCategoriesIndependantes for pie or radar"
                )
                donnees_representation_indicateur = (
                    DonneesPourRepresentationDiagrammeCategoriesIndependantes(
                        self.caracteristiques_indicateur,
                        self.maille,
                        self.type_territoire,
                        self.code_territoire,
                        self.annee,
                        self.unit_params,
                    )
                )
                resultats = await donnees_representation_indicateur.obtenir_donnees(
                    provenance, filtres_categorie
                )
            else:
                logger.info("Use DonneesPourRepresentationDiagramme for pie or radar")
                donnees_representation_indicateur = DonneesPourRepresentationDiagramme(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
                resultats = await donnees_representation_indicateur.obtenir_donnees(
                    provenance, 0, filtres_categorie
                )
            resultats["confid"] = confid
        elif self.representation == "jauge":
            logger.info("Use DonneesPourRepresentationJauge for jauge")
            donnees_representation_indicateur = DonneesPourRepresentationJauge(
                self.caracteristiques_indicateur,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
                self.unit_params,
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees()
        elif self.representation == "jauge-circulaire":
            logger.info(
                "Use DonneesPourRepresentationJaugeCirculaire for jauge-circulaire"
            )
            donnees_representation_indicateur = (
                DonneesPourRepresentationJaugeCirculaire(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees()
        elif (
            self.representation == "histogramme"
            or self.representation == "histogramme-normalise"
        ):
            logger.info(
                "Use DonneesPourRepresentationHistogramme for histogramme-normalise"
            )
            donnees_representation_indicateur = DonneesPourRepresentationHistogramme(
                self.caracteristiques_indicateur,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
                self.unit_params,
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees()
        elif self.representation == "histogramme-data-ratio":
            logger.info(
                "Use DonneesPourRepresentationHistogrammeDataRatio for histogramme-data-ratio"
            )
            donnees_representation_indicateur = (
                DonneesPourRepresentationHistogrammeDataRatio(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees(
                provenance, filtres_categorie
            )
        elif self.representation == "courbes-historiques":
            logger.info(
                "Use DonneesPourRepresentationCourbesHistoriques for courbes-historiques"
            )
            donnees_representation_indicateur = (
                DonneesPourRepresentationCourbesHistoriques(
                    self.caracteristiques_indicateur,
                    self.maille,
                    self.type_territoire,
                    self.code_territoire,
                    self.annee,
                    self.unit_params,
                )
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees(
                provenance, filtres_categorie
            )
        elif self.representation == "marqueur-svg":
            logger.info("Use DonneesPourRepresentationMarqueurSVG for marqueur-svg")
            donnees_representation_indicateur = DonneesPourRepresentationMarqueurSVG(
                self.caracteristiques_indicateur,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
                self.unit_params,
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees(
                filtres_categorie
            )
        elif self.representation == "analysis-launcher":
            logger.info("Use DonneesPourRepresentationNomSimple for analysis-launcher")
            donnees_representation_indicateur = DonneesPourRepresentationNomSimple(
                self.caracteristiques_indicateur,
                self.maille,
                self.type_territoire,
                self.code_territoire,
                self.annee,
            )
            resultats = await donnees_representation_indicateur.obtenir_donnees()
        else:
            raise ValueError("Invalid representation mode")
        return resultats

    @property
    def afficher_indicateur_maille(self):
        if self.caracteristiques_indicateur["disabled_for_macro_level"]:
            if "," in self.caracteristiques_indicateur["disabled_for_macro_level"]:
                territoire = (
                    self.caracteristiques_indicateur["disabled_for_macro_level"]
                ).split(",")
                for i in territoire:
                    if self.type_territoire == i:
                        return False
                return True
            elif (
                self.type_territoire
                == self.caracteristiques_indicateur["disabled_for_macro_level"]
            ):
                return False
        return True
