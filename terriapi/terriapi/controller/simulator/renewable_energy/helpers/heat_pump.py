# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import numpy as np


def get_production_value(B, label):
    """
    Retrieve the annual production value for a given label (aérothermie or géothermie).

    Parameters:
        B (dict): The data structure containing the production values.
        label (str): The label for the type of production (e.g., 'aérothermie', 'géothermie').

    Returns:
        float: The annual production value for the given label.
    """
    labels = B["7a"]["projects_data"][0]["labels"]
    production_values = B["7a"]["projects_data"][0]["production_annuelle"]

    if label in labels:
        index = labels.index(label)
        return production_values[index]
    else:
        raise ValueError(f"Label '{label}' not found in the data.")


def get_heat_pump(A: dict, B: dict) -> dict:
    """
    Calculate initial settings for heat pump categories (aérothermie and géothermie)
    based on specific and generic input parameters.

    Parameters:
        A (dict): Specific input parameters related to the territory.
        B (dict): actions parameters

    Returns:
        dict: A dictionary containing calculated consumption evolution values
              for aérothermie and géothermie.
    """
    heat_pump = {"aérothermie": {}, "géothermie": {}}

    # Retrieve the diagnosis year dynamically or fallback to NA
    diagnosis_year = A.get("pcaet-diagnosis-year", "NA")
    last_year = A.get("last-year-prod", "NA")
    default_target_year = 2030  # entered by user default 2030

    # Retrieve values for annual production
    annual_prod_aerothermal = get_production_value(
        B, "aérothermie"
    )  # Aérothermie production
    annual_prod_geothermal = get_production_value(
        B, "géothermie"
    )  # Géothermie production

    # Check for NA values in the PCAET production data
    aerothermal_2030 = (
        A.get("pcaet-production", {}).get("aerothermal-heat-pumps", {}).get(2030, "NA")
    )
    aerothermal_2050 = (
        A.get("pcaet-production", {}).get("aerothermal-heat-pumps", {}).get(2050, "NA")
    )

    geothermal_2030 = (
        A.get("pcaet-production", {}).get("geothermal-heat-pumps", {}).get(2030, "NA")
    )
    geothermal_2050 = (
        A.get("pcaet-production", {}).get("geothermal-heat-pumps", {}).get(2050, "NA")
    )

    # Define specific data for aérothermie
    aerothermie_data = {
        "current_year": A.get("prod-aerothermal-pac-max-year", 0),
        "year_n_1": A.get("prod-aerothermal-pac-max-year-1", 0),
        "year_n_2": A.get("prod-aerothermal-pac-max-year-2", 0),
        "year_n_3": A.get("prod-aerothermal-pac-max-year-3", 0),
        "pcaet_2030": aerothermal_2030,
        "pcaet_2050": aerothermal_2050,
    }

    # Define specific data for géothermie
    geothermie_data = {
        "current_year": A.get("prod-geothermal-pac-max-year", 0),
        "year_n_1": A.get("prod-geothermal-pac-max-year-1", 0),
        "year_n_2": A.get("prod-geothermal-pac-max-year-2", 0),
        "year_n_3": A.get("prod-geothermal-pac-max-year-3", 0),
        "pcaet_2030": geothermal_2030,
        "pcaet_2050": geothermal_2050,
    }

    # Helper function to compute historical rhythm
    def calculate_historical_rhythm(data):
        # Check if any of the required values are "NA"
        if "NA" in [
            data.get("current_year"),
            data.get("year_n_1"),
            data.get("year_n_2"),
            data.get("year_n_3"),
        ]:
            return "NA"

        # Calculate the production differences
        production_differences = [
            data["current_year"] - data["year_n_1"],
            data["year_n_1"] - data["year_n_2"],
            data["year_n_2"] - data["year_n_3"],
        ]

        # Return the average of the production differences
        return np.mean(production_differences)

    # Populate the dictionaries for aérothermie
    heat_pump["aérothermie"] = {
        "historical_rhythm": calculate_historical_rhythm(aerothermie_data),
        "target_rhythm": annual_prod_aerothermal
        / (default_target_year - last_year - 1),
        "pcaet-2030": aerothermie_data["pcaet_2030"],
        "pcaet-2050": aerothermie_data["pcaet_2050"],
        "last_year_data_availability": last_year,
    }

    # Populate the dictionaries for géothermie
    heat_pump["géothermie"] = {
        "historical_rhythm": calculate_historical_rhythm(geothermie_data),
        "target_rhythm": annual_prod_geothermal / (default_target_year - last_year - 1),
        "pcaet-2030": geothermie_data["pcaet_2030"],
        "pcaet-2050": geothermie_data["pcaet_2050"],
        "last_year_data_availability": last_year,
    }

    return heat_pump
