# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import numpy as np


def get_photovoltaic(A: dict, B: dict) -> dict:
    """
    Calculate initial settings for photovoltaic energy based on specific and generic input parameters.

    Parameters:
        A (dict): Specific input parameters related to the territory.
        B (dict): actions parameters

    Returns:
        dict: A dictionary containing calculated consumption evolution values for photovoltaic energy.
    """

    photovoltaic = {}

    # Retrieve the diagnosis year dynamically or fallback to NA
    diagnosis_year = A.get("pcaet-diagnosis-year", "NA")
    last_year = A.get("last-year-prod", "NA")
    default_target_year = 2030  # entered by user default 2030
    annual_prod = B["1a"]["projects_data"][0]["production_annuelle"]

    # Check for NA values in the PCAET production data
    photovoltaic_2030 = (
        A.get("pcaet-production", {}).get("solar-photovoltaic", {}).get(2030, "NA")
    )
    photovoltaic_2050 = (
        A.get("pcaet-production", {}).get("solar-photovoltaic", {}).get(2050, "NA")
    )

    # Calculate historical rhythm only if all data is available
    def calculate_historical_rhythm():
        if any(
            key not in A or A[key] == "NA"
            for key in [
                "prod-solar-pv-max-year",
                "prod-solar-pv-max-year-1",
                "prod-solar-pv-max-year-2",
                "prod-solar-pv-max-year-3",
            ]
        ):
            return "NA"

        production_difference_year_n_vs_year_n_1 = (
            A["prod-solar-pv-max-year"] - A["prod-solar-pv-max-year-1"]
        )
        production_difference_year_n_1_vs_year_n_2 = (
            A["prod-solar-pv-max-year-1"] - A["prod-solar-pv-max-year-2"]
        )
        production_difference_year_n_2_vs_year_n_3 = (
            A["prod-solar-pv-max-year-2"] - A["prod-solar-pv-max-year-3"]
        )

        # Calculate historical rhythm as the mean of the differences
        return np.mean(
            [
                production_difference_year_n_vs_year_n_1,
                production_difference_year_n_1_vs_year_n_2,
                production_difference_year_n_2_vs_year_n_3,
            ]
        )

    # Calculate the historical rhythm
    historical_rhythm = calculate_historical_rhythm()

    # Calculate the projected rhythm
    target_rhythm = annual_prod / (default_target_year - last_year - 1)

    # Analysis sum calculation only if the key exists and is valid
    analysis_sum = (
        A["solar-pv-no-max-year"]
        if "solar-pv-no-max-year" in A and A["solar-pv-no-max-year"] != "NA"
        else "NA"
    )

    # Populate the initial_setting dictionary
    photovoltaic["historical_rhythm"] = historical_rhythm
    photovoltaic["target_rhythm"] = target_rhythm
    photovoltaic["pcaet-2030"] = photovoltaic_2030
    photovoltaic["pcaet-2050"] = photovoltaic_2050
    photovoltaic["last_year_data_availability"] = last_year
    photovoltaic["analysis"] = {
        "name": "Potentiel PV sur toiture",
        "id": 28,
        "sum": analysis_sum,
        "unit": "GWh (potentiel maximum)",
    }

    return photovoltaic
