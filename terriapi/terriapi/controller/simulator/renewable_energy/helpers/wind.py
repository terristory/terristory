# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import numpy as np


def get_wind(A: dict, B: dict) -> dict:
    """
    Calculate initial settings for wind energy based on specific and generic input parameters.

    Parameters:
        A (dict): Specific input parameters related to the territory.
        B (dict): actions parameters

    Returns:
        dict: A dictionary containing calculated consumption evolution values for wind energy.
    """

    wind = {}

    # Retrieve the diagnosis year dynamically or fallback to NA
    diagnosis_year = A.get("pcaet-diagnosis-year", "NA")
    last_year = A.get("last-year-prod", "NA")
    default_target_year = 2030  # entered by user default 2030
    annual_prod = B["2a"]["projects_data"][0]["production_annuelle"]

    # Check for NA values in the PCAET production data
    wind_2030 = A.get("pcaet-production", {}).get("wind-energy", {}).get(2030, "NA")
    wind_2050 = A.get("pcaet-production", {}).get("wind-energy", {}).get(2050, "NA")

    # Calculate historical rhythm only if all data is available
    def calculate_historical_rhythm():
        if any(
            key not in A or A[key] == "NA"
            for key in [
                "prod-wind-max-year",
                "prod-wind-max-year-1",
                "prod-wind-max-year-2",
                "prod-wind-max-year-3",
            ]
        ):
            return "NA"

        production_difference_year_n_vs_year_n_1 = (
            A["prod-wind-max-year"] - A["prod-wind-max-year-1"]
        )
        production_difference_year_n_1_vs_year_n_2 = (
            A["prod-wind-max-year-1"] - A["prod-wind-max-year-2"]
        )
        production_difference_year_n_2_vs_year_n_3 = (
            A["prod-wind-max-year-2"] - A["prod-wind-max-year-3"]
        )

        # Calculate historical rhythm as the mean of the differences
        return np.mean(
            [
                production_difference_year_n_vs_year_n_1,
                production_difference_year_n_1_vs_year_n_2,
                production_difference_year_n_2_vs_year_n_3,
            ]
        )

    # Calculate the historical rhythm
    historical_rhythm = calculate_historical_rhythm()

    # Calculate the projected rhythm
    target_rhythm = annual_prod / (default_target_year - last_year - 1)

    # Populate the initial_setting dictionary
    wind["historical_rhythm"] = historical_rhythm
    wind["target_rhythm"] = target_rhythm
    wind["pcaet-2030"] = wind_2030
    wind["pcaet-2050"] = wind_2050
    wind["last_year_data_availability"] = last_year
    wind["analysis"] = {
        "name": "Potentiel d'implantation de l'éolien",
        "id": 25,
        "sum": A.get("wind-no-constraint-max-year", "NA"),
        "unit": "ha",
    }

    return wind
