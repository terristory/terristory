# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import numpy as np


def calculate_historical_rhythm(keys, A):
    """
    Calculate initial settings for methanization 4a.
    The function will calculate for each sub_action:
        historical_rhythm, pcaet-2030, pcaet-2050, target_rhythm, analysis, and last_year_data_availability.

    Parameters:
        A (dict): Specific input parameters related to the territory.
        settings (dict): Actions parameters.

    Returns:
        dict: A dictionary containing calculated consumption evolution values for '4a' (injection, cogeneration).
    """

    if any(key not in A or A[key] == "NA" for key in keys):
        return "NA"

    differences = [
        A[keys[0]] - A[keys[1]],
        A[keys[1]] - A[keys[2]],
        A[keys[2]] - A[keys[3]],
    ]
    return np.mean(differences)


def get_production_values_from_array(projects_data, labels):
    """
    Extract production values for each label from projects_data.
    """
    production_dict = {}
    for data_entry in projects_data:
        production_values = data_entry["production_annuelle"]
        for idx, label in enumerate(data_entry["labels"]):
            if label in labels:
                production_dict[label] = production_values[idx]
    return production_dict


def calculate_global_historical_rhythm(A):
    """
    Calculate the global historical rhythm using deviations for injection, cogeneration of electricity, and heat
    """

    # Calculation of deviations for injection, electricity, and heat
    ecart_injection_1 = (
        A["prod-biogas-injec-max-year"] - A["prod-biogas-injec-max-year-1"]
    )
    ecart_injection_2 = (
        A["prod-biogas-injec-max-year-1"] - A["prod-biogas-injec-max-year-2"]
    )
    ecart_injection_3 = (
        A["prod-biogas-injec-max-year-2"] - A["prod-biogas-injec-max-year-3"]
    )

    ecart_cogeneration_electricite_1 = (
        A["prod-biogas-elec-max-year"] - A["prod-biogas-elec-max-year-1"]
    )
    ecart_cogeneration_electricite_2 = (
        A["prod-biogas-elec-max-year-1"] - A["prod-biogas-elec-max-year-2"]
    )
    ecart_cogeneration_electricite_3 = (
        A["prod-biogas-elec-max-year-2"] - A["prod-biogas-elec-max-year-3"]
    )

    ecart_cogeneration_chaleur_1 = (
        A["prod-biogas-ther-max-year"] - A["prod-biogas-ther-max-year-1"]
    )
    ecart_cogeneration_chaleur_2 = (
        A["prod-biogas-ther-max-year-1"] - A["prod-biogas-ther-max-year-2"]
    )
    ecart_cogeneration_chaleur_3 = (
        A["prod-biogas-ther-max-year-2"] - A["prod-biogas-ther-max-year-3"]
    )

    # Calculation of historical rhythms
    rhythm_1 = (
        ecart_injection_1
        + ecart_cogeneration_electricite_1
        + ecart_cogeneration_chaleur_1
    )
    rhythm_2 = (
        ecart_injection_2
        + ecart_cogeneration_electricite_2
        + ecart_cogeneration_chaleur_2
    )
    rhythm_3 = (
        ecart_injection_3
        + ecart_cogeneration_electricite_3
        + ecart_cogeneration_chaleur_3
    )

    # Calculation of the average historical rhythm
    historical_rhythm = np.mean([rhythm_1, rhythm_2, rhythm_3])

    return historical_rhythm


def get_methanization(A, settings):
    """
    Calculate initial settings for methanization categories (injection and cogeneration) and their sub_action (electric and heat)
    based on specific and generic input parameters.

    Parameters:
        A (dict): Specific input parameters related to the territory.
        settings (dict): actions parameters

    Returns:
        dict: A dictionary containing calculated consumption evolution values.
    """

    methanization_results = {}
    production_sum_value = 0

    # Retrieve the diagnosis year and the last year of data availibility or fallback to NA
    diagnosis_year = A.get("pcaet-diagnosis-year", "NA")
    last_year = A.get("last-year-prod", "NA")
    default_target_year = 2030  # entered by user default 2030

    # PCAET Data
    pcaet_production = A.get("pcaet-production", {})

    # Iterate through the sub-actions of '4a'
    for sub_action_name, sub_action_data in settings["4a"]["sub_action"].items():
        methanization_results[sub_action_name] = {}

        if sub_action_name == "injection":
            # Historical rhythm for 'injection'
            keys = [
                "prod-biogas-injec-max-year",
                "prod-biogas-injec-max-year-1",
                "prod-biogas-injec-max-year-2",
                "prod-biogas-injec-max-year-3",
            ]
            historical_rhythm = calculate_historical_rhythm(keys, A)

            # Retrieve production_annuelle from projects_data
            production_value = sub_action_data["projects_data"][0][
                "production_annuelle"
            ]
            production_sum_value += production_value
            # Calculate the projected rhythm
            target_rhythm = (
                production_value / (default_target_year - last_year - 1)
                if last_year != "NA"
                else "NA"
            )

            # Retrieve PCAET values
            pcaet_2030 = pcaet_production.get("methanization", {}).get(2030, "NA")
            pcaet_2050 = pcaet_production.get("methanization", {}).get(2050, "NA")

            # Store the results
            methanization_results[sub_action_name] = {
                "type_display": "simple",
                "target_rhythm": target_rhythm,
                "historical_rhythm": historical_rhythm,
                "pcaet-2030": pcaet_2030,
                "pcaet-2050": pcaet_2050,
                "last_year_data_availability": last_year,
            }

        elif sub_action_name == "cogeneration":
            # Store the results by sub-action's production type (electricity, heat)
            methanization_results[sub_action_name] = {}

            # Historical rhythm for 'electricity'
            keys_elec = [
                "prod-biogas-elec-max-year",
                "prod-biogas-elec-max-year-1",
                "prod-biogas-elec-max-year-2",
                "prod-biogas-elec-max-year-3",
            ]
            historical_rhythm_elec = calculate_historical_rhythm(keys_elec, A)

            # Historical rate for 'heat'
            keys_ther = [
                "prod-biogas-ther-max-year",
                "prod-biogas-ther-max-year-1",
                "prod-biogas-ther-max-year-2",
                "prod-biogas-ther-max-year-3",
            ]
            historical_rhythm_ther = calculate_historical_rhythm(keys_ther, A)

            # Extract the production values from projects_data
            label_to_production = get_production_values_from_array(
                sub_action_data["projects_data"], ["électricité", "chaleur"]
            )

            # Calculate the projected rhythm
            for label in ["électricité", "chaleur"]:
                production_value = label_to_production.get(label, 0)
                production_sum_value += production_value
                target_rhythm = (
                    production_value / (default_target_year - last_year - 1)
                    if last_year != "NA"
                    else "NA"
                )

                historical_rhythm = (
                    historical_rhythm_elec
                    if label == "électricité"
                    else historical_rhythm_ther
                )

                # Retrieve PCAET values
                if label == "électricité":
                    pcaet_2030 = pcaet_production.get("electric-cogeneration", {}).get(
                        2030, "NA"
                    )
                    pcaet_2050 = pcaet_production.get("electric-cogeneration", {}).get(
                        2050, "NA"
                    )
                else:
                    pcaet_2030 = pcaet_production.get("thermal-cogeneration", {}).get(
                        2030, "NA"
                    )
                    pcaet_2050 = pcaet_production.get("thermal-cogeneration", {}).get(
                        2050, "NA"
                    )

                methanization_results[sub_action_name][label] = {
                    "target_rhythm": target_rhythm,
                    "historical_rhythm": historical_rhythm,
                    "pcaet-2030": pcaet_2030,
                    "pcaet-2050": pcaet_2050,
                    "last_year_data_availability": last_year,
                }

    # Calculate global rhythm
    historical_rhythm_global = calculate_global_historical_rhythm(A)
    target_rhythm_global = production_sum_value / (default_target_year - last_year - 1)

    # Analysis sum calculation only if the key exists and is valid
    analysis_sum = (
        A["methanization-max-year"]
        if "methanization-max-year" in A and A["methanization-max-year"] != "NA"
        else "NA"
    )

    # Store global results
    methanization_results["global"] = {
        "type_display": "simple",
        "historical_rhythm": historical_rhythm_global,
        "target_rhythm": target_rhythm_global,
        "last_year_data_availability": last_year,
        "analysis": {
            "name": "Potentiel de méthanisation",
            "sum": analysis_sum,
            "unit": "GWh",
        },
    }

    return methanization_results
