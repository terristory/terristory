# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


def get_initial_setting(A: dict) -> dict:
    """
    Calculate initial settings based on specific and generic input parameters.

    Parameters:
        A (dict): Specific input parameters related to the territory.

    Returns:
        dict: A dictionary containing calculated consumption evolution values.
    """

    initial_setting = {}

    # Retrieve the diagnosis year dynamically or fallback to 2015
    diagnosis_year = A.get("pcaet-diagnosis-year", 2015)

    # Retrieve last year of data availability
    last_year = A.get("last-year-prod", 2022)

    initial_setting["last_year_prod"] = last_year
    initial_setting["diagnosis_year"] = diagnosis_year

    # Calculate consumption evolution for non-pcaet fields
    try:
        energy_consumption_diagnosis_year = A.get(
            "energy-consumption-diagnosis-year", 0
        )
        energy_consumption_max_year = A.get("energy-consumption-max-year", 0)

        if energy_consumption_diagnosis_year == 0 or energy_consumption_max_year == 0:
            initial_setting["consumption-evolution-2015-last-year"] = "NA"
        else:
            consumption_evolution_2015_last_year = round(
                (energy_consumption_max_year - energy_consumption_diagnosis_year)
                / energy_consumption_diagnosis_year
                * 100
            )
            initial_setting["consumption-evolution-2015-last-year"] = (
                consumption_evolution_2015_last_year
            )
    except KeyError as e:
        raise KeyError(f"Missing required key in input parameters: {e}") from e
    except ZeroDivisionError:
        raise ZeroDivisionError(
            "Division by zero occurred in the calculations. Check the input data."
        )

    # If diagnosis_year is NA, return NA for pcaet
    if diagnosis_year == "NA":
        initial_setting.update(
            {
                "consumption-evolution-diagnosis-2030": "NA",
                "consumption-evolution-diagnosis-2050": "NA",
            }
        )
        initial_setting["diagnosis_year"] = 2015  # fallback to 2015
    else:
        # Calculate pcaet-related values only if diagnosis_year is valid
        try:
            pcaet_consumption = A.get("pcaet-consumption", {})
            start_year_consumption = pcaet_consumption.get(diagnosis_year, 0)

            if start_year_consumption == 0 or start_year_consumption == "NA":
                initial_setting["consumption-evolution-diagnosis-2030"] = "NA"
                initial_setting["consumption-evolution-diagnosis-2050"] = "NA"
            else:
                consumption_evolution_diagnosis_2030 = round(
                    (pcaet_consumption.get(2030, 0) - start_year_consumption)
                    / pcaet_consumption.get(diagnosis_year, 1)
                    * 100
                )

                consumption_evolution_diagnosis_2050 = round(
                    (pcaet_consumption.get(2050, 0) - start_year_consumption)
                    / pcaet_consumption.get(diagnosis_year, 1)
                    * 100
                )

                initial_setting["consumption-evolution-diagnosis-2030"] = (
                    consumption_evolution_diagnosis_2030
                )
                initial_setting["consumption-evolution-diagnosis-2050"] = (
                    consumption_evolution_diagnosis_2050
                )
        except KeyError as e:
            raise KeyError(
                f"Missing required key in pcaet-consumption data: {e}"
            ) from e
        except ZeroDivisionError:
            raise ZeroDivisionError(
                "Division by zero occurred in the pcaet-consumption calculations. Check the input data."
            )

    return initial_setting
