# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


import logging
from collections import defaultdict
from typing import Any, Dict

from terriapi import controller
from terriapi.controller.analyse import get_global_irradiation
from terriapi.controller.pcaet_ademe import get_pcaet_ademe
from terriapi.controller.simulator.renewable_energy.helpers import (
    heat_pump,
    hydraulic,
    initial_setting,
    methanization,
    photovoltaic,
    solar_thermal,
    solid_biomass,
    wind,
)
from terriapi.controller.simulator.simulator import Simulator

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class SimulatorRenewableEnergy(Simulator):
    """
    This class simulates renewable energy data for a selected region.
    """

    def get_default_settings(self, A, B):
        """
        Gets default settings for actions.
        Parameters
        ----------
            A : dict
                specific input parameters
            B : dict
                generic input paramateres

        Returns
        -------
            data : dict
        """
        settings = {
            "1a": {
                "display": "simple",
                "type": "elec",
                "color": "#156082",
                "max_year": A["last-year-renewable-elec-installed"],
                "parc_data": [
                    {
                        "production_annuelle": A["prod-solar-pv-max-year"],
                        "puissance_annuelle": A[
                            "renewable-elec-installed-power-photovoltaic-max-year"
                        ],
                    }
                ],
                "projects_data": [{"production_annuelle": 0, "puissance_annuelle": 0}],
                "bilan_data": [
                    {
                        "production_annuelle": A["prod-solar-pv-max-year"],
                        "puissance_annuelle": A[
                            "renewable-elec-installed-power-photovoltaic-max-year"
                        ],
                    }
                ],
            },
            "2a": {
                "display": "simple",
                "type": "elec",
                "color": "#E97132",
                "parc_data": [
                    {
                        "production_annuelle": A["prod-wind-max-year"],
                        "puissance_annuelle": A[
                            "renewable-elec-installed-power-wind-max-year"
                        ],
                    }
                ],
                "projects_data": [{"production_annuelle": 0, "puissance_annuelle": 0}],
                "bilan_data": [
                    {
                        "production_annuelle": A["prod-wind-max-year"],
                        "puissance_annuelle": A[
                            "renewable-elec-installed-power-wind-max-year"
                        ],
                    }
                ],
            },
            "3a": {
                "display": "simple",
                "color": "#196B24",
                "type": "elec",
                "parc_data": [
                    {
                        "production_annuelle": A["prod-hydro-high-max-year"]
                        + A["prod-hydro-low-max-year"],
                        "puissance_annuelle": A[
                            "renewable-elec-installed-power-hydroelectric-low-max-year"
                        ]
                        + A[
                            "renewable-elec-installed-power-hydroelectric-high-max-year"
                        ],
                    }
                ],
                "projects_data": [
                    {"production_annuelle": 0, "puissance_annuelle": 0},
                ],
                "bilan_data": [
                    {
                        "production_annuelle": A["prod-hydro-high-max-year"]
                        + A["prod-hydro-low-max-year"],
                        "puissance_annuelle": A[
                            "renewable-elec-installed-power-hydroelectric-low-max-year"
                        ]
                        + A[
                            "renewable-elec-installed-power-hydroelectric-high-max-year"
                        ],
                    }
                ],
            },
            "4a": {
                "display": "complex",
                "color": "#0F9ED5",
                "sub_action": {
                    "injection": {
                        "type_display": "simple",
                        "type": "heat",
                        "title": "Biogaz en injection",
                        "color": "#0D3A4E",
                        "parc_data": [
                            {"production_annuelle": A["prod-biogas-injec-max-year"]}
                        ],
                        "projects_data": [{"production_annuelle": 0}],
                        "bilan_data": [
                            {"production_annuelle": A["prod-biogas-injec-max-year"]}
                        ],
                    },
                    "cogeneration": {
                        "type_display": "complex",
                        "title": "Biogaz en cogénération",
                        "parc_data": [
                            {
                                "production_annuelle": [
                                    A["prod-biogas-elec-max-year"],
                                    A["prod-biogas-ther-max-year"],
                                ],
                                "labels": ["électricité", "chaleur"],
                                "type": ["elec", "heat"],
                                "colors": ["#0F9ED5", "#4EA72E"],
                            }
                        ],
                        "projects_data": [
                            {
                                "production_annuelle": [0, 0],
                                "labels": ["électricité", "chaleur"],
                            }
                        ],
                        "bilan_data": [
                            {
                                "production_annuelle": [
                                    A["prod-biogas-elec-max-year"],
                                    A["prod-biogas-ther-max-year"],
                                ],
                                "labels": ["électricité", "chaleur"],
                                "type": ["elec", "heat"],
                                "colors": ["#0F9ED5", "#4EA72E"],
                            }
                        ],
                    },
                },
            },
            "5a": {
                "display": "simple",
                "type": "heat",
                "color": "#994010",
                "parc_data": [
                    {"production_annuelle": A["prod-biomass-ther-domestic-max-year"]},
                ],
                "projects_data": [{"production_annuelle": 0}],
                "bilan_data": [
                    {"production_annuelle": A["prod-biomass-ther-domestic-max-year"]}
                ],
            },
            "6a": {
                "display": "simple",
                "type": "heat",
                "color": "#0F4016",
                "parc_data": [
                    {"production_annuelle": A["prod-solar-ther-max-year"]},
                ],
                "projects_data": [
                    {"production_annuelle": 0},
                ],
                "bilan_data": [
                    {"production_annuelle": A["prod-solar-ther-max-year"]},
                ],
            },
            "7a": {
                "display": "simple",
                "type": "heat",
                "color": "#FFC000",
                "parc_data": [
                    {
                        "production_annuelle": [
                            A["prod-aerothermal-pac-max-year"],
                            A["prod-geothermal-pac-max-year"],
                        ],
                        "labels": ["aérothermie", "géothermie"],
                    }
                ],
                "projects_data": [
                    {
                        "production_annuelle": [0, 0],
                        "labels": ["aérothermie", "géothermie"],
                    }
                ],
                "bilan_data": [
                    {
                        "production_annuelle": [
                            A["prod-aerothermal-pac-max-year"],
                            A["prod-geothermal-pac-max-year"],
                        ],
                        "labels": ["aérothermie", "géothermie"],
                    }
                ],
            },
        }

        logger.debug("Default settings.")
        return settings

    def get_default_impacts(self, A, activated_impacts, actions_params):
        """
        Computes the initial values of impacts for energy consumption and production.

        Parameters
        ----------
        A : dict
            Specific data.
        activated_impacts : list
            List of activated impacts.
        actions_params : dict
            Parameters of actions.

        Returns
        -------
        dict
            Initial settings of the impacts for energy consumption and production.
        """

        # Initialize impact variables for global and electrical results
        impact_energy_global = {}
        impact_production_global = {}
        coverage_rate_global = {}
        mix_enr_global = {}

        impact_energy_elec = {}
        impact_production_elec = {}
        coverage_rate_elec = {}
        mix_enr_elec = {}

        def calculate_total_production(data_key, filter_type=None):
            """
            Calculate total energy production.

            - If `filter_type == "elec"`, only sum values where `type == "elec"`.
            - Otherwise, sum all values (for global energy results).

            Parameters
            ----------
            data_key : str
                The key to extract data from (e.g., "parc_data", "bilan_data").
            filter_type : str or None
                If "elec", filter out non-electric production.

            Returns
            -------
            float
                The total energy production.
            """
            total = 0

            for action in actions_params.values():
                action_type = action.get("type")  # Main action type

                # Case 1: If filtering by "elec", check if action type is "elec"
                if filter_type == "elec" and action_type != "elec":
                    # If not "elec", check its sub_actions
                    if "sub_action" in action:
                        for sub_action in action["sub_action"].values():
                            # Check if sub_action has a type
                            sub_action_type = sub_action.get("type")

                            # If sub_action has a type, it must include "elec"
                            if sub_action_type and "elec" in sub_action_type:
                                for bilan in sub_action.get(data_key, []):
                                    if isinstance(bilan["production_annuelle"], list):
                                        # If production is a list, check type list
                                        for i, value in enumerate(
                                            bilan["production_annuelle"]
                                        ):
                                            if (
                                                bilan.get("type")
                                                and bilan["type"][i] == "elec"
                                            ):
                                                total += value
                                    else:
                                        total += bilan["production_annuelle"]

                            # If no explicit type but production_annuelle is a list, check type inside list
                            elif not sub_action_type:
                                for bilan in sub_action.get(data_key, []):
                                    if isinstance(bilan["production_annuelle"], list):
                                        for i, value in enumerate(
                                            bilan["production_annuelle"]
                                        ):
                                            if (
                                                bilan.get("type")
                                                and bilan["type"][i] == "elec"
                                            ):
                                                total += value
                                    else:
                                        total += bilan["production_annuelle"]

                    # Skip this action if it's not "elec" and has no valid "elec" sub-actions
                    continue

                # Case 2: No filtering (global results) → Sum everything
                for bilan in action.get(data_key, []):
                    if isinstance(bilan["production_annuelle"], list):
                        total += sum(bilan["production_annuelle"])
                    else:
                        total += bilan["production_annuelle"]

                # Process sub-actions
                if "sub_action" in action:
                    for sub_action in action["sub_action"].values():
                        for bilan in sub_action.get(data_key, []):
                            if isinstance(bilan["production_annuelle"], list):
                                total += sum(bilan["production_annuelle"])
                            else:
                                total += bilan["production_annuelle"]

            return total

        # Calculate total production for global and electrical results
        total_parc_production = calculate_total_production("parc_data")
        total_parc_production_elec = calculate_total_production(
            "parc_data", filter_type="elec"
        )

        # Define global energy results
        if "global-results" in activated_impacts:
            energy_consumption = A.get("energy-consumption-max-year", 0)
            impact_energy_global = {
                "type": "bar",
                "label": "Consommation énergétique",
                "impactKey": "impact-energy",
                "default": energy_consumption,
            }
            impact_production_global = {
                "type": "bar",
                "impactKey": "impact-production",
                "label": "Production EnR",
                "default": total_parc_production,
            }
            coverage_rate_global = {
                "type": "text",
                "impactKey": "coverage-rate",
                "label": "Taux de couverture",
                "unit": "%",
                "default": (
                    total_parc_production / energy_consumption * 100
                    if energy_consumption != 0
                    else 0
                ),
            }
            mix_enr_global = {
                "type": "pie",
                "impactKey": "mix-enr",
                "unit": "GWh",
                "label": "Mix EnR",
            }

        # Define EnR electricity results
        if "enr-elec" in activated_impacts:
            elec_consumption = A.get("energy-consumption-electricity-max-year", 0)

            impact_energy_elec = {
                "type": "bar",
                "label": "Consommation EnR électrique",
                "impactKey": "impact-energy",
                "default": elec_consumption,
            }
            impact_production_elec = {
                "type": "bar",
                "impactKey": "impact-production",
                "label": "Production EnR électrique",
                "default": total_parc_production_elec,
            }
            coverage_rate_elec = {
                "type": "text",
                "impactKey": "coverage-rate",
                "label": "Taux de couverture électrique",
                "unit": "%",
                "default": (
                    total_parc_production_elec / elec_consumption * 100
                    if elec_consumption != 0
                    else 0
                ),
            }
            mix_enr_elec = {
                "type": "pie",
                "impactKey": "mix-enr",
                "unit": "GWh",
                "label": "Mix EnR électrique",
            }

        # Construct global results
        global_results = {
            "current": {
                "impact-energy": impact_energy_global,
                "impact-production": impact_production_global,
                "coverage-rate": coverage_rate_global,
                "mix-enr": mix_enr_global,
            },
            "target": {
                "impact-energy": impact_energy_global,
                "impact-production": impact_production_global,
                "coverage-rate": coverage_rate_global,
                "mix-enr": mix_enr_global,
            },
        }

        # Construct EnR electric results
        enr_elec_results = {
            "current": {
                "impact-energy": impact_energy_elec,
                "impact-production": impact_production_elec,
                "coverage-rate": coverage_rate_elec,
                "mix-enr": mix_enr_elec,
            },
            "target": {
                "impact-energy": impact_energy_elec,
                "impact-production": impact_production_elec,
                "coverage-rate": coverage_rate_elec,
                "mix-enr": mix_enr_elec,
            },
        }

        # Final settings
        settings = {
            "global-results": global_results,
            "enr-elec": enr_elec_results,
        }

        return settings

    async def get_metadata(self):
        """
        Retrieves metadata related to renewable energy production.
        """
        try:
            simulator = {}
            metadata_enr = await self.get_configuration()
            if isinstance(metadata_enr["level"], dict):
                simulator["metadata"] = metadata_enr
                return simulator

            # Fetch additional configuration and data
            activated_impacts = await self.get_region_impact_configuration()
            generic_input_param = await self.load_generic_input_data()
            specific_input_param = await self.load_specific_data(
                metadata_enr["level"], activated_impacts, generic_input_param
            )

            # Get actions default params
            actions_params = self.get_default_settings(
                specific_input_param, generic_input_param
            )

            # Get intermediate calculations
            intermediate_calcul = await self.inter_calcul(
                specific_input_param, actions_params
            )

            # Get default impact Settings
            impacts_params = self.get_default_impacts(
                specific_input_param, activated_impacts, actions_params
            )

            simulator["metadata"] = metadata_enr
            simulator["actions_params"] = actions_params
            simulator["generic_input_param"] = generic_input_param
            simulator["specific_input_param"] = specific_input_param
            simulator["intermediate_compute"] = intermediate_calcul
            simulator["impacts_params"] = impacts_params
            simulator["activated_impacts"] = activated_impacts

            return simulator

        except Exception as e:
            logger.exception("Error in get_metadata: %s", str(e))
            raise

    async def load_specific_data(self, level, activated_impacts, input_param):
        """
        Gets data for the selected region.
        """
        try:
            data = {}

            # Process energy consumption data
            data.update(await self.process_energy_consumption_data())

            # Process renewable energy production data
            data.update(await self.process_renewable_energy_data())

            # Process renewable electricity power data
            data.update(await self.process_renewable_electricity_power_installed_data())

            # Process potential energy data
            data.update(await self.process_potential_energy())

            # Porcess PCAET ademe data
            data.update(await self.process_pcaet_ademe())

            logger.debug("Data simulation results: %s", data)
            return data

        except Exception as e:
            logger.exception("Error in load_specific_data: %s", str(e))
            raise

    async def fetch_and_validate_data(
        self, description, data_set, filter_type, param=None
    ):
        """Helper function to fetch and validate dataset-related information."""
        desc = await self.query(description)
        if not desc:
            logger.error(f"Failed to fetch {data_set} dataset description.")
            raise ValueError(f"{data_set} dataset description is unavailable.")

        data_table = await self.filter(desc, data_set, filter_type)
        if not data_table:
            logger.error(f"Failed to retrieve {data_set} table.")
            raise ValueError(f"{data_set} table is unavailable.")

        if param:
            data_unit = await self.filter(desc, data_set, "data_unit")
            if not data_unit:
                logger.error(f"Failed to retrieve {data_set} unit.")
                raise ValueError(f"{data_set} unit is unavailable.")

        return data_table, desc, data_unit if param else None

    async def process_energy_consumption_data(self) -> Dict[str, Any]:
        """
        Processes energy consumption data for various sectors, commodities, and usages.

        Returns:
            A dictionary with the processed energy consumption data.
        """
        data = {}

        conso_ener_cf_query, conso_ener_cf_params = await self.get_custom_filters(
            "DataSet.CONSUMPTION", 4
        )
        # Fetch and validate datasets
        (
            energy_consumption_table,
            energy_consumption_desc,
            energy_consumption_unit_in,
        ) = await self.fetch_and_validate_data(
            "DataSet.CONSUMPTION", "DataSet.CONSUMPTION", "match", True
        )
        population_table, _, _ = await self.fetch_and_validate_data(
            "DataSet.POPULATION", "DataSet.POPULATION", "match"
        )
        tertiary_employment_table, _, _ = await self.fetch_and_validate_data(
            "DataSet.TERTIARY_EMPLOYMENT", "DataSet.TERTIARY_EMPLOYMENT", "match"
        )

        # Fetch column names and IDs
        columns = {
            "sector": await self.filter(
                energy_consumption_desc, "DataSet.CONSUMPTION/Sector", "match"
            ),
            "commodity": await self.filter(
                energy_consumption_desc, "DataSet.CONSUMPTION/Commodity", "match"
            ),
            "usage": await self.filter(
                energy_consumption_desc, "DataSet.CONSUMPTION/Usage", "match"
            ),
        }

        ids = {
            "sector_services": await self.filter(
                energy_consumption_desc, "DataSet.CONSUMPTION/Sector.SERVICES", "match"
            ),
            "sector_residential": await self.filter(
                energy_consumption_desc,
                "DataSet.CONSUMPTION/Sector.RESIDENTIAL",
                "match",
            ),
            "commodity_electricity": await self.filter(
                energy_consumption_desc,
                "DataSet.CONSUMPTION/Commodity.ELECTRICITY",
                "match",
                True,
            ),
            "usage_heating": await self.filter(
                energy_consumption_desc, "DataSet.CONSUMPTION/Usage.HEATING", "match"
            ),
        }

        if not all(columns.values()) or not all(ids.values()):
            raise ValueError("Required columns or IDs are missing.")

        # Fetch data for the latest year and diagnosis year
        max_years = {
            "energy_consumption": await self.get_max_year(energy_consumption_table),
            "population": await self.get_max_year(population_table),
            "tertiary_employment": await self.get_max_year(tertiary_employment_table),
        }

        energy_data_latest_year = await self.fetch_data_analysis(
            energy_consumption_table,
            "*",
            "a.annee = $3 " + conso_ener_cf_query,
            params=[max_years["energy_consumption"]] + conso_ener_cf_params,
        )

        population_total = await self.fetch_data_analysis(
            population_table,
            "sum(a.valeur) as valeur",
            "a.annee = $3",
            params=[max_years["population"]],
        )
        tertiary_employment_total = await self.fetch_data_analysis(
            tertiary_employment_table,
            "sum(a.valeur) as valeur",
            "a.annee = $3",
            params=[max_years["tertiary_employment"]],
        )

        diagnosis_year = 2015
        energy_data_diagnosis_year_total = await self.fetch_data_analysis(
            energy_consumption_table,
            "sum(a.valeur) as valeur",
            "a.annee = $3",
            params=[diagnosis_year],
        )

        # Calculate totals for the latest year
        totals = {
            "max_year": 0,
            "electricity": 0,
            "services_heating": 0,
            "residential_heating": 0,
        }

        for record in energy_data_latest_year:
            totals["max_year"] += record["valeur"]
            if int(record[columns["sector"]]) == int(ids["sector_services"]) and int(
                record[columns["usage"]]
            ) == int(ids["usage_heating"]):
                totals["services_heating"] += record["valeur"]
            if int(record[columns["sector"]]) == int(ids["sector_residential"]) and int(
                record[columns["usage"]]
            ) == int(ids["usage_heating"]):
                totals["residential_heating"] += record["valeur"]
            if int(record[columns["commodity"]]) == int(ids["commodity_electricity"]):
                totals["electricity"] += record["valeur"]

        # Convert units and store result
        data.update(
            {
                "energy-consumption-max-year": self.unit_converter_with_unit_out(
                    "GWh", energy_consumption_unit_in, totals["max_year"]
                ),
                "energy-consumption-services-heating-max-year": self.unit_converter_with_unit_out(
                    "GWh", energy_consumption_unit_in, totals["services_heating"]
                ),
                "energy-consumption-residential-heating-max-year": self.unit_converter_with_unit_out(
                    "GWh", energy_consumption_unit_in, totals["residential_heating"]
                ),
                "energy-consumption-electricity-max-year": self.unit_converter_with_unit_out(
                    "GWh", energy_consumption_unit_in, totals["electricity"]
                ),
                "energy-consumption-diagnosis-year": self.unit_converter_with_unit_out(
                    "GWh",
                    energy_consumption_unit_in,
                    energy_data_diagnosis_year_total[0]["valeur"],
                ),
                "energy-consumption-per-habitant-residential-chauffage-max-year": (
                    self.unit_converter_with_unit_out(
                        "kWh", energy_consumption_unit_in, totals["residential_heating"]
                    )
                    / population_total[0]["valeur"]
                ),
                "energy-consumption-per-habitant-services-chauffage-max-year": (
                    self.unit_converter_with_unit_out(
                        "kWh", energy_consumption_unit_in, totals["services_heating"]
                    )
                    / tertiary_employment_total[0]["valeur"]
                ),
                "number-employees-tertiary-sector": tertiary_employment_total[0][
                    "valeur"
                ],
                "number-inhabitants": population_total[0]["valeur"],
                "last-year-consumption": max_years["energy_consumption"],
            }
        )

        return data

    async def process_renewable_energy_data(self) -> Dict[str, Any]:
        """
        Processes renewable energy production data.
        """
        data = {}

        # Fetching and validating dataset details for renewable energy
        (
            renewable_energy_prod_table,
            renewable_energy_prod_desc,
            renewable_energy_prod_unit_in,
        ) = await self.fetch_and_validate_data(
            "DataSet.PRODUCTION", "DataSet.PRODUCTION", "match", True
        )

        renewable_energy_vector_column = await self.filter(
            renewable_energy_prod_desc, "DataSet.PRODUCTION/RenewableProd", "match"
        )

        # Fetch all renewable energy production types and their ids
        renewable_energy_types = [
            "SOLAR_PV",
            "WIND",
            "HYDRO_HIGH",
            "HYDRO_LOW",
            "BIOGAS_INJEC",
            "BIOGAS_ELEC",
            "BIOGAS_THER",
            "BIOMASS_THER_DOMESTIC",
            "SOLAR_THER",
            "AEROTHERMAL_PAC",
            "GEOTHERMAL_PAC",
        ]

        energy_ids = {
            energy_type: await self.filter(
                renewable_energy_prod_desc,
                f"DataSet.PRODUCTION/RenewableProd.{energy_type}",
                "match",
            )
            for energy_type in renewable_energy_types
        }

        # Fetch data for the latest year available
        cons_ener_data_max_year = await self.fetch_data_analysis(
            renewable_energy_prod_table, "*"
        )
        max_year_renewable_energy_prod = await self.get_max_year(
            renewable_energy_prod_table
        )

        # Initialize dictionaries to store yearly data for each renewable energy type
        yearly_data = {
            energy_type: [0, 0, 0, 0] for energy_type in renewable_energy_types
        }

        # Process each record and accumulate values for the last four years
        for item in cons_ener_data_max_year:
            year = int(float(item["annee"]))
            if year >= max_year_renewable_energy_prod - 3:
                for energy_type in renewable_energy_types:
                    energy_id = int(energy_ids[energy_type])
                    if int(item[renewable_energy_vector_column]) == energy_id:
                        year_index = int(max_year_renewable_energy_prod - year)
                        yearly_data[energy_type][year_index] += item["valeur"]

        # Convert units and store the results in the `data` dictionary
        for energy_type, values in yearly_data.items():
            for i, value in enumerate(values):
                year_label = (
                    f"prod-{energy_type.lower().replace('_', '-')}-max-year-{i}"
                    if i > 0
                    else f"prod-{energy_type.lower().replace('_', '-')}-max-year"
                )
                data[year_label] = self.unit_converter_with_unit_out(
                    "GWh", renewable_energy_prod_unit_in, value
                )
        # Fetch irradiation data
        irradiation_global = await get_global_irradiation(
            self.region, self.territory_type, self.territory_code
        )
        data["global-irradiation"] = irradiation_global["fixe"]
        data["last-year-prod"] = max_year_renewable_energy_prod

        return data

    async def process_renewable_electricity_power_installed_data(
        self,
    ) -> Dict[str, Any]:
        """
        Processes renewable electricity power data.
        """
        data = {}

        # Fetching and validating dataset details for renewable energy
        (
            renewable_elec_power_table,
            renewable_elec_power_desc,
            renewable_elec_power_unit_in,
        ) = await self.fetch_and_validate_data(
            "DataSet.RENEWABLE_ELECTRICITY_POWER",
            "DataSet.RENEWABLE_ELECTRICITY_POWER",
            "match",
            True,
        )

        # get Columns and IDs
        renewable_elec_power_vector_column = await self.filter(
            renewable_elec_power_desc,
            "DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE",
            "match",
        )

        # Fetch all renewable energy production types and their ids
        renewable_energy_types = [
            "WIND",
            "PHOTOVOLTAIC",
            "HYDROELECTRIC_LOW",
            "HYDROELECTRIC_HIGH",
        ]

        energy_ids = {
            energy_type: await self.filter(
                renewable_elec_power_desc,
                f"DataSet.RENEWABLE_ELECTRICITY_POWER/POWER_TYPE.{energy_type}",
                "match",
            )
            for energy_type in renewable_energy_types
        }

        # Fetch data for the latest year available
        max_year = await self.get_max_year(renewable_elec_power_table)
        cons_ener_data_max_year = await self.fetch_data_analysis(
            renewable_elec_power_table,
            "*",
            "a.annee = $3",
            params=[max_year],
        )

        # Initialize dictionaries to store data for each renewable energy type
        yearly_data = {energy_type: 0 for energy_type in renewable_energy_types}

        # Process each record and accumulate values
        for item in cons_ener_data_max_year:
            # Process each energy type in the current record
            for energy_type in renewable_energy_types:
                energy_id = int(energy_ids[energy_type])
                if (
                    int(item[renewable_elec_power_vector_column]) == energy_id
                    and item["valeur"] is not None
                ):  # TODO : None value in Database ??
                    yearly_data[energy_type] += item["valeur"]

        # Convert units and store the results in the `data` dictionary
        for energy_type, values in yearly_data.items():
            label = f"renewable-elec-installed-power-{energy_type.lower().replace('_', '-')}-max-year"
            data[label] = self.unit_converter_with_unit_out(
                "MW", renewable_elec_power_unit_in, values
            )  # TODO : add analysis name to label !!

        # Latest year available
        data["last-year-renewable-elec-installed"] = max_year

        return data

    async def process_potential_energy(self) -> Dict[str, Any]:
        """
        Processes potential energy data for:
        - Solar photovoltaic potential.
        - Wind power potential.
        - Methanization potential.

        Returns:
            A dictionary with the processed potential energy data.
        """
        data = {}

        # Fetch and validate dataset details
        datasets = {
            "solar-pv": {
                "key": "DataSet.SOLAR_PHOTOVOLTAIC_POTENTIAL",
                "table": None,
                "desc": None,
                "column": None,
                "column_label": "PHOTOVOLTAIC_CONSTRAINT",
                "energy_types": ["NO"],
                "energy_ids": {},
                "max_year": None,
                "data": [],
                "yearly_data": {},
                "unit_in": None,
                "unit_out": "GWh",
            },
            "wind": {
                "key": "DataSet.WIND_ENERGY_POTENTIAL",
                "table": None,
                "desc": None,
                "column": None,
                "column_label": "Stakes",
                "energy_types": ["NO_CONSTRAINT"],
                "energy_ids": {},
                "max_year": None,
                "data": [],
                "yearly_data": {},
                "unit_in": None,
                "unit_out": "ha",
            },
            "methanization": {
                "key": "DataSet.METHANIZATION_POTENTIAL",
                "table": None,
                "desc": None,
                "column": None,
                "max_year": None,
                "data": [],
                "unit_in": None,
                "unit_out": "GWh",
            },
        }

        # Fetch and validate data for each dataset
        for key, dataset in datasets.items():
            value_condition = "sum(valeur) as valeur"
            (
                dataset["table"],
                dataset["desc"],
                dataset["unit_in"],
            ) = await self.fetch_and_validate_data(
                dataset["key"], dataset["key"], "match", True
            )
            if key != "methanization":  # Methanization does not have energy types
                value_condition = "*"
                dataset["column"] = await self.filter(
                    dataset["desc"],
                    f"{dataset['key']}/{dataset['column_label']}",
                    "match",
                )
                dataset["energy_ids"] = {
                    energy_type: await self.filter(
                        dataset["desc"],
                        f"{dataset['key']}/{dataset['column_label']}.{energy_type}",
                        "match",
                    )
                    for energy_type in dataset["energy_types"]
                }
            dataset["max_year"] = await self.get_max_year(dataset["table"])

            # Fetching data
            dataset["data"] = await self.fetch_data_analysis(
                dataset["table"],
                value_condition,
                "a.annee = $3",
                params=[dataset["max_year"]],
            )

            # Store last year available
            data[f"last-year-{key}"] = dataset["max_year"]

        # Process solar PV and wind data
        for key in ["solar-pv", "wind"]:
            dataset = datasets[key]
            dataset["yearly_data"] = {etype: 0 for etype in dataset["energy_types"]}
            for record in dataset["data"]:
                for energy_type, energy_id in dataset["energy_ids"].items():
                    if (
                        int(record[dataset["column"]]) == int(energy_id)
                        and record["valeur"] is not None
                    ):
                        dataset["yearly_data"][energy_type] += record["valeur"]

            # Convert and store results
            for energy_type, value in dataset["yearly_data"].items():
                label = f"{key}-{energy_type.lower().replace('_', '-')}-max-year"
                data[label] = self.unit_converter_with_unit_out(
                    dataset["unit_out"], dataset["unit_in"], value
                )  # TODO: merge unit_converter functions

        if datasets["methanization"]["data"]:
            data["methanization-max-year"] = self.unit_converter_with_unit_out(
                datasets["methanization"]["unit_out"],
                datasets["methanization"]["unit_in"],
                datasets["methanization"]["data"][0]["valeur"],
            )
        else:
            data["methanization-max-year"] = 0

        return data

    async def process_pcaet_ademe(self) -> Dict[str, Any]:
        """
        Processes PCAET data for consumption and renewable energy production.
        """

        # Define a default data structure for "NA" responses
        def default_na_data(diagnosis_year=None):
            years = [diagnosis_year, 2030, 2050] if diagnosis_year else [2030, 2050]
            return {
                "pcaet-diagnosis-year": "NA",
                "pcaet-consumption": {year: "NA" for year in years},
                "pcaet-production": {
                    category: {year: "NA" for year in years}
                    for category in [
                        "solar-photovoltaic",
                        "wind-energy",
                        "hydroelectric-energy",
                        "methanization",
                        "electric-cogeneration",
                        "thermal-cogeneration",
                        "solid-biomass",
                        "solar-thermal",
                        "aerothermal-heat-pumps",
                        "geothermal-heat-pumps",
                    ]
                },
            }

        # Check if the territory type is "epci"
        if self.territory_type != "epci":
            return default_na_data()

        # Fetch PCAET dataframe
        pcaet_df = await get_pcaet_ademe(
            self.region, self.territory, self.territory_code
        )

        # Handle case where no PCAET data is available
        if pcaet_df is None:
            return default_na_data()

        # Determine the diagnosis year as the minimum year
        diagnosis_year = int(pcaet_df["year"].min())

        # Initialize output data structure
        data = default_na_data(diagnosis_year)
        data["pcaet-diagnosis-year"] = diagnosis_year

        # Define dataset configurations
        datasets = {
            "consumption": {
                "key": "DataSet.CONSUMPTION",
                "column": None,
                "desc": None,
                "unit_in": None,
                "unit_out": "MWh",
                "years": [diagnosis_year, 2030, 2050],
                "yearly_data": {},
            },
            "production": {
                "key": "DataSet.PRODUCTION",
                "column": None,
                "column_label": "RenewableProd",
                "energy_ids": {},
                "unit_in": None,
                "unit_out": "MWh",
                "years": [2030, 2050],
                "yearly_data": {},
            },
        }

        # Define the specific energy categories
        energy_categories = {
            "solar-photovoltaic": ["SOLAR_THERMODYNAMIC"],
            "wind-energy": ["HYDRO_HIGH"],
            "hydroelectric-energy": ["WASTE_VALORIZATION_ELEC", "BIOGAS_THER"],
            "methanization": ["BIOGAS_INJEC"],
            "electric-cogeneration": ["BIOMASS_THER_COGENERATION"],
            "thermal-cogeneration": [
                "BIOMASS_THER_CHAUFFERIE",
                "BIOMASS_THER_DOMESTIC",
            ],
            "solid-biomass": ["BIOGAS_ELEC"],
            "solar-thermal": ["SOLAR_PV"],
            "aerothermal-heat-pumps": ["WIND"],
            "geothermal-heat-pumps": ["GEOTHERMAL_PAC"],
        }

        # Fetch and validate datasets
        for key, dataset in datasets.items():
            dataset["desc"] = await self.query(dataset["key"], "pcaet")
            if key == "production":
                dataset["column"] = await self.filter(
                    dataset["desc"],
                    f"{dataset['key']}/{dataset['column_label']}",
                    "match",
                )
                # Fetch energy IDs by category
                for category, energy_types in energy_categories.items():
                    dataset["energy_ids"][category] = {
                        energy_type: await self.filter(
                            dataset["desc"],
                            f"{dataset['key']}/{dataset['column_label']}.{energy_type}",
                            "match",
                        )
                        for energy_type in energy_types
                    }

        # Process consumption trajectory
        consumption_df = pcaet_df[pcaet_df["trajectory"] == "consommation_ener"]
        for year in datasets["consumption"]["years"]:
            filtered_df = consumption_df[consumption_df["year"] == year]
            datasets["consumption"]["yearly_data"][year] = filtered_df["value"].sum()

        # Process production trajectory
        production_df = pcaet_df[pcaet_df["trajectory"] == "enr_production"]
        for category, energy_ids in datasets["production"]["energy_ids"].items():
            for year in datasets["production"]["years"]:
                for energy_type, energy_id in energy_ids.items():
                    filtered_df = production_df[
                        (
                            production_df[datasets["production"]["column"]]
                            .astype(int)
                            .isin([int(energy_id)])
                        )
                        & (production_df["year"].astype(int) == int(year))
                    ]
                    datasets["production"]["yearly_data"].setdefault(
                        category, {}
                    ).setdefault(year, 0)
                    datasets["production"]["yearly_data"][category][
                        year
                    ] += filtered_df["value"].sum()

        # Fill in consumption data
        for year, value in datasets["consumption"]["yearly_data"].items():
            if year in data["pcaet-consumption"]:
                data["pcaet-consumption"][year] = value

        # Fill in production data
        for category, yearly_data in datasets["production"]["yearly_data"].items():
            if category in data["pcaet-production"]:
                for year, value in yearly_data.items():
                    if year in data["pcaet-production"][category]:
                        data["pcaet-production"][category][year] = value

        return data

    async def inter_calcul(self, A, B):
        """
        Performs intermediate calculations for simulator impacts.
        Parameters
        ----------
            A : dict
                specific input parameters
            B : dict
                actions parameters

        Returns
        -------
            data : dict

        """

        inter_calcul = defaultdict(dict)

        # Initial setting
        initial_setting_params = initial_setting.get_initial_setting(A)
        # Actions
        pv = photovoltaic.get_photovoltaic(A, B)
        wind_power = wind.get_wind(A, B)
        hydraulic_power = hydraulic.get_hydraulic(A, B)
        methanization_power = methanization.get_methanization(A, B)
        solid_biomass_power = solid_biomass.get_solid_biomass(A, B)
        solar_thermal_power = solar_thermal.get_solar_thermal(A, B)
        heat_pump_power = heat_pump.get_heat_pump(A, B)

        help_data = {
            "1a": {"type_display": "simple", "data": pv},
            "2a": {"type_display": "simple", "data": wind_power},
            "3a": {"type_display": "simple", "data": hydraulic_power},
            "4a": {
                "type_display": "simple",
                "data": methanization_power,
            },  # has simple and complex data, type_display used only in global mode TODO : improve this
            "5a": {"type_display": "simple", "data": solid_biomass_power},
            "6a": {"type_display": "simple", "data": solar_thermal_power},
            "7a": {"type_display": "complex", "data": heat_pump_power},
        }

        inter_calcul["initial-setting"] = initial_setting_params
        inter_calcul["help_data_actions"] = help_data

        return inter_calcul
