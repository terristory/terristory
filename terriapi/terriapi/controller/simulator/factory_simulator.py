# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


from terriapi.controller.simulator.mobility.simulator_mobility import SimulatorMobility
from terriapi.controller.simulator.renewable_energy.simulator_renewable_energy import (
    SimulatorRenewableEnergy,
)
from terriapi.controller.simulator.simulator import Simulator


class FactorySimulator:
    """
    A Factory class to create instances of the simulator based on the thematic.
    """

    @staticmethod
    def create_simulator(
        region, territory, territory_type, territory_code, thematic
    ) -> Simulator:
        """
        Create and return the appropriate simulator instance based on the thematic.
        """
        if thematic == "mobility":
            return SimulatorMobility(
                region, territory, territory_type, territory_code, thematic
            )
        if thematic == "enr":
            return SimulatorRenewableEnergy(
                region, territory, territory_type, territory_code, thematic
            )
        else:
            raise ValueError(f"Unknown thematic: {thematic}")
