# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


import logging
from abc import ABC, abstractmethod
from collections import defaultdict

from sanic.log import logger

from terriapi import controller


class Simulator(ABC):
    def __init__(self, region, territory, territory_type, territory_code, thematic):
        self.region = region
        self.territory = territory
        self.territory_type = territory_type
        self.territory_code = territory_code
        self.thematic = thematic

    @abstractmethod
    def load_specific_data(self):
        return NotImplemented

    @abstractmethod
    def get_metadata(self):
        return NotImplemented

    async def load_generic_input_data(self):
        """
        this method gets the generic data for all regions
        """

        request_category = """ SELECT * FROM simulator.generic_input_param WHERE thematic = $1"""  # TODO : rename data by category or type
        rslt = await controller.fetch(request_category, self.thematic)
        data = [dict(x) for x in rslt]

        return data

    async def load_generic_inter_data(self):
        """
        this method gets the generic data for all regions for interm calcul
        """

        request_category = """ SELECT * FROM simulator.generic_interm_param WHERE thematic = $1"""  # TODO : rename data by category or type
        rslt = await controller.fetch(request_category, self.thematic)
        data = [dict(x) for x in rslt]

        return data

    async def get_levels(self):
        """
        Get the simulator level of the selected region (level 0 : .... level 1 : .... ) TODO : add description of each level
        """

        request = """
            SELECT * from simulator.simulator_level WHERE region = $1 AND thematic = $2 """

        try:
            res = await controller.fetch(request, self.region, self.thematic)
            return res[0]["level"]
        except Exception as e:
            logging.error(f"Error fetching simulator levels: {e}")
            return {
                "status": "error",
                "message": "Les données nécessaires pour exécuter le simulateur de mobilité dans cette région ne sont pas disponibles. Veuillez contacter un administrateur pour obtenir plus d'informations.",
            }

    async def get_region_impact_configuration(self):
        """
        Get the region configuration (activated categories, levers and impacts)
        """

        request = """
            SELECT type from simulator.simulator_impact WHERE $1 = ANY(regions) AND thematic = $2"""

        res = await controller.fetch(request, self.region, self.thematic)

        reslt = [i["type"] for i in res]

        return reslt

    async def get_levers(self):
        """
        Get the list of enabled category lever, lever and action related to a specific region

        Parameters
        ----------
        region : str
            normalized region name (ex : auvergne-rhone-alpes, occitanie)

        Returns
        -------
        list of dict
            list of all category lever, lever and action organized as follow:
            data : {
                categoryLever1: {
                    id: "",
                    categoryTitle: "",
                    levers : {
                        lever1 : {
                        lever1Title: "",
                        lever1Actions : [{number: "", action1Title: ""}, {number: "", action2Title: ""}]
                        }
                    }
                }
            }
        """

        data = {}

        req_cat = """
            SELECT * FROM simulator.category_lever WHERE thematic = $1
            ORDER BY category_order
            """
        resp_cat = await controller.fetch(req_cat, self.thematic)
        res_cat = [dict(i) for i in resp_cat]
        if len(res_cat) > 0:
            for cat in res_cat:
                category = {}
                levers = {}
                category["id"] = cat["id"]
                category["categoryTitle"] = cat["title"]
                category["categoryOrder"] = cat["category_order"]
                category["regions_enabled"] = cat["regions"]
                req_lever = """
                    SELECT * from simulator.lever 
                    WHERE category_lever_id = $1
                    ORDER BY lever_order
                """
                resp_lever = await controller.fetch(req_lever, cat["id"])
                res_lever = [dict(i) for i in resp_lever]
                if len(res_lever) > 0:
                    for subcat in res_lever:
                        lever = {}
                        lever["id"] = subcat["id"]
                        lever["leverOrder"] = subcat["lever_order"]
                        lever["leverTitle"] = subcat["title"]
                        lever["regions_enabled"] = subcat["regions"]
                        req_actions = """
                            SELECT * from simulator.lever_action 
                            WHERE lever_id = $1
                        """
                        resp_actions = await controller.fetch(req_actions, subcat["id"])
                        res_actions = [dict(i) for i in resp_actions]
                        if len(res_actions) > 0:
                            lever["actions"] = res_actions
                        levers[lever["leverOrder"]] = lever
                    category["levers"] = levers
                data[category["categoryOrder"]] = category

        return data

    async def get_impacts(self):
        """
        Get the list of enabled impact related to a specific region

        Parameters
        ----------
        region : str
            normalized region name (ex : auvergne-rhone-alpes, occitanie)
        thematic : str

        Returns
        -------
        list of dict :
        list of all enabled impacts organized as follow:
            impact : {
                impact1: {
                    id: "",
                    impactTitle: "",
                    impactType: "",
                    unit: "",
                    color: "",
                    impactOrder: "",
                }
            }
        """

        sql = """
            SELECT * FROM simulator.simulator_impact 
            WHERE $1 = ANY(regions) 
            AND thematic = $2
            ORDER BY impact_order
            """
        resp = await controller.fetch(sql, self.region, self.thematic)
        res = [dict(i) for i in resp]

        impacts = {}
        if len(res) > 0:
            for item in res:
                impact = {}
                impact["impactTitle"] = item["title"]
                impact["impactType"] = item["type"]
                new_unit = self.unit_name(item["type"])
                impact["unit"] = item["unit"] if new_unit is None else new_unit
                impact["color"] = item["color"]
                impact["impactOrder"] = item["impact_order"]
                impact["enabled"] = item["type"] == "impact-ener"
                impacts[impact["impactOrder"]] = impact
        self.impacts = impacts
        return impacts

    async def get_units(self):
        query_units = """
            SELECT du.unit_id, du.other_key, du.is_default_for_zone_type,
            d.unit_name, d.conversion_factor
            FROM meta.data_units_other_modules_association du
            LEFT JOIN meta.data_units d ON d.unit_id = du.unit_id AND d.region = du.region
            WHERE du.region = $1 AND du.module_key = $2
            """
        res = await controller.fetch(
            query_units, self.region, "simulator-" + self.thematic
        )
        self.units = defaultdict(dict)
        for d in res:
            self.units[d["other_key"]][d["unit_id"]] = dict(d)
        return self.units

    async def get_configuration(self):
        """
        gets configuration : enabled category levers, enabled lever and  enabled impact for the selected region
        """

        simulator = {}
        simulator["level"] = await self.get_levels()
        if not isinstance(simulator["level"], dict):
            simulator["units"] = await self.get_units()
            simulator["data"] = await self.get_levers()
            simulator["impacts"] = await self.get_impacts()

        return simulator

    def get_unit(self, key):
        if key in self.units:
            available_units = [
                x
                for x in self.units[key].values()
                if self.territory_type in x["is_default_for_zone_type"].split(",")
            ]
            if len(available_units) > 0:
                return available_units[0]
        return None

    def unit_converter(self, key, unit_in, val):
        # Get the output unit based on the key
        unit_out = next(
            (x["unit"] for x in self.impacts.values() if x["impactType"] == key), None
        )

        # Conversion factors
        converter = {
            "kgeqCO2": 1.0e3,
            "teqCO2": 1.0e6,
            "kteqCO2": 1.0e9,
            "ktCO2e": 1.0e9,
            "ktCO2": 1.0e9,
            "kWh": 1.0e3,
            "MWh": 1.0e6,
            "GWh": 1.0e9,
            "kg": 1.0e3,
            "t": 1.0e6,
            "kt": 1.0e9,
            "€": 1.0,
            "k€": 1e3,
            "M€": 1.0e6,
        }

        # Handle cases where unit_in or unit_out is None or invalid
        if unit_in not in converter or unit_out not in converter:
            logging.warning(
                f"Invalid units: input unit={unit_in}, output unit ={unit_out}. Return to original value : {val}"
            )
            return val  # Return the original value as is

        return float(val * converter[unit_in] / converter[unit_out])

    def unit_converter_with_unit_out(self, unit_out, unit_in, val):
        """
        Converts a value from one unit to another based on predefined conversion factors.

        Args:
            unit_out (str): The target unit.
            unit_in (str): The source unit.
            val (float): The value to convert.

        Returns:
            float: The converted value, or the original value if the conversion fails.
        """
        # Conversion factors
        converter = {
            "kgeqCO2": 1.0e3,
            "teqCO2": 1.0e6,
            "kteqCO2": 1.0e9,
            "ktCO2e": 1.0e9,
            "ktCO2": 1.0e9,
            "kWh": 1.0e3,
            "MWh": 1.0e6,
            "GWh": 1.0e9,
            "kW": 1.0e3,
            "MW": 1.0e6,
            "GW": 1.0e9,
            "kg": 1.0e3,
            "t": 1.0e6,
            "kt": 1.0e9,
            "€": 1.0,
            "k€": 1.0e3,
            "M€": 1.0e6,
        }

        # Normalize units to match dictionary keys (case-insensitive comparison)
        unit_in = unit_in.strip()
        unit_out = unit_out.strip()

        # Debugging logs
        print(f"Converting {val} from {unit_in} to {unit_out}")

        # Validate input units
        if unit_in not in converter or unit_out not in converter:
            logging.warning(
                f"Invalid units: input unit={unit_in}, output unit ={unit_out}. Return to original value : {val}"
            )
            return val  # Return the original value unchanged if units are invalid

        # Validate value
        try:
            val = float(val)
        except (ValueError, TypeError):
            logging.warning(f"Invalid value for conversion: {val}")
            return val  # Return the original value unchanged if it's invalid

        # Perform conversion
        unit_in_factor = converter[unit_in]
        unit_out_factor = converter[unit_out]
        converted_value = val * unit_in_factor / unit_out_factor

        # Debugging logs
        print(f"Converted value: {converted_value}")

        return converted_value

    def unit_name(self, key):
        unit = self.get_unit(key)
        return unit.get("unit_name", None) if unit is not None else None

    async def query(self, table_key, region=None):
        """
        Queries the passage_table for specific keys for a given region.

        Args:
            table_key (str): The key to search for in the table.
            region (str): The region filter (defaults to self.region).

        Returns:
            List[Dict]: A list of dictionaries containing the query results.
        """

        # Use the default region if not explicitly provided
        if region is None:
            region = self.region
        query_filters = """
            SELECT key, match, association_type, data_unit FROM strategie_territoire.passage_table 
            WHERE region = $1 AND 
            (
                key::text = $2 OR key::text SIMILAR TO $3
            )"""

        # Execute query and fetch results
        try:
            res = await controller.fetch(
                query_filters, region, str(table_key), str(table_key) + "%"
            )
            # Convert the result rows to a list of dictionaries
            df = [dict(r) for r in res]
            return df
        except Exception as e:
            print(f"Error while querying the table: {e}")
            return []

    async def get_custom_filters(
        self, table_key: str, id_first_param: int = 2
    ) -> tuple[str, list]:
        """
        returns the potential conditions and parameters associated with a table.

        id_first_param corresponds to the first index of the params associated to the query
        (asyncpg queries use numbered params).
        """
        query_filters = """
            SELECT key, match, association_type, data_unit FROM strategie_territoire.passage_table 
            WHERE region = $1 AND key::text LIKE CONCAT($2::text, '/CustomFilter.%')"""
        res = await controller.fetch(query_filters, self.region, str(table_key))
        if len(res) == 0:
            return "", []
        # TOFIX: case with more than one value per column is possible? good idea?
        df_cols = {r["key"]: dict(r) for r in res if r["association_type"] == "column"}
        df_values = [dict(r) for r in res if r["association_type"] == "value"]

        query_filters, params = [], []
        i = id_first_param
        for value in df_values:
            modality, column_name = value["key"][::-1].split(".", 1)
            modality, column_name = modality[::-1], column_name[::-1]
            if column_name not in df_cols:
                logger.warning("No column name associated to a custom filter!")
                continue
            col_conf = df_cols[column_name]
            query_filters.append(f"AND a.{col_conf['match']}::int = ${i}")
            params.append(int(value["match"]))
            i += 1
        return "\n".join(query_filters), params

    async def filter(self, df, key, column_name, id=False):
        val = next(
            (x[column_name] for x in df if x["key"] == key),
            0,
        )
        if id:
            return int(val)
        return val

    async def get_max_year(self, table_name):
        """
        return the max year of the table passed in parameters
        """
        schema_region = self.region.replace("-", "_")
        request = f"""select max(annee) from {schema_region}.{table_name}"""

        res = await controller.fetch(request)

        return int(res[0]["max"])

    async def fetch_data_analysis(
        self, table_name, value_condition, year_condition="", data_filter="", params=[]
    ):
        """
        this method fetchs the data from DB for the parameter table name
        """
        schema_region = self.region.replace("-", "_")
        # Construct the WHERE clause based on conditions
        where_clause = ""
        if year_condition:
            where_clause += f"WHERE {year_condition}"

        if data_filter:
            if where_clause:
                where_clause += " AND "
            else:
                where_clause += "WHERE "
            where_clause += f"{data_filter}"

        request = f"""
                SELECT {value_condition}
                FROM {schema_region}.{table_name} AS a
                JOIN {schema_region}.territoire AS tzone 
                    ON tzone.commune = a.commune 
                    AND tzone.code = $1
                    AND tzone.type_territoire = $2
                {where_clause}
            """

        res = await controller.fetch(
            request, self.territory_code, self.territory_type, *params
        )
        reslt = [dict(i) for i in res]

        return reslt
