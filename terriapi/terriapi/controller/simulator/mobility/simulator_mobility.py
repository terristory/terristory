# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


from collections import defaultdict

from terriapi import controller
from terriapi.controller.simulator.mobility import (
    air_pollutant,
    consumption,
    energy_bill,
    ghg,
)
from terriapi.controller.simulator.simulator import Simulator
from terriapi.controller.simulator.utils import get_input_param


class SimulatorMobility(Simulator):
    """
    this class is used .... TODO : add description
    """

    async def load_specific_data(self, level, activated_impacts, input_param):
        """
        this method gets data fro the selected region
        """
        data = {}
        # get data accessible from the indicators distance domicile-travail
        # get the name of mobility table for the selected region
        mobilite_insee_desc = await self.query("DataSet.MOBILITY")
        mobilite_insee_table = await self.filter(
            mobilite_insee_desc, "DataSet.MOBILITY", "match"
        )

        # get max year for mobilite data
        max_year_mobilite_insee = await self.get_max_year(mobilite_insee_table)

        home_work_staff_total = 0
        total_km = 0
        cpt_total = 0
        # < 2km
        home_work_staff_less_2km = 0
        total_km_2km = 0
        cpt_2k = 0
        car_2wheels_less_2Km = 0
        velo_pied_less_2Km = 0
        tc_less_2k = 0

        # [2-10] km
        total_2_10km = 0
        total_km_2_10km = 0
        cpt_2_10km = 0
        car_2wheels_2_10km = 0
        velo_pied_2_10km = 0
        tc_2_10km = 0
        # [10-80] km
        total_10_80km = 0
        total_km_10_80km = 0
        cpt_10_80km = 0
        car_2wheels_10_80km = 0
        velo_pied_10_80km = 0
        tc_10_80km = 0

        # category column's name
        filter_column_name = await self.filter(
            mobilite_insee_desc, "DataSet.MOBILITY/Filter", "match"
        )
        ppal_trans_column_name = await self.filter(
            mobilite_insee_desc, "DataSet.MOBILITY/PrincipalMode", "match"
        )

        # get data
        mobilite_insee_data = await self.fetch_data_analysis(
            mobilite_insee_table,
            "a.valeur, a.{valeur_filtre}, a.{trans_ppal}".format(
                valeur_filtre=filter_column_name, trans_ppal=ppal_trans_column_name
            ),
            year_condition="a.annee = $3",
            params=[max_year_mobilite_insee],
        )

        # category ids
        two_wheel_id = await self.filter(
            mobilite_insee_desc,
            "DataSet.MOBILITY/PrincipalMode.TWO_WHEEL_MOTORIZED",
            "match",
            True,
        )
        car_truck_van_id = await self.filter(
            mobilite_insee_desc,
            "DataSet.MOBILITY/PrincipalMode.CAR_TRUCK_VAN",
            "match",
            True,
        )
        walk_id = await self.filter(
            mobilite_insee_desc, "DataSet.MOBILITY/PrincipalMode.WALK", "match", True
        )
        public_transport_id = await self.filter(
            mobilite_insee_desc,
            "DataSet.MOBILITY/PrincipalMode.PUBLIC_TRANSPORT",
            "match",
            True,
        )
        bike_id = await self.filter(
            mobilite_insee_desc, "DataSet.MOBILITY/PrincipalMode.BIKE", "match", True
        )

        for item in mobilite_insee_data:
            if item[filter_column_name] > 0 and item[filter_column_name] < 80:
                home_work_staff_total = home_work_staff_total + item["valeur"]
                total_km = total_km + item[filter_column_name]
                cpt_total = cpt_total + 1

            if item[filter_column_name] > 0 and item[filter_column_name] < 2:
                home_work_staff_less_2km = home_work_staff_less_2km + item["valeur"]
                total_km_2km = total_km_2km + item[filter_column_name]
                cpt_2k = cpt_2k + 1
                if item[ppal_trans_column_name] in [two_wheel_id, car_truck_van_id]:
                    car_2wheels_less_2Km = car_2wheels_less_2Km + item["valeur"]

                if item[ppal_trans_column_name] in [walk_id, bike_id]:
                    velo_pied_less_2Km = velo_pied_less_2Km + item["valeur"]

                if item[ppal_trans_column_name] == public_transport_id:
                    tc_less_2k = tc_less_2k + item["valeur"]

            if item[filter_column_name] > 2 and item[filter_column_name] < 10:
                total_2_10km = total_2_10km + item["valeur"]
                total_km_2_10km = total_km_2_10km + item[filter_column_name]
                cpt_2_10km = cpt_2_10km + 1
                if item[ppal_trans_column_name] in [two_wheel_id, car_truck_van_id]:
                    car_2wheels_2_10km = car_2wheels_2_10km + item["valeur"]

                if item[ppal_trans_column_name] in [walk_id, bike_id]:
                    velo_pied_2_10km = velo_pied_2_10km + item["valeur"]

                if item[ppal_trans_column_name] == public_transport_id:
                    tc_2_10km = tc_2_10km + item["valeur"]

            if item[filter_column_name] > 10 and item[filter_column_name] < 80:
                total_10_80km = total_10_80km + item["valeur"]
                total_km_10_80km = total_km_10_80km + item[filter_column_name]
                cpt_10_80km = cpt_10_80km + 1
                if item[ppal_trans_column_name] in [two_wheel_id, car_truck_van_id]:
                    car_2wheels_10_80km = car_2wheels_10_80km + item["valeur"]

                if item[ppal_trans_column_name] in [walk_id, bike_id]:
                    velo_pied_10_80km = velo_pied_10_80km + item["valeur"]

                if item[ppal_trans_column_name] == public_transport_id:
                    tc_10_80km = tc_10_80km + item["valeur"]

        # get specific data for the selected region
        home_work_avg_km = total_km / cpt_total if cpt_total != 0 else 0

        ## < 2km
        home_work_avg_km_less_2km = total_km_2km / cpt_2k if cpt_2k != 0 else 0

        part_car_2wheels = (
            car_2wheels_less_2Km / home_work_staff_less_2km * 100
            if home_work_staff_less_2km != 0
            else 0
        )
        part_velo_pied = (
            velo_pied_less_2Km / home_work_staff_less_2km * 100
            if home_work_staff_less_2km != 0
            else 0
        )
        part_tc = (
            tc_less_2k / home_work_staff_less_2km * 100
            if home_work_staff_less_2km != 0
            else 0
        )

        data["Nombre d'actifs domicile-travail"] = home_work_staff_total
        data["Distance moyenne domicile-travail"] = home_work_avg_km
        data["Nombre d'actifs trajet < 2 km"] = home_work_staff_less_2km
        data["Distance moyenne trajets < 2 km"] = home_work_avg_km_less_2km
        data["Part modale voiture + 2 roues pour trajet < 2 km"] = part_car_2wheels
        data["Part modale marche à pied / vélo pour trajet < 2 km"] = part_velo_pied
        data["Part modale transport en commun pour trajet < 2 km"] = part_tc

        ######### between [2-10km]

        home_work_staff_2_10km = total_2_10km
        home_work_avg_km_2_10km = total_km_2_10km / cpt_2_10km if cpt_2_10km != 0 else 0

        part_car_2wheels_2_10km = (
            car_2wheels_2_10km / home_work_staff_2_10km * 100
            if home_work_staff_2_10km != 0
            else 0
        )
        part_velo_pied_2_10km = (
            velo_pied_2_10km / home_work_staff_2_10km * 100
            if home_work_staff_2_10km != 0
            else 0
        )
        part_tc_2_10km = (
            tc_2_10km / home_work_staff_2_10km * 100
            if home_work_staff_2_10km != 0
            else 0
        )

        data["Nombre d'actifs [2-10] km"] = home_work_staff_2_10km
        data["Distance moyenne [2-10] km"] = home_work_avg_km_2_10km
        data["Part modale voiture + 2 roues pour trajet [2-10] km"] = (
            part_car_2wheels_2_10km
        )
        data["Part modale marche à pied / vélo pour trajet [2-10] km"] = (
            part_velo_pied_2_10km
        )
        data["Part modale transport en commun pour trajet [2-10] km"] = part_tc_2_10km

        ############## between [10-80km]

        home_work_staff_10_80km = total_10_80km
        home_work_avg_km_10_80km = (
            total_km_10_80km / cpt_10_80km if cpt_10_80km != 0 else 0
        )

        part_car_2wheels_10_80km = (
            car_2wheels_10_80km / home_work_staff_10_80km * 100
            if home_work_staff_10_80km != 0
            else 0
        )
        part_velo_pied_10_80km = (
            velo_pied_10_80km / home_work_staff_10_80km * 100
            if home_work_staff_10_80km != 0
            else 0
        )
        part_tc_10_80km = (
            tc_10_80km / home_work_staff_10_80km * 100
            if home_work_staff_10_80km != 0
            else 0
        )

        data["Nombre d'actifs [10-80] km"] = home_work_staff_10_80km
        data["Distance moyenne [10-80] km"] = home_work_avg_km_10_80km
        data["Part modale voiture + 2 roues pour trajet [10-80] km"] = (
            part_car_2wheels_10_80km
        )
        data["Part modale marche à pied / vélo pour trajet [10-80] km"] = (
            part_velo_pied_10_80km
        )
        data["Part modale transport en commun pour trajet [10-80] km"] = part_tc_10_80km

        ## get avg km traveled out home work distance
        km_traveld_share_hw_france_2km = (
            get_input_param(input_param, "donnees-france", "Part km travail < 2 km")
            / 100
        )
        km_traveld_share_hw_france_2_10km = (
            get_input_param(input_param, "donnees-france", "Part km travail [2-10] km")
            / 100
        )
        km_traveld_share_hw_france_10_80km = (
            get_input_param(input_param, "donnees-france", "Part km travail [10-80] km")
            / 100
        )
        km_traveld_share_out_hw_france_2km = (
            get_input_param(
                input_param, "donnees-france", "Part km hors travail < 2 km"
            )
            / 100
        )
        km_traveld_share_out_hw_france_2_10km = (
            get_input_param(
                input_param, "donnees-france", "Part km hors travail [2-10] km"
            )
            / 100
        )
        km_traveld_share_out_hw_france_10_80km = (
            get_input_param(
                input_param, "donnees-france", "Part km hors travail [10-80] km"
            )
            / 100
        )

        term1 = (
            home_work_avg_km_less_2km
            / km_traveld_share_hw_france_2km
            * km_traveld_share_out_hw_france_2km
        )
        term2 = (
            home_work_avg_km_2_10km
            / km_traveld_share_hw_france_2_10km
            * km_traveld_share_out_hw_france_2_10km
        )
        term3 = (
            home_work_avg_km_10_80km
            / km_traveld_share_hw_france_10_80km
            * km_traveld_share_out_hw_france_10_80km
        )

        data["Part km hors travail < 2 km"] = (
            term1 / (term1 + term2 + term3) if (term1 + term2 + term3) != 0 else 0
        )
        data["Part km hors travail [2-10] km"] = (
            term2 / (term1 + term2 + term3) if (term1 + term2 + term3) != 0 else 0
        )
        data["Part km hors travail [10-80] km"] = (
            term3 / (term1 + term2 + term3) if (term1 + term2 + term3) != 0 else 0
        )

        # get data accessible from the indicator vehicule fleet
        # get the name of vehicule fleet table for the selected region
        vehicle_fleet_desc = await self.query("DataSet.VEHICLE_FLEET")
        vehicle_fleet_table = await self.filter(
            vehicle_fleet_desc, "DataSet.VEHICLE_FLEET", "match"
        )
        # get the name of VehicleFuel column for the selected region
        vehicle_fuel_column = await self.filter(
            vehicle_fleet_desc, "DataSet.VEHICLE_FLEET/VehicleFuel", "match"
        )
        # get the name of VehicleType column for the selected region
        vehicle_type_column = await self.filter(
            vehicle_fleet_desc, "DataSet.VEHICLE_FLEET/VehicleType", "match"
        )

        # get max year for mobilite_insee data
        max_year_vehicle_fleet = await self.get_max_year(vehicle_fleet_table)

        # get category id
        vehicle_type_column = await self.filter(
            vehicle_fleet_desc, "DataSet.VEHICLE_FLEET/VehicleType", "match"
        )
        vehicle_type_column = await self.filter(
            vehicle_fleet_desc, "DataSet.VEHICLE_FLEET/VehicleType", "match"
        )
        vehicle_type_column = await self.filter(
            vehicle_fleet_desc, "DataSet.VEHICLE_FLEET/VehicleType", "match"
        )

        parc_auto_data = await self.fetch_data_analysis(
            vehicle_fleet_table,
            "a.valeur, a.{vehicle_type}, a.{vehicle_fuel}".format(
                vehicle_type=vehicle_type_column, vehicle_fuel=vehicle_fuel_column
            ),
            year_condition="a.annee = $3",
            params=[max_year_vehicle_fleet],
        )

        # voiture particulier
        vp_total = 0
        vp_diesel_essence = 0
        vp_elec = 0
        vp_gnv = 0
        # cars et bus
        tc_total = 0
        tc_diesel_essence = 0
        tc_elec = 0
        tc_gnv = 0
        # poids lourds
        pl_total = 0
        pl_diesel = 0
        pl_gnv = 0

        # vehicle type ids
        pv_id = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleType.PASSENGER_CARS",
            "match",
            True,
        )
        pt_id = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleType.BUSES_COACHES",
            "match",
            True,
        )
        hw_id = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleType.TRUCKS",
            "match",
            True,
        )

        # vehicle fuel ids
        diesl = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleFuel.DIESEL",
            "match",
            True,
        )
        gasoline = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleFuel.GASOLINE",
            "match",
            True,
        )
        elec_h2 = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleFuel.ELEC_H2",
            "match",
            True,
        )
        electric = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleFuel.ELECTRIC",
            "match",
            True,
        )
        gas = await self.filter(
            vehicle_fleet_desc, "DataSet.VEHICLE_FLEET/VehicleFuel.GAS", "match", True
        )
        hybrid_diesel = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_DIESEL",
            "match",
            True,
        )
        hybrid_gasoline = await self.filter(
            vehicle_fleet_desc,
            "DataSet.VEHICLE_FLEET/VehicleFuel.PLUG_HYBRID_GASOLINE ",
            "match",
            True,
        )

        for item in parc_auto_data:
            ## vp = passenger car
            if item["type_veh"] == pv_id:
                vp_total = vp_total + item["valeur"]
                if item["energie_carburant"] in [diesl, gasoline, elec_h2, electric]:
                    vp_diesel_essence = vp_diesel_essence + item["valeur"]
                if item["energie_carburant"] in [hybrid_diesel, hybrid_gasoline]:
                    vp_elec = vp_elec + item["valeur"]
                if item["energie_carburant"] == gas:
                    vp_gnv = vp_gnv + item["valeur"]
            ## tc : cars and bus
            elif item["type_veh"] == pt_id:
                tc_total = tc_total + item["valeur"]
                if item["energie_carburant"] in [diesl, gasoline, elec_h2, electric]:
                    tc_diesel_essence = tc_diesel_essence + item["valeur"]
                if item["energie_carburant"] in [hybrid_diesel, hybrid_gasoline]:
                    tc_elec = tc_elec + item["valeur"]
                if item["energie_carburant"] == gas:
                    tc_gnv = tc_gnv + item["valeur"]

            ## heavy weight
            elif item["type_veh"] == hw_id:
                pl_total = pl_total + item["valeur"]
                if item["energie_carburant"] in [diesl, gasoline, elec_h2, electric]:
                    pl_diesel = pl_diesel + item["valeur"]
                if item["energie_carburant"] == gas:
                    pl_gnv = pl_gnv + item["valeur"]

        ## pourcentage

        part_vp_diesel_essence = (
            vp_diesel_essence / vp_total * 100 if vp_total != 0 else 0
        )
        part_vp_elec = vp_elec / vp_total * 100 if vp_total != 0 else 0
        part_vp_gnv = vp_gnv / vp_total * 100 if vp_total != 0 else 0

        part_tc_diesel_essence = (
            tc_diesel_essence / tc_total * 100 if tc_total != 0 else 0
        )
        part_tc_elec = tc_elec / tc_total * 100 if tc_total != 0 else 0
        part_tc_gnv = tc_gnv / tc_total * 100 if tc_total != 0 else 0

        part_pl_diesel = pl_diesel / pl_total * 100 if pl_total != 0 else 0
        part_pl_gnv = pl_gnv / pl_total * 100 if pl_total != 0 else 0

        # Véhicule particulier
        data["Part moteur diesel/essence - véhicule particulier"] = (
            part_vp_diesel_essence
        )
        data["Part moteur électrique - véhicule particulier"] = part_vp_elec
        data["Part moteur GNV - véhicule particulier"] = part_vp_gnv
        data["Part moteur hydrogène - véhicule particulier"] = 0  # Hypothese
        # Cars et bus
        data["Part moteur diesel/essence - bus et cars"] = part_tc_diesel_essence
        data["Part moteur électrique - bus et cars"] = part_tc_elec
        data["Part moteur GNV - bus et cars"] = part_tc_gnv
        data["Part moteur hydrogène - bus et cars"] = 0  # Hypothese
        # Poids lourds
        data["Part moteur diesel - poids lourds"] = (
            part_pl_diesel if (part_pl_diesel + part_pl_gnv) != 0 else 100
        )  # Hydrogen must be added if present in the database
        data["Part moteur GNV - poids lourds"] = part_pl_gnv
        data["Part moteur hydrogène - poids lourds"] = 0  # Hypothese

        # Energy consumption (GWh)
        if "impact-ener" in activated_impacts:
            # get the name of energy consumption table for the selected region
            conso_ener_desc = await self.query("DataSet.CONSUMPTION")
            conso_ener_cf_query, conso_ener_cf_params = await self.get_custom_filters(
                "DataSet.CONSUMPTION", 5
            )
            conso_ener_table = await self.filter(
                conso_ener_desc, "DataSet.CONSUMPTION", "match"
            )
            conso_ener_unit_in = await self.filter(
                conso_ener_desc, "DataSet.CONSUMPTION", "data_unit"
            )
            # get the name of energy consumption categories column for the selected region
            conso_ener_sector_column = await self.filter(
                conso_ener_desc, "DataSet.CONSUMPTION/Sector", "match"
            )
            conso_ener_usage_column = await self.filter(
                conso_ener_desc, "DataSet.CONSUMPTION/Usage", "match"
            )

            # get the id of energy consumption categories for the selected region
            conso_ener_sector_id = await self.filter(
                conso_ener_desc, "DataSet.CONSUMPTION/Sector.ROAD_TRANSPORT", "match"
            )

            # get specific data for the selected region
            # get max year for conso_ener data
            max_year_cons_ener = await self.get_max_year(conso_ener_table)
            cons_ener_data = await self.fetch_data_analysis(
                conso_ener_table,
                "*",
                "a.annee = $3 ",
                f"a.{conso_ener_sector_column}::int = $4 " + conso_ener_cf_query,
                params=[max_year_cons_ener, int(conso_ener_sector_id)]
                + conso_ener_cf_params,
            )

            conso_ener_total = 0
            ener_cons_passenger_transport = 0
            ener_cons_goods_transport = 0

            # usage ids
            conso_passenger_transport_id = await self.filter(
                conso_ener_desc,
                "DataSet.CONSUMPTION/Usage.PASSENGER_TRANSPORT",
                "match",
                True,
            )
            conso_goods_transport_id = await self.filter(
                conso_ener_desc,
                "DataSet.CONSUMPTION/Usage.GOODS_TRANSPORT",
                "match",
                True,
            )

            # Energy consumption - transport
            for item in cons_ener_data:
                conso_ener_total = conso_ener_total + item["valeur"]
                if level > 0:
                    if item[conso_ener_usage_column] == conso_passenger_transport_id:
                        ener_cons_passenger_transport = (
                            ener_cons_passenger_transport + item["valeur"]
                        )
                    elif item[conso_ener_usage_column] == (conso_goods_transport_id):
                        ener_cons_goods_transport = (
                            ener_cons_goods_transport + item["valeur"]
                        )

            data["Consommation énergétique - transport routier"] = self.unit_converter(
                "impact-ener", conso_ener_unit_in, conso_ener_total
            )
            if level > 0:
                data[
                    "Consommation énergétique - transport routier - transport de personnes"
                ] = self.unit_converter(
                    "impact-ener", conso_ener_unit_in, ener_cons_passenger_transport
                )
                data[
                    "Consommation énergétique - transport routier - transport de marchandises"
                ] = self.unit_converter(
                    "impact-ener", conso_ener_unit_in, ener_cons_goods_transport
                )

        # GHG emissions (ktCO2eq) - transport
        if "impact-carbone" in activated_impacts:
            # get the name of ghg emissions table for the selected region
            ghg_emissions_desc = await self.query("DataSet.EMISSIONS")
            (
                ghg_emissions_cf_query,
                ghg_emissions_cf_params,
            ) = await self.get_custom_filters("DataSet.EMISSIONS", 5)
            ghg_emissions_table = await self.filter(
                ghg_emissions_desc, "DataSet.EMISSIONS", "match"
            )
            ghg_unit_in = await self.filter(
                ghg_emissions_desc, "DataSet.EMISSIONS", "data_unit"
            )
            # get the name of ghg emissions sectcategories column for the selected region
            ghg_emissions_sector_column = await self.filter(
                ghg_emissions_desc, "DataSet.EMISSIONS/Sector", "match"
            )
            ghg_emissions_usage_column = await self.filter(
                ghg_emissions_desc, "DataSet.EMISSIONS/Usage", "match"
            )

            # get the id of ghg emissions categories for the selected region
            ghg_emissions_sector_id = await self.filter(
                ghg_emissions_desc,
                "DataSet.EMISSIONS/Sector.ROAD_TRANSPORT",
                "match",
                True,
            )

            # get max year for ghg_emission data
            max_year_ghg = await self.get_max_year(ghg_emissions_table)

            ghg_data = await self.fetch_data_analysis(
                ghg_emissions_table,
                "*",
                "a.annee = $3 ",
                f"a.{ghg_emissions_sector_column}::int = $4 " + ghg_emissions_cf_query,
                params=[max_year_ghg, int(ghg_emissions_sector_id)]
                + ghg_emissions_cf_params,
            )

            ghg_emissions_total = 0
            ghg_emissions_passenger_transport = 0
            ghg_emissions_goods_transport = 0

            # usage ids
            ghg_passenger_transport_id = await self.filter(
                ghg_emissions_desc,
                "DataSet.EMISSIONS/Usage.PASSENGER_TRANSPORT",
                "match",
                True,
            )
            ghg_goods_transport_id = await self.filter(
                ghg_emissions_desc,
                "DataSet.EMISSIONS/Usage.GOODS_TRANSPORT",
                "match",
                True,
            )

            for item in ghg_data:
                ghg_emissions_total = ghg_emissions_total + item["valeur"]
                if level > 0:
                    if item[ghg_emissions_usage_column] == ghg_passenger_transport_id:
                        ghg_emissions_passenger_transport = (
                            ghg_emissions_passenger_transport + item["valeur"]
                        )
                    elif item[ghg_emissions_usage_column] == ghg_goods_transport_id:
                        ghg_emissions_goods_transport = (
                            ghg_emissions_goods_transport + item["valeur"]
                        )

            data["Emissions GES - transport routier"] = self.unit_converter(
                "impact-carbone", ghg_unit_in, ghg_emissions_total
            )
            if level > 0:
                data["Emissions GES - transport routier - transport de personnes"] = (
                    self.unit_converter(
                        "impact-carbone", ghg_unit_in, ghg_emissions_passenger_transport
                    )
                )
                data[
                    "Emissions GES - transport routier - transport de marchandises"
                ] = self.unit_converter(
                    "impact-carbone", ghg_unit_in, ghg_emissions_goods_transport
                )

        # Energy bill (€/MWh)
        if "impact-facture-ener" in activated_impacts:
            # get the name of energy bll table for the selected region
            energy_bill_desc = await self.query("DataSet.ENERGY_BILL")
            energy_bill_cf_query, energy_bill_cf_params = await self.get_custom_filters(
                "DataSet.ENERGY_BILL", 5
            )
            energy_bill_table = await self.filter(
                energy_bill_desc, "DataSet.ENERGY_BILL", "match"
            )
            energy_bill_unit_in = await self.filter(
                energy_bill_desc, "DataSet.ENERGY_BILL", "data_unit"
            )

            # get the name of energy bill categories column for the selected region
            energy_bill_sector_column = await self.filter(
                energy_bill_desc, "DataSet.ENERGY_BILL/Sector", "match"
            )
            energy_bill_usage_column = await self.filter(
                energy_bill_desc, "DataSet.ENERGY_BILL/Usage", "match"
            )

            # get the id of energy bill categories value for the selected region
            energy_bill_sector_id = await self.filter(
                energy_bill_desc,
                "DataSet.ENERGY_BILL/Sector.ROAD_TRANSPORT",
                "match",
                True,
            )
            # get max year for energy_bill data
            max_year_energy_bill = await self.get_max_year(energy_bill_table)

            energy_bill_data = await self.fetch_data_analysis(
                energy_bill_table,
                "*",
                "a.annee = $3 ",
                f"a.{energy_bill_sector_column}::int = $4 " + energy_bill_cf_query,
                params=[max_year_energy_bill, int(energy_bill_sector_id)]
                + energy_bill_cf_params,
            )

            energy_bill_total = 0
            energy_bill_passenger_transport = 0
            energy_bill_goods_transport = 0

            # usage ids
            energy_bill_passenger_transport_id = await self.filter(
                energy_bill_desc,
                "DataSet.ENERGY_BILL/Usage.PASSENGER_TRANSPORT",
                "match",
                True,
            )
            energy_bill_goods_transport_id = await self.filter(
                energy_bill_desc,
                "DataSet.ENERGY_BILL/Usage.GOODS_TRANSPORT",
                "match",
                True,
            )

            for item in energy_bill_data:
                energy_bill_total = energy_bill_total + item["valeur"]
                if level > 0:
                    if item[energy_bill_usage_column] == (
                        energy_bill_passenger_transport_id
                    ):
                        energy_bill_passenger_transport = (
                            energy_bill_passenger_transport + item["valeur"]
                        )
                    elif item[energy_bill_usage_column] == (
                        energy_bill_goods_transport_id
                    ):
                        energy_bill_goods_transport = (
                            energy_bill_goods_transport + item["valeur"]
                        )
            data["Facture énergétique - transport routier"] = self.unit_converter(
                "impact-facture-ener", energy_bill_unit_in, energy_bill_total
            )
            if level > 0:
                data[
                    "Facture énergétique - transport routier - transport de personnes"
                ] = self.unit_converter(
                    "impact-facture-ener",
                    energy_bill_unit_in,
                    energy_bill_passenger_transport,
                )
                data[
                    "Facture énergétique - transport routier - transport de marchandises"
                ] = self.unit_converter(
                    "impact-facture-ener",
                    energy_bill_unit_in,
                    energy_bill_goods_transport,
                )

        # Atmospheric pollutant emissions (t)
        if "impact-atmo" in activated_impacts and level > 0:
            # Nox
            nox_desc = await self.query("DataSet.POLLUTANT_NOX")
            nox_cf_query, nox_cf_params = await self.get_custom_filters(
                "DataSet.POLLUTANT_NOX", 5
            )
            nox_data_table = await self.filter(
                nox_desc, "DataSet.POLLUTANT_NOX", "match"
            )
            atmo_unit_in = await self.filter(
                nox_desc, "DataSet.POLLUTANT_NOX", "data_unit"
            )

            # get the name of energy consumption sector column for the selected region
            nox_sector_column = await self.filter(
                nox_desc, "DataSet.POLLUTANT_NOX/Sector", "match"
            )
            nox_usage_column = await self.filter(
                nox_desc, "DataSet.POLLUTANT_NOX/Usage", "match"
            )

            # get the id of energy consumption categories value for the selected region
            nox_sector_id = await self.filter(
                nox_desc, "DataSet.POLLUTANT_NOX/Sector.ROAD_TRANSPORT", "match", True
            )

            # max year
            max_year_nox = await self.get_max_year(nox_data_table)

            # get data
            nox_data = await self.fetch_data_analysis(
                nox_data_table,
                "*",
                "a.annee = $3 ",
                f"a.{nox_sector_column}::int = $4 " + nox_cf_query,
                params=[max_year_nox, int(nox_sector_id)] + nox_cf_params,
            )

            nox_total = 0
            nox_passenger_transport = 0
            nox_goods_transport = 0

            # usage ids
            nox_passenger_transport_id = await self.filter(
                nox_desc,
                "DataSet.POLLUTANT_NOX/Usage.PASSENGER_TRANSPORT",
                "match",
                True,
            )
            nox_goods_transport_id = await self.filter(
                nox_desc, "DataSet.POLLUTANT_NOX/Usage.GOODS_TRANSPORT", "match", True
            )
            for item in nox_data:
                nox_total = nox_total + item["valeur"]
                if level > 0:
                    if item[nox_usage_column] == nox_passenger_transport_id:
                        nox_passenger_transport = (
                            nox_passenger_transport + item["valeur"]
                        )
                    elif item[nox_usage_column] == nox_goods_transport_id:
                        nox_goods_transport = nox_goods_transport + item["valeur"]

            # PM10
            pm10_desc = await self.query("DataSet.POLLUTANT_PM10")
            pm10_cf_query, pm10_cf_params = await self.get_custom_filters(
                "DataSet.POLLUTANT_PM10", 5
            )
            # get the name of pm10 table for the selected region
            pm10_data_table = await self.filter(
                pm10_desc, "DataSet.POLLUTANT_PM10", "match"
            )

            # get the name of pm10 categories column for the selected region
            pm10_sector_column = await self.filter(
                pm10_desc, "DataSet.POLLUTANT_PM10/Sector", "match"
            )
            pm10_usage_column = await self.filter(
                pm10_desc, "DataSet.POLLUTANT_PM10/Usage", "match"
            )

            # get the id of pm10 categories for the selected region
            pm10_sector_id = await self.filter(
                pm10_desc, "DataSet.POLLUTANT_PM10/Sector.ROAD_TRANSPORT", "match", True
            )

            # max year
            max_year_pm10 = await self.get_max_year(pm10_data_table)

            pm10_data = await self.fetch_data_analysis(
                pm10_data_table,
                "*",
                "a.annee = $3 ",
                f"a.{pm10_sector_column}::int = $4 " + pm10_cf_query,
                params=[max_year_pm10, int(pm10_sector_id)] + pm10_cf_params,
            )

            pm10_total = 0
            pm10_passenger_transport = 0
            pm10_goods_transport = 0

            # usage ids
            pm10_passenger_transport_id = await self.filter(
                pm10_desc,
                "DataSet.POLLUTANT_PM10/Usage.PASSENGER_TRANSPORT",
                "match",
                True,
            )
            pm10_goods_transport_id = await self.filter(
                pm10_desc, "DataSet.POLLUTANT_PM10/Usage.GOODS_TRANSPORT", "match", True
            )

            for item in pm10_data:
                pm10_total = pm10_total + item["valeur"]
                if level > 0:
                    if item[pm10_usage_column] == pm10_passenger_transport_id:
                        pm10_passenger_transport = (
                            pm10_passenger_transport + item["valeur"]
                        )
                    elif item[pm10_usage_column] == pm10_goods_transport_id:
                        pm10_goods_transport = pm10_goods_transport + item["valeur"]

            # PM2.5
            pm25_desc = await self.query("DataSet.POLLUTANT_PM25")
            pm25_cf_query, pm25_cf_params = await self.get_custom_filters(
                "DataSet.POLLUTANT_PM25", 5
            )
            # get the name of pm25 table for the selected region
            pm25_data_table = await self.filter(
                pm25_desc, "DataSet.POLLUTANT_PM25", "match"
            )

            # get the name of pm25 categories column for the selected region
            pm25_sector_column = await self.filter(
                pm25_desc, "DataSet.POLLUTANT_PM25/Sector", "match"
            )
            pm25_usage_column = await self.filter(
                pm25_desc, "DataSet.POLLUTANT_PM25/Usage", "match"
            )

            # get the id of pm25 categories for the selected region
            pm25_sector_id = await self.filter(
                pm25_desc, "DataSet.POLLUTANT_PM25/Sector.ROAD_TRANSPORT", "match", True
            )

            # max year
            max_year_pm25 = await self.get_max_year(pm25_data_table)

            pm25_data = await self.fetch_data_analysis(
                pm25_data_table,
                "*",
                "a.annee = $3 ",
                f"a.{pm25_sector_column}::int = $4 " + pm25_cf_query,
                params=[max_year_pm25, int(pm25_sector_id)] + pm25_cf_params,
            )

            pm25_total = 0
            pm25_passenger_transport = 0
            pm25_goods_transport = 0

            # usage ids
            pm25_passenger_transport_id = await self.filter(
                pm25_desc,
                "DataSet.POLLUTANT_PM25/Usage.PASSENGER_TRANSPORT",
                "match",
                True,
            )
            pm25_goods_transport_id = await self.filter(
                pm25_desc, "DataSet.POLLUTANT_PM25/Usage.GOODS_TRANSPORT", "match", True
            )

            for item in pm25_data:
                pm25_total = pm25_total + item["valeur"]
                if level > 0:
                    if item[pm25_usage_column] == pm25_passenger_transport_id:
                        pm25_passenger_transport = (
                            pm25_passenger_transport + item["valeur"]
                        )
                    elif item[pm25_usage_column] == pm25_goods_transport_id:
                        pm25_goods_transport = pm25_goods_transport + item["valeur"]

            # COVNM
            covnm_desc = await self.query("DataSet.POLLUTANT_COVNM")
            covnm_cf_query, covnm_cf_params = await self.get_custom_filters(
                "DataSet.POLLUTANT_COVNM", 5
            )
            # get the name of covnm table for the selected region
            covnm_data_table = await self.filter(
                covnm_desc, "DataSet.POLLUTANT_COVNM", "match"
            )

            # get the name of covnm categories column for the selected region
            covnm_sector_column = await self.filter(
                covnm_desc, "DataSet.POLLUTANT_COVNM/Sector", "match"
            )
            covnm_usage_column = await self.filter(
                covnm_desc, "DataSet.POLLUTANT_COVNM/Usage", "match"
            )

            # get the id of covnm categories for the selected region
            covnm_sector_id = await self.filter(
                covnm_desc,
                "DataSet.POLLUTANT_COVNM/Sector.ROAD_TRANSPORT",
                "match",
                True,
            )

            # max year
            max_year_covnm = await self.get_max_year(covnm_data_table)

            covnm_data = await self.fetch_data_analysis(
                covnm_data_table,
                "*",
                "a.annee = $3 ",
                f"a.{covnm_sector_column}::int = $4 " + covnm_cf_query,
                params=[max_year_covnm, int(covnm_sector_id)] + covnm_cf_params,
            )

            covnm_total = 0
            covnm_passenger_transport = 0
            covnm_goods_transport = 0

            # usage ids
            covnm_passenger_transport_id = await self.filter(
                covnm_desc,
                "DataSet.POLLUTANT_COVNM/Usage.PASSENGER_TRANSPORT",
                "match",
                True,
            )
            covnm_goods_transport_id = await self.filter(
                covnm_desc,
                "DataSet.POLLUTANT_COVNM/Usage.GOODS_TRANSPORT",
                "match",
                True,
            )

            for item in covnm_data:
                covnm_total = covnm_total + item["valeur"]
                if level > 0:
                    if item[covnm_usage_column] == covnm_passenger_transport_id:
                        covnm_passenger_transport = (
                            covnm_passenger_transport + item["valeur"]
                        )
                    elif item[covnm_usage_column] == covnm_goods_transport_id:
                        covnm_goods_transport = covnm_goods_transport + item["valeur"]

            # formatting data
            data["Emissions NOx - transport routier"] = self.unit_converter(
                "impact-atmo", atmo_unit_in, nox_total
            )
            data["Emissions PM10 - transport routier"] = self.unit_converter(
                "impact-atmo", atmo_unit_in, pm10_total
            )
            data["Emissions PM25 - transport routier"] = self.unit_converter(
                "impact-atmo", atmo_unit_in, pm25_total
            )
            data["Emissions COVNM - transport routier"] = self.unit_converter(
                "impact-atmo", atmo_unit_in, covnm_total
            )
            data["Emissions NOx - transport routier - transport de personnes"] = (
                self.unit_converter(
                    "impact-atmo", atmo_unit_in, nox_passenger_transport
                )
            )
            data["Emissions PM10 - transport routier - transport de personnes"] = (
                self.unit_converter(
                    "impact-atmo", atmo_unit_in, pm10_passenger_transport
                )
            )
            data["Emissions PM25 - transport routier - transport de personnes"] = (
                self.unit_converter(
                    "impact-atmo", atmo_unit_in, pm25_passenger_transport
                )
            )
            data["Emissions COVNM - transport routier - transport de personnes"] = (
                self.unit_converter(
                    "impact-atmo", atmo_unit_in, covnm_passenger_transport
                )
            )
            data["Emissions NOx - transport routier - transport de marchandises"] = (
                self.unit_converter("impact-atmo", atmo_unit_in, nox_goods_transport)
            )
            data["Emissions PM10 - transport routier - transport de marchandises"] = (
                self.unit_converter("impact-atmo", atmo_unit_in, pm10_goods_transport)
            )
            data["Emissions PM25 - transport routier - transport de marchandises"] = (
                self.unit_converter("impact-atmo", atmo_unit_in, pm25_goods_transport)
            )
            data["Emissions COVNM - transport routier - transport de marchandises"] = (
                self.unit_converter("impact-atmo", atmo_unit_in, covnm_goods_transport)
            )

        return data

    async def inter_calcul(self, A, B, C, level, activated_impacts):
        """
        this fonction is used to perform the intermediate calculations necessary to calculate the different impacts of the simulator for each category

        Parameters
        ----------
        A : dict
            specific input parameters
        B : dict
            generic input paramateres
        C : dict
            generic intermediate paramateres

        Returns
        -------
        data : dict

        """

        inter_calcul = defaultdict(dict)

        # Energy consumption impacts
        consumptiond = {}
        if "impact-ener" in activated_impacts:
            consumptiond = consumption.get_consumption(A, B, C, level)

        # GHG emissions
        ghg_dict = {}
        if "impact-carbone" in activated_impacts:
            ghg_dict = ghg.get_ghg(A, B, C, level)

        # Energy bill impacts
        energy_bill_dict = {}
        if "impact-facture-ener" in activated_impacts:
            energy_bill_dict = energy_bill.get_energy_bill(A, B, C, level)
        if level > 0 and "impact-atmo" in activated_impacts:
            # COVNM
            covnm = air_pollutant.get_pollutant(A, B, C, level, "COVNM", "fe_covnm")
            # PM10
            pm10 = air_pollutant.get_pollutant(A, B, C, level, "PM10", "fe_pm10")
            # PM25
            pm25 = air_pollutant.get_pollutant(A, B, C, level, "PM25", "fe_pm25")
            # NOx
            nox = air_pollutant.get_pollutant(A, B, C, level, "NOx", "fe_nox")

            inter_calcul["covnm"] = covnm
            inter_calcul["pm10"] = pm10
            inter_calcul["pm25"] = pm25
            inter_calcul["nox"] = nox

        # Final result
        inter_calcul["consumption"] = consumptiond
        inter_calcul["ghg"] = ghg_dict
        inter_calcul["energy_bill"] = energy_bill_dict

        return inter_calcul

    def get_default_settings(self, A, B):
        """
        get default settings for actions
        """

        # ('1a','Réduire le nombre d''actifs devant se déplacer', 1),
        # ('1b','Réduire le nombre de km parcourus par trajet', 1),
        # ('2a','Réduire le nombre de km à parcourir pour accéder aux services & loisirs', 2),
        # ('3a','Réduire le nombre de km pour le transport de marchandise', 3),
        # ('4a','Evolution part modale vélo/marche à pied vs voiture - trajets < 3km', 4),
        # ('4b','Evolution part modale vélo vs voiture - trajets de 3 à 10km', 4),
        # ('5a','Evolution part modale transport en commun vs voiture - trajets > 10km', 5),
        # ('6a','Développement de covoiturage', 6),
        # ('7a','Changement de motorisation des véhicules particuliers', 7),
        # ('8a','Changement de motorisation des bus et cars',8),
        # ('9a','Changement de motorisation poids lourds', 9);

        share_carpool_users = next(
            (
                x["value"]
                for x in B
                if (
                    x["data"] == "domicile-travail"
                    and x["label"] == "Nombre d'actifs faisant du covoiturage"
                )
            ),
            0,
        )
        default_avg_carpoolers = next(
            (
                x["value"]
                for x in B
                if (
                    x["data"] == "domicile-travail"
                    and x["label"] == "Nombre de covoitureurs en moyenne par défaut"
                )
            ),
            0,
        )

        share_biognv = next(
            (
                x["value"]
                for x in B
                if (
                    x["data"] == "motorisation"
                    and x["label"] == "Part de bioGNV dans le GNV"
                )
            ),
            0,
        )

        share_biofuel = next(
            (
                x["value"]
                for x in B
                if (
                    x["data"] == "motorisation"
                    and x["label"] == "Part de biocarburant dans essence / diesel"
                )
            ),
            0,
        )

        greenhydro_share = next(
            (
                x["value"]
                for x in B
                if (
                    x["data"] == "motorisation"
                    and x["label"] == "Part d'hydrogène vert dans l'hydrogène"
                )
            ),
            0,
        )
        # settings for complex actions
        ## carpooling
        default_6a = {
            1: {
                "id": "share_carpool_users",
                "title": "Part de personnes faisant du covoiturage (%)",
                "default": share_carpool_users,
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            2: {
                "id": "avg_carpoolers",
                "title": "Nombre moyen de covoitureurs par véhicule",
                "default": default_avg_carpoolers,
                "min": 1,
                "max": 4,
                "type": "primary",
            },
        }

        ## vp
        default_7a = {
            1: {
                "id": "diesel_essence",
                "title": "Diesel/ essence",
                "default": A["Part moteur diesel/essence - véhicule particulier"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            2: {
                "id": "elec",
                "title": "Electrique",
                "default": A["Part moteur électrique - véhicule particulier"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            3: {
                "id": "gnv",
                "title": "GNV",
                "default": A["Part moteur GNV - véhicule particulier"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            4: {
                "id": "hydro",
                "title": "Hydrogène",
                "default": A["Part moteur hydrogène - véhicule particulier"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            5: {
                "id": "biofuel_share",  # get it from DB
                "title": "Part biocarburant dans essence/diesel",
                "default": share_biofuel,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
            6: {
                "id": "biognv_share",  # get it from DB
                "title": "Part bioGNV dans le GNV",
                "default": share_biognv,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
            7: {
                "id": "greenhydro_share",  # get it from DB
                "title": "Part hydrogène vert dans l'hydrogène",
                "default": greenhydro_share,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
        }

        ## pt
        default_8a = {
            1: {
                "id": "diesel_essence",
                "title": "Diesel/ essence",
                "default": A["Part moteur diesel/essence - bus et cars"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            2: {
                "id": "elec",
                "title": "Electrique",
                "default": A["Part moteur électrique - bus et cars"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            3: {
                "id": "gnv",
                "title": "GNV",
                "default": A["Part moteur GNV - bus et cars"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            4: {
                "id": "hydro",
                "title": "Hydrogène",
                "default": A["Part moteur hydrogène - bus et cars"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            5: {
                "id": "biofuel_share",
                "title": "Part biocarburant dans essence/diesel",
                "default": share_biofuel,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
            6: {
                "id": "biognv_share",
                "title": "Part bioGNV dans le GNV",
                "default": share_biognv,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
            7: {
                "id": "greenhydro_share",
                "title": "Part hydrogène vert dans l'hydrogène",
                "default": greenhydro_share,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
        }

        ## hw
        default_9a = {
            1: {
                "id": "diesel_essence",
                "title": "Diesel/ essence",
                "default": A["Part moteur diesel - poids lourds"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            2: {
                "id": "gnv",
                "title": "GNV",
                "default": A["Part moteur GNV - poids lourds"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            3: {
                "id": "hydro",
                "title": "Hydrogène",
                "default": A["Part moteur hydrogène - poids lourds"],
                "min": 0,
                "max": 100,
                "type": "primary",
            },
            4: {
                "id": "biofuel_share",
                "title": "Part biocarburant dans essence/diesel",
                "default": share_biofuel,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
            5: {
                "id": "biognv_share",
                "title": "Part bioGNV dans le GNV",
                "default": share_biognv,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
            6: {
                "id": "greenhydro_share",
                "title": "Part hydrogène vert dans l'hydrogène",
                "default": greenhydro_share,
                "min": 0,
                "max": 100,
                "type": "secondary",
            },
        }

        settings = {
            "1a": {
                "type": "simple",
                "default": A["Nombre d'actifs domicile-travail"],
                "min": 0,
                "max": A["Nombre d'actifs domicile-travail"],
            },
            "1b": {
                "type": "simple",
                "default": A["Distance moyenne domicile-travail"],
                "min": 0,
                "max": A["Distance moyenne domicile-travail"],
            },
            "2a": {"type": "simple", "default": 100, "min": 0, "max": 100},  # 100%
            "3a": {"type": "simple", "default": 100, "min": 0, "max": 100},  # 100%
            "4a": {
                "type": "simple",
                "default": A["Part modale marche à pied / vélo pour trajet < 2 km"],
                "min": 0,
                "max": (100 - A["Part modale transport en commun pour trajet < 2 km"]),
            },
            "4b": {
                "type": "simple",
                "default": A["Part modale marche à pied / vélo pour trajet [2-10] km"],
                "min": 0,
                "max": (
                    100 - A["Part modale transport en commun pour trajet [2-10] km"]
                ),
            },
            "5a": {
                "type": "simple",
                "default": A["Part modale transport en commun pour trajet [10-80] km"],
                "min": 0,
                "max": (
                    100 - A["Part modale marche à pied / vélo pour trajet [10-80] km"]
                ),
            },
            "6a": {
                "type": "complex",
                "default": default_6a,
                "summable": False,
            },
            "7a": {
                "type": "complex",
                "default": default_7a,
                "summable": True,
            },  # complex action => TODO : seperate simple and complex action in BD
            "8a": {
                "type": "complex",
                "default": default_8a,
                "summable": True,
            },  # complex action
            "9a": {
                "type": "complex",
                "default": default_9a,
                "summable": True,
            },  # complex action
        }

        return settings

    def get_default_impacts(self, inter_calcul, level, activated_impacts):
        """
        return the initial values of the impacts for Energy consumption, GHG, energy bill and pollutants
        """

        impact_ener = {}
        impact_ghg = {}
        impact_energy_bill = {}
        impact_atmo = {}
        if "impact-ener" in activated_impacts:
            impact_ener = {
                "type": "simple",
                "default": round(
                    inter_calcul["consumption"]["before"]["total_conso_transport"]
                ),
            }
        if "impact-carbone" in activated_impacts:
            impact_ghg = {
                "type": "simple",
                "default": round(inter_calcul["ghg"]["before"]["total_ghg_transport"]),
            }

        if "impact-facture-ener" in activated_impacts:
            impact_energy_bill = {
                "type": "simple",
                "default": round(
                    inter_calcul["energy_bill"]["before"]["total_energy_bill_transport"]
                ),
            }

        if "impact-atmo" in activated_impacts:
            # add sub impact for atmo impact
            subimpactatmo = {}
            if level > 0:
                subimpactatmo = {
                    1: {
                        "id": "nox",
                        "title": "NOx",
                        "default": round(
                            inter_calcul["nox"]["before"]["total_emission_transport"]
                        ),
                    },
                    2: {
                        "id": "pm10",
                        "title": "PM10",
                        "default": round(
                            inter_calcul["pm10"]["before"]["total_emission_transport"]
                        ),
                    },
                    3: {
                        "id": "pm25",
                        "title": "PM2.5",
                        "default": round(
                            inter_calcul["pm25"]["before"]["total_emission_transport"]
                        ),
                    },
                    4: {
                        "id": "covnm",
                        "title": "COVNM",
                        "default": round(
                            inter_calcul["covnm"]["before"]["total_emission_transport"]
                        ),
                    },
                }
            impact_atmo = {"type": "complex", "default": subimpactatmo}

        settings = {
            "impact-ener": impact_ener,
            "impact-carbone": impact_ghg,
            "impact-atmo": impact_atmo,
            "impact-facture-ener": impact_energy_bill,
            # "other-impacts": 200,
        }

        return settings

    async def get_metadata(self):
        """
        Get metadata related to the mobility simulation.
        """
        simulator = {}
        metadata_mobility = await self.get_configuration()

        if isinstance(metadata_mobility["level"], dict):
            simulator["metadata"] = metadata_mobility
            return simulator

        # Get region configuration (which categories, levers and impacts are activated)
        activated_impacts = await self.get_region_impact_configuration()

        # Get generic data (consumption, emission factors, energy bill)
        generic_input_param = await self.load_generic_input_data()

        # Get generic data for intermediate computation (consumption, emission factors, energy bill)
        generic_inter_param = await self.load_generic_inter_data()

        # Get specific data for the selected region
        specific_input_param = await self.load_specific_data(
            metadata_mobility["level"], activated_impacts, generic_input_param
        )

        # Get actions default params
        actions_params = self.get_default_settings(
            specific_input_param, generic_input_param
        )

        # Get intermediate calculations for energy, GHG, energy bill, and pollutant impacts
        intermediate_calcul = await self.inter_calcul(
            specific_input_param,
            generic_input_param,
            generic_inter_param,
            metadata_mobility["level"],
            activated_impacts,
        )

        # Get impacts default params
        impacts_params = self.get_default_impacts(
            intermediate_calcul, metadata_mobility["level"], activated_impacts
        )

        simulator["metadata"] = metadata_mobility
        simulator["generic_input_param"] = generic_input_param
        simulator["specific_input_param"] = specific_input_param
        simulator["generic_inter_param"] = generic_inter_param
        simulator["intermediate_compute"] = intermediate_calcul
        simulator["actions_params"] = actions_params
        simulator["impacts_params"] = impacts_params
        simulator["activated_impacts"] = activated_impacts

        return simulator
