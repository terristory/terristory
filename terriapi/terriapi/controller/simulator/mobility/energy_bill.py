# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


from collections import defaultdict

from terriapi.controller.simulator.utils import get_input_param, get_inter_param


def get_energy_bill(A, B, C, level):
    energy_bill = defaultdict(dict)

    ## Move less (suffix _ML)
    """
        C_ML = Distance parcourue avant action - indice 100 domicile-travail Transport de personnes (100%)
        D_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de personnes (100%)
        E_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de marchandise (100%)
    """
    C_ML = 1

    ## Move efficiently (suffix _ME)
    """
        O_ME = Facture moyenne par km avant action (k€/veh.km) Transport de personnes Voiture
        P_ME = Facture moyenne par km avant action (k€/veh.km) Transport de personnes Bus et cars
        Q_ME = Facture moyenne par km avant action (k€/veh.km) Transport de marchandise Poids lourds
        R_ME = Facture moyenne par km avant action (k€/veh.km) Transport de personnes Vélo / marche à pied
        
    """

    ### passanger transport
    conso_vp_diesel_essence = get_inter_param(
        C, "consommation", "véhicule particulier", "diesel_essence"
    )
    conso_vp_elec = get_inter_param(
        C, "consommation", "véhicule particulier", "électrique"
    )
    conso_vp_gnv = get_inter_param(C, "consommation", "véhicule particulier", "GNV")
    conso_vp_hydrogen = get_inter_param(
        C, "consommation", "véhicule particulier", "hydrogène"
    )

    biofuel_share = (
        get_input_param(B, "motorisation", "Part de biocarburant dans essence / diesel")
        / 100
    )
    biognv_share = (
        get_input_param(B, "motorisation", "Part de bioGNV dans le GNV") / 100
    )
    share_greenhydro = (
        get_input_param(B, "motorisation", "Part d'hydrogène vert dans l'hydrogène")
        / 100
    )

    fuel_price = get_inter_param(C, "facture_ener", "carburant", "diesel_essence")
    organo_fuel_price = get_inter_param(C, "facture_ener", "carburant", "organo")
    elec_price = get_inter_param(C, "facture_ener", "recharge", "électrique")
    gnv_price = get_inter_param(C, "facture_ener", "carburant", "GNV")
    hydrogen_price = get_inter_param(C, "facture_ener", "recharge", "hydrogène")
    greenhydrogen_price = get_inter_param(
        C, "facture_ener", "recharge", "hydrogènevert"
    )
    biognv_price = get_inter_param(C, "facture_ener", "carburant", "bioGNV")

    O_ME = (
        (
            A["Part moteur diesel/essence - véhicule particulier"]
            / 100
            * (1 - biofuel_share)
            * conso_vp_diesel_essence
            * fuel_price
        )
        + (
            A["Part moteur diesel/essence - véhicule particulier"]
            / 100
            * biofuel_share
            * conso_vp_diesel_essence
            * organo_fuel_price
        )
        + (
            A["Part moteur électrique - véhicule particulier"]
            / 100
            * conso_vp_elec
            * elec_price
        )
        + (
            A["Part moteur GNV - véhicule particulier"]
            / 100
            * (1 - biognv_share)
            * conso_vp_gnv
            * gnv_price
        )
        + (
            A["Part moteur GNV - véhicule particulier"]
            / 100
            * biognv_share
            * conso_vp_gnv
            * biognv_price
        )
        + (
            A["Part moteur hydrogène - véhicule particulier"]
            / 100
            * (1 - share_greenhydro)
            * conso_vp_hydrogen
            * hydrogen_price
        )
        + (
            A["Part moteur hydrogène - véhicule particulier"]
            / 100
            * share_greenhydro
            * conso_vp_hydrogen
            * greenhydrogen_price
        )
    ) / 10

    ### public transport
    conso_pt_diesel_essence = get_inter_param(
        C, "consommation", "bus et cars", "diesel_essence"
    )
    conso_pt_elec = get_inter_param(C, "consommation", "bus et cars", "électrique")
    conso_pt_gnv = get_inter_param(C, "consommation", "bus et cars", "GNV")
    conso_pt_hydrogen = get_inter_param(C, "consommation", "bus et cars", "hydrogène")

    P_ME = (
        (
            A["Part moteur diesel/essence - bus et cars"]
            / 100
            * (1 - biofuel_share)
            * conso_pt_diesel_essence
            * fuel_price
        )
        + (
            A["Part moteur diesel/essence - bus et cars"]
            / 100
            * biofuel_share
            * conso_pt_diesel_essence
            * organo_fuel_price
        )
        + (A["Part moteur électrique - bus et cars"] / 100 * conso_pt_elec * elec_price)
        + (
            A["Part moteur GNV - bus et cars"]
            / 100
            * (1 - biognv_share)
            * conso_pt_gnv
            * gnv_price
        )
        + (
            A["Part moteur GNV - bus et cars"]
            / 100
            * biognv_share
            * conso_pt_gnv
            * biognv_price
        )
        + (
            A["Part moteur hydrogène - bus et cars"]
            / 100
            * (1 - share_greenhydro)
            * conso_pt_hydrogen
            * hydrogen_price
        )
        + (
            A["Part moteur hydrogène - bus et cars"]
            / 100
            * share_greenhydro
            * conso_pt_hydrogen
            * greenhydrogen_price
        )
    ) / 10

    ### heavyweight transport
    conso_hw_diesel_essence = get_inter_param(
        C, "consommation", "poids lourd", "diesel"
    )
    conso_hw_hydrogen = get_inter_param(C, "consommation", "poids lourd", "hydrogène")
    conso_hw_gnv = get_inter_param(C, "consommation", "poids lourd", "GNV")

    Q_ME = (
        (
            A["Part moteur diesel - poids lourds"]
            / 100
            * (1 - biofuel_share)
            * conso_hw_diesel_essence
            * fuel_price
        )
        + (
            A["Part moteur diesel - poids lourds"]
            / 100
            * biofuel_share
            * conso_hw_diesel_essence
            * organo_fuel_price
        )
        + (
            A["Part moteur GNV - poids lourds"]
            / 100
            * (1 - biognv_share)
            * conso_hw_gnv
            * gnv_price
        )
        + (
            A["Part moteur GNV - poids lourds"]
            / 100
            * biognv_share
            * conso_hw_gnv
            * biognv_price
        )
        + (
            A["Part moteur hydrogène - poids lourds"]
            / 100
            * (1 - share_greenhydro)
            * conso_hw_hydrogen
            * hydrogen_price
        )
        + (
            A["Part moteur hydrogène - poids lourds"]
            / 100
            * share_greenhydro
            * conso_hw_hydrogen
            * greenhydrogen_price
        )
    ) / 10

    ## Move differently (suffix _MD)
    """
        O_MD = Facture moyenne par km avant action (k€/pers.km) domicile-travail transport personnes distance < 2km 
        P_MD = Facture moyenne par km avant action (k€/pers.km) domicile-travail transport personnes distance 2-10km
        Q_MD = Facture moyenne par km avant action (k€/pers.km) domicile-travail transport personnes distance 10-80km
        R_MD = Facture moyenne par km avant action (k€/pers.km) domicile-travail transport personnes toute distance(>80km)
        S_MD = Facture moyenne par km avant action (k€/pers.km) hors domicile-travail transport personnes toute distance
        T_MD = Facture moyenne par km avant action (k€/pers.km) transport de marchandise hors domicile-travail toute distance
        
    """

    default_asset_nbr_percar = get_input_param(
        B, "domicile-travail", "Nombre d'actifs par voiture par défaut"
    )
    filling_rate_pt = get_input_param(
        B,
        "transport-personnes",
        "Taux de remplissage des bus/cars",
    )

    O_MD = (
        A["Part modale voiture + 2 roues pour trajet < 2 km"]
        / 100
        * O_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet < 2 km"]
        / 100
        * P_ME
        / filling_rate_pt
    )

    P_MD = (
        A["Part modale voiture + 2 roues pour trajet [2-10] km"]
        / 100
        * O_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet [2-10] km"]
        / 100
        * P_ME
        / filling_rate_pt
    )

    Q_MD = (
        A["Part modale voiture + 2 roues pour trajet [10-80] km"]
        / 100
        * O_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet [10-80] km"]
        / 100
        * P_ME
        / filling_rate_pt
    )

    R_MD = (
        O_MD * A["Nombre d'actifs trajet < 2 km"] * A["Distance moyenne trajets < 2 km"]
        + P_MD * A["Nombre d'actifs [2-10] km"] * A["Distance moyenne [2-10] km"]
        + Q_MD * A["Nombre d'actifs [10-80] km"] * A["Distance moyenne [10-80] km"]
    ) / (A["Nombre d'actifs domicile-travail"] * A["Distance moyenne domicile-travail"])

    S_MD = (
        O_MD * A["Part km hors travail < 2 km"]
        + P_MD * A["Part km hors travail [2-10] km"]
        + Q_MD * A["Part km hors travail [10-80] km"]
    )
    T_MD = Q_ME

    energy_bill["move_differently"] = {
        "ener_bill_passenger_less_2km": O_MD,
        "ener_bill_passenger_2_10km": P_MD,
        "ener_bill_passenger_10_80km": Q_MD,
        "ener_bill_total_home_work": R_MD,
        "ener_bill_total_out_home_work": S_MD,
        "ener_bill_goods_transport": T_MD,
    }

    # final result before action
    """
        W : Facture énergétique avant action (k€) Transport de personnes domicile-travail
        X : Facture énergétique avant action (k€) Transport de personnes hors domicile-travail
        Y : Facture énergétique avant action (k€) Transport de marchandise hors domicile-travail
        Z : Total Facture énergétique (k€) transport routier avant action  

    """

    worked_day_per_year = get_input_param(
        B, "domicile-travail", "Nombre de jours travaillés par an"
    )
    daily_trips = get_input_param(B, "domicile-travail", "Nombre de trajets par jour")

    W = (
        (R_MD / 100)
        * C_ML
        * A["Nombre d'actifs domicile-travail"]
        * A["Distance moyenne domicile-travail"]
        * worked_day_per_year
        * daily_trips
    ) / 1000000

    if level == 0:
        Z = A["Facture énergétique - transport routier"]

        energy_bill["before"] = {
            "trasport_passenger_home_work": W,
            "total_energy_bill_transport": Z,
        }
    else:
        X = A["Facture énergétique - transport routier - transport de personnes"] - W
        Y = A["Facture énergétique - transport routier - transport de marchandises"]
        Z = W + X + Y
        energy_bill["before"] = {
            "trasport_passenger_home_work": W,
            "transport_passenger_off_home_work": X,
            "transport_goods_off_domicile_travail": Y,
            "total_energy_bill_transport": Z,
        }

    return energy_bill
