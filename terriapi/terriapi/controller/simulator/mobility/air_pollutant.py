# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


from collections import defaultdict

from terriapi.controller.simulator.utils import get_input_param, get_inter_param


def get_pollutant(
    specific_input_param,
    generic_input_param,
    generic_inter_param,
    level,
    pollutant,
    type_fe,
):
    """
    specific_input_param : dict
        specific input parameters to territory
    generic_input_param : dict
        generic input paramateres
    generic_inter_param : dict
        generic intermediate paramateres
    level: integer
        simulator level
    pollutant: string
        pollutant's name (NOx, PM10, PM25, SO2, COVNM, Nh3)
    type_fe: string
        fe label in database
    """

    result = defaultdict(dict)

    ## Move less (suffix _ML)
    """
        C_ML = Distance parcourue avant action - indice 100 domicile-travail Transport de personnes (100%)
        D_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de personnes (100%)
        E_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de marchandise (100%)
    """
    C_ML = 1

    ## Move efficiently (suffix _ME)
    """
        C_ME = Emissions moyenne par km avant action (g/veh.km) Transport de personnes Voiture
        D_ME = Emissions moyenne par km avant action (g/veh.km) Transport de personnes Bus et cars
        E_ME = Emissions moyenne par km avant action (g/veh.km) Transport de marchandise Poids lourds
        F_ME = Emissions moyenne par km avant action (g/veh.km) Transport de personnes Vélo / marche à pied
        
    """

    ### passanger transport
    fe_vp_diesel_essence = get_inter_param(
        generic_inter_param, type_fe, "véhicule particulier", "diesel_essence"
    )
    fe_vp_elec = get_inter_param(
        generic_inter_param, type_fe, "véhicule particulier", "électrique"
    )
    fe_vp_gnv = get_inter_param(
        generic_inter_param, type_fe, "véhicule particulier", "GNV"
    )
    fe_vp_hydrogen = get_inter_param(
        generic_inter_param, type_fe, "véhicule particulier", "hydrogène"
    )

    C_ME = (
        specific_input_param["Part moteur diesel/essence - véhicule particulier"]
        / 100
        * fe_vp_diesel_essence
        + specific_input_param["Part moteur électrique - véhicule particulier"]
        / 100
        * fe_vp_elec
        + specific_input_param["Part moteur GNV - véhicule particulier"]
        / 100
        * fe_vp_gnv
        + specific_input_param["Part moteur hydrogène - véhicule particulier"]
        / 100
        * fe_vp_hydrogen
    )

    ### public transport
    fe_pt_diesel_essence = get_inter_param(
        generic_inter_param, type_fe, "bus et cars", "diesel_essence"
    )
    fe_pt_elec = get_inter_param(
        generic_inter_param, type_fe, "bus et cars", "électrique"
    )
    fe_pt_gnv = get_inter_param(generic_inter_param, type_fe, "bus et cars", "GNV")
    fe_pt_hydrogen = get_inter_param(
        generic_inter_param, type_fe, "bus et cars", "hydrogène"
    )

    D_ME = (
        specific_input_param["Part moteur diesel/essence - bus et cars"]
        / 100
        * fe_pt_diesel_essence
        + specific_input_param["Part moteur électrique - bus et cars"]
        / 100
        * fe_pt_elec
        + specific_input_param["Part moteur GNV - bus et cars"] / 100 * fe_pt_gnv
        + specific_input_param["Part moteur hydrogène - bus et cars"]
        / 100
        * fe_pt_hydrogen
    )

    ### heavyweight transport

    fe_hw_diesel_essence = get_inter_param(
        generic_inter_param, type_fe, "poids lourd", "diesel"
    )
    fe_hw_hydrogen = get_inter_param(
        generic_inter_param, type_fe, "poids lourd", "hydrogène"
    )
    fe_hw_gnv = get_inter_param(generic_inter_param, type_fe, "poids lourd", "GNV")

    E_ME = (
        specific_input_param["Part moteur diesel - poids lourds"]
        / 100
        * fe_hw_diesel_essence
        + specific_input_param["Part moteur hydrogène - poids lourds"]
        / 100
        * fe_hw_hydrogen
        + specific_input_param["Part moteur GNV - poids lourds"] / 100 * fe_hw_gnv
    )

    ### walk and bike
    F_ME = 0

    result["move_efficently"] = {
        "avg_cons_passenger_cas": C_ME,
        "avg_cons_passenger_bus": D_ME,
        "avg_cons_total_hw": E_ME,
        "avg_cons_passenger_velo": F_ME,
    }

    ## Move differently (suffix _MD)
    """
        C_MD = Emissions moyenne par km avant action (g/veh.km) domicile-travail Transport de personnes  distance < 2 km 
        D_MD = Emissions moyenne par km avant action (g/veh.km) domicile-travail Transport de personnes distance 2-10 km
        E_MD = Emissions moyenne par km avant action (g/veh.km) domicile-travail Transport de personnes distance 10-80 km
        F_MD = Emissions moyenne par km avant action (g/veh.km) domicile-travail Transport de personnes toute distance
        G_MD = Emissions moyenne par km avant action (g/veh.km) hors domicile-travail Transport de personnes toute distance
        H_MD = Emissions moyenne par km avant action (g/veh.km) transport de marchandise hors domicile-travail toute distance
    """

    default_asset_nbr_percar = get_input_param(
        generic_input_param,
        "domicile-travail",
        "Nombre d'actifs par voiture par défaut",
    )
    filling_rate_pt = get_input_param(
        generic_input_param, "transport-personnes", "Taux de remplissage des bus/cars"
    )

    C_MD = (
        specific_input_param["Part modale voiture + 2 roues pour trajet < 2 km"]
        / 100
        * C_ME
        / default_asset_nbr_percar
    ) + (
        specific_input_param["Part modale transport en commun pour trajet < 2 km"]
        / 100
        * D_ME
        / filling_rate_pt
    )

    D_MD = (
        specific_input_param["Part modale voiture + 2 roues pour trajet [2-10] km"]
        * C_ME
        / default_asset_nbr_percar
        + specific_input_param["Part modale transport en commun pour trajet [2-10] km"]
        * D_ME
        / filling_rate_pt
    ) / 100

    E_MD = (
        specific_input_param["Part modale voiture + 2 roues pour trajet [10-80] km"]
        * C_ME
        / default_asset_nbr_percar
        + specific_input_param["Part modale transport en commun pour trajet [10-80] km"]
        * D_ME
        / filling_rate_pt
    ) / 100

    F_MD = (
        C_MD
        * specific_input_param["Nombre d'actifs trajet < 2 km"]
        * specific_input_param["Distance moyenne trajets < 2 km"]
        + D_MD
        * specific_input_param["Nombre d'actifs [2-10] km"]
        * specific_input_param["Distance moyenne [2-10] km"]
        + E_MD
        * specific_input_param["Nombre d'actifs [10-80] km"]
        * specific_input_param["Distance moyenne [10-80] km"]
    ) / (
        specific_input_param["Nombre d'actifs domicile-travail"]
        * specific_input_param["Distance moyenne domicile-travail"]
    )

    G_MD = (
        C_MD * specific_input_param["Part km hors travail < 2 km"]
        + D_MD * specific_input_param["Part km hors travail [2-10] km"]
        + E_MD * specific_input_param["Part km hors travail [10-80] km"]
    )

    H_MD = E_ME

    result["move_differently"] = {
        "avg_emission_passenger_less_2km": C_MD,
        "avg_emission_passenger_2_10km": D_MD,
        "avg_emission_passenger_10_80km": E_MD,
        "avg_emission_total_home_work": F_MD,
        "avg_emission_total_out_home_work": G_MD,
        "avg_emission_goods_transport": H_MD,
    }

    # final result before action
    """
        W : Emissions avant action Transport de personnes domicile-travail
        X : Emissions avant action Transport de personnes hors domicile-travail
        Y : Emissions avant action Transport de marchandise hors domicile-travail
        Z : Total Emissions transport routier avant action  

    """

    worked_day_per_year = get_input_param(
        generic_input_param, "domicile-travail", "Nombre de jours travaillés par an"
    )
    daily_trips = get_input_param(
        generic_input_param, "domicile-travail", "Nombre de trajets par jour"
    )

    W = (
        F_MD
        * C_ML
        * specific_input_param["Nombre d'actifs domicile-travail"]
        * specific_input_param["Distance moyenne domicile-travail"]
        * worked_day_per_year
        * daily_trips
    ) / 1000000

    if level == 0:
        Z = specific_input_param["Emissions {pollutant} - transport routier"]

        result["before"] = {
            "transport_passenger_home_work": W,
            "total_emission_transport": Z,
        }
    else:
        X = (
            specific_input_param[
                "Emissions "
                + str(pollutant)
                + " - transport routier - transport de personnes"
            ]
            - W
        )
        Y = specific_input_param[
            "Emissions "
            + str(pollutant)
            + " - transport routier - transport de marchandises"
        ]
        Z = W + X + Y
        result["before"] = {
            "trasport_passenger_home_work": W,
            "transport_passenger_off_home_work": X,
            "transport_goods_off_domicile_travail": Y,
            "total_emission_transport": Z,
        }

    return result
