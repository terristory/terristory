# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from collections import defaultdict

from terriapi.controller.simulator.utils import get_input_param, get_inter_param


def get_ghg(A, B, C, level):
    ghg = defaultdict(dict)

    ## Move less (suffix _ML)
    """
        C_ML = Distance parcourue avant action - indice 100 domicile-travail Transport de personnes (100%)
        D_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de personnes (100%)
        E_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de marchandise (100%)
    """
    C_ML = 1

    ## Move efficiently (suffix _ME)
    """
    I_ME = Emissions moyennes par km avant action (gCO2eq/veh.km) Transport de personnes Voiture
    J_ME = Emissions moyennes par km avant action (gCO2eq/veh.km) Transport de personnes Bus et cars
    K_ME = Emissions moyennes par km avant action (gCO2eq/veh.km) Transport de marchandise Poids lourds
    L_ME = Emissions moyennes par km avant action (gCO2eq/veh.km) Transport de personnes Vélo / marche à pied

    """

    biofuel_share = (
        get_input_param(B, "motorisation", "Part de biocarburant dans essence / diesel")
        / 100
    )
    biognv_share = (
        get_input_param(B, "motorisation", "Part de bioGNV dans le GNV") / 100
    )
    greenhydro_share = (
        get_input_param(B, "motorisation", "Part d'hydrogène vert dans l'hydrogène")
        / 100
    )

    ### passanger transport

    emission_vp_diesel_essence = get_inter_param(
        C, "fe_ges", "véhicule particulier", "diesel_essence"
    )
    emission_vp_biofuel = get_inter_param(
        C, "fe_ges", "véhicule particulier", "biocarburant"
    )
    emission_vp_elec = get_inter_param(
        C, "fe_ges", "véhicule particulier", "électrique"
    )
    emission_vp_gnv = get_inter_param(C, "fe_ges", "véhicule particulier", "GNV")
    emission_vp_biognv = get_inter_param(C, "fe_ges", "véhicule particulier", "bioGNV")

    I_ME = (
        A["Part moteur diesel/essence - véhicule particulier"]
        / 100
        * (
            (1 - biofuel_share) * emission_vp_diesel_essence
            + biofuel_share * emission_vp_biofuel
        )
        + A["Part moteur électrique - véhicule particulier"] / 100 * emission_vp_elec
        + A["Part moteur GNV - véhicule particulier"]
        / 100
        * ((1 - biognv_share) * emission_vp_gnv + biognv_share * emission_vp_biognv)
    )

    ### public transport
    emission_pt_diesel_essence = get_inter_param(
        C, "fe_ges", "bus et cars", "diesel_essence"
    )
    emission_pt_elec = get_inter_param(C, "fe_ges", "bus et cars", "électrique")
    emission_pt_gnv = get_inter_param(C, "fe_ges", "bus et cars", "GNV")
    emission_pt_biognv = get_inter_param(C, "fe_ges", "bus et cars", "bioGNV")
    emission_pt_hydrogen = get_inter_param(C, "fe_ges", "bus et cars", "hydrogène")
    emission_pt_biofuel = get_inter_param(C, "fe_ges", "bus et cars", "biocarburant")
    emission_pt_greenhydro = get_inter_param(
        C, "fe_ges", "bus et cars", "hydrogènevert"
    )

    J_ME = (
        A["Part moteur diesel/essence - bus et cars"]
        / 100
        * (
            (1 - biofuel_share) * emission_pt_diesel_essence
            + biofuel_share * emission_pt_biofuel
        )
        + A["Part moteur électrique - bus et cars"] / 100 * emission_pt_elec
        + A["Part moteur GNV - bus et cars"]
        / 100
        * ((1 - biognv_share) * emission_pt_gnv + biognv_share * emission_pt_biognv)
        + A["Part moteur hydrogène - bus et cars"]
        / 100
        * (
            (1 - greenhydro_share) * emission_pt_hydrogen
            + greenhydro_share * emission_pt_greenhydro
        )
    )

    ### heavyweight transport
    emission_hw_diesel = get_inter_param(C, "fe_ges", "poids lourd", "diesel")
    emission_hw_gnv = get_inter_param(C, "fe_ges", "poids lourd", "GNV")
    emission_hw_biognv = get_inter_param(C, "fe_ges", "poids lourd", "bioGNV")
    emission_hw_hydrogen = get_inter_param(C, "fe_ges", "poids lourd", "hydrogène")
    emission_hw_biofuel = get_inter_param(C, "fe_ges", "poids lourd", "biocarburant")
    emission_hw_greenhydro = get_inter_param(
        C, "fe_ges", "poids lourd", "hydrogènevert"
    )

    K_ME = (
        A["Part moteur diesel - poids lourds"]
        / 100
        * (
            (1 - biofuel_share) * emission_hw_diesel
            + biofuel_share * emission_hw_biofuel
        )
        + A["Part moteur GNV - poids lourds"]
        / 100
        * ((1 - biognv_share) * emission_hw_gnv + biognv_share * emission_hw_biognv)
        + A["Part moteur hydrogène - poids lourds"]
        / 100
        * (
            (1 - greenhydro_share) * emission_hw_hydrogen
            + greenhydro_share * emission_hw_greenhydro
        )
    )

    ## Move differently (suffix _MD)
    """
            I_MD = Emissions moyennes par km avant action (gCO2eq/pers.km) domicile-travail Transport personnes distance < 3km 
            J_MD = Emissions moyennes par km avant action (gCO2eq/pers.km) domicile-travail Transport personnes distance 3-10km
            K_MD = Emissions moyennes par km avant action (gCO2eq/pers.km) domicile-travail Transport personnes distance > 10km
            L_MD = Emissions moyennes par km avant action (gCO2eq/pers.km) domicile-travail Transport personnes toute distance
            M_MD = Emissions moyennes par km avant action (gCO2eq/pers.km) hors domicile-travail Transport personnes toute distance
            N_MD = Emissions moyennes par km avant action (gCO2eq/pers.km) transport de marchandise hors domicile-travail toute distance
            
        """

    default_asset_nbr_percar = get_input_param(
        B, "domicile-travail", "Nombre d'actifs par voiture par défaut"
    )
    filling_rate_pt = get_input_param(
        B, "transport-personnes", "Taux de remplissage des bus/cars"
    )

    I_MD = (
        A["Part modale voiture + 2 roues pour trajet < 2 km"]
        / 100
        * I_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet < 2 km"]
        / 100
        * J_ME
        / filling_rate_pt
    )

    J_MD = (
        A["Part modale voiture + 2 roues pour trajet [2-10] km"]
        / 100
        * I_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet [2-10] km"]
        / 100
        * J_ME
        / filling_rate_pt
    )

    K_MD = (
        A["Part modale voiture + 2 roues pour trajet [10-80] km"]
        / 100
        * I_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet [10-80] km"]
        / 100
        * J_ME
        / filling_rate_pt
    )

    L_MD = (
        I_MD * A["Nombre d'actifs trajet < 2 km"] * A["Distance moyenne trajets < 2 km"]
        + J_MD * A["Nombre d'actifs [2-10] km"] * A["Distance moyenne [2-10] km"]
        + K_MD * A["Nombre d'actifs [10-80] km"] * A["Distance moyenne [10-80] km"]
    ) / (A["Nombre d'actifs domicile-travail"] * A["Distance moyenne domicile-travail"])

    M_MD = (
        I_MD * A["Part km hors travail < 2 km"]
        + J_MD * A["Part km hors travail [2-10] km"]
        + K_MD * A["Part km hors travail [10-80] km"]
    )

    N_MD = K_ME

    ghg["move_differently"] = {
        "ghg_passenger_less_2km": I_MD,
        "ghg_passenger_2_10km": J_MD,
        "ghg_passenger_10_80km": K_MD,
        "ghg_total_home_work": L_MD,
        "ghg_total_out_home_work": M_MD,
        "ghg_goods_transport": N_MD,
    }

    # final result before action
    """
        W : Emissions de GES avant action Transport de personnes domicile-travail
        X : Emissions de GES avant action Transport de personnes hors domicile-travail
        Y : Emissions de GES avant action Transport de marchandise hors domicile-travail
        Z : Total Emissions transport routier avant action  

    """
    worked_day_per_year = get_input_param(
        B, "domicile-travail", "Nombre de jours travaillés par an"
    )
    daily_trips = get_input_param(B, "domicile-travail", "Nombre de trajets par jour")

    W = (
        L_MD
        * C_ML
        * A["Nombre d'actifs domicile-travail"]
        * A["Distance moyenne domicile-travail"]
        * worked_day_per_year
        * daily_trips
        / 1000000000
    )

    if level == 0:
        Z = A["Emissions GES - transport routier"]

        ghg["before"] = {"trasport_passenger_home_work": W, "total_ghg_transport": Z}
    else:
        X = A["Emissions GES - transport routier - transport de personnes"] - W
        Y = A["Emissions GES - transport routier - transport de marchandises"]
        Z = W + X + Y
        ghg["before"] = {
            "trasport_passenger_home_work": W,
            "transport_passenger_off_home_work": X,
            "transport_goods_off_domicile_travail": Y,
            "total_ghg_transport": Z,
        }

    return ghg
