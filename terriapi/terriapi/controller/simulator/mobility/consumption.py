# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


from collections import defaultdict

from terriapi.controller.simulator.utils import get_input_param, get_inter_param


def get_consumption(A, B, C, level):
    """ """

    consumption = defaultdict(dict)

    ## Move less (suffix _ML)
    """
        C_ML = Distance parcourue avant action - indice 100 domicile-travail Transport de personnes (100%)
        D_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de personnes (100%)
        E_ML = Distance parcourue avant action - indice 100 hors domicile-travail Transport de marchandise (100%)
    """
    C_ML = 1

    ## Move efficiently (suffix _ME)
    """
        C_ME = Consommation moyenne par km avant action (kWh/pers.km) Transport de personnes Voiture
        D_ME = Consommation moyenne par km avant action (kWh/pers.km) Transport de personnes Bus et cars
        E_ME = Consommation moyenne par km avant action (kWh/pers.km) Transport de marchandise Poids lourds
        F_ME = Consommation moyenne par km avant action (kWh/pers.km) Transport de personnes Vélo / marche à pied
        
    """

    ### passanger transport
    conso_vp_diesel_essence = get_inter_param(
        C, "consommation", "véhicule particulier", "diesel_essence"
    )
    conso_vp_elec = get_inter_param(
        C, "consommation", "véhicule particulier", "électrique"
    )
    conso_vp_gnv = get_inter_param(C, "consommation", "véhicule particulier", "GNV")
    conso_vp_hydrogen = get_inter_param(
        C, "consommation", "véhicule particulier", "hydrogène"
    )

    C_ME = (
        A["Part moteur diesel/essence - véhicule particulier"] * conso_vp_diesel_essence
        + A["Part moteur électrique - véhicule particulier"] * conso_vp_elec
        + A["Part moteur GNV - véhicule particulier"] * conso_vp_gnv
        + A["Part moteur hydrogène - véhicule particulier"] * conso_vp_hydrogen
    ) / 100

    ### public transport
    conso_pt_diesel_essence = get_inter_param(
        C, "consommation", "bus et cars", "diesel_essence"
    )
    conso_pt_elec = get_inter_param(C, "consommation", "bus et cars", "électrique")
    conso_pt_gnv = get_inter_param(C, "consommation", "bus et cars", "GNV")
    conso_pt_hydrogen = get_inter_param(C, "consommation", "bus et cars", "hydrogène")

    D_ME = (
        A["Part moteur diesel/essence - bus et cars"] * conso_pt_diesel_essence
        + A["Part moteur électrique - bus et cars"] * conso_pt_elec
        + A["Part moteur GNV - bus et cars"] * conso_pt_gnv
        + A["Part moteur hydrogène - bus et cars"] * conso_pt_hydrogen
    ) / 100

    ### heavyweight transport

    conso_hw_diesel_essence = get_inter_param(
        C, "consommation", "poids lourd", "diesel"
    )
    conso_hw_hydrogen = get_inter_param(C, "consommation", "poids lourd", "hydrogène")
    conso_hw_gnv = get_inter_param(C, "consommation", "poids lourd", "GNV")

    E_ME = (
        A["Part moteur diesel - poids lourds"] * conso_hw_diesel_essence
        + A["Part moteur hydrogène - poids lourds"] * conso_hw_hydrogen
        + A["Part moteur GNV - poids lourds"] * conso_hw_gnv
    ) / 100

    ### walk and bike

    ## Move differently (suffix _MD)
    """
        C_MD = Consommation moyenne par km avant action (kWh/pers.km) domicile-travail Transport de personnes  distance < 2 km 
        D_MD = Consommation moyenne par km avant action (kWh/pers.km) domicile-travail Transport de personnes distance 2-10 km
        E_MD = Consommation moyenne par km avant action (kWh/pers.km) domicile-travail Transport de personnes distance 10-80 km
        F_MD = Consommation moyenne par km avant action (kWh/pers.km) domicile-travail Transport de personnes toute distance
        G_MD = Consommation moyenne par km avant action (kWh/pers.km) hors domicile-travail Transport de personnes toute distance
        H_MD = Consommation moyenne par km avant action (kWh/pers.km) transport de marchandise hors domicile-travail toute distance
    """

    default_asset_nbr_percar = get_input_param(
        B, "domicile-travail", "Nombre d'actifs par voiture par défaut"
    )
    filling_rate_pt = get_input_param(
        B, "transport-personnes", "Taux de remplissage des bus/cars"
    )

    C_MD = (
        A["Part modale voiture + 2 roues pour trajet < 2 km"]
        / 100
        * C_ME
        / default_asset_nbr_percar
    ) + (
        A["Part modale transport en commun pour trajet < 2 km"]
        / 100
        * D_ME
        / filling_rate_pt
    )

    D_MD = (
        A["Part modale voiture + 2 roues pour trajet [2-10] km"]
        * C_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet [2-10] km"]
        * D_ME
        / filling_rate_pt
    ) / 100

    E_MD = (
        A["Part modale voiture + 2 roues pour trajet [10-80] km"]
        * C_ME
        / default_asset_nbr_percar
        + A["Part modale transport en commun pour trajet [10-80] km"]
        * D_ME
        / filling_rate_pt
    ) / 100

    F_MD = (
        C_MD * A["Nombre d'actifs trajet < 2 km"] * A["Distance moyenne trajets < 2 km"]
        + D_MD * A["Nombre d'actifs [2-10] km"] * A["Distance moyenne [2-10] km"]
        + E_MD * A["Nombre d'actifs [10-80] km"] * A["Distance moyenne [10-80] km"]
    ) / (A["Nombre d'actifs domicile-travail"] * A["Distance moyenne domicile-travail"])

    G_MD = (
        C_MD * A["Part km hors travail < 2 km"]
        + D_MD * A["Part km hors travail [2-10] km"]
        + E_MD * A["Part km hors travail [10-80] km"]
    )

    H_MD = E_ME

    consumption["move_differently"] = {
        "avg_cons_passenger_less_2km": C_MD,
        "avg_cons_passenger_2_10km": D_MD,
        "avg_cons_passenger_10_80km": E_MD,
        "avg_cons_total_home_work": F_MD,
        "avg_conso_total_out_home_work": G_MD,
        "avg_conso_goods_transport": H_MD,
    }

    # final result before action
    """
        W : Consommation avant action Transport de personnes domicile-travail
        X : Consommation avant action Transport de personnes hors domicile-travail
        Y : Consommation avant action Transport de marchandise hors domicile-travail
        Z : Total consommation transport routier avant action  

    """

    worked_day_per_year = get_input_param(
        B, "domicile-travail", "Nombre de jours travaillés par an"
    )
    daily_trips = get_input_param(B, "domicile-travail", "Nombre de trajets par jour")

    W = (
        F_MD
        * C_ML
        * A["Nombre d'actifs domicile-travail"]
        * A["Distance moyenne domicile-travail"]
        * worked_day_per_year
        * daily_trips
        / 1000000
    )

    if level == 0:
        Z = A["Consommation énergétique - transport routier"]

        consumption["before"] = {
            "trasport_passenger_home_work": W,
            "total_conso_transport": Z,
        }
    else:
        X = (
            A["Consommation énergétique - transport routier - transport de personnes"]
            - W
        )
        Y = A[
            "Consommation énergétique - transport routier - transport de marchandises"
        ]
        Z = W + X + Y
        consumption["before"] = {
            "trasport_passenger_home_work": W,
            "transport_passenger_off_home_work": X,
            "transport_goods_off_domicile_travail": Y,
            "total_conso_transport": Z,
        }

    return consumption
