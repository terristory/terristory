# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


def get_input_param(data_dict, data, label):
    return next(
        (x["value"] for x in data_dict if (x["data"] == data and x["label"] == label)),
        0,
    )


def get_inter_param(data_dict, data, category, engine):
    return next(
        (
            x["value"]
            for x in data_dict
            if (
                x["data"] == data
                and x["category"] == category
                and x["engine"] == engine
            )
        ),
        0,
    )
