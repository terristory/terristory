﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import datetime
import os
from pathlib import Path

import pandas as pd
from openpyxl import load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from sanic.log import logger

from terriapi import here, settings
from terriapi.controller import execute

EXCEL_EXPORT_IMPACTS = (
    here / "ressources" / "export-impacts-strategie-territoriale-default.xlsx"
)


def replace_value_in_sheet(sheet, tokens, search=[]):
    """
    Replace the values of tokens keys by tokens values in sheet

    Parameters
    ----------
    sheet : openpyxl sheet
        the worksheet opened through openpyxl
    tokens : dict
        the keys to replace by the values
    """
    found = {}
    variables_tokens = {}
    for r in range(1, sheet.max_row + 1):
        for c in range(1, sheet.max_column + 1):
            s = sheet.cell(r, c).value
            if s in search:
                found[s] = found.get(s, []) + [(r, c)]
            # if we have a potential variable parameter
            elif s is not None and ">#" in str(s):
                # we strip parameter from variable part
                s_parameters = s.split("#")
                real_s = s_parameters[0]
                variable_value = s_parameters[1]
                # we still save the location of this parameter
                found[s] = found.get(s, []) + [(r, c)]
                # and we add the need to retrieve new data related to this variable
                if variable_value not in variables_tokens:
                    variables_tokens[variable_value] = set()
                variables_tokens[variable_value].add(real_s)
            if s is not None and s in tokens:
                sheet.cell(r, c).value = s.replace(s, tokens[s])
    return sheet, found, variables_tokens


def flatten_dict(nested_dict, ignore_keys=()):
    """
    Flattens a dictionary recursively. Ignores key if specified.

    Parameters
    ----------
    nested_dict : dict
        the dictionary to flatten
    ignore_keys : tuple, optional
        the keys to ignore, by default ()

    Returns
    -------
    dict
        the dictionary flattened with nested keys in tuple key
    """
    res = {}
    if isinstance(nested_dict, dict):
        for k in nested_dict:
            if k in ignore_keys:
                continue
            flattened_dict = flatten_dict(nested_dict[k], ignore_keys=ignore_keys)
            for key, val in flattened_dict.items():
                key = list(key)
                key.insert(0, k)
                res[tuple(key)] = val
    else:
        res[()] = nested_dict
    return res


def nested_dict_to_df(values_dict, ignore_keys=()):
    """
    Converts a nest dictionary to a flattened dataframe with nested keys as
    tuples.

    E.g., from:

    .. code-block:: python

            {
                "2019": {
                    "direct": {
                        "region": {
                            "potentiel": {
                                "nb_emploi_total": 10.0,
                                "va_totale": 10.0,
                            }
                        }
                    }
                },
                ...
            }

    to:

    .. list-table:: pandas.DataFrame
        :widths: 15 15 15 15 15 15
        :header-rows: 1

        * - year
          - direct
          - level
          - type
          - nb_emploi_total
          - va_totale
        * - 2019
          - direct
          - region
          - perenne
          - 0.0
          - 10.5
        * - 2019
          - direct
          - region
          - ponctuel
          - 2.4
          - 16.25

    Parameters
    ----------
    values_dict : dict
        the nested dictionary to convert
    ignore_keys : tuple, optional
        keys that should be ignored when flattening to remove elements, by default ()

    Returns
    -------
    pd.DataFrame
        the dataframe resulting from the operation
    """
    if len(values_dict) == 0:
        return pd.DataFrame(values_dict)
    flat_dict = flatten_dict(values_dict, ignore_keys=ignore_keys)
    df = pd.DataFrame.from_dict(flat_dict, orient="index")
    df.index = pd.MultiIndex.from_tuples(df.index)
    df = df.unstack(level=-1)
    df.columns = df.columns.map("{0[1]}".format)
    return df


def append_regional_info(data, level):
    for key, value in data.items():
        pass
    return data


def prepare_data_for_jobs(data):
    """
    Prepare the results data for pdf export by flattening the nested input dictionary
    and going from:

    .. code-block:: python

            {
                "2019": {
                    "direct": {
                        "region": {
                            "potentiel": {
                                "nb_emploi_total": 10.0,
                                "va_totale": 10.0,
                            }
                        }
                    }
                },
                ...
            }

    to a tuple of two dictionaries looking like:

    .. code-block:: python

            {
                "2019": {
                    "total": 0.0,
                    "direct": 0.0,
                    "indirect": 0.0
                },
                ...
            }

    Parameters
    ----------
    data : dict
        the input data concerning jobs

    Returns
    -------
    tuple
        two dictionaries corresponding to jobs and added value data
    """
    # if empty data, return two empty dict
    if len(data) == 0:
        return data, data

    # flatten the dictionary
    df_impacts = nested_dict_to_df(data, ("investissement",))
    # name each column
    df_impacts.index = df_impacts.index.set_names(["year", "direct", "level", "type"])
    # differentiate rows that will be displayed and groups by year
    df_impacts_direct = (
        df_impacts.loc[df_impacts.index.get_level_values("direct") == "direct", :]
        .groupby(["level", "year"])
        .sum()
    )
    df_impacts_indirect = (
        df_impacts.loc[df_impacts.index.get_level_values("direct") == "indirect", :]
        .groupby(["level", "year"])
        .sum()
    )

    # create rows corresponding to jobs
    df_emplois = pd.concat(
        [
            df_impacts_direct.loc[:, "nb_emploi_total"]
            + df_impacts_indirect.loc[:, "nb_emploi_total"],
            df_impacts_direct.loc[:, "nb_emploi_total"],
            df_impacts_indirect.loc[:, "nb_emploi_total"],
        ],
        axis=1,
    )
    # prepare for output
    df_emplois.columns = ["total", "direct", "indirect"]
    data_emplois = df_emplois.transpose().to_dict()

    # create rows corresponding to added value
    df_va = pd.concat(
        [
            df_impacts_direct.loc[:, "va_totale"]
            + df_impacts_indirect.loc[:, "va_totale"],
            df_impacts_direct.loc[:, "va_totale"],
            df_impacts_indirect.loc[:, "va_totale"],
        ],
        axis=1,
    )
    # prepare for output
    df_va.columns = ["total", "direct", "indirect"]
    data_va = df_va.transpose().to_dict()

    # if we have more than one geographical level, we group by geographical level
    if len(df_impacts.index.get_level_values("level").unique()) > 1:
        # get the other regional level concerned here
        # TODO: handles more than only one level
        all_regional_levels = list(df_emplois.index.get_level_values("level").unique())
        all_regional_levels.remove("region")
        other_regional_level = all_regional_levels[0]

        # rearrange the arrays and rename the columns
        df_emplois_region = df_emplois.loc["region", :]
        df_emplois_region.columns = [
            "Région - total emplois générés",
            "dont emplois directs",
            "dont emplois indirects",
        ]
        df_emplois_other = df_emplois.loc[other_regional_level, :]
        df_emplois_other.columns = [
            other_regional_level.capitalize() + " - total emplois générés",
            "dont emplois directs",
            "dont emplois indirects",
        ]
        data_emplois = {
            "region": df_emplois_region.transpose().to_dict(),
            "other_regional_level": df_emplois_other.transpose().to_dict(),
        }

        # rearrange the arrays and rename the columns
        df_va_region = df_va.loc["region", :]
        df_va_region.columns = [
            "Région - total valeur ajoutée générée",
            "dont valeur ajoutée directe",
            "dont valeur ajoutée indirecte",
        ]
        df_va_other = df_va.loc[other_regional_level, :]
        df_va_other.columns = [
            other_regional_level.capitalize() + " - total valeur ajoutée générée",
            "dont valeur ajoutée directe",
            "dont valeur ajoutée indirecte",
        ]
        data_va = {
            "region": df_va_region.transpose().to_dict(),
            "other_regional_level": df_va_other.transpose().to_dict(),
        }
    else:
        data_emplois = {"region": data_emplois}
        data_va = {"region": data_va}

    return data_emplois, data_va


def _handle_reference_trajectories(traj_data, filter):
    """
    Filter and refactor an array of reference trajectory data
    and return only data relative to filter value in a correct way.

    .. code-block:: python

        {
            key: {
                year1: value1,
                year2: value2,
                ...
            },
            ...
        }

    Parameters
    ----------
    traj_data : dict
        the initial data dict to filter
    filter : str
        the string used to filter scenarios

    Returns
    -------
    dict
        the dictionary built
    """
    return {
        key: {
            year_data.get("annee", ""):
            # we compute the value as `ref_value * (1 - decrease_in_% / 100.0)`
            trajectory.get("valeur_reference", 0.0)
            * (1.0 + year_data.get("valeur", 0.0) / 100.0)
            for year_data in [
                # for first year, we want to use reference value => so no change
                {"annee": trajectory.get("annee_reference"), "valeur": 0.0}
            ]
            + trajectory.get("valeurs_annees", [])
        }
        for key in traj_data.keys()
        for trajectory in traj_data[key]
        if filter in trajectory.get("titre", "")
    }


def _convert_custom_trajectory_data(dic):
    """
    Function to format custom trajectory data to a correct format:

    .. code-block:: python

        {
            year1: value1,
            year2: value2,
            ...
        }

    Parameters
    ----------
    dic : dict
        the dictionary containing the data

    Returns
    -------
    dict
        the dictionary formatted
    """
    if "annees_valeurs" in dic:
        return dic["annees_valeurs"]
    else:
        return {year: value for year, value in zip(dic["annees"], dic["valeurs"])}


def _convert_pcaet_trajectory_data(dic, key, first_year):
    """
    Function to format pcaet trajectory data to a correct format:

    .. code-block:: python

        {
            year1: value1,
            year2: value2,
            ...
        }

    Parameters
    ----------
    dic : dict
        the dictionary containing the data

    Returns
    -------
    dict
        the dictionary formatted
    """
    # TODO: fix the range to be dynamic
    return {year: dic.get(f"{key}_annee_{year}", "") for year in range(2019, 2051)}


def _handle_input_parameters(parameters):
    """
    Transforms the parameters sent from the frontend to functioning DataFrames
    that can be inserted into the Excel output files.

    Parameters
    ----------
    parameters : dict
        the parameters given in input

    Returns
    -------
    dict
        a dictionary with two dataframes, one for main coefficients and
        one for advanced parameters.
    """

    return {
        "main_coeffs": _handle_main_input_parameters(parameters),
        "advanced_coeffs": _handle_advanced_parameters(parameters),
    }


def _handle_advanced_parameters(parameters):
    coeffs = []
    for action_parameters in parameters.get("coeffsAdvanced", []):
        # we handle economic and other parameters separately as they have
        # different data structures

        # economic parameters are stored as dict in a list
        for economic_parameter in action_parameters.get("params_avances", {}).get(
            "economique", []
        ):
            coeffs.append(
                {
                    "id": economic_parameter.get("id", ""),
                    "numero_action": action_parameters["action"],
                    "parameter_type": "Paramètre économique",
                    "parameter_name": economic_parameter.get("phase_projet", ""),
                    "parameter_subname": economic_parameter.get("maillon", ""),
                    "france_average": economic_parameter.get("part_france", ""),
                    "value": economic_parameter.get("part", ""),
                    "unit": "%",
                }
            )

        other_parameters = action_parameters.get("params_avances", {}).get("autres", {})
        # other parameters have such structure :
        #       >   "group_name": {
        #       >       "columns": [
        #       >           "nom",
        #       >           "unite",
        #       >           "valeur"
        #       >       ],
        #       >       "index": [
        #       >           id1,
        #       >           ...
        #       >       ],
        #       >       "data": [
        #       >           [
        #       >               "Parameter name",
        #       >               "Unit",
        #       >               value
        #       >           ],
        #       >           ...
        #       >       ]
        #       >   }
        for other_subname, other_subparameter in other_parameters.items():
            list_values = other_subparameter.get("columns", [])
            # handle various parameters names
            name_parameter_name = "nom"
            if "nom_parametre" in list_values:
                name_parameter_name = "nom_parametre"
            elif "paramètre" in list_values:
                name_parameter_name = "paramètre"
            elif "gain énergétique" in list_values:
                name_parameter_name = "gain énergétique"
            elif "répartition des rénovations par sous secteur" in list_values:
                name_parameter_name = "répartition des rénovations par sous secteur"
            unit_parameter_name = "unite"
            if "unité" in list_values:
                unit_parameter_name = "unité"

            # if we find the corresponding data in the advanced coeffs,
            # we use them to fill the export excel file
            if name_parameter_name in list_values:
                column_name_id = list_values.index(name_parameter_name)
            else:
                column_name_id = None
            if unit_parameter_name in list_values:
                column_unit_id = list_values.index(unit_parameter_name)
            else:
                column_unit_id = None
            column_value_id = list_values.index("valeur")

            for i, param in enumerate(other_subparameter.get("data", [])):
                d_param = {k: v for k, v in enumerate(param)}
                coeffs.append(
                    {
                        "id": other_subparameter.get("index", [])[i],
                        "numero_action": action_parameters["action"],
                        "parameter_type": "Autre paramètre",
                        "parameter_name": d_param.get(column_name_id, "Inconnu"),
                        "parameter_subname": "",
                        "france_average": "",
                        "value": d_param.get(column_value_id, "Inconnue"),
                        "unit": d_param.get(column_unit_id, "Non spécifiée"),
                    }
                )

    # we add information on related actions
    df_actions = pd.DataFrame(parameters.get("actions", []))
    df_coeffs = pd.DataFrame(coeffs)
    if df_coeffs.shape[0] == 0 and df_actions.shape[0] == 0:
        return pd.DataFrame([])
    enriched_df = df_coeffs.join(
        df_actions[["numero", "name", "categorie"]].set_index("numero"),
        on="numero_action",
        rsuffix="_action",
    )

    # we rename columns and sort the final dataframe
    enriched_df = enriched_df.rename(
        columns={
            "id": "#",
            "name": "Nom de l'action",
            "numero_action": "Action #",
            "categorie": "Catégorie de l'action",
            "parameter_name": "Nom du paramètre",
            "parameter_subname": "Nom du sous-paramètre",
            "parameter_type": "Type de paramètre",
            "unit": "Unité",
            "value": "Valeur renseignée",
            "france_average": "Valeur moyenne en France",
        }
    ).fillna("")

    return enriched_df[
        [
            "Catégorie de l'action",
            "Action #",
            "Nom de l'action",
            "#",
            "Type de paramètre",
            "Nom du paramètre",
            "Nom du sous-paramètre",
            "Unité",
            "Valeur moyenne en France",
            "Valeur renseignée",
        ]
    ].sort_values(by=["Action #", "Type de paramètre", "#"])


def _handle_main_input_parameters(parameters):
    df_coeffs = pd.DataFrame(
        [
            {
                "id": x["id"],
                "nom": x["nom"],
                "cochee": x.get("cochee", False),
                "numero_action": x["action"],
                "unit": x["unite"],
                "value": x["valeur_annee"][e],
                "year": e,
            }
            for x in parameters.get("coeffs", {})
            for e in x["valeur_annee"]
        ]
    )
    df_actions = pd.DataFrame(parameters.get("actions", []))
    if df_coeffs.shape[0] == 0 and df_actions.shape[0] == 0:
        return pd.DataFrame([])

    # joining both arrays
    enriched_df = df_coeffs.join(
        df_actions.set_index("numero"), on="numero_action", rsuffix="_action"
    )

    # removing useless columns
    # (useless as we sort the array at the end but we still keep it to know
    # that we deleted some columns)
    if "category_class" in enriched_df.columns:
        del enriched_df["category_class"]
    if "coeffs_ref" in enriched_df.columns:
        del enriched_df["coeffs_ref"]
    if "annee_ref" in enriched_df.columns:
        del enriched_df["annee_ref"]
    if "id" in enriched_df.columns:
        del enriched_df["id"]
    if "id_action" in enriched_df.columns:
        del enriched_df["id_action"]
    if "enabled" in enriched_df.columns:
        del enriched_df["enabled"]
    enriched_df["description"] = enriched_df["description"].fillna("")

    # renaming colums
    enriched_df = enriched_df.rename(
        columns={
            "name": "Nom de l'action",
            "categorie": "Catégorie de l'action",
            "description": "Description de l'action",
            # "enabled": "Action activée ?",
            # "id": "",
            "nom": "Nom du paramètre",
            "cochee": "Action activée",
            "numero_action": "Numéro de l'action",
            "unit": "Unité",
            "value": "Valeur renseignée",
            "year": "Année de la valeur",
        }
    ).fillna("NaN")

    # fixing some values to remove booleans
    enriched_df["Action activée"] = (
        enriched_df["Action activée"]
        .astype(str)
        .str.replace("True", "Oui")
        .str.replace("False", "Non")
    )

    # rearrange the final dataframe
    return enriched_df[
        [
            "Catégorie de l'action",
            "Numéro de l'action",
            "Nom de l'action",
            "Description de l'action",
            "Action activée",
            "Nom du paramètre",
            "Unité",
            "Année de la valeur",
            "Valeur renseignée",
        ]
    ].sort_values(by=["Catégorie de l'action", "Numéro de l'action"])


def _handle_investments(results_jobs):
    """
    Transforms input investments results in a usable dictionary.

    Parameters
    ----------
    results_jobs : dict
        the input data given through the API

    Returns
    -------
    dict
        the yearly sorted dictionary
    """
    return {year: results_jobs[year]["investissement"] for year in results_jobs}


def export_excel_results(
    region, parameters, results, trajectories, output_filepath, metadata
):
    """Generate an Excel file with input parameters, trajectories and results.

    This function requires the presence of an Excel file located in
    `ressources/export-impacts-strategie-territoriale-<region>.xlsx`

    This excel file can be filled with various parameters that will be replaced
    with corresponding values, either from input parameters, reference trajectories
    or results.

    Existing tokens that can be replaced are currently the following:

    * General information:

        * <Nom_Territoire>
        * <Nom_Strategie>
        * <Date_Export>
        * <Mail_Contact>
        * <Source_Donnees>

    * Input parameters:

        * <Input_Parameters>
        * <Input_AdvancedParameters>

    * Results:

        * <Impacts_GES_par_energie> and <Impacts_GES_par_secteur>
        * <Impacts_ConsoNRG_par_energie> and <Impacts_ConsoNRG_par_secteur>
        * <Impacts_Pollutant_par_energie> and <Impacts_Pollutant_par_secteur> where *Pollutant* is any of following list:

            * COVNM
            * NH3
            * NOX
            * PM10
            * PM25
            * SOX
        * <Impacts_ProdENR_par_energie>
        * <Impacts_Nb_Emplois_Region>
        * <Impacts_VA_Region>
        * <Impacts_Nb_Emplois_Territoire>
        * <Impacts_VA_Territoire>
        * <Impacts_Inv>
        * <Impacts_Retomb_fisc>
        * <Impacts_Baisse_facture>

    * Trajectories

        * <Trajectoire_GES_Off_PCAET>
        * <Trajectoire_ConsoNRG_Off_PCAET>
        * <Trajectoire_ProdENR_Off_PCAET>
        * <Trajectoire_Pollutant_Cible> where *Pollutant* is any of following list:

            * COVNM
            * NH3
            * NOX
            * PM10
            * PM25
            * SOX
        * <Trajectoire_GES_Cible>
        * <Trajectoire_ConsoNRG_Cible>
        * <Trajectoire_ProdENR_Cible>
        * <Trajectoire_GES_Off>: this parameter requires a variable name corresponding
            to the trajectory that must be specified after a # sign.
            For example, by writing `<Trajectoire_GES_Off>#SRADDET` in a cell,
            the corresponding values of the trajectory which name contains SRADDET
            will be used (if it exists). Same applies to the two following tokens.
        * <Trajectoire_ConsoNRG_Off>: same than <Trajectoire_GES_Off>
        * <Trajectoire_ProdENR_Off>: same than <Trajectoire_GES_Off>

    Parameters
    ----------
    impact_energie : dict
        dictionnaire des impacts ges, énergie, production EnR
    polluants : dict
        dictionnaire des données de polluants atmosphériques
    output_filepath : str

    """
    # Loads file
    filename = "{}/export-impacts-strategie-territoriale-{}.xlsx".format(
        settings.get("api", "upload_other_files", fallback="/tmp"), region
    )
    if not os.path.exists(filename):
        logger.warn(
            f"Use default export template at {EXCEL_EXPORT_IMPACTS} as previous file at {filename} doesn't exist."
        )
        filename = str(EXCEL_EXPORT_IMPACTS)
    wb = load_workbook(Path(filename))
    logger.info("Loaded workbook")

    # transforms params data
    input_parameters = _handle_input_parameters(parameters)
    logger.info("Loaded input parameters")

    # transforms data for easier manipulation later
    investments = _handle_investments(results.get("impact_emplois", {}))
    logger.info("Loaded investments")

    # handles PCAET if enabled here
    associations_keys_pcaet_results = {
        "consommation_ener": "energie_economisee",
        "emission_ges": "emission_ges",
        "enr_production": "energie_produite",
    }
    if trajectories.get("pcaet", False):
        pcaet_ref_data_trajectories = {
            associations_keys_pcaet_results[key]: _convert_pcaet_trajectory_data(
                trajectories.get("target", {}),
                key,
                results.get(associations_keys_pcaet_results[key], {}).get(
                    "max_annee", 2019
                ),
            )
            for key in ["consommation_ener", "emission_ges", "enr_production"]
        }
    else:
        pcaet_ref_data_trajectories = {}
    logger.info("Loaded pcaet trajectory")

    # handles custom trajectory
    custom_data_trajectories = {
        key: _convert_custom_trajectory_data(trajectories.get("custom", {})[key])
        for key in trajectories.get("custom", {}).keys()
    }
    logger.info("Loaded custom trajectory")

    # if we have at least one result
    retombees_fiscales = nested_dict_to_df(results.get("retombees_fiscales", {}))
    retombees_fiscales = retombees_fiscales.transpose().to_dict()

    data_emplois, data_va = prepare_data_for_jobs(results.get("impact_emplois", {}))
    logger.info("Loaded jobs data")

    # Keys for replacement
    keys_to_replace = {
        "<Impacts_GES_par_secteur>": {
            "data": results.get("emission_ges", {}).get("secteur", {}),
        },
        "<Impacts_GES_par_energie>": {
            "data": results.get("emission_ges", {}).get("energie", {}),
        },
        "<Impacts_ConsoNRG_par_energie>": {
            "data": results.get("energie_economisee", {}).get("energie", {}),
        },
        "<Impacts_ConsoNRG_par_secteur>": {
            "data": results.get("energie_economisee", {}).get("secteur", {}),
        },
        "<Impacts_ProdENR_par_energie>": {
            "data": results.get("energie_produite", {}).get("energie", {}),
        },
        "<Trajectoire_GES_Off>": {
            "input_sub_trajectory_name": "emission_ges",
        },
        "<Trajectoire_ConsoNRG_Off>": {
            "input_sub_trajectory_name": "energie_economisee",
        },
        "<Trajectoire_ProdENR_Off>": {
            "input_sub_trajectory_name": "energie_produite",
        },
        "<Trajectoire_GES_Off_PCAET>": {
            "data": pcaet_ref_data_trajectories.get("emission_ges", {}),
            "empty_value": "Non applicable",
        },
        "<Trajectoire_ConsoNRG_Off_PCAET>": {
            "data": pcaet_ref_data_trajectories.get("energie_economisee", {}),
            "empty_value": "Non applicable",
        },
        "<Trajectoire_ProdENR_Off_PCAET>": {
            "data": pcaet_ref_data_trajectories.get("energie_produite", {}),
            "empty_value": "Non applicable",
        },
        "<Trajectoire_GES_Cible>": {
            "data": custom_data_trajectories.get("emission_ges", {}),
            "empty_value": "Non personnalisée",
        },
        "<Trajectoire_ConsoNRG_Cible>": {
            "data": custom_data_trajectories.get("energie_economisee", {}),
            "empty_value": "Non personnalisée",
        },
        "<Trajectoire_ProdENR_Cible>": {
            "data": custom_data_trajectories.get("energie_produite", {}),
            "empty_value": "Non personnalisée",
        },
        "<Impacts_Nb_Emplois_Region>": {
            "data": data_emplois.get("region", {}),
            "prepend_name": True,
        },
        "<Impacts_VA_Region>": {
            "data": data_va.get("region", {}),
            "prepend_name": True,
        },
        "<Impacts_Nb_Emplois_Territoire>": {
            "data": data_emplois.get("other_regional_level", {}),
            "prepend_name": True,
        },
        "<Impacts_VA_Territoire>": {
            "data": data_va.get("other_regional_level", {}),
            "prepend_name": True,
        },
        "<Impacts_Inv>": {
            "data": investments,
            "prepend_name": False,
        },
        "<Impacts_Retomb_fisc>": {
            "data": retombees_fiscales,
        },
        "<Impacts_Baisse_facture>": {
            "data": results.get("facture_energetique", {}),
            "prepend_name": False,
        },
        "<Input_Parameters>": {
            "data": input_parameters.get("main_coeffs", pd.DataFrame([])),
        },
        "<Input_AdvancedParameters>": {
            "data": input_parameters.get("advanced_coeffs", pd.DataFrame([])),
        },
    }

    pollutants = ["sox", "pm10", "pm25", "nh3", "covnm", "nox"]
    for pollutant in pollutants:
        keys_to_replace[f"<Impacts_{pollutant.upper()}_par_secteur>"] = {
            "data": results.get(pollutant, {}).get("secteur", {}),
        }
        keys_to_replace[f"<Impacts_{pollutant.upper()}_par_energie>"] = {
            "data": results.get(pollutant, {}).get("energie", {}),
        }
        keys_to_replace[f"<Trajectoire_{pollutant.upper()}_Cible>"] = {
            "data": custom_data_trajectories.get(pollutant, {}),
            "empty_value": "Non personnalisée",
        }

    custom_trajectories = {}
    # Parses the sheets
    for sheetname in wb.sheetnames:
        sheet = wb[sheetname]

        replace_values = {
            "<Nom_Territoire>": metadata.get("territory_name", ""),
            "<Nom_Strategie>": metadata.get("title", ""),
            "<Date_Export>": metadata.get("date_export", ""),
            "<Mail_Contact>": metadata.get("contact_email", ""),
            "<Source_Donnees>": metadata.get("source", ""),
        }

        search = keys_to_replace.keys()
        logger.info(f"Starts replacing values in {sheetname}")
        sheet, found, variables_tokens = replace_value_in_sheet(
            sheet, replace_values, search
        )
        logger.info(f"Replaced values in {sheetname}")

        # we add variable tokens data if not already present
        # so we dynamically enhance the keys_to_replace dictionary
        # with the new "values"
        for variable, token in variables_tokens.items():
            for key_to_replace in token:
                new_key_after_variable = f"{key_to_replace}#{variable}"
                # if we already have a corresponding element
                if new_key_after_variable in keys_to_replace:
                    continue

                if variable not in custom_trajectories:
                    # transforms data for easier manipulation later
                    custom_ref_data_trajectory = _handle_reference_trajectories(
                        trajectories.get("target_ref", {}), variable
                    )
                    logger.info(f"Loaded variable trajectory with parameter {variable}")
                    custom_trajectories[variable] = custom_ref_data_trajectory
                # we get the subtrajectory name
                sub_trajectory_name = keys_to_replace[key_to_replace][
                    "input_sub_trajectory_name"
                ]

                keys_to_replace[new_key_after_variable] = {
                    "data": custom_trajectories[variable].get(sub_trajectory_name, {})
                }

        # merges found values
        for k, v in found.items():
            if not "locations" in keys_to_replace[k]:
                keys_to_replace[k]["locations"] = []
            keys_to_replace[k]["locations"].append((sheetname, v))

    # stores rows to delete
    row_to_delete = {}

    # replaces all indicators results
    for indicator, metadata in keys_to_replace.items():
        logger.info(f"Starts replacing value for {indicator}")
        data = metadata.get("data", False)
        prepend_name = metadata.get("prepend_name", True)
        delete_row_if_empty = metadata.get("delete_row_if_empty", False)
        empty_value = metadata.get("empty_value", " ")

        for location in metadata.get("locations", []):
            sheetname = location[0]
            sheet = wb[sheetname]

            all_cells = location[1]

            for cell in all_cells:
                # we add the row to the row supposed to be deleted at the end
                if delete_row_if_empty and not data:
                    row_id = cell[0]
                    if not sheetname in row_to_delete:
                        row_to_delete[sheetname] = set()
                    row_to_delete[sheetname].add(row_id)
                else:
                    put_data_in_excel_sheet_at_cell(
                        sheet, cell, data, prepend_name, empty_value=empty_value
                    )

    # TOFIX: Problem with that is that merged cells are not merged anymore
    # we delete rows
    # we didn't do it earlier to prevent problems of coordinates
    # if we had deleted rows before putting all data in the sheets, some coordinates
    # that were computed first in `keys_to_replace` wouldn't be correct anymore
    for sheetname, rows in row_to_delete.items():
        sheet = wb[sheetname]
        rows_ordered = sorted(rows, reverse=True)
        for row in rows_ordered:
            sheet.delete_rows(row)

    wb.save(output_filepath)
    return output_filepath


def put_data_in_excel_sheet_at_cell(
    sheet, cell, data, prepend_name=True, remove_tags=True, empty_value=" "
):
    """
    Put data inside an excel sheet at the spot given by the cell coordinates
    Doesn't do anything to the formatting.

    Parameters
    ----------
    sheet : openpyxl worksheet
        the sheet to edit (edited inplace)
    cell : tuple
        the coordinates (row, column) in integers, (1,1) corresponding to the
        top left corner of the worksheet.
    data : pd.DataFrame or list or dict
        the list of data to insert. It must be structured as follows:

        .. code-block:: python

            [
                {
                    'nom': 'Sector name', 'couleur': '#1fa22E',
                    'confidentiel': 'non',
                    'data': [
                        {'annee': 1990, 'valeur': 9022.62},
                        {'annee': 2000, 'valeur': 9424.92},
                        ...
                    ]
                },
                {
                    'nom': 'Other sector', 'couleur': '#933588',
                    'confidentiel': 'non',
                    'data': [
                        {'annee': 1990, 'valeur': 3792.3},
                        {'annee': 2000, 'valeur': 4107.48},
                        ...
                    ]
                }
            ]

        or:

        .. code-block:: python

                {
                    "2019": nested_value,
                    ...
                }

        with `nested_value` either directly a value (float, etc.) or a dictionary
        containing multiple values that will be spreaded on different rows in the
        final document.
    """
    r, c = cell
    # if we received a pandas DataFrame
    if isinstance(data, pd.DataFrame):
        if data.shape[0] > 0:
            rows = dataframe_to_rows(data, index=False)
            for r_idx, row in enumerate(rows):
                for c_idx, value in enumerate(row):
                    clean_value = value or ""
                    sheet.cell(row=r + r_idx, column=c + c_idx, value=str(clean_value))
    # if we don't have data, we remove the value
    elif not data:
        if remove_tags:
            sheet.cell(r, c).value = empty_value
        else:
            pass
    # if we have directly a dict and not a list
    elif isinstance(data, dict):
        list_years = list(data.keys())
        list_years = sorted(list_years)
        for year in list_years:
            # we reset to left of area
            r = cell[0]
            # if we have a value again
            if isinstance(data[year], dict):
                # we go through the different values
                for key, value in data[year].items():
                    # if we have to print cell names
                    if prepend_name and c == cell[1]:
                        sheet.cell(r, c).value = str(key).capitalize()
                        sheet.cell(r, c + 1).value = value
                    # if we had to display column names in the first column
                    # then all values are moved one column to the right
                    # thus => +1
                    elif prepend_name:
                        sheet.cell(r, c + 1).value = value
                    # normal case
                    else:
                        sheet.cell(r, c).value = value
                    r += 1
                c += 1

            # else we probably have a real value right now
            else:
                sheet.cell(r, c).value = data[year]
                # move to next column
                c += 1
    # else if we have a list
    elif isinstance(data, list):
        for data_category in data:
            # reset to left cell
            c = cell[1]

            if prepend_name:
                # title
                sheet.cell(r, c).value = data_category["nom"]
                c += 1

            for data_year in data_category["data"]:
                value = data_year["valeur"]
                sheet.cell(r, c).value = value

                # move to next column
                c += 1
            # we move to next line
            r += 1
    elif isinstance(data, str):
        sheet.cell(r, c).value = data
    else:
        raise ValueError("Data provided are not correct")


async def update_strategy_export_template_file(region, env):
    now = datetime.datetime.now()
    sql = """
          UPDATE regions_configuration SET export_excel_update_date = $1 WHERE id = $2 AND env = $3
          """
    await execute(sql, now, region, env)
    return now
