﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module qui contient la classe dont le rôle
est de retourner toutes les caractéristiques
d'une analyse (métadonnées, représentations possibles)
"""


from terriapi.controller import analyse, compute_formula, fetch


class ChartMode:
    nom: str
    arg: str


async def get_chart_modes_by_indicator_conf(conf: dict) -> list[ChartMode]:
    """Retrieve available chart modes for a specific indicator configuration."""
    representations_possibles = []
    if not conf:
        return representations_possibles
    if conf.get("unit", None) == "%":
        representations_possibles.append({"nom": "Jauge territoire", "arg": "jauge"})
        representations_possibles.append(
            {"nom": "Jauge circulaire", "arg": "jauge-circulaire"}
        )

    if conf.get("data_ratio", False):
        representations_possibles.append(
            {"nom": "Courbes comparées", "arg": "courbes_croisees"}
        )

    if (
        conf.get("years", False)
        and conf.get("charts", "[null]") != "[null]"
        and not conf.get("data_ratio", True)
    ):
        if (
            conf.get("charts", False)
            and len(conf.get("years", [])) > 1
            and conf.get("concatenation", False) is None
        ):
            representations_possibles.append({"nom": "Courbes empilées", "arg": "line"})
            # representations_possibles.append(
            #     {"nom": "Courbes empilées (années activées)", "arg": "line-years"}
            # )
            representations_possibles.append(
                {"nom": "Diagramme en barres", "arg": "bar"}
            )
            # representations_possibles.append(
            #     {"nom": "Diagramme en barres (années activées)", "arg": "bar-years"}
            # )

    if conf.get("charts", "[null]") != "[null]" and not conf.get("data_ratio", True):
        representations_possibles.append(
            {"nom": "Diagrammes circulaires", "arg": "pie"}
        )
        representations_possibles.append({"nom": "Radar", "arg": "radar"})

    if conf.get("concatenation", False) is not None:
        representations_possibles.append({"nom": "Histogramme", "arg": "histogramme"})
        representations_possibles.append(
            {"nom": "Histogramme Normalisé", "arg": "histogramme-normalise"}
        )
        representations_possibles.append(
            {"nom": "Courbes historiques", "arg": "courbes-historiques"}
        )

    if conf.get("concatenation", False) is None:
        representations_possibles.append({"nom": "pictogramme", "arg": "marqueur-svg"})

    if conf.get("concatenation", False) is None:
        representations_possibles.append(
            {"nom": "Nom simple de l'indicateur", "arg": "analysis-launcher"}
        )
        representations_possibles.append({"nom": "Carte", "arg": "map"})

    return representations_possibles


class IndicateurTableauBord:
    """Obtention des métadonnées d'un indicateur et
    définition des représentations possibles

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    id_indicateur : int
        identifiant de l'indicateur
    """

    def __init__(self, region, id_indicateur):
        self.region = region
        self.id_indicateur = id_indicateur

    @property
    def schema(self):
        return self.region.replace("-", "_")

    async def _filtre_initial_modalites_par_categorie(self, filtre_initial):
        req = """
              SELECT DISTINCT c.categorie, c.type, c.ordre, array_agg(cat.modalite ORDER BY cat.ordre) as liste_modalites
              FROM meta.chart c
              JOIN meta.categorie cat ON cat.nom = c.categorie
              WHERE c.indicateur = $1
              AND cat.region = $2
              GROUP BY c.categorie, c.type, c.ordre
              ORDER BY c.ordre;
              """
        res = await fetch(req, self.id_indicateur, self.region)
        modalites_par_categorie = {}
        for element in res:
            modalites_par_categorie[element["categorie"]] = [
                {"filtre_categorie": element["categorie"] + "." + modalite}
                for modalite in element["liste_modalites"]
            ]
            # if we have a switch button or a selection field, we only put first category in the values
            if element["type"] in ["switch-button", "selection", "slider"]:
                # we still need all possible values in case we need to display the alternatives
                modalites_par_categorie["____all_" + element["categorie"]] = (
                    modalites_par_categorie[element["categorie"]]
                )
                modalites_par_categorie[element["categorie"]] = modalites_par_categorie[
                    element["categorie"]
                ][0:1]
        if filtre_initial:
            groupeFiltresCategorie = filtre_initial.split(".")[0]
            modalites_par_categorie[groupeFiltresCategorie] = [
                {"filtre_categorie": filtre_initial}
            ]
        return modalites_par_categorie

    async def _representations_possibles(self, caracteristiques_indicateur):
        return await get_chart_modes_by_indicator_conf(caracteristiques_indicateur)

    async def meta_donnees_indicateur(self, caracteristiques_indicateur=None) -> dict:
        if not caracteristiques_indicateur:
            caracteristiques_indicateur = await analyse.analysis(
                self.region, self.id_indicateur
            )
        if len(caracteristiques_indicateur) == 0:
            return {}
        representations_possibles = await self._representations_possibles(
            caracteristiques_indicateur
        )
        definition_indicateur = compute_formula(caracteristiques_indicateur["data"])
        modalites_par_categorie = await self._filtre_initial_modalites_par_categorie(
            caracteristiques_indicateur["filter"]
        )
        if caracteristiques_indicateur["type"] == "choropleth_cat":
            carto_category = await fetch(
                """
                SELECT modalite_id, modalite, couleur, ordre
                FROM meta.indicateur_representation_details
                JOIN meta.categorie ON value=nom
                WHERE indicateur_id=$1 AND name='carto_category' AND region=$2
                ORDER BY ordre
                """,
                self.id_indicateur,
                self.region,
            )
            caracteristiques_indicateur["carto_category"] = [
                dict(modalite) for modalite in carto_category
            ]
        caracteristiques_indicateur["representations_possibles"] = (
            representations_possibles
        )
        caracteristiques_indicateur["definition_indicateur"] = definition_indicateur
        caracteristiques_indicateur["region"] = self.region
        caracteristiques_indicateur["schema_region"] = self.schema
        caracteristiques_indicateur["filtre_initial_modalites_par_categorie"] = (
            modalites_par_categorie
        )
        return caracteristiques_indicateur
