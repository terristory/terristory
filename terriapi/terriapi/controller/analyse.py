﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module de gestion des requêtes SQL pour les analyses."""
from __future__ import annotations

import json

import geopandas
import numpy as np
import pandas as pd

from terriapi import controller
from terriapi.controller import compute_formula, execute, fetch
from terriapi.controller.donnees_pour_representation import DonneesPourRepresentations
from terriapi.controller.strategy_actions.passage_table import PassageTable


async def get_region_code(region: str) -> str | None:
    schema_region = region.replace("-", "_")
    requete = """
              select code from {schema_region}.region;
              """.format(
        schema_region=schema_region
    )

    code_region = await fetch(requete)
    return code_region[0]["code"] if code_region else None


async def list_categories(region: str) -> list:
    sql = """
          select distinct nom
          from meta.categorie
          where region = $1;
          """
    res = await fetch(sql, region)
    return [x["nom"] for x in res]


async def analysis(
    region: str,
    analyse_id: int | None = None,
    only_active: bool = True,
    with_territorialsynthesis: bool = False,
    only_exportable: bool = False,
) -> list | dict:
    """Récupération de la liste des analyses et de leurs métadonnées
    (paramètres de visualisations)

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    analyse_id : [optional] int
        identifiant d'analyse
    only_active : [optional] bool
        pour préciser si on veut récupérer toutes les analyses y compris celles non activées
        (sans effet si `analyse_id` a été spécifié)
    """
    where_add = ""
    if analyse_id is not None:
        where_add = "and a.id = $2"
    elif only_active:
        where_add = "and a.active"

    where_exportable = " and a.donnees_exportables <> '' " if only_exportable else ""

    select_territorialsynthesis = ""
    join_territorialsynthesis = ""
    groupby_territorialsynthesis = ""
    if with_territorialsynthesis:
        select_territorialsynthesis = ", cb.order"
        join_territorialsynthesis = (
            " join meta.cesba cb on cb.indicateur_id = a.id and cb.order is not null"
        )
        groupby_territorialsynthesis = ", cb.order"

    sql = f"""
        select
            a.*
            {select_territorialsynthesis},
            COALESCE(jsonb_agg(DISTINCT c.*) FILTER (WHERE c.categorie IS NOT NULL), '[]'::jsonb) AS charts,
            COALESCE(jsonb_agg(DISTINCT si.*) FILTER (WHERE si.indicator IS NOT NULL), NULL::jsonb) AS sub_indicator,
            (SELECT EXISTS (SELECT 1 FROM meta.sub_indicator WHERE indicator = a.id)) AS is_sub_indicator,
            COALESCE(jsonb_object_agg(d.name, d.value) FILTER (WHERE d.name IS NOT NULL), '{{}}'::jsonb) AS representation_details,
            COALESCE(jsonb_object_agg(ia.unit_id, ia.is_default_for_zone_type) FILTER (WHERE ia.unit_id IS NOT NULL), '{{}}'::jsonb) AS units_default_details,
            COALESCE(jsonb_object_agg(ia.unit_id, ia.is_hidden_for_zone_type) FILTER (WHERE ia.unit_id IS NOT NULL), '{{}}'::jsonb) AS units_hidden_details
        from meta.indicateur a
        left join meta.chart c on c.indicateur = a.id
        left join meta.sub_indicator si on si.parent = a.id
        left join meta.indicateur_representation_details d on d.indicateur_id = a.id
        left join meta.data_units_indicators_association ia on ia.indicator_id = a.id
        {join_territorialsynthesis}
        where a.region = $1
        {where_add}
        {where_exportable}
        group by
            a.id
            , a.nom
            , a.data
            , a.data_deuxieme_representation
            , a.type
            , a.color_start
            , a.color_end
            , a.valeur_filtre_defaut
            , a.titre_legende
            , a.titre_legende_deuxieme_representation
            , a.donnees_exportables
            , a.data_type
            , a.titre_dans_infobulle
            , a.afficher_calcul_et_donnees_table
            , a.titre_graphiques_indicateurs
            {groupby_territorialsynthesis}
        order by a.ordre_ui_theme, ordre_indicateur, a.nom
    """

    analyses = (
        await fetch(sql, region)
        if analyse_id is None
        else await fetch(sql, region, analyse_id)
    )

    if not analyses:
        return {} if analyse_id is not None else []

    parsed_analyses = []
    for analysis in analyses:
        parsed_analysis = dict(analysis)

        # Parse lists and sort
        parsed_analysis["charts"] = json.loads(parsed_analysis["charts"])
        parsed_analysis["charts"].sort(
            key=lambda x: x.get("ordre", 0) or 0 if x is not None else 0
        )
        parsed_analysis["representation_details"] = json.loads(
            parsed_analysis["representation_details"]
        )
        parsed_analysis["units_default_details"] = json.loads(
            parsed_analysis["units_default_details"]
        )
        parsed_analysis["units_hidden_details"] = json.loads(
            parsed_analysis["units_hidden_details"]
        )
        if parsed_analysis["classification_config"]:
            parsed_analysis["classification_config"] = json.loads(
                parsed_analysis["classification_config"]
            )

        parsed_analyses.append(parsed_analysis)

    return parsed_analyses[0] if analyse_id is not None else parsed_analyses


async def list_indicator_categories(region: str, indicator_id: int) -> list:
    sql = """
          SELECT c.titre as title, c.categorie as key, 
          JSONB_AGG(JSONB_BUILD_OBJECT('id', mc.modalite_id, 'value', mc.modalite) ORDER BY mc.ordre) as modalities
          FROM meta.chart c 
          LEFT JOIN meta.categorie mc ON mc.nom = c.categorie AND mc.region = $1
          WHERE c.indicateur = $2
          GROUP BY c.titre, c.categorie, c.ordre
          ORDER BY c.ordre
          """
    res = await fetch(sql, region, indicator_id)
    return [dict(x) for x in res]


async def list_confid_layers(region: str):
    """
    Retrieve confid layers parameters for current region.

    Parameters
    ----------
    region : str
        key used for regional schema
    """
    schema = region.replace("-", "_")
    sql = f"""
        select distinct(type_analyse) from {schema}.confid_maille
        ORDER BY type_analyse
    """

    return await fetch(sql)


async def analysis_ui_themes(region: str):
    """Récupération de la liste des thèmes des indicateurs
    (paramètres de visualisations)

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    """
    sql = f"""
        select distinct(ui_theme) as label from meta.indicateur
        where region = $1
        order by ui_theme
    """

    return await fetch(sql, region)


async def update_analysis_ui_theme(data, region):
    """Mise à jour de l'ordre et du thème d'un indicateur

    Parameters
    ----------
    data : json
        {themes : {id_analyse: ordre} ... }
    """

    for theme in data:
        for analyse_id in data[theme]:
            ordre = data[theme][analyse_id]
            sql = """
                update meta.indicateur
                set ordre_indicateur = $1,
                    ui_theme = $2
                where id = $3 AND region = $4
                """
            await execute(sql, int(ordre), theme, int(analyse_id), region)


async def rename_analysis_ui_theme(old_name, new_name, region):
    """Rename indicators' category.

    Parameters
    ----------
    old_name : str
        Old category name
    new_name : str
        New category name
    region : str
        Region name
    """

    sql = """
        update meta.indicateur
        set ui_theme = $1
        where ui_theme = $2 AND region = $3
    """
    await execute(sql, new_name, old_name, region)


async def update_themes_ui_theme(data, region: str):
    """Mise à jour de l'ordre des themes

    Parameters
    ----------
    data : json
        {themes : {id_analyse: ordre} ... }
    """
    ordre = 1
    for theme in data:
        sql = f"""
                update meta.indicateur
                set ordre_ui_theme = $1
                where region = $2
                  and ui_theme = $3
                """
        await execute(sql, ordre, region, theme)
        ordre += 1


async def activate_analysis(analyse_id: int):
    """Activation d'une analyse

    Parameters
    ----------
    analyse_id : int
        identifiant d'analyse
    """
    sql = """
        update meta.indicateur a
        set active = true
        where id = $1"""
    await execute(sql, analyse_id)


async def deactivate_analysis(analyse_id: int):
    """Désactivation d'une analyse

    Parameters
    ----------
    analyse_id : int
        identifiant d'analyse
    """
    sql = """
        update meta.indicateur a
        set active = false
        where id = $1"""
    await execute(sql, analyse_id)


async def get_global_irradiation(region, zone, zone_id):
    """Récupère les valeurs d'irradiations moyennes
    pour le territoire concerné
    """
    schema = region.replace("-", "_")

    if zone == "commune":
        results = await fetch(
            f"""
            SELECT
                AVG(irradiation_systsuivi)::int as suivi,
                AVG(irradiation_systfixe)::int as fixe
            FROM {schema}.global_irradiation
            WHERE code = $1
            """,
            zone_id,
        )
    else:
        results = await fetch(
            f"""
            SELECT
                AVG(irradiation_systsuivi)::int as suivi,
                AVG(irradiation_systfixe)::int as fixe
            FROM {schema}."{zone}" z
            JOIN {schema}.commune c ON ARRAY[c.code] && z.communes
            JOIN {schema}.global_irradiation i ON i.code = c.code
            WHERE z.code = $1
            """,
            zone_id,
        )

    if results:
        return results[0]


async def check_year_in_data_table(
    region: str, zone_type: str, zone_id: str, table_name: str, year: int | None
) -> int | None:
    schema = region.replace("-", "_")
    req = f"""
        SELECT DISTINCT annee FROM {schema}.{table_name} as a
                JOIN {schema}.territoire t on t.commune = a.commune
                AND t.type_territoire = $1
                AND t.code = $2;
    """
    rset = await fetch(req, zone_type, zone_id)
    years: list[int] = [x["annee"] for x in rset]
    if not years:
        return year
    return year if year in years else max(years)


async def total_territory_value(
    region: str,
    zone_type: str,
    zone_id: str,
    table_name: str,
    year: int | None = None,
    custom_filters: dict = {},
    filter: list[str] | None = None,
) -> float:
    """
    Get the total value for a simple analysis (no ratio, no confid...) in a given territory

    Parameters
    ----------
    year : int|None
        By default: last available year
    """
    df = await total_value_by_mesh(
        region, table_name, zone_type, zone_id, None, year, custom_filters, filter
    )
    return df.loc[0, "valeur"] if not df.empty else 0


async def total_value_by_mesh(
    region: str,
    table_name: str,
    zone_type: str = "region",
    zone_id: str | None = None,
    mesh_type: str | None = None,
    year: int | None = None,
    custom_filters: dict = {},
    filter: list[str] | None = None,
) -> pd.DataFrame:
    """
    Get the value for a simple analysis (no ratio, no confid...) in a given territory,
    detailed by a given mesh level

    Parameters
    ----------
    year : int|None
        By default: last available year

    Returns
    -------
    DataFrame with columns: ["code", "valeur"]
    """
    schema = region.replace("-", "_")
    if zone_id is None and zone_type == "region":
        zone_id = await get_region_code(region)
    if zone_id is None:
        raise ValueError("Cannot infer default zone_id for non-region scales")
    if mesh_type is None:
        mesh_type = zone_type
    year = await check_year_in_data_table(region, zone_type, zone_id, table_name, year)

    params = [zone_type, zone_id, mesh_type, year]

    custom_condition = ""
    for col, val in custom_filters.items():
        params.append(str(val))
        custom_condition += f"AND {col}::text = ${len(params)} "

    filter_join = ""
    filter_condition = ""
    if filter:
        category = filter[0].split(".", 1)[0]
        filter_join = f"LEFT JOIN meta.categorie c ON c.modalite_id = data.{category} AND c.nom='{category}' AND c.region='{region}'"
        conditions = []
        for modality in filter:
            params.append(modality.split(".", 1)[1])
            conditions.append(f"c.modalite = ${len(params)}")
        filter_condition = f"AND({' OR '.join(conditions)})"

    req = f"""
        SELECT mesh.code, SUM(valeur) AS valeur
        FROM {schema}.{table_name} AS data
        JOIN {schema}.territoire zone ON data.commune = zone.commune
        JOIN {schema}.territoire mesh ON data.commune = mesh.commune
        {filter_join}
        WHERE zone.type_territoire = $1
            AND zone.code = $2
            AND mesh.type_territoire = $3
            AND data.annee = $4
            {custom_condition}
            {filter_condition}
        GROUP BY mesh.code
    """
    rset = await fetch(req, *params)
    return pd.DataFrame([dict(x) for x in rset])


async def territory_value_by_category(
    region: str,
    table_name: str,
    category_name: str,
    zone_type: str,
    zone_id: str,
    year: int | None = None,
    custom_filters: dict = {},
    filter: list[str] | None = None,
) -> pd.DataFrame:
    """
    Get the value for a simple analysis (no ratio, no confid...) in a given territory,
    detailed by a given category

    Parameters
    ----------
    year : int|None
        By default: last available year

    Returns
    -------
    DataFrame with columns: [category_name, "valeur"]
    """
    data = await value_by_mesh_and_category(
        region,
        table_name,
        category_name,
        zone_type,
        zone_id,
        None,
        year,
        custom_filters,
        filter,
    )
    return data[[category_name, "valeur"]]


async def value_by_mesh_and_category(
    region: str,
    table_name: str,
    category_name: str,
    zone_type: str = "region",
    zone_id: str | None = None,
    mesh_type: str | None = None,
    year: int | None = None,
    custom_filters: dict = {},
    filter: list[str] | None = None,
) -> pd.DataFrame:
    """
    Get the value for a simple analysis (no ratio, no confid...) in a given territory,
    detailed by given category and mesh level

    Parameters
    ----------
    year : int|None
        By default: last available year

    Returns
    -------
    DataFrame with columns: ["code", category_name, "valeur"]
    """
    schema = region.replace("-", "_")
    if zone_id is None and zone_type == "region":
        zone_id = await get_region_code(region)
    if zone_id is None:
        raise ValueError("Cannot infer default zone_id for non-region scales")
    if mesh_type is None:
        mesh_type = zone_type
    year = await check_year_in_data_table(region, zone_type, zone_id, table_name, year)

    params = [zone_type, zone_id, mesh_type, year]

    custom_condition = ""
    for col, val in custom_filters.items():
        params.append(str(val))
        custom_condition += f"AND {col}::text = ${len(params)} "

    filter_join = ""
    filter_condition = ""
    if filter:
        category = filter[0].split(".", 1)[0]
        filter_join = f"LEFT JOIN {schema}.{category} c ON c.id = data.{category}"
        conditions = []
        for modality in filter:
            params.append(modality.split(".", 1)[1])
            conditions.append(f"c.nom = ${len(params)}")
        filter_condition = f"AND({' OR '.join(conditions)})"

    req = f"""
        SELECT mesh.code, {category_name}, SUM(valeur) AS valeur
        FROM {schema}.{table_name} AS data
        JOIN {schema}.territoire zone ON data.commune = zone.commune
        JOIN {schema}.territoire mesh ON data.commune = mesh.commune
        {filter_join}
        WHERE zone.type_territoire = $1
            AND zone.code = $2
            AND mesh.type_territoire = $3
            AND data.annee = $4
            {custom_condition}
            {filter_condition}
        GROUP BY mesh.code, {category_name}
    """
    rset = await fetch(req, *params)
    return pd.DataFrame([dict(x) for x in rset])


async def get_export_dataframe(
    zone: str,
    maille: str,
    zone_id: str,
    analysis_meta: dict,
    year: int | None,
) -> pd.DataFrame:
    """
    Return a pandas Dataframe representing the analysis data to export to csv/excel.
    If year is not specified, every available year for the analysis will be included.
    """

    async def get_dataframe_for_year(year: int | None) -> pd.DataFrame:
        donnees_pour_representations = DonneesPourRepresentations(
            analysis_meta, maille, zone, zone_id, specific_year=year
        )
        data = await donnees_pour_representations.donnees_finales(
            "export CSV", 0, {}, reinitialiser_filtres=True
        )
        data = pd.DataFrame(data["export"]) if "export" in data else pd.DataFrame()
        if year is not None:
            data["annee"] = year
        return data

    if year is None and analysis_meta.get("years") is not None:
        # Si aucune année n'est spécifiée, on exporte toutes les années disponibles
        dataParts = (get_dataframe_for_year(year) for year in analysis_meta["years"])
        data = pd.concat([await p for p in dataParts], axis=0, ignore_index=True)
    else:
        data = await get_dataframe_for_year(year)

    if "confidentiel" in data:
        data["confidentiel"] = data["confidentiel"].replace(np.nan, False)
        data["confidentiel"] = data["confidentiel"].apply(lambda x: str(x).lower())

    # Reorder columns
    columns = ["nom", "code", "annee", "val", "x", "y", "confidentiel"]
    columns[2:2] = (col for col in data.columns if col not in columns)
    columns = [col for col in columns if col in data]
    # if we have any estimate year
    estimated_years = analysis_meta.get("estimated_years")
    if estimated_years:
        columns.append("type de donnees")
        data["type de donnees"] = np.where(
            data["annee"].astype(int).isin(estimated_years), "Estimée", "Consolidée"
        )
    data = data.reindex(columns=columns)

    # Convert to number
    if "val" in data:
        data["val"] = pd.to_numeric(data["val"], errors="ignore")

    # Rename columns with additional info
    data = data.rename(
        columns={
            "val": "valeur_" + analysis_meta["unit"],
            "code": "code_" + maille,
            "x": "x (EPSG:3857)",
            "y": "y (EPSG:3857)",
        }
    )
    return data


async def get_export_geo_dataframe(
    zone: str,
    maille: str,
    zone_id: str,
    analysis_meta: dict,
    year: int | None,
) -> geopandas.GeoDataFrame:
    """
    Return a GeoDataframe representing the analysis data to export to gpkg/shp/geojson.
    If year is not specified, every available year for the analysis will be included.
    """

    async def get_dataframe_for_year(year: int | None) -> pd.DataFrame:
        donnees_pour_representations = DonneesPourRepresentations(
            analysis_meta, maille, zone, zone_id, specific_year=year
        )
        data = await donnees_pour_representations.donnees_finales(
            "export CSV", 0, {}, reinitialiser_filtres=True
        )
        data = pd.DataFrame(data["map"]) if "map" in data else pd.DataFrame()
        if year is not None:
            data["annee"] = year
        return data

    if year is None and analysis_meta.get("years") is not None:
        # Si aucune année n'est spécifiée, on exporte toutes les années disponibles
        dataParts = (get_dataframe_for_year(year) for year in analysis_meta["years"])
        data = pd.concat([await p for p in dataParts], axis=0, ignore_index=True)
    else:
        data = await get_dataframe_for_year(year)

    if data.empty:
        return geopandas.GeoDataFrame(data)

    data.drop(["x", "y"], axis=1, inplace=True)

    if "confidentiel" in data:
        data["confidentiel"] = data["confidentiel"].replace(np.nan, False)
        data["confidentiel"] = data["confidentiel"].apply(lambda x: str(x).lower())
        data["val"] = pd.to_numeric(data["val"], errors="coerce")

    # if we have any estimate year
    estimated_years = analysis_meta.get("estimated_years")
    if estimated_years:
        data["type de donnees"] = np.where(
            data["annee"].astype(int).isin(estimated_years), "Estimée", "Consolidée"
        )

    geometry = await fetch(
        """SELECT code, ST_AsText(geom) AS geom
        FROM {schema}.{maille} GROUP BY code""".format(
            maille=maille, schema=analysis_meta["region"].replace("-", "_")
        )
    )
    geometry = pd.DataFrame(dict(row) for row in geometry)
    geometry.set_index("code", inplace=True)
    data.set_index("code", inplace=True, drop=True)

    gdf = geopandas.GeoDataFrame(data)
    gdf["geom"] = geopandas.GeoSeries.from_wkt(geometry["geom"])
    gdf.set_geometry("geom", crs="EPSG:3857", inplace=True)
    gdf.reset_index(inplace=True)

    gdf.rename(
        columns={
            "val": "valeur_" + analysis_meta["unit"],
            "code": "code_" + maille,
        },
        inplace=True,
    )
    return gdf


async def delete_analysis(region, id):
    """
    Delete an indicator from the database.

    Parameters
    ----------
    region: str
        region key
    id: int
        indicator id
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            query = f"""DELETE FROM meta.chart
            WHERE indicateur = $1"""
            await conn.execute(query, int(id))

            query = f"""SELECT EXISTS (
                SELECT FROM 
                    pg_tables
                WHERE 
                    schemaname = 'tests' AND 
                    tablename  = 'resultats'
                );"""
            res = await conn.fetch(query)
            if res[0]["exists"]:
                query = f"""DELETE FROM tests.resultats
                WHERE indicateur = $1"""
                await conn.execute(query, int(id))
            query = f"""DELETE FROM meta.indicateur
            WHERE region = $1 AND id = $2"""
            await conn.execute(query, region, int(id))


async def delete_data_table(region, table_name):
    """
    Delete a data table from the database. Also disables related indicators
    and remove any association inside `meta.perimetre_geographique` table.

    Parameters
    ----------
    region : str
        region key
    table_name : str
        table name corresponding to both the real PostgreSQL table and the
        value inside `meta.perimetre_geographique` and indicateur formula.
    """
    schema = region.replace("-", "_")
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            query = f"""DELETE FROM meta.perimetre_geographique
            WHERE region = $1 AND nom_table = $2"""
            await conn.execute(query, region, table_name)
            query = f"""UPDATE meta.indicateur SET active = false WHERE data LIKE $1 AND region = $2"""
            await conn.execute(query, f"%{table_name}%", region)
            check_exists = """SELECT EXISTS (
                SELECT FROM information_schema.tables 
                WHERE  table_schema = $1
                AND    table_name   = $2
            );"""
            exists = await conn.fetchval(check_exists, schema, table_name)
            if exists:
                query = f"""DROP TABLE {schema}.{table_name}"""
                await conn.execute(query)
                return True
            else:
                return "Table de données manquante."


async def delete_category(
    region, category_name, related_indicators=[], related_dashboards_groups=[]
):
    """
    Delete a category from the database. Also disable related indicators, and
    delete all associations between these indicators and charts for this category.

    Parameters
    ----------
    region : str
        region key
    category_name : str
        category name (used as key inside queries)
    related_indicators : list, optional
        related indicators that must be disabled to prevent bugs, by default []
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            query = f"""SELECT id, graphiques FROM meta.tableau_thematique WHERE id = ANY($1)"""
            dashboards_groups = await conn.fetch(query, related_dashboards_groups)
            for group in dashboards_groups:
                graphiques = json.loads(group.get("graphiques", "[]"))
                for graph in graphiques:
                    # TODO: handle case when no more categories?
                    if category_name in graph["categories"]:
                        del graph["categories"][category_name]
                await conn.execute(
                    "UPDATE meta.tableau_thematique SET graphiques = $1 WHERE id = $2",
                    json.dumps(graphiques),
                    int(group["id"]),
                )
            query = f"""DELETE FROM meta.categorie
            WHERE nom = $1 AND region = $2"""
            await conn.execute(query, category_name, region)
            query = f"""DELETE FROM meta.chart
            WHERE categorie = $1 AND indicateur = ANY($2) """
            await conn.execute(query, category_name, related_indicators)
            query = f"""UPDATE meta.indicateur SET active = false WHERE id = ANY($1) AND region = $2"""
            await conn.execute(query, related_indicators, region)


async def get_territorial_strategy_protected_tables(region):
    """
    Retrieve protected tables for territorial strategy module inside current region.

    Parameters
    ----------
    region : str
        regional key

    Returns
    -------
    set
        protected tables names
    """
    passage_table = PassageTable(region)
    protected_tables = await passage_table.filter_by_type("table")
    return {r["match"] for r in protected_tables}


async def get_protected_tables(region):
    """
    Get protected tables for all modules in current region.

    Check territorialsynthesis, supra-regional goals and territorial strategy modules.

    Parameters
    ----------
    region : str
        regional key

    Returns
    -------
    set
        protected tables names
    """
    # get territorialsynthesis protected tables
    query = f"""SELECT i.data FROM meta.cesba c 
    LEFT JOIN meta.indicateur i ON i.id = c.indicateur_id
    WHERE i.region = $1"""
    territorialsynthesis_tables = await fetch(query, region)
    territorialsynthesis_tables = set(r["data"] for r in territorialsynthesis_tables)
    territorialsynthesis_tables = set(
        compute_formula(r).get("indicateur", "") for r in territorialsynthesis_tables
    )

    # get other protected tables
    strategy_tables = await get_territorial_strategy_protected_tables(region)
    return strategy_tables.union(territorialsynthesis_tables)


async def is_protected_analysis(region, analysis_id):
    """
    Get protected analyses in current region

    Check territorialsynthesis module.

    Parameters
    ----------
    region : str
        regional key

    Returns
    -------
    set
        protected tables names
    """
    # get territorialsynthesis protected tables
    query = f"""SELECT i.data FROM meta.cesba c 
    LEFT JOIN meta.indicateur i ON i.id = c.indicateur_id
    WHERE i.region = $1 AND c.indicateur_id = $2"""
    territorialsynthesis_tables = await fetch(query, region, analysis_id)

    return len(territorialsynthesis_tables) > 0


async def get_unit_from_analysis(region, unit_id, indicator_id, zone=None):
    """
    Get unit from analysis and ID

    Parameters
    ----------
    region : str
        regional key
    unit_id : int
        unit ID
    indicator_id : int
        indicator ID

    Returns
    -------
    dict
        the content related to this unit
    """
    if unit_id is None:
        query = f"""SELECT u.unit_id, u.unit_type, u.unit_name, u.conversion_factor
        FROM meta.data_units u
        LEFT JOIN meta.data_units_indicators_association AS ui ON ui.unit_id = u.unit_id
        WHERE u.region = $1 AND ui.indicator_id = $2 AND ui.is_default_for_zone_type = $3"""
        unit_content = await fetch(query, region, indicator_id, zone)
        return dict(unit_content[0]) if len(unit_content) > 0 else {}

    else:
        query = f"""SELECT u.unit_id, u.unit_type, u.unit_name, u.conversion_factor
        FROM meta.data_units u
        LEFT JOIN meta.data_units_indicators_association AS ui ON ui.unit_id = u.unit_id
        WHERE u.region = $1 AND ui.indicator_id = $2 AND u.unit_id = $3"""
        unit_content = await fetch(query, region, indicator_id, unit_id)
        return dict(unit_content[0]) if len(unit_content) > 0 else {}
