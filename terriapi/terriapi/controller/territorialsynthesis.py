﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Fonctions et requêtes SQL pour les notes et analyses territorialsynthesis
"""

from terriapi import controller
from terriapi.controller import execute, fetch


async def get_territorialsynthesis_notes(
    schema: str, zone: str, zone_id: str, code: bool = False
):
    """Récupération des notes d'un territoire
    et des statistiques sur tous les indicateurs territorialsynthesis

    Parameters
    ----------
    schema : str
        région (schéma en base)
    zone: str
        type de territoire (scot, pnr...)
    zone_id: str|int
        identifiant de territoire (peut être soit int soit str suivant le code utilisé)
    code: bool
        si code = True alors on fait la requête via le code territoire (insee)
        à la place de son identifiant

    Returns
    -------
    list[Dict]: liste des indicateurs avec
        identifiant de l'analyse
        numero de l'analyse
        thème de l'analyse
        valeur du territoire
        unité de l'indicateur
        note du territoire
        note de la médiane sur tous les territoires du même type
        valeur médiane
        valeur min
        valeur max
    """
    if not code:
        sql = f"""
        select
            a.id
            , a.nom
            , c.numero
            , c.theme
            , n.value
            , a.unit
            , n.note
            , s.median_note
            , s.median
            , s.vmin
            , s.vmax
            , a.years[1]
        from {schema}.cesba_stats s
        join meta.indicateur a on a.id = s.analysis
        join meta.cesba c on c.indicateur_id = s.analysis
        join {schema}.cesba_notes n
            on s.type_territoire = n.type_territoire
            and s.analysis = n.analysis
        where s.type_territoire = $1 and n.id_territoire = $2
        order by c.theme, c.numero
        """
        return await fetch(sql, zone, zone_id)
    else:
        sql = f"""
        select
            a.id
            , a.nom
            , c.numero
            , c.theme
            , n.value
            , a.unit
            , n.note
            , s.median_note
            , s.median
            , s.vmin
            , s.vmax
        from {schema}.cesba_stats s
        join meta.indicateur a on a.id = s.analysis
        join meta.cesba c on c.indicateur_id = s.analysis
        join {schema}.{zone} t on t.code = $2
        join {schema}.cesba_notes n
            on s.type_territoire = n.type_territoire
            and s.analysis = n.analysis
            and n.id_territoire = t.id
        where s.type_territoire = $1
        order by c.theme, c.numero
        """
        return await fetch(sql, zone, zone_id)


async def update_territorialsynthesis_stats(
    region, terr, aid, vmin, vmax, medi, median_note
):
    """Insertion des données de statisiques des indicateurs territorialsynthesis
    sur un type de territoire donné et une analyse donnée
    """
    await execute(
        f"""
        insert into {region}.cesba_stats (type_territoire, analysis, vmin, vmax,
                                 median, median_note)
        values ($1, $2, $3, $4, $5, $6)
        """,
        terr,
        aid,
        vmin,
        vmax,
        medi,
        median_note,
    )


async def update_territorialsynthesis_notes(region, notes):
    """Insertion des notes CESBA pour les couples types territoire / analyse"""
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            # if one fails, nothing should be inserted/commited
            await conn.copy_records_to_table(
                "cesba_notes",
                schema_name=region,
                records=notes,
                columns=[
                    "type_territoire",
                    "id_territoire",
                    "analysis",
                    "value",
                    "note",
                ],
            )
