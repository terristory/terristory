﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Fonctions et requêtes SQL sur les stations de mesures des indicateurs climat
"""

from terriapi.controller import fetch


async def get_stations_mesures(region, jeu_de_donnees):
    """
    Selection des données des stations de mesure qui ont des données pour l'indicateur selectionné

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)

    jeu_de_donnees : str
        Nom de l'indicateur climat
    """

    query = """
            select nom, longitude as x, latitude as y, altitude
            from {schema_region}_station_mesure.station_mesure
            where {jeu_de_donnees}=true
            """.format(
        schema_region=region, jeu_de_donnees=jeu_de_donnees
    )
    valeurs_par_station = await fetch(query)
    resultat = [dict(x) for x in valeurs_par_station]

    return resultat


async def get_value_par_station(
    region, jeu_de_donnees, stations_mesures, min_year, max_year
):
    """
    Selection des données des stations de mesure par station :
    Cette fonction permet de parcourir la liste des stations qui ont des données pour l'indicateur selectionné
    et récupérer tous les valeurs et libelle_mois pour chaque station depuis l'année minimal jusqu'à la date
    maximale de disponibilité de données

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)

    jeu_de_donnees : str
        Nom de l'indicateur climat
    """
    list_val_par_station = {}
    group_by = ""
    if jeu_de_donnees == "data_chaleur":
        select_column = "valeur"
    elif jeu_de_donnees == "data_enneigement":
        select_column = "valeur, jour_mois, libelle_mois"
    else:
        sql = f"""(select case when exists (select *
                                from INFORMATION_SCHEMA.COLUMNS 
                                where table_schema = $1 and 
                                         table_name = $2 and
                                        column_name = 'saison'
                                )
                                then 'saison'
                                else 'libelle_mois'
                                end)"""

        res = await fetch(sql, region, jeu_de_donnees)
        libelle_mois_saison = [dict(x) for x in res][0]["case"]
        select_column = f"sum(valeur) as valeur, {libelle_mois_saison}"
        group_by = f"group by {libelle_mois_saison}"

    for x in stations_mesures:
        year = {}
        for y in list(range(min_year, max_year + 1)):
            query = """
                    select {select_column}
                    from {schema_region}.{jeu_de_donnees} as d
                    where d.annee = $1
                    and d.nom = $2
                    {group_by}
                    """.format(
                schema_region=region,
                jeu_de_donnees=jeu_de_donnees,
                select_column=select_column,
                group_by=group_by,
            )
            valeurs_par_station_par_an = await fetch(query, y, x["nom"])
            resultat = [dict(z) for z in valeurs_par_station_par_an]
            year[y] = resultat
        list_val_par_station[x["nom"]] = year

    return list_val_par_station
