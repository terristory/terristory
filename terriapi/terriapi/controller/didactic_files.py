# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
import os

from sanic.log import logger

from terriapi import controller


async def get_list(region):
    """
    Get the list of didactic files related to a specific region

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)

    Returns
    -------
    list of dict
        list of all didactic files and their parameters in dictionaries
    """
    req = """
          select * from meta.didactic_file
          where region = $1;
          """
    resp = await controller.fetch(req, region)
    return [dict(i) for i in resp]


async def details_file(region, id):
    """
    Get the details for a specific didactic file in a region

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : int
        the didactic file ID

    Returns
    -------
    False if not found
    dict
        contain all parameters organized as follow:

            data : {
                catOrder1: {
                    id: "",
                    categoryTitle: "",
                    categoryOrder: "",

                    subCategory: {
                        subCatOrder1 : {
                            id: "",
                            subCategoryOrder: "",
                            subCategoryTitle: "",
                            typeAnalysis: "",
                            section : {
                                sectionOder1 : {
                                    id: "",
                                    sectionTitle:"",
                                    sectionComment:"",
                                    sectionOrder:"",
                                    analysis: {}
                                }
                            },
                        }
                    }
                }
            },
            metadata : {
                id : 0,
                fileTitle : "",
                description : ""
            }

    """
    req_file = """
          select * 
          from meta.didactic_file
          where id = $1
          and region = $2
          """

    resp = await controller.fetch(req_file, int(id), region)
    res = [dict(i) for i in resp]
    if len(res) == 0:
        return False

    didactic_file = {}
    data = {}
    metadata = {}
    metadata["fileTitle"] = res[0]["title"]
    metadata["description"] = res[0]["description"]
    metadata["id"] = res[0]["id"]

    req_cat = """ 
                SELECT *
                FROM meta.didactic_file_category
                WHERE didactic_file_id = $1
                ORDER BY category_order
            """

    resp_cat = await controller.fetch(req_cat, int(metadata["id"]))
    res_cat = [dict(i) for i in resp_cat]

    if len(res_cat) > 0:
        for cat in res_cat:
            category = {}
            sub_categories = {}
            category["id"] = cat["id"]
            category["categoryTitle"] = cat["title"]
            category["categoryOrder"] = cat["category_order"]

            id_cat = cat["id"]
            req_sub_cat = """ 
                    SELECT *
                    FROM meta.didactic_file_sub_category
                    WHERE category_id = $1
                    ORDER BY subcategory_order
                """
            resp_sub_cat = await controller.fetch(req_sub_cat, int(id_cat))
            res_sub_cat = [dict(i) for i in resp_sub_cat]

            if len(res_sub_cat) > 0:
                for subcat in res_sub_cat:
                    sub_category = {}
                    sections = {}
                    sub_category["id"] = subcat["id"]
                    sub_category["subCategoryOrder"] = subcat["subcategory_order"]
                    sub_category["subCategoryTitle"] = subcat["title"]
                    sub_category["typeAnalysis"] = subcat["type"]
                    sub_categories[sub_category["subCategoryOrder"]] = sub_category

                    req_section = """ 
                            SELECT *
                            FROM meta.didactic_file_section
                            WHERE subcategory_id = $1
                            ORDER BY section_order
                        """
                    resp_section = await controller.fetch(
                        req_section, int(subcat["id"])
                    )
                    res_section = [dict(i) for i in resp_section]

                    for sect in res_section:
                        section = {}
                        section["id"] = sect["id"]
                        section["sectionOrder"] = sect["section_order"]
                        section["sectionTitle"] = sect["title"]
                        section["sectionComment"] = sect["comment"]
                        section["analysis"] = json.loads(sect["analysis"])
                        sections[section["sectionOrder"]] = section

                    sub_category["section"] = sections
                category["subCategory"] = sub_categories
            else:
                category["subCategory"] = {}
            data[category["categoryOrder"]] = category

    # final result
    didactic_file["data"] = data
    didactic_file["metadata"] = metadata
    return didactic_file


async def delete_file(region, id):
    """
    Delete a didactic file in a specific region

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : int
        the didactic file ID

    Returns
    -------
    False if deletion fails
    list of dict
        list of all didactic file types and their parameters in dictionaries
    """
    req = """
          delete 
          from meta.didactic_file
          where id = $1
          and region = $2
          returning id
          """
    res = await controller.fetch(req, int(id), region)
    if not res:
        return False
    return res[0]["id"]


async def add_file(region, didactic_file):
    """
    Add a new didactic file in a specific region

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    didactic_file : dict
        contains data related to the new didactic file.

        Should follow the organization described below:

            data : {
                catOrder1: {
                    categoryTitle: "",
                    categoryOrder: "",

                    subCategory: {
                        subCatOrder1 : {
                            subCategoryOrder: "",
                            subCategoryTitle: "",
                            typeAnalysis: "",
                            section : {
                                sectionOder1 : {
                                    id: "",
                                    sectionTitle:"",
                                    sectionComment:"",
                                    sectionOrder:"",
                                    analysis: {}
                                }
                            },
                        }
                    }
                }
            },
            metadata : {
                fileTitle : "",
                description : "",
            }

    Returns
    -------
    boolean
        False if fails, true otherwise
    """

    title_file = didactic_file["metadata"]["fileTitle"]
    description = didactic_file["metadata"]["description"]

    try:
        async with controller.db.acquire() as conn:
            async with conn.transaction():
                sql_file = """
                        INSERT INTO meta.didactic_file
                        (title, description, region)
                        VALUES ($1, $2, $3)
                        returning id;
                        """
                id_file = await conn.fetch(sql_file, title_file, description, region)

                for category_id, category_data in didactic_file["data"].items():
                    category_title = category_data["categoryTitle"]
                    sql_cat = """
                        INSERT INTO meta.didactic_file_category
                        (title, didactic_file_id, category_order)
                        VALUES ($1, $2, $3)
                        returning id;
                        """
                    id_cat = await conn.fetch(
                        sql_cat,
                        category_title,
                        id_file[0]["id"],
                        int(category_id),
                    )

                    if len(category_data["subCategory"]) > 0:
                        for subcat_id, subcat_data in category_data[
                            "subCategory"
                        ].items():
                            sub_category_title = subcat_data["subCategoryTitle"]
                            analysis_type = subcat_data["typeAnalysis"]
                            sql_sub_cat = """
                                INSERT INTO meta.didactic_file_sub_category
                                (title, type, category_id, subcategory_order)
                                VALUES ($1, $2, $3, $4)
                                returning id;
                                """
                            id_subcat = await conn.fetch(
                                sql_sub_cat,
                                sub_category_title,
                                analysis_type,
                                id_cat[0]["id"],
                                int(subcat_id),
                            )
                            if (
                                "section" in subcat_data
                                and len(subcat_data["section"]) > 0
                            ):
                                for section_id, section_data in subcat_data[
                                    "section"
                                ].items():
                                    section_title = section_data["sectionTitle"]
                                    section_comment = section_data["sectionComment"]
                                    analysis_list = json.dumps(section_data["analysis"])
                                    sql_sub_cat = """
                                        INSERT INTO meta.didactic_file_section
                                        (title, comment, type, analysis, section_order, subcategory_id)
                                        VALUES ($1, $2, $3, $4, $5, $6);
                                        """
                                    await conn.fetch(
                                        sql_sub_cat,
                                        section_title,
                                        section_comment,
                                        analysis_type,
                                        analysis_list,
                                        int(section_id),
                                        id_subcat[0]["id"],
                                    )

    except EnvironmentError as e:
        logger.error(f"Erreur: {str(e)}")
        return False
    return True


async def update_file(region, id, didactic_file):
    """
    Update an existing didactic file in a specific region

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : int
        current didactic file ID
    didactic_file : dict
        contains data related to the new didactic file.

        Should follow the organization described below:

            data : {
                catOrder1: {
                    categoryTitle: "",
                    categoryOrder: "",

                    subCategory: {
                        subCatOrder1 : {
                            subCategoryOrder: "",
                            subCategoryTitle: "",
                            typeAnalysis: "",
                            section : {
                                sectionOder1 : {
                                    id: "",
                                    sectionTitle:"",
                                    sectionComment:"",
                                    sectionOrder:"",
                                    analysis: {}
                                }
                            },
                        }
                        }
                    }
                }
            },
            metadata : {
                fileTitle : "",
                description : "",
            }

    Returns
    -------
    boolean
        False if fails, true otherwise
    """
    title_file = didactic_file["metadata"]["fileTitle"]
    description = didactic_file["metadata"]["description"]

    try:
        async with controller.db.acquire() as conn:
            async with conn.transaction():
                sql_file = """
                        update meta.didactic_file
                        set title = $1 , description = $2
                        where id = $3 and region = $4;
                        """
                await conn.fetch(
                    sql_file,
                    title_file,
                    description,
                    int(id),
                    region,
                )
                if len(didactic_file["data"]) > 0:
                    # clear category and sub-category table
                    sql_del_cat = """
                            delete from meta.didactic_file_category
                            where didactic_file_id = $1
                            """
                    await conn.fetch(sql_del_cat, int(id))

                    for category_id, category_data in didactic_file["data"].items():
                        category_title = category_data["categoryTitle"]
                        # add new values
                        sql_cat = """
                            INSERT INTO meta.didactic_file_category
                            (title, didactic_file_id, category_order) 
                            VALUES ($1, $2, $3)
                            returning id;
                            """
                        id_cat = await conn.fetch(
                            sql_cat,
                            category_title,
                            int(id),
                            int(category_id),
                        )

                        if (
                            "subCategory" in category_data
                            and len(category_data["subCategory"]) > 0
                        ):
                            for subcat_id, subcat_data in category_data[
                                "subCategory"
                            ].items():
                                sub_category_title = subcat_data["subCategoryTitle"]
                                analysis_type = subcat_data["typeAnalysis"]

                                # add new values
                                sql_sub_cat = """
                                    INSERT INTO meta.didactic_file_sub_category
                                    (title, type, category_id, subcategory_order) 
                                    VALUES ($1, $2, $3, $4)
                                    returning id;
                                    """
                                id_subcat = await conn.fetch(
                                    sql_sub_cat,
                                    sub_category_title,
                                    analysis_type,
                                    id_cat[0]["id"],
                                    int(subcat_id),
                                )
                                if (
                                    "section" in subcat_data
                                    and len(subcat_data["section"]) > 0
                                ):
                                    for section_id, section_data in subcat_data[
                                        "section"
                                    ].items():
                                        section_title = section_data["sectionTitle"]
                                        section_comment = section_data["sectionComment"]
                                        analysis_list = json.dumps(
                                            section_data["analysis"]
                                        )
                                        sql_sub_cat = """
                                            INSERT INTO meta.didactic_file_section
                                            (title, comment, type, analysis, section_order, subcategory_id)
                                            VALUES ($1, $2, $3, $4, $5, $6);
                                            """
                                        await conn.fetch(
                                            sql_sub_cat,
                                            section_title,
                                            section_comment,
                                            analysis_type,
                                            analysis_list,
                                            int(section_id),
                                            id_subcat[0]["id"],
                                        )

    except EnvironmentError as e:
        logger.error(f"Erreur: {str(e)}")
        return False
    return True


async def delete_logo(region, id):
    """
    Delete a logo in a specific region

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : int
        the logo ID

    Returns
    -------
    False if deletion fails
    str
        the name of the logo deleted
    """
    path_sql = """
            select logo_path, name
            from meta.data_source_logo
            where id = $1
            and region = $2
    """
    res = await controller.fetch(path_sql, int(id), region)
    if len(res) == 0:
        return False
    path = res[0].get("logo_path", False)
    name = res[0].get("name", "")

    req = """
          delete 
          from meta.data_source_logo
          where id = $1
          and region = $2
          """
    await controller.fetch(req, int(id), region)

    if path and os.path.exists(path):
        os.remove(path)
    return name


async def details_logo(region, id):
    """
    Retrieve details of a logo in a specific region

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : int
        the logo ID

    Returns
    -------
    list of dict
        list of all logo corresponding to this ID in dictionaries
    """
    req = """
          select * 
          from meta.data_source_logo
          where id = $1
          and region = $2
          """

    resp = await controller.fetch(req, int(id), region)
    return [dict(i) for i in resp]
