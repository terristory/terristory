﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module pour les fonctions et requêtes SQL de la page suivi de trajectoire

i.e. historique de certaines analyses comme la consommation énergétique, la
production d'énergie renouvelable (aka prod enr)
"""

from __future__ import annotations

import asyncio
from datetime import datetime

import numpy as np
import pandas as pd
from sanic.log import logger

from terriapi.controller import QueryBuilder, fetch
from terriapi.controller.strategy_actions import (
    Commodity,
    DataSet,
    EnergyVector,
    Impacts,
    RenewableProd,
    Sector,
    Usage,
)
from terriapi.controller.strategy_actions.passage_table import PassageTable

CONFIDENTIEL_COULEUR = "#777778"
MAX_ANNEE_ACTION = 2050


async def categories_metadata(region, categorie):
    """Retourne les couleurs associées à chaque type d'une table des catégories

    Parameters
    ----------
    region : str
        Nom de la région (nom officiel complet en minuscule, sans accent, e.g. auvergne-rhone-alpes)
    categorie : str
        Nom d'une table des catégories (table en base)
    Returns
    -------
    dict
    """
    # TODO: avoid calling too many times
    if isinstance(categorie, (list, tuple, set)):
        req = """
            select nom, modalite_id, modalite as nom, couleur, ordre
            from meta.categorie
            where region = $1
            and nom = ANY($2);
            """
        rset = await fetch(req, region, categorie)
        res = pd.DataFrame([dict(x) for x in rset])
        return res
    req = """
          select modalite_id, modalite as nom, couleur, ordre
          from meta.categorie
          where region = $1
          and nom = $2;
          """
    rset = await fetch(req, region, categorie)
    res = pd.DataFrame([dict(x) for x in rset])
    res = res.rename(columns={"modalite_id": categorie})
    return res


async def _suivi_trajectoire_analyze(
    region,
    jeu_de_donnees,
    type_territoire,
    code_territoire,
    categorie: str | list,
    custom_filters: dict = {},
):
    """Retourne les données historiques d'une analyse par catégorie.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    analyse : str
        Nom de la table
    zone : str
        Zone géographique sélectionnée
    zone_id : str
        Identifiant de la zone géographique
    categorie : str
        Catégorie de l'analyse, e.g. secteur, energie, etc.

    Returns
    -------
    dict
    """
    custom_condition = ""
    custom_params = []
    for col, val in custom_filters.items():
        custom_condition += f"AND {col}::text = ${len(custom_params)+3} "
        custom_params.append(str(val))

    if isinstance(categorie, (list, tuple, set)):
        schema = region.replace("-", "_")
        columns = ", ".join([f"a.{c}" for c in set(categorie)])
        req = f"""
            select a.annee
            , {columns}
            , sum(valeur) as valeur
            from {schema}.{jeu_de_donnees} as a
            join {schema}.territoire as b on b.commune = a.commune
            where b.type_territoire = $1
            and b.code = $2
            {custom_condition}            
            group by a.annee, {columns}
            order by {columns}, a.annee
            """
        rset = await fetch(req, type_territoire, code_territoire, *custom_params)

        # Define the column names for an empty DataFrame
        column_names = ["annee"] + list(set(categorie)) + ["valeur"]
        if not rset:
            return pd.DataFrame(columns=column_names)

        return pd.DataFrame([dict(x) for x in rset])

    schema = region.replace("-", "_")
    req = f"""
          select a.annee
          , a.{categorie}
          , sum(valeur) as valeur
          from {schema}.{jeu_de_donnees} as a
          join {schema}.territoire as b on b.commune = a.commune
          where b.type_territoire = $1
          and b.code = $2
          {custom_condition}
          group by a.annee, a.{categorie}
          order by a.{categorie}, a.annee
          """
    rset = await fetch(req, type_territoire, code_territoire, *custom_params)

    # Define the column names for an empty DataFrame
    column_names = ["annee", categorie, "valeur"]
    if not rset:
        return pd.DataFrame(columns=column_names)

    return pd.DataFrame([dict(x) for x in rset])


async def get_data_trajectory_with_categories(
    region, jeu_de_donnees, type_territoire, code_territoire, categorie
):
    """Retourne les données historiques d'une analyse par catégorie.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    analyse : str
        Nom de la table
    zone : str
        Zone géographique sélectionnée
    zone_id : str
        Identifiant de la zone géographique
    categorie : str
        Catégorie de l'analyse, e.g. secteur, energie, etc.

    Returns
    -------
    dict
    """
    schema = region.replace("-", "_")
    req = f"""
          select a.annee
          , c.modalite as nom
          , c.modalite_id as {categorie}
          , couleur
          , sum(valeur) as valeur
          from {schema}.{jeu_de_donnees} as a
          join {schema}.territoire as b on b.commune = a.commune
          join meta.categorie as c on a.{categorie} = c.modalite_id
          where b.type_territoire = $1
          and b.code = $2
          and c.nom = $3
          and c.region = $4
          group by a.annee, c.modalite, c.modalite_id, c.ordre, c.couleur
          order by c.ordre, a.annee
          """
    rset = await fetch(req, type_territoire, code_territoire, categorie, region)
    return [dict(x) for x in rset]


async def obtenir_annee_max_indicateur(region, donnees, data_ratio):
    condition_data_ratio = "and data_ratio is null"
    if data_ratio:
        condition_data_ratio = "and data_ratio is not null"
    req = f"""
          select years from meta.indicateur
          where region = $1
          and data = $2
          {condition_data_ratio}
          """
    res = await fetch(req, region, donnees)
    if res:
        return max([x["years"] for x in res][0])
    else:
        return


async def _mix_energetique_industriel_agriculture(
    region, analyse, zone, zone_id, categorie, id_secteur
):
    """
    Retourne le mix énergétique dans le secteur défini par l'argument id_secteur.
    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    analyse : str
        Nom de la table
    zone : str
        Zone géographique sélectionnée
    zone_id : str
        Identifiant de la zone géographique
    categorie : str
        Catégorie de l'analyse, e.g. secteur, energie, etc.
    id_secteur : int
        identifiant associé à un secteur (ici industriel ou agriculture)
    Returns
    -------
    dict
    """
    max_annee = await obtenir_annee_max_indicateur(region, analyse, False)
    schema = region.replace("-", "_")
    req = f"""
          select a.energie, e.modalite as nom, sum(a.valeur) as valeur
          from {schema}.{analyse} as a
          join meta.categorie u on u.modalite_id = a.usage
          join meta.categorie e on e.modalite_id = a.energie
          join {schema}.territoire t on t.commune = a.commune
          where a.{categorie} = $1
          and t.type_territoire = $2
          and t.code = $3
          and a.annee = $4
          and u.nom = 'usage'
          and e.nom = 'energie'
          and e.region = $5
          and u.region = $5
          group by energie, e.modalite;
          """

    rset = await fetch(req, id_secteur, zone, zone_id, max_annee, region)

    return [dict(x) for x in rset]


async def _retrieve_confidentiality_rules(region, zone, zone_id, analyse, category):
    """Retourne les éléments de confidentialité 'secteur' ou 'énergie' pour un territoire donné.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    zone : str
        Type de territoire
    zone_id : int
        Identifiant du territoire
    analyse : str
        Nom de l'analyse
    categorie : str
       'energie' ou 'secteur'

    Returns
    -------
    list
    """
    schema = region.replace("-", "_")
    # TODO: passage table here?
    assert category in ("secteur", "energie")
    other_category = "secteur" if category == "energie" else "energie"
    table = "confid_maille"
    sql = QueryBuilder(table, schema)
    sql.add_select("annee", table=table)
    sql.add_select(f"conditions->>'{category}' as {category}", table=table)
    sql.add_select(f"conditions->>'{other_category}' as {other_category}", table=table)
    sql.add_join(zone, on="{}.code = a.code")
    sql.add_select(table=zone, column="code")
    sql.add_filter(zone, "code", zone_id)
    sql.add_filter(table, "type_analyse", analyse)
    sql.add_filter(table, f"conditions->>'{other_category}'", "*")
    rset = await fetch(sql.query, *sql.params)
    if not rset:
        return []
    df = pd.DataFrame([dict(x) for x in rset]).drop_duplicates()
    return df[category].unique().tolist()


async def _get_confidentiality(
    region, zone, zone_id, passage_table, table, filtered_category
):
    """Retourne les éléments de confidentialité 'secteur' ou 'énergie' pour un territoire donné.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    zone : str
        Type de territoire
    zone_id : int
        Identifiant du territoire
    analyse : str
        Nom de l'analyse
    categorie : str
       'energie' ou 'secteur'

    Returns
    -------
    list
    """
    data_table_name = passage_table.convert_table_key(table)
    confid_table = "confid_maille"
    confid_columns = passage_table.get_columns(table, only_confid=True)
    reg_cat_key = passage_table.convert_column_key(filtered_category, table)
    if filtered_category == Sector:
        reg_cat_in_confid_table = "secteur"
    else:
        reg_cat_in_confid_table = "energie"

    schema = region.replace("-", "_")
    autres = set(f"conditions->>'{x}'" for x in confid_columns if x != reg_cat_key)

    sql = QueryBuilder(confid_table, schema)
    sql.add_select("annee", table=confid_table)
    sql.add_select(
        f"conditions->>'{reg_cat_in_confid_table}' as {reg_cat_in_confid_table}",
        table=confid_table,
    )
    for autre in autres:
        sql.add_select(autre, table=confid_table)
    sql.add_join(zone, on="{}.code = a.code")
    sql.add_select(table=zone, column="code")
    sql.add_filter(zone, "code", zone_id)
    sql.add_filter(confid_table, "type_analyse", data_table_name)
    for autre in autres:
        sql.add_filter(confid_table, autre, "*")
    rset = await fetch(sql.query, *sql.params)
    if not rset:
        return []
    df = pd.DataFrame([dict(x) for x in rset]).drop_duplicates()
    return df[reg_cat_in_confid_table].unique().tolist()


def _rendre_donnees_confidentielles(data, modalites, specific_cat=None):
    """Transforme la donnée pour la rendre confidentielle.

    Toutes les modalités dites confidentielles sont réunies et sommées par année.

    Parameters
    ----------
    data : pd.DataFrame
        Séries temporelles avec les différentes valeurs par modalités
    modalites : list
        Nom des modalités qui doivent être confidentielles

    Returns
    -------
    pd.DataFrame
    """
    # Si on a '*' dans les modalités, cela veut dire que ces données pour toutes les
    # modalités de la catégorie 'secteur' ou 'energie' sont confidentielles. On
    # renvoie un DataFrame vide.
    if "*" in modalites:
        return "Confidentiel"
    # TODO: do not use nom but PASSAGE_TABLE
    groupe = data[data["nom"].isin(modalites)]
    result = data.drop(index=groupe.index)
    agg_specific_cat = (
        ",".join(map(str, groupe[str(specific_cat)].unique())) if specific_cat else None
    )
    groupe = (
        groupe.groupby("annee")["valeur"]
        .sum()
        .reset_index()
        .assign(nom="Autres : {}".format(", ".join(modalites)))
        .assign(couleur=CONFIDENTIEL_COULEUR)
        .assign(confidentiel="oui")
    )
    if specific_cat is not None:
        groupe = groupe.assign(**{str(specific_cat): agg_specific_cat})
    return pd.concat([result, groupe], ignore_index=True, sort=False)


def _zero_annee_manquant_a_gauche(data, name_column: str | list = "nom"):
    """On met des zéros pour toutes les années manquantes à gauche (extrapolation backfill) pour toutes les modalités.

    Pour les données de suivi de trajectoire.
    """
    index = data["annee"].unique().tolist()
    index.sort()

    def inner(df):
        df_tmp = df.groupby("annee").first()
        df_tmp["valeur"] = df[["annee", "valeur"]].groupby("annee").sum()
        df = df_tmp
        df = df.reindex(pd.Index(index, name="annee"))
        if "couleur" in df.columns:
            df["couleur"] = df["couleur"].bfill()
            df["couleur"] = df["couleur"].ffill()
        if isinstance(name_column, (list, tuple)):
            for col in name_column:
                if col not in df.columns:
                    continue
                df[col] = df[col].bfill()
                df[col] = df[col].ffill()
        elif name_column in df.columns:
            df[name_column] = df[name_column].bfill()
            df[name_column] = df[name_column].ffill()
        df["confidentiel"] = "non"
        df["valeur"] = df["valeur"].fillna(0.0)
        df.reset_index(inplace=True)
        return df

    # pour garder le même ordre des noms de modalité. avec le groupby, pandas peut
    # changer l'ordre des groupes.
    grp = data.groupby(name_column)
    result = grp.apply(inner)
    result = result.sort_values(by="annee")
    if not isinstance(name_column, (list, tuple)):
        noms = data[name_column].unique().tolist()
        result["ordre"] = result[name_column].apply(lambda x: noms.index(x))
        result = result.sort_values(by=["ordre", "annee"]).drop(columns=["ordre"])
    return result.reset_index(drop=True)


def _ajouter_annee_future(
    data, max_annee, name_column: str | list = "nom", fill_missing_zero=False
):
    """On ajoute des données "à droite" en extrapolant dans le futur.

    On suppose que toutes les modalités ont bien les mêmes années. C'est une
    extrapolation en "marche d'escalier", i.e. forward-fill. On prend simplement la
    même valeur à n+1 que la valeur à n.
    """
    index = set(data["annee"].unique().tolist())
    future = range(int(data["annee"].max()) + 1, int(max_annee) + 1)
    index = list(index.union(future))
    index.sort()

    def inner(df):
        """
        This function is applied to all groups of data (one group per modalité).
        """
        df = df.groupby([c for c in df.columns if c != "valeur"]).sum().reset_index()
        df = df.set_index("annee")
        df = df.reindex(pd.Index(index, name="annee"))
        if "couleur" in df.columns:
            df["couleur"] = df["couleur"].bfill()
            df["couleur"] = df["couleur"].ffill()
        if isinstance(name_column, (list, tuple)):
            for col in name_column:
                if col not in df.columns:
                    continue
                df[col] = df[col].bfill()
                df[col] = df[col].ffill()
        elif name_column in df.columns:
            df[name_column] = df[name_column].bfill()
            df[name_column] = df[name_column].ffill()
        df["confidentiel"] = "non"
        if fill_missing_zero:
            df["valeur"] = df["valeur"].fillna(0.0)
        else:
            df["valeur"] = df["valeur"].ffill()
        df.reset_index(inplace=True)
        return df

    # pour garder le même ordre des noms de modalité. avec le groupby, pandas peut
    # changer l'ordre des groupes.
    grp = data.groupby(name_column)
    result = grp.apply(inner)
    result = result.sort_values(by="annee")
    if not isinstance(name_column, (set, list, tuple)):
        noms = data[name_column].unique().tolist()
        result["ordre"] = result[name_column].apply(lambda x: noms.index(x))
        result = result.sort_values(by=["ordre", "annee"]).drop(columns=["ordre"])
    return result.reset_index(drop=True)


def _graphe_format(df):
    """formatte la données DataFrame pour le graphe stacked plot

    {"nom": "Nom de la modalité, e.g. Résidentiel,
     "couleur": "Couleur de la modalité"
     "data": [{"annee": 2010, "valeur": 500},
              {"annee": 2011, "valeur": 550}]
    }
    """
    if not isinstance(df, pd.DataFrame):
        return df
    if df.empty:
        return []
    df = df.copy()
    result = []
    iter_group = df.groupby(["nom", "couleur", "confidentiel"], sort=False)
    for (nom, couleur, confidentiel), group in iter_group:
        result.append(
            {
                "nom": nom,
                "couleur": couleur,
                "confidentiel": confidentiel,
                "data": [
                    {"annee": x.annee, "valeur": x.valeur} for _, x in group.iterrows()
                ],
            }
        )
    return result


async def _historique_et_impact_avec_confidentialite(
    region,
    zone,
    zone_id,
    reference_year,
    final_year,
    resultat_impact,
    table,
    passage_table,
    is_national,
    remove_passage_data: bool = True,
):
    """Données historiques + résultats impact actions avec application de la confidentialité

    pour un territoire donné.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    zone : str
        Type de territoire
    zone : int
        Identifiant du territoire
    analyse : str
        Nom de l'analyse: 'conso_energetique' ou 'emission_ges'
    resultat_impact : dict

    Returns
    -------
    dict
        Dictionnaire de DataFrame
    """
    table_name = passage_table.convert_table_key(table)
    sector_column = passage_table.convert_column_key(Sector, table)
    commodity_column = passage_table.convert_column_key(Commodity, table)
    custom_filters = passage_table.get_custom_real_conditions(table)

    # TODO: optimize? data already retrieved?
    history_sector, history_commodity = await asyncio.gather(
        _suivi_trajectoire_analyze(
            region, table_name, zone, zone_id, sector_column, custom_filters
        ),
        _suivi_trajectoire_analyze(
            region, table_name, zone, zone_id, commodity_column, custom_filters
        ),
    )

    # Cut out history after the reference year
    if not history_sector.empty:
        history_sector = history_sector[history_sector["annee"] <= reference_year]
    if not history_commodity.empty:
        history_commodity = history_commodity[
            history_commodity["annee"] <= reference_year
        ]

    # resultat_impact -> énergie économisée par les actions
    results = resultat_impact.get(Sector)
    impact_sector = pd.DataFrame(results).fillna(0.0)
    results = resultat_impact.get(Commodity)
    impact_commodity = pd.DataFrame(results).fillna(0.0)

    metadata_commodity, metadata_sector = await asyncio.gather(
        categories_metadata(region, commodity_column),
        categories_metadata(region, sector_column),
    )
    reverted_impact_sector = passage_table.revert_impacts(table, impact_sector)
    reverted_impact_commodity = passage_table.revert_impacts(table, impact_commodity)

    logger.info(f"Merge history and future for {table_name} based on sector")
    result_sector = fusion_historique_et_future(
        history_sector,
        reverted_impact_sector,
        metadata_sector,
        sector_column,
        final_year,
        soustraire=True,
    )
    logger.info(f"Merge history and future for {table_name} based on commodity")
    result_commodity = fusion_historique_et_future(
        history_commodity,
        reverted_impact_commodity,
        metadata_commodity,
        commodity_column,
        final_year,
        soustraire=True,
    )

    # On corrige si nécessaire la conso par type d'énergie.

    # on rend les données confidentielles si besoin
    confid_sector, confid_type = None, None
    if not is_national:
        confid_sector, confid_type = await asyncio.gather(
            _get_confidentiality(region, zone, zone_id, passage_table, table, Sector),
            _get_confidentiality(
                region, zone, zone_id, passage_table, table, Commodity
            ),
        )

    # if (
    #     len(result_commodity) == 2
    # ):  # S'il s'agit d'un tuple de deux dataframe, on prend celui par type d'énergie
    #     result_commodity = result_commodity[0]

    if str(Commodity) in result_commodity.columns:
        result_commodity = result_commodity.drop(columns=[str(Commodity)])
    if str(Sector) in result_sector.columns:
        result_sector = result_sector.drop(columns=[str(Sector)])

    if not remove_passage_data and not result_commodity.empty:
        # we need to convert the relevant categories columns to int as strings
        # because passage table are using strings
        result_commodity[commodity_column] = (
            result_commodity[commodity_column].astype(int).astype(str)
        )
        result_commodity = passage_table.convert_data(table, result_commodity)

    if not remove_passage_data and not result_sector.empty:
        # we need to convert the relevant categories columns to int as strings
        # because passage table are using strings
        result_sector[sector_column] = (
            result_sector[sector_column].astype(int).astype(str)
        )
        result_sector = passage_table.convert_data(table, result_sector)

    if confid_sector:
        result_sector = _rendre_donnees_confidentielles(
            result_sector, confid_sector, sector_column
        )
    if confid_type:
        result_commodity = _rendre_donnees_confidentielles(
            result_commodity, confid_type, commodity_column
        )

    # HACK np.int64 is not serializable in json or ujson
    # can get OverflowError: Maximum recursion level reached
    # see https://github.com/esnme/ultrajson/issues/221
    max_year = None
    if not history_sector.empty:
        max_year = int(history_sector["annee"].max())
    return {
        "secteur": result_sector,
        "energie": result_commodity,
        "max_annee": max_year,
        "is_empty": isinstance(resultat_impact, Impacts.Empty),
    }


def historique_et_impact_reduction_conso(
    region,
    zone,
    zone_id,
    reference_year,
    final_year,
    resultat_impact,
    passage_table,
    is_national,
    remove_passage_data: bool = True,
):
    """Données historique de conso + impact pour actions réductions de conso. Retourne une coroutine"""
    return _historique_et_impact_avec_confidentialite(
        region,
        zone,
        zone_id,
        reference_year,
        final_year,
        resultat_impact,
        DataSet.CONSUMPTION,
        passage_table,
        is_national,
        remove_passage_data,
    )


def historique_et_impact_emission_ges(
    region,
    zone,
    zone_id,
    reference_year,
    final_year,
    resultat_impact,
    passage_table,
    is_national,
    remove_passage_data: bool = True,
):
    """Données historique d'émissions GES + impact pour actions réductions de conso. Retourne une coroutine"""
    return _historique_et_impact_avec_confidentialite(
        region,
        zone,
        zone_id,
        reference_year,
        final_year,
        resultat_impact,
        DataSet.EMISSIONS,
        passage_table,
        is_national,
        remove_passage_data,
    )


def historique_et_impact_polluants_atmo(
    region,
    zone,
    zone_id,
    reference_year,
    final_year,
    resultat_impact,
    pollutant,
    passage_table,
    is_national,
    remove_passage_data: bool = True,
):
    """Données historique d'émissions des polluants atmosphériques + impact pour actions réductions de conso. Retourne une coroutine"""
    return _historique_et_impact_avec_confidentialite(
        region,
        zone,
        zone_id,
        reference_year,
        final_year,
        resultat_impact,
        pollutant,
        passage_table,
        is_national,
        remove_passage_data,
    )


def fusion_historique_et_future(
    history, future, metadata, by_column, final_year, soustraire=True
):
    """Fusionne les séries temporelles historiques avec la réduction future.

    On extrapole à droite la séries temporelles issues des données historiques
    jusqu'à l'année max fournie dans `future`. On déduit ces valeurs avec les valeurs
    calculées de réduction de conso d'énergie (resp. émission GES).

    Parameters
    ----------
    history : pd.DataFrame
    future : pd.DataFrame
        Résultats issus des actions (index : modalité, columns : annee)

    Returns
    -------
    pd.DataFrame
    """
    # TODO: fix?
    by_column = str(by_column)
    by_column_year = [by_column, "annee"]
    result = _zero_annee_manquant_a_gauche(history, by_column)

    # if we have no trajectory computed
    if future.empty:
        # we still interpolate until last year
        result = _ajouter_annee_future(result, final_year, by_column)

        result = pd.merge(
            result,
            metadata,
            how="inner",
            left_on=by_column,
            right_on=by_column,
        )
        result = result.sort_values(by=["ordre", "annee"])
        return result.drop(columns=["ordre"])

    # we fill missing years
    if not result.empty:
        result = _ajouter_annee_future(result, final_year, by_column)

    # we convert future trajectory to have right structure
    future = future.copy()
    future["annee"] = future["annee"].astype(np.int64)
    # we also add missing years to future values (i.e. years that had no impact)
    future = _ajouter_annee_future(
        future, final_year, by_column, fill_missing_zero=True
    )
    # we need a cumulative sum for each sector. Easier to substract/add to actual
    # extrapolated values.
    future["economisee"] = future.groupby([by_column])["valeur"].cumsum()
    future = future.drop(columns=["valeur", "confidentiel"])

    result[by_column] = result[by_column].astype(int)
    future = future.loc[future[by_column] != ""]
    future[by_column] = future[by_column].astype(int)

    result = pd.merge(
        result,
        future,
        how="outer",
        left_on=by_column_year,
        right_on=by_column_year,
    )
    # On obtient toutes les lignes des dataframe. Si la correspondance entre les colonnes ne se fait pas,
    # les valeur sont égales à NaN
    result = _zero_annee_manquant_a_gauche(result, by_column)
    result["confidentiel"] = result["confidentiel"].fillna("non")
    result["economisee"] = result["economisee"].fillna(0.0)
    result["valeur"] = result["valeur"].fillna(0.0)

    # we merge with metadata to have access to order, color, real name
    result = pd.merge(
        result,
        metadata,
        how="inner",
        left_on=by_column,
        right_on=by_column,
    )
    # dans le cas de la conso d'énergie ou des émissions GES, on soustrait
    # l'extrapolation issue des données historiques avec les résultats issues des
    # impacts d'actions de type réduction de conso d'énergie. Dans le cas de la
    # production d'énergie renouvelable, on ajoute l'historique avec les futures
    # productions d'énergie.
    facteur = -1 if soustraire else 1
    result["valeur"] = result["valeur"] + facteur * result["economisee"]
    # comme on peut avoir une valeur négative, on cape
    result["valeur"] = result["valeur"].clip(lower=0.0)

    # we sort according to metadata
    result = result.sort_values(by=["ordre", "annee"])
    return result.drop(columns=["ordre", "economisee"])


def fusion_historique_et_future_prod(
    history,
    future,
    metadata_prod,
    metadata_vector,
    by_column_prod,
    by_column_vector,
    final_year,
    soustraire=True,
):
    """Fusionne les séries temporelles historiques avec la réduction future.

    On extrapole à droite la séries temporelles issues des données historiques
    jusqu'à l'année max fournie dans `future`. On déduit ces valeurs avec les valeurs
    calculées de réduction de conso d'énergie (resp. émission GES).

    Parameters
    ----------
    history : pd.DataFrame
    future : pd.DataFrame
        Résultats issus des actions (index : modalité, columns : annee)

    Returns
    -------
    pd.DataFrame
    """
    # TODO: fix?
    by_columns = list(set((by_column_prod, by_column_vector)))
    result = pd.DataFrame(columns=["annee", "valeur", by_column_vector])
    if not history.empty:
        result = _zero_annee_manquant_a_gauche(
            history, [by_column_prod, by_column_vector]
        )

    # if we have the other column in meta data (ie. the same column from the raw data
    # serves both for prod and vector), we drop it
    if by_column_prod != by_column_vector:
        if by_column_prod in metadata_vector.columns:
            metadata_vector = metadata_vector.drop(columns=[by_column_prod])
        if by_column_vector in metadata_prod.columns:
            metadata_prod = metadata_prod.drop(columns=[by_column_vector])

    # we fill missing years
    if not history.empty:
        result = _ajouter_annee_future(result, final_year, by_columns)

    result = pd.merge(
        result,
        metadata_vector,
        how="inner",
        left_on=by_column_vector,
        right_on=by_column_vector,
    )

    result = pd.merge(
        result,
        metadata_prod,
        how="inner",
        left_on=by_column_prod,
        right_on=by_column_prod,
        suffixes=["_vector", "_prod"],
    )
    result[by_column_prod] = result[by_column_prod].astype(int)
    result[by_column_vector] = result[by_column_vector].astype(int)

    # if we have no trajectory computed
    if future.empty:
        result = result.sort_values(by=["ordre_prod", "ordre_vector", "annee"])
        return result.drop(columns=["ordre_prod", "ordre_vector"])

    # we convert future trajectory to have right structure
    future = future.copy()
    # we also add missing years to future values (i.e. years that had no impact)
    future["annee"] = future["annee"].astype(np.int64)
    # we fill na with 0 to make sure the cumsum following will not repeatly pile up
    # punctual investments
    future["valeur"] = future["valeur"].fillna(0.0)
    future = _ajouter_annee_future(future, final_year, by_columns)
    # we need a cumulative sum for each sector. Easier to substract/add to actual
    # extrapolated values.
    future["economisee"] = future.groupby(by_columns)["valeur"].cumsum()
    future = future.drop(columns=["valeur", "confidentiel"])

    future[by_column_prod] = future[by_column_prod].astype(int)
    future[by_column_vector] = future[by_column_vector].astype(int)
    future = pd.merge(
        future,
        metadata_vector,
        how="inner",
        left_on=by_column_vector,
        right_on=by_column_vector,
    )

    future = pd.merge(
        future,
        metadata_prod,
        how="inner",
        left_on=by_column_prod,
        right_on=by_column_prod,
        suffixes=["_vector", "_prod"],
    )

    by_column_year = by_columns + [
        "nom_vector",
        "couleur_vector",
        "ordre_vector",
        "nom_prod",
        "couleur_prod",
        "ordre_prod",
        "annee",
    ]
    result = pd.merge(
        result, future, how="outer", left_on=by_column_year, right_on=by_column_year
    )

    # On obtient toutes les lignes des dataframe. Si la correspondance entre les colonnes ne se fait pas,
    # les valeur sont égales à NaN
    result = _zero_annee_manquant_a_gauche(result, by_columns)
    result["confidentiel"] = result["confidentiel"].fillna("non")
    result["economisee"] = result["economisee"].fillna(0.0)
    result["valeur"] = result["valeur"].fillna(0.0)

    # dans le cas de la conso d'énergie ou des émissions GES, on soustrait
    # l'extrapolation issue des données historiques avec les résultats issues des
    # impacts d'actions de type réduction de conso d'énergie. Dans le cas de la
    # production d'énergie renouvelable, on ajoute l'historique avec les futures
    # productions d'énergie.
    facteur = -1 if soustraire else 1
    result["valeur"] = result["valeur"] + facteur * result["economisee"]
    # comme on peut avoir une valeur négative, on cape
    result["valeur"] = result["valeur"].clip(lower=0.0)

    # we sort according to metadata
    result = result.sort_values(by=["ordre_prod", "ordre_vector", "annee"])
    return result.drop(columns=["ordre_prod", "ordre_vector", "economisee"])


async def suivi_trajectoire(region, zone, zone_id):
    """Données pour la page de suivi de trajectoire

    i.e. les données historiques de consommation & production d'énergie renouvelable
    pour un territoire donné.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    zone : str
        Zone géographique sélectionnée
    zone_id : str
        Identifiant de la zone géographique

    Returns
    -------
    dict
        - Données de barplot conso vs. production énergie
        - Données pour stacked plot de conso & production énergie renouvelable par catégories
        - chiffres-clé pour le territoire sélectionné
    """
    passage_table = PassageTable(region)
    await passage_table.load((DataSet.PRODUCTION, DataSet.CONSUMPTION))
    # TODO: use DataLoader here?
    # data_loader = DataLoader(passage_table)
    # require defining DataRequisite or equivalent?

    energy_prod_table = passage_table.convert_table_key(DataSet.PRODUCTION)
    enr_col = passage_table.convert_column_key(RenewableProd, DataSet.PRODUCTION)

    energy_table = passage_table.convert_table_key(DataSet.CONSUMPTION)
    sector_col = passage_table.convert_column_key(Sector, DataSet.CONSUMPTION)
    commodity_col = passage_table.convert_column_key(Commodity, DataSet.CONSUMPTION)
    usage_col = passage_table.convert_column_key(Usage, DataSet.CONSUMPTION)

    conso_secteur, conso_energie, conso_usage, production = await asyncio.gather(
        get_data_trajectory_with_categories(
            region, energy_table, zone, zone_id, sector_col
        ),
        get_data_trajectory_with_categories(
            region, energy_table, zone, zone_id, commodity_col
        ),
        get_data_trajectory_with_categories(
            region, energy_table, zone, zone_id, usage_col
        ),
        get_data_trajectory_with_categories(
            region, energy_prod_table, zone, zone_id, enr_col
        ),
    )

    conso_secteur = pd.DataFrame(conso_secteur)
    conso_energie = pd.DataFrame(conso_energie)
    conso_usage = pd.DataFrame(conso_usage)
    production = pd.DataFrame(production)

    # on somme par année pour le barplot consommation & production par an (quelque soit la catégorie)
    conso_annee = (
        conso_secteur.groupby("annee")["valeur"]
        .sum()
        .to_frame()
        .rename(columns={"valeur": "consommation"})
    )
    production_annee = (
        production.groupby("annee")["valeur"]
        .sum()
        .to_frame()
        .rename(columns={"valeur": "production"})
    )
    # on garde les années qui sont présentes dans les deux datasets
    barplot = production_annee.merge(
        conso_annee, how="left", left_index=True, right_index=True
    ).reset_index()
    barplot["annee"] = barplot["annee"].astype(int)
    barplot["consommation"] = barplot["consommation"].round(2)
    barplot["production"] = (barplot["production"]).round(2)

    conso_secteur = _zero_annee_manquant_a_gauche(conso_secteur)
    conso_energie = _zero_annee_manquant_a_gauche(conso_energie)
    conso_usage = _zero_annee_manquant_a_gauche(conso_usage)
    production = _zero_annee_manquant_a_gauche(production)

    # Gérer la confidentialité des données (conso)
    secteur_confidentiel, energie_confidentiel = await asyncio.gather(
        _retrieve_confidentiality_rules(region, zone, zone_id, energy_table, "secteur"),
        _retrieve_confidentiality_rules(region, zone, zone_id, energy_table, "energie"),
    )
    conso_secteur = _rendre_donnees_confidentielles(conso_secteur, secteur_confidentiel)
    conso_energie = _rendre_donnees_confidentielles(conso_energie, energie_confidentiel)
    # si l'un des deux DataFrame est vide, les données sont toutes confidentielles et
    # on ne veut pas afficher le total de la conso énergétique dans le barplot.
    if (
        (
            not isinstance(conso_secteur, pd.DataFrame)
            and conso_secteur == "Confidentiel"
        )
        or (
            not isinstance(conso_energie, pd.DataFrame)
            and conso_energie == "Confidentiel"
        )
        or conso_secteur.empty
        or conso_energie.empty
    ):
        barplot["consommation"] = 0.0
    barplot = barplot.dropna()
    return {
        "conso_energetique": {
            "secteur": _graphe_format(conso_secteur),
            "energie": _graphe_format(conso_energie),
            "usage": _graphe_format(conso_usage),
        },
        "prod_enr": {
            "type_prod_enr": _graphe_format(production),
        },
        "barplot": barplot.to_dict("records"),
    }


async def check_year_in_mobility_table(region, zone, zone_id, year, buffer=0):
    schema = region.replace("-", "_")
    req = f"""
        SELECT DISTINCT annee FROM {schema}.mobilite_insee as a
                JOIN {schema}.territoire t on t.commune = a.commune
                AND t.type_territoire = $1
                AND t.code = $2
                where a.trans_ppal = 2 and a.valeur_filtre >= $3;
    """
    rset = await fetch(req, zone, zone_id, buffer)
    years = [x["annee"] for x in rset]
    return year if year in years else max(years)


async def nombre_actifs_en_voiture(region, zone, zone_id, year=None, buffer=0):
    """Renvoie le nombre d'actif en voiture.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    zone : str
        Zone géographique sélectionnée
    zone_id : str
        Identifiant de la zone géographique
    buffer : int
        Détermine le nombre d'actif dans un rayon donnée en km

    Returns
    -------
    float
    """
    schema = region.replace("-", "_")

    year = await check_year_in_mobility_table(region, zone, zone_id, year, buffer)
    req = f"""
        SELECT SUM(a.valeur) as valeur, a.annee
            FROM {schema}.mobilite_insee as a
            JOIN {schema}.territoire t on t.commune = a.commune
            AND t.type_territoire = $1
            AND t.code = $2
            AND a.annee = $3
            WHERE a.trans_ppal = 2 AND a.valeur_filtre >= $4
            GROUP BY a.annee;
    """
    rset = await fetch(req, zone, zone_id, year, buffer)
    nombre_actifs_en_voiture = [dict(x) for x in rset]

    return nombre_actifs_en_voiture[0]["annee"], nombre_actifs_en_voiture[0]["valeur"]


async def distance_moyenne_par_actif_en_voiture(
    region, zone, zone_id, year=None, buffer=0
):
    """Renvoie la distance moyenne parcouru par les actifs en voiture dans le cadre de leur trajet domicile-travail.

    Parameters
    ----------
    region : str
        Nom de la région (schéma en base)
    zone : str
        Zone géographique sélectionnée
    zone_id : str
        Identifiant de la zone géographique
    buffer : int
        Détermine le nombre d'actif dans un rayon donnée en km

    Returns
    -------
    float
    """
    schema = region.replace("-", "_")
    year = await check_year_in_mobility_table(region, zone, zone_id, year, buffer)
    req = f"""
        select (
            (
                select SUM(a.valeur_filtre * a.valeur) as valeur
                from {schema}.mobilite_insee as a
                join {schema}.territoire t on t.commune = a.commune
                and t.type_territoire = $1
                and t.code = $2
                and a.annee = $3
                where a.trans_ppal = 2 and a.valeur_filtre >= $4
            ) /
            (
                select SUM(a.valeur) as valeur
                from {schema}.mobilite_insee as a
                join {schema}.territoire t on t.commune = a.commune
                and t.type_territoire = $1
                and t.code = $2
                and a.annee = $3
                where a.trans_ppal = 2 and a.valeur_filtre >= $4
            )
        ) as distance_moyenne_par_actif
        """

    rset = await fetch(req, zone, zone_id, year, buffer)
    distance_moyenne_par_actif_en_voiture = [dict(x) for x in rset]

    return (
        year,
        distance_moyenne_par_actif_en_voiture[0]["distance_moyenne_par_actif"],
    )


async def get_page_parameters(region, env, page):
    """Get the page graph types enabled.

    Parameters
    ----------
    region : str
        Region name (e.g., auvergne-rhone-alpes)
    env : str
        Environment considered (e.g., dev)
    page : str
        Page name (e.g., suivi_energetique)

    Returns
    -------
    dict
        for each graph type, a boolean indicating whether the graph is enabled.
    """
    req = f"""
        SELECT graph_type, status FROM meta.analysis_page_parameters
        WHERE region_id = $1 AND env_name = $2 AND page_name = $3
    """
    rset = await fetch(req, region, env, page)
    return {x["graph_type"]: x["status"] for x in rset}


async def get_pages_enabled(env, region=None):
    """Get the analyses enabled pages list

    Parameters
    ----------
    region : str
        Region name (e.g., auvergne-rhone-alpes)
    env : str
        Environment considered (e.g., dev)

    Returns
    -------
    list
        pages names
    """
    if region is None:
        req = f"""
            SELECT region_id, ARRAY_AGG(DISTINCT page_name) as pages FROM meta.analysis_page_parameters
            WHERE env_name = $1 AND status = true
            GROUP BY region_id
        """
        rset = await fetch(req, env)
        return {x["region_id"]: x["pages"] for x in rset}
    else:
        req = f"""
            SELECT DISTINCT page_name FROM meta.analysis_page_parameters
            WHERE (region_id = $1 OR $1 = '*') AND env_name = $2 AND status = true
        """
        rset = await fetch(req, region, env)
        return {region: [x["page_name"] for x in rset]}


async def historique_et_impact_prod(
    region: str,
    zone: str,
    zone_id: str,
    reference_year: int,
    final_year: int,
    resultat_impact: Impacts,
    passage_table: PassageTable,
    remove_passage_data: bool = True,
):
    prod_enr_table = passage_table.convert_table_key(DataSet.PRODUCTION)
    prod_enr_column_prod = passage_table.convert_column_key(
        RenewableProd, DataSet.PRODUCTION
    )
    prod_enr_column_vector = passage_table.convert_column_key(
        EnergyVector, DataSet.PRODUCTION
    )
    custom_filters = passage_table.get_custom_real_conditions(DataSet.PRODUCTION)

    history_ren_prod = await _suivi_trajectoire_analyze(
        region,
        prod_enr_table,
        zone,
        zone_id,
        [prod_enr_column_prod, prod_enr_column_vector],
        custom_filters,
    )
    history_ren_prod = pd.DataFrame(history_ren_prod)

    metadata_ren_prod_prod = await categories_metadata(region, prod_enr_column_prod)
    metadata_ren_prod_vector = await categories_metadata(region, prod_enr_column_vector)

    logger.info(f"Merge history and future for prod_enr based on type_prod")

    # if we don't have any impacts on prod (e.g., action on consumption reduction)
    impact_prod = pd.DataFrame(
        resultat_impact.get([RenewableProd, EnergyVector])
    ).fillna(0.0)
    reverted_impact_prod = passage_table.revert_impacts(DataSet.PRODUCTION, impact_prod)
    if not reverted_impact_prod.empty:
        reverted_impact_prod = reverted_impact_prod.drop(
            columns=[str(RenewableProd), str(EnergyVector)]
        )
    if not history_ren_prod.empty:
        history_ren_prod.loc[:, "valeur"] = history_ren_prod.loc[:, "valeur"].fillna(
            0.0
        )

    # Cut out history after the reference year
    if not history_ren_prod.empty:
        history_ren_prod = history_ren_prod[history_ren_prod["annee"] <= reference_year]

    prod_enr = fusion_historique_et_future_prod(
        history_ren_prod,
        reverted_impact_prod,
        metadata_ren_prod_prod,
        metadata_ren_prod_vector,
        prod_enr_column_prod,
        prod_enr_column_vector,
        final_year,
        soustraire=False,
    )
    columns_prod = [
        "annee",
        prod_enr_column_prod,
        "confidentiel",
        "nom_prod",
        "couleur_prod",
    ]
    columns_vector = [
        "annee",
        prod_enr_column_vector,
        "confidentiel",
        "nom_vector",
        "couleur_vector",
    ]

    if not remove_passage_data and not prod_enr.empty:
        # we need to convert the relevant categories columns to int as strings
        # because passage table are using strings
        prod_enr[prod_enr_column_prod] = (
            prod_enr[prod_enr_column_prod].astype(int).astype(str)
        )
        prod_enr[prod_enr_column_vector] = (
            prod_enr[prod_enr_column_vector].astype(int).astype(str)
        )
        prod_enr = passage_table.convert_data(DataSet.PRODUCTION, prod_enr)
        # we add two new columns to keep passage keys in outputs
        columns_prod += [str(RenewableProd)]
        columns_vector += [str(EnergyVector)]

    # we have to group by + sum one last time to get only detail for vector or prod mode
    # Check if history_ren_prod is empty or if 'annee' column is empty
    if history_ren_prod.empty or history_ren_prod["annee"].dropna().empty:
        if reverted_impact_prod.empty:
            max_annee = datetime.now().year
        else:
            max_annee = int(reverted_impact_prod["annee"].min()) - 1
    else:
        max_annee = int(history_ren_prod["annee"].max())
    output = {
        "max_annee": max_annee,
    }
    if prod_enr.empty:
        output["energie"] = pd.DataFrame()
    else:
        output["energie"] = (
            prod_enr[columns_prod + ["valeur"]]
            .groupby(columns_prod)
            .sum()
            .reset_index()
            .rename(columns={"nom_prod": "nom", "couleur_prod": "couleur"})
        )
    if prod_enr_column_prod != prod_enr_column_vector:
        return {
            **output,
            "vecteur": prod_enr[columns_vector + ["valeur"]]
            .groupby(columns_vector)
            .sum()
            .reset_index()
            .rename(columns={"nom_vector": "nom", "couleur_vector": "couleur"}),
        }
    else:
        return output
