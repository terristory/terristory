﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
import urllib.request

import pandas as pd
from sanic.log import logger

from terriapi.controller import fetch
from terriapi.integration.validation import normalize


def obtenir_donnees_disponibles():
    """Permet d'obtenir la liste des jeux de données disponibles sur la plateforme ODRé"""
    url = "https://opendata.reseaux-energies.fr/api/datasets/1.0/search/?disjunctive.theme=true&disjunctive.publisher=true&disjunctive.maille-geographique=true&disjunctive.frequence-de-mise-a-jour=true&disjunctive.pas-temporel=true&sort=explore.popularity_score&rows=200&start=10&extrametas=true&interopmetas=true&timezone=Europe%2FBerlin&lang=fr"
    json_chaine_caractere = "{}"
    try:
        with urllib.request.urlopen(url) as data:
            json_chaine_caractere = data.read().decode("utf-8")
    except BaseException as e:
        logger.warning(
            "L'appel de l'API du registre RTE n'a pas fonctionné. Erreur : " + str(e)
        )
        return {}
    liste_donnees = [x for x in json.loads(json_chaine_caractere)["datasets"]]
    json.loads(json_chaine_caractere)
    return [
        {
            "nom": x["metas"]["title"],
            "table": normalize(x["datasetid"]),
            "jeu_de_donnees": x["datasetid"],
            "annee": int(x["metas"]["data_processed"][0:4]),
        }
        for x in liste_donnees
    ]


class DonneesRegistre:
    """Classe dont les méthodes permettent d'obtenir les données du jeu de données du registre que sélectionne l'administrateur en région.

    Paramètres
    ----------

    url: Chaîne de caractères
        Url d'obtention du jeu de données
    _donnees: DataFrame pandas
        Jeu de données obtenus (attribut modifié par une méthode de la classe)
    """

    def __init__(self, url):
        self.url = url
        self._donnees = pd.DataFrame()
        self._liste_donnees = []

    def obtenir_donnees(self):
        """Modifie l'attribut _donnees à partir des données que la méthode permet de télécharger."""
        donnees_chaine_caracteres = ""
        try:
            with urllib.request.urlopen(self.url) as data:
                donnees_chaine_caracteres = data.read().decode("utf-8")
        except BaseException as e:
            logger.warning(
                "L'appel de l'API du registre RTE n'a pas fonctionné. Erreur : "
                + str(e)
            )
            return None
        liste_finale = []
        lignes = donnees_chaine_caracteres.split("\n")
        for i in range(len(lignes[1:])):
            l = lignes[i].split(";")
            d = {}
            cols = lignes[0].split(";")
            for x in range(len(l)):
                d[cols[x]] = l[x]
            liste_finale.append(d)
        self._donnees = pd.DataFrame([x for x in liste_finale][1:])

    @property
    def liste_colonnes(self):
        """Retourne la liste des colonnes"""
        return list(self.donnees.keys())

    @property
    def liste_intitules(self):
        """Retourne pour chaque colonne, la liste des valeurs qu'elle contient (sans doublon)."""
        d = {}
        for i in self.liste_colonnes:
            d[i] = list(self.donnees[i].unique())
        return d

    @property
    def donnees(self):
        """Retourne les données accessibles depuis l'attribut _donnees"""
        return self._donnees

    async def obtenir_liste_donnees_registre_deja_configurees(self):
        """Retourne la liste des données du registre déjà configurées

        Retourne
        --------

        Liste
        """

        sql = """
              select nom_table
              from meta.perimetre_geographique
              where donnees_registre = true;
              """
        res = await fetch(sql)
        self._liste_donnees = [x["nom_table"] for x in res]
        return [x["nom_table"] for x in res]

    @property
    def liste_donnees(self):
        """Accès à l'attribut privé _liste_donnees"""
        return self._liste_donnees
