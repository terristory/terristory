﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Fonctions pour l'insertion automatisée des PCAET disponibles sur le site de l'Ademe."""
from __future__ import annotations

import json
import os
import re

import numpy as np
import pandas as pd
import requests
from sanic.log import logger
from sqlalchemy import text

from terriapi import settings
from terriapi.controller import (
    execute,
    executemany,
    fetch,
    get_pg_engine,
    regions_configuration,
)
from terriapi.controller.analyse import (
    territory_value_by_category,
    total_territory_value,
    total_value_by_mesh,
    value_by_mesh_and_category,
)
from terriapi.controller.strategy_actions.constants import (
    ColumnPassageType,
    DataSet,
    RenewableProd,
    Sector,
)
from terriapi.controller.strategy_actions.passage_table import PassageTable

PCAET_TABLE = "pcaet"
PCAET_META_TABLE = "meta.pcaet_header"

TRAJECTORY_KEYS = [
    DataSet.EMISSIONS,
    DataSet.CONSUMPTION,
    DataSet.PRODUCTION,
    DataSet.POLLUTANT_PM10,
    DataSet.POLLUTANT_PM25,
    DataSet.POLLUTANT_NOX,
    DataSet.POLLUTANT_SOX,
    DataSet.POLLUTANT_COVNM,
    DataSet.POLLUTANT_NH3,
]


def pcaet_trajectories_meta(enable_air_pollutants_impacts: bool = True):
    pcaet_meta = [
        {
            "id": "consommation_ener",
            "table": DataSet.CONSUMPTION,
            "column": Sector,
            "name": "Consommation d'énergie",
            "unit": "GWh",
            "category_title": "Par secteur",
        },
        {
            "id": "enr_production",
            "table": DataSet.PRODUCTION,
            "column": RenewableProd,
            "name": "Production d'énergie renouvelable",
            "unit": "GWh",
            "category_title": "Par type d'énergie",
        },
        {
            "id": "emission_ges",
            "table": DataSet.EMISSIONS,
            "column": Sector,
            "name": "Émissions de GES",
            "unit": "kt éq.CO₂",
            "category_title": "Par secteur",
        },
    ]
    if enable_air_pollutants_impacts:
        pcaet_meta += [
            {
                "id": "covnm",
                "table": DataSet.POLLUTANT_COVNM,
                "column": Sector,
                "name": "Émissions de COVNM",
                "unit": "t",
                "category_title": "Par secteur",
            },
            {
                "id": "nh3",
                "table": DataSet.POLLUTANT_NH3,
                "column": Sector,
                "name": "Émissions de NH₃",
                "unit": "t",
                "category_title": "Par secteur",
            },
            {
                "id": "nox",
                "table": DataSet.POLLUTANT_NOX,
                "column": Sector,
                "name": "Émissions de NOₓ",
                "unit": "t",
                "category_title": "Par secteur",
            },
            {
                "id": "pm10",
                "table": DataSet.POLLUTANT_PM10,
                "column": Sector,
                "name": "Émissions de PM10",
                "unit": "t",
                "category_title": "Par secteur",
            },
            {
                "id": "pm25",
                "table": DataSet.POLLUTANT_PM25,
                "column": Sector,
                "name": "Émissions de PM2,5",
                "unit": "t",
                "category_title": "Par secteur",
            },
            {
                "id": "sox",
                "table": DataSet.POLLUTANT_SOX,
                "column": Sector,
                "name": "Émissions de SOₓ",
                "unit": "t",
                "category_title": "Par secteur",
            },
        ]
    return pcaet_meta


def download_updated_pcaet(type_fichier: str, region: str):
    """
    Cette fonction récupère les dernières versions de PCAET sur le site de l'Ademe:
    https://www.territoires-climat.ademe.fr/opendata
    Et les stocke dans des fichiers locaux

    Parameters
     ----------
    type_fichier: str
        Type de pcaet à récupérer. Peut être "pec_seq", "enr", "polluants"...
    region : str
        Region transmise lors de l'appel de la fonction MAJ PCAET par l'URL
    """
    # TODO: refactor and translate this function
    erreur_status_code = False
    numero_version_pcaet = 0
    # boucle qui itère les url des données relatives aux pcaet
    while erreur_status_code is False:
        # incrémentation du numéro de version
        numero_version_pcaet = numero_version_pcaet + 1
        try:
            csv_url = settings.get(
                "api", f"csv_url_pcaet_{type_fichier}_ademe_v{numero_version_pcaet}"
            )
        except Exception:
            break
        # récupération du contenu qui est renoyé par l'url. L'URL a son numéro de version qui change à chaque itération
        req = requests.get(csv_url.format(val=numero_version_pcaet))
        # si le status du site est différent de 200 la boucle s'arrête et on récupère les données de la dernière url valide
        if req.status_code == 200:
            url_content = req.content
            # récupération du chemin vers le dossier qui stocke les pcaet de manière provisoire
            pcaet_files_path = settings.get("api", "pcaet_path")
            logger.info(
                f"""La version {numero_version_pcaet} du PCAET {type_fichier} a été récupérée (URL : {
                    csv_url.format(val=numero_version_pcaet)
                }) et est sauvegardée dans le dossier {pcaet_files_path}."""
            )
            # on colle le contenu de l'URL dans un fichier CSV qu'on enregistre
            try:
                with open(
                    pcaet_files_path
                    + region
                    + f"_Demarches_PCAET_V{numero_version_pcaet}_{type_fichier}.csv",
                    "wb",
                ) as csv_file:
                    csv_file.write(url_content)
            except FileNotFoundError:
                raise PcaetError("Configuration Error: Unable to save PCAET CSV file")
        else:
            erreur_status_code = True


class BasePcaet:
    """Base class for all PCAET DataFrame manipulation classes"""

    def __init__(self, csv_chemin: str):
        self.csv_chemin = csv_chemin
        self.df: pd.DataFrame = None
        self.list_final_pcaet_error = []

    # Abstract methods, to override
    def get_input_columns(self) -> list[str]:
        """Liste des colonnes du DataFrame au format initial ADEME."""
        raise NotImplementedError()

    def get_trajectories(self) -> list[str]:
        """Liste des types de trajectoires qu'on va utiliser pour notre df final."""
        raise NotImplementedError()

    def get_regex_list(self) -> list[str]:
        """Liste des expressions régulière qu'on va utiliser pour sélectionner les colonnes du df."""
        raise NotImplementedError()

    def define_reference_years(self):
        """Rename or reference years columns to annee_compta_{trajectory}."""
        raise NotImplementedError()

    def specific_treatment(self, df):
        return df

    # Common methods
    def read_csv(self):
        col_list = self.get_input_columns()
        self.df = pd.read_csv(self.csv_chemin, delimiter=";", usecols=col_list)

    def sort_out_older_duplicates(self):
        """
        Met à jour le dataframe en ne conservant que la version la plus récente pour chaque territoire.
        """
        self.df["Date_modification"] = pd.to_datetime(
            self.df["Date_modification"], format="%d/%m/%Y"
        ).dt.date

        self.df = (
            self.df.sort_values(by="Date_modification")
            .drop_duplicates(["siren"], keep="last")
            .drop("Date_modification", axis=1)
        )

    def get_df_with_new_columns(self) -> pd.DataFrame:
        YEARS = ["ref", "2021", "2026", "2030", "2050", "2030", "2050"]
        trajectories = self.get_trajectories()
        regex_list = self.get_regex_list()

        OUT_COLUMNS = ["siren", "trajectory", "category", "year", "value", "is_ref"]
        final_df = pd.DataFrame([], columns=OUT_COLUMNS)
        for trajectory_id, trajectory in enumerate(trajectories):
            df = self.df.dropna(subset=[f"annee_compta_{trajectory}"])
            reference_year = df[f"annee_compta_{trajectory}"].astype(int)

            for col in df.columns:
                match = re.search(regex_list[trajectory_id], col)
                if not match:
                    continue
                category_id, year_id = match.groups()
                year = YEARS[int(year_id) - 1]

                current_df = pd.DataFrame([], columns=OUT_COLUMNS)
                current_df["siren"] = df["siren"]
                current_df["trajectory"] = trajectory
                current_df["category"] = int(category_id)
                current_df["year"] = reference_year if year == "ref" else int(year)
                current_df["value"] = df[col]
                current_df["is_ref"] = year == "ref"

                final_df = pd.concat([final_df, current_df], ignore_index=True)
        return final_df

    def drop_null_values(self, df: pd.DataFrame) -> pd.DataFrame:
        """Remove objectives if the all values are 0 for a modality or a year"""
        df = df.set_index(["siren", "trajectory", "year", "category"])
        year_is_null = (
            (df["value"] == 0).groupby(level=["siren", "trajectory", "year"]).all()
        )
        category_is_null = (
            (df["value"] == 0).groupby(level=["siren", "trajectory", "category"]).all()
        )
        df = df.loc[df["is_ref"] | ~year_is_null]
        df = df.reorder_levels(["siren", "trajectory", "category", "year"])
        df = df.loc[~category_is_null]

        # Empty ref values are filled with zeros
        df.loc[df["is_ref"] & df["value"].isna(), "value"] = 0
        return df.reset_index()

    # Public methods
    def create_final_df(self):
        """Get the final dataframe for the specified PCAET"""
        self.read_csv()

        self.df.rename(
            {"SIREN collectivites_coporteuses": "siren"}, axis=1, inplace=True
        )
        self.df.dropna(subset=["siren"], inplace=True)
        self.df["siren"] = self.df["siren"].astype(int).astype(str)
        # Convert eventual SIRET numbers (14 digits) to SIREN (9 digits)
        self.df["siren"] = self.df["siren"].apply(lambda siren: siren[:9])

        self.sort_out_older_duplicates()

        self.define_reference_years()

        new_df = self.get_df_with_new_columns()

        new_df.dropna(subset=["year"], inplace=True)
        new_df["value"].fillna(0, inplace=True)

        new_df = self.specific_treatment(new_df)
        new_df = self.drop_null_values(new_df)

        return new_df


class PcaetPecSeq(BasePcaet):
    """Renvoie un dataframe avec les données du PCAET Pec seq."""

    def get_input_columns(self):
        if re.search("V1_pec", self.csv_chemin):
            # liste des colonnes pour la pcaet pec seq V1
            return [
                "Id",
                "Date_modification",
                "Date_lancement",
                "SIREN collectivites_coporteuses",
                "PEC_Emission_GES_Annee_Compta",
                *[f"PEC_GES_{a}_{b}" for a in range(1, 9) for b in range(1, 6)],
                "PEC_Consommation_Annee_Compta",
                *[f"PEC_CONSO_{a}_{b}" for a in range(1, 9) for b in range(1, 6)],
            ]
        elif re.search("V2_pec", self.csv_chemin):
            # liste des colonnes pour la pcaet pec seq V2
            return [
                "Id",
                "Date_modification",
                "Date_lancement",
                "SIREN collectivites_coporteuses",
                "PEC_Emission_GES_Annee_Compta",
                *[f"PEC_GES_{a}_{b}" for a in range(1, 9) for b in (1, 3, 6, 7)],
                "PEC_Consommation_Annee_Compta",
                *[f"PEC_CONSO_{a}_{b}" for a in range(1, 9) for b in (1, 3, 6, 7)],
            ]
        else:
            raise PcaetError(
                f"Unexpected CSV file name: {self.csv_chemin} (should contain 'V1_pec' or 'V2_pec')"
            )

    def get_trajectories(self):
        return ["emission_ges", "consommation_ener"]

    def get_regex_list(self):
        return [
            r"^PEC_GES_([1-8])_([1-7])$",
            r"^PEC_CONSO_([1-8])_([1-7])$",
        ]

    def define_reference_years(self):
        self.df[["annee_compta_emission_ges", "annee_compta_consommation_ener"]] = (
            self.df[["PEC_Emission_GES_Annee_Compta", "PEC_Consommation_Annee_Compta"]]
        )

    def specific_treatment(self, df):
        # Convert form teqCO2 to kteqCO2
        df.loc[df["trajectory"] == "emission_ges", "value"] /= 1000
        return df


class PcaetEnr(BasePcaet):
    """Renvoie un dataframe avec les données du PCAET Enr."""

    def get_input_columns(self):
        if re.search(("V1_enr"), self.csv_chemin):
            # liste des colonnes pour la pcaet enr V1
            return [
                "Id",
                "Date_modification",
                "SIREN collectivites_coporteuses",
                *[f"ENR_{a}_Diag_Annee_Compta" for a in range(1, 15)],
                *[f"ENR_{a}_{b}_Production" for a in range(1, 15) for b in range(1, 6)],
                *[
                    f"ENR_{a}_{b}_Consommation"
                    for a in (8, 9, 10, 11, 12, 14)
                    for b in (2, 3, 4, 5)
                ],
                *[f"ENR_valorisation_recuperation_{a}" for a in range(2, 6)],
            ]
        elif re.search(("V2_enr"), self.csv_chemin):
            # liste des colonnes pour la pcaet enr V2
            return [
                "Id",
                "Date_modification",
                "SIREN collectivites_coporteuses",
                *[f"ENR_{a}_Diag_Annee_Compta" for a in range(1, 15)],
                *[
                    f"ENR_{a}_{b}_Production"
                    for a in (1, 2, 3, 4, 5, 6, 7, 13)
                    for b in (1, 3, 6, 7)
                ],
                *[
                    f"ENR_{a}_{b}_Consommation"
                    for a in (8, 9, 10, 11, 12, 14)
                    for b in (1, 3, 6, 7)
                ],
                *[f"ENR_valorisation_recuperation_{a}" for a in (3, 6, 7)],
            ]
        else:
            raise PcaetError(
                f"Unexpected CSV file name: {self.csv_chemin} (should contain 'V1_enr' or 'V2_enr')"
            )

    def get_trajectories(self):
        return ["enr_production"]

    def get_regex_list(self):
        return [r"^ENR_([1-9]|1[0-4]|50)_([1-7])_(?:Production|Consommation)$"]

    def define_reference_years(self):
        """Défini l'année maximale du diagnostique."""
        annee_diag = [col for col in self.df.columns if "Diag_Annee_Compta" in col]
        self.df["annee_compta_enr_production"] = self.df[annee_diag].max(axis=1)

        self.df[[f"ENR_valorisation_recuperation_1"]] = 0
        # Renomme les colonnes valorisation_recuperation, avec un nouvel id arbitraire: 50
        self.df = self.df.rename(
            columns={
                f"ENR_valorisation_recuperation_{a}": f"ENR_50_{a}_Production"
                for a in range(1, 8)
            },
        )

    def specific_treatment(self, df):
        if re.search("V1_enr", self.csv_chemin):
            # For renewables in PCAET V1, there can be two values : one in production and
            # one in consumption. We only keep the max of them
            df = (
                df.groupby(["siren", "trajectory", "category", "year", "is_ref"])
                .max()
                .reset_index()
            )

            # Convert from MWh to GWh
            df.loc[df["category"] == "13", "value"] /= 1000
            df.loc[df["category"] == "14", "value"] /= 1000
            df.loc[df["category"] == "50", "value"] /= 1000

        return df


class PcaetPolluant(BasePcaet):
    """Renvoie un dataframe avec les données du PCAET Polluants atmosphériques."""

    def get_input_columns(self):
        if re.search(("V1_polluant"), self.csv_chemin):
            # liste des colonnes pour la pcaet polluant V1
            return [
                "Id",
                "Date_modification",
                "SIREN collectivites_coporteuses",
                *[f"POL_{a}_Diag_Annee_Compta" for a in range(1, 7)],
                *[
                    f"POL_{a}_{b}_{c}"
                    for a in range(1, 9)
                    for b in range(1, 6)
                    for c in range(1, 7)
                ],
            ]
        elif re.search(("V2_polluant"), self.csv_chemin):
            # liste des colonnes pour la pcaet polluant V2
            return [
                "Id",
                "Date_modification",
                "SIREN collectivites_coporteuses",
                *[f"POL_{a}_Diag_Annee_Compta" for a in range(1, 7)],
                *[
                    f"POL_{a}_{b}_{c}"
                    for a in range(1, 9)
                    for b in ("1", "3", "6", "7")
                    for c in range(1, 7)
                ],
            ]
        else:
            raise PcaetError(
                f"Unexpected CSV file name: {self.csv_chemin} (should contain 'V1_polluant' or 'V2_polluant')"
            )

    def get_regex_list(self):
        return [
            r"^POL_([1-8])_([1-7])_1$",
            r"^POL_([1-8])_([1-7])_2$",
            r"^POL_([1-8])_([1-7])_3$",
            r"^POL_([1-8])_([1-7])_4$",
            r"^POL_([1-8])_([1-7])_5$",
            r"^POL_([1-8])_([1-7])_6$",
        ]

    def get_trajectories(self):
        return ["pm10", "pm25", "nox", "sox", "covnm", "nh3"]

    def define_reference_years(self):
        for i, col in enumerate(self.get_trajectories()):
            self.df[f"annee_compta_{col}"] = self.df[f"POL_{i+1}_Diag_Annee_Compta"]


def get_pcaet_paths(region: str) -> list[str]:
    """
    Returns the list of paths corresponding to each existing version of each trajectory.
    Earlier version are place prior to later ones. [pec_seq_V1, pec_seq_V2, enr_V1, ...]
    """

    # path vers le dossier qui contient les pcaets au format csv.
    pcaet_files_path = settings.get("api", "pcaet_path")

    pcaet_paths = []
    # création de path de manière dynamique vers les différents pcaets
    for type_pcaet in ["pec_seq", "enr", "polluant"]:
        version = 1
        while True:  # Broken when a version's file is not found
            path = (
                pcaet_files_path
                + region
                + "_Demarches_PCAET_V{version}_{type_pcaet}.csv".format(
                    version=version, type_pcaet=type_pcaet
                )
            )

            if not os.path.isfile(path):
                break

            pcaet_paths.append(path)
            version += 1

    if not pcaet_paths:
        raise PcaetError("No path was found to the PCAETs. Were files downloaded?")

    return pcaet_paths


def convert_dataframe_for_every_trajectory(
    dataframe: pd.DataFrame, passage_table: PassageTable, keep_duplicates: bool
) -> pd.DataFrame:
    df_out = pd.DataFrame()
    for key in TRAJECTORY_KEYS:
        trajectory = passage_table.convert_table_key(key)
        current_df = dataframe[dataframe["trajectory"] == trajectory]
        current_df = (
            passage_table.convert_data(key, current_df, keep_duplicates)
            .drop(columns=["category"] if keep_duplicates else ["category", "is_ref"])
            .rename(columns={"SECTOR": "category", "RENEWABLEPROD": "category"})
        )
        df_out = pd.concat([df_out, current_df], ignore_index=True)
    return df_out


async def get_siren_assignment(region: str) -> pd.DataFrame:
    schema = region.replace("-", "_")
    data = await fetch(
        f"""
        SELECT code AS siren, 'epci' AS zone_type, code AS zone_id FROM {schema}.epci
        UNION
        SELECT siren, zone_type, zone_id FROM {schema}.siren_assignment
        """
    )
    return pd.DataFrame(dict(line) for line in data)


async def get_territories(region: str) -> pd.DataFrame:
    schema = region.replace("-", "_")
    data = await fetch(
        f"""
        SELECT commune, type_territoire as zone_type, code as zone_id
        FROM {schema}.territoire
        """
    )
    return pd.DataFrame(dict(line) for line in data)


def convert_traj_pcaet_to_regional_categories_and_aggregate(
    traj_pcaet: pd.DataFrame, trajectory: dict, region_passage_table: PassageTable
) -> pd.DataFrame:
    traj_key = trajectory["table"]
    region_category = region_passage_table.convert_column_key(
        trajectory["column"], traj_key
    )
    # TODO: handle ENERGYVECTOR here ?
    passage_df = (
        region_passage_table.get_dataframe(traj_key)
        .rename({str(trajectory["column"]): "category"}, axis=1)
        .dropna(subset=["category"])
        .dropna(axis=1)
    )
    traj_pcaet = pd.merge(traj_pcaet, passage_df, "inner", on=["category"]).drop(
        columns=["category"]
    )
    traj_pcaet[region_category] = traj_pcaet[region_category].astype(int)
    traj_pcaet = (
        traj_pcaet.groupby(["siren", "trajectory", region_category, "year", "is_ref"])
        .sum()
        .reset_index("is_ref")
    )
    return traj_pcaet


async def get_historical_data(
    region: str, table_key: DataSet, category: str
) -> pd.DataFrame:
    schema = region.replace("-", "_")

    passage_table = PassageTable(region)
    await passage_table.load([table_key])
    table_name = passage_table.convert_table_key(table_key)
    custom_filters = passage_table.get_custom_real_conditions(table_key)

    params = []
    custom_condition = ""
    for col, val in custom_filters.items():
        params.append(str(val))
        custom_condition += f"AND {col}::text = ${len(params)} "

    data = await fetch(
        f"""
        SELECT commune, {category}, annee AS year, SUM(valeur) AS history
        FROM {schema}.{table_name}
        WHERE TRUE {custom_condition}
        GROUP BY commune, {category}, year
        """,
        *params,
    )
    history_df = pd.DataFrame(dict(line) for line in data)
    history_df[category] = history_df[category].astype(int)
    return history_df


def get_corrected_year_and_coef(
    traj_pcaet: pd.DataFrame, available_years: list[int], region_category: str
) -> pd.DataFrame:
    traj_ref = (
        traj_pcaet.loc[traj_pcaet["is_ref"], []]
        .reset_index("year")
        .rename(columns={"year": "ref_year"})
    )
    traj_ref["first_obj_year"] = (
        traj_pcaet.loc[~traj_pcaet["is_ref"], []]
        .reset_index()
        .groupby(["siren", "trajectory", region_category])
        .min()["year"]
    )
    traj_ref["first_obj_year"] = traj_ref["first_obj_year"].fillna(traj_ref["ref_year"])
    traj_ref["first_obj_val"] = traj_pcaet.loc[
        traj_ref.set_index(["first_obj_year"], append=True).index,
        "relative_value",
    ].reset_index(["year"], drop=True)

    # The reference year if available, else the closest available year above it
    traj_ref["year"] = pd.NA
    for year in available_years:
        selector = traj_ref["year"].isna() & (traj_ref["ref_year"] <= year)
        traj_ref.loc[selector, "year"] = year
    # In case there are reference years > any available year
    traj_ref.loc[traj_ref["year"].isna(), "year"] = year

    # Apply the year correction. If the reference year is available (year = ref_year), it will have no effect (coef = 1)
    traj_ref["correction_coef"] = (
        (traj_ref["first_obj_val"] - 1)
        * (traj_ref["year"] - traj_ref["ref_year"])
        / (traj_ref["first_obj_year"] - traj_ref["ref_year"]).replace(0, 1)
    ) + 1
    return traj_ref.drop(columns=["first_obj_year", "first_obj_val", "ref_year"])


def convert_regional_pcaet_to_enums(
    traj_pcaet: pd.DataFrame, trajectory: dict, region_passage_table: PassageTable
) -> pd.DataFrame:
    traj_key = trajectory["table"]
    region_category = region_passage_table.convert_column_key(
        trajectory["column"], traj_key
    )
    # TODO: handle ENERGYVECTOR here ?
    traj_pcaet = traj_pcaet.reset_index()
    traj_pcaet[region_category] = traj_pcaet[region_category].astype(int)
    traj_pcaet = region_passage_table.convert_data(traj_key, traj_pcaet).rename(
        columns={str(trajectory["column"]): "category"}
    )
    # Remove potential NaN index values that could happen for double categories
    # (eg., type_prod_enr is used for both RENEWABLEPROD and ENERGYVECTOR in AuRA)
    traj_pcaet = traj_pcaet[traj_pcaet["category"] != ""].dropna(subset=["category"])
    return traj_pcaet


async def insert_relative_methodology_pcaet(pcaet: pd.DataFrame):
    """
    This function inserts values with the "relative methodology" into the PCAET dataframe.

    The main steps are :
        - convert PCAET categories to enums with passage tables
        - convert enums to categories of the region with passage tables
        - calculate relative objectives by dividing by the reference values
        - get the historical data at reference year
        - manage special case when there is no historical data at reference year
        - sum historical (communal) data for each PCAET territory
        - multiply historical data with objectives in % to get final values
        - convert categories back to enums with passage tables

    See test_relative_pcaet_methodology_* for test cases.

    pcaet: DataFrame with columns: siren, trajectory, category, year, is_ref, value
    returns the DataFrame with 2 new columns: region and relative_value
    """
    # Create a duplicated dataframe for relative PCAET without changing the value
    relative_pcaet_df = pcaet.copy().rename(columns={"value": "relative_value"})

    # Convert PCAET dataframe with passage tables, dropping duplicated categories
    passage_table = PassageTable("pcaet")
    await passage_table.load(TRAJECTORY_KEYS)
    pcaet = convert_dataframe_for_every_trajectory(
        pcaet, passage_table, keep_duplicates=False
    )

    # Convert relative PCAET dataframe with passage tables, keeping duplicated categories
    relative_pcaet_df = convert_dataframe_for_every_trajectory(
        relative_pcaet_df, passage_table, keep_duplicates=True
    )

    pcaet = pd.merge(
        pcaet,
        relative_pcaet_df,
        "outer",
        on=["siren", "trajectory", "category", "year"],
    )

    # TODO: Why are there duplicates ??
    pcaet.drop_duplicates(inplace=True)

    relative_pcaet = pd.DataFrame(
        columns=["region", "siren", "trajectory", "category", "year", "relative_value"]
    )
    regions = await regions_configuration.get_configuration()
    for region in regions:
        if region["is_national"] or region["id"] == "france":
            continue

        siren_assignment = await get_siren_assignment(region["id"])
        # inner join to only keep territories that have a PCAET AND are in the region
        region_pcaet = pd.merge(pcaet, siren_assignment, "inner", on=["siren"])
        # columns: siren, trajectory, category, year, value, relative_value, is_ref, zone_type, zone_id

        if region_pcaet.empty:
            continue

        region_ref_df = (
            region_pcaet[region_pcaet["is_ref"]]
            .drop(columns=["category", "value", "relative_value", "is_ref", "year"])
            .drop_duplicates()
        )
        territories = await get_territories(region["id"])
        region_ref_df = pd.merge(
            region_ref_df, territories, on=["zone_type", "zone_id"]
        ).drop(columns=["zone_type", "zone_id"])
        # columns: siren, trajectory, commune

        region_pcaet = region_pcaet.drop(columns=["zone_type", "zone_id"])

        trajectories = pcaet_trajectories_meta()
        region_passage_table = PassageTable(region["id"])
        await region_passage_table.load([traj["table"] for traj in trajectories])

        for trajectory in trajectories:
            traj_key = trajectory["table"]
            traj_id = trajectory["id"]
            try:
                region_table_name = region_passage_table.convert_table_key(traj_key)
                region_category = region_passage_table.convert_column_key(
                    trajectory["column"], traj_key
                )
            except ValueError:
                continue
            if not region_table_name or not region_category:
                continue

            traj_pcaet = region_pcaet[region_pcaet["trajectory"] == traj_id]
            if traj_pcaet.empty:
                continue
            # columns: siren, trajectory, category, year, value, relative_value, is_ref
            # TODO: keep a 0 and add the raw value to the observed value to get objectives
            traj_pcaet[traj_pcaet["is_ref"]] = traj_pcaet[traj_pcaet["is_ref"]].replace(
                0, 0.0001
            )

            traj_pcaet = convert_traj_pcaet_to_regional_categories_and_aggregate(
                traj_pcaet, trajectory, region_passage_table
            )
            # columns: siren*, trajectory*, secteur*, year*, value, relative_value, is_ref

            # Calculate variations
            ref = (
                traj_pcaet[traj_pcaet["is_ref"]]
                .drop(columns=["is_ref"])
                .reset_index("year")
            )
            traj_pcaet["relative_value"] = (
                traj_pcaet["relative_value"] / ref["relative_value"]
            )
            # columns: siren*, trajectory*, secteur*, year*, value, is_ref, relative_value

            history_df = await get_historical_data(
                region["id"], traj_key, region_category
            )
            # columns: commune, secteur (or other), year, history

            # In case there is no historical data at reference year, we need to take
            # the closest available year and apply a correction coeficient
            available_years = sorted(history_df["year"].drop_duplicates().to_list())
            traj_ref = get_corrected_year_and_coef(
                traj_pcaet, available_years, region_category
            )
            # columns: siren*, trajectory*, secteur*, year, correction_coef
            traj_pcaet.loc[~traj_pcaet["is_ref"], "relative_value"] /= traj_ref[
                "correction_coef"
            ]

            traj_ref = traj_ref.reset_index()[
                ["siren", "trajectory", "year"]
            ].drop_duplicates()
            traj_ref = pd.merge(
                traj_ref,
                region_ref_df[region_ref_df["trajectory"] == traj_id],
                on=["siren", "trajectory"],
            )
            # columns: siren, trajectory, year, commune

            # Link communal data to the siren numbers and filter reference years
            history_df = (
                pd.merge(traj_ref, history_df, "left", on=["commune", "year"])
                .drop(columns=["commune"])
                # Sum all communes for each territory
                .groupby(["siren", "trajectory", region_category, "year"])
                .sum()
            )
            # columns: siren*, trajectory*, secteur*, year*, history

            # Add missing history years and move relative_value to these years
            traj_pcaet = traj_pcaet.merge(
                history_df[[]], "outer", left_index=True, right_index=True
            )
            traj_pcaet.loc[traj_pcaet["is_ref"].isna()] = traj_pcaet.loc[
                traj_pcaet["is_ref"].fillna(False)
            ].reset_index("year")
            traj_pcaet["is_ref"] = traj_pcaet["is_ref"].astype(bool)

            history_df = history_df.reset_index("year")
            traj_pcaet = pd.merge(
                traj_pcaet, history_df, "left", left_index=True, right_index=True
            ).drop(columns=["year"])
            history_df = history_df.reset_index(region_category).drop(
                columns=[region_category, "history"]
            )
            history_df = history_df[~history_df.index.duplicated(keep="first")]
            # columns: siren*, trajectory*, year
            traj_pcaet["year"] = traj_pcaet.index.get_level_values("year")
            traj_pcaet.loc[traj_pcaet["is_ref"], "year"] = history_df["year"]
            traj_pcaet = traj_pcaet.reset_index("year", drop=True).set_index(
                "year", append=True
            )
            traj_pcaet = traj_pcaet[~traj_pcaet.index.duplicated(keep="first")]
            # For categories that don't have historical data, put 0.0001 in order not to
            # loose any data by a division/multiplication by 0
            traj_pcaet["history"] = traj_pcaet["history"].fillna(0).replace(0, 0.0001)

            # Calculate final values in relative methodo
            traj_pcaet["relative_value"] = traj_pcaet["relative_value"] * traj_pcaet[
                "history"
            ].astype(float)

            # Convert back pcaet dataframe with passage tables
            traj_pcaet = convert_regional_pcaet_to_enums(
                traj_pcaet, trajectory, region_passage_table
            )

            traj_pcaet = traj_pcaet[
                ["siren", "trajectory", "year", "category", "relative_value"]
            ]
            traj_pcaet["region"] = region["id"]
            relative_pcaet = pd.concat([relative_pcaet, traj_pcaet], ignore_index=True)
            # columns: region, siren, trajectory, category, year, relative_value

    pcaet = pd.merge(
        pcaet.drop(columns=["relative_value"]),
        relative_pcaet,
        "outer",
        on=["siren", "trajectory", "category", "year"],
    )
    # columns: region, siren, trajectory, category, year, is_ref, value, relative_value

    pcaet = pcaet[pcaet["value"].notna() | pcaet["relative_value"].notna()]
    pcaet["is_ref"] = pcaet["is_ref"].fillna(True)

    # columns: region, siren, trajectory, category, year, is_ref, value, relative_value
    return pcaet


async def create_pcaet_dataframe(region: str) -> pd.DataFrame:
    """
    Returns the PCAET dataframe created from all trajectory sources (pec_seq, enr and polluant).
    The columns of the dataframe are : siren, trajectory, category, year, value, variation and is_ref.
    """
    # Since paths are ordered by version, it's important to keep the same order until concatenation.
    pcaet_paths = get_pcaet_paths(region)

    # Create a dataframe builder for each pcaet file, and extract the dataframes
    pcaet_builders: list[BasePcaet] = []
    for path in pcaet_paths:
        if "pec_seq" in path:
            pcaet_builders.append(PcaetPecSeq(path))
        if "enr" in path:
            pcaet_builders.append(PcaetEnr(path))
        if "polluant" in path:
            pcaet_builders.append(PcaetPolluant(path))
    traj_dataframes = [builder.create_final_df() for builder in pcaet_builders]

    # Concatenate dataframes splited by trajectory and version
    # For each version on the PCAET, we drop previous PCAET of the same zone
    # So dataframes must be in order earlier to later version
    pcaet_df = traj_dataframes[0]
    for df in traj_dataframes[1:]:
        zones = pd.concat(
            [
                df[["siren", "trajectory"]].drop_duplicates(),
                pcaet_df[["siren", "trajectory"]].drop_duplicates(),
            ]
        )
        dup_zones = zones[zones.duplicated(keep="first")]
        for _, row in dup_zones.iterrows():
            pcaet_df = pcaet_df[(pcaet_df[["siren", "trajectory"]] != row).any(axis=1)]
        pcaet_df = pd.concat([pcaet_df, df], ignore_index=True)

    pcaet_df = await insert_relative_methodology_pcaet(pcaet_df)

    # Convert to str for DB insertion
    pcaet_df["category"] = pcaet_df["category"].astype(str)
    pcaet_df = pcaet_df.sort_values(["siren", "trajectory", "category", "year"])
    # Fill region column because it's in the primary key (not nullable)
    pcaet_df["region"] = pcaet_df["region"].fillna("unknown")
    return pcaet_df


def create_pcaet_meta(region: str):
    pcaet_files_path = settings.get("api", "pcaet_path")
    pcaet_paths = []
    version = 1
    while True:  # Broken when a version's file is not found
        path = f"{pcaet_files_path}{region}_Demarches_PCAET_V{version}_meta.csv"
        if not os.path.isfile(path):
            break
        pcaet_paths.append(path)
        version += 1
    df = pd.DataFrame()
    for path in pcaet_paths:
        col_list = [
            "SIREN collectivites_coporteuses",
            "Demarche_etat",
            "Date_modification",
            "Date_lancement",
            "Oblige",
        ]
        version_df: pd.DataFrame = pd.read_csv(path, delimiter=";", usecols=col_list)
        version_df["updated"] = pd.to_datetime(
            version_df["Date_modification"], format="%d/%m/%Y"
        ).dt.date
        version_df.sort_values(by="Date_modification", inplace=True)
        df = (
            pd.concat([df, version_df])
            .drop_duplicates(["SIREN collectivites_coporteuses"], keep="last")
            .dropna(subset=["SIREN collectivites_coporteuses"])
        )
    df["siren"] = df["SIREN collectivites_coporteuses"].astype(int).astype(str)
    # Convert eventual SIRET numbers (14 digits) to SIREN (9 digits)
    df["siren"] = df["siren"].apply(lambda siren: siren[:9])
    df["statut"] = df["Demarche_etat"].replace(
        {"Définitif": "Mise en œuvre", "Mise en oeuvre": "Mise en œuvre"}
    )
    df["oblige"] = df["Oblige"] == "O"
    df = df.rename(
        {"Date_modification": "date_modification", "Date_lancement": "date_lancement"},
        axis=1,
    ).drop(
        ["updated", "SIREN collectivites_coporteuses", "Demarche_etat", "Oblige"],
        axis=1,
    )
    return df


def update_db_with_new_pcaet(pcaet_df: pd.DataFrame, pcaet_meta: pd.DataFrame):
    """Insert PCAET dataframe into the database."""
    engine = get_pg_engine()
    with engine.connect() as conn:
        conn.execute(
            text(f"DELETE FROM {PCAET_TABLE}; DELETE FROM {PCAET_META_TABLE};")
        )

        schema = PCAET_TABLE.split(".")[0] if "." in PCAET_TABLE else None
        table = PCAET_TABLE.split(".", 1)[-1]
        pcaet_df.to_sql(table, conn, schema, if_exists="append", index=False)

        schema = PCAET_META_TABLE.split(".")[0] if "." in PCAET_META_TABLE else None
        table = PCAET_META_TABLE.split(".", 1)[-1]
        pcaet_meta.to_sql(table, conn, schema, if_exists="append", index=False)

        conn.commit()


def delete_pcaet_files(region: str):
    """Supprime les fichiers pcaet au format csv."""
    for f in get_pcaet_paths(region):
        os.remove(f)

    pcaet_files_path = settings.get("api", "pcaet_path")
    version = 1
    while True:  # Broken when a version's file is not found
        path = f"{pcaet_files_path}{region}_Demarches_PCAET_V{version}_meta.csv"
        if not os.path.isfile(path):
            break
        os.remove(path)
        version += 1


async def convert_pcaet_header_to_regional_data():
    """
    Convert meta.pcaet_header to <region>.data_pcaet
    This will update every available region where a table named "data_pcaet" exist
    """
    regions = await regions_configuration.get_configuration()
    for region in regions:
        if region["is_national"] or region["id"] == "france":
            continue
        schema = region["id"].replace("-", "_")

        exists = await fetch(
            """SELECT EXISTS (
                SELECT FROM information_schema.tables
                WHERE table_schema = $1 AND table_name = 'data_pcaet'
            )""",
            schema,
        )

        if not exists or not exists[0].get("exists"):
            logger.info(f"Region {region['id']}: table data_pcaet was not found.")
            continue

        # Delete previous values, only for current year (older years are kept)
        await execute(
            f"""
            DELETE FROM {schema}.data_pcaet WHERE annee = date_part('year', now());
            """
        )

        await execute(
            f"""
            INSERT INTO {schema}.data_pcaet (
                SELECT DISTINCT
                    epci,
                    c.modalite_id AS valeur,
                    date_part('year', now()) AS annee, date_modification, date_lancement,
                    (CASE WHEN oblige THEN 'Démarche obligatoire' ELSE 'Démarche volontaire' END) AS demarche,
                    t.nom AS regroupement
                FROM (
                    SELECT
                        code AS epci, COALESCE(s.siren, code) AS siren,
                        COALESCE(s.zone_type, 'epci') AS zone_type,
                        COALESCE(s.zone_id, code) AS zone_id
                    FROM {schema}.epci
                    LEFT JOIN (
                        SELECT UNNEST(epcis) AS epci, siren, zone_type, zone_id
                        FROM {schema}.siren_assignment
                    ) s ON s.epci = code
                ) s
                INNER JOIN meta.pcaet_header h ON s.siren = h.siren
                LEFT JOIN {schema}.territoire t
                    ON s.zone_type = t.type_territoire AND s.zone_id = t.code AND t.type_territoire != 'epci' -- retrieve regroupment names
                LEFT JOIN meta.categorie c
                    ON c.modalite = statut AND c.region = $1 AND c.nom = 'statut_pcaet' -- convert modalities names to integer ids
            );
            """,
            region["id"],
        )

        passage_table = PassageTable(region["id"])
        await passage_table.load([DataSet.POPULATION])
        population_table = passage_table.convert_table_key(DataSet.POPULATION)

        # Retrieve EPCI that have more than 20k inhab and no PCAET
        population_limit = 20_000
        population_year = 2020  # TODO: get last available year
        await execute(
            f"""
            INSERT INTO {schema}.data_pcaet (
                SELECT code AS epci FROM (
                    SELECT code, SUM(valeur) > {population_limit} AS valeur
                    FROM {schema}.{population_table} p
                    JOIN {schema}.territoire t ON p.commune = t.commune
                    WHERE type_territoire = 'epci' AND annee = {population_year}
                    GROUP BY code
                ) AS obligatoire WHERE obligatoire.valeur
                EXCEPT SELECT epci FROM {schema}.data_pcaet
                    WHERE annee = date_part('year', now())
            );
            """
        )
        await execute(
            f"""
            UPDATE {schema}.data_pcaet
            SET valeur = 0, annee = date_part('year', now()), demarche = 'Démarche obligatoire'
            WHERE valeur IS NULL;
            """
        )
        logger.info(f"Region {region['id']}: successfully updated data_pcaet.")


async def update_pcaet_from_ademe(region: str):
    download_updated_pcaet("pec_seq", region)
    download_updated_pcaet("enr", region)
    download_updated_pcaet("polluant", region)
    download_updated_pcaet("meta", region)
    pcaet_dataframe = await create_pcaet_dataframe(region)
    pcaet_meta_df = create_pcaet_meta(region)
    update_db_with_new_pcaet(pcaet_dataframe, pcaet_meta_df)
    await convert_pcaet_header_to_regional_data()
    delete_pcaet_files(region)


class PcaetError(Exception):
    pass


async def update_siren_assignment(schema: str, fpath: str):
    """Update the list of non-EPCI SIREN numbers"""
    try:
        siren_df = pd.read_csv(fpath, delimiter=";", dtype=str).to_dict("records")
    except Exception:
        raise PcaetError("Erreur lors de la lecture du fichier CSV")
    try:
        for row in siren_df:
            row["epcis"] = list(json.loads(row["epcis"]))
    except (json.JSONDecodeError, TypeError):
        raise PcaetError('Format de la colonne "epcis" incorrect')

    siren_df = [
        (
            row.get("zone_type", ""),
            row.get("zone_id", ""),
            row.get("siren", ""),
            [str(e) for e in row["epcis"]],
        )
        for row in siren_df
    ]

    await execute(f"DELETE FROM {schema}.siren_assignment")
    req = f"""INSERT INTO {schema}.siren_assignment (zone_type, zone_id, siren, epcis)
        VALUES ($1, $2, $3, $4)"""
    await executemany(req, siren_df)


async def get_siren_number(region: str, zone_type: str, zone_id: str) -> str | None:
    """Renvoie le numéro de siren de la maille concernée"""
    if zone_type == "epci":
        return zone_id

    req = """
        SELECT siren FROM {schema}.siren_assignment
        WHERE zone_type = $1 AND zone_id = $2
    """.format(
        schema=region.replace("-", "_")
    )
    rset = await fetch(req, zone_type, zone_id)
    if not rset:
        return None
    result = [dict(x) for x in rset]
    return result[0]["siren"]


async def get_pcaet_ademe(
    region: str, zone_type: str, zone_id: str, relative: list[str] = []
) -> pd.DataFrame | None:
    """
    Fonction qui renvoie les valeurs associées au pcaet ademe.

    Returns
    -------
    pd.DataFrame with columns: {
        "trajectory": str,
        "year": int,
        "category": int,
        "is_ref": bool,
        "value": float,
    }
    or None if there is no PCAET associated to our zone_id
    """
    siren = await get_siren_number(region, zone_type, zone_id)
    if siren is None:
        return None
    rset = await fetch(
        f"""
        SELECT trajectory, year, category, is_ref, (
            CASE WHEN trajectory = ANY($3) THEN relative_value ELSE value END
        ) AS value
        FROM {PCAET_TABLE}
        WHERE siren = $1 AND (region = $2 OR region = 'unknown')
        """,
        siren,
        region,
        relative or [],
    )

    if not rset:
        return None
    pcaet_ademe = pd.DataFrame([dict(x) for x in rset]).dropna(subset=["value"])
    pcaet_ademe["is_ref"] = pcaet_ademe["is_ref"].astype(bool)

    pcaet_passage_table = PassageTable("pcaet")
    await pcaet_passage_table.load(TRAJECTORY_KEYS)
    regional_passage_table = PassageTable(region)
    await regional_passage_table.load(TRAJECTORY_KEYS)

    pcaet_out = pd.DataFrame()
    for key in TRAJECTORY_KEYS:
        regional_table_name = regional_passage_table.convert_table_key(key)
        trajectory = pcaet_passage_table.convert_table_key(key)
        column: type[ColumnPassageType] = next(
            meta for meta in pcaet_trajectories_meta() if meta["table"] == key
        )["column"]

        if not regional_table_name or not trajectory:
            continue

        traj_pcaet = pcaet_ademe[pcaet_ademe["trajectory"] == trajectory]
        traj_pcaet["category"] = traj_pcaet["category"].apply(lambda name: column[name])

        reference = traj_pcaet[traj_pcaet["is_ref"]]
        objectives = traj_pcaet[~traj_pcaet["is_ref"]]

        if objectives.empty or reference.empty:
            continue

        # Get regional categories and merge the ones considered the same by the region
        traj_pcaet["category"] = [
            (
                regional_passage_table.convert_key(category_key, key)[1][False][0]
                if regional_passage_table.contains_key(category_key, key)
                else np.nan
            )
            for category_key in traj_pcaet["category"]
        ]
        traj_pcaet = traj_pcaet.dropna(subset=["category"])
        traj_pcaet = (
            traj_pcaet.groupby(["trajectory", "category", "year"]).sum().reset_index()
        )
        traj_pcaet[["is_ref"]] = traj_pcaet[["is_ref"]].astype(bool)

        pcaet_out = pd.concat([pcaet_out, traj_pcaet], ignore_index=True)

    return pcaet_out


async def get_pcaet_by_category(
    region: str,
    zone_type: str,
    zone_id: str,
    table_key: DataSet,
    category: str,
    enable_relative_pcaet: list[str],
) -> list[dict] | None:
    pcaet_passage_table = PassageTable("pcaet")
    await pcaet_passage_table.load([table_key])
    pcaet_table_name = pcaet_passage_table.convert_table_key(table_key)

    pcaet_df = await get_pcaet_ademe(region, zone_type, zone_id, enable_relative_pcaet)
    if pcaet_df is None or pcaet_df.empty:
        # If there is no pcaet associated to our zone_id
        return None

    # Only keep current trajectory
    pcaet_df = pcaet_df[pcaet_df["trajectory"] == pcaet_table_name]
    pcaet_df.drop("trajectory", axis=1, inplace=True)
    if pcaet_df.empty:
        return None

    categories_meta = await fetch(
        """ SELECT modalite_id, modalite, couleur FROM meta.categorie
            WHERE nom = $1 AND region = $2 """,
        category,
        region,
    )
    categories_meta = {dict(row)["modalite_id"]: dict(row) for row in categories_meta}

    pcaet_out = []
    for cat_id, cat_df in pcaet_df.groupby("category"):
        data = [
            {"annee": data["year"], "valeur": data["value"]}
            for _, data in cat_df.iterrows()
        ]
        pcaet_out.append(
            {
                "nom": categories_meta[int(cat_id)]["modalite"],
                "couleur": categories_meta[int(cat_id)]["couleur"],
                "data": data,
            }
        )

    return pcaet_out


async def pcaet_meta(region: str, zone_type: str, zone_id: str) -> pd.DataFrame:
    """
    Returns
    -------
    pd.DataFrame[siren: str, nom: str, epci_nb: int]
    """
    schema = region.replace("-", "_")
    pcaet_meta = await fetch(
        f"""
        SELECT DISTINCT meta.siren, t1.nom, CARDINALITY(epcis) AS epci_nb,
            (CASE WHEN oblige THEN 'Obligée' ELSE 'Volontaire' END) AS demarche
        FROM (
            SELECT siren, zone_type, zone_id, epcis
                FROM {schema}.siren_assignment
            UNION
            SELECT code AS siren, 'epci' AS zone_type, code AS zone_id, ARRAY[code] AS epcis
                FROM {schema}.epci
        ) AS siren_assignment
        JOIN {schema}.territoire t1 ON zone_id = t1.code AND zone_type = t1.type_territoire
        JOIN {schema}.territoire t2 ON t1.commune = t2.commune
        JOIN {PCAET_META_TABLE} AS meta ON meta.siren=siren_assignment.siren
        WHERE t2.type_territoire = $1 AND t2.code = $2
        """,
        zone_type,
        zone_id,
    )
    pcaet_meta = pd.DataFrame(dict(x) for x in pcaet_meta)
    return pcaet_meta


async def all_pcaet_objectives(
    region: str, trajectory: str, relative_pcaet: bool
) -> pd.DataFrame:
    """
    Returns
    -------
    pd.DataFrame[siren: str, category: str, year: int, value: float]
    """
    # TODO: relative_pcaet as list[str] => if trajectory in relative_pcaet
    if relative_pcaet:
        req = f"""
            SELECT siren, category, year, relative_value AS value FROM {PCAET_TABLE}
            WHERE trajectory=$1 AND region=$2 AND NOT is_ref
        """
    else:
        req = f"""
            SELECT siren, category, year, value FROM {PCAET_TABLE}
            WHERE trajectory=$1 AND (region=$2 OR region='unknown') AND NOT is_ref
        """
    pcaet = await fetch(req, trajectory, region)
    pcaet = pd.DataFrame(dict(r) for r in pcaet)
    return pcaet


async def admin_get_all_pcaet(
    region: str,
    zone_type: str,
    zone_id: str,
    relative_pcaet: bool,
    enable_pollutants: bool,
) -> list[dict]:
    YEARS = [2021, 2026, 2030, 2050]
    trajectories_meta = pcaet_trajectories_meta(enable_pollutants)
    trajectory_keys = [key["table"] for key in trajectories_meta]
    pcaet_passage_table = PassageTable("pcaet")
    await pcaet_passage_table.load(trajectory_keys)
    passage_table = PassageTable(region)
    await passage_table.load(trajectory_keys + [DataSet.POPULATION])

    schema = region.replace("-", "_")

    pcaet_by_territory = await pcaet_meta(region, zone_type, zone_id)
    if pcaet_by_territory.empty:
        return []
    pcaet_by_territory = pcaet_by_territory[["siren", "nom", "epci_nb", "demarche"]]

    # Fill meta values for the supra territory
    meta = await fetch(
        f"""
        SELECT '' AS siren, t.nom, COUNT(DISTINCT e.code) AS epci_nb
        FROM {schema}.territoire t
        JOIN {schema}.territoire e ON e.commune = t.commune
        WHERE t.type_territoire = $1 AND t.code = $2 AND e.type_territoire = 'epci'
        GROUP BY t.code, t.nom
        """,
        zone_type,
        zone_id,
    )
    meta = pd.DataFrame([dict(row) for row in meta])
    pcaet_by_territory = pd.concat([pcaet_by_territory, meta])

    pcaet_by_territory.set_index("siren", inplace=True)
    for traj in trajectories_meta:
        key = traj["table"]
        trajectory = pcaet_passage_table.convert_table_key(key)
        table = passage_table.convert_table_key(key)
        column_name = passage_table.convert_column_key(traj["column"], key)
        custom_filters = passage_table.get_custom_real_conditions(key)

        pcaet = await all_pcaet_objectives(region, trajectory, relative_pcaet)
        if pcaet.empty:
            continue
        pcaet = pcaet.dropna(subset=["value"])
        # Convert pcaet to regional categories and aggregate on them
        passage_df = (
            passage_table.get_dataframe(key)
            .rename({str(traj["column"]): "category"}, axis=1)
            .dropna(subset=["category"])
            .dropna(axis=1)
            .astype({"category": str, column_name: int})
        )
        pcaet = pd.merge(pcaet, passage_df, "inner", on=["category"]).drop(
            columns=["category"]
        )
        pcaet = pcaet.groupby(["siren", column_name, "year"]).sum().reset_index()

        pcaet = pcaet.groupby(["year", "siren"]).apply(
            lambda df: df.set_index(column_name)["value"].to_dict()
        )
        for year in YEARS:
            if year in pcaet.index:
                current_pcaet = pcaet.loc[year]
                pcaet_by_territory[f"{trajectory}_{year}"] = current_pcaet

        observed = await value_by_mesh_and_category(
            region, table, column_name, zone_type, zone_id, "epci", None, custom_filters
        )
        observed = (
            observed.rename(columns={"code": "siren"})
            .groupby("siren")
            .apply(lambda df: df.set_index(column_name)["valeur"].to_dict())
        )
        pcaet_by_territory[f"{trajectory}_obs"] = observed

        for siren, _ in pcaet_by_territory[
            pd.isna(pcaet_by_territory[f"{trajectory}_obs"])
        ].iterrows():
            # Fill observed values for non-epci territories
            zone = await fetch(
                f"""SELECT zone_type, zone_id
                FROM {schema}.siren_assignment WHERE siren='{siren}'"""
            )
            if not zone:
                continue
            observed = await territory_value_by_category(
                region,
                table,
                column_name,
                zone[0]["zone_type"],
                zone[0]["zone_id"],
                None,
                custom_filters,
            )
            observed = observed.set_index(column_name)["valeur"].to_dict()
            # Without putting observed in a list, pandas tries to deconstruct the dict and crashes
            pcaet_by_territory.loc[[siren], f"{trajectory}_obs"] = [observed]

        # Fill observed value for the supra territory
        observed = await territory_value_by_category(
            region, table, column_name, zone_type, zone_id, None, custom_filters
        )
        observed = observed.set_index(column_name)["valeur"].to_dict()
        pcaet_by_territory.loc[[""], f"{trajectory}_obs"] = [observed]

    # Fill the "population" column
    population_table = passage_table.convert_table_key(DataSet.POPULATION)
    population = await total_value_by_mesh(
        region, population_table, zone_type, zone_id, "epci"
    )
    population = population.rename({"code": "siren"}, axis=1).set_index("siren")
    pcaet_by_territory["population"] = population["valeur"]
    for siren, _ in pcaet_by_territory[
        pd.isna(pcaet_by_territory["population"])
    ].iterrows():
        # Fill population values for non-epci territories
        zone = await fetch(
            f"""SELECT zone_type, zone_id
            FROM {schema}.siren_assignment WHERE siren='{siren}'"""
        )
        if not zone:
            continue
        pcaet_by_territory.loc[siren, "population"] = await total_territory_value(
            region, zone[0]["zone_type"], zone[0]["zone_id"], population_table
        )
    pcaet_by_territory.loc["", f"population"] = await total_territory_value(
        region, zone_type, zone_id, population_table
    )

    records = pcaet_by_territory.reset_index().to_dict("records")
    records = [
        {key: value for key, value in record.items() if pd.notna(value)}
        for record in records
    ]
    return records
