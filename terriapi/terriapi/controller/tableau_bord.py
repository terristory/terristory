﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Module dédié aux fonctions d'intéraction avec la base de données pour les tableaux de bord
"""

import json

from terriapi import controller
from terriapi.utils import check_is_admin_in_region, sanitize


async def check_is_valid_dashboard(region, user, thematiques):
    if await check_is_admin_in_region(user, region):
        return True
    protected_representations = [
        "link-launcher",
        "didactic-file-launcher",
        "logo",
        "iframe",
    ]
    for group in thematiques:
        for graph in group.get("graphiques", []):
            if graph.get("representation", None) in protected_representations:
                return False
    return True


def clean_blocks(graphs):
    for graph in graphs:
        if graph.get("representation", None) != "text-block":
            continue
        graph["content"] = sanitize(graph["content"])
    return graphs


async def check_write_rights_on_dashboard(region, dashboard_id, usermail):
    query = f"""SELECT EXISTS (
        SELECT FROM meta.tableau_bord
        WHERE region = $1 AND id = $2
        AND (
            mail = $3 OR array(SELECT unnest(full_share)) @> ARRAY[$3]
        )
    )"""
    res = await controller.fetch(query, region, dashboard_id, usermail)
    return res[0]["exists"]


async def create_dashboard(titre, user, description, region, thematiques):
    """Créer un nouveau tableau de bord

    Parameters
    ----------
    titre : str
    usermail : str
    description : str
    region : str
    thematiques : list

    Returns
    -------
    dict
        Nouvel identifiant
    """
    if not await check_is_valid_dashboard(region, user, thematiques):
        raise PermissionError(
            "Vous ne pouvez créer un tableau avec des éléments réservés aux administrateurs."
        )
    query = """insert into meta.tableau_bord (titre, mail, description, region)
    values ($1, $2, $3, $4) returning id;
    """
    thematiques_query = """insert into meta.tableau_thematique (titre, description, tableau, graphiques, ordre)
    values ($1, $2, $3, $4, $5);
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            rset = await conn.fetch(query, titre, user.mail, description, region)
            new_id = rset[0]["id"]
            # insertion des thématiques via liste de (titre, tableau_id, graphiques)
            await conn.executemany(
                thematiques_query,
                [
                    (
                        x["titre_thematique"],
                        sanitize(x["description_thematique"]),
                        new_id,
                        json.dumps(clean_blocks(x["graphiques"])),
                        x.get("ordre", i),
                    )
                    for i, x in enumerate(thematiques)
                ],
            )
    return {"id": new_id}


async def update_dashboard(dashboard_id, titre, user, description, region, thematiques):
    check_rights = await check_write_rights_on_dashboard(
        region, dashboard_id, user.mail
    )
    if not check_rights:
        raise PermissionError("Vous ne pouvez modifier ce tableau de bord.")
    if not await check_is_valid_dashboard(region, user, thematiques):
        raise PermissionError(
            "Vous ne pouvez créer un tableau avec des éléments réservés aux administrateurs."
        )
    requete_suppression_thematiques = """
                                      delete from meta.tableau_thematique
                                      where tableau = $1;
                                      """

    requete_ajout_thematiques = """
                               insert into meta.tableau_thematique (titre, description, tableau, graphiques, ordre)
                               values ($1, $2, $3, $4, $5);
                               """

    requete_mise_a_jour_tableau_bord = """
                                       update meta.tableau_bord
                                       set titre = $1,
                                       description = $2
                                       where id = $3;
                                       """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.fetch(requete_suppression_thematiques, dashboard_id)
            await conn.fetch(
                requete_mise_a_jour_tableau_bord, titre, description, dashboard_id
            )
            # insertion des thématiques via liste de (titre, tableau_id, graphiques)
            await conn.executemany(
                requete_ajout_thematiques,
                [
                    (
                        x["titre_thematique"],
                        sanitize(x["description_thematique"]),
                        dashboard_id,
                        json.dumps(clean_blocks(x["graphiques"])),
                        x.get("ordre", i),
                    )
                    for i, x in enumerate(thematiques)
                ],
            )
    return {"id": dashboard_id}


async def delete_dashboard(dashboard_id: int, current_user, region: str):
    """Delete a dashboard from database with the given id and the current user.

    Returns
    --------
    dict
        dashboard ID or False if the dashboard does not exist (or couldn't be deleted)
    """
    # on supprime le tableau si l'utilisateur en est bien le propriétaire ou s'il est admin
    owner_condition = "and mail=$2"
    if current_user.est_admin:
        owner_condition = "and $2=$2"
    requete_suppression_tableau_bord = f"""
       delete from meta.tableau_bord where id = $1 and region = $3
       {owner_condition}
       """
    res = await controller.execute(
        requete_suppression_tableau_bord, int(dashboard_id), current_user.mail, region
    )

    # si la suppression a réussi
    if res == "DELETE 0":
        return False
    else:
        return {"id": dashboard_id}


async def liste_tableau(user, region):
    """Récupère l'ensemble des tableaux pour un·e utilisateur·rice et région données

    Parameters
    ----------
    user : Utilisateur
    region : str

    Returns
    -------
    list
        Liste de dict
    """
    query = """select tableau.id
      , tableau.titre
      , tableau.description
      , to_char(tableau.date, 'YYYY-MM-DD HH24:MI') as date
      , tableau.full_share
      , tableau.readonly_share
      , mail as shared_by
      , json_agg(json_build_object('titre', thematique.titre, 'description', thematique.description, 'graphiques', thematique.graphiques, 'ordre', thematique.ordre) ORDER BY thematique.ordre) as thematiques
      from meta.tableau_bord as tableau
      join meta.tableau_thematique as thematique on thematique.tableau = tableau.id
    where (mail=$1 OR array(SELECT unnest(tableau.full_share)) @> ARRAY[$1] 
    OR  array(SELECT unnest(tableau.readonly_share)) @> ARRAY[$1] )
      and region=$2
    group by tableau.id, tableau.titre, tableau.description, tableau.date
    order by tableau.date, tableau.titre
    """
    rset = await controller.fetch(query, user.mail, region)
    tableaux = []
    # pour chaque tableau de bord, on désérialise les graphiques des thématiques stockées en JSON.
    for record in rset:
        record = dict(record)
        record["thematiques"] = json.loads(record["thematiques"])
        record["user_table"] = True
        record["is_readonly"] = False
        if record["shared_by"] == user.mail:
            del record["shared_by"]
        elif user.profil != "admin":
            record["is_readonly"] = (
                isinstance(record["readonly_share"], list)
                and user.mail in record["readonly_share"]
                and (
                    not isinstance(record["full_share"], list)
                    or user.mail not in record["full_share"]
                )
            )
            del record["full_share"]
            del record["readonly_share"]
        tableaux.append(record)
    return tableaux


async def list_public_dashboards(region, type_territoire, code_insee_territoire):
    """Récupère l'ensemble des tableaux publiés pour un type de territoire ou territoire spécifique donné

    Parameters
    ----------
    usermail : str
    region : str

    Returns
    -------
    list
        Liste de dict
    """
    query = """select tableau.id
      , tableau.titre
      , tableau.description
      , to_char(tableau.date, 'YYYY-MM-DD HH24:MI') as date
      , json_agg(json_build_object('titre', thematique.titre, 'description', thematique.description, 'graphiques', thematique.graphiques, 'ordre', thematique.ordre) ORDER BY thematique.ordre) as thematiques
      from meta.tableau_bord as tableau
      join meta.tableau_thematique as thematique on thematique.tableau = tableau.id
      join meta.tableau_affectation affectation on affectation.tableau = tableau.id
      where region = $1
      and ((affectation.zone = $2
      and zone_id = $3)
      or (affectation.zone = $2
      and affectation.zone_id is null))
    group by tableau.id, tableau.titre, tableau.description, tableau.date
    order by tableau.date, tableau.titre
    """
    rset = await controller.fetch(query, region, type_territoire, code_insee_territoire)
    tableaux = []
    # pour chaque tableau de bord, on désérialise les graphiques des thématiques stockées en JSON.
    for record in rset:
        record = dict(record)
        record["thematiques"] = json.loads(record["thematiques"])
        tableaux.append(record)
    return tableaux


async def get_dashboard(
    region, tableau_id, utilisateur, type_territoire, code_insee_territoire
):
    """Récupère les données pour un tableau donné.

    Parameters
    ----------
    tableau_id : int

    Returns
    -------
    dict
    """
    region_configuration = await controller.regions_configuration.get_configuration(
        region
    )
    allow_sharing_dashboard_by_url = region_configuration[0][
        "allow_sharing_dashboard_by_url"
    ]

    # On teste si le tableau n'est pas publié sur certaines régions
    public_query = """SELECT COUNT(*) FROM meta.tableau_affectation
        WHERE tableau = $1 AND ((zone = $2
        and zone_id = $3)
        or (zone = $2
        and zone_id is null))"""
    rset = await controller.fetch(
        public_query, tableau_id, type_territoire, code_insee_territoire
    )
    # Si c'est le cas, on diminue la rigueur de la requête en ajoutant la possibilité
    # que le tableau ne soit pas à l'utilisateur courant.
    is_public = rset[0]["count"] > 0 or allow_sharing_dashboard_by_url

    query = """select tableau.id
      , tableau.titre
      , tableau.description
      , to_char(tableau.date, 'YYYY-MM-DD HH24:MI') as date
      , json_agg(json_build_object('id', thematique.id, 'titre', thematique.titre, 'description', thematique.description, 'graphiques', thematique.graphiques, 'ordre', thematique.ordre) ORDER BY thematique.ordre) as thematiques
      , CASE WHEN tableau.mail = $2 THEN true ELSE false END AS user_table
      from meta.tableau_bord as tableau
      join meta.tableau_thematique as thematique on thematique.tableau = tableau.id
    where tableau.id = $1 and (
        tableau.mail = $2
        OR true = $3
        OR ( array(SELECT unnest(tableau.full_share)) @> ARRAY[$2] )
        OR ( array(SELECT unnest(tableau.readonly_share)) @> ARRAY[$2] )
    )
    and tableau.region = $4
    group by tableau.id, tableau.titre, tableau.description, tableau.date
    order by tableau.date, tableau.titre
    """

    # on récupère le mail de l'utilisateur courant (s'il existe)
    if utilisateur:
        id_utilisateur = utilisateur.mail
    else:
        id_utilisateur = None

    rset = await controller.fetch(query, tableau_id, id_utilisateur, is_public, region)
    if len(rset) == 0:
        return None
    result = dict(rset[0])
    result["thematiques"] = json.loads(result["thematiques"])
    return result


async def assign_zone(tableau_id, zone, code_insee_territoire):
    """Affecte une zone à un tableau

    Parameters
    ----------
    tableau_id : int
    zone : str
       'departement', 'epci', etc.

    Returns
    -------
    """
    # on vérifie d'abord que cette affectation n'existe pas déjà.
    if code_insee_territoire:
        affectation_deja_existante = """select tableau
          , zone
          , zone_id
        from meta.tableau_affectation
        where tableau = $1
           and zone = $2
           and zone_id = $3
        """
        rset = await controller.fetch(
            affectation_deja_existante, tableau_id, zone, code_insee_territoire
        )
    else:
        affectation_deja_existante = """select tableau
          , zone
          , zone_id
        from meta.tableau_affectation
        where tableau = $1
           and zone = $2
           and zone_id is null
        """
        rset = await controller.fetch(affectation_deja_existante, tableau_id, zone)
    if len(rset) != 0:
        return None
    query = """insert into meta.tableau_affectation (tableau, zone, zone_id)
     values ($1, $2, $3)
    """
    await controller.execute(query, tableau_id, zone, code_insee_territoire)
    return {"tableau_id": tableau_id}


async def is_dashboard_owner(usermail, tableau_id, region):
    """Vérifie si le tableau spécifié par son identifiant appartient bien à l'utilisateur·rice

    Parameters
    ----------
    usermail : str
    tableau_id : int

    Returns
    -------
    bool
    """
    query = """select id
    from meta.tableau_bord
    where mail = $1
       and id = $2
       and region = $3
    """
    rset = await controller.fetch(query, usermail, tableau_id, region)
    if len(rset) == 0:
        return False
    return True


async def list_dashboard_assignments(tableau_id):
    req = """
          select zone, zone_id
          from meta.tableau_affectation
          where tableau = $1;
          """
    rset = await controller.fetch(req, tableau_id)
    if len(rset) == 0:
        return []
    return [dict(x) for x in rset]


async def remove_dashboard_assignment(
    tableau_id, type_territoire, code_insee_territoire
):
    # Selon le type d'affectation, on supprime bien la bonne règle
    if code_insee_territoire:
        req = """
              delete
              from meta.tableau_affectation
              where tableau = $1
              and zone = $2
              and zone_id = $3;
              """
        res = await controller.execute(
            req, tableau_id, type_territoire, code_insee_territoire
        )
    else:
        req = """
              delete
              from meta.tableau_affectation
              where tableau = $1
              and zone = $2
              and zone_id is null;
              """
        res = await controller.execute(req, tableau_id, type_territoire)
    if res == "DELETE 0":
        return False
    liste_affectations = await list_dashboard_assignments(tableau_id)
    return liste_affectations


async def share_dashboard(tableau_id, emails, is_readonly_share=False):
    """Share a dashboard with other users by their email

    Parameters
    ----------
    tableau_id : int
        The dashboard identifier.
    emails : list[str]
        The list of emails.
    """
    if is_readonly_share:
        req = """update meta.tableau_bord
        set readonly_share = $2
        where id = $1
        """
    else:
        req = """update meta.tableau_bord
        set full_share = $2
        where id = $1
        """
    await controller.execute(req, tableau_id, emails)


async def remove_dashboard_sharing(
    dashboard_id: int, region: str, user, is_readonly_share=False
):
    """Remove the current user form the list of shared user for the specified dashboard"""
    if is_readonly_share:
        col = "readonly_share"
    else:
        col = "full_share"
    rset = await controller.fetch(
        f"""
        select {col} from meta.tableau_bord
        where id = $1 and region = $2 and ( array(SELECT unnest({col})) @> ARRAY[$3] )
        """,
        dashboard_id,
        region,
        user.mail,
    )
    if len(rset) == 0:
        return False
    shared_emails = rset[0][col]
    new_shared_emails = [email for email in shared_emails if email != user.mail]
    await share_dashboard(dashboard_id, new_shared_emails, is_readonly_share)
    return True


async def duplicate_dashboard(region: str, user, dashboard_id: int, title: str):
    query = """select tableau.id
      , tableau.titre
      , tableau.description
      , to_char(tableau.date, 'YYYY-MM-DD HH24:MI') as date
      , json_agg(json_build_object('id', thematique.id, 'titre', thematique.titre, 'description', thematique.description, 'graphiques', thematique.graphiques, 'ordre', thematique.ordre) ORDER BY thematique.ordre) as thematiques
      , CASE WHEN tableau.mail = $2 THEN true ELSE false END AS user_table
      from meta.tableau_bord as tableau
      join meta.tableau_thematique as thematique on thematique.tableau = tableau.id
    where tableau.id = $1 and (
        tableau.mail = $2
        OR array(SELECT unnest(tableau.full_share)) @> ARRAY[$2]
        OR array(SELECT unnest(tableau.readonly_share)) @> ARRAY[$2]
    )
    and tableau.region = $3
    group by tableau.id, tableau.titre, tableau.description, tableau.date
    order by tableau.date, tableau.titre
    """

    rset = await controller.fetch(query, dashboard_id, user.mail, region)
    if len(rset) == 0:
        return None
    result = dict(rset[0])
    result["thematiques"] = json.loads(result["thematiques"])

    if not await check_is_valid_dashboard(region, user, result["thematiques"]):
        raise PermissionError
    query = """insert into meta.tableau_bord (titre, mail, description, region)
    values ($1, $2, $3, $4) returning id;
    """
    thematiques_query = """insert into meta.tableau_thematique (titre, description, tableau, graphiques, ordre)
    values ($1, $2, $3, $4, $5);
    """
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            rset = await conn.fetch(
                query, title, user.mail, result["description"], region
            )
            new_id = rset[0]["id"]
            # insertion des thématiques via liste de (titre, tableau_id, graphiques)
            await conn.executemany(
                thematiques_query,
                [
                    (
                        x["titre"],
                        sanitize(x["description"]),
                        new_id,
                        json.dumps(clean_blocks(x["graphiques"])),
                        x.get("ordre", i),
                    )
                    for i, x in enumerate(result["thematiques"])
                ],
            )
    return {"id": new_id}
