# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from shutil import which
from subprocess import run

from sanic.log import logger

from terriapi.controller import fetch


def command_exists(name):
    return which(name) is not None


async def convert_archive_to_folder(input_path, output_path):
    """
    Télécharge les données communales admin express de l'IGN

    Parameters
    ----------
    annee : entier
        Année dont datent les données à télécharger
    input_path : chaîne de caractères
        Chemin d'accès au répertoire où déposer ces données
    """
    logger.info(f"Unzipping IGN file from {input_path} to {output_path}")
    if not command_exists("7z"):
        raise ValueError("7z executable command doesn't exist, please install it.")
    output = run("7z x -y " + input_path + " -aoa -o" + output_path, shell=True)
    logger.info(f"IGN file unzipped in {output_path}")


async def integrate_ign_data(year):
    """Intègre la table des communes dans le schéma régional à partir des données IGN téléchargée automatiquement

    Parameters
    ----------

    schema: str
        Nom du schéma régional
    """
    year = int(year)
    req_nom_colonne = """
                      select column_name from information_schema.columns
                      where table_schema = 'france'
                      and table_name = $1;
                      """
    noms_colonnes_brut = await fetch(req_nom_colonne, f"ign_communes_{year}")
    liste_noms_colonnes = [i["column_name"] for i in noms_colonnes_brut]

    req_nom_colonne_code_epci = """
                                select column_name from information_schema.columns
                                where table_schema = 'france'
                                and table_name = $1;
                                """
    noms_colonnes_brut_code_epci = await fetch(
        req_nom_colonne_code_epci, f"ign_epci_{year}"
    )
    liste_noms_colonnes_code_epci = [
        i["column_name"] for i in noms_colonnes_brut_code_epci
    ]

    req_nom_colonne_code_dep = """
                                select column_name from information_schema.columns
                                where table_schema = 'france'
                                and table_name = $1;
                                """
    noms_colonnes_brut_code_dep = await fetch(
        req_nom_colonne_code_dep, f"ign_dep_{year}"
    )
    liste_noms_colonnes_code_dep = [
        i["column_name"] for i in noms_colonnes_brut_code_dep
    ]

    nom_dep = "nom"
    if nom_dep not in liste_noms_colonnes_code_dep:
        nom_dep = "nom_dep"

    nom_epci = "nom"
    nom_code_epci = "code_siren"
    nom = "nom"
    nom_code_commune_epci = "siren_epci"

    if nom not in liste_noms_colonnes:
        nom = "nom_com"

    if nom_code_commune_epci not in liste_noms_colonnes:
        nom_code_commune_epci = "code_epci"

    if nom_code_epci not in liste_noms_colonnes_code_epci:
        nom_code_epci = "code_epci"

    if nom_epci not in liste_noms_colonnes_code_epci:
        nom_epci = "nom_epci"
    req_suppr = f"DROP TABLE IF EXISTS france.communes_{year} CASCADE;"
    req = f"""
        CREATE TABLE france.communes_{year} AS (
            SELECT 
                c.gid,
                c.insee_com, c.insee_dep, c.insee_reg, te.{nom_code_epci} as code_epci, 
                c.{nom} as nom_com, c.statut, te.{nom_epci} as nom_epci, td.{nom_dep} as nom_dep, 
                c.geom
            FROM france.ign_communes_{year} c
            LEFT JOIN france.ign_dep_{year} td on td.insee_dep = c.insee_dep
            LEFT JOIN france.ign_epci_{year} te on te.{nom_code_epci} = c.{nom_code_commune_epci}
        UNION ALL
            SELECT ar.gid,
                ar.insee_arm as insee_com, c.insee_dep, c.insee_reg, tarep.{nom_code_epci} as code_epci,
                ar.nom as nom_com, 'Arrondissement' as statut, tarep.nom as nom_epci, tardep.nom as nom_dep,
                ar.geom
            FROM france.ign_arrondissements_{year} ar
            LEFT JOIN france.ign_communes_{year} c ON c.insee_com = ar.insee_com
            LEFT JOIN france.ign_dep_{year} tardep on tardep.insee_dep = c.insee_dep
            LEFT JOIN france.ign_epci_{year} tarep on tarep.{nom_code_epci} = c.{nom_code_commune_epci}
        );
          """

    await fetch(req_suppr)
    await fetch(req)


async def liste_donnees_perimetres(region):
    sql = """
          select *
          from meta.perimetre_geographique
          where region = $1;
          """

    res = await fetch(sql, region)
    return [x["nom_table"] for x in res]
