﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Fonctions et requêtes SQL sur la gestion des scénarios utilisateur·rice·s
"""

from __future__ import annotations

import json

from terriapi import controller
from terriapi.controller import execute, fetch
from terriapi.controller.strategy_actions.constants import DataSet
from terriapi.controller.strategy_actions.passage_table import PassageTable


async def create_scenario(
    titre: str,
    mail: str,
    description: str | None,
    zone_type: str,
    zone_id: str,
    nom_territoire: str,
    region: str,
    is_national: bool,
    reference_year: int | None,
    actions: list[str],
    params: dict[int, dict],
    advanced_params: list[dict],
    trajectoires_cibles: dict[str, dict],
):
    """Créer un scénario pour un·e utilisateur·rice spécifique.

    Parameters
    ----------
    titre : str
        Titre du scénario.
    mail : str
        Login du compte qui crée le scénario.
    description : str
        Description du scénario. Peut être None.
    zone_type : str
        Type de la zone courante
    zone_id: int
        Id de la zone courante
    nom_territoire: str
        Nom du territoire
    region: str
        Nom standardisé de la région
    actions: list
        Liste de str du numéro des actions activées (i.e. cochées)
    params : dict
        Clé/valeur de tous les paramètres action_params à enregistrer. Clé : id de
        l'action param.
    advanced_params: dict
        Clé/valeur de tous les paramètres avancés pour chaque catégorie d'actions
    trajectoires_cibles: dict[dict]
        Clé/valeur des 3 trajectoires cibles 'energie_produite', 'emission_ges', 'energie_economisee'

    Returns
    -------
    int
        L'ID du scénario.

    """
    schema_type = "regional"
    if is_national:
        schema_type = "national"

    if reference_year is None:
        # Get default reference year by taking the minimal last available year between each data tables
        tables = [DataSet.CONSUMPTION, DataSet.EMISSIONS, DataSet.PRODUCTION]
        passage_table = PassageTable(region)
        await passage_table.load(tables)
        for table in tables:
            table = passage_table.convert_table_key(table)
            schema = region.replace("-", "_")
            year = (await fetch(f"SELECT MAX(annee) FROM {schema}.{table}"))[0]["max"]
            reference_year = min(reference_year or year, year)

    query = """insert into utilisateur_scenario (titre, mail, description, zone_type, zone_id, actions, nom_territoire, region, type, reference_year)
    values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) returning id;
    """
    scenario_id = None
    async with controller.db.acquire() as conn:
        async with conn.transaction():
            # if one fails, nothing should be inserted/commited
            # https://magicstack.github.io/asyncpg/current/api/index.html#transactions
            scenario_id = await conn.fetch(
                query,
                titre,
                mail,
                description,
                zone_type,
                zone_id,
                actions,
                nom_territoire,
                region,
                schema_type,
                reference_year,
            )
            # triplet (action_param id, scenario id, valeur)
            records = (
                (param_id, scenario_id[0]["id"], json.dumps(value))
                for param_id, value in params.items()
            )
            await conn.copy_records_to_table(
                "utilisateur_scenario_params",
                records=records,
                columns=["action_param", "scenario", "valeur"],
            )

            records = (
                (scenario_id[0]["id"], d["action"], json.dumps(d["params_avances"]))
                for d in advanced_params
            )
            await conn.copy_records_to_table(
                "utilisateur_scenario_params_advanced",
                records=records,
                columns=["scenario", "action", "params_avances"],
            )

            records = (
                (scenario_id[0]["id"], nom, json.dumps(valeurs))
                for nom, valeurs in trajectoires_cibles.items()
            )
            await conn.copy_records_to_table(
                "utilisateur_scenario_trajectoire_cible",
                records=records,
                columns=["scenario", "nom", "valeur"],
            )
    if scenario_id is None:
        return False
    else:
        return scenario_id[0]["id"]


async def verifie_scenario(scenario_id):
    """On vérifie l'existence et le droit de lecture pour une stratégie donnée

    Parameters
    ----------
    scenario_id : int
        L'identifiant du scénario

    Returns
    -------
    dict
    """
    query = """select id
       , mail
       , partage
       , publique
       , region
    from utilisateur_scenario
    where id = $1
    """
    scenario = await fetch(query, scenario_id)
    if not scenario:
        return None
    assert len(scenario) == 1
    return scenario[0]


async def get_scenario(scenario_id):
    """Récupère les paramètres pour un scénario donné.

    Parameters
    ----------
    scenario_id : int
        L'identifiant du scénario.
    partage : list
        Liste de mails des identifiants bénéficiaires.
    partage_par : str
        Identifiant du compte qui partage ce scénario.

    Returns
    -------
    dict
        Avec le titre, la description, la date et une clé "params" avec toutes les
        valeurs des paramètres.
    """
    query = """
        SELECT
            id, mail, titre, description,
            zone_type, zone_id, nom_territoire,
            actions,
            TO_CHAR(derniere_modif, 'DD/MM/YYYY HH24:MI:SS') AS derniere_modif,
            partage, partage_desactive, publique,
            reference_year
        FROM utilisateur_scenario
        WHERE id=$1
    """
    scenario = await fetch(query, scenario_id)
    if not scenario:
        return None
    query_params = """
        SELECT
            action_param AS id,
            p.action_number AS action,
            CONCAT(p.action_number, '_', p.nom) AS nom,
            p.label,
            u.valeur,
            p.unite
        FROM utilisateur_scenario_params AS u
        JOIN strategie_territoire.action_subactions AS p ON p.id=u.action_param
        WHERE scenario=$1
        ORDER BY action_param
    """
    rset = await fetch(query_params, scenario_id)
    result = dict(scenario[0])
    params = [dict(x) for x in rset]
    for param in params:
        param["valeur"] = json.loads(param["valeur"])
    result["params"] = params

    advanced_params = """
        SELECT action, params_avances
        FROM utilisateur_scenario_params_advanced
        WHERE scenario=$1
    """
    rset = await fetch(advanced_params, scenario_id)
    advanced = [dict(x) for x in rset]
    for item in advanced:
        item["params_avances"] = json.loads(item["params_avances"])
    result["advanced"] = advanced

    traj_cible = """
        SELECT nom, valeur
        FROM utilisateur_scenario_trajectoire_cible
        WHERE scenario=$1
    """
    rset = await fetch(traj_cible, scenario_id)
    result["trajectoires"] = {x["nom"]: json.loads(x["valeur"]) for x in rset}
    return result


async def region_scenarios(region):
    """Get all the scenarios for a given region

    Parameters
    ----------
    region : str
        Normalized region name.

    Returns
    -------
    list
        List of the scenarios for the region
    """
    query = """
    select distinct id
      , mail
      , titre
      , description
      , zone_type
      , nom_territoire
      , to_char(derniere_modif, 'YYYY-MM-DD"T"HH24:MI:SS') as derniere_modif
    from utilisateur_scenario us
    where region=$1;
    """
    scenario_list = await fetch(query, region)
    if not scenario_list:
        return None
    return [dict(x) for x in scenario_list]


async def user_scenario(region, login):
    """Récupère tous les scénarii d'un·e utilisateur·rice.

    Parameters
    ----------
    login : str
        L'identifiant du compte (i.e. mail).

    Returns
    -------
    list
        Liste de scénarii dont toutes les valeurs sont dans un dict.
    """
    # La requête est une union entre les scénarios du user courant,
    # mais aussi ceux qui ont été partagé avec le user, et qui n'ont pas été
    # désactivés par ce derniers, et pour finir les scénarios publiés
    query = """
    select id
      , mail
      , titre
      , description
      , zone_type
      , zone_id
      , nom_territoire
      , to_char(derniere_modif, 'DD/MM/YYYY HH24:MI:SS') as derniere_modif
      , partage
      , null as partage_par
      , publique
    from utilisateur_scenario
    where mail=$2 AND region = $1

    union all

    select id
      , mail
      , titre
      , description
      , zone_type
      , zone_id
      , nom_territoire
      , to_char(derniere_modif, 'DD/MM/YYYY HH24:MI:SS') as derniere_modif
      , null as partage -- on ne veux pas afficher le partage des autres pour l'utilisateur qui recoit le partage
      , mail as partage_par
      , publique
    from utilisateur_scenario
    -- on fait la différence des 2 tableaux et on regarde si le login en cours est contenu dedans
    where ( array(SELECT unnest(partage) EXCEPT SELECT unnest(partage_desactive)) @> ARRAY[$2] AND region = $1 )
        or ( publique AND region = $1 )
    order by publique,id
    """
    scenario_list = await fetch(query, region, login)
    return [dict(x) for x in scenario_list]


async def update_scenario(
    scenario_id: int,
    titre: str,
    description: str,
    reference_year: int,
    actions: list[str],
    params: dict[int, dict],
    advanced_params: list[dict],
    trajectoires_cibles: dict[str, dict],
):
    """Récupère les paramètres pour un scénarion donné.

    Parameters
    ----------
    scenario_id : int
        L'identifiant du scénario.
    titre : str
        Titre du scénario.
    description : str
        Description du scénario. Peut être None.
    params : list
        Liste de dict avec tous les paramètres action_params à enregistrer.
    advanced_params: dict
        Clé/valeur de tous les paramètres avancés pour chaque catégorie d'actions
    trajectoires_cibles: list[dict]
        Clé/valeur des 3 trajectoires cibles 'energie_produite', 'emission_ges', 'energie_economisee'

    Returns
    -------
    dict
        Avec le titre, la description, la date et une clé "params" avec toutes les
        valeurs des paramètres.
    """
    query = """update utilisateur_scenario
    set titre=$2,
        description=$3,
        actions=$4,
        reference_year=$5,
        derniere_modif=now()
    where id=$1
    """
    await execute(query, scenario_id, titre, description, actions, reference_year)
    query_params = """update utilisateur_scenario_params
    set valeur=$3
    where scenario=$1 and action_param=$2
    """
    for action_param, value in params.items():
        await execute(query_params, scenario_id, action_param, json.dumps(value))

    query_trajectoire_cible = """update utilisateur_scenario_trajectoire_cible
    set valeur = $3
    where scenario=$1 and nom=$2
    """
    for nom, valeurs in trajectoires_cibles.items():
        await execute(query_trajectoire_cible, scenario_id, nom, json.dumps(valeurs))

    records = (
        (scenario_id, d["action"], json.dumps(d["params_avances"]))
        for d in advanced_params
    )

    async with controller.db.acquire() as conn:
        async with conn.transaction():
            await conn.execute(
                """delete from utilisateur_scenario_params_advanced
                where scenario = $1""",
                scenario_id,
            )

            await conn.copy_records_to_table(
                "utilisateur_scenario_params_advanced",
                records=records,
                columns=["scenario", "action", "params_avances"],
            )


async def delete_scenario(region, scenario_id):
    """Supprime un scénario.

    Parameters
    ----------
    scenario_id : int
        L'identifiant du scénario.
    """
    query = """delete from utilisateur_scenario
    where id = $2 AND region = $1
    """
    await execute(query, region, scenario_id)


async def share_scenario(region, scenario_id, emails):
    """Partager un scénario avec d'autres utilisateurs

    Parameters
    ----------
    scenario_id : int
        L'identifiant du scénario.
    emails : list
        La liste des emails.
    """
    query = """update utilisateur_scenario
    set partage = $3
    where id = $2 AND region = $1
    """
    await execute(query, region, scenario_id, emails)


async def disable_scenario(region, user, scenario_id):
    """Désactiver le partage d'un scénario (par un bénéficiaire)

    Parameters
    ----------
    user : str
        L'identifiant du user qui désactive
    scenario_id : int
        L'identifiant du scénario.
    """
    query = """update utilisateur_scenario
    set partage_desactive = partage_desactive||$3
    where id = $2 AND region = $1
    """
    await execute(query, region, scenario_id, [user])


async def change_public_state_scenario(region, scenario_id, publique):
    """Change l'état de publication d'un scénario.

    Parameters
    ----------
    scenario_id : int
        L'identifiant du scénario.
    publique : boolean
        État actuel de la publication
    """
    query = """update utilisateur_scenario
    set publique = not $3
    where id = $2 AND region = $1
    """
    await execute(query, region, scenario_id, publique)
