# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""
api.py
====================================
The core module of the api which controls all actions.
"""

import datetime
import json
import os
import re
import shutil
import tempfile
import traceback
import urllib.request
from collections import defaultdict
from math import isnan

import asyncpg
import numpy as np
import pandas as pd
import slugify
from sanic import Blueprint, response
from sanic.log import LOGGING_CONFIG_DEFAULTS, logger
from sanic_jwt import BaseEndpoint, Initialize
from sanic_jwt.decorators import protected
from sanic_jwt.responses import _set_cookie

from terriapi import (
    __version__,
    auth,
    controller,
    exceptions,
    here,
    settings,
    user,
    utils,
)
from terriapi.controller import QueryBuilder
from terriapi.controller import actions as controller_actions
from terriapi.controller import analyse as controller_analyse
from terriapi.controller import didactic_files as controller_didactic
from terriapi.controller import (
    external_api,
    get_pg_engine,
    init_trajectory,
    mesure_audience,
)
from terriapi.controller import national as national_controller
from terriapi.controller import pcaet_ademe
from terriapi.controller import poi as controller_poi
from terriapi.controller import regions_configuration
from terriapi.controller import saisie_objectifs as controller_saisie_objectifs
from terriapi.controller import sankey, scenario, static_files, strategy_export
from terriapi.controller import suivi_trajectoire as controller_suivi_trajectoire
from terriapi.controller import table_datatypes
from terriapi.controller import tableau_bord as controller_tableau
from terriapi.controller import territorialsynthesis as controller_territorialsynthesis
from terriapi.controller import visiting_stats
from terriapi.controller import zone as controller_zone
from terriapi.controller.donnees_pour_representation import (
    DonneesPourRepresentations,
    FabriqueRepresentationsTableauBord,
)
from terriapi.controller.donnees_registre import (
    DonneesRegistre,
    obtenir_donnees_disponibles,
)
from terriapi.controller.gestion_images import ecriture_image_base64, list_path_file
from terriapi.controller.indicateur_tableau_bord import IndicateurTableauBord
from terriapi.controller.perimeters import liste_donnees_perimetres
from terriapi.controller.pop_up_accueil import (
    liste_images_carousel_avec_path,
    suppression_images_region_pop_up_accueil,
    update_actu_pop_up,
)
from terriapi.controller.simulator.factory_simulator import FactorySimulator
from terriapi.controller.strategy_actions import (
    Commodity,
    DataKeys,
    DataSet,
    RenewableProd,
    Sector,
    Usage,
    VehicleType,
    actions_dispatcher,
    meta_descriptor,
    passage_table,
)
from terriapi.controller.strategy_actions.jobs import FabriqueImpactEmploi
from terriapi.controller.strategy_actions.loaders import (
    SingleDataLoader,
    actions_data,
    specific_loaders,
)
from terriapi.controller.strategy_actions.passage_table import PassageTable
from terriapi.controller.strategy_actions.tests import testing as actions_testing
from terriapi.controller.user import (
    admins,
    auth_user,
    delete_refresh_token,
    send_mail,
    users,
)
from terriapi.integration import (
    convert_tables_to_new_perimeter,
    create_indicator,
    date_perimetre_donnee_ajoutee,
    get_current_perimeter_year,
    get_table_perimeter_year,
    inserer_nouvelle_categorie,
    integrer_donnee,
    liste_tables_donnees,
    obtenir_donnees_obligatoires_pour_maille_territoriales,
    obtenir_liste_tables_pour_indicateur,
    rebuild_all_geographic_perimeters,
    rebuild_supra_perimeters,
    update_all_data_to_current_perimeter,
    update_categorie,
    update_data,
    update_metadata_indicator,
    upload_svg,
    validation,
    verifier_integration_donnees_obligatoires_pour_maille_territoriales,
)
from terriapi.integration.validation import normalize


def geojson(record):
    """Transforme un Recode (asyncpg) en dict "GeoJSON"

    On suppose qu'on a une colonne 'geom' pour les features géométriques.

    :param record: asyncpg.Record
    :return dict: dictionnaire avec une structure "GeoJSON"
    """
    for row in record:
        yield {
            "type": "Feature",
            "properties": {k: v for k, v in row.items() if k != "geom"},
            "geometry": json.loads(row["geom"]),
        }


bp_v1 = Blueprint("api", url_prefix="/api")


@bp_v1.route(r"/version")
async def whatistheversion(request):
    return response.json({"version": __version__})


@bp_v1.route(r"/region")
async def region_list(request):
    """Récupère les settings liés aux régions"""
    # récupération du dictionnaire.
    retour = await regions_configuration.get_configuration(allow_national_regions=False)
    ref_index = 0
    # itération du dictionnaire pour transformer les valeurs binaires de la colonne actu
    # et les autres valeurs qui ne passent pas en json
    for i in retour:
        for key, value in i.items():
            if key == "actu":
                binary_string = retour[ref_index]["actu"]
                if binary_string is None:
                    continue
                binary_values = binary_string.split()
                string = ""
                for binary_value in binary_values:
                    an_integer = int(binary_value, 2)
                    string_character = chr(an_integer)
                    string += string_character
                retour[ref_index]["actu"] = string
            elif isinstance(value, datetime.datetime):
                retour[ref_index][key] = value.strftime("%d/%m/%Y, %H:%M:%S")
        ref_index += 1

    return response.json(retour)


@bp_v1.route(r"/<region>/config/<key>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_region_configuration(request, region: str, key: str):
    """Updates a colum from regions_configuration,"""
    if key not in ["auto_select_maille"]:
        return response.json({"message": "Clé de configuration inattendue"}, status=404)
    value = request.args.get("value")
    if value is None:
        return response.json({"message": "Le paramètre 'value' est requis"}, status=400)
    # Convert value to boolean.
    value = value == "true"
    await regions_configuration.update_configuration(region, key, value)
    return response.json({"message": "Paramètre mis à jour"}, status=200)


@bp_v1.route(r"/<region>/<code_region>/id_utilisateur")
@utils.is_valid_region()
async def obtenir_id_utilisateur(request, region: str, code_region: str):
    """Attribue à tout.e utilisateur.rice un identifiant grâce auquel on le/la suit tout le long de sa navigation.

    Parameters
    ----------
    region : chaine de caractère
        nom de la région dépourvu d'accents et de majuscules (ex : auvergne-rhone-alpes)
    code_region :
        code de la région
    """
    userdata = await auth_user(request, region)
    is_admin = userdata is not None and userdata.est_admin
    try:
        ip_client = request.remote_addr or request.ip
        if ip_client == "127.0.0.1" or ip_client is None:  # localhost
            mesureur_d_audience = mesure_audience.MesureDAudienceTerritoriale(
                region, "Inconnu", "Inconnu", donnees_ip=None
            )
            id_utilisateur = await mesureur_d_audience.inserer_donnees_localisation(
                is_admin
            )
        else:
            service_api_ip = urllib.request.urlopen(
                "http://www.geoplugin.net/json.gp?ip={}".format(ip_client)
            )
            reponse_json = service_api_ip.read().decode("utf-8")
            mesureur_d_audience = mesure_audience.MesureDAudienceTerritoriale(
                region, code_region, "region", donnees_ip=json.loads(reponse_json)
            )
            id_utilisateur = await mesureur_d_audience.inserer_donnees_localisation(
                is_admin
            )
    except urllib.error.URLError:
        mesureur_d_audience = mesure_audience.MesureDAudienceTerritoriale(
            region, "Inconnu", "Inconnu", donnees_ip=None
        )
        id_utilisateur = await mesureur_d_audience.inserer_donnees_localisation(
            is_admin
        )
    return response.json({"id_utilisateur": id_utilisateur})


@bp_v1.route(r"/<region>/analysis")
@utils.is_valid_region()
async def analysis(request, region: str):
    """Point d'entrée pour récupérer toutes les analyses de données
    possibles
    """
    is_authenticated = await request.app.ctx.auth.is_authenticated(request)
    only_active = True  # Précise si on veut récupérer toutes les analyses (admin)
    # ou seulement celles activées

    if is_authenticated:
        userdata = await auth_user(request, region)
        only_active = (
            not userdata.est_admin and not userdata.acces_indicateurs_desactives
        )

    analyses = await controller_analyse.analysis(region, only_active=only_active)

    for analysis in analyses:
        indicateur_pour_tableau_de_bord = IndicateurTableauBord(region, analysis["id"])
        caracteristiques_indicateur_courant = (
            await indicateur_pour_tableau_de_bord.meta_donnees_indicateur(analysis)
        )
        representations_possibles = caracteristiques_indicateur_courant[
            "representations_possibles"
        ]
        analysis["representations_possibles"] = representations_possibles
        # we parse and sort charts by ordre field
        analysis["charts"].sort(
            key=lambda x: x.get("ordre", 0) or 0 if x is not None else 0
        )
    return response.json(analyses)


@bp_v1.route(r"/<region>/analysis/list/confid/layers/")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def analysis_confid_layers(request, region: str):
    """Entrypoint to retrieve existing confid layers"""
    rset = await controller_analyse.list_confid_layers(region)
    resp = [dict(row) for row in rset]
    return response.json(resp)


@bp_v1.route(r"/<region>/analysis/uithemes")
@utils.is_valid_region()
async def analysis_ui_themes(request, region: str):
    """Point d'entrée pour récupérer les thèmes des indicateur"""
    rset = await controller_analyse.analysis_ui_themes(region)
    resp = [dict(row) for row in rset]
    return response.json(resp)


@bp_v1.route(r"/<region>/analysis/uitheme/order", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_analysis_ui_theme(request, region):
    """Mettre à jour l'ordre et le thème d'un indicteur dans le menu

    Parameters
    ----------
    themes_analyses_ordres : objet contenant la liste des thèmes, analyse et leur ordre
    themes_ordres : objet contenant la liste des thèmes (seuls) et leur ordre
    """
    if (
        not request.json
        or "themes_analyses_ordres" not in request.json
        or "themes_ordres" not in request.json
    ):
        return response.json(
            {
                "status": "error",
                "message": "Il manque des paramètres pour classer les indicateurs",
            },
            status=400,
        )

    # Il faut initialiser l'ordre de tous les indicateurs dans la base
    data_analyses = request.json["themes_analyses_ordres"]
    data_themes = request.json["themes_ordres"]

    await controller_analyse.update_analysis_ui_theme(data_analyses, region)
    await controller_analyse.update_themes_ui_theme(data_themes, region)
    return response.json({"status": "updated"}, status=200)


@bp_v1.route(r"/<region>/analysis/uitheme/rename", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def rename_analysis_ui_theme(request, region):
    """Mettre à jour l'ordre et le thème d'un indicteur dans le menu

    Parameters
    ----------
    old_name : ancien nom du groupe d'indicateurs
    new_name : nouveau nom du groupe d'indicateurs
    """
    if (
        not request.json
        or "old_name" not in request.json
        or "new_name" not in request.json
    ):
        return response.json(
            {
                "status": "error",
                "message": "Il manque des paramètres pour renommer la catégorie d'indicateurs",
            },
            status=400,
        )

    old_name = request.json["old_name"]
    new_name = request.json["new_name"]

    await controller_analyse.rename_analysis_ui_theme(old_name, new_name, region)
    return response.json({"status": "updated"}, status=200)


@bp_v1.route(r"/<region>/data/units/")
@utils.is_valid_region()
async def list_data_units(request, region: str):
    """Point d'entrée pour récupérer les thèmes des indicateur"""
    rset = await regions_configuration.get_data_units(region)
    return response.json(rset, status=200)


@bp_v1.route(
    r"/<region>/analysis/<id_indicateur:int>/graphique/<representation>",
    methods=["POST"],
)
@utils.is_valid_region()
async def donnees_pour_tableaux_de_bord(
    request, region, id_indicateur: int, representation
):
    """
    Liste les données pour un indicateur dans un tableau de bord.

    :param region: la région considérée
    :type region: str
    :param id_indicateur: l'ID de l'indicateur concerné
    :type id_indicateur: integer
    :param representation: la représentation sélectionnée parmi `line`, `pie`, `radar`,
        `jauge`, `jauge-circulaire`, `histogramme`, `histogramme-normalise`,
        `histogramme-data-ratio` et `marqueur-svg`
    :type representation: str
    """
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    maille = await utils.valid_territory_type(region, request.args.get("maille"))
    zone_id = request.args.get("zone_id")
    filtre_categorie = request.json
    if zone is None or maille is None or zone_id is None:
        return response.text("zone, maille et zone_id sont requis", status=400)
    indicateur_pour_tableau_de_bord = IndicateurTableauBord(region, id_indicateur)
    caracteristiques_indicateur = (
        await indicateur_pour_tableau_de_bord.meta_donnees_indicateur()
    )
    if not caracteristiques_indicateur:
        return response.json({"disabled": True, "message": "Indicateur supprimé"})
    if not caracteristiques_indicateur.get("active", False):
        return response.json({"disabled": True, "message": "Indicateur désactivé"})

    # if we need to specify some dynamic args to FabriqueRepresentation class
    kwargs = {}

    # handling unit specified through parameters
    unit = request.args.get("unit_id", None)
    unit = int(unit) if unit is not None else None
    unit_params = await controller_analyse.get_unit_from_analysis(
        region, unit, id_indicateur, maille
    )
    if "unit_name" in unit_params:
        kwargs["specific_unit_params"] = unit_params
        caracteristiques_indicateur["unit"] = unit_params["unit_name"]

    # defines and retrieves data
    donnees_representations_tableaux_bord = FabriqueRepresentationsTableauBord(
        caracteristiques_indicateur, maille, zone, zone_id, representation, **kwargs
    )
    if not donnees_representations_tableaux_bord.afficher_indicateur_maille:
        return response.json(
            {"disabled": True, "message": "Indicateur non activé à cette échelle"}
        )
    resultats = await donnees_representations_tableaux_bord.donnees_finales(
        "tableau_de_bord", filtre_categorie
    )
    return response.json(resultats)


@bp_v1.route(r"/<region>/analysis/<aid:int>/data", methods=["POST"])
@utils.is_valid_region()
async def data_analysis(request, region, aid: int):
    """Wrapper sur _data_analysis

    Parameters
    ------------
    region: str
        nom de la région de travail
    aid: int
        identifiant de l'indicateur
    zone : str
        territoire de visualisation (region|departement|epci...)
    zone_id : str
        identifiant de la zone de visualisation
    annee : int
        restreindre l'analyse aux données de cette année
    maille : str
        zone d'aggrégation des données
    """
    # zone géographique passée en query arg (maille de visualisation)
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    maille = await utils.valid_territory_type(region, request.args.get("maille"))
    # identifiant de la zone étudiée
    zone_id = request.args.get("zone_id")
    provenance = request.args.get("provenance")
    id_utilisateur = request.args.get("id_utilisateur")
    filtre = request.args.get("filtre")
    filtres_categorie = request.json
    reinitialiser_filtres = request.args.get("reinitialiserFiltres")

    if filtres_categorie is None:
        filtres_categorie = {}
    if not filtre:
        filtre = 0
    if not zone:
        return response.text("Type de Zone non reconnue", status=404)

    if not maille:
        return response.text("Maille non reconnue", status=404)

    if not zone_id:
        return response.text("Identifiant de zone non renseigné", status=404)

    if not provenance:
        return response.text(
            "Provenance du lancement de l'indicateur non renseignée", status=404
        )
    if not id_utilisateur or id_utilisateur == "undefined":
        return response.text(
            "Identifiant de l'utilisateur.rice suivi.e non indiqué", status=404
        )

    mesureur_d_audience = mesure_audience.MesureDAudienceIndicateurs(
        provenance, region, zone_id, zone, aid
    )
    await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)
    # on ajoute l'id "métier" du territoire dans la réponse
    annee = request.args.get("annee", None)
    annee = int(annee) if annee is not None else None

    indicateur = IndicateurTableauBord(region, aid)
    aconf = await indicateur.meta_donnees_indicateur()
    if not aconf:
        return response.text(f"L'analyse {aid} n'existe pas", status=404)

    # get unit
    unit = request.args.get("unit", None)
    unit = int(unit) if unit is not None else None
    unit_params = await controller_analyse.get_unit_from_analysis(
        region, unit, aid, maille
    )
    if "unit_name" in unit_params:
        aconf["unit"] = unit_params["unit_name"]

    only_active = True  # Précise si on veut récupérer toutes les analyses (admin)
    is_authenticated = await request.app.ctx.auth.is_authenticated(request)
    if is_authenticated:
        userdata = await auth_user(request, region)
        only_active = (
            not userdata.est_admin and not userdata.acces_indicateurs_desactives
        )

    if only_active and not aconf.get("active", False):
        return response.text(f"L'analyse {aid} n'est pas activée", status=401)

    donnees_pour_representations = DonneesPourRepresentations(
        aconf,
        maille,
        zone,
        zone_id,
        specific_year=annee,
        specific_unit_params=unit_params,
    )
    results = await donnees_pour_representations.donnees_finales(
        provenance, filtre, filtres_categorie, reinitialiser_filtres
    )
    if "export" in results:
        del results["export"]

    if isinstance(results, dict):
        return response.json(results)
    else:
        return results


@bp_v1.route(r"/<region>/analysis/<aid:int>/flow", methods=["POST"])
@utils.is_valid_region()
async def flow_analysis(request, region: str, aid: int):
    """
    récupération des données d'analyse de flux
    """
    schema = region.replace("-", "_")
    # zone géographique passée en query arg (maille de visualisation)
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    maille = await utils.valid_territory_type(region, request.args.get("maille"))
    # seuil de flux passé en query arg
    flowfilter = request.args.get("filtre", 250)
    # récupération de la zone d'agrégation
    maille_table = maille
    if maille == "commune":
        #  On a besoin des communes de la France entière
        maille_table = "commune_fr"
    if maille == "epci":
        #  On a besoin des epci de la France entière
        maille_table = "epci_fr"
    if maille == "departement":
        #  On a besoin des départements France entière
        maille_table = "departement_fr"

    if not zone:
        return response.text("Zone non reconnue", status=404)

    if not maille:
        return response.text("Maille non reconnue", status=404)

    # filtre maille
    zone_id = request.args.get("zone_id")

    # Récupération de la config de l'analyse demandée
    sql = """
        select
        a.data, a.type, jsonb_agg(c.* ORDER BY c.ordre) as charts
        from meta.indicateur a
        full join meta.chart c on c.indicateur = a.id
        where a.id = $1
        group by a.id, a.nom, a.data, a.type, a.color_start, a.color_end
    """

    rset = await controller.fetch(sql, aid)

    resp = [dict(x) for x in rset]
    # récupération de la table des indicateurs
    indicator = resp[0]["data"]

    if resp[0]["type"] != "flow":
        return response.text(
            "Problème d'identifiant, ceci n'est pas une analyse de flux", status=404
        )

    sql = QueryBuilder(indicator, schema)
    # jointure origine sur les communes
    sql.add_join("commune_fr", jointype="left join", on="a.commune = {}.code", index=0)
    # jointure destination sur les communes
    sql.add_join(
        "commune_fr", jointype="left join", on="a.commune_dest = {}.code", index=1
    )

    if maille != "commune" and (zone != maille or zone == "epci"):
        # ajout des jointures pour les mailles concernées (epci...)
        sql.add_join(
            maille_table,
            jointype="left join",
            on="ARRAY[a.commune] <@ {}.communes",
            index=0,
        )
        sql.add_join(
            maille_table,
            jointype="left join",
            on="ARRAY[a.commune_dest] <@ {}.communes",
            index=1,
        )
        sql.add_complex_having(
            [(indicator, 0)],
            expression="sum({0}.valeur) > ${{}}",
            values=[int(flowfilter)],
        )
    elif maille == "commune":
        sql.add_filter(indicator, "valeur", int(flowfilter), operator=">")
    if zone_id and zone != "region":
        # ajout du filtrage tu territoire concerné
        sql.add_join(
            zone, jointype="left join", on="ARRAY[a.commune] <@ {}.communes", index=0
        )
        sql.add_join(
            zone,
            jointype="left join",
            on="ARRAY[a.commune_dest] <@ {}.communes",
            index=1,
        )
        sql.add_complex_filter(
            [(zone, 0), (zone, 1)],
            expression="{0}.code = ${{}} or {1}.code = ${{}}",
            values=[zone_id, zone_id],
        )

    if maille != "commune":
        # si on est pas à la maille communale il faut agréger les valeurs par territoire
        sql.add_groupby(table=maille_table, column="x", index=0)
        sql.add_groupby(table=maille_table, column="y", index=0)
        sql.add_groupby(table=maille_table, column="x", index=1)
        sql.add_groupby(table=maille_table, column="y", index=1)
        sql.add_groupby(table=indicator, column="country_dest", index=0)
        sql.add_groupby(table=maille_table, column="nom", index=0)
        sql.add_groupby(table=maille_table, column="nom", index=1)
        sql.add_select(expression="round(sum(a.valeur)) as val")
    else:
        sql.add_select(expression="round(a.valeur) as val")

    sql.add_select(
        table=maille_table, expression="ARRAY[{0}.x, {0}.y] as orig", index=0
    )
    sql.add_select(
        table=maille_table, expression="ARRAY[{0}.x, {0}.y] as dest", index=1
    )
    sql.add_select(expression="a.country_dest")
    sql.add_select(table=maille_table, expression="{0}.nom as lab_o", index=0)
    sql.add_select(table=maille_table, expression="{0}.nom as lab_d", index=1)

    if zone == maille and zone != "epci":
        sql.add_filter(
            maille, "nom", "Null", operator="is not", index=0, as_param=False
        )
        sql.add_filter(
            maille, "nom", "Null", operator="is not", index=1, as_param=False
        )
        sql.add_complex_having(
            [(indicator, 0)],
            expression="sum({0}.valeur) > ${{}}",
            values=[int(flowfilter)],
        )

    mapset = await controller.fetch(sql.query, *sql.params)
    liste_val = [x["val"] for x in mapset]
    return response.json(
        {
            "map": [dict(x) for x in mapset],
            "total": {"val": None, "divider": None},
            "min_max_valeurs": {"min": min(liste_val), "max": max(liste_val)},
        }
    )


@bp_v1.route(r"/<region>/analysis/<aid:int>/activate")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def activer_analysis(request, region, aid: int):
    """Activation d'une analyse"""
    analysis_data = await controller_analyse.analysis(region, aid)
    if analysis_data == {}:
        return response.json(
            {"status": "error", "message": "Analyse non trouvée"}, status=404
        )
    await controller_analyse.activate_analysis(aid)
    # Update history
    utilisateur = await auth_user(request, region)
    action = "Activation d'un indicateur"
    schema = region.replace("-", "_")
    await controller.add_to_data_history(
        region, schema, utilisateur, action, analysis_data["nom"] + f" (id: {aid})"
    )

    return response.json({"status": "ok", "aid": aid})


@bp_v1.route(r"/<region>/analysis/<aid:int>/deactivate")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def desactiver_analysis(request, region, aid: int):
    """Désactivation d'une analyse"""
    analysis_data = await controller_analyse.analysis(region, aid)
    if analysis_data == {}:
        return response.json(
            {"status": "error", "message": "Analyse non trouvée"}, status=404
        )
    await controller_analyse.deactivate_analysis(aid)
    # Update history
    utilisateur = await auth_user(request, region)
    action = "Désactivation d'un indicateur"
    schema = region.replace("-", "_")
    await controller.add_to_data_history(
        region, schema, utilisateur, action, analysis_data["nom"] + f" (id: {aid})"
    )

    return response.json({"status": "ok", "aid": aid})


@bp_v1.route(r"/<region>/sankey/meta_donnees")
@utils.is_valid_region()
async def sankey_meta_donnees(request, region):
    """Retrieve regional Sankey diagrams meta data

    ------------
    region : str
        Region key
    """
    sankey_chosen = request.args.get("sankey")
    if sankey_chosen == "undefined" or sankey_chosen == "":
        sankey_chosen = None
    meta_donnees = await sankey.get_meta_data(region, sankey_chosen)
    if len(meta_donnees) > 0:
        meta_donnees = dict(meta_donnees[0])
        meta_donnees["division_factors"] = json.loads(
            meta_donnees.get("division_factors", "{}")
        )
        meta_donnees["division_units"] = json.loads(
            meta_donnees.get("division_units", "{}")
        )
        return response.json(meta_donnees)
    else:
        return response.text("Impossible de récupérer les métadonnées", status=404)


@bp_v1.route(r"/<region>/sankey/layout/<sankey_table>/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def sankeys_get_layout(request, region, sankey_table):
    """Retrieve the layout file from a Sankey diagram.

    ------------
    region : str
        Region key
    sankey_table : str
        Sankey slug (corresponding to data table)
    """
    meta_data = await sankey.get_meta_data(region, sankey_table)
    meta_data = [dict(i) for i in meta_data]
    if len(meta_data) == 0:
        return response.text("Diagramme de Sankey non trouvé.", status=404)
    meta_data = meta_data[0]

    # compute Sankey layout file path from sankey table slug
    main_path_to_files = settings.get("api", "upload_other_files", fallback="/tmp")
    filename = f"{main_path_to_files}/{region}_{slugify.slugify(sankey_table)}.json"
    data = {}
    with open(filename, "r") as f:
        data = json.load(f)
    return response.json(data)


@bp_v1.route(r"/<region>/sankey/layout/<sankey_table>/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def sankeys_update_layout(request, region, sankey_table):
    """Update layout file from Sankey diagram.

    ------------
    region : str
        Region key
    sankey_table : str
        Sankey slug (corresponding to data table)
    """
    if request.files is None:
        return response.json(
            {"message": "La mise à jour du template nécessite un nouveau template."},
            status=404,
        )

    meta_data = await sankey.get_meta_data(region, sankey_table)
    meta_data = [dict(i) for i in meta_data]
    if len(meta_data) == 0:
        return response.text("Diagramme de Sankey non trouvé.", status=404)
    meta_data = meta_data[0]

    # Defining path related to current Sankey layout json file
    main_path_to_files = settings.get("api", "upload_other_files", fallback="/tmp")
    filename = f"{main_path_to_files}/{region}_{slugify.slugify(sankey_table)}.json"

    # Retrieve and save json from query
    data_file = request.files.get("template")
    if data_file is None:
        return response.json(
            {"message": "La mise à jour du template nécessite un template non vide."},
            status=404,
        )

    with open(filename, "wb") as f:
        f.write(data_file.body)
    return response.json({"message": "La mise à jour du template a fonctionné."})


@bp_v1.route(r"/<region>/sankey/data/<sankey_table>/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def sankeys_update_data(request, region, sankey_table):
    """Update a Sankey data table.

    ------------
    region : str
        Region key
    sankey_table : str
        Sankey slug (corresponding to data table)
    """
    if request.files is None:
        return response.json(
            {"message": "La mise à jour des données nécessite de nouvelles données."},
            status=404,
        )
    meta_data = await sankey.get_meta_data(region, sankey_table)
    meta_data = [dict(i) for i in meta_data]
    if len(meta_data) == 0:
        return response.text("Diagramme de Sankey non trouvé.", status=404)
    meta_data = meta_data[0]

    data_file = request.files.get("data_file")
    if data_file is None:
        return response.json(
            {"message": "La mise à jour des données nécessite des données non vide."},
            status=404,
        )
    with tempfile.NamedTemporaryFile(mode="w+b", suffix=".csv") as fp:
        fp.write(data_file.body)
        fp.seek(0)
        try:
            res = await sankey.update_data(region, sankey_table, fp.name)
            if res is False:
                return response.json(
                    {
                        "message": "La mise à jour n'a écrit aucune donnée ! Vérifiez que la structure de la nouvelle table de donnée est bien valide"
                    },
                    status=400,
                )

        except BaseException as e:
            return response.json(
                {"message": "La mise à jour a échoué (" + str(e) + ")."}, status=400
            )

    return response.json(
        {"message": "La mise à jour du fichier de données a fonctionné."}
    )


@bp_v1.route(r"/<region>/sankey/update/<sankey_table>/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def sankeys_update_diagram(request, region, sankey_table):
    """Update a Sankey diagram metadata from json query data.

    ------------
    region : str
        Region key
    sankey_table : str
        Sankey slug (corresponding to data table)
    """
    if request.json is None:
        return response.json(
            {
                "message": "Aucune donnée n'a été fournie pour créer un diagramme de Sankey."
            },
            status=400,
        )

    meta_data = await sankey.get_meta_data(region, sankey_table)
    meta_data = [dict(i) for i in meta_data]
    if len(meta_data) == 0:
        return response.text("Diagramme de Sankey non trouvé.", status=404)
    meta_data = meta_data[0]

    # parsing json parameters
    meta_data["division_factors"] = json.loads(meta_data.get("division_factors", "{}"))
    meta_data["division_units"] = json.loads(meta_data.get("division_units", "{}"))
    try:
        await sankey.update_meta_data(region, sankey_table, request.json, meta_data)
    except BaseException as e:
        return response.json(
            {"message": "La mise à jour a échoué (" + str(e) + ")."}, status=400
        )
    return response.json({"message": "La mise à jour a fonctionné !"})


@bp_v1.route(r"/<region>/sankey/delete/<sankey_table>/", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def sankeys_delete_diagram(request, region, sankey_table):
    """Update a Sankey diagram metadata from json query data.

    ------------
    region : str
        Region key
    sankey_table : str
        Sankey slug (corresponding to data table)
    """
    meta_data = await sankey.get_meta_data(region, sankey_table)
    meta_data = [dict(i) for i in meta_data]
    if len(meta_data) == 0:
        return response.text("Diagramme de Sankey non trouvé.", status=404)

    try:
        await sankey.delete_diagram(region, sankey_table)
    except BaseException as e:
        return response.json(
            {"message": "La suppression a échoué (" + str(e) + ")."}, status=400
        )
    return response.json({"message": "La suppression a fonctionné !"})


@bp_v1.route(r"/<region>/sankey/new", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def sankeys_new_diagram(request, region):
    """Add a new Sankey diagram

    ------------
    region : str
        Region key
    """
    new_data = request.json
    if new_data is None:
        return response.json(
            {
                "message": "Aucune donnée n'a été fournie pour créer un diagramme de Sankey."
            },
            status=400,
        )
    if new_data.get("division_factors", {}) == "":
        new_data["division_factors"] = {}
    if new_data.get("division_units", {}) == "":
        new_data["division_units"] = {}

    if "data_table" not in new_data or "sankey_name" not in new_data:
        return response.text("Il manque des informations minimales.", status=400)
    await sankey.add_new_metadata(region, new_data)
    return response.json({"message": "La mise à jour a fonctionné !"})


@bp_v1.route(r"/<region>/sankey/list")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def sankeys_list(request, region):
    """Retrieve the list of all sankeys inside current region schema as well
    as the headers (5 lines) of associated data tables.

    ------------
    region : str
        Region key
    """
    meta_data = await sankey.get_meta_data(region, all=True)
    meta_data = [dict(i) for i in meta_data]

    # if we have any sankey diagram saved
    if len(meta_data) > 0:
        # we retrieve all data tables associated and parse json fields
        list_tables = []
        for _sankey in meta_data:
            list_tables.append(_sankey["data_table"])
            _sankey["division_factors"] = json.loads(
                _sankey.get("division_factors", "{}")
            )
            _sankey["division_units"] = json.loads(_sankey.get("division_units", "{}"))
        data_tables_headers = await controller.get_data_tables_headers(
            region, list_tables
        )
        # We add the headers to the dictionnary that gathers information about the
        # data tables
        for _sankey in meta_data:
            _sankey["data_header"] = data_tables_headers[_sankey["data_table"]]
    return response.json(meta_data)


@bp_v1.route(r"/<region>/sankey/get/")
@utils.is_valid_region()
async def get_sankey(request, region):
    """Get a single sankey

    Query Params
    ------------
    zone : str
        Nom de la zone
    zone_id : str
        Identifiant de la zone
    """
    # zone géographique passée en query arg (maille de visualisation)
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    # identifiant de la zone étudiée
    zone_id = request.args.get("zone_id")
    id_utilisateur = request.args.get("id_utilisateur")
    sankey_chosen = request.args.get("sankey")
    if not id_utilisateur:
        return response.text(
            "Identifiant de l'utilisateur.rice suivi.e non indiqué", status=404
        )
    if not zone:
        return response.text("Zone non reconnue", status=404)

    if not zone_id:
        return response.text("Identifiant de zone non renseigné", status=404)

    meta_data = await sankey.get_meta_data(region, sankey_chosen)
    sankey_data = {}
    if len(meta_data) == 1:
        if sankey_chosen is None:
            sankey_chosen = meta_data[0]["data_table"]
        geo_div_enabled = meta_data[0].get("geographical_levels_enabled", "")
        if zone not in geo_div_enabled:
            sankey_data = {"statut": "non disponible"}
        else:
            json = await sankey.generate_json(region, sankey_chosen, zone, zone_id)
            sankey_data = json
    else:
        sankey_data = {"statut": "configuration de sankey non trouvée"}

    return response.json(sankey_data)


@bp_v1.route(r"/<region>/analysis/suivi_trajectoire")
@utils.is_valid_region()
async def suivi_trajectoire(request, region):
    """Trajectoire (données historiques) de consommation énergétique et production d'énergie

    Query Params
    ------------
    zone : str
        Nom de la zone
    zone_id : str
        Identifiant de la zone
    """
    # zone géographique passée en query arg (maille de visualisation)
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    # identifiant de la zone étudiée
    zone_id = request.args.get("zone_id")
    id_utilisateur = request.args.get("id_utilisateur")
    sankey_chosen = request.args.get("sankey")
    if not id_utilisateur:
        return response.text(
            "Identifiant de l'utilisateur.rice suivi.e non indiqué", status=404
        )
    if not zone:
        return response.text("Zone non reconnue", status=404)

    if not zone_id:
        return response.text("Identifiant de zone non renseigné", status=404)

    try:
        data = await controller_suivi_trajectoire.suivi_trajectoire(
            region, zone, zone_id
        )
        if zone_id:
            mesureur_d_audience = mesure_audience.MesureDAudienceAnalysesTerritoriales(
                region, zone_id, zone, "suivi_trajectoire"
            )
            await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)
    except Exception as e:
        logger.exception(e)
        return response.json(
            {"erreur": f"Erreur dans l'URL (message : {e})"}, status=400
        )

    if sankey_chosen is not None:
        meta_data = await sankey.get_meta_data(region, sankey_chosen)
        if len(meta_data) == 1:
            if sankey_chosen is None:
                sankey_chosen = meta_data[0]["data_table"]
            geo_div_enabled = meta_data[0].get("geographical_levels_enabled", "")
            if zone not in geo_div_enabled:
                data["sankey"] = {"statut": "non disponible"}
            else:
                json = await sankey.generate_json(region, sankey_chosen, zone, zone_id)
                data["sankey"] = json
        else:
            data["sankey"] = {"statut": "configuration de sankey non trouvée"}
    else:
        data["sankey"] = {"statut": "type de sankey non précisé"}

    return response.json(data)


@bp_v1.route(r"/<region>/<table>/annees")
@utils.is_valid_region(True)
async def obtenir_annees_disponibles_tables(request, region, table):
    """Retourne la liste des années qui figurent dans l'historique d'une table à indiquer dans les paramètres

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    table : str
        Nom de la table où il faut aller chercher l'historque des données
    """
    schema = region.replace("-", "_")
    if table.startswith("DataSet."):
        passage_table = PassageTable(region)
        await passage_table.load((table,))
        table = passage_table.convert_table_key(table)
    else:
        is_valid = await utils.check_is_valid_table(schema, table)
        if not is_valid:
            return response.json({"message": "Table inexistante."}, status=404)

    req = """
          select distinct annee
          from {region}.{table}
          order by annee;
          """.format(
        region=schema, table=table
    )

    resp = await controller.fetch(req)
    res = [i["annee"] for i in resp]
    if not res:
        # TODO : WHERE TO STORE THIS VALUE ?
        res = [2023]
    return response.json(res)


@bp_v1.route(r"/<region>/supra/goals", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def add_supra_goal(request, region):
    """Intègre un objectif en base de données

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    """
    donnees = request.json
    resp = await controller_saisie_objectifs.ajout_objectif(region, donnees)
    return response.json(resp, status=resp["status"])


@bp_v1.route(r"/<region>/supra/goals")
@utils.is_valid_region()
async def get_all_supra_goals(request, region):
    """Retourne les caractéristiques de tous les objectifs (sans les données de projection)

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    """
    req = """
        SELECT sg.supra_goal_id, sg.annee_reference, sg.titre, sg.graphique, sg.couleur,
            sg.derniere_annee_projection, sg.filter, t.match
        FROM strategie_territoire.supra_goals sg
        JOIN strategie_territoire.passage_table t ON t.key = sg.graphique AND t.association_type = 'table'
        WHERE sg.region = $1 AND t.region = $1;
    """
    resp = await controller.fetch(req, region)
    res = [dict(i) for i in resp]
    return response.json(res)


@bp_v1.route(r"/<region>/supra/goals/<supra_goal_id:int>/", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_supra_goal(request, region, supra_goal_id: int):
    """Supprime en base de données un objectif

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    titre : str
        Titre de l'objectif à supprimer
    graphique : str
        Nom du graphique auquel s'appliquait l'objectif à supprimer
    """
    if not await controller_saisie_objectifs.check_goal_exists(
        region, int(supra_goal_id)
    ):
        return response.json(
            {"message": "Objectif inexistant !"},
            status=404,
        )
    await controller_saisie_objectifs.supprimer_objectif(region, int(supra_goal_id))
    return response.json(
        {"message": "Objectif supprimé !"},
        status=200,
    )


@bp_v1.route(r"/<region>/supra/goals/<supra_goal_id:int>/")
@utils.is_valid_region()
async def get_supra_goal_values(request, region: str, supra_goal_id: int):
    """Permet d'obtenir les méta-données, données et affectations territoriales d'un objectif"""
    req = """
        SELECT supra_goal_id, titre, graphique, annee_reference, couleur,
            derniere_annee_projection, description, filter
        FROM strategie_territoire.supra_goals
        WHERE supra_goal_id = $1 AND region = $2
    """
    resp = await controller.fetch(req, int(supra_goal_id), region)
    meta_data = [dict(i) for i in resp]
    if not meta_data:
        return response.json(
            {"message": "Objectif inexistant !"},
            status=404,
        )
    meta_data = meta_data[0]

    req = """
        SELECT sg.supra_goal_id, o.annee, o.valeur, o.annee_modifiee
        FROM strategie_territoire.supra_goals sg
        JOIN strategie_territoire.supra_goals_values o ON sg.supra_goal_id = o.supra_goal_id
        WHERE sg.supra_goal_id = $1
            AND sg.region = $2
        ORDER BY o.annee ASC
    """
    resp = await controller.fetch(req, int(supra_goal_id), region)
    values = [dict(i) for i in resp]

    req = """
          SELECT l.*
          FROM strategie_territoire.supra_goals_links l
          LEFT JOIN strategie_territoire.supra_goals p
          ON l.supra_goal_id = p.supra_goal_id
          WHERE p.supra_goal_id = $1 AND p.region = $2
      """
    resp = await controller.fetch(req, int(supra_goal_id), region)
    assignations = [dict(i) for i in resp]

    return response.json(
        {"goal_meta": meta_data, "goal_values": values, "assignations": assignations}
    )


@bp_v1.route(r"/<region>/supra/goals/<supra_goal_id:int>/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_supra_goal_data(request, region, supra_goal_id: int):
    """Met à jour un objectf (suppression des données puis réintégration)
    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    titre : str
       Titre de l'objectif à supprimer
    graphique : str
       Nom du graphique auquel s'applique l'objectif à supprimer
    """

    if not await controller_saisie_objectifs.check_goal_exists(
        region, int(supra_goal_id)
    ):
        return response.json(
            {"message": "Objectif inexistant !"},
            status=404,
        )
    donnees = request.json
    await controller_saisie_objectifs.supprimer_objectif(region, supra_goal_id)
    await controller_saisie_objectifs.ajout_objectif(region, donnees, supra_goal_id)
    return response.json({"message": "Objectif mis à jour"}, status=200)


@bp_v1.route(r"/<region>/zone")
@utils.is_valid_region(allowed_in_national_regions=True)
async def zones(request, region):
    """
    Point d'entrée pour récupérer la config des zones
    """
    schema = region.replace("-", "_")
    return response.json(await controller_zone.all_zones(schema))


@bp_v1.route(r"/<region>/order/zone", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def change_order_zone(request, region):
    """
    Change order of the zones in the schema zone table.
    """
    data = request.json
    if data is None:
        return response.json(
            {"message": "Aucune donnée n'a été fournie pour modifier une zone."},
            status=404,
        )

    name = data.get("name", None)
    direction = data.get("direction", None)

    if None in [name, direction]:
        return response.json(
            {"message": "Des données manquent dans la requête fournie."}, status=404
        )
    schema = region.replace("-", "_")

    existing_zones = await controller_zone.all_zones(schema, with_order=True)
    editing_existing = list(filter(lambda x: x["libelle"] == name, existing_zones))
    if len(editing_existing) != 1:
        return response.json(
            {"message": "Vous n'êtes pas en train de modifier une zone existante."},
            status=400,
        )
    current_position = editing_existing[0]["ordre"]
    new_position = current_position - 1 if direction == "up" else current_position + 1
    if new_position <= 0 or new_position > len(existing_zones):
        return response.json(
            {"message": "Vous ne pouvez effectuer ce déplacement de zone."}, status=400
        )

    await controller_zone.change_order_zone(
        schema, name, current_position, new_position
    )

    return response.json(
        {"message": "La modification a réussi et la zone a été réordonnée !"},
        status=200,
    )


@bp_v1.route(r"/<region>/toggle/zone", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def change_toggle_zone(request, region):
    """
    Change order of the zones in the schema zone table.
    """
    data = request.json
    if data is None:
        return response.json(
            {"message": "Aucune donnée n'a été fournie pour modifier une zone."},
            status=404,
        )

    name = data.get("name", None)
    if name is None:
        return response.json(
            {"message": "Des données manquent dans la requête fournie."}, status=404
        )
    schema = region.replace("-", "_")
    await controller_zone.toggle_zone(schema, name)

    return response.json(
        {
            "message": "La modification a réussi et l'affichage de la zone a été modifié !"
        },
        status=200,
    )


@bp_v1.route(r"/<region>/delete/zone/<slug>", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_zone(request, region, slug: str):
    """
    Change order of the zones in the schema zone table.
    """
    if slug is None:
        return response.json(
            {"message": "Des données manquent dans la requête fournie."}, status=404
        )
    schema = region.replace("-", "_")
    existing_zones = await controller_zone.all_zones(schema, with_order=True)
    editing_existing = list(
        filter(lambda x: x["nom"] + "-" + x["maille"] == slug, existing_zones)
    )
    if len(editing_existing) != 1:
        return response.json(
            {"message": "Vous n'êtes pas en train de supprimer une zone existante."},
            status=400,
        )
    current_position = editing_existing[0]["ordre"]
    name = editing_existing[0]["libelle"]

    await controller_zone.delete_zone(schema, name, current_position)

    return response.json(
        {"message": "La modification a réussi et la zone a été supprimée !"},
        status=200,
    )


@bp_v1.route(r"/<region>/add/zone", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def add_zone(request, region):
    """
    Entrypoint to edit a zone definition.
    """
    data = request.json
    if data is None:
        return response.json(
            {"message": "Aucune donnée n'a été fournie pour créer une zone."},
            status=404,
        )

    name = data.get("name", None)
    level = data.get("level", None)
    maille = data.get("maille", None)
    if None in [name, level, maille]:
        return response.json(
            {"message": "Des données manquent dans la requête fournie."}, status=404
        )

    schema = region.replace("-", "_")
    existing_zones = await controller_zone.all_zones(schema)
    not_overriding_existing = list(
        filter(lambda x: x["libelle"] == name, existing_zones)
    )
    if len(not_overriding_existing) > 0:
        return response.json(
            {
                "message": "Le nom fourni ne doit pas être celui d'un niveau géographique déjà présent."
            },
            status=400,
        )

    territorial_levels = await controller_zone.all_territorial_levels(schema)
    if level not in territorial_levels or maille not in territorial_levels:
        return response.json(
            {"message": "La maille et la zone géographique doivent être valides."},
            status=400,
        )

    updated = await controller_zone.add_zone(
        schema, name, level, maille, len(existing_zones) + 1
    )
    return response.json(
        {"message": "La modification a réussi et la zone a été ajoutée !"}, status=200
    )


@bp_v1.route(r"/<region>/edit/zone", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def edit_zone(request, region):
    """
    Entrypoint to edit a zone definition.
    """
    data = request.json
    if data is None:
        return response.json(
            {"message": "Aucune donnée n'a été fournie pour créer une zone."},
            status=404,
        )

    old_name = data.get("old_name", None)
    name = data.get("name", None)
    level = data.get("level", None)
    maille = data.get("maille", None)
    if None in [old_name, name, level, maille]:
        return response.json(
            {"message": "Des données manquent dans la requête fournie."}, status=404
        )

    schema = region.replace("-", "_")
    existing_zones = await controller_zone.all_zones(schema)
    editing_existing = list(filter(lambda x: x["libelle"] == old_name, existing_zones))
    if len(editing_existing) != 1:
        return response.json(
            {"message": "Vous n'êtes pas en train de modifier une zone existante."},
            status=400,
        )
    not_overriding_existing = list(
        filter(lambda x: x["libelle"] == name, existing_zones)
    )
    if old_name != name and len(not_overriding_existing) > 0:
        return response.json(
            {
                "message": "Le nom fourni ne doit pas être celui d'un niveau géographique déjà présent."
            },
            status=400,
        )

    territorial_levels = await controller_zone.all_territorial_levels(schema)
    if level not in territorial_levels or maille not in territorial_levels:
        return response.json(
            {"message": "La maille et la zone géographique doivent être valides."},
            status=400,
        )

    await controller_zone.edit_zone(schema, old_name, name, level, maille)
    return response.json(
        {"message": "La modification a réussi et la zone a été modifiée !"}, status=200
    )


@bp_v1.route(r"/<region>/territorial/levels")
@utils.is_valid_region()
async def get_territorial_levels(request, region):
    """
    Point d'entrée pour récupérer la config des zones
    """
    schema = region.replace("-", "_")
    return response.json(await controller_zone.all_territorial_levels(schema))


@bp_v1.route(r"/<region>/zone/<name:str>")
@utils.is_valid_region(True)
@utils.is_valid_tablename("region", "name")
async def zone_geographique(request, region, name):
    """
    Point d'entrée pour récupérer les zones géographiques
    """
    schema = region.replace("-", "_")
    if " " in name:
        return response.text("Zone non reconnue", status=404)

    sql = f"""
        select code, nom {", communes" if name != "commune" else ""}
        from "{schema}"."{name}"
        order by nom
    """

    rset = await controller.fetch(sql)
    return response.json([dict(x) for x in rset])


@bp_v1.route(r"/<region:str>/zone/<zone_type:str>/<zone_id:str>/geometry")
@utils.is_valid_region()
@utils.is_valid_tablename("region", "zone_type")
async def get_zone_geometry(request, region: str, zone_type: str, zone_id: str):
    """
    Point d'entrée pour récupérer les géometries d'une zone géographique
    """
    schema = region.replace("-", "_")

    sql = f"""
        select st_asgeojson(st_simplify(st_transform(geom,4326), 0.001), 5) as geom
        from "{schema}"."{zone_type}"
        where code=$1
    """
    rset = await controller.fetch(sql, zone_id)

    if not rset:
        return response.json(
            {"message": f"Zone code '{zone_id}' not found for type '{zone_type}'"},
            status=404,
        )

    return response.json(next(geojson(rset)))


@bp_v1.route(r"/<region>/poi/layer/consult/<nom_couche>/<cochee>")
@utils.is_valid_region()
async def suivi_consultation_poi(request, region: str, nom_couche: str, cochee: bool):
    """Instancie un mesureur d'audience afin d'assurer le suivi de consultations des couches de POI

    Parameters
    ----------
    region : str
        nom de la région dépourvu de majuscule et accent (ex : auvergne-rhone-alpes)
    nom_couche : str
        Nom de la couche POI
    cochee : booléen
        Booléen qui renvoie True si la couche a été activée et False si elle a été désactivée.
    """
    type_territoire = request.args.get("type_territoire")
    code_territoire = request.args.get("code_territoire")
    id_utilisateur = request.args.get("id_utilisateur")
    # TODO: ajouter l'ID de la couche
    mesureur_d_audience = mesure_audience.MesureDAudiencePoi(
        region, code_territoire, type_territoire, nom_couche, cochee
    )
    id = await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)
    return response.json({"id": id})


@bp_v1.route(r"/<region>/poi/layers")
@utils.is_valid_region()
async def get_all_poi_layers(request, region):
    """
    Entry point to retrieve all POI elements in current region.
    """
    schema = region.replace("-", "_")
    rset = await controller.fetch(
        f"""
        SELECT id, nom, label, couleur, modifiable, theme, type_installation,
        type_geom, statut, ancrage_icone, donnees_exportables, credits_data_sources,
        credits_data_producers, theme_order, order_in_theme
        FROM {schema}_poi.layer
        ORDER BY theme_order, order_in_theme, label
        """
    )
    poi_data = [dict(r) for r in rset]
    poi_layers_editable = [l["nom"] for l in poi_data if l["modifiable"]]
    rset = await controller_poi.get_layers_constraints(
        region,
        poi_layers_editable,
    )
    for r in rset:
        for layer in poi_data:
            if layer["nom"] == r["layer_name"]:
                layer["structure_constraints"] = json.loads(r["structure_constraints"])

    return response.json(poi_data)


@bp_v1.route(r"/<region>/poi/themes")
@utils.is_valid_region()
async def get_poi_themes(request, region: str):
    """
    Entrypoint to retrieve all POI themes in current region.
    """
    rset = await controller_poi.get_themes(region)
    resp = [dict(row) for row in rset]
    return response.json(resp)


@bp_v1.route(r"/<region>/poi/layers/order", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_poi_theme_order(request, region):
    """
    Update the order of POI themes and POI within themes.

    Parameters
    ----------
    themes_analyses_ordres : dict
        object containing list of POI inside each theme and its order
    themes_ordres : dict
        object containing themes list in the right order
    """
    if (
        not request.json
        or "themes_analyses_ordres" not in request.json
        or "themes_ordres" not in request.json
    ):
        return response.json(
            {
                "status": "error",
                "message": "Il manque des paramètres pour classer les indicateurs",
            },
            status=400,
        )

    # Il faut initialiser l'ordre de tous les indicateurs dans la base
    data_analyses = request.json["themes_analyses_ordres"]
    data_themes = request.json["themes_ordres"]

    await controller_poi.update_order_within_themes(data_analyses, region)
    await controller_poi.update_theme_order(data_themes, region)
    return response.json({"status": "updated"}, status=200)


@bp_v1.route(r"/<region>/poi/theme/rename", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def rename_poi_theme(request, region):
    """
    Rename a POI theme

    Parameters
    ----------
    old_name : str
        old name of the POIs group
    new_name : str
        new name of the POIs group
    """
    if (
        not request.json
        or "old_name" not in request.json
        or "new_name" not in request.json
    ):
        return response.json(
            {
                "status": "error",
                "message": "Il manque des paramètres pour renommer le thème d'indicateurs",
            },
            status=400,
        )

    old_name = request.json["old_name"]
    new_name = request.json["new_name"]

    await controller_poi.rename_theme(old_name, new_name, region)
    return response.json({"status": "updated"}, status=200)


@bp_v1.route(r"/<region>/poi/layer/export/<layer>")
@utils.is_valid_region()
async def get_poi_properties(request, region, layer):
    """
    Récupérer les propriétés d'une couche POI
    """
    main_schema = region.replace("-", "_")
    poi_schema = main_schema + "_poi"
    zone = request.args.get("zone") or None
    zone_id = request.args.get("zone_id") or None
    if zone and not await utils.check_is_valid_table(main_schema, zone):
        return response.json(
            {"layer": layer, "status": f"invalid parameter zone: {zone}"}, status=400
        )
    couche = await controller.fetch(
        f"""
        select nom, type_geom, donnees_exportables from {poi_schema}.layer
        where nom = $1
        """,
        layer,
    )
    if len(couche) == 0:
        return response.json({"layer": layer, "status": "not found"}, status=404)
    if not couche[0]["donnees_exportables"]:
        return response.json(
            {"layer": layer, "status": "layer not exportable"}, status=401
        )
    dict_installations = await controller_poi.obtenir_installation_et_geom(
        poi_schema, couche, main_schema, zone, zone_id
    )
    return response.json(dict_installations[couche[0]["nom"]], status=200)


@bp_v1.route(r"/<region>/poi/object/<layer>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.is_valid_tablename("region", "layer", schema_suffix="_poi")
async def update_poi_object(request, region, layer):
    """Mettre à jour un objet POI dans une couche.

    Parameters
    ----------
    id : int
    properties : json
        l'ensemble des données attributaires de l'élément POI à modifier
    x : float
    y : float
    """
    schema = region.replace("-", "_")
    userdata = await auth_user(request, region)
    rset = await controller.fetch(
        """
        SELECT id, nom, label, statut, modifiable from {region}_poi.layer
        WHERE nom = $1 AND modifiable is true
        """.format(
            region=schema
        ),
        layer,
    )
    if len(rset) == 0:
        return response.text("Couche POI non reconnue", status=404)

    record_id = request.json["id"]
    properties = request.json["properties"]
    if "Typologie" in properties:
        properties["type"] = properties["Typologie"]
    xlon = request.json.get("x", None)
    ylat = request.json.get("y", None)

    structure_constraints = await controller_poi.get_layers_constraints(
        region,
        layer,
    )
    try:
        await controller_poi.validate_contribution(structure_constraints, properties)
    except ValueError as e:
        return response.json(
            {
                "message": f"Les modifications ne respectent pas les contraintes en vigueur sur la couche d'équipement (champ {str(e)}).",
            },
            status=400,
        )

    success = await controller_poi.update_poi_contribution(
        region, userdata, layer, record_id, properties, (xlon, ylat)
    )
    if success:
        await notify_poi_modification(
            "Modification d'un objet", region, layer, record_id
        )

        return response.json(
            {"status": "updated", "id": record_id, "edit_id": success}, status=200
        )
    return response.json({"id": record_id, "status": "not found"}, status=404)


@bp_v1.route("/<region>/import/common/poi/layer/<layer_id>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def import_common_poi_layer(request, region, layer_id):
    """Copy a poi layer from common base to regional schema

    Parameters
    ----------
    region : str
        region name
    layer : str
        layer name
    """
    layer_id = int(layer_id)
    userdata = await auth_user(request, region)
    metadata = await controller.fetch(
        f"""
        SELECT nom, type_installation, type_geom
        FROM france_poi.layer
        WHERE id = $1
        """,
        layer_id,
    )
    if len(metadata) == 0:
        return response.json(
            {
                "message": f"Cette table n'existe pas.",
            },
            status=404,
        )
    metadata = metadata[0]

    force_update = request.args.get("forceUpdate") == "true"
    try:
        await controller_poi.import_layer(
            region, layer_id, metadata["nom"], userdata, force_update
        )
    except exceptions.ForceUpdateAllowedException as e:
        return response.json(
            {
                "message": f"This operation should not be performed but can be forced.",
            },
            status=403,
        )
    if metadata["type_geom"] == "Point":
        base_path_icons = settings.get("api", "upload_icones_svg") + "/"
        if len(metadata["type_installation"]) > 0:
            multi_icons = metadata["type_installation"].split(",")
            for modality in multi_icons:
                icon_source_path = f"{base_path_icons}/france/{metadata['nom']}_{slugify.slugify(modality)}.svg"
                icon_target_path = f"{base_path_icons}/{region}/{metadata['nom']}_{slugify.slugify(modality)}.svg"
                if os.path.isfile(icon_source_path):
                    shutil.copyfile(icon_source_path, icon_target_path)
        else:
            file_suffixes = ["", "_en_projet", "_legende"]
            for file_suffixe in file_suffixes:
                icon_source_path = (
                    f"{base_path_icons}/france/{metadata['nom']}{file_suffixe}.svg"
                )
                icon_target_path = (
                    f"{base_path_icons}/{region}/{metadata['nom']}{file_suffixe}.svg"
                )
                if os.path.isfile(icon_source_path):
                    shutil.copyfile(icon_source_path, icon_target_path)

    return response.json({"message": "Données importées"})


@bp_v1.route(r"/<region>/poi/object/<layer>", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.is_valid_tablename("region", "layer", schema_suffix="_poi")
async def create_poi_object(request, region, layer):
    """Création d'un POI pour une couche donnée

    Parameters
    ----------
    properties : json
        l'ensemble des données attributaires de l'élément POI à modifier
    x : float
    y : float
    """
    schema = region.replace("-", "_")
    userdata = await auth_user(request, region)
    rset = await controller.fetch(
        f"""
        SELECT id, nom, label, statut, modifiable from {schema}_poi.layer
        WHERE nom = $1 AND modifiable is true
        """,
        layer,
    )
    if len(rset) == 0:
        return response.text("Couche POI non reconnue", status=404)
    properties = request.json["properties"]
    if "Typologie" in properties:
        properties["type"] = properties["Typologie"]
    xlon = request.json["x"]
    ylat = request.json["y"]

    structure_constraints = await controller_poi.get_layers_constraints(
        region,
        layer,
    )
    try:
        await controller_poi.validate_contribution(structure_constraints, properties)
    except ValueError as e:
        return response.json(
            {
                "message": f"Les ajouts ne respectent pas les contraintes en vigueur sur la couche d'équipement (champ {str(e)}).",
            },
            status=400,
        )

    record_id = await controller_poi.create_poi_contribution(
        region, userdata, layer, properties, (xlon, ylat)
    )
    if record_id is not None:
        await notify_poi_modification("Ajout d'un objet", region, layer, record_id)
        return response.json(
            {"edit_id": record_id, "layer": layer, "status": "created"}, status=200
        )
    return response.json({"id": None, "status": "error", "layer": layer}, status=404)


@bp_v1.route(r"/<region>/poi/object/<layer>/<object_id>", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.is_valid_tablename("region", "layer", schema_suffix="_poi")
async def delete_poi_object(request, region, layer, object_id: int):
    """
    Delete a POI single object inside a layer.

    Parameters
    ----------
    id : json
    """
    schema = region.replace("-", "_")
    userdata = await auth_user(request, region)
    rset = await controller.fetch(
        """
        SELECT id, nom, label, statut, modifiable from {region}_poi.layer
        WHERE nom = $1 AND modifiable is true
        """.format(
            region=schema
        ),
        layer,
    )
    if len(rset) == 0:
        return response.text("Couche POI non reconnue", status=404)
    success = await controller_poi.delete_poi_object_contribution(
        region, userdata, layer, object_id
    )
    if success:
        await notify_poi_modification(
            "Suppression d'un objet", region, layer, object_id
        )
        return response.json(
            {"id": object_id, "layer": layer, "status": "deleted", "edit_id": success},
            status=200,
        )
    return response.json(
        {"message": "Désolé, la suppression a échoué pour une raison inconnue !"},
        status=404,
    )


@bp_v1.route(r"/<region>/admin/poi/contributions", methods=["GET"])
@utils.is_valid_region()
@protected()
async def poi_contributions(request, region):
    """Récupération de l'historique des modifications des couche POI."""
    userdata = await auth_user(request, region)
    is_admin = await utils.check_is_admin_in_region(userdata, region)
    if not is_admin and not userdata.can_validate_poi_contributions:
        return response.json(
            {
                "message": "Vous n'êtes pas autorisé·e à effectuer cette opération (non administrateur)."
            },
            status=401,
        )
    data = await controller_poi.get_poi_contributions(
        region, userdata.poi_contributions if not is_admin else "*"
    )
    return response.json(data)


@bp_v1.route(
    r"/<region>/admin/poi/contribution/<edit_id:int>/<action>/", methods=["POST"]
)
@utils.is_valid_region()
@protected()
async def manage_poi_contribution(request, region, edit_id, action):
    """Récupération de l'historique des modifications des couche POI."""
    userdata = await auth_user(request, region)
    is_admin = await utils.check_is_admin_in_region(userdata, region)
    if not is_admin and not userdata.can_validate_poi_contributions:
        return response.json(
            {
                "message": "Vous n'êtes pas autorisé·e à effectuer cette opération (non administrateur)."
            },
            status=401,
        )

    data = await controller_poi.get_poi_contribution(region, edit_id)
    if data is None:
        return response.json({"message": "Contribution introuvable"}, status=404)

    if not is_admin and data["poi_table"] not in userdata.poi_contributions:
        return response.json(
            {
                "message": "Vous n'êtes pas autorisé·e à effectuer cette opération (couche d'équipements non autorisée)."
            },
            status=401,
        )

    if data["state"] != "pending":
        return response.json(
            {
                "message": "Impossible de modifier une contribution déjà acceptée ou refusée."
            },
            status=404,
        )

    if action == "accepted":
        layer = data["poi_table"]
        properties = (
            json.loads(data["new_properties"])
            if data["new_properties"] is not None
            else {}
        )
        geom = data["new_geometry"]
        contribution_action = data["action"]
        record_id = data["poi_id"]
        res = None
        if contribution_action == "add":
            res = await controller_poi.create_poi(
                region, userdata, layer, properties, geom
            )
        elif contribution_action == "edit":
            res = await controller_poi.update_poi(
                region, userdata, layer, record_id, properties, geom
            )
        elif contribution_action == "delete":
            res = await controller_poi.delete_poi_object(
                region, userdata, layer, record_id
            )
        await controller_poi.update_contribution(region, edit_id, action)
        return response.json({"message": "Action accepté avec succès !", "result": res})
    elif action == "refused":
        await controller_poi.update_contribution(region, edit_id, action)
        return response.json({"message": "Action refusée avec succès !"})
    else:
        return response.json({"message": "Action invalide"}, status=404)


@bp_v1.route(r"/<region>/poi/layer/<layer>/rights/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def update_poi_edition_rights(request, region, layer):
    """
    Update the edition rights on a POI layer.

    Parameters
    ----------
    droit : boolean
    """
    schema = region.replace("-", "_")
    rset = await controller.fetch(
        f"""
        select id, nom, label from {schema}_poi.layer
        """
    )
    layers = [r["nom"] for r in rset]
    if layer not in layers:
        return response.text("Couche POI non reconnue", status=404)

    if not request.json or "droit" not in request.json:
        return response.text(
            "Nouveau statut de modification non accessible.", status=404
        )
    droit = request.json["droit"]
    userdata = await auth_user(request, region)
    success = await controller_poi.update_poi_edition_rights(
        region, layer, droit, userdata
    )
    if success:
        return response.json({"status": "updated"}, status=200)
    return response.json({"status": "not found"}, status=404)


@bp_v1.route(r"/<region>/poi/layer/add", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def create_poi_layer(request, region):
    """
    Add a new layer of POI.

    Parameters
    ----------
    droit : boolean
    """
    userdata = await auth_user(request, region)

    schema = region.replace("-", "_") + "_poi"

    metadata = {}
    metadata["nom"] = request.form["nomEquipement"][0]
    metadata["nom_table"] = utils.parse_table_name(request.form["nomTable"][0])
    metadata["couleur"] = request.form["couleur"][0]
    metadata["theme"] = request.form["nomRubrique"][0]
    metadata["type_geom"] = request.form["typeGeom"][0]
    metadata["statut"] = request.form["afficherStatut"][0].lower() == "true"
    metadata["ancrage_icone"] = (
        request.form["ancrageIcone"][0] if "ancrageIcone" in request.form else ""
    )
    metadata["donnees_exportables"] = (
        request.form["donneesExportables"][0].lower() == "true"
    )
    metadata["fichierPdfMethodo"] = request.files.get("fichierPdf")
    metadata["credits_data_sources"] = request.form["creditsDataSources"][0]
    metadata["credits_data_producers"] = request.form["creditsDataProducers"][0]

    # On crée un nom unique pour les fichiers de données et icones
    now = datetime.datetime.now()
    date_id = (
        "%d%02d%d_%02d%02d%02d%d"
        % (
            now.year,
            now.month,
            now.day,
            now.hour,
            now.minute,
            now.second,
            now.microsecond,
        )
    )[:-4]
    fichier_donnees = request.files.get("fichier")
    nom_fichier = "{}/{}_{}".format(settings.get("api", "upload"), region, date_id)
    try:
        with open(nom_fichier, "wb") as fd:
            fd.write(fichier_donnees.body)
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {nom_fichier}.")
        logger.error(f"Erreur: {str(e)}")
        return response.json(
            {
                "status": "error",
                "message": "Problème lors du téléchargement du fichier de données",
            },
            status=400,
        )

    base_path_icons = settings.get("api", "upload_icones_svg") + "/" + region + "/"
    if metadata["type_geom"] == "Point":
        multi_icons = set(
            key.removeprefix("iconsLabel")
            for key in request.form.keys()
            if key.startswith("iconsLabel")
        )
        typologies_modalities = set()
        for icon_id in multi_icons:
            category_modality = request.form["iconsLabel" + icon_id][0]
            icon_data = request.files.get("iconsFile" + icon_id)
            if not category_modality or not icon_data:
                continue
            icon_path = "{}/{}_{}.svg".format(
                base_path_icons,
                metadata["nom_table"],
                slugify.slugify(category_modality),
            )
            try:
                with open(icon_path, "wb") as fd:
                    fd.write(icon_data.body)
            except EnvironmentError as e:
                logger.error(f"Problème de téléchargement vers {icon_path}.")
                logger.error(f"Erreur: {str(e)}")
                return response.json(
                    {
                        "status": "error",
                        "message": "Problème lors du téléchargement d'un fichier icône",
                    },
                    status=400,
                )
            typologies_modalities.add(category_modality)
        metadata["typologies_modalities"] = ",".join(sorted(typologies_modalities))

        # normal case if no multiple data
        if len(typologies_modalities) == 0:
            icons_keys = ["fichierIcone", "fichierIconeProjet", "fichierIconeLegende"]
            file_suffixes = ["", "_en_projet", "_legende"]
            for icon_key, file_suffixe in zip(icons_keys, file_suffixes):
                file_data = request.files.get(icon_key)
                if not file_data:
                    continue
                has_upload_succeeded = upload_svg(
                    "{}/{}{}.svg".format(
                        base_path_icons, metadata["nom_table"], file_suffixe
                    ),
                    file_data,
                )
                if not has_upload_succeeded:
                    return response.json(
                        {
                            "status": "error",
                            "message": "Problème lors du téléchargement du fichier icône en projet",
                        },
                        status=400,
                    )

    # Appel à la fonction d'ajout de la couche équipement
    try:
        res = await controller_poi.create_layer(
            region, schema, metadata["nom_table"], metadata, userdata, nom_fichier
        )
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    return response.json({"message": "Couche équipement ajoutée"}, status=200)


@bp_v1.route(r"/<region>/poi/layer/<table_id>", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_poi_layer(request, region, table_id):
    """
    Entry point to delete a POI layer.
    """
    schema = region.replace("-", "_") + "_poi"
    layer = await controller.fetch(
        f"""select nom, type_geom, donnees_exportables from {schema}.layer where id = $1""",
        int(table_id),
    )
    if len(layer) == 0:
        return response.json({"status": "Couche d'équipements invalide."}, status=404)
    layer = dict(layer[0])

    # Appel à la fonction de suppression de la couche équipement
    result = await controller_poi.delete_poi_layer(schema, int(table_id), layer["nom"])
    suffixe = ""
    if result != True:
        suffixe = " (" + result + ")"
    return response.json(
        {"message": "Couche d'équipement supprimée" + suffixe}, status=200
    )


@bp_v1.route(r"/<region>/poi/layer/<table>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
@utils.is_valid_tablename("region", "table", schema_suffix="_poi")
async def update_poi_layer(request, region, table):
    """Point d'entrée pour mettre à jour les données d'une couche équipements

    Query Params
    ------------
    form : form
    """
    userdata = await auth_user(request, region)

    schema = region.replace("-", "_") + "_poi"

    metadata = {}
    metadata["couleur"] = request.form["couleur"][0]
    metadata["theme"] = request.form["nomRubrique"][0]
    metadata["label"] = request.form["nomEquipement"][0]
    metadata["nom_table"] = table
    metadata["type_geom"] = request.form["typeGeom"][0]
    metadata["fichierPdfMethodo"] = request.files.get("fichierPdf")
    metadata["statut"] = request.form["afficherStatut"][0].lower() == "true"
    metadata["ancrage_icone"] = (
        request.form["ancrageIcone"][0] if "ancrageIcone" in request.form else ""
    )
    metadata["donnees_exportables"] = (
        request.form["donneesExportables"][0].lower() == "true"
    )
    metadata["credits_data_sources"] = request.form["creditsDataSources"][0]
    metadata["credits_data_producers"] = request.form["creditsDataProducers"][0]

    # On crée un nom unique pour les fichiers de données et icones
    now = datetime.datetime.now()
    date_id = (
        "%d%02d%d_%02d%02d%02d%d"
        % (
            now.year,
            now.month,
            now.day,
            now.hour,
            now.minute,
            now.second,
            now.microsecond,
        )
    )[:-4]
    fichier_donnees = request.files.get("fichier")
    nom_fichier = None
    if fichier_donnees:
        nom_fichier = "{}/{}_{}".format(
            settings.get("api", "upload"), region, date_id, fichier_donnees.name
        )
        try:
            with open(nom_fichier, "wb") as fd:
                fd.write(fichier_donnees.body)
        except EnvironmentError as e:
            logger.error(f"Problème de téléchargement vers {nom_fichier}.")
            logger.error(f"Erreur: {str(e)}")
            return response.json(
                {
                    "status": "error",
                    "message": "Problème lors du téléchargement du fichier de données",
                },
                status=400,
            )

    multi_icons = set()
    typologies_modalities = set()
    if metadata["type_geom"] == "Point":
        multi_icons = set(
            key.removeprefix("iconsLabel")
            for key in request.form.keys()
            if key.startswith("iconsLabel")
        )
        for icon_id in multi_icons:
            category_modality = request.form["iconsLabel" + icon_id][0]
            typologies_modalities.add(category_modality)
        metadata["typologies_modalities"] = ",".join(sorted(typologies_modalities))

    # Appel à la fonction de mise à jour de la couche équipement
    try:
        res = await controller_poi.update_layer(
            region, schema, table, metadata, userdata, nom_fichier
        )
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    except asyncpg.exceptions.InvalidTextRepresentationError as e:
        return response.json({"status": "error", "message": str(e)}, status=400)
    except asyncpg.exceptions.PostgresSyntaxError as e:
        return response.json({"status": "error", "message": str(e)}, status=400)

    base_path_icons = settings.get("api", "upload_icones_svg") + "/" + region + "/"
    if metadata["type_geom"] == "Point":
        # normal case if no multiple data
        if len(typologies_modalities) == 0:
            icons_keys = ["fichierIcone", "fichierIconeProjet", "fichierIconeLegende"]
            file_suffixes = ["", "_en_projet", "_legende"]
            for icon_key, file_suffixe in zip(icons_keys, file_suffixes):
                file_data = request.files.get(icon_key)
                if not file_data:
                    continue
                has_upload_succeeded = upload_svg(
                    "{}/{}{}.svg".format(
                        base_path_icons, metadata["nom_table"], file_suffixe
                    ),
                    file_data,
                )
                if not has_upload_succeeded:
                    return response.json(
                        {
                            "status": "error",
                            "message": "Problème lors du téléchargement du fichier icône en projet",
                        },
                        status=400,
                    )
        else:
            for icon_id in multi_icons:
                category_modality = request.form["iconsLabel" + icon_id][0]
                icon_data = request.files.get("iconsFile" + icon_id)
                # if we have no data to update, we do nothing
                if not category_modality or not icon_data:
                    continue
                icon_path = "{}/{}_{}.svg".format(
                    base_path_icons,
                    metadata["nom_table"],
                    slugify.slugify(category_modality),
                )
                try:
                    with open(icon_path, "wb") as fd:
                        fd.write(icon_data.body)
                except EnvironmentError as e:
                    logger.error(f"Problème de téléchargement vers {icon_path}.")
                    logger.error(f"Erreur: {str(e)}")
                    return response.json(
                        {
                            "status": "error",
                            "message": "Problème lors du téléchargement d'un fichier icône",
                        },
                        status=400,
                    )

    return response.json(
        {"status": "success", "message": "Equipements mis à jour"}, status=200
    )


async def notify_poi_modification(sujet, region, layer, id):
    """ " Envoi d'un email à l'administrateur du site
    Pour le notifier qu'une modification sur les POI a été faite

    Parameters
    ----------
    sujet : string
    layer : string
    id : int
    """

    region_configuration = await regions_configuration.get_configuration(
        region, with_private=True
    )
    contact_email_region = region_configuration[0]["contact_email"]
    contact_email_response_to = region_configuration[0]["contact_email_response_to"]
    region_label = region_configuration[0]["label"]
    content = """
        Sujet: {sujet} sur la couche {layer}

        Une modification vient d'être apportée sur la plateforme.

        ID de l'objet : {id}
               Couche : {layer}

        Merci de consulter l'interface d'administration pour plus de détails.

        """.format(
        layer=layer, sujet=sujet, id=id
    )

    poi_contributions_contacts = (
        await regions_configuration.get_poi_contributions_managers(region, layer)
    )
    if poi_contributions_contacts:
        contacts_emails = [
            p["email"] for p in poi_contributions_contacts if p["is_receiving_mails"]
        ]
        logger.info("Send mails to contacts for POI edition")
        for mail in contacts_emails:
            await send_mail(
                contact_email_region,
                region_label,
                contact_email_response_to,
                mail,
                sujet,
                content,
                settings,
            )


@bp_v1.route(r"/<region>/poi/layers/history", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def historique_poi(request, region):
    """Récupération de l'historique des modifications des couche POI."""
    data = await controller_poi.historique_poi(region)
    return response.json(data)


@bp_v1.route(r"/<region>/update/strategie/template/", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_template_strategy_export(request, region):
    """Update the template file used in strategy export."""
    filedata = request.files.get("file")

    # On crée un nom unique pour les fichiers de données et icones
    filename = "{}/export-impacts-strategie-territoriale-{}.xlsx".format(
        settings.get("api", "upload_other_files", fallback="/tmp"), region
    )
    # Saving the uploaded file
    try:
        with open(filename, "wb") as fd:
            fd.write(filedata.body)
    except EnvironmentError:
        return response.json(
            {
                "status": "error",
                "message": "Problème lors du téléchargement du fichier de données",
            },
            status=400,
        )
    # Appel à la fonction d'ajout de la couche équipement
    try:
        env = settings.get("api", "env")
        res = await strategy_export.update_strategy_export_template_file(region, env)
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    return response.json(
        {"message": "Fichier mis à jour !", "date": res.strftime("%d/%m/%Y, %H:%M:%S")},
        status=200,
    )


@bp_v1.route(r"/<region>/get/strategie/template/")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_template_strategy_export(request, region):
    """Get the template file used in strategy export."""
    # filename from region
    filename = "{}/export-impacts-strategie-territoriale-{}.xlsx".format(
        settings.get("api", "upload_other_files", fallback="/tmp"), region
    )
    if not os.path.exists(filename):
        logger.warn(
            f"Use default export template at {strategy_export.EXCEL_EXPORT_IMPACTS} as previous file at {filename} doesn't exist."
        )
        filename = str(strategy_export.EXCEL_EXPORT_IMPACTS)
    mime_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    return await response.file(filename, status=200, mime_type=mime_type)


@bp_v1.route(r"/<region>/actions/old", methods=["GET"])
@utils.is_valid_region(True)
async def old_actions(request, region):
    """Retourne les actions possibles

    pour l'impact emploi et l'impact sur les énergies / émissions GES
    """

    schema = region.replace("-", "_")

    # zone géographique passée en query arg (maille de visualisation)
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    # identifiant de la zone étudiée
    zone_id = request.args.get("zone_id")

    activation_pcaet_ademe = request.args.get("pcaetAdeme")

    if not zone:
        return response.text("Zone non reconnue", status=404)

    if not zone_id:
        return response.text("Identifiant de zone non renseigné", status=404)

    rset = await actions_data.get_actions_list(region)
    actions = []
    for row in rset:
        dict_row = dict(row)
        reference_coeffs = {}
        if row["numero"] in ["16", "17"]:
            reference_coeffs = await controller_actions.get_percent_equipement_chauffage_residentiel_tertiaire(
                region, zone, zone_id, row["numero"], "api"
            )
            dict_row["coeffs_ref"] = reference_coeffs["coeffs_ref"]
            dict_row["annee_ref"] = reference_coeffs["annee_ref"]
        actions.append(dict_row)
        if row["numero"] in ["14", "15"]:
            if row["numero"] == "14":
                sector = Sector.INDUSTRY_WITHOUT_ENERGY
            elif row["numero"] == "15":
                sector = Sector.AGRICULTURE
            passage_table = PassageTable(region, "Consommation")
            await passage_table.load((DataSet.CONSUMPTION,))
            if passage_table.empty:
                continue
            table_name, filters = passage_table.get_query_params(
                DataKeys((DataSet.CONSUMPTION, sector))
            )
            data_loader = SingleDataLoader(region)
            await data_loader.load(
                table_name,
                zone,
                zone_id,
                passage_table.get_columns(DataSet.CONSUMPTION),
                filters,
            )

            # we save data inside current array
            data = passage_table.convert_data(DataSet.CONSUMPTION, data_loader.data)
            if not data.empty:
                total_consumption = round(data["valeur"].sum(), 2)
                actions[-1][
                    "description"
                ] += f"Total de la consommation énergétique ({int(data_loader.max_year)}) : {total_consumption} GWh. "
        if row["numero"] in ["16", "17"]:
            actions[-1][
                "description"
            ] += "La somme des différentes parts doit être égale à 100%"
        if row["numero"] == "1":
            actions[-1]["description"] += (
                "Pour chaque niveau de rénovation (cf. paramètres avancés), "
                "il a été pris comme hypothèse que l'effort de rénovation "
                "était majoritairement ciblé sur les classes G et F."
            )
        if row["numero"] in ["18", "19"]:
            # nombre d'actif en voiture sur le territoire
            (
                max_annee_nb,
                nombre_actifs_en_voiture,
            ) = await controller_suivi_trajectoire.nombre_actifs_en_voiture(
                region, zone, zone_id, dict_row["first_year"] - 1, 0
            )
            # distance_moyenne_par_actif_en_voiture pour un territoire
            (
                max_annee_dist,
                distance_moyenne_par_actif_en_voiture,
            ) = await controller_suivi_trajectoire.distance_moyenne_par_actif_en_voiture(
                region, zone, zone_id, dict_row["first_year"] - 1, 0
            )
            actions[-1]["description"] += (
                "Nombre d'actifs se déplaçant en voiture sur le territoire ("
                + str(max_annee_nb)
                + ") : "
                + str("{:,}".format(round(nombre_actifs_en_voiture)).replace(",", " "))
                + " "
                + "----------- Distance moyenne parcourue par actif sur le trajet domicile-travail ("
                + str(max_annee_dist)
                + ") : "
                + str(round(distance_moyenne_par_actif_en_voiture))
                + " km"
            )
        if row["numero"] == "2":
            actions[-1][
                "description"
            ] += "La somme des différentes parts doit être égale à 100%"
        if row["numero"] == "20":
            passage_table = PassageTable(region)
            fleet_table = DataSet.VEHICLE_FLEET
            res = await passage_table.load((fleet_table,))
            default_val = False
            if res:
                fleet_real_table = passage_table.convert_table_key(fleet_table)
                try:
                    vehicle_type_key = passage_table.convert_column_key(
                        VehicleType, fleet_table
                    )
                    schema = region.replace("-", "_")
                    req = f"""
                        select a.annee
                        , a.{vehicle_type_key}
                        , sum(valeur) as valeur
                        from {schema}.{fleet_real_table} as a
                        join {schema}.territoire as b on b.commune = a.commune
                        where b.type_territoire = $1
                        and b.code = $2
                        group by a.annee, a.{vehicle_type_key}
                        order by a.{vehicle_type_key}, a.annee
                        """
                    rset = await controller.fetch(req, zone, zone_id)
                    raw_data = pd.DataFrame([dict(x) for x in rset])
                    data = passage_table.convert_data(fleet_table, raw_data)
                    data = data.loc[(data.VEHICLETYPE == VehicleType.BUSES_COACHES)]
                    nombre_total_bus = float(
                        data.loc[(data.annee == max(data.annee)), "valeur"]
                    )
                except ValueError as e:
                    logger.warn("Use default value for bus fleet due to: " + str(e))
                    default_val = True

            if default_val:
                # le nombre de bus/car sur le territoire
                nombre_bus = await specific_loaders.nb_bus_cars(region, zone, zone_id)
                nombre_total_bus = next(
                    (x["nb"] for x in nombre_bus if x["energie"] == "total"), None
                )
            actions[-1][
                "description"
            ] += "Nombre de bus/cars existants sur le territoire : " + str(
                "{:,}".format(round(nombre_total_bus)).replace(",", " ")
            )
        if row["numero"] == "21":
            passage_table = PassageTable(region, "Emissions")
            await passage_table.load((DataSet.EMISSIONS,))
            if passage_table.empty:
                continue
            table_name, filters = passage_table.get_query_params(
                DataKeys(
                    (
                        DataSet.EMISSIONS,
                        Sector.INDUSTRY_WITHOUT_ENERGY,
                        Commodity.NON_ENERGETIC,
                    )
                )
            )
            data_loader = SingleDataLoader(region)
            await data_loader.load(
                table_name,
                zone,
                zone_id,
                passage_table.get_columns(DataSet.EMISSIONS),
                filters,
            )

            # we save data inside current array
            data = passage_table.convert_data(DataSet.EMISSIONS, data_loader.data)
            if not data.empty:
                total_emissions = round(data["valeur"].sum(), 2)
                actions[-1][
                    "description"
                ] += f"Total des émissions GES non-énergétiques dans le secteur industriel ({int(data_loader.max_year)}) : {total_emissions} kteqco2. "
            actions[-1][
                "description"
            ] += 'Modifier les autres paramètres pour identifier "CSC = Capture et Stockage de Carbone"'
        if row["numero"] == "22":
            passage_table = PassageTable(region, "Emissions")
            await passage_table.load((DataSet.EMISSIONS,))
            if passage_table.empty:
                continue
            table_name, filters = passage_table.get_query_params(
                DataKeys(
                    (DataSet.EMISSIONS, Sector.AGRICULTURE, Commodity.NON_ENERGETIC)
                )
            )
            data_loader = SingleDataLoader(region)
            await data_loader.load(
                table_name,
                zone,
                zone_id,
                passage_table.get_columns(DataSet.EMISSIONS),
                filters,
            )

            # we save data inside current array
            data = passage_table.convert_data(DataSet.EMISSIONS, data_loader.data)
            if not data.empty:
                total_emission_agri = round(data["valeur"].sum(), 2)
                actions[-1][
                    "description"
                ] += f"Total des émissions GES non-énergétiques dans le secteur agricole ({int(data_loader.max_year)}) : {total_emission_agri} kteqco2. "

    results = {"actions": actions}

    # we get regional configuration
    settings_regions = await regions_configuration.get_configuration(region)
    enable_history_actions_plan = settings_regions[0]["enable_history_actions_plan"]
    enable_relative_pcaet = settings_regions[0]["relative_pcaet"]
    enable_air_pollutants_impacts = settings_regions[0]["enable_air_pollutants_impacts"]

    trajectories = await init_trajectory.get_initial_trajectories(
        region,
        zone,
        zone_id,
        actions,
        enable_air_pollutants_impacts,
        enable_history_actions_plan,
    )
    results["trajectoires"] = trajectories["future"]
    results["historical_trajectories"] = trajectories["history"]
    results["pcaetAdemeIncorrect"] = "pcaet_ademe_vide"
    pcaet_ademe = await init_trajectory.get_pcaet_initial_trajectory(
        region,
        zone,
        zone_id,
        results["trajectoires"],
        trajectories["observatory_year"],
        enable_relative_pcaet,
    )
    # si le pcaet ademe existe, on renvoie les valeurs associés
    if pcaet_ademe is not None and activation_pcaet_ademe == "true":
        results["trajectoires"] = pcaet_ademe
        results["pcaetAdemeIncorrect"] = False
        results["retourEtatDefaut"] = False
    # retour à une valeur par défaut de pcaet
    elif pcaet_ademe is not None and activation_pcaet_ademe == "false":
        results["pcaetAdemeIncorrect"] = False
        results["retourEtatDefaut"] = True
    # liste des ojectifs références crée via les interfaces admins
    results["trajectoires_reference"] = await init_trajectory.get_trajectory_objectives(
        region, zone, zone_id
    )
    if pcaet_ademe is not None and activation_pcaet_ademe == "true":
        pcaet_reference = await init_trajectory.get_pcaet_objectives(
            region, zone, zone_id, enable_relative_pcaet
        )
        if pcaet_reference is not None:
            for key, value in pcaet_reference.items():
                results["trajectoires_reference"][key].append(value)

    return response.json(results)


@bp_v1.route(r"/<region>/actions", methods=["GET"], name="get_actions_list")
@utils.is_valid_region(True)
async def actions(request, region):
    """Get available actions in Territorial Strategies module and retrieve some helpful
    data relative to the given territory"""
    schema = region.replace("-", "_")
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")

    if not zone:
        return response.text("Zone non reconnue", status=400)
    if not zone_id:
        return response.text("Identifiant de zone non renseigné", status=400)

    rset = await actions_data.get_actions_list(region)

    actions = []
    for row in rset:
        dict_row = dict(row)
        reference_coeffs = {}
        if row["numero"] in ["16", "17"]:
            reference_coeffs = await controller_actions.get_percent_equipement_chauffage_residentiel_tertiaire(
                region, zone, zone_id, row["numero"], "api"
            )
            dict_row["coeffs_ref"] = reference_coeffs["coeffs_ref"]
            dict_row["annee_ref"] = reference_coeffs["annee_ref"]
        actions.append(dict_row)
        if row["numero"] in ["14", "15"]:
            if row["numero"] == "14":
                sector = Sector.INDUSTRY_WITHOUT_ENERGY
            elif row["numero"] == "15":
                sector = Sector.AGRICULTURE
            passage_table = PassageTable(region, "Consommation")
            await passage_table.load((DataSet.CONSUMPTION,))
            if passage_table.empty:
                continue
            table_name, filters = passage_table.get_query_params(
                DataKeys((DataSet.CONSUMPTION, sector))
            )
            data_loader = SingleDataLoader(region)
            await data_loader.load(
                table_name,
                zone,
                zone_id,
                passage_table.get_columns(DataSet.CONSUMPTION),
                filters,
            )

            # we save data inside current array
            data = passage_table.convert_data(DataSet.CONSUMPTION, data_loader.data)
            if not data.empty:
                total_consumption = round(data["valeur"].sum(), 2)
                actions[-1][
                    "description"
                ] += f"Total de la consommation énergétique ({int(data_loader.max_year)}) : {total_consumption} GWh. "
        if row["numero"] in ["16", "17"]:
            actions[-1][
                "description"
            ] += "La somme des différentes parts doit être égale à 100%"
        if row["numero"] == "1":
            actions[-1]["description"] += (
                "Pour chaque niveau de rénovation (cf. paramètres avancés), "
                "il a été pris comme hypothèse que l'effort de rénovation "
                "était majoritairement ciblé sur les classes G et F."
            )
        if row["numero"] in ["18", "19"]:
            # nombre d'actif en voiture sur le territoire
            (
                max_annee_nb,
                nombre_actifs_en_voiture,
            ) = await controller_suivi_trajectoire.nombre_actifs_en_voiture(
                region, zone, zone_id, dict_row["first_year"] - 1, 0
            )
            # distance_moyenne_par_actif_en_voiture pour un territoire
            (
                max_annee_dist,
                distance_moyenne_par_actif_en_voiture,
            ) = await controller_suivi_trajectoire.distance_moyenne_par_actif_en_voiture(
                region, zone, zone_id, dict_row["first_year"] - 1, 0
            )
            actions[-1]["description"] += (
                "Nombre d'actifs se déplaçant en voiture sur le territoire ("
                + str(max_annee_nb)
                + ") : "
                + str("{:,}".format(round(nombre_actifs_en_voiture)).replace(",", " "))
                + " "
                + "----------- Distance moyenne parcourue par actif sur le trajet domicile-travail ("
                + str(max_annee_dist)
                + ") : "
                + str(round(distance_moyenne_par_actif_en_voiture))
                + " km"
            )
        if row["numero"] == "2":
            actions[-1][
                "description"
            ] += "La somme des différentes parts doit être égale à 100%"
        if row["numero"] == "20":
            passage_table = PassageTable(region)
            fleet_table = DataSet.VEHICLE_FLEET
            res = await passage_table.load((fleet_table,))
            default_val = False
            if res:
                fleet_real_table = passage_table.convert_table_key(fleet_table)
                try:
                    vehicle_type_key = passage_table.convert_column_key(
                        VehicleType, fleet_table
                    )
                    schema = region.replace("-", "_")
                    req = f"""
                        select a.annee
                        , a.{vehicle_type_key}
                        , sum(valeur) as valeur
                        from {schema}.{fleet_real_table} as a
                        join {schema}.territoire as b on b.commune = a.commune
                        where b.type_territoire = $1
                        and b.code = $2
                        group by a.annee, a.{vehicle_type_key}
                        order by a.{vehicle_type_key}, a.annee
                        """
                    rset = await controller.fetch(req, zone, zone_id)
                    raw_data = pd.DataFrame([dict(x) for x in rset])
                    data = passage_table.convert_data(fleet_table, raw_data)
                    data = data.loc[(data.VEHICLETYPE == VehicleType.BUSES_COACHES)]
                    nombre_total_bus = float(
                        data.loc[(data.annee == max(data.annee)), "valeur"]
                    )
                except ValueError as e:
                    logger.warn("Use default value for bus fleet due to: " + str(e))
                    default_val = True

            if default_val:
                # le nombre de bus/car sur le territoire
                nombre_bus = await specific_loaders.nb_bus_cars(region, zone, zone_id)
                nombre_total_bus = next(
                    (x["nb"] for x in nombre_bus if x["energie"] == "total"), None
                )
            actions[-1][
                "description"
            ] += "Nombre de bus/cars existants sur le territoire : " + str(
                "{:,}".format(round(nombre_total_bus)).replace(",", " ")
            )
        if row["numero"] == "21":
            passage_table = PassageTable(region, "Emissions")
            await passage_table.load((DataSet.EMISSIONS,))
            if passage_table.empty:
                continue
            table_name, filters = passage_table.get_query_params(
                DataKeys(
                    (
                        DataSet.EMISSIONS,
                        Sector.INDUSTRY_WITHOUT_ENERGY,
                        Commodity.NON_ENERGETIC,
                    )
                )
            )
            data_loader = SingleDataLoader(region)
            await data_loader.load(
                table_name,
                zone,
                zone_id,
                passage_table.get_columns(DataSet.EMISSIONS),
                filters,
            )

            # we save data inside current array
            data = passage_table.convert_data(DataSet.EMISSIONS, data_loader.data)
            if not data.empty:
                total_emissions = round(data["valeur"].sum(), 2)
                actions[-1][
                    "description"
                ] += f"Total des émissions GES non-énergétiques dans le secteur industriel ({int(data_loader.max_year)}) : {total_emissions} kteqco2. "
            actions[-1][
                "description"
            ] += 'Modifier les autres paramètres pour identifier "CSC = Capture et Stockage de Carbone"'
        if row["numero"] == "22":
            passage_table = PassageTable(region, "Emissions")
            await passage_table.load((DataSet.EMISSIONS,))
            if passage_table.empty:
                continue
            table_name, filters = passage_table.get_query_params(
                DataKeys(
                    (DataSet.EMISSIONS, Sector.AGRICULTURE, Commodity.NON_ENERGETIC)
                )
            )
            data_loader = SingleDataLoader(region)
            await data_loader.load(
                table_name,
                zone,
                zone_id,
                passage_table.get_columns(DataSet.EMISSIONS),
                filters,
            )

            # we save data inside current array
            data = passage_table.convert_data(DataSet.EMISSIONS, data_loader.data)
            if not data.empty:
                total_emission_agri = round(data["valeur"].sum(), 2)
                actions[-1][
                    "description"
                ] += f"Total des émissions GES non-énergétiques dans le secteur agricole ({int(data_loader.max_year)}) : {total_emission_agri} kteqco2. "

    results = {"actions": actions}
    return response.json(results)


@bp_v1.route(r"/<region>/actions/params/reference", methods=["POST"])
@utils.is_valid_region(True)
async def get_and_post_actions(request, region):
    """Recharge les actions avec la répartition actualisée des équipement de chauffage de l'année de référence
    pour les actions 16 et 17 : conversion des équipements de chauffage dans le résidentiel et le tertaire
    si l'utilisateur change les autres paramètres avancés
    """
    # zone géographique passée en query arg (maille de visualisation)
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    # identifiant de la zone étudiée
    zone_id = request.args.get("zone_id")

    if not zone:
        return response.text("Zone non reconnue", status=404)

    if not zone_id:
        return response.text("Identifiant de zone non renseigné", status=404)

    actions = request.json["actions"]
    rslt = []
    advanced_params = request.json["advanced"]

    # reference user actions available for this region
    user_action_params = await actions_data.get_user_action_params(region)

    for row in actions:
        reference_coeffs = {}
        if row["numero"] in [
            "16",
            "17",
        ]:  # actions conversion équipement chauffage tertiaire
            reference_coeffs = await controller_actions.get_percent_equipement_chauffage_residentiel_tertiaire(
                region,
                zone,
                zone_id,
                row["numero"],
                "api_update",
                advanced_params,
            )
            row["coeffs_ref"] = reference_coeffs["coeffs_ref"]
            row["annee_ref"] = reference_coeffs["annee_ref"]
            rslt[-1][
                "description"
            ] = "La somme des différentes parts doit être égale à 100%"
        rslt.append(row)

        if row["numero"] in [
            "18",
            "19",
        ]:  # actions mobilité durable covoit et réduction distance trajet domicile-travail
            first_year_data = (
                await controller_actions.get_first_year_data_from_action(
                    region, row["numero"]
                )
                - 1
            )
            buffer_en_km = 0

            # we retrieve buffer value set by user
            if (row["numero"] == "19" and "19" in advanced_params) or (
                row["numero"] == "18" and "18" in advanced_params
            ):
                # we get the ID from the parameter corresponding to distance buffer
                id_param_buffer = int(
                    user_action_params[row["numero"]]["parametres_avances"]
                    .loc[
                        user_action_params[row["numero"]]["parametres_avances"]["nom"]
                        == "Seuil minimum de distance considéré pour les trajets domicile-travail"
                    ]
                    .index[0]
                )
                # we then get the value given by the user
                buffer_en_km = advanced_params[row["numero"]]["parametres_avances"].get(
                    id_param_buffer, 0
                )
            # nombre d'actif en voiture sur le territoire
            (
                max_annee_nb,
                nombre_actifs_en_voiture,
            ) = await controller_suivi_trajectoire.nombre_actifs_en_voiture(
                region, zone, zone_id, first_year_data, buffer_en_km
            )
            # distance_moyenne_par_actif_en_voiture pour un territoire
            (
                max_annee_dist,
                distance_moyenne_par_actif_en_voiture,
            ) = await controller_suivi_trajectoire.distance_moyenne_par_actif_en_voiture(
                region, zone, zone_id, first_year_data, buffer_en_km
            )
            description = (
                "Nombre d'actifs sur le territoire ("
                + str(max_annee_nb)
                + ") : "
                + str("{:,}".format(round(nombre_actifs_en_voiture)).replace(",", " "))
                + " "
                + "----------- Distance moyenne parcourue par actif sur le trajet domicile-travail ("
                + str(max_annee_dist)
                + ") : "
                + str(round(distance_moyenne_par_actif_en_voiture))
                + " km"
            )
            row["description"] = description

    results = {"actions": rslt}

    return response.json(results)


@bp_v1.route(r"/<region>/actions/subactions")
@utils.is_valid_region(True)
async def get_actions_parameters(request, region):
    """
    Retourne les paramètres d'action que l'utilisateur peut surcharger dans l'interface
    """
    results = await actions_data.get_subactions_list(region)
    return response.json(results)


@bp_v1.route(r"/<region>/actions/params/advanced")
@utils.is_valid_region(True)
async def get_actions_advanced_parameters(request, region):
    """
    Retourne les paramètres d'action avancés que l'utilisateur peut surcharger dans l'interface
    """

    # FIXME à simplifier pour n'envoyer qu'une liste action, phase_projet, maillon
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")
    if not zone:
        return response.text("Zone non reconnue", status=404)
    if not zone_id:
        return response.text("Identifiant de zone non renseigné", status=404)

    results = await actions_data.get_advanced_params_list(region, zone, zone_id)
    return response.json(results)


@bp_v1.route(r"/<region>/actions", methods=["POST"], name="post_actions")
@utils.is_valid_region(True)
async def post_actions(request, region):
    """Gère les actions POST

    Parameters
    ----------
    zone: str
        le territoire sélectionné
    zone_id: str
        l'identifiant du territoire
    post_payload: json
        liste des paramètres utilisaturs par actions et par année, ex:

        .. code-block:: python

            {
                "1_nb_logements": {2018: "20", 2019: "25", ...},
                "1_surface_moy_logement": {2018: "70", ...},
                "2_surface_bat_tertiaire": {2018: "10000", ...},
                "3_puiss_crete": {2018: "20", ...},
                "3b_puiss_install": {2018: "20", ...},
                "4_nb_batiment_collectif_renouv": {2018: "10", ...},
                "4_surf_capteurs_bati": {2018: "1000", ...},
                "5_fonctionnement_pleine_puissance": {2018: "4000", ...},
                "5_nb_chaufferies_bois": {2018: "1", ...},
                "5_nb_chaufferies_gaz": {2018: "0", ...},
                "5_puiss_moy_chaufferies_bois": {2018: "2", ...},
                "5_puiss_moy_chaufferies_gaz": {2018: "0", ...},
                "5_taille_reseau_chaleur": {2018: "10", ...},
                "5_taux_utilisation_energie_appoint": {2018: "0.25", ...},
                "6_nb_methaniseur": {2018: "1", ...},
                "6_puiss_moy_methaniseurs": {2018: "3", ...},
                "6b_nb_methaniseur": {2018: "1", ...},
                "6b_puiss_moy_methaniseurs": {2018: "3", ...},
                "7_longueur_bandes_cyclables": {2018: "50", ...},
                "7_longueur_pistes_cyclables": {2018: "20", ...},
                "actions": ["2018": [3", "6b"], ...]
                "advanced": {
                    "2": {
                    "gain_energetique": {
                        "0": 15,
                        "1": 40
                    },
                    "proportion_secteurs_renoves": {
                        "0": 20,
                        "1": 10,
                        "2": 10,
                        "3": 10,
                        "4": 10,
                        "5": 20,
                        "6": 20
                    }
                    },
                },
                "economique": {
                    "19": 13.7,
                    "20": 66.1,
                    "21": 66.1,
                    "22": 20.4,
                    "23": 20.4,...
            }


    Returns
    -------
    json:
        combinaison du nombre d'emplois + valeur ajoutée générés
        pour les impacts ponctuels/pérennes/direct/indirect
        de chaque année.
        La sortie contient également une clé 'mix'
        pour les calculs de mix énergétiques si présents sur l'année

        Par exemple :

        .. code-block::

            {2018: {direct: region: {ponctuel: {va_totale: 1.801, nb_emploi_total: 27},
                                    perenne: {va_totale: 0, nb_emploi_total: 0}},
                    indirect: {,…}},
                    mix: {,…}},
            2019: {direct: {,…}, indirect: {,…}},…}

    """

    config_region = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    config_region = config_region[0]
    is_national = config_region["is_national"]

    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")
    id_utilisateur = request.args.get("id_utilisateur")
    type_action = request.args.get("type_action")
    rounding = request.args.get("rounding", "true") == "true"
    if not type_action:
        type_action = "Lancement calcul"
    if zone is None or zone_id is None:
        return response.text("Zone et zone_id sont requis", status=400)
    if not id_utilisateur and not is_national:
        return response.text(
            "L'identifiant de l'utilisateur.rice suivi.e n'est pas indiqué.", status=400
        )

    if not request.json:
        # pas d'action renseignée
        return response.text(
            "Aucune action trouvée, contenu du POST invalide", status=404
        )

    # pour chaque année, la liste des actions (identifiants)
    # TODO(dag): ce sont les mêmes chaque année.
    action_year = dict(request.json["actions"].items())
    liste_traj_cibles = request.json["trajectoires_cibles"]
    liste_actions_cochees = []

    for annee in action_year:
        for action in action_year[annee]:
            if action not in liste_actions_cochees:
                liste_actions_cochees.append(action)

    if zone_id and zone and id_utilisateur and not is_national:
        mesureur_d_audience = mesure_audience.MesureDAudienceActions(
            region, zone_id, zone, liste_actions_cochees, liste_traj_cibles, type_action
        )
        await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)

    # liste des années renseignées dans l'interface
    years = set()
    for key, values in request.json.items():
        if key in ("actions", "economique", "advanced", "yearAdvancedParams"):
            continue
        if not isinstance(values, dict):
            continue
        for year in values.keys():
            years.add(year)

    action_params = defaultdict(dict)
    for key, values in request.json.items():
        if key in ("actions", "economique", "advanced", "yearAdvancedParams"):
            continue
        if not isinstance(values, dict):
            continue
        for year in years:
            action_params[key][year] = values.get(year, 0)

    # Advanced parameters with years
    advanced_params_years = {}
    if "yearAdvancedParams" in request.json:
        advanced_params_years = request.json["yearAdvancedParams"]

    # Les impacts liées aux actions : énergie (conso et enr), émission ges
    dispatcher = await controller_actions.compute_total_impact(
        region,
        zone,
        zone_id,
        years,
        action_year,
        action_params,
        request.json.get("advanced", {}),
        request.json.get("economique", {}),
        advanced_params_years=advanced_params_years,
        regional_config=config_region,
    )
    years = dispatcher.fixed_years
    # we execute
    outputs_by_action = await dispatcher.execute_all()
    impacts = await dispatcher.merge_outputs(outputs_by_action)
    # On parcourt chaque dictionnaire pour passer les DataFrame au format de graphe
    # attendu par l'application web

    for type_resultat in [
        "energie_economisee",
        "emission_ges",
        "covnm",
        "nh3",
        "sox",
        "nox",
        "pm10",
        "pm25",
    ]:
        if not impacts.get(type_resultat, False):
            continue
        for type_graphe in ("secteur", "energie"):
            data = impacts[type_resultat][type_graphe]
            if isinstance(data, str) and data == "Confidentiel":
                continue
            graphe = controller_suivi_trajectoire._graphe_format(data)
            impacts[type_resultat][type_graphe] = graphe
    # Même chose pour la prod EnR (il n'y a que les types d'énergie, pas les secteurs)
    data = impacts["energie_produite"]["energie"]
    if not data.empty:
        data = data.sort_values(["nom", "annee"])
    graphe = controller_suivi_trajectoire._graphe_format(data)
    impacts["energie_produite"]["energie"] = graphe
    if "vecteur" in impacts["energie_produite"]:
        data = impacts["energie_produite"]["vecteur"]
        if not data.empty:
            data = data.sort_values(["nom", "annee"])
        graphe = controller_suivi_trajectoire._graphe_format(data)
        impacts["energie_produite"]["vecteur"] = graphe

    action_year = defaultdict(list)
    has_heating_mix = {}
    has_thermal_solar_mix = {}
    for key, action in dispatcher.actions.items():
        if not action.disabled and len(action.enabled_years) > 0:
            for year in action.enabled_years:
                if key == "heating_network_mix":
                    has_heating_mix[year] = True
                elif key == "solar_thermal/ecs":
                    has_thermal_solar_mix[year] = True
                action_year[str(year)].append(action)

    for year in action_year:
        if has_heating_mix.get(year, False) and not has_thermal_solar_mix.get(
            year, False
        ):
            action_year[year].append(dispatcher.actions["solar_thermal/ecs"])

    mix_energetique = impacts["mix_energetique"]
    indice_consommation = impacts["indice_consommation"]
    difference_conso = impacts["difference_conso"]
    emission_co2_par_industrie = impacts["emission_co2_par_industrie"]
    emission_co2_par_sous_action = impacts["emission_co2_par_sous_action"]
    impact_emploi_obj = FabriqueImpactEmploi()
    results = {}

    results["impact_emplois"] = await impact_emploi_obj.calcule_impact_emploi(
        region,
        zone,
        zone_id,
        years,
        mix_energetique,
        indice_consommation,
        difference_conso,
        emission_co2_par_industrie,
        emission_co2_par_sous_action,
        action_year,
        dispatcher.inputs,
    )
    # on y ajoute les impacts énergie, émission GES & retombées fiscales.
    results["energie_economisee"] = impacts["energie_economisee"]
    results["energie_produite"] = impacts["energie_produite"]
    results["emission_ges"] = impacts["emission_ges"]
    if impacts["covnm"]:
        results["covnm"] = impacts["covnm"]
    if impacts["nh3"]:
        results["nh3"] = impacts["nh3"]
    if impacts["sox"]:
        results["sox"] = impacts["sox"]
    if impacts["nox"]:
        results["nox"] = impacts["nox"]
    if impacts["pm10"]:
        results["pm10"] = impacts["pm10"]
    if impacts["pm25"]:
        results["pm25"] = impacts["pm25"]
    results["facture_energetique"] = impacts["facture_energetique"].to_dict()
    # Conversion of values to negative values for a better understanding of the table in the case of a drop
    results["facture_energetique"] = {
        year: -value for year, value in results["facture_energetique"].items()
    }
    results["retombees_fiscales"] = impacts["retombees_fiscales"].to_dict()

    failed_actions = dispatcher.get_failed_actions()
    if len(failed_actions) > 0:
        results["failed_actions"] = failed_actions

    return response.json(results)


@bp_v1.route(r"/<region>/actions/export", methods=["POST"])
@utils.is_valid_region(True)
async def export_actions_excel(request, region):
    """Export Excel (format PCAET de l'ADEME)

    Des résultats des impacts énergie & émissions GES via les actions.

    Parameters
    ----------
    zone: str
        le territoire sélectionné
    zone_id: str
        l'identifiant du territoire
    post_payload: json
        liste des paramètres utilisaturs par actions et par année, ex:

        .. code-block:: python

            {
                "1_nb_logements": {2018: "20", 2019: "25", ...},
                "1_surface_moy_logement": {2018: "70", ...},
                "2_surface_bat_tertiaire": {2018: "10000", ...},
                "3_puiss_crete": {2018: "20", ...},
                "3b_puiss_install": {2018: "20", ...},
                "4_nb_batiment_collectif_renouv": {2018: "10", ...},
                "4_surf_capteurs_bati": {2018: "1000", ...},
                "5_fonctionnement_pleine_puissance": {2018: "4000", ...},
                "5_nb_chaufferies_bois": {2018: "1", ...},
                "5_nb_chaufferies_gaz": {2018: "0", ...},
                "5_puiss_moy_chaufferies_bois": {2018: "2", ...},
                "5_puiss_moy_chaufferies_gaz": {2018: "0", ...},
                "5_taille_reseau_chaleur": {2018: "10", ...},
                "5_taux_utilisation_energie_appoint": {2018: "0.25", ...},
                "6_nb_methaniseur": {2018: "1", ...},
                "6_puiss_moy_methaniseurs": {2018: "3", ...},
                "6b_nb_methaniseur": {2018: "1", ...},
                "6b_puiss_moy_methaniseurs": {2018: "3", ...},
                "7_longueur_bandes_cyclables": {2018: "50", ...},
                "7_longueur_pistes_cyclables": {2018: "20", ...},
                "actions": ["2018": [3", "6b"], ...]
                "advanced": {
                    "2": {
                    "gain_energetique": {
                        "0": 15,
                        "1": 40
                    },
                    "proportion_secteurs_renoves": {
                        "0": 20,
                        "1": 10,
                        "2": 10,
                        "3": 10,
                        "4": 10,
                        "5": 20,
                        "6": 20
                    }
                    },
                },
                "economique": {
                    "19": 13.7,
                    "20": 66.1,
                    "21": 66.1,
                    "22": 20.4,
                    "23": 20.4,...
            }


    Returns
    -------
    json:
        combinaison du nombre d'emplois + valeur ajoutée générés pour
        les impacts ponctuels/pérennes/direct/indirect de chaque année.
        La sortie contient également une clé 'mix'
        pour les calculs de mix énergétiques si présents sur l'année

        Par exemple :

        .. code-block::

            {2018: {direct: region: {ponctuel: {va_totale: 1.801, nb_emploi_total: 27},
                perenne: {va_totale: 0, nb_emploi_total: 0}},
                indirect: {,…}},
                mix: {,…}},
            2019: {direct: {,…}, indirect: {,…}},…}
    """

    config_region = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    config_region = config_region[0]
    ges_export_excel = config_region["ges_export_excel"]
    is_national = config_region["is_national"]

    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")
    type_action = request.args.get("type_action")
    if not type_action:
        type_action = "Export ADEME"
    id_utilisateur = request.args.get("id_utilisateur")
    if zone is None or zone_id is None:
        return response.text("Zone et zone_id sont requis", status=400)
    if not id_utilisateur and not is_national:
        return response.text(
            "L'identifiant de l'utilisateur.rice suivi.e n'est pas indiqué.", status=400
        )

    # liste des années renseignées dans l'interface
    years = set()

    action_params = defaultdict(dict)

    if not request.json:
        # pas d'action renseignée
        return response.text(
            "Aucune action trouvée, contenu du POST invalide", status=404
        )

    # pour chaque année, la liste des actions (identifiants)
    # TODO(dag): ce sont les mêmes chaque année.
    action_year = dict(request.json["actions"].items())
    liste_traj_cibles = request.json["trajectoires_cibles"]
    liste_actions_cochees = []
    for annee in action_year:
        for action in action_year[annee]:
            if action not in liste_actions_cochees:
                liste_actions_cochees.append(action)
    mesureur_d_audience = mesure_audience.MesureDAudienceActions(
        region, zone_id, zone, liste_actions_cochees, liste_traj_cibles, type_action
    )
    if not is_national:
        await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)
    for key, values in request.json.items():
        if key in (
            "actions",
            "economique",
            "advanced",
            "trajectoires",
            "trajectoires_cibles",
            "yearAdvancedParams",
        ):
            continue
        # key -> nom des actions. Valeur paire année/valeur des paramètres pour chaque action
        if isinstance(values, dict):
            for year, value in values.items():
                years.add(year)
                action_params[key][year] = value

    # Advanced parameters with years
    advanced_params_years = {}
    if "yearAdvancedParams" in request.json:
        advanced_params_years = request.json["yearAdvancedParams"]

    # Les impacts liées aux actions : énergie (conso et enr), émission ges
    meta_analyses = await controller_analyse.analysis(region)
    conf_polluants = [
        x for x in meta_analyses if x["ui_theme"] == "Polluants atmosphériques"
    ]
    polluants = []
    if conf_polluants:
        polluants = await controller_actions.diagnostic_polluants_atmospheriques(
            region, zone, zone_id
        )

    # Les impacts liées aux actions : énergie (conso et enr), émission ges
    dispatcher = await controller_actions.compute_total_impact(
        region,
        zone,
        zone_id,
        years,
        action_year,
        action_params,
        request.json.get("advanced", {}),
        request.json.get("economique", {}),
        advanced_params_years=advanced_params_years,
        regional_config=config_region,
    )
    years = dispatcher.fixed_years
    # we execute
    outputs_by_action = await dispatcher.execute_all()
    # as we have removed passage data, we will probably have multiple rows for certain
    # commodities depending on passage table complexity => need to sum inside export_excel function
    impacts = await dispatcher.merge_outputs(
        outputs_by_action, remove_passage_data=False
    )

    # convert kteqco2 to teqco2
    impacts["emission_ges"]["secteur"].loc[:, "valeur"] *= 1000
    impacts["emission_ges"]["energie"].loc[:, "valeur"] *= 1000

    mime_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    with tempfile.NamedTemporaryFile(mode="w+b") as fp:
        await controller_actions.export_excel(
            region, impacts, polluants, fp.name, ges_export_excel
        )
        return await response.file(
            fp.name, status=200, mime_type=mime_type, filename="export-pcaet-ademe.xlsx"
        )


@bp_v1.route(r"/<region>/strategie/results/export", methods=["POST"])
@utils.is_valid_region(True)
async def export_results_territorial_strategy(request, region):
    """
    Export Excel (results of territorial strategies)
    """

    # regions configuration
    settings_regions = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    is_national = settings_regions[0]["is_national"]
    export_excel_results = settings_regions[0]["export_excel_results"]
    if not export_excel_results:
        return response.text("L'export de résultat n'est pas activé", status=400)

    # args
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")
    id_utilisateur = request.args.get("id_utilisateur")

    if zone is None or zone_id is None:
        return response.text("Zone et zone_id sont requis", status=400)
    if not id_utilisateur and not is_national:
        return response.text(
            "L'identifiant de l'utilisateur.rice suivi.e n'est pas indiqué.", status=400
        )
    # missing data
    if not request.json:
        return response.text(
            "Aucune action trouvée, contenu du POST invalide", status=404
        )

    mesureur_d_audience = mesure_audience.MesureDAudienceActions(
        region, zone_id, zone, [], [], "Export results"
    )
    if not is_national:
        await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)

    # retrieves results
    results = request.json.get("results", {})
    # missing data
    if len(results) == 0:
        return response.text("Aucun résultat fourni", status=400)

    # retrieves trajectories
    trajectories = request.json.get("trajectories", {})

    # retrieves parameters
    parameters = request.json.get("parameters", {})

    # handles metadata
    title = request.json.get("title", "")
    source = request.json.get("source", "")
    territory_name = request.json.get("territory_name", "")
    contact_email = settings_regions[0].get("contact_email", "")
    now = datetime.datetime.now()
    date_export = "%d %02d %d" % (now.day, now.month, now.year)
    metadata = {
        "title": title,
        "territory_name": territory_name,
        "source": source,
        "date_export": date_export,
        "contact_email": contact_email,
    }

    mime_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    with tempfile.NamedTemporaryFile(mode="w+b") as fp:
        strategy_export.export_excel_results(
            region, parameters, results, trajectories, fp.name, metadata
        )

        date_file = "%d%02d%d" % (now.year, now.month, now.day)

        return await response.file(
            fp.name,
            status=200,
            mime_type=mime_type,
            filename=f"{date_file}_export_impact_strategie_territoriale_{zone}.xlsx",
        )


@bp_v1.route(r"/<region>/actions/export_params", methods=["POST"])
@utils.is_valid_region()
async def export_strategies_params(request, region):
    """
    Exporte les paramètres d'une stratégie territoriale
    """
    df = pd.DataFrame(
        [
            {
                "id": x["id"],
                "nom": x["nom"],
                "cochee": x["cochee"],
                "numero_action": x["action"],
                "unite": x["unite"],
                "valeur": x["valeur_annee"][e],
                "annee": e,
            }
            for x in request.json
            for e in x["valeur_annee"]
        ]
    )

    # on utilise un dossier temporaire pour éviter des problèmes
    # de concurrence de fichiers et des problèmes de permissions.
    mime_type = "text/csv"
    with tempfile.NamedTemporaryFile(mode="w+b") as fp:
        df.to_csv(fp.name, sep=";", index=False)
        return await response.file(
            fp.name, status=200, mime_type=mime_type, filename="export_parametres.csv"
        )


@bp_v1.route(r"/<region>/strategie/import", methods=["POST"])
@utils.is_valid_region()
async def import_parametres_strategie(request, region):
    """Retourne le contenu du fichier importé au format JSON."""
    fichierDonnees = request.files.get("fichier")
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    nomFichier = "{}/{}/{}_{}".format(
        settings.get("api", "upload"), region, dateId, fichierDonnees.name
    )
    # Saving the uploaded file

    with open(nomFichier, "wb") as fd:
        fd.write(fichierDonnees.body)

    df = pd.read_csv(nomFichier, sep=";", encoding="utf-8")
    da = {}

    for f in df.to_dict("records"):
        if f["nom"] not in da.keys():
            da[f["nom"]] = {"valeur_annee": {}}
            da[f["nom"]]["unite"] = f["unite"]
            da[f["nom"]]["action"] = f["numero_action"]
            da[f["nom"]]["cochee"] = f["cochee"]
            da[f["nom"]]["id"] = f["id"]
        da[f["nom"]]["valeur_annee"][f["annee"]] = int(f["valeur"])

    donnees_finales = []
    for i in da:
        unite = da[i]["unite"]
        if isinstance(unite, (float, int)) and isnan(unite):
            unite = ""
        donnees_finales.append(
            {
                "nom": i,
                "unite": unite,
                "action": da[i]["action"],
                "cochee": da[i]["cochee"],
                "valeur_annee": da[i]["valeur_annee"],
            }
        )

    return response.json(donnees_finales, status=200)


@bp_v1.route(r"/<region>/territorialsynthesis/notes/<zone:str>/<zone_id:str>")
@utils.is_valid_region()
async def territorialsynthesis_notes(request, region, zone, zone_id):
    schema = region.replace("-", "_")
    id_utilisateur = request.args.get("id_utilisateur")
    if not id_utilisateur:
        return response.text("Identifiant de l'utilisateur.rice suivi.e non indiqué")
    resp = await controller_territorialsynthesis.get_territorialsynthesis_notes(
        schema, zone, zone_id
    )
    if zone_id and len(resp) != 0:
        mesureur_d_audience = mesure_audience.MesureDAudienceAnalysesTerritoriales(
            region, zone_id, zone, "territorialsynthesis"
        )
        await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)

    return response.json([dict(r) for r in resp])


@bp_v1.route(r"/<region>/tableau", methods=["POST"])
@utils.is_valid_region()
@protected()
async def create_dashboard(request, region):
    """Création d'un nouveau tableau de bord pour une région donnée.

    Voici un exemple de la structure de la requête appliquées

    .. code-block:: python

        {
            "titre": "mon super tableau de bord",
            "description": "une description",
            "zone": "departement",
            "thematiques": [
            {
                "description": "description,"
                "titre": "énergie renouvelable",
                "graphiques": [
                    {"indicateur_id": 12,
                    "type": "pie",
                    "categories": ["secteur", "energie"]},
                    {"indicateur_id": 10,
                    "type": "pie",
                    "categories": ["usage", "energie"]}
                ]
                },
            ...
            ]
        }

    Returns
    ---------
    id
        l'entier correspondant au tableau créé
    """

    current_user = await auth_user(request, region)
    titre = request.json["titre"]

    description = request.json.get("description", "")
    description = utils.sanitize(description)
    thematiques = request.json["thematiques"]
    try:
        rset = await controller_tableau.create_dashboard(
            titre, current_user, description, region, thematiques
        )
    except asyncpg.exceptions.UniqueViolationError:
        return response.json(
            {
                "status": "error",
                "message": "Un tableau de bord existe déjà pour vous avec ce nom.",
            },
            status=400,
        )
    except PermissionError as e:
        return response.json({"status": "error", "message": str(e)}, status=401)
    return response.json({"tableau_id": rset["id"]})


@bp_v1.route(r"/<region>/tableau/<id_tableau_bord:int>", methods=["POST"])
@utils.is_valid_region()
@protected()
async def update_dashboard(request, region, id_tableau_bord):
    """Mise à jour d'un tableau de bord."""
    current_user = await auth_user(request, region)
    titre = request.json["titre"]
    description = utils.sanitize(request.json.get("description", ""))
    thematiques = request.json["thematiques"]
    try:
        rset = await controller_tableau.update_dashboard(
            id_tableau_bord, titre, current_user, description, region, thematiques
        )
    except asyncpg.exceptions.UniqueViolationError:
        return response.json(
            {"status": "error", "message": "Ce tableau de bord existe déjà."},
            status=400,
        )
    except PermissionError as e:
        return response.json({"status": "error", "message": str(e)}, status=401)
    return response.json({"tableau_id": rset["id"]})


@bp_v1.route(r"/<region>/tableau/<id_tableau_bord>", methods=["DELETE"])
@utils.is_valid_region()
@protected()
async def delete_dashboard(request, region, id_tableau_bord: int):
    """Delete a user's dashboard."""
    utilisateur = await auth_user(request, region)
    rset = await controller_tableau.delete_dashboard(
        id_tableau_bord, utilisateur, region
    )
    if rset:
        return response.json({"tableau_id": rset["id"]})
    else:
        return response.json(
            {"message": "La suppression de ce tableau de bord a échoué."}, status=400
        )


@bp_v1.route(r"/<region>/my/dashboards")
@utils.is_valid_region()
async def list_user_dashboards(request, region):
    """
    Retrieve the list of dashboards for current user (if connected).
    """
    utilisateur = await auth_user(request, region)
    if utilisateur is not None:
        rset = await controller_tableau.liste_tableau(utilisateur, region)
        return response.json(rset)
    return response.json({"statut": "Non connecté"})


@bp_v1.route(r"/<region>/public/dashboards")
@utils.is_valid_region()
async def list_public_dashboards(request, region):
    """
    Retrieve the list of dashboards published on a specific type of territory
    or on a territory
    """
    type_territoire = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")
    rset = await controller_tableau.list_public_dashboards(
        region, type_territoire, zone_id
    )
    return response.json(rset)


@bp_v1.route(r"/<region>/tableau/<dashboard_id:int>")
@utils.is_valid_region()
async def get_dashboard(request, region, dashboard_id):
    """Récupération d'un tableau de bord donné via son identifiant."""
    current_user = await auth_user(request, region)
    type_territoire = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")
    rset = await controller_tableau.get_dashboard(
        region, dashboard_id, current_user, type_territoire, zone_id
    )
    if rset is None:
        return response.json(
            {"message": "Ce tableau de bord n'existe pas."}, status=404
        )
    return response.json(rset)


@bp_v1.route(r"/<region>/tableau/dupliquer/<dashboard_id:int>", methods=["POST"])
@utils.is_valid_region()
@protected()
async def duplicate_dashboard(request, region, dashboard_id):
    current_user = await auth_user(request, region)
    title = request.json["title"]

    try:
        rset = await controller_tableau.duplicate_dashboard(
            region, current_user, dashboard_id, title
        )
    except asyncpg.exceptions.UniqueViolationError:
        return response.json(
            {
                "status": "error",
                "message": "Un tableau de bord existe déjà pour vous avec ce nom.",
            },
            status=400,
        )
    except PermissionError:
        return response.json(
            {
                "status": "error",
                "message": "Vous ne pouvez créer un tableau avec des éléments réservés aux administrateurs.",
            },
            status=401,
        )
    if rset is None:
        return response.json(
            {"message": "Ce tableau de bord n'existe pas."}, status=404
        )
    return response.json(rset)


@bp_v1.route(r"/<region>/tableau/<identifiant_tableau_bord>/liste/affectations")
@utils.is_valid_region()
async def list_dashboard_assignments(request, region, identifiant_tableau_bord):
    tableaux = await controller_tableau.list_dashboard_assignments(
        int(identifiant_tableau_bord)
    )
    return response.json({"liste_affectations": tableaux})


@bp_v1.route(r"/<region>/tableau/<dashboard_id:int>/affectation", methods=["PUT"])
@utils.is_valid_region()
@protected()
async def assign_zone_to_dashboard(request, region, dashboard_id):
    """Affecter une zone territoire à un tableau de bord."""
    userdata = await auth_user(request, region)
    zone = request.json["zone"]
    code_insee_territoire = None
    if "zone_id" in request.json.keys():
        code_insee_territoire = request.json["zone_id"]
    usermail = userdata.mail

    # on vérifie que l'utilisateur a bien le droit de modifier ce tableau
    is_tableau_user = await controller_tableau.is_dashboard_owner(
        usermail, dashboard_id, region
    )
    if not is_tableau_user:
        return response.json(
            {"message": "Vous n'êtes pas autorisé·e à modifier ce tableau de bord."},
            status=401,
        )
    # on affecte ensuite
    rset = await controller_tableau.assign_zone(
        dashboard_id, zone, code_insee_territoire
    )
    if not rset:
        return response.json(
            {
                "message": "L'affectation pour cette zone n'a pas fonctionné (déjà existante ?)."
            },
            status=400,
        )
    return response.json({"message": "Tableau de bord publié."})


@bp_v1.route(r"/<region>/tableau/<dashboard_id:int>/depublication", methods=["DELETE"])
@utils.is_valid_region()
@protected()
async def remove_dashboard_assignment(request, region, dashboard_id):
    """
    Dépublie un tableau pour une certaine échelle et un ID

    :param request: sanic request
    :type request: SanicRequest
    :param region: the region considered
    :type region: str
    :param dashboard_id: the table ID
    :type dashboard_id: int
    :return: json response
    :rtype: SanicResponse
    """
    current_user = await auth_user(request, region)
    is_tableau_user = await controller_tableau.is_dashboard_owner(
        current_user.mail, dashboard_id, region
    )
    if not is_tableau_user:
        return response.json(
            {"message": "Vous n'êtes pas autorisé·e à modifier ce tableau de bord."},
            status=401,
        )

    type_territoire = await utils.valid_territory_type(region, request.args.get("zone"))
    code_insee_territoire = None
    if "zone_id" in request.args.keys():
        code_insee_territoire = request.args.get("zone_id")
    liste_affectations = await controller_tableau.remove_dashboard_assignment(
        int(dashboard_id), type_territoire, code_insee_territoire
    )
    if liste_affectations == False:
        return response.json(
            {"message": "Aucun tableau de bord à dépublier."}, status=400
        )
    return response.json(
        {
            "liste_affectations": liste_affectations,
            "message": "Tableau de bord dé-publié",
        },
        status=200,
    )


@bp_v1.route(r"/<region>/tableau/<dashboard_id:int>/share", methods=["POST"])
@utils.is_valid_region()
@protected()
async def share_dashboard(request, region, dashboard_id):
    """Share a dashboard with other users."""
    if not request.json:
        return response.json(
            {"message": "Il manque des données dans la requête."}, status=400
        )

    # Le partage du TdB ne peut être effectué que par son auteur ou par un admin.
    current_user = await auth_user(request, region)
    is_tableau_user = await controller_tableau.is_dashboard_owner(
        current_user.mail, dashboard_id, region
    )
    if not (is_tableau_user or current_user.est_admin):
        return response.json(
            {"message": "Vous n'êtes pas autorisé·e à partager ce tableau de bord."},
            status=401,
        )

    # On vérifie que chaque email correspond bien à un login
    emails = [email.lower() for email in request.json["emails"].split()]
    logins = []
    users_ = await users()
    logins = [user["mail"].lower() for user in users_]
    for email in emails:
        if email not in logins:
            return response.json(
                {
                    "message": "Des emails sont inexistants dans la base des utilisateurs"
                },
                status=400,
            )

    is_readonly_share = request.json.get("is_readonly_share", False)

    await controller_tableau.share_dashboard(dashboard_id, emails, is_readonly_share)

    # On envoie un email à chaque bénéficiaire
    region_configuration = await regions_configuration.get_configuration(
        region, with_private=True
    )
    contact_email_region = region_configuration[0]["contact_email"]
    contact_email_response_to = region_configuration[0]["contact_email_response_to"]
    region_label = region_configuration[0]["label"]
    url = "https://{}.{}.terristory.fr".format(
        region_configuration[0]["url"], region_configuration[0]["env"]
    )
    if region_configuration[0]["env"] == "prod":
        url = "https://{}.terristory.fr".format(region_configuration[0]["url"])

    for email in emails:
        subject = "[TerriSTORY] Partage de tableau de bord"
        content = f"""Bonjour,

Un partage de tableau de bord vient d'être effectué sur la plateforme TerriSTORY par {current_user.mail}

Vous pouvez dès à présent la consulter dans votre espace personnel : {url}.

--
L'équipe TerriSTORY
        """

        await send_mail(
            contact_email_region,
            region_label,
            contact_email_response_to,
            email,
            subject,
            content,
            settings,
        )
    return response.json({"status": "ok"})


@bp_v1.route(r"/<region>/tableau/<dashboard_id:int>/share/", methods=["DELETE"])
@utils.is_valid_region()
@protected()
async def remove_dashboard_sharing(request, region: str, dashboard_id: int):
    """Remove a shared dashboard from a user's profile."""
    current_user = await auth_user(request, region)
    readonly = request.args.get("readonly", "false")

    ok = await controller_tableau.remove_dashboard_sharing(
        dashboard_id, region, current_user, readonly == "true"
    )
    if ok:
        return response.json({"message": "Tableau de bord retiré de votre profil."})
    else:
        return response.json(
            {"message": "Le retrait de ce tableau de bord a échoué."}, status=400
        )


@bp_v1.route(r"/<region>/contact")
@utils.is_valid_region()
async def contact(request, region):
    """
    Envoie un mail de contact à l'admin du site
    """

    region_settings = await regions_configuration.get_configuration(
        region, with_private=True
    )
    sender = request.args.get("email", "Inconnu")
    first_name = request.args.get("firstname", "Inconnu")
    last_name = request.args.get("lastname", "Inconnu")
    organization = request.args.get("organism", "Inconnu")
    subject = request.args.get("subject", "Inconnu")
    question = request.args.get("question", "Inconnu")
    rgpd = request.args.get("rgpd", "Inconnu")
    captcha_token = request.args.get("captcha_token", None)
    try:
        await user.check_token_captcha(captcha_token)
    except ValueError as e:
        return response.json({"message": str(e)}, status=401)
    RGPD = ""
    if rgpd == "true":
        RGPD = "L'utilisateur a accepté que les informations saisies soient exploitées par TerriSTORY pour le recontacter."

    content = """
    Sujet: {subject}

    {question}

    {RGPD}

    De: {first_name} {last_name}
    Structure: {organization}
""".format(
        subject=subject,
        first_name=first_name,
        last_name=last_name,
        organization=organization,
        question=question,
        RGPD=RGPD,
    )

    contact_email_response_to = region_settings[0]["contact_email_response_to"]
    region_label = region_settings[0]["label"]
    status = await send_mail(
        sender,
        region_label,
        sender,
        contact_email_response_to,
        subject,
        content,
        settings,
    )
    return response.json({"status": status})


@bp_v1.route(r"/<region>/newsletter", methods=["POST"])
@utils.is_valid_region()
async def subscribe_newsletter(request, region):
    region_settings = await regions_configuration.get_configuration(
        region, with_private=True
    )
    newsletter_national_enabled = region_settings[0]["enable_newsletter_national"]
    newsletter_regional_enabled = region_settings[0]["enable_newsletter_regional"]
    if not newsletter_national_enabled and not newsletter_regional_enabled:
        return response.json({"status": "La newsletter est désactivée"}, status=400)

    prenom = request.json["prenom"]
    nom = request.json["nom"]
    mail_user = request.json["mail"]
    organisation = request.json["organisation"]
    fonction = request.json.get("fonction")
    type_newsletter = request.json.get("type_newsletter")

    contact_email_region = region_settings[0]["contact_email"]
    contact_mails = []
    unsubscribe_email = None
    region_label = None

    if "national_newsletter" in type_newsletter and newsletter_national_enabled:
        contact_mails.append("contact@terristory.fr")
        region_label = "national"
        unsubscribe_email = contact_email_region

    if "regional_newsletter" in type_newsletter and newsletter_regional_enabled:
        contact_mails.append(contact_email_region)
        region_label = region_settings[0]["label"]
        unsubscribe_email = "contact@terristory.fr"

    if (
        "regional_newsletter" in type_newsletter
        and "national_newsletter" in type_newsletter
    ):
        region_label = region_settings[0]["label"] + " et national"
        unsubscribe_email = (
            contact_email_region
            + " (pour la newsletter régionale) et contact@terristory.fr (pour la newsletter nationale)"
        )
    if region_label is None:
        return response.json(
            {"status": "Aucun type valide de newsletter spécifié"}, status=404
        )

    title = f"Confirmation d’abonnement au fil d’actu TerriSTORY® {region_label}"
    title_admin = f"Demande d’abonnement au fil d’actu TerriSTORY® {region_label}"
    content = f"""
                <html>
                <p>Bonjour,</p>
                <p>L’abonnement au fil d’actu TerriSTORY® {region_label} a bien été pris en compte pour :</p>
                <p>Nom : {nom} <br />
                Prénom : {prenom}<br />
                Organisme : {organisation}<br />
                Fonction : {fonction}<br />
                Email : {mail_user}</p>
                <p>Si vous souhaitez vous désabonner, vous pouvez écrire à l'adresse {unsubscribe_email}.</p>

                <p>Bien cordialement,<br />
                L'équipe TerriSTORY </p>
                </html>
            """
    content_admin = f"""
                <html>
                <p>Bonjour,</p>
                <p>Une nouvelle demande d'abonnement au fil d’actu TerriSTORY® {region_label} :</p>
                <p>Nom : {nom} <br />
                Prénom : {prenom}<br />
                Organisme : {organisation}<br />
                Fonction : {fonction}<br />
                Email : {mail_user}</p>

                <p>Bien cordialement,<br />
                L'équipe TerriSTORY </p>
                </html>
            """
    status = await send_mail(
        "contact@terristory.fr",
        region_label,
        None,
        mail_user,
        title,
        content,
        settings,
        typemail="html",
    )

    for mail in contact_mails:
        status = await send_mail(
            "contact@terristory.fr",
            region_label,
            None,
            mail,
            title_admin,
            content_admin,
            settings,
            typemail="html",
        )
    return response.json({"status": status})


@bp_v1.route(r"/national/subscribe", methods=["POST"])
async def subscribe_other_territory(request):
    prenom = request.json["firstName"]
    nom = request.json["lastName"]
    mail_user = request.json["email"]
    fonction = request.json.get("fonction")
    territoire = request.json.get("territoire")

    region_configuration = await regions_configuration.get_configuration("national")
    contact_email_region = region_configuration[0]["contact_email"]

    region_label = "Autre Territoire"
    title = (
        f"Confirmation de demande pour la création d'un nouveau territoire TerriSTORY®"
    )
    title_admin = f"Demande de création d'un nouveau territoire TerriSTORY®"
    content = f"""
                <p>Bonjour,</p>
                <p>La demande de création d'un nouveau territoire TerriSTORY® a bien été pris en compte pour :</p>
                <p>Nom : {nom} <br />
                Prénom : {prenom}<br />
                Fonction : {fonction}<br />
                Email : {mail_user}</p>
                Territoire: {territoire} </p>
                <p>Si vous avez des questions, vous pouvez écrire à l'adresse {contact_email_region}.</p>

                <p>Bien cordialement,<br />
                L'équipe TerriSTORY </p>
            """
    content_admin = f"""
                <p>Bonjour,</p>
                <p>Une nouvelle demande de création d'un nouveau territoire TerriSTORY® :</p>
                <p>Nom : {nom} <br />
                Prénom : {prenom}<br />
                Fonction : {fonction}<br />
                Email : {mail_user}</p>
                Territoire: {territoire} </p>
                <p>Bien cordialement,<br />
                L'équipe TerriSTORY </p>
            """
    status = await send_mail(
        contact_email_region,
        region_label,
        None,
        mail_user,
        title,
        content,
        settings,
        typemail="html",
    )

    status = await send_mail(
        contact_email_region,
        region_label,
        None,
        contact_email_region,
        title_admin,
        content_admin,
        settings,
        typemail="html",
    )
    return response.json({"status": status})


@bp_v1.route(r"/national/<region>/import/metadata")
@utils.is_valid_region(True)
async def get_national_import_metadata(request, region):
    list_metadata = await national_controller.get_metadata(region)

    list_tables_names = [t["table_key"] for t in list_metadata]
    # Fetch headers of each data table
    data_table_header = await controller.get_data_tables_headers(
        region, list_tables_names
    )

    # Fetch last update date for each data table
    data_history = await controller.get_last_update_date(region, list_tables_names)

    # Add headers and update date information to the dictionary of data tables
    for table in list_metadata:
        table_key = table["table_key"]
        table["header"] = data_table_header.get(table_key, [])
        table["data_exist"] = bool(table["header"])
        if data_history.get(table_key):
            table["date_maj"] = data_history[table_key][0].get("date_maj", "")
        table["requirements"] = json.loads(table["requirements"])
        table["optional"] = json.loads(table["optional"])

    # Process headers to replace IDs with modalities and remove 'commune' column
    for data in list_metadata:
        if data["header"]:
            # Convert the header list of dictionaries to a DataFrame
            header_df = pd.DataFrame(data["header"])

            # Replace IDs with modalities asynchronously
            header_df = await national_controller.convert_modality_to_id(
                region, header_df, True
            )

            # Remove 'commune' column if it exists
            if "commune" in header_df.columns:
                del header_df["commune"]

            # Convert the DataFrame back to a list of dictionaries
            data["header"] = header_df.to_dict(orient="records")

    return response.json(list_metadata)


@bp_v1.route(r"/national/<region>/import/metadata/<table_key>/")
@utils.is_valid_region(True)
async def get_national_import_metadata_for_table(request, region, table_key):
    list_metadata = await national_controller.get_metadata(region, table_key)
    list_metadata["requirements"] = json.loads(list_metadata["requirements"])
    list_metadata["optional"] = json.loads(list_metadata["optional"])
    return response.json(list_metadata)


@bp_v1.route(r"/<region>/categories")
@utils.is_valid_region()
async def categories(request, region):
    """Récupére la liste des catégories (indicateurs) pour une région"""
    list_categories = await controller.list_categories(region)
    return response.json(list_categories)


@bp_v1.route(r"/<region>/categories/data")
@utils.is_valid_region()
async def categories_full_data(request, region):
    """Récupére la liste des catégories (indicateurs) pour une région"""
    list_categories = await controller.get_categories_full_contents(region)
    return response.json(list_categories)


@bp_v1.route(r"/<region>/categories_modalites")
@utils.is_valid_region()
async def categories_modalities(request, region):
    """Récupére la liste des catégories concaténées aux modalités pour une région"""
    list_categories_modalites = await controller.list_categories_modalites(region)
    return response.json(list_categories_modalites)


@bp_v1.route(r"/<region>/analysis/ajouter", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def add_indicator(request, region):
    """Point d'entrée pour ajouter un nouvel indicateur

    Query Params
    ------------
    form : form
    """
    schema = region.replace("-", "_")
    if not request.form:
        return response.json({"message": "Aucune donnée envoyée !"}, status=400)
    metadata = {}
    metadata["nom"] = request.form["nomIndicateur"][0]
    metadata["nom_table"] = utils.parse_table_name(request.form["nomTable"][0])
    metadata["filter"] = request.form["filtre"][0]
    metadata["type"] = request.form["type"][0]
    metadata["is_ratio"] = request.form["isRatio"][0].lower() in [
        "true"
    ]  # conversion str to bool
    metadata["color_start"] = request.form["couleurDebut"][0].replace("#", "")
    metadata["color_end"] = request.form["couleurFin"][0].replace("#", "")
    metadata["classification_config"] = request.form["classification_config"][0]
    metadata["representation_details"] = json.loads(
        request.form["representation_details"][0]
    )
    metadata["unit"] = request.form["unite"][0]
    metadata["decimals"] = (
        int(request.form["decimales"][0]) if request.form["decimales"][0] else 0
    )
    try:
        metadata["years"] = utils.parse_list_and_ranges(request.form["annees"][0])
    except ValueError as e:
        return response.json({"status": "error", "message": str(e)}, status=400)
    metadata["estimated_years"] = []
    if "anneesEstimees" in request.form and len(request.form["anneesEstimees"]) > 0:
        try:
            metadata["estimated_years"] = utils.parse_list_and_ranges(
                request.form["anneesEstimees"][0]
            )
        except ValueError:
            pass
    metadata["year_selection_input_type"] = request.form["year_selection_input_type"][0]
    metadata["ui_theme"] = request.form["theme"][0]
    metadata["categories"] = json.loads(request.form["categories"][0])
    metadata["confidentiel"] = request.form["confidentiel"][0]
    metadata["donnees_exportables"] = request.form["donneesExportables"][0]
    metadata["only_for_zone"] = request.form["onlyForZone"][0]
    metadata["disabled_for_zone"] = request.form["disabledForZone"][0]
    metadata["disabled_for_macro_level"] = request.form["disabledForMacroLevel"][0]
    metadata["display_total"] = request.form["display_total"][0] == "true"
    metadata["disabled_in_dashboard"] = (
        request.form["disabled_in_dashboard"][0] == "true"
    )
    metadata["credits_analysis_producers"] = request.form["credits_analysis_producers"][
        0
    ]
    metadata["credits_data_sources"] = request.form["credits_data_sources"][0]
    metadata["credits_data_producers"] = request.form["credits_data_producers"][0]
    metadata["annee_perimetre_geographique"] = request.form[
        "anneePerimetreGeographique"
    ][0]
    metadata["parent"] = request.form["parent"][0]
    metadata["parentCategory"] = (
        json.loads(request.form["parentCategory"][0])
        if request.form["parentCategory"][0]
        else None
    )
    metadata["fichierPdfMethodo"] = request.files.get("fichierPdfMethodo")
    metadata["nomFichierPdfMethodo"] = request.files.get("nomFichierPdfMethodo")

    data_custom = request.form["dataCustom"][0]
    donnees_existantes = request.form["donneesExistantes"][0]

    nomFichier = ""
    if metadata["type"] != "wms_feed" and not donnees_existantes.lower() in ["true"]:
        fichier_donnees = request.files.get("fichier")
        # Unique name for file uploaded on the server
        dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
        nomFichier = "{}/{}/{}_{}".format(
            settings.get("api", "upload"), region, dateId, fichier_donnees.name
        )

        # Saving the uploaded file
        try:
            with open(nomFichier, "wb") as fd:
                fd.write(fichier_donnees.body)
        except EnvironmentError as e:
            logger.exception(e)
            return response.json(
                {
                    "status": "error",
                    "message": "Problème lors du téléchargement du fichier de données",
                },
                status=400,
            )

    # Appel à la fonction d'ajout de l'indicateur (qui est accessible en ligne de commande également)
    try:
        indicator_id = await create_indicator(
            region, schema, metadata["nom_table"], metadata, nomFichier, data_custom
        )

        # Update history
        utilisateur = await auth_user(request, region)
        action = "Création d'un indicateur"
        # we take the whole metadata except files binaries
        del metadata["fichierPdfMethodo"]
        details = json.dumps(metadata)
        await controller.add_to_data_history(
            region,
            schema,
            utilisateur,
            action,
            metadata["nom"] + f" (id: {indicator_id})",
            details,
        )
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)

    return response.json(
        {"message": "Indicateur ajouté", "id": indicator_id}, status=200
    )


@bp_v1.route(r"/<region>/donnees_territoriales/ajouter/<tablename>", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def integrer_donnees_territoriales(request, region, tablename):
    """Point d'entrée pour ajouter la table d'une donnée nécessaire à la mise à jour des territoires

    Query Params
    ------------
    form : form
    """
    global_region = request.args.get("global_region")
    region_concerned = region
    # TODO: allow for more general regions?
    if global_region is not None and global_region == "france":
        region_concerned = global_region

    schema = region_concerned.replace("-", "_")

    colonnes_obligatoires = ("commune", "valeur")
    if request.args.get("territoire"):
        colonnes_obligatoires = ("commune", "code", "type_territoire", "nom")
    data_file = request.files.get("fichier")
    # Unique name for file uploaded on the server
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    filename = "{}/{}/{}_{}".format(
        settings.get("api", "upload"), region_concerned, dateId, data_file.name
    )

    # Saving the uploaded file
    try:
        with open(filename, "wb") as fd:
            fd.write(data_file.body)
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {filename}.")
        logger.error(f"Erreur: {str(e)}")
        return response.json(
            {
                "status": "error",
                "message": "Problème lors du téléchargement du fichier de données",
            },
            status=400,
        )

    # Appel à la fonction d'ajout de l'indicateur (qui est accessible en ligne de commande également)
    try:
        tablename = utils.parse_table_name(tablename)
        async with controller.db.acquire() as conn:
            async with conn.transaction():
                await integrer_donnee(
                    conn, schema, tablename, filename, colonnes_obligatoires
                )
                await date_perimetre_donnee_ajoutee(
                    conn,
                    region_concerned,
                    tablename,
                    int(request.args.get("date_perimetre")),
                )
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    return response.json({"message": "Données ajoutées"}, status=200)


@bp_v1.route(r"/<region>/analysis/<id>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def update_indicator(request, region, id):
    """Point d'entrée pour mettre à jour les données d'un indicateur

    Query Params
    ------------
    form : form
    """
    schema = region.replace("-", "_")
    if not request.form:
        return response.json({"message": "Aucune donnée envoyée !"}, status=400)
    existing_indicator = await controller_analyse.analysis(region, int(id), False)
    if len(existing_indicator) == 0:
        return response.json(
            {"message": "Aucun indicateur disponible dans votre région avec cet ID."},
            status=404,
        )

    metadata = {}
    metadata["nom"] = request.form["nomIndicateur"][0]
    metadata["unit"] = request.form["unite"][0]
    metadata["decimals"] = (
        int(request.form["decimales"][0]) if request.form["decimales"][0] else 0
    )
    try:
        metadata["years"] = utils.parse_list_and_ranges(request.form["annees"][0])
    except ValueError as e:
        return response.json({"status": "error", "message": str(e)}, status=400)
    metadata["estimated_years"] = []
    if "anneesEstimees" in request.form and len(request.form["anneesEstimees"]) > 0:
        try:
            metadata["estimated_years"] = utils.parse_list_and_ranges(
                request.form["anneesEstimees"][0]
            )
        except ValueError:
            pass
    metadata["year_selection_input_type"] = request.form["year_selection_input_type"][0]
    metadata["categories"] = json.loads(request.form["categories"][0])
    metadata["filter"] = request.form["filtre"][0]
    metadata["color_start"] = request.form["couleurDebut"][0].replace("#", "")
    metadata["color_end"] = request.form["couleurFin"][0].replace("#", "")
    metadata["classification_config"] = request.form["classification_config"][0]
    metadata["representation_details"] = json.loads(
        request.form["representation_details"][0]
    )
    metadata["type"] = request.form["type"][0]
    metadata["confidentiel"] = request.form["confidentiel"][0]
    metadata["donnees_exportables"] = request.form["donneesExportables"][0]
    metadata["only_for_zone"] = request.form["onlyForZone"][0]
    metadata["disabled_for_zone"] = request.form["disabledForZone"][0]
    metadata["disabled_for_macro_level"] = request.form["disabledForMacroLevel"][0]
    metadata["display_total"] = request.form["display_total"][0] == "true"
    metadata["disabled_in_dashboard"] = (
        request.form["disabled_in_dashboard"][0] == "true"
    )
    metadata["credits_analysis_producers"] = request.form["credits_analysis_producers"][
        0
    ]
    metadata["credits_data_sources"] = request.form["credits_data_sources"][0]
    metadata["credits_data_producers"] = request.form["credits_data_producers"][0]
    metadata["parent"] = request.form["parent"][0]
    metadata["parentCategory"] = json.loads(request.form["parentCategory"][0])

    metadata["fichierPdfMethodo"] = request.files.get("fichierPdfMethodo")
    metadata["nomFichierPdfMethodo"] = request.files.get("nomFichierPdfMethodo")

    data_custom = request.form["dataCustom"][0]

    # Appel à la fonction de mise à jour de l'indicateur (qui est accessible en ligne de commande également)
    try:
        res = await update_metadata_indicator(
            region, schema, int(id), metadata, data_custom
        )

        # Update history
        utilisateur = await auth_user(request, region)
        action = "Mise à jour d'un indicateur"
        del metadata["fichierPdfMethodo"]
        autres_informations = json.dumps(metadata)
        await controller.add_to_data_history(
            region,
            schema,
            utilisateur,
            action,
            metadata["nom"] + f" (id: {id})",
            autres_informations,
        )

        # Check if pdf upload failed
        if res.get("status", True) and res.get("pdf_failed", False):
            return response.json(
                {"message": "Indicateur mis à jour (mais PDF non uploadé)"}, status=200
            )
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)

    return response.json({"message": "Indicateur mis à jour"}, status=200)


@bp_v1.route(r"/<region>/analysis/<id>", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_analysis(request, region, id):
    """
    Entrypoint to delete an analysis. Proceed in two steps:

    1. first, if confirm GET arg was not sent, returns nb of dashboards linked
       to current analysis;
    2. if confirm GET arg was sent, delete analysis.
    """
    confirm = request.args.get("confirm")

    query = """SELECT * FROM meta.indicateur WHERE id = $1 AND region = $2"""
    rset = await controller.fetch(query, int(id), region)
    if len(rset) == 0:
        return response.json(
            {"message": "Indicateur non trouvé dans cette région !"}, status=404
        )
    data = dict(rset[0])
    protected_tables = await controller_analyse.get_protected_tables(region)
    if data["data"] in protected_tables and (
        data["filter"] == "" or data["filter"] is None
    ):
        return response.json(
            {
                "message": "Vous ne pouvez pas supprimer cet indicateur car sa donnée source est protégée."
            },
            status=400,
        )
    is_protected = await controller_analyse.is_protected_analysis(region, int(id))
    if is_protected:
        return response.json(
            {
                "message": "Vous ne pouvez pas supprimer cet indicateur car il est protégé."
            },
            status=400,
        )

    if confirm != "true":
        query = f"""SELECT COUNT(DISTINCT t.id) AS nb_dashboards
        FROM meta.tableau_bord t
        LEFT JOIN meta.tableau_thematique tt ON tt.tableau = t.id
        WHERE t.region = $1 AND
        (tt.graphiques::text LIKE '%"id_analysis": {int(id)}%'
        OR tt.graphiques::text LIKE '%"id_analysis": "{int(id)}"%')"""
        rset = await controller.fetch(query, region)
        return response.json(
            {"related_dashboards": rset[0]["nb_dashboards"]}, status=200
        )
    else:
        await controller_analyse.delete_analysis(region, int(id))
        return response.json({"message": "Indicateur supprimé"}, status=200)


@bp_v1.route(r"/<region>/analysis/data")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def get_indicators_headers(request, region):
    """Entry point to get the dictionnary that gathers information about data tables and
    the headers of each table, for a given region
    """
    list_tables = await controller.list_tables(region)
    list_tables_names = [t["nom"] for t in list_tables]
    data_table_header = await controller.get_data_tables_headers(
        region, list_tables_names
    )
    data_history = await controller.get_update_date(region, list_tables_names)
    # We add the headers to the dictionnary that gathers information about the
    # data tables and update date
    for table in list_tables:
        table["header"] = data_table_header[table["nom"]]
        if data_history[table["nom"]]:
            table["date_maj"] = data_history[table["nom"]][0]["date_maj"]
    return response.json(list_tables)


@bp_v1.route(r"/<region>/<territory>/code")
@utils.is_valid_region()
@utils.is_valid_national_region()
async def get_region_code(request, region, territory):
    """
    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    """
    schema = territory.replace("-", "_")
    req = f"""
          SELECT code FROM {schema}.territoire
          WHERE type_territoire = 'region'
          """
    resp = await controller.fetch(req)
    res = resp[0]["code"]

    return response.json(res)


@bp_v1.route(r"/<region>/<territory>/analysis/data/<table_name>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
@utils.is_valid_national_region()
async def data_national_indicateur_update(request, region, territory, table_name):
    """
    Entry point to update data for a given table.
    """
    schema = territory.replace("-", "_")

    data_file = request.files.get("file")

    region_settings = await regions_configuration.get_configuration(
        territory, with_private=True, allow_national_regions=True
    )
    is_national = region_settings[0]["is_national"]

    # Create a unique name for the data file
    now = datetime.datetime.now()
    dateId = now.strftime("%Y%m%d_%H%M%S%f")[:-4]
    original_filename = "{}/{}/{}/{}_{}.csv".format(
        settings.get("api", "upload"), region, territory, dateId, table_name
    )

    modified_filename = "{}/{}/{}/{}_{}_modified.csv".format(
        settings.get("api", "upload"), region, territory, dateId, table_name
    )

    # Define mandatory columns
    mandatory_columns = ("annee", "valeur")

    # Save the original file temporarily
    try:
        with open(original_filename, "wb") as fd:
            fd.write(data_file.body)
    except EnvironmentError as e:
        logger.error(f"Problem downloading to {original_filename}.")
        logger.error(f"Error: {str(e)}")
        return response.json(
            {"status": "error", "message": "Problem downloading the data file."},
            status=400,
        )

    datatypes = await table_datatypes(schema, table_name)

    # Read and modify the CSV file
    try:
        df = pd.read_csv(original_filename, delimiter=";")

        # Modify the static "Code commune" to match the corresponding number in the DATABASE
        df["commune"] = "01"

        # Apply the "fill up" step based on database settings
        df = await national_controller.fill_in_uploaded_data(
            territory, table_name, df, datatypes
        )

        # Convert modality string to Integer IDs
        df = await national_controller.convert_modality_to_id(territory, df)

        # Save the modified DataFrame to a new CSV file
        df.to_csv(modified_filename, index=False, sep=";")
    except Exception as e:
        logger.error(f"Problem processing the CSV file: {str(e)}")
        logger.exception(e)
        return response.json(
            {"status": "error", "message": "Problem processing the data file."},
            status=400,
        )
    finally:
        # Clean up the original file if no longer needed
        if os.path.exists(original_filename):
            os.remove(original_filename)

    # Call the function to update the indicator data
    try:
        res = await update_data(
            schema, table_name, modified_filename, mandatory_columns, is_national
        )
        # Update history
        utilisateur = await auth_user(request, region)
        action = "Update data"
        autres_informations = "TODO"
        await controller.add_to_data_history(
            region, schema, utilisateur, action, table_name, autres_informations
        )

    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    finally:
        # Clean up the modified file if no longer needed
        if os.path.exists(modified_filename):
            os.remove(modified_filename)

    return response.json({"message": "Données mises à jour"}, status=200)


@bp_v1.route(
    r"/<region>/<territory>/analysis/data/export/<table_name>/", methods=["GET"]
)
@utils.is_valid_tablename("territory", "table_name")
@protected()
@utils.only_if_admin_in_region()
@utils.is_valid_national_region()
async def export_national_analysis_data_table_content(
    request, region, territory, table_name
):
    """
    Entrypoint to retrieve the full content of a given table name.

    Returns
    -------
    csv
       csv file
    """
    # Fetch data from the controller
    data = await controller.get_data_table(territory, table_name)

    # Check if data is found
    if isinstance(data, bool) and data == False:
        return response.json(
            {"message": f"Impossible de trouver la table de données : {table_name}."},
            status=404,
        )

    # Replace IDs with modalities
    data = await national_controller.convert_modality_to_id(territory, data, True)

    # Create a temporary file to avoid permissions issues
    mime_type = "text/csv"
    try:
        with tempfile.NamedTemporaryFile(mode="w+b") as fp:
            # Write data to CSV file
            data.to_csv(fp.name, sep=";", index=False)

            # Return CSV file as response
            return await response.file(
                fp.name,
                status=200,
                mime_type=mime_type,
                filename=f"{territory}_{table_name}.csv",
            )
    except Exception as e:
        logger.exception(e)

    # Handle error if data export fails
    return response.json(
        {
            "message": f"Une erreur s'est produite lors de l'exportation des données. Impossible de fournir le fichier des données : {table_name}."
        },
        status=404,
    )


@bp_v1.route(
    r"/<region>/<territory>/analysis/data/<table_name>/categories", methods=["GET"]
)
@utils.is_valid_tablename("territory", "table_name")
@protected()
@utils.only_if_admin_in_region()
@utils.is_valid_national_region()
async def get_analysis_data_categories(request, region, territory, table_name):
    """
    Retrieves the required categories for the given table name in the specified region and territory.

    Returns
    -------
    json
        A JSON response containing the required categories.
    """
    try:
        # Query to fetch data unit
        query_data_unit = """
            SELECT data_unit FROM strategie_territoire.passage_table 
            WHERE association_type = 'table' AND region=$1 AND match=$2
        """

        result = await controller.fetch(query_data_unit, territory, table_name)
        data_unit = result[0]["data_unit"]

        # Query to fetch column names
        query_column_name = """
            SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = $1
            AND table_name = $2;
        """
        rset = await controller.fetch(query_column_name, territory, table_name)

        # Extract column names from the result set
        column_names = [row["column_name"] for row in rset]

        # Get required categories grouped by 'nom'
        categories_grouped = await national_controller.get_required_categories_name(
            territory, column_names
        )

        return response.json({"categories": categories_grouped, "data_unit": data_unit})

    except Exception as e:
        logger.exception(e)
        return response.json(
            {"message": "An error occurred while processing the request."}, status=500
        )


@bp_v1.route(r"/<region>/<territory>/territorial_strategy/activate")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
@utils.is_valid_national_region()
async def enable_territorial_strategy(request, region, territory):
    """
    Check and update metadata for simulator and territorial strategy for a given region
    """
    try:
        # Retrieve the list of tables for the territory
        list_tables = await controller.list_tables(territory)
        list_tables_names = [t["nom"] for t in list_tables]

        # Fetch headers of each data table
        data_table_header = await controller.get_data_tables_headers(
            territory, list_tables_names
        )

        # Verify whether the necessary data for the territorial strategy is stored in the database, and activate the corresponding actions accordingly.
        status_territorial_strategy = (
            await national_controller.check_and_update_actions_status(
                territory, data_table_header
            )
        )

        # Check and update metadata for simulator mobility
        status_simulator = await national_controller.update_metadata_simulator(
            data_table_header, territory
        )

        # Return JSON response
        return response.json(
            {
                "simulator_mobility": status_simulator["mobility_simulator_status"],
                "simulator_enr": status_simulator["enr_simulator_status"],
                "territorial_strategy": status_territorial_strategy,
            }
        )
    except Exception as e:
        logger.error(f"An error occurred while enabling territorial strategy: {e}")
        logger.exception(e)
        return response.json(
            {
                "error": "An error occurred while processing your request.",
                "details": str(e),
            },
            status=500,
        )


@bp_v1.route(r"/<region>/<territory>/territorial_strategy/status")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
@utils.is_valid_national_region()
async def check_data_validity(request, region, territory):
    """
    Check if the necessary data for the territorial strategy and simulator is valid  for a given region.
    Returns:
        A JSON response indicating whether all required metadata is valid
        in the database for the territorial strategy and the simulator.
    """
    try:
        # Retrieve the list of tables for the territory
        list_tables = await controller.list_tables(territory)
        list_tables_names = [t["nom"] for t in list_tables]

        # Fetch headers of each data table
        data_table_header = await controller.get_data_tables_headers(
            territory, list_tables_names
        )

        # Verify whether the necessary data for the territorial strategy is stored in the database, and activate the corresponding actions accordingly.
        enable_territorial_strategy = (
            await national_controller.check_and_update_actions_status(
                territory, data_table_header
            )
        )

        # Verify if the necessary data for the simulator is stored in the database
        enable_simulator = await national_controller.check_data_validity_simulator(
            territory, data_table_header
        )

        # Return JSON response
        return response.json(
            {
                "simulator_mobility": enable_simulator["mobility_simulator_enabled"],
                "simulator_enr": enable_simulator["enr_simulator_enabled"],
                "territorial_strategy": enable_territorial_strategy,
            }
        )
    except Exception as e:
        logger.error(f"An error occurred while checking data validity: {e}")
        logger.exception(e)
        return response.json(
            {
                "error": "An error occurred while processing your request.",
                "details": str(e),
            },
            status=500,
        )


@bp_v1.route(r"/<region>/analysis/data", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def insert_new_data_table(request, region):
    schema = region.replace("-", "_")

    data_file = request.files.get("file")
    table_name = request.form["tableName"][0]
    table_name = utils.parse_table_name(table_name)
    perimeter_date = request.form["datePerimeter"][0]
    only_for_zone = request.form["onlyForZone"][0]

    if await utils.is_forbidden_technical_table(region, table_name):
        return response.json(
            {
                "status": "error",
                "message": "Impossible de créer une table de données avec ce nom qui est protégé (territoire ou table fonctionnelle).",
            },
            status=403,
        )

    # Unique name for file uploaded on the server
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    filename = "{}/{}/{}_{}".format(
        settings.get("api", "upload"), region, dateId, table_name + ".csv"
    )

    mesh_perimeter = "commune"
    if only_for_zone != "":
        if not await utils.valid_territory_type(region, only_for_zone):
            return response.json(
                {
                    "status": "error",
                    "message": "Le type de maille spécifié n'est pas valide.",
                },
                status=400,
            )
        colonnes_obligatoires = ("annee", only_for_zone, "valeur")
        mesh_perimeter = only_for_zone
    else:
        colonnes_obligatoires = ("annee", "commune", "valeur")

    # Saving the uploaded file
    try:
        with open(filename, "wb") as fd:
            fd.write(data_file.body)
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {filename}.")
        logger.error(f"Erreur: {str(e)}")
        return response.json(
            {
                "status": "error",
                "message": "Problème lors du téléchargement du fichier de données",
            },
            status=400,
        )

    # Appel à la fonction de mise à jour de l'indicateur (qui est accessible en ligne de commande également)
    try:
        async with controller.db.acquire() as conn:
            async with conn.transaction():
                await integrer_donnee(
                    conn,
                    schema,
                    table_name,
                    filename,
                    colonnes_obligatoires,
                    mesh_perimeter,
                )
                await date_perimetre_donnee_ajoutee(
                    conn, region, table_name, int(perimeter_date), only_for_zone
                )
        # Update history
        utilisateur = await auth_user(request, region)
        action = "Ajout d'une table de donnée"
        autres_informations = "TODO"
        await controller.add_to_data_history(
            region, schema, utilisateur, action, table_name, autres_informations
        )

    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    return response.json({"message": "Table de données ajoutée !"}, status=200)


@bp_v1.route(r"/<region>/analysis/data/history")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def data_update_history(request, region):
    """
    Entrypoint to retrieve the data update history for current region.
    """

    def parse_history_dict(dict):
        if dict["autres_informations"].startswith("{"):
            dict["autres_informations"] = json.loads(dict["autres_informations"])
        return dict

    schema = region.replace("-", "_")
    query = f"""SELECT id, nom_donnee, mail as utilisateur, action, 
            to_char(date_maj, 'YYYY-MM-DD HH24:MI:SS') as date_maj, autres_informations
            FROM {schema}.historique_indicateurs ORDER BY date_maj DESC"""
    rset = await controller.fetch(query)
    data = [parse_history_dict(dict(x)) for x in rset]
    return response.json(data)


@bp_v1.route(r"/<region>/analysis/data/presence")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def data_presente_en_base(request, region):
    """Point d'entrée pour récupérer la liste des tables de données y compris celles qui ne sont pas impliquées dans le calcul d'un indicateur.

    Parameters
    ----------
    region : chaîne de caractères
        Nom de la région normalisé

    Returns
    --------
    Liste :
        Liste de dictionnaires contenant le nom de la table ainsi que la date de son périmètre géographique

    """
    req = """
          select nom_table as nom, date_perimetre
          from meta.perimetre_geographique
          where region = $1;
          """
    list_tables = await controller.fetch(req, region)
    return response.json([dict(x) for x in list_tables])


@bp_v1.route(r"/<region>/analysis/donnees_communes/<global_region>", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def donnees_communes(request, region, global_region):
    """Point d'entrée pour récupérer la liste des tables de données (indicateurs)"""
    if global_region != "france":
        return response.json(
            {
                "message": f"La seule région globale de données communes est la France actuellement."
            },
            status=404,
        )
    list_tables = await controller.liste_tables_jdd_communs(global_region)
    list_tables = [dict(i) for i in list_tables]
    list_tables_names = [t["nom"] for t in list_tables]

    # Fetch headers of each data table
    data_table_header = await controller.get_data_tables_headers(
        global_region, list_tables_names
    )
    # We add the headers to the dictionnary that gathers information about the
    # data tables
    for table in list_tables:
        table["header"] = data_table_header[table["nom"]]

    return response.json(list_tables)


@bp_v1.route(
    r"/<region>/analysis/donnees_communes/<global_region>/export/<table_name>/",
    methods=["GET"],
)
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def export_common_data_values(request, region, global_region, table_name):
    """
    Entrypoint to retrieve the full content of a table located in common dataset.
    Just require the table name (real table name).

    Returns
    -------
    csv
        a csv file named after the table and containing the results fo the query
    """
    if global_region != "france":
        return response.json(
            {
                "message": f"La seule région globale de données communes est la France actuellement."
            },
            status=404,
        )

    data = await controller.get_common_dataset_table(global_region, table_name)
    # we first check we do have indeed found data
    if isinstance(data, bool) and data == False:
        return response.json(
            {
                "message": f"Impossible de trouver la table de données que vous avez sélectionnée dans le registre commun (table : {table_name})."
            },
            status=404,
        )

    # we temp folder to be sure we can create the temporary file and avoid
    # permissions issues.
    mime_type = "text/csv"
    try:
        with tempfile.NamedTemporaryFile(mode="w+b") as fp:
            data.to_csv(fp.name, sep=";", index=False)
            return await response.file(
                fp.name, status=200, mime_type=mime_type, filename=table_name + ".csv"
            )
    except:
        pass
    return response.json(
        {
            "message": f"Impossible de fournir le fichier des données que vous avez sélectionnées (table : {table_name})."
        },
        status=404,
    )


@bp_v1.route(r"/<region>/analysis/data/export/<table_name>/", methods=["GET"])
@utils.is_valid_tablename("region", "table_name")
@protected()
@utils.only_if_admin_in_region()
async def export_analysis_data_table_content(request, region, table_name):
    """
    Entrypoint to retrieve the full content of a table located in common dataset.
    Just require the table name (real table name).

    Returns
    -------
    csv
        a csv file named after the table and containing the results fo the query
    """
    data = await controller.get_data_table(region, table_name)
    # we first check we do have indeed found data
    if isinstance(data, bool) and data == False:
        return response.json(
            {
                "message": f"Impossible de trouver la table de données que vous avez sélectionnée dans le registre régional (table : {table_name})."
            },
            status=404,
        )

    # we temp folder to be sure we can create the temporary file and avoid
    # permissions issues.
    mime_type = "text/csv"
    try:
        with tempfile.NamedTemporaryFile(mode="w+b") as fp:
            data.to_csv(fp.name, sep=";", index=False)
            return await response.file(
                fp.name,
                status=200,
                mime_type=mime_type,
                filename=region + "_" + table_name + ".csv",
            )
    except:
        pass
    return response.json(
        {
            "message": f"Impossible de fournir le fichier des données que vous avez sélectionnées (table : {table_name})."
        },
        status=404,
    )


@bp_v1.route(r"/<region>/analysis/export/<analysis_id:int>/", methods=["GET"])
@utils.is_valid_region()
async def export_analysis(request, region: str, analysis_id: int):
    """
    Entrypoint to export the data of an analysis by its id at given zone/sub-territory.
    Can export in csv or excel format, for the specified year or all of them by default.

    Returns
    -------
    csv
        A csv file containing the results of the query
    """
    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    maille = await utils.valid_territory_type(region, request.args.get("maille"))
    zone_id = request.args.get("zone_id")
    if zone is None or maille is None or zone_id is None:
        return response.json(
            {
                "message": "Paramètre de zone manquant : zone, maille et zone_id sont requis"
            },
            status=400,
        )
    annee = request.args.get("annee")
    try:
        annee = int(annee)
    except (TypeError, ValueError):
        annee = None
    format_ = request.args.get("format")
    if format_ not in ["csv", "excel", "gpkg"]:
        format_ = "csv"

    # analysis_meta = await controller_analyse.analysis(region, analysis_id)
    indicateur = IndicateurTableauBord(region, analysis_id)
    analysis_meta = await indicateur.meta_donnees_indicateur()
    if not analysis_meta:
        return response.json(
            {"message": f"L'indicateur n°{analysis_id} n'existe pas dans cette région"},
            status=400,
        )
    if maille not in analysis_meta["donnees_exportables"].split(","):
        return response.json(
            {"message": f"L'indicateur n'est pas exportable à la maille {maille}."},
            status=401,
        )
    if format_ in ["gpkg"]:
        export_data = await controller_analyse.get_export_geo_dataframe(
            zone,
            maille,
            zone_id,
            analysis_meta,
            annee,
        )
    else:
        export_data = await controller_analyse.get_export_dataframe(
            zone,
            maille,
            zone_id,
            analysis_meta,
            annee,
        )

    if format_ == "excel":
        suffix = ".xlsx"
        mime_type = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    elif format_ == "gpkg":
        suffix = ".gpkg"
        mime_type = "application/geopackage+sqlite3"
    else:
        suffix = ".csv"
        mime_type = "text/csv"
    filename = slugify.slugify(analysis_meta["nom"]) + suffix

    try:
        # we use temp folder to be sure we can create the temporary file and avoid
        # permissions issues.
        with tempfile.TemporaryDirectory() as tmp:
            tmp_file = tmp + "/" + filename
            if format_ == "excel":
                export_data.to_excel(tmp_file, engine="openpyxl", index=False)
            elif format_ == "gpkg":
                export_data.to_file(tmp_file, driver="GPKG")
            else:
                export_data.to_csv(tmp_file, sep=";", decimal=",", index=False)
            return await response.file(
                tmp_file,
                status=200,
                mime_type=mime_type,
                filename=filename,
            )
    except Exception as e:
        logger.error(f"Problème d'export {format_} vers {filename}.")
        logger.exception(e)
        return response.json(
            {
                "message": f"Impossible de fournir le fichier des données que vous avez sélectionnées (indicateur : {analysis_meta['nom']})."
            },
            status=400,
        )


@bp_v1.route(r"/<region>/donnees_territoriales")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def donnees_territoriales_obligatoires(request, region):
    """Point d'entrée pour récupérer la liste des données associées aux mailles territoriales"""
    schema = region.replace("-", "_")
    donnees_obligatoires = await obtenir_donnees_obligatoires_pour_maille_territoriales(
        region
    )
    liste_donnees_presentes = await liste_tables_donnees(schema)
    liste_donnees_pour_indicateur = await obtenir_liste_tables_pour_indicateur(region)
    donnees_obligatoires = (
        verifier_integration_donnees_obligatoires_pour_maille_territoriales(
            liste_donnees_presentes, liste_donnees_pour_indicateur, donnees_obligatoires
        )
    )

    return response.json(donnees_obligatoires)


@bp_v1.route(r"/<region>/donnees_geographiques")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def donnees_geographiques(request, region):
    """Point d'entrée pour récupérer la liste des données
    géographiques nécessaires (communes de France et table territoire)
    """
    schema = region.replace("-", "_")
    liste_donnees_obligatoires = [
        "territoire",
        "confid_maille",
        "confid_territoire",
    ]
    liste_tables_presentes = await liste_tables_donnees(schema)
    presences_dans_base = [
        {"presente": (x in liste_tables_presentes), "nom": x, "indicateur": False}
        for x in liste_donnees_obligatoires
    ]

    return response.json(presences_dans_base)


@bp_v1.route(r"/<region>/analysis/data/<nom_table>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def data_indicateur_update(request, region, nom_table):
    """Point d'entrée pour metre à jour les données d'une table de données (indicateur)

    Query Params
    ------------
    form : form
    """
    schema = region.replace("-", "_")

    if (
        await utils.is_forbidden_technical_table(region, nom_table)
        and nom_table != "territoire"
    ):
        return response.json(
            {
                "status": "error",
                "message": "Impossible de mettre à jour cette table de données qui est protégée (table fonctionnelle).",
            },
            status=403,
        )

    data_file = request.files.get("fichier")
    region_settings = await regions_configuration.get_configuration(
        region, with_private=True
    )
    is_national = region_settings[0]["is_national"]

    # Unique name for file uploaded on the server
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    filename = "{}/{}/{}_{}".format(
        settings.get("api", "upload"), region, dateId, data_file.name
    )
    colonnes_obligatoires = ("annee", "commune", "valeur")
    force_update = request.args.get("force_update", False)
    if request.args.get("donnees_territoriales"):
        colonnes_obligatoires = ("commune", "valeur")
    elif request.args.get("territoire"):
        colonnes_obligatoires = ("commune", "code", "type_territoire", "nom")
    elif request.args.get("only_for_zone"):
        colonnes_obligatoires = ("annee", request.args.get("only_for_zone"), "valeur")
    # Saving the uploaded file
    try:
        with open(filename, "wb") as fd:
            fd.write(data_file.body)
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {filename}.")
        logger.error(f"Erreur: {str(e)}")
        return response.json(
            {
                "status": "error",
                "message": "Problème lors du téléchargement du fichier de données",
            },
            status=400,
        )

    # Appel à la fonction de mise à jour de l'indicateur (qui est accessible en ligne de commande également)
    try:
        res = await update_data(
            schema,
            nom_table,
            filename,
            colonnes_obligatoires,
            is_national,
            force_update,
        )
        # Update history
        utilisateur = await auth_user(request, region)
        action = "Mise à jour d'une donnée"
        autres_informations = "TODO"
        await controller.add_to_data_history(
            region, schema, utilisateur, action, nom_table, autres_informations
        )

    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    except exceptions.ForceUpdateAllowedException as e:
        return response.json(
            {
                "status": "error",
                "message": "This operation should not be performed but can be forced.",
            },
            status=403,
        )
    return response.json({"message": "Données mises à jour"}, status=200)


@bp_v1.route(r"/<region>/analysis/confid/data/<table_name>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def confid_data_indicateur_update(request, region, table_name):
    schema = region.replace("-", "_")

    if table_name not in ("confid_maille", "confid_camembert"):
        return response.json(
            {
                "status": "error",
                "message": "Impossible de mettre à jour des tables de données hors tables de confid.",
            },
            status=403,
        )

    data_file = request.files.get("fichier")

    # Unique name for file uploaded on the server
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    filename = "{}/{}/{}_{}.csv".format(
        settings.get("api", "upload"), region, dateId, table_name
    )

    # Saving the uploaded file
    try:
        with open(filename, "wb") as fd:
            fd.write(data_file.body)
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {filename}.")
        logger.error(f"Erreur: {str(e)}")
        return response.json(
            {
                "status": "error",
                "message": "Problème lors du téléchargement du fichier de données",
            },
            status=400,
        )
    if table_name == "confid_maille":
        colonnes_obligatoires = (
            "code",
            "type_territoire",
            "type_analyse",
            "annee",
        )
        df_confid = pd.read_csv(filename, sep=";")
        other_columns = set(df_confid.columns) - set(colonnes_obligatoires)
        df_confid["conditions"] = df_confid[other_columns].to_dict("records")
        df_confid["conditions"] = df_confid["conditions"].apply(json.dumps)
        df_confid = df_confid.drop(other_columns, axis=1)
        df_confid.to_csv(filename, sep=";", index=False)
    elif table_name == "confid_camembert":
        colonnes_obligatoires = (
            "code",
            "type_territoire",
            "type_analyse",
            "annee",
            "type_confid",
        )

    try:
        res = await update_data(
            schema, table_name, filename, colonnes_obligatoires, False
        )
        # Update history
        utilisateur = await auth_user(request, region)
        action = "Mise à jour de la confid"
        autres_informations = "TODO"
        await controller.add_to_data_history(
            region, schema, utilisateur, action, table_name, autres_informations
        )

    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    except exceptions.ForceUpdateAllowedException as e:
        return response.json(
            {
                "status": "error",
                "message": "This operation should not be performed but can be forced.",
            },
            status=403,
        )
    return response.json({"message": "Données mises à jour"}, status=200)


@bp_v1.route(r"/<region>/analysis/data/<table_name>", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_data_table(request, region, table_name):
    """
    Entrypoint to delete a datatable.

    Will disable all indicators related to this data table and remove associations
    inside `meta.perimetre_geographique` table.
    """
    confirm = request.args.get("confirm")
    protected_tables = await controller_analyse.get_protected_tables(region)
    if table_name in protected_tables:
        return response.json(
            {
                "message": "Vous ne pouvez pas supprimer ce tableau de données car il est protégé (dans les modules de stratégie ou de synthèse territoriale)."
            },
            status=400,
        )

    list_tables = await controller.list_tables(region)
    try:
        related_tables = next(
            v["indicateurs"] for v in list_tables if v["nom"] == table_name
        )
    except StopIteration:
        return response.json(
            {"message": "Table de données non trouvée dans cette région !"}, status=404
        )

    if confirm != "true":
        return response.json({"related_tables": related_tables}, status=200)
    else:
        res = await controller_analyse.delete_data_table(region, table_name)
        if res == True:
            return response.json({"message": "Table de données supprimée"}, status=200)
        else:
            return response.json({"message": res}, status=200)


@bp_v1.route("/<region>/date_perimetre", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def mettre_a_jour_perimetre_geo(request, region):
    """Met à jour le millésime du périmètre dans lequel les données sont exprimées (sans opérer de conversion des données).

    En effet, pour convertir des données dans le périmètre géographique de millésime sélectionné par l'administrateur
    en région, il est indispensable de connaître le périmètre dans lequel les données sont actuellement exprimée.
    Cette information est à saisir directement par l'administrateur en région depuis l'onglet de l'interface d'admin
    consacré à la gestion des données.
    """

    global_region = request.args.get("global_region")
    region_concerned = region
    # TODO: allow for more general regions?
    if global_region is not None and global_region == "france":
        region_concerned = global_region

    annee = int(request.json["annee_perimetre"], 10)
    table = request.json["nom"]
    req = """
          update meta.perimetre_geographique
          set date_perimetre = $1
          where region = $2
          and nom_table = $3;
          """
    await controller.fetch(req, annee, region_concerned, table)

    # Update history
    utilisateur = await auth_user(request, region)
    action = "Changement de la date du périmètre d'une donnée"
    schema = region_concerned.replace("-", "_")
    await controller.add_to_data_history(
        region_concerned,
        schema,
        utilisateur,
        action,
        table,
        f"Nouvelle année : {annee}",
    )

    return response.json({"message": "Date du périmètre mise à jour"}, status=200)


@bp_v1.route("/<region>/conversion_perimetre", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def convertir_perimetre_geo(request, region):
    """Converts a single data table to a new geographical perimeter.

    Parameters
    ----------

    region : str
            normalized region name (ex : auvergne-rhone-alpes, occitanie)
    """
    initial_perimeter_year = int(request.json["initialPerimeterYear"], 10)
    target_perimeter_year = int(request.json["targetPerimeterYear"], 10)
    table = request.json["tableName"]
    aggregation_function = request.json["aggregationFunction"]
    user_info = await auth_user(request, region)

    # Only accept the aggregation functions avalaible in the select field
    if aggregation_function not in ["sum", "mean", "min", "max"]:
        return response.json(
            {
                "message": "La fonction d'agrégation n'est pas valide. Elle doit être 'sum', 'mean', 'min' ou 'max'."
            },
            status=400,
        )

    # get the perimeter year currently saved in the database
    try:
        db_perimeter_year = await get_table_perimeter_year(region, table)
    except ValueError as e:
        return response.json(
            {"message": f"Erreur lors de l'accès à la table : {str(e)}"}, status=404
        )
    # make sure the initial perimeter in the request (that was visible in the UI) is up to date with the perimeter year saved in the database.
    if initial_perimeter_year != db_perimeter_year:
        return response.json(
            {
                "message": "Le périmètre enregistré pour la table ne correspond plus à celui de la requête. Veuillez recharger la page et réessayer."
            },
            status=409,
        )

    schema = region.replace("-", "_")
    conversion_results = await convert_tables_to_new_perimeter(
        region,
        schema,
        user_info,
        [{"table": table, "aggregation_function": aggregation_function}],
        target_perimeter_year,
    )
    conversion_successful = conversion_results[0]["success"]
    if not conversion_successful:
        error_message = conversion_results[0]["error_message"]
        return response.json(
            {
                "message": f"Une erreur est survenue lors de la conversion de périmètre : {error_message} "
            },
            status=400,
        )

    return response.json(
        {
            "message": f"La table a été convertie dans le périmètre {target_perimeter_year}",
        },
        status=200,
    )


@bp_v1.route("/<region>/geographic/perimeters/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_available_geographic_perimeters(request, region):
    perimeters = await controller.fetch(
        "SELECT year FROM meta.perimeter_global ORDER BY year DESC"
    )
    return response.json([int(x["year"]) for x in perimeters], status=200)


@bp_v1.route("/<region>/geographic/perimeter/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_current_regional_perimeter(request, region):
    perimeters = await controller.fetch(
        "SELECT year FROM meta.perimeter_regional WHERE region = $1 ORDER BY year DESC LIMIT 1",
        region,
    )
    return response.json({"current_perimeter": int(perimeters[0]["year"])}, status=200)


@bp_v1.route("/<region>/<region_code>/geographic/perimeter/<year>/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_all_geographic_perimeters(request, region, region_code, year):
    """Lance la mise à jour finale des territoires pour la région sélectionnée par l'administrateur

    Parameters
    -----------
    region : str
        Nom de la région sans accent ni majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)
    """
    year = int(year)
    perimeter = await controller.fetch(
        "SELECT EXISTS (SELECT * FROM meta.perimeter_global WHERE year = $1)", year
    )
    if not perimeter[0]["exists"]:
        return response.json(
            {"status": "error", "message": "Ce périmètre n'existe pas."}, status=404
        )

    try:
        await rebuild_all_geographic_perimeters(region, region_code, year)
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    return response.json(
        {"message": "Territoires et données associées mis à jour"}, status=200
    )


@bp_v1.route("/<region>/rebuild/geographic/perimeters/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def rebuild_geographic_perimeters(request, region):
    try:
        await get_current_perimeter_year(region)
    except ValueError:
        return response.json(
            {
                "status": "error",
                "message": "Aucun millésime de périmètre n'a été défini pour cette région. Veuillez faire tourner la mise à jour des périmètres communaux.",
            },
            status=404,
        )

    try:
        await rebuild_supra_perimeters(region)
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    return response.json(
        {"message": "Territoires et données associées mis à jour"}, status=200
    )


@bp_v1.route("/<region>/convert/data/geographic/perimeter/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def convert_all_data_to_new_geographic_perimeter(request, region):
    perimeter = await controller.fetch(
        "SELECT year FROM meta.perimeter_regional WHERE region = $1", region
    )
    if len(perimeter) == 0:
        return response.json(
            {
                "status": "error",
                "message": "Aucun millésime de périmètre n'a été défini pour cette région. Veuillez faire tourner la mise à jour des périmètres communaux.",
            },
            status=404,
        )

    year = int(perimeter[0]["year"])
    try:
        await update_all_data_to_current_perimeter(region, year)
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)
    return response.json(
        {"message": "Territoires et données associées mis à jour"}, status=200
    )


@bp_v1.route("/<region>/import/common/data/table/<table>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def import_common_data_table(request, region, table):
    """Copy a data table from common base to regional schema

    Parameters
    ----------
    region : str
        region name
    table : str
        table slug
    """
    force_update = request.args.get("forceUpdate") == "true"

    await controller.import_data_table_from_common_base(region, table, force_update)
    # Update or create the row in the meta.perimeter_geographique table
    if not force_update:
        async with controller.db.acquire() as conn:
            await date_perimetre_donnee_ajoutee(
                conn, region, table, int(request.args.get("date_perimetre"), 10)
            )
    else:
        update_query = """UPDATE meta.perimetre_geographique
        SET date_perimetre = $1
        WHERE nom_table = $2
        AND region = $3;
        """
        await controller.fetch(
            update_query, int(request.args.get("date_perimetre"), 10), table, region
        )

    return response.json({"message": "Données importées"})


@bp_v1.route("/<region>/import/common/indicator/<indicator_id>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def import_common_indicator(request, region: str, indicator_id: int):
    """Copy an indicator from common base to regional schema

    Parameters
    ----------
    region : str
        Nom de la région normalisé
    table : str
        Nom de la table
    """
    force_update = request.args.get("forceUpdate") == "true"
    try:
        await controller.import_indicator_from_common_base(
            region, int(indicator_id), force_update
        )
    except exceptions.ForceUpdateAllowedException as e:
        return response.json(
            {
                "message": f"This operation should not be performed but can be forced. Details: "
                + str(e),
            },
            status=403,
        )
    # Update or create the row in the meta.perimeter_geographique table
    return response.json({"message": "Indicateur importé !"})


@bp_v1.route("/<region>/import/common/category/<category_name>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def import_common_category(request, region, category_name):
    """Copy a category from common base to regional schema

    Parameters
    ----------
    region : str
        region name
    category : str
        category slug
    """
    force_update = request.args.get("forceUpdate") == "true"
    exists = await controller.check_category_exists("france", category_name)
    if not exists:
        return response.json(
            {"message": "Catégorie inexistante dans la région france !"}, status=404
        )

    await controller.import_category_from_common_base(
        region, category_name, force_update
    )
    # Update or create the row in the meta.perimeter_geographique table
    return response.json({"message": "Indicateur importé !"})


@bp_v1.route("/<region>/maj_pcaet_ademe", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def mettre_a_jour_table_pcaet(request, region):
    """Lance la mise à jour de la table pcaet.

    Parameters
    -----------
    region : str
        Nom de la région sans accent si majuscule (exemples : auvergne-rhone-alpes, occitanie, nouvelle-aquitaine)
    """

    # URL vers les CSV des PCAET de l'ademe.
    try:
        await pcaet_ademe.update_pcaet_from_ademe(region)
    except Exception as e:
        logger.error(f"La mise à jour des PCAET a échoué.")
        logger.exception(e)
        message = "Erreur lors de la mise à jour des données PCAET"
        if isinstance(e, pcaet_ademe.PcaetError):
            message = str(e)
        return response.json({"message": message}, status=400)
    logger.info("Mise à jour des PCAET en BD réussie.")
    return response.json({"message": "PCAET mis à jour"}, status=200)


@bp_v1.route(r"/<region>/siren/assignment", methods=["GET"])
@utils.is_valid_region()
async def get_siren_assigment(request, region):
    """
    Get SIREN codes for non-EPCI territories that have a PCAET
    """
    schema = region.replace("-", "_")
    data = await controller.fetch(f"SELECT * FROM {schema}.siren_assignment")
    data = [dict(d) for d in data]
    return response.json(data, status=200)


@bp_v1.route(r"/<region>/siren/assignment", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_siren_assignment(request, region):
    """
    Update the list of non-EPCI SIREN codes from a CSV file
    """
    data_file = request.files.get("fichier")

    # On crée un nom unique pour le fichier de données
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    filename = f"{settings.get('api', 'upload')}/{region}/{dateId}_siren_assignment.csv"

    # Enregistrement du fichier
    try:
        with open(filename, "wb") as fd:
            fd.write(data_file.body)
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {filename} : {str(e)}")
        return response.json(
            {"message": "Problème lors du téléchargement du fichier de données"},
            status=400,
        )

    schema = region.replace("-", "_")
    try:
        await pcaet_ademe.update_siren_assignment(schema, filename)
    except pcaet_ademe.PcaetError as e:
        logger.exception(e)
        return response.json({"message": str(e)}, status=400)

    return response.json({"message": "Numéros de SIREN mis à jour"}, status=200)


@bp_v1.route(r"/raw/pcaet", methods=["GET"])
async def get_raw_pcaet_table(request):
    pcaet = await controller.fetch(f"SELECT * FROM {pcaet_ademe.PCAET_TABLE}")
    if not pcaet or not len(pcaet):
        return response.json({"message": "No PCAET found."}, status=404)
    pcaet = pd.DataFrame(dict(line) for line in pcaet)
    try:
        with tempfile.NamedTemporaryFile(mode="w+b") as fp:
            pcaet.to_csv(fp.name, sep=";", index=False)
            return await response.file(
                fp.name,
                status=200,
                mime_type="text/csv",
                filename="pcaet.csv",
            )
    except:
        pass
    return response.json({"message": "Unable to provide PCAET file."}, status=404)


@bp_v1.route(r"/<region>/pcaet/trajectories/list", methods=["GET"])
@utils.is_valid_region(True)
async def get_pcaet_trajectories_list(request, region):
    """
    Retrieve the list of available PCAET related trajectories.

    Parameters
    ----------
    region : str
        region name

    Returns
    -------
    list
    """
    settings_regions = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    ui_show_strategy = settings_regions[0]["ui_show_plan_actions"]
    enable_air_pollutants_impacts = settings_regions[0]["enable_air_pollutants_impacts"]
    if not ui_show_strategy:
        return response.json([], status=200)

    pcaet_meta = pcaet_ademe.pcaet_trajectories_meta(enable_air_pollutants_impacts)

    passage_table = PassageTable(region)
    await passage_table.load([x["table"] for x in pcaet_meta])

    for meta in pcaet_meta:
        category_name = passage_table.convert_column_key(meta["column"], meta["table"])
        meta["category_name"] = category_name

    categories = await controller.fetch(
        """
        SELECT nom, modalite FROM meta.categorie
        WHERE region = $1 AND nom = ANY($2)
        """,
        region,
        [meta["category_name"] for meta in pcaet_meta],
    )
    categories = pd.DataFrame(dict(res) for res in categories).groupby(["nom"])

    for meta in pcaet_meta:
        meta["categories"] = {
            meta["category_name"]: {
                "categorie": meta["category_name"],
                "titre": meta["category_title"],
                "visible": True,
            }
        }
        meta["filters"] = {
            meta["category_name"]: [
                {"filtre_categorie": meta["category_name"] + "." + modalite}
                for modalite in categories.get_group(meta["category_name"])["modalite"]
            ]
        }
        table = passage_table.convert_table_key(meta["table"])
        schema = region.replace("-", "_")
        obs_year = await controller.fetch(f"SELECT MAX(annee) FROM {schema}.{table}")
        if obs_year[0]["max"] is None:
            import time

            meta["obs_year"] = time.localtime().tm_year
        else:
            meta["obs_year"] = int(obs_year[0]["max"])
        meta["table"] = str(meta["table"])

        del meta["column"], meta["category_name"], meta["category_title"]

    return response.json(pcaet_meta, status=200)


@bp_v1.route(r"/<region>/pcaet/trajectory/details/<trajectory_id>/", methods=["GET"])
@utils.is_valid_region(True)
async def get_pcaet_trajectory_details(request, region, trajectory_id):
    """
    Retrieve the list of available PCAET related trajectories.

    Parameters
    ----------
    region : str
        region name

    Returns
    -------
    list
    """
    zone = request.args.get("zone")
    zone_id = request.args.get("zone_id")

    settings_regions = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    enable_relative_pcaet = settings_regions[0]["relative_pcaet"]

    pcaet_meta = pcaet_ademe.pcaet_trajectories_meta()
    pcaet_meta = [x for x in pcaet_meta if x["id"] == trajectory_id]
    if not pcaet_meta:
        return response.json({"msg": "Trajectoire inconnue"}, status=404)

    table_key = pcaet_meta[0]["table"]
    category_key = pcaet_meta[0]["column"]
    unit = pcaet_meta[0]["unit"]

    category, historical_data = await init_trajectory.get_initial_trajectory(
        region, zone, zone_id, table_key, category_key
    )
    pcaet_trajectory = await pcaet_ademe.get_pcaet_by_category(
        region, zone, zone_id, table_key, category, enable_relative_pcaet
    )

    ref_year = ""
    if pcaet_trajectory and pcaet_trajectory[0]["data"]:
        ref_year = str(pcaet_trajectory[0]["data"][0]["annee"])

    supra_goals = await init_trajectory.get_supra_goals_for_trajectory(
        region, zone, zone_id, trajectory_id
    )

    data = {
        "historical_data": historical_data,
        "pcaet_trajectory": pcaet_trajectory,
        "unit": unit,
        "category": category,
        "ref_year": ref_year,
        "supra_goals": supra_goals,
    }

    return response.json(data, status=200)


@bp_v1.route(r"/<region>/pcaet/all", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_all_pcaet(request, region):
    """
    Récupérer l'ensemble des PCAET de la région. Utilisé pour le module d’agrégation.
    """
    settings_region = (await regions_configuration.get_configuration(region))[0]
    relative_pcaet = request.args.get(
        "relative_pcaet", settings_region["relative_pcaet"]
    ) in [True, "true"]
    air_pollutants_impacts = settings_region["enable_air_pollutants_impacts"]

    zone_type = request.args.get("zone_type", "region")
    zone_id = request.args.get("zone_id")
    if zone_type == "region" and zone_id is None:
        zone_id = await controller.fetch(
            f"SELECT code FROM {region.replace('-', '_')}.region"
        )
        zone_id = zone_id[0]["code"]
    if zone_id is None:
        return response.json({"message": "Zone non renseignée"}, status=400)
    if zone_type not in ["region", "departement"]:
        # TODO: paramétrer dans regions_configuration ?
        return response.json({"message": "Type de zone non valide"}, status=404)

    data = await pcaet_ademe.admin_get_all_pcaet(
        region, zone_type, zone_id, relative_pcaet, air_pollutants_impacts
    )
    return response.json(data, status=200)


@bp_v1.route(r"/<region>/categorie/ajouter", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region(allow_france=True)
async def categorie_ajouter(request, region):
    """Point d'entrée pour ajouter une nouvelle categorie

    Query Params
    ------------
    form : form
    """
    nom = request.form["nom"][0]
    data_file = request.files.get("fichier")

    # Unique name for file uploaded on the server
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    filename = "{}/{}/{}_{}".format(
        settings.get("api", "upload"), region, dateId, data_file.name
    )

    # Saving the uploaded file
    try:
        with open(filename, "wb") as fd:
            fd.write(data_file.body)
    except EnvironmentError as e:
        logger.error(f"Problème de téléchargement vers {filename}.")
        logger.error(f"Erreur: {str(e)}")
        return response.json(
            {
                "status": "error",
                "message": "Une erreur est survenue lors du téléchargement du fichier.",
            },
            status=400,
        )

    # Appel à la fonction d'ajout de la catégorie (qui est accessible en ligne de commande également)
    try:
        await inserer_nouvelle_categorie(filename, nom, region)
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)

    return response.json({"message": "Catégorie ajoutée"}, status=200)


@bp_v1.route(r"/<region>/mise_a_jour_pop_up", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_pop_up_accueil(request, region):
    """Mettre à jour la pop-up d'accueil.

    Parameters
    ----------
    region : la région à mettre à jour
    """
    if not request.json or "texteMarkdown" not in request.json:
        return response.json(
            {
                "status": "error",
                "message": "Il manque les données de MAJ de la popup.",
            },
            status=400,
        )
    # récupération des valeurs json
    texte_markdown = request.json["texteMarkdown"]
    tableau_img = request.json["tableauImg"]
    # maj de l'actu
    await update_actu_pop_up(region, texte_markdown)
    # lecture et écriture des images
    img_pop_up_accueil_path = settings.get("api", "img_pop_up_accueil_path")
    # groupe d'image auquel appartient le carousel
    groupe_img_carrousel = 0
    # si le tableau d'images n'est pas vide, on met à jour les images du carousel
    if tableau_img:
        suppression_images_region_pop_up_accueil(region, img_pop_up_accueil_path)
        for value in tableau_img:
            groupe_img_carrousel += 1
            # id de l'image
            id_img = 0
            # parcourt chaque image
            for subvalue in value:
                #  si l'image correspond à un format jpeg, jpg, png ou gif
                if re.search(r"image/jpe*g|gif|png", subvalue["data_url"]):
                    id_img += 1
                    # recherche l'extension du fichier
                    extension_img = re.findall(
                        r"jpe{0,1}g|gif|png", subvalue["data_url"]
                    )
                    # suppression des caractères inutiles dans la conversion de la chaine en image de base 64
                    img_format_txt = re.sub(
                        "(data:image/jpg;base64,)|(data:image/jpeg;base64,)|(data:image/gif;base64,)|(data:image/png;base64,)",
                        "",
                        subvalue["data_url"],
                    )
                    # création du chemin de sortie avec le nom du fichier
                    path = (
                        img_pop_up_accueil_path
                        + region
                        + "_"
                        + str(groupe_img_carrousel)
                        + "_"
                        + str(id_img)
                        + "_"
                        + "carrousel."
                        + extension_img[0]
                    )
                    # écriture de l'image en base 64
                    ecriture_image_base64(img_format_txt, path)

    return response.json({"status": "updated"}, status=200)


@bp_v1.route(r"/<region>/images_carousel", methods=["GET"])
@utils.is_valid_region()
async def recuperation_chemin_images(request, region):
    lien_dossier = settings.get("api", "img_pop_up_accueil_path")
    all_images = []
    for img in ["jpg", "jpeg", "gif", "png"]:
        images = list_path_file(lien_dossier, img)
        all_images += images
    path = "/img/img_carousel/"
    all_images_region = liste_images_carousel_avec_path(path, all_images, region)

    return response.json({"images": all_images_region})


@bp_v1.route(r"/<region>/logo_sources/add", methods=["PUT"])
@utils.is_valid_region()
@utils.only_if_admin_in_region()
async def save_logo_source(request, region):
    """
    Save source logo data.

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    """
    if "sources" not in request.form:
        return response.json(
            {
                "status": "error",
                "message": "Il faut fournir les données pour ajouter un logo.",
            },
            status=400,
        )
    try:
        sources = json.loads(request.form["sources"][0])
    except json.decoder.JSONDecodeError:
        return response.json(
            {
                "status": "error",
                "message": "Le format du json fourni n'est pas correct.",
            },
            status=400,
        )
    for item in sources:
        # Transform enabled_zone to a comma-separated string
        if isinstance(item["enabled_zone"], list):
            # If it's a list, convert it to a comma-separated string
            item["enabled_zone"] = ",".join(item["enabled_zone"])
        image = request.files.get("file" + str(item["id"]))
        if not image:
            return response.json(
                {
                    "status": "error",
                    "message": "Veuillez ajouter un logo.",
                },
                status=400,
            )
        if not item.get("url"):
            return response.json(
                {
                    "status": "error",
                    "message": "Veuillez saisir un URL.",
                },
                status=400,
            )
        if not item.get("name"):
            return response.json(
                {
                    "status": "error",
                    "message": "Veuillez saisir un nom.",
                },
                status=400,
            )

        if not item.get("enabled_zone"):
            item["enabled_zone"] = ""
        if not item.get("default_url"):
            item["default_url"] = ""

        file_extension = image.name.split(".")[-1]
        if file_extension not in ("png", "jpg", "jpeg"):
            return response.json(
                {
                    "status": "error",
                    "message": "Veuillez insérer une image (png, jpeg ou jpg).",
                },
                status=400,
            )

        try:
            sql = """
                insert into meta.data_source_logo(name,url,region, extension, enabled_zone, default_url) values ($1, $2, $3, $4, $5, $6)
                returning id;
                """
            id = await controller.fetch(
                sql,
                item["name"],
                item["url"],
                region,
                file_extension,
                item["enabled_zone"],
                item["default_url"],
            )
            path_logo = "{}{}_logo{}.{}".format(
                settings.get("api", "img_logo_source_fiches"),
                region,
                id[0]["id"],
                file_extension,
            )
            with open(path_logo, "wb") as fd:
                fd.write(image.body)

            up_sql = """
                update meta.data_source_logo set logo_path = $1
                where id = $2
                and region = $3
            """
            await controller.fetch(up_sql, path_logo, id[0]["id"], region)

        except EnvironmentError as e:
            logger.error(f"Problème de téléchargement vers {path_logo}.")
            logger.error(f"Erreur: {str(e)}")
            return response.json(
                {
                    "status": "error",
                    "message": "Problème lors du téléchargement du logo.",
                },
                status=400,
            )

    return response.json({"message": "Lien ajouté"}, status=200)


@bp_v1.route(r"/<region>/logo_sources/list")
@utils.is_valid_region()
async def get_logo_sources_list(request, region):
    """
    Return a list of logo sources.

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    """
    req = """
          select * from meta.data_source_logo
          where region = $1;
          """
    resp = await controller.fetch(req, region)
    res = [dict(i) for i in resp]

    return response.json(res)


@bp_v1.route(r"/<region>/<id>/logo_sources", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_data_source_logo(request, region, id):
    """
    Delete logo source related data.

    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    id : integer
        identifiant du logo
    """
    deletion = await controller_didactic.delete_logo(region, id)
    if not deletion:
        return response.json(
            {
                "status": "error",
                "message": "Impossible de trouver le logo demandé.",
            },
            status=404,
        )

    return response.json(
        {"message": "Logo " + deletion + " supprimé"},
        status=200,
    )


@bp_v1.route(r"/<region>/<id>/logo_sources/update", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_logo_source(request, region, id):
    """Mettre à jour les caractéristiques et le logo du source de données
    Parameters
    ----------
    region : str
        nom normalisé de la région (ex : auvergne-rhone-alpes, occitanie)
    id : int
        identifiant du logo
    """
    if "sources" not in request.form:
        return response.json(
            {
                "status": "error",
                "message": "Il faut fournir les données pour ajouter un logo.",
            },
            status=400,
        )

    try:
        sources = json.loads(request.form["sources"][0])
    except json.decoder.JSONDecodeError:
        return response.json(
            {
                "status": "error",
                "message": "Le format du json fourni n'est pas correct.",
            },
            status=400,
        )

    source = sources[0]

    sql = """
        SELECT * FROM meta.data_source_logo
        WHERE id = $1
        AND region = $2
    """

    res = await controller.fetch(sql, int(id), region)
    if len(res) == 0:
        return response.json(
            {
                "status": "error",
                "message": "Impossible de trouver le logo demandé.",
            },
            status=404,
        )

    # Prepare the image upload process
    image = request.files.get(f"file{id}")

    req = """
        UPDATE meta.data_source_logo
        SET url = $1, name = $2
        {image_update}
        {enabled_zone_update}
        {default_url_update}
        WHERE id = $3
        AND region = $4
    """
    enabled_zone_update = ""
    default_url_update = ""
    image_update = ""
    params = [source["url"], source["name"], int(id), region]

    if image:
        # Check file extension
        file_extension = image.name.split(".")[-1]
        if file_extension not in ("png", "jpg", "jpeg"):
            return response.json(
                {
                    "status": "error",
                    "message": "Veuillez insérer une image (png, jpeg ou jpg).",
                },
                status=400,
            )

        path_to_save = f"{settings.get('api', 'img_logo_source_fiches')}{region}_logo{int(id)}.{file_extension}"
        try:
            with open(path_to_save, "wb") as fd:
                fd.write(image.body)
        except EnvironmentError as e:
            logger.error(f"Problème de téléchargement vers {path_to_save}.")
            logger.error(f"Erreur: {str(e)}")
            return response.json(
                {
                    "status": "error",
                    "message": "Impossible de télécharger le logo vers le chemin demandé.",
                },
                status=400,
            )

        image_update = (
            f", logo_path = ${len(params) + 1}, extension = ${len(params) + 2}"
        )
        params += [path_to_save, file_extension]

    # Check if optional parameters are filled
    if "enabled_zone" in source and "default_url" in source:
        params.append(source["enabled_zone"])
        enabled_zone_update = f", enabled_zone = ${len(params)}"
        params.append(source["default_url"])
        default_url_update = f", default_url = ${len(params)}"

    # Format the final query
    final_query = req.format(
        image_update=image_update,
        enabled_zone_update=enabled_zone_update,
        default_url_update=default_url_update,
    )

    await controller.fetch(final_query, *params)

    return response.json(
        {"message": "Logo modifié"},
        status=200,
    )


@bp_v1.route(r"/<region>/<id>/logo_sources", methods=["GET"])
@utils.is_valid_region()
async def get_logo_source(request, region, id):
    """
    Return a logo details

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : int
        the logo ID
    """
    res = await controller_didactic.details_logo(region, id)
    if len(res) == 0:
        return response.json(
            {"message": "Impossible de trouver le logo demandé."}, status=404
        )
    return response.json(res[0])


@bp_v1.route(r"/<region>/didactic_file/list")
@utils.is_valid_region()
@utils.only_if_admin_in_region()
async def get_didactic_file_list(request, region):
    """
    Return a list of didactic files.

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    """
    res = await controller_didactic.get_list(region)
    return response.json(res)


@bp_v1.route(r"/<region>/<id>/didactic_file", methods=["GET"])
@utils.is_valid_region()
async def get_didactic_file(request, region, id):
    """
    Return a didactic files.

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : integer
        file ID
    """
    didactic_file = await controller_didactic.details_file(region, id)
    if not didactic_file:
        return response.json(
            {
                "status": "error",
                "message": "Impossible de trouver cette fiche didactique.",
            },
            status=404,
        )
    return response.json(didactic_file)


@bp_v1.route(r"/<region>/didactic_file/add", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def save_didactic_file(request, region):
    """
    Save didactic file data.

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    """
    if "didacticFile" not in request.form:
        return response.json(
            {
                "status": "error",
                "message": "Il faut fournir les données pour ajouter une fiche didactique.",
            },
            status=400,
        )
    try:
        didactic_file = json.loads(request.form["didacticFile"][0])
    except json.decoder.JSONDecodeError:
        return response.json(
            {
                "status": "error",
                "message": "Le format du json fourni n'est pas correct.",
            },
            status=400,
        )

    addition = await controller_didactic.add_file(region, didactic_file)
    if not addition:
        return response.json(
            {
                "status": "error",
                "message": "Un problème est survenu lors de la sauvegarde des données.",
            },
            status=400,
        )

    return response.json({"message": "Fiche enregistrée"}, status=200)


@bp_v1.route(r"/<region>/<id>/didactic_file/update", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_didactic_file(request, region, id):
    """update a didactic file
    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : entier
        file ID
    """

    if "didacticFile" not in request.form:
        return response.json(
            {
                "status": "error",
                "message": "Il faut fournir les données pour ajouter une fiche.",
            },
            status=400,
        )

    try:
        didactic_file = json.loads(request.form["didacticFile"][0])
    except json.decoder.JSONDecodeError:
        return response.json(
            {
                "status": "error",
                "message": "Le format du json fourni n'est pas correct.",
            },
            status=400,
        )

    update = await controller_didactic.update_file(region, id, didactic_file)
    if not update:
        return response.json(
            {
                "status": "error",
                "message": "Un problème est survenu lors de la sauvegarde des données.",
            },
            status=400,
        )

    return response.json(
        {"message": "Fiche modifiée"},
        status=200,
    )


@bp_v1.route(r"/<region>/<id>/didactic_file", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_didactic_file(request, region, id):
    """
    delete a didactic file

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    id : integer
        file ID
    """
    res = await controller_didactic.delete_file(region, id)
    if not res:
        return response.json(
            {
                "status": "error",
                "message": "Un problème est survenu lors de la suppression des données.",
            },
            status=400,
        )

    return response.json(
        {"message": "Fiche " + str(id) + " supprimée"},
        status=200,
    )


@bp_v1.route(r"/<region>/actions/list", name="get_all_actions_list")
@utils.is_valid_region()
@utils.only_if_admin_in_region()
async def get_actions_list(request, region):
    """
    Return la liste des actions existants

    Parameters
    ----------
    region : str
        normalized region name (ex : auvergne-rhone-alpes, occitanie)
    """
    res = await controller_actions.get_list(region)
    return response.json(res)


@bp_v1.route(
    r"/<region>/<simulator_type>/simulator", methods=["GET"], name="get_simulator"
)
@utils.is_valid_region(True)
async def get_simulator(request, region, simulator_type):
    """
    Return {data, metadata} of simulator

    Parameters
    ----------
    region : str
        normalized region name (ex: auvergne-rhone-alpes, occitanie)

    type : str
        simulator type (ex: mobility)

    territory : str, optional
        normalized territory name if present in the URL
    """
    config_region = await regions_configuration.get_configuration(
        region, allow_national_regions=True
    )
    config_region = config_region[0]
    if not config_region["ui_show_simulators"].get(simulator_type, False):
        return response.json(
            {"message": f"Unknown or disabled simulator."},
            status=404,
        )

    territory_maille = await utils.valid_territory_type(
        region, request.args.get("maille")
    )
    territory_type = await utils.valid_territory_type(region, request.args.get("zone"))
    territory_code = request.args.get("zone_id")

    # Create the simulator instance using the factory, based on the thematic (e.g., mobility or EnR)

    try:
        simulator = FactorySimulator.create_simulator(
            region, territory_maille, territory_type, territory_code, simulator_type
        )
    except ValueError as e:
        logger.error(e)
        return response.json(
            {"message": f"Unknown simulator."},
            status=404,
        )

    # Call get_metadata() on the created simulator instance
    try:
        simulator_configuration = await simulator.get_metadata()
    except ValueError as e:
        logger.error(e)
        return response.json(
            {
                "message": f"Méta-données manquantes dans la configuration du simulateur ({e})."
            },
            status=404,
        )

    return response.json(simulator_configuration)


@bp_v1.route("/<region>/registre")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def obtenir_jeu_de_donnees_registre(request, region):
    """Obtention de la liste des données disponibles sur la plateforme ODRé"""

    liste_donnees = obtenir_donnees_disponibles()
    donnees_registre = DonneesRegistre("")
    await donnees_registre.obtenir_liste_donnees_registre_deja_configurees()
    liste_tables_deja_configurees = donnees_registre.liste_donnees
    return response.json(
        {
            "liste_tables_rte": liste_donnees,
            "liste_tables_configuree": liste_tables_deja_configurees,
        },
        status=200,
    )


@bp_v1.route("/registre/<region>/<table>")
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def donnees_registre(request, region, table):
    """Obtention des données du registre.
    On ajoute aux données JSON la liste des données utilisées dans les indicateurs

    paramètres
    ----------
    region : chaîne de caractères
        Nom de la région normalisé
    table : chaîne de caractères
        Nom de la table dont on souhaite moissonner les données depuis la plateforme ODRé
    """

    url = (
        "https://opendata.reseaux-energies.fr/api/records/1.0/download/?dataset="
        + table
    )
    donnees_registre = DonneesRegistre(url)
    donnees_registre.obtenir_donnees()
    await donnees_registre.obtenir_liste_donnees_registre_deja_configurees()
    liste_tables_dispo = await obtenir_liste_tables_pour_indicateur(region)
    liste_tables_finale = [{"label": x["nom"]} for x in liste_tables_dispo]

    return response.json(
        {
            "liste_colonnes": [
                {"label": x, "value": x} for x in donnees_registre.liste_colonnes
            ],
            "valeurs_par_colonnes": donnees_registre.liste_intitules,
            "donnees_finales": donnees_registre.donnees.to_dict("records"),
            "liste_tables_dispo": liste_tables_finale,
        },
        status=200,
    )


@bp_v1.route(r"/registre/<region>/<table>/integration", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def integrer_donnees_registre(request, region, table):
    """Integrate RTE registre data.

    Parameters
    ----------
    region: str
        Normalized region name
    table: str
        Table name in which we integrate new data (or that we update if existing)
    put_payload: json
        Contains the data to include. Must have following structure:

        .. code-block:: python

            {
                "donnees": [
                    {
                        "year_col": "2014",
                        "city_code": "12345",
                        "value_name": "59609",
                        "category_1": "Try",
                        "category_2": "Test",
                    },
                    ...
                ],
                "categorie": [
                    {
                        "region": "region_name",
                        "colonne": "category_1",
                        "association": {
                            "Try": {
                                "modalite": "Try",
                                "modaliteId": 0,
                                "couleur": "#1265CA",
                            },
                            ...
                        },
                    },
                    {
                        "region": "pytest",
                        "colonne": "category_2",
                        "association": {
                            "Test": {
                                "modalite": "Test",
                                "modaliteId": 1,
                                "couleur": "#A2C412",
                            },
                            ...
                        },
                    },
                ],
                "nomTable": "new_table_name",
                "colAnnee": "year_col",
                "colCommune": "city_code",
                "colValeur": "value_name",
            }
    """

    schema = region.replace("-", "_")
    d = {}
    df = pd.DataFrame(request.json["donnees"])
    liste_tables_perimetre = await liste_donnees_perimetres(region)

    engine = get_pg_engine()
    liste_cat = await controller_analyse.list_categories(region)

    for i in request.json["categorie"]:
        categorie = [
            {
                "intitule": x,
                "region": i["region"],
                "modalite": i["association"][x]["modalite"],
                "modalite_id": int(i["association"][x]["modaliteId"]),
                "couleur": i["association"][x]["couleur"],
                "ordre": int(i["association"][x]["modaliteId"]),
                "nom": i["colonne"],
            }
            for x in i["association"]
            if "modalite" in i["association"][x].keys()
            and "modaliteId" in i["association"][x].keys()
        ]
        d[i["colonne"]] = categorie

        cat = pd.DataFrame(categorie).drop("intitule", axis=1)
        l_col = list(cat.keys())
        cat = cat[l_col].drop_duplicates()
        if i["colonne"] in liste_cat:
            sql = """
                  delete from meta.categorie
                  where region = $1
                  and nom = $2;
                  """
            await controller.fetch(sql, region, i["colonne"])
            cat.to_sql(
                "categorie", con=engine, index=False, schema="meta", if_exists="append"
            )
        else:
            cat.to_sql(
                "categorie", con=engine, index=False, schema="meta", if_exists="append"
            )

    l = []

    for a in d:
        df = df.merge(
            pd.DataFrame(d[a])[["intitule", "modalite_id"]],
            left_on=a,
            right_on="intitule",
        )
        df[a] = df["modalite_id"]
        df = df.drop("modalite_id", axis=1)
        l.append(a)

    if request.json["colAnnee"] == "":
        df["annee"] = int(request.json["annee"])
    else:
        df["annee"] = df[request.json["colAnnee"]]

    df = df[[request.json["colCommune"], request.json["colValeur"], "annee"] + l]
    df["commune"] = df[request.json["colCommune"]]
    df["valeur"] = df[request.json["colValeur"]]

    df = df[np.logical_and(df["valeur"] != "", df["commune"] != "")]
    df = df[np.logical_and(df["valeur"] != "\r", df["valeur"] != "")]

    df["valeur"] = df["valeur"].astype(float)
    df = df.drop(request.json["colValeur"], axis=1)
    liste_col = ["commune", "annee"] + l
    df = df.groupby(liste_col)[["valeur"]].agg("sum").reset_index()
    nomTable = normalize(request.json["nomTable"])[:30]
    df.to_sql(
        "{table}".format(table=nomTable),
        con=engine,
        index=False,
        schema="{schema}".format(schema=schema),
        if_exists="replace",
    )
    if nomTable not in liste_tables_perimetre:
        sql = """
              insert into meta.perimetre_geographique values ($1, $2, $3, $4);
              """
        await controller.fetch(
            sql, nomTable, region, datetime.datetime.today().year - 1, True
        )
    return response.json({"message": "Données intégrées"}, status=200)


@bp_v1.route("/changelog")
async def get_changelog(request):
    """
    Get change log file content
    """
    changelog_path = here / ".." / ".." / "CHANGELOG.md"
    if settings.has_option("api", "changelog_path"):
        changelog_path = settings.get("api", "changelog_path") + "CHANGELOG.md"
        with open(changelog_path, "r") as f:
            content = f.read()
            content = re.sub(
                r"#([0-9]+)",
                """<a href="https://gitlab.com/terristory/terristory/-/issues/\\1"
                    rel="noreferrer"
                    target="_blank">#\\1</a>""",
                content,
            )
    else:
        changelog_path = ""
        content = "Aucun changement disponible pour l'instant."
    re_last_version = re.search(r"## Version ([^-]+) - ([^\n]+)", content)
    # if we found last version data
    if re_last_version:
        last_version = re_last_version.group(1)
        last_version_date = re_last_version.group(2)
        branch_name = f"prod-{last_version}"
    else:
        last_version = False
        last_version_date = False
        branch_name = "master"

    content = content.replace(
        "Tous les changements notables sont documentés dans ce fichier.",
        f"""Tous les changements notables sont 
            <a 
                rel="noreferrer"
                target="_blank"
                href="https://gitlab.com/terristory/terristory/-/blob/{branch_name}/CHANGELOG.md">
                documentés ici</a>.""",
    )
    return response.json(
        {
            "content": content.strip("# Changelog\n"),
            "last_version": last_version,
            "last_version_date": last_version_date,
        },
        status=200,
    )


@bp_v1.route(r"/<region>/categorie", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_category_data(request, region):
    """Entry point to update a category of indicator values."""
    nom = request.form["categorie"][0]
    fichierDonnees = request.files.get("fichierCategorie")

    # Unique name for file uploaded on the server
    dateId = datetime.datetime.now().strftime("%d%m%y_%H%M%S")
    nomFichier = "{}/{}/{}_{}".format(
        settings.get("api", "upload"), region, dateId, fichierDonnees.name
    )

    # Saving the uploaded file
    try:
        with open(nomFichier, "wb") as fd:
            fd.write(fichierDonnees.body)
    except EnvironmentError:
        return response.json(
            {
                "status": "error",
                "message": "Problème lors du téléchargement du fichier de données",
            },
            status=400,
        )

    # Appel à la fonction d'ajout de la catégorie (qui est accessible en ligne de commande également)
    try:
        await update_categorie(nomFichier, nom, region)
    except exceptions.ValidationError as e:
        return response.json({"status": "error", "message": e.msg}, status=400)

    return response.json({"message": "Catégorie mise à jour"}, status=200)


@bp_v1.route(r"/<region>/categorie/<category_name>", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_category(request, region, category_name):
    """
    Entry point to update a category of indicator modalities. Also disable
    related indicators, and delete all associations between these indicators
    and charts for this category.
    """
    confirm = request.args.get("confirm")

    exists = await controller.check_category_exists(region, category_name)
    if not exists:
        return response.json(
            {"message": "Catégorie non trouvée dans cette région !"}, status=404
        )
    protected_tables = ("energie", "secteur", "usage")
    if category_name in protected_tables:
        return response.json(
            {
                "message": "Vous ne pouvez pas supprimer cette catégorie car elle est protégée."
            },
            status=400,
        )

    query_charts = f"""SELECT c.indicateur, c.titre, c.type
    FROM meta.chart c
    LEFT JOIN meta.indicateur i ON i.id = c.indicateur
    WHERE c.categorie = $1 AND i.region = $2"""
    rset_charts = await controller.fetch(query_charts, category_name, region)
    query_dashboards = f"""SELECT tableau, t.id as thematique
    FROM meta.tableau_thematique t
    LEFT JOIN meta.tableau_bord tb ON t.tableau = tb.id
    WHERE EXISTS (
        SELECT *
        FROM json_array_elements(t.graphiques) as x(o)
        WHERE (x.o ->> 'categories')::jsonb ? $1
    ) AND tb.region = $2;"""
    rset_dashboards = await controller.fetch(query_dashboards, category_name, region)
    related_dashboards = set(int(chart["tableau"]) for chart in rset_dashboards)
    if confirm != "true":
        schema = region.replace("-", "_")
        query_datatables = f"""SELECT COUNT(DISTINCT t.table_name) AS nb_datatables
        FROM meta.perimetre_geographique pg
        LEFT JOIN information_schema.columns t 
        ON t.table_schema = $1 AND t.table_name = pg.nom_table
        WHERE t.column_name = $2"""
        rset_datatables = await controller.fetch(
            query_datatables, schema, category_name
        )

        return response.json(
            {
                "related_datatables": rset_datatables[0]["nb_datatables"],
                "related_charts": len(rset_charts),
                "related_dashboards": len(related_dashboards),
            },
            status=200,
        )
    else:
        related_indicators = [int(chart["indicateur"]) for chart in rset_charts]
        related_dashboards_groups = [
            int(chart["thematique"]) for chart in rset_dashboards
        ]
        await controller_analyse.delete_category(
            region, category_name, related_indicators, related_dashboards_groups
        )
        return response.json({"message": "Catégorie supprimée"}, status=200)


@bp_v1.route(r"/<region>/consultation/autre/page/", methods=["PUT"])
@utils.is_valid_region()
async def suivi_consultation_autres_pages(request, region: str):
    """Instancie un mesureur d'audience afin d'assurer le suivi de consultations
    d'autres pages (A propos, documents pdfs...)

    Utilise les paramètres postés via PUT pour préciser la page, l'utilisateur
    et les détails.

    Il convient donc de fournir un dictionnaire contenant :
    - page : le nom de la page consultée
    - id_utilisateur : le token de l'utilisateur (valide dans la table dédiée)
    - details (optionnel) : des détails plus précis sur la page consultée

    Parameters
    ----------
    region : str
        nom de la région dépourvu de majuscule et accent (ex : auvergne-rhone-alpes)
    """
    if (
        not request.json
        or "page" not in request.json
        or "id_utilisateur" not in request.json
    ):
        return response.json(
            {
                "message": "Il faut spécifier la page consultée ainsi que l'ID de l'utilisateur."
            },
            status=400,
        )

    # on récupère les paramètres postés via PUT
    page = request.json["page"]
    details = request.json.get("details", "")
    id_utilisateur = request.json["id_utilisateur"]
    mesureur_d_audience = mesure_audience.MesureDAudienceAutresPages(
        region, page, details
    )
    id = await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)
    return response.json({"id": id})


@bp_v1.route(r"/<region>/consultation/tableau/bord/", methods=["PUT"])
@utils.is_valid_region()
async def suivi_consultation_tableaux_bords(request, region: str):
    """Instancie un mesureur d'audience afin d'assurer le suivi de consultations
    des tableaux de bords.

    Utilise les paramètres postés via PUT pour préciser le tableau de bord et
    l'utilisateur.

    Il convient donc de fournir un dictionnaire contenant :
    - id_utilisateur : le token de l'utilisateur (valide dans la table dédiée)
    - tableau_id : l'ID du tableau de bord
    - code_territoire : le code du territoire consulté
    - type_territoire : le type de territoire consulté

    Parameters
    ----------
    region : str
        nom de la région dépourvu de majuscule et accent (ex : auvergne-rhone-alpes)
    """
    # si on n'a pas toutes les données nécessaires
    if not request.json or not set(
        ("tableau_id", "id_utilisateur", "code_territoire", "type_territoire")
    ).issubset(set(request.json.keys())):
        # on retourne une erreur
        return response.json(
            {
                "message": "Il faut spécifier l'ensemble des paramètres sur le tableau de bord."
            },
            status=400,
        )

    # on récupère les paramètres postés via PUT
    tableau_id = request.json["tableau_id"]
    id_utilisateur = request.json["id_utilisateur"]
    code_territoire = request.json["code_territoire"]
    type_territoire = request.json["type_territoire"]

    query = """SELECT id, titre FROM meta.tableau_bord WHERE id = $1"""
    rset = await controller.fetch(query, tableau_id)
    if len(rset) == 0:
        return response.json(
            {"message": "Impossible de trouver le tableau de bord spécifié."},
            status=400,
        )

    tableau_nom = rset[0]["titre"]
    mesureur_d_audience = mesure_audience.MesureDAudienceTableauxBords(
        region, code_territoire, type_territoire, tableau_id, tableau_nom
    )
    id = await mesureur_d_audience.inserer_donnees_consultations(id_utilisateur)
    return response.json({"id": id})


@bp_v1.route(r"/<region>/stats/<stat_type>/<from_date>", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_visitors_stats(request, region, stat_type, from_date):
    """
    Retrieve the visitors stats for a specific region, type of stats and a date
    interval. The date interval must be defined at least by a beginning date.
    The end date is optional and current date is used as default.

    To specify the end date of the interval, one must add `to_date` param to the
    GET query (respecting the same format than *from_date* param).


    Parameters
    ----------
    request : _type_
        _description_
    region : str
        region name
    stat_type : str
        stat types among `visiting_stats` existing list
    from_date : str
        start date respecting YYYY-MM-DD format.

    Returns
    -------
    json
        a dict containing either:

        * message: in case of error, describe the problem encountered

        or:

        * data: the data content as a list of dicts
        * headers: the headers of corresponding data (which are the table columns)
    """
    if stat_type not in visiting_stats.STAT_TYPES:
        return response.json(
            {"message": "Impossible de trouver le type de statistiques demandé."},
            status=400,
        )

    try:
        from_datetime = datetime.datetime.strptime(from_date, "%Y-%m-%d")
    except ValueError:
        return response.json(
            {
                "message": "Le format de la date de début de période est incorrect (AAAA-MM-JJ)."
            },
            status=400,
        )

    to_date = request.args.get("to_date")
    to_datetime = datetime.datetime.now()
    if to_date is not None:
        try:
            to_datetime = datetime.datetime.strptime(to_date, "%Y-%m-%d")
        except ValueError:
            return response.json(
                {
                    "message": "Le format de la date de fin de période est incorrect (AAAA-MM-JJ)."
                },
                status=400,
            )

    data = await visiting_stats.get_stats_by_type(
        region, stat_type, from_datetime, to_datetime
    )
    if len(data) > 0:
        headers = list(data[0].keys())
    else:
        headers = []
    return response.json({"data": data, "headers": headers})


@bp_v1.route(r"/<region>/stats/types/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_visitors_stats_types(request, region):
    """
    Retrieve the stat types currently available inside `visiting_stats` module.

    Returns
    -------
    json
        a dict containing:

        * stats_types: the list of stat types names
    """
    return response.json({"stats_types": visiting_stats.STAT_TYPES})


@bp_v1.route(r"/<region>/all/stats/<from_date>", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_visitors_stats_all_regions(request, region, from_date):
    """
    Retrieve the stat types currently available inside `visiting_stats` module.

    Returns
    -------
    json
        a dict containing:

        * stats_types: the list of stat types names
    """
    env = settings.get("api", "env")
    userdata = await auth_user(request, region)

    if not userdata.global_admin:
        return response.json(
            {"message": "Vous n'avez pas les droits pour accéder à cette ressource."},
            status=403,
        )

    try:
        from_datetime = datetime.datetime.strptime(from_date, "%Y-%m-%d")
    except ValueError:
        return response.json(
            {
                "message": "Le format de la date de début de période est incorrect (AAAA-MM-JJ)."
            },
            status=400,
        )

    to_date = request.args.get("to_date")
    to_datetime = datetime.datetime.now()
    if to_date is not None:
        try:
            to_datetime = datetime.datetime.strptime(to_date, "%Y-%m-%d")
        except ValueError:
            return response.json(
                {
                    "message": "Le format de la date de fin de période est incorrect (AAAA-MM-JJ)."
                },
                status=400,
            )

    regions = await regions_configuration.get_non_national_regions(env)
    data = {}
    for region in regions:
        data[region] = {}
        for stat_type in visiting_stats.STAT_TYPES:
            data[region][stat_type] = await visiting_stats.get_stats_by_type(
                region, stat_type, from_datetime, to_datetime
            )
    return response.json(data)


@bp_v1.route(r"/<region>/static/files", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_static_files_list(request, region):
    """
    Retrieve the list of static files for current region inside the database.

    Parameters
    ----------
    region : str
        the region name

    Returns
    -------
    json
        a dict of:

        * data: the data content as a list of dicts
        * headers: the headers of corresponding data (which are the table columns)
    """
    data = await static_files.get_list(region)
    if len(data) > 0:
        headers = list(data[0].keys())
    else:
        headers = []
    return response.json({"data": data, "headers": headers})


@bp_v1.route(r"/<region>/static/file/<file_id>", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_static_file(request, region, file_id):
    """
    Update a static file using a new file given by the params.

    Parameters
    ----------
    region : str
        the region name
    file_id : str or int
        the file ID

    Returns
    -------
    json
        if no error happened, will return a dict with:

        * message: success message and
        * new_date: new `update_date` value.

        if an error happens, will a return a dict with:

        * message: error message describing the problem.
    """
    try:
        static_file = await static_files.get_static_file(region, file_id)
    except ValueError:
        return response.json(
            {"message": "Impossible de trouver le fichier statique."}, status=404
        )
    new_file = request.files.get("file")
    if new_file is None:
        return response.json(
            {"message": "Nouveau fichier manquant en paramètre."}, status=400
        )
    folder_path = settings.get("api", "upload_methodo", fallback=False) + "/" + region
    path_to_file = folder_path + "/" + static_file["file_name"] + ".pdf"

    # checking if the directory demo_folder
    # exist or not.
    if not os.path.exists(folder_path):
        # if the demo_folder directory is not present
        # then create it.
        os.makedirs(folder_path)

    with open(path_to_file, "wb") as fd:
        fd.write(new_file.body)
    updated_date = await static_files.update_static_file(region, file_id)
    if not updated_date:
        return response.json(
            {
                "message": "Une erreur est survenue lors de la mise à jour de la base de données !"
            },
            status=400,
        )
    else:
        return response.json(
            {
                "message": "Fichier bien mis à jour !",
                "new_date": updated_date.strftime("%d/%m/%Y, %H:%M:%S"),
            }
        )


@bp_v1.route(r"/<region>/analysis/page/parameters/<page>", methods=["GET"])
@utils.is_valid_region()
async def get_analysis_page_parameters(request, region, page):
    """
    Retrieve the stat types currently available inside `visiting_stats` module.

    Returns
    -------
    json
        a dict containing:

        * stats_types: the list of stat types names
    """
    env = settings.get("api", "env")
    data = await controller_suivi_trajectoire.get_page_parameters(region, env, page)
    passage_table = PassageTable(region)
    tables = {}

    if page == "suivi_energetique":
        tables = {
            DataSet.PRODUCTION: [RenewableProd],
            DataSet.CONSUMPTION: [Sector, Commodity, Usage],
        }
    elif page == "suivi_emission_ges":
        tables = {DataSet.EMISSIONS: [Sector, Commodity, Usage]}
    elif page.startswith("suivi_polluants_"):
        tables = {
            DataSet.table_from_pollutant(page[len("suivi_polluants_") :]): [
                Sector,
                Commodity,
                Usage,
            ]
        }
    if len(tables) == 0:
        return response.json({"message": "Pas de paramètres pour cette page."}, 404)

    await passage_table.load(list(tables.keys()))
    passage_table_elements = {}
    for table, cols in tables.items():
        _elts = {"name": passage_table.convert_table_key(table), "cols": {}}
        for col in cols:
            try:
                _elts["cols"][col] = passage_table.convert_column_key(col, table)
            except ValueError as e:
                logger.error(e)
                continue
        passage_table_elements[table] = _elts

    return response.json({"parameters": data, "passage_table": passage_table_elements})


@bp_v1.route(r"/<region>/analysis/pages/enabled/", methods=["GET"])
@utils.is_valid_region()
async def get_analysis_pages_enabled(request, region):
    """
    Retrieve the stat types currently available inside `visiting_stats` module.

    Returns
    -------
    json
        a dict containing:

        * stats_types: the list of stat types names
    """
    env = settings.get("api", "env")
    data = await controller_suivi_trajectoire.get_pages_enabled(env, region)
    return response.json({"pages": data.get(region, [])})


@bp_v1.route(r"/<region>/scenarios", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_region_scenarios(request, region):
    """Get all the scenarios for a given region

    Parameters
    ----------
    region : str
        Normalized region name.

    Returns
    -------
    list
        List of the scenarios for the region
    """
    data = await scenario.region_scenarios(region)
    if data is None:
        return response.json({"message": "Pas de stratégies trouvées."}, 404)
    return response.json(data)


@bp_v1.route(r"/<region>/actions/requisites", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_actions_requisites(request, region):
    """Get all the scenarios for a given region

    Parameters
    ----------
    region : str
        Normalized region name.

    Returns
    -------
    list
        List of the scenarios for the region
    """

    dispatcher = actions_dispatcher.ActionsDispatcher(None, None, None)
    tester = actions_testing.ActionsTester(None, None, None)
    requisites_per_action = defaultdict(dict)
    for key, action in dispatcher.actions.items():
        requisites_per_action[key]["name"] = str(action)
        file_content = meta_descriptor.get_file_content(key)
        variables_used = (
            meta_descriptor.find_constants_usage(file_content) if file_content else []
        )
        outputs = meta_descriptor.find_outputs(file_content) if file_content else []
        requisites_per_action[key]["variables_used"] = sorted(list(variables_used))
        requisites_per_action[key]["outputs"] = sorted(list(outputs))
        requisites_per_action[key]["requisites"] = defaultdict(list)
        for requisite in action.requisites:
            requisites_per_action[key]["requisites"][str(requisite.r_type)].append(
                str(requisite)
            )
        tests = tester.get_action_tests(str(key), [])
        if len(tests) > 0:
            tests = [test.to_dict() for test in tests]
        requisites_per_action[key]["tests"] = tests
    return response.json(requisites_per_action)


@bp_v1.route(r"/<region>/test/actions/<action_key>/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def post_test_actions(request, region, action_key):
    """
    Test one action
    """
    config_region = await regions_configuration.get_configuration(region)
    config_region = config_region[0]

    zone = await utils.valid_territory_type(region, request.args.get("zone"))
    zone_id = request.args.get("zone_id")
    if zone is None or zone_id is None:
        return response.text("Zone et zone_id sont requis", status=400)

    # Les impacts liées aux actions : énergie (conso et enr), émission ges
    results = await controller_actions.single_action_tester(
        action_key,
        region,
        zone,
        zone_id,
        config_region,
    )

    return response.json({"message": "Succès !", "results": results})


@bp_v1.route(r"/<region>/passage/table/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_passage_table(request, region):
    """Get all the scenarios for a given region

    Parameters
    ----------
    region : str
        Normalized region name.

    Returns
    -------
    list
        List of the scenarios for the region
    """
    table = passage_table.PassageTable(region)
    table_data = await table.whole()
    return response.json(table_data)


@bp_v1.route(r"/<region>/strategy/actions/errors/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_strategy_actions_errors(request, region):
    """Get all the scenarios for a given region

    Parameters
    ----------
    region : str
        Normalized region name.

    Returns
    -------
    list
        List of the scenarios for the region
    """
    errors = await controller_actions.get_actions_errors(region)
    return response.json(errors)


@bp_v1.route(r"/<region>/passage/table/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_passage_table(request, region):
    """Update a passage table from a csv file

    Parameters
    ----------
    region : str
        Normalized region name.

    Returns
    -------
    list
        List of the scenarios for the region
    """
    data_file = request.files.get("file")

    table = passage_table.PassageTable(region)

    with tempfile.NamedTemporaryFile(mode="w+b") as fp:
        try:
            with open(fp.name, "wb") as fd:
                fd.write(data_file.body)
        except:
            return response.json(
                {"message": f"Impossible d'enregistrer le fichier fourni."},
                status=404,
            )
        try:
            await table.update_from_csv(fp.name)
        except ValueError as e:
            logger.error(traceback.format_exc())

            return response.json(
                {"message": str(e)},
                status=400,
            )

    return response.json({"message": "Table de passage mise à jour !"})


@bp_v1.route(r"/<region>/call/external/api/<api_id>/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def call_external_api(request, region, api_id):
    """
    Call an external API to retrieve data and create local table in regional schema.

    Parameters
    ----------
    api_id : int
        API id inside `meta.external_api` table
    """
    api_config = await external_api.get_external_api_config(region, api_id)
    if api_config is None:
        return response.json(
            {"message": "Cette API externe n'existe pas dans la base."},
            status=404,
        )
    try:
        external_response = await external_api.call_external_api(api_config)

        output = await external_api.import_data(region, api_config, external_response)
    except ValueError as e:
        return response.json({"status": "failed", "message": str(e)}, 400)

    # we add full external API response to our response
    output["full_response"] = external_response
    return response.json(output)


@bp_v1.route(r"/<region>/external/api/update/<api_id>/", methods=["PUT"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def update_external_api(request, region, api_id):
    """
    Call an external API to retrieve data and create local table in regional schema.

    Parameters
    ----------
    api_id : int
        API id inside `meta.external_api` table
    """
    if request.json is None:
        return response.json(
            {"message": "Aucune donnée n'a été fournie pour créer un lien."},
            status=400,
        )
    api_config = await external_api.get_external_api_config(region, api_id)
    if api_config is None:
        return response.json(
            {"message": "Cette API externe n'existe pas dans la base."},
            status=404,
        )

    try:
        await external_api.update_meta_data(region, api_id, request.json, api_config)
    except ValueError as e:
        return response.json({"status": "failed", "message": str(e)}, 400)

    # we add full external API response to our response
    return response.json(
        {"status": "success", "message": "La mise à jour a fonctionné !"}
    )


@bp_v1.route(r"/<region>/external/api/new/", methods=["POST"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def add_external_api(request, region):
    """
    Call an external API to retrieve data and create local table in regional schema.

    Parameters
    ----------
    api_id : int
        API id inside `meta.external_api` table
    """
    new_data = request.json
    if new_data is None:
        return response.json(
            {"message": "Aucune donnée n'a été fournie pour créer un lien."},
            status=400,
        )
    if new_data.get("columns", {}) == "":
        new_data["columns"] = {}
    if new_data.get("filters", {}) == "":
        new_data["filters"] = {}
    if new_data.get("post_data", {}) == "":
        new_data["post_data"] = {}
    try:
        await external_api.add_new_api(region, new_data)
    except ValueError as e:
        return response.json({"status": "failed", "message": str(e)}, 400)

    # we add full external API response to our response
    return response.json({"status": "success", "message": "L'ajout a fonctionné !"})


@bp_v1.route(r"/<region>/external/api/delete/<api_id>/", methods=["DELETE"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def delete_external_api(request, region, api_id):
    """
    Delete an external API.

    Parameters
    ----------
    api_id : int
        API id inside `meta.external_api` table
    """
    api_config = await external_api.get_external_api_config(region, api_id)
    if api_config is None:
        return response.json(
            {"message": "Cette API externe n'existe pas dans la base."},
            status=404,
        )

    try:
        await external_api.delete_api(region, api_id)
    except ValueError as e:
        return response.json({"status": "failed", "message": str(e)}, 400)

    return response.json(
        {"status": "success", "message": "La suppression a fonctionné !"}
    )


@bp_v1.route(r"/<region>/get/external/api/list/", methods=["GET"])
@utils.is_valid_region()
@protected()
@utils.only_if_admin_in_region()
async def get_external_api_list(request, region):
    """
    Get the list of configured external API for current region inside `meta.external_api`
    table. Will hide real secret keys that are used to call APIs.
    """
    list_apis = await external_api.get_external_api_list(region)
    output = []
    for api in list_apis:
        if "key" in api:
            api["key"] = "*************"
        output.append(api)

    return response.json(output)
