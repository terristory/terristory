﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import math
from collections import defaultdict
from itertools import islice
from pathlib import Path

import pytest
from numpy import isclose

from terriapi.controller import fetch

pytestmark = pytest.mark.skip("Tests en attente de mise à jour")

# nombre de lignes de test à injecter dans un test pytest (= 1 ligne d'output pour chaque chunk)
CHUNK = 10
# échantillonnage à prendre dans chaque fichier
STEP = 30
# on limite le nombre de lecture dans les fichiers de test
LIMIT = 10

here = Path(__file__).resolve().parent

analysis = (
    ("a81", "Émissions GES / hab"),
    ("a83", "Émissions GES tertiaire / employé"),
    ("a84", "Émissions GES résidentiel / hab"),
    ("b11", "Consommation d'énergie / hab"),
    ("b12", "Consommation d'énergie tertiaire / employé"),
    ("b13", "Consommation d'énergie résidentiel / hab"),
    ("b18", "Consommation d'énergie primaire 2015 / hab"),
    ("b120", "Part Enr/Consommation d'énergie"),
    ("a16", "Stocks de carbone 2012"),
    ("a31", "Part d'espaces naturels 2012"),
    ("b46", "Taux d'étalement urbain 2012"),
    ("b41", "Taux logements vacants résidentiel 2014"),
    ("c111", "Part transports publics dans déplacements 2014"),
    ("d227", "Taux d'emploi (15-64 ans) 2014"),
    ("e16", "Emplois dans la construction 2014"),
    ("c51", "Part de logements desservis par un réseau de chaleur 2017"),
    ("e118", "Facture énergétique / hab"),
    ("e119", "Facture énergétique tertiaire / employé"),
    ("e120", "Facture énergétique résidentiel / hab"),
)
analysis_codes = [code for code, _ in analysis]


@pytest.fixture()
def metadict(app, loop):
    """dictionnaire contenant les identifiants des analyses
    récupérés dans la base à partir de leur nom
    """
    aid = loop.run_until_complete(
        *[
            fetch("select id, decimals, years from analysis where name = $1", name)
            for _, name in analysis
        ]
    )

    id_analyse = {
        code: {"id": res[0]["id"], "decimals": math.pow(10, -res[0]["decimals"])}
        for res, (code, _) in zip(aid, analysis)
    }
    return id_analyse


# dictionnaire contenant toutes les données de références
# pour chaque indicateur territorialsynthesis
references = defaultdict(list)
# paramètres d'URL pour chaque test territorialsynthesis
url_params = defaultdict(list)

# fichiers de tests sans filtre
for aid, filename in (
    ("a81", "test_a81_emission_ges_par_habitant.csv"),
    ("b11", "test_b11_conso_energie_par_habitant.csv"),
    ("b18", "test_b18_energie_primaire.csv"),
    ("a31", "test_surface_naturelle.csv"),
    ("b46", "test_etalement-urbain.csv"),
    ("b41", "test_logement-vacant.csv"),
    ("c111", "test_actifs-domicile-travail-transport-commun.csv"),
    ("d227", "test_actif-1564.csv"),
    ("e16", "test_emploi-construction.csv"),
    ("c51", "test_logement-desservis-rdc.csv"),
    ("e118", "test_e118_facture_energetique.csv"),
):
    with (here / "data" / filename).open() as lines:
        next(lines)
        for idx, line in enumerate(islice(lines, 0, None, STEP)):
            if LIMIT and idx > LIMIT:
                break
            cols = [field.strip() for field in line.split(";")]
            # [(code, valeur)...]
            references[aid].append((cols[2], float(cols[0])))
            url_params[aid].append(
                [
                    ("zone", "region"),
                    ("maille", cols[1]),
                    ("zone_id", "1"),
                    ("addcode", "1"),
                    ("nochart", "1"),
                ]
            )

for aid, filename in (
    ("a83", "test_a83_emission_ges_tertiaire_par_employes.csv"),
    ("a84", "test_a84_emissions_ges_residentiel_par_habitant.csv"),
    ("b12", "test_b12_conso_energie_tertiaire_par_employe.csv"),
    ("b13", "test_b13_conso_energie_residentiel_par_habitant.csv"),
):
    with (here / "data" / filename).open() as lines:
        next(lines)
        for idx, line in enumerate(islice(lines, 0, None, STEP)):
            if LIMIT and idx > LIMIT:
                break
            cols = [field.strip() for field in line.split(";")]
            # [(code, valeur)...]
            references[aid].append((cols[4], float(cols[0])))
            url_params[aid].append(
                [
                    ("zone", "region"),
                    ("maille", cols[3]),
                    ("zone_id", "1"),
                    ("addcode", "1"),
                    ("nochart", "1"),
                ]
            )
            if cols[1] != "*":
                url_params[aid][-1].append(("filter", "energie.{}".format(cols[1])))
            if cols[2] != "*":
                url_params[aid][-1].append(("filter", "usage.{}".format(cols[2])))


for aid, filename in (
    ("e119", "test_e119_facture_energetique_tertiaire.csv"),
    ("e120", "test_e120_facture_energetique_residentiel.csv"),
):
    with (here / "data" / filename).open() as lines:
        next(lines)
        for idx, line in enumerate(islice(lines, 0, None, STEP)):
            if LIMIT and idx > LIMIT:
                break
            cols = [field.strip() for field in line.split(";")]
            # [(code, valeur)...]
            references[aid].append((cols[4], float(cols[0])))
            url_params[aid].append(
                [
                    ("zone", "region"),
                    ("maille", cols[3]),
                    ("zone_id", "1"),
                    ("addcode", "1"),
                    ("nochart", "1"),
                ]
            )
            if cols[1] != "*":
                url_params[aid][-1].append(
                    ("filter", "facture_energie.{}".format(cols[1]))
                )
            if cols[2] != "*":
                url_params[aid][-1].append(
                    ("filter", "facture_usage.{}".format(cols[2]))
                )

with (here / "data" / "test_b120_production_enr.csv").open() as lines:
    next(lines)
    for idx, line in enumerate(islice(lines, 0, None, STEP)):
        if LIMIT and idx > LIMIT:
            break
        cols = [field.strip() for field in line.split(";")]
        # [(code, valeur)...]
        references["b120"].append((cols[4], float(cols[0])))
        url_params["b120"].append(
            [
                ("zone", "region"),
                ("maille", cols[3]),
                ("zone_id", "1"),
                ("addcode", "1"),
                ("annee", "2015"),
                ("nochart", "1"),
            ]
        )
        if cols[2] != "*":
            url_params["b120"][-1].append(
                ("filter", "type_prod_enr.{}".format(cols[2]))
            )


with (here / "data" / "test_stock-carbone.csv").open() as lines:
    next(lines)
    for idx, line in enumerate(islice(lines, 0, None, STEP)):
        if LIMIT and idx > LIMIT:
            break
        cols = [field.strip() for field in line.split(";")]
        # [(code, valeur)...]
        references["a16"].append((cols[4], float(cols[0])))
        url_params["a16"].append(
            [
                ("zone", "region"),
                ("maille", cols[3]),
                ("zone_id", "1"),
                ("addcode", "1"),
                ("nochart", "1"),
            ]
        )
        if cols[2] != "*":
            url_params["a16"][-1].append(("filter", "type_occsol.{}".format(cols[2])))

# découpage des URL de tests par CHUNK
chunked_params = {}
# découpage des identifiants pour la sortie de pytest
chunked_ids = {}
for acode in analysis_codes:
    chunked_params[acode] = [
        list(zip(url_params[acode][i : i + CHUNK], references[acode][i : i + CHUNK]))
        for i in range(0, len(url_params[acode]), CHUNK)
    ]
    chunked_ids[acode] = [
        "line {}:{}".format(i, min(i + CHUNK, len(url_params[acode])))
        for i in range(0, len(url_params[acode]), CHUNK)
    ]


def get_api_values(client_anonymous, acode, metadict, params_ref, loop):
    """Demande à l'API les résultats de l'analyse `acode`
    et teste chacune des valeurs issues des fichiers de tests

    Parameters
    ----------
    acode: int
        identifiant de l'analyse
    metadict: dict
        association code territorialsynthesis / identifiant dans la base
    params_ref: dict
        portion de valeurs de références issues des fichiers de test
    """
    terrivalues = loop.run_until_complete(
        # valeurs récupérées dans l'api pour la maille de test
        *[
            client_anonymous.get(
                "/api/analysis/{}/data".format(metadict[acode]["id"]), params=params
            )
            for params, _ in params_ref
        ]
    )
    terrivalues = loop.run_until_complete(*[res.json() for res in terrivalues])
    for results, (params, refvalues) in zip(terrivalues, params_ref):
        terrivalue = [
            item["val"] for item in results["map"] if item["code"] == refvalues[0]
        ][0]
        if terrivalue is None:
            # donnée confidentielle ou division par 0
            continue
        assert isclose(refvalues[1], terrivalue, atol=metadict[acode]["decimals"])


@pytest.mark.parametrize("params_ref", chunked_params["a81"], ids=chunked_ids["a81"])
def test_analysis_a81(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "a81", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["a83"], ids=chunked_ids["a83"])
def test_analysis_a83(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "a83", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["a84"], ids=chunked_ids["a84"])
def test_analysis_a84(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "a84", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["b11"], ids=chunked_ids["b11"])
def test_analysis_b11(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "b11", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["b12"], ids=chunked_ids["b12"])
def test_analysis_b12(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "b12", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["b13"], ids=chunked_ids["b13"])
def test_analysis_b13(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "b13", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["b18"], ids=chunked_ids["b18"])
def test_analysis_b18(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "b18", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["b120"], ids=chunked_ids["b120"])
def test_analysis_b120(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "b120", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["a16"], ids=chunked_ids["a16"])
def test_analysis_a16(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "a16", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["a31"], ids=chunked_ids["a31"])
def test_analysis_a31(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "a31", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["b46"], ids=chunked_ids["b46"])
def test_analysis_b46(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "b46", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["b41"], ids=chunked_ids["b41"])
def test_analysis_b41(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "b41", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["c111"], ids=chunked_ids["c111"])
def test_analysis_c111(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "c111", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["d227"], ids=chunked_ids["d227"])
def test_analysis_d227(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "d227", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["e16"], ids=chunked_ids["e16"])
def test_analysis_e16(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "e16", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["c51"], ids=chunked_ids["c51"])
def test_analysis_c51(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "c51", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["e118"], ids=chunked_ids["e118"])
def test_analysis_e118(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "e118", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["e119"], ids=chunked_ids["e119"])
def test_analysis_e119(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "e119", metadict, params_ref)
    )


@pytest.mark.parametrize("params_ref", chunked_params["e120"], ids=chunked_ids["e120"])
def test_analysis_e120(client_anonymous, metadict, params_ref, loop):
    loop.run_until_complete(
        get_api_values(client_anonymous, "e120", metadict, params_ref)
    )


def test_territorialsynthesis_notes(client_anonymous, metadict):
    """lecture des fichiers de tests et aggrégation par territoire
    car l'api est requêtée par territoire
    """
    refvalues = defaultdict(dict)
    for acode, filename in (
        ("a83", "note_cesba_a83.csv"),
        ("a81", "note_cesba_a81.csv"),
    ):
        with (here / "data" / filename).open() as lines:
            next(lines)
            for line in lines:
                if LIMIT and idx > LIMIT:
                    break
                cols = [field.strip() for field in line.split(";")]
                # {(type_terr, code_terr): [(analyse, note)...}
                refvalues[(cols[1], cols[2])][metadict[acode]["id"]] = float(cols[0])

    # /api/territorialsynthesis/notes/<zone:string>/<zone_id:str>?code=1
    for (zone, zone_id), values in refvalues.items():
        _request, resp = client_anonymous.get(
            f"/api/territorialsynthesis/notes/{zone}/{zone_id}?code=1"
        )
        resp = resp.json
        # on réorganise les données par identifiant d'analyse
        resp = {item["id"]: item["note"] for item in resp}
        # on itère sur les tests
        for aid, note in values.items():
            assert isclose(resp[aid], note, atol=metadict[acode]["decimals"])
