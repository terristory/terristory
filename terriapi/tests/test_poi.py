# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
from pathlib import Path

import pytest

from terriapi.controller import execute

from .conftest import JOHN

here = Path(__file__).resolve().parent


@pytest.fixture
def adding_poi_through_contribution(
    region, poi, client_anonymous, client_basic_user, client_admin_user
):
    # data for contribution
    data_reseau = {
        "id": None,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Accepted value",
        },
        "x": 297357,
        "y": 5772605,
    }

    # with a user, possible to create
    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 200
    response = resp.json
    assert response["status"] == "created"
    edit_id = response["edit_id"]

    # need to accept suggested contribution
    # impossible when not admin
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    assert resp.status == 200
    response = resp.json
    assert (
        "message" in response and response["message"] == "Action accepté avec succès !"
    )
    yield response["result"]


def test_contribution_editing_poi(
    region,
    poi,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    adding_poi_through_contribution,
):
    id_cree = adding_poi_through_contribution
    # data for contribution
    data_reseau = {
        "id": id_cree,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Accepted value",
        },
    }

    # doable with a user
    _req, resp = client_basic_user.put(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    response = resp.json
    assert resp.status == 200

    _req, resp = client_admin_user.get("/api/pytest/admin/poi/contributions")
    assert resp.status == 200
    response = resp.json
    # addition (params) + update
    assert len(response) == 2

    edit_response = next(filter(lambda x: x["action"] == "edit", response))

    # we check no change was made on poi localisation
    assert (
        "geolocalisation" in edit_response
        and edit_response["geolocalisation"] == "distance (m) : 0.00"
    )


def test_modification_poi(
    region, poi, client_anonymous, client_basic_user, client_admin_user
):
    # data for contribution
    data_reseau = {
        "id": None,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Accepted value",
        },
        "x": 297357,
        "y": 5772605,
    }

    _request, resp = client_anonymous.post(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 401
    response = resp.json
    assert response["exception"] == "Unauthorized"

    # with a user, possible to create
    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 200
    response = resp.json
    assert response["status"] == "created"
    edit_id = response["edit_id"]

    # even with a user, impossible to create when non editable
    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi_non_modifiables", json=data_reseau
    )
    assert resp.status == 404

    # need to accept suggested contribution
    # impossible when not admin
    _request, resp = client_anonymous.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    assert resp.status == 401
    _req, resp = client_basic_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    assert resp.status == 401
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    assert resp.status == 200
    response = resp.json
    assert (
        "message" in response and response["message"] == "Action accepté avec succès !"
    )
    id_cree = response["result"]

    # new data
    data_reseau = {
        "id": id_cree,
        "properties": {
            "Nom": "Nouveau réseau",
            "Adresse": "Pas ici en tout cas",
            "Constrained field": "Accepted value",
        },
        "x": 227357,
        "y": 5472605,
    }

    # impossible anonymously
    _request, resp = client_anonymous.put(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    response = resp.json
    assert resp.status == 401

    # doable with a user
    _req, resp = client_basic_user.put(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    response = resp.json
    assert resp.status == 200

    # TODO: check that edition worked

    # with a user, possible to delete
    _req, resp = client_admin_user.delete(
        f"/api/pytest/poi/object/exemple_poi/{id_cree}/"
    )
    assert resp.status == 200

    # impossible to delete layer not editable
    _req, resp = client_admin_user.delete(
        f"/api/pytest/poi/object/exemple_poi_non_modifiables/{id_cree}/"
    )
    assert resp.status == 404

    _req, resp = client_admin_user.get("/api/pytest/admin/poi/contributions")
    response = resp.json
    assert resp.status == 200
    assert len(response) == 3


def test_modification_poi_with_constraints(
    region,
    poi,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    poi_structure_constraints,
):
    # data for contribution with wrong value for constrained field
    data_reseau = {
        "id": None,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Random value",
        },
        "x": 297357,
        "y": 5772605,
    }

    # with a user, possible to create
    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 400
    response = resp.json
    assert (
        response["message"]
        == "Les ajouts ne respectent pas les contraintes en vigueur sur la couche d'équipement (champ Constrained field)."
    )

    # we use a right value
    data_reseau["properties"]["Constrained field"] = "Accepted value"

    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 200
    response = resp.json
    edit_id = response["edit_id"]

    # we accept edition
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    assert resp.status == 200
    response = resp.json
    id_cree = response["result"]

    # updated data still with wrong value in constrained field
    data_reseau = {
        "id": id_cree,
        "properties": {
            "Nom": "Nouveau réseau",
            "Adresse": "Pas ici en tout cas",
            "Constrained field": "Other random value",
        },
        "x": 227357,
        "y": 5472605,
    }

    # impossible with wrong value
    _req, resp = client_basic_user.put(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 400
    response = resp.json
    assert (
        response["message"]
        == "Les modifications ne respectent pas les contraintes en vigueur sur la couche d'équipement (champ Constrained field)."
    )

    # possible
    data_reseau["properties"]["Constrained field"] = "Other accepted value"
    _req, resp = client_basic_user.put(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 200


def test_contribution_accepting_deletion_poi(
    region,
    poi,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    adding_poi_through_contribution,
):
    id_cree = adding_poi_through_contribution
    _req, resp = client_basic_user.delete(
        f"/api/pytest/poi/object/exemple_poi/{id_cree}"
    )
    response = resp.json
    assert response["status"] == "deleted"
    edit_id = response["edit_id"]

    # we still have the data
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/exemple_poi"
    )
    assert poi_resp.status == 200
    response = poi_resp.json
    assert len(response) == 1

    # need to accept suggested contribution
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    assert resp.status == 200
    response = resp.json
    assert (
        "message" in response and response["message"] == "Action accepté avec succès !"
    )

    # we don't have the data anymore
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/exemple_poi"
    )
    assert poi_resp.status == 200
    response = poi_resp.json
    assert len(response) == 0


def test_contribution_refusing_deletion_poi(
    region,
    poi,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    adding_poi_through_contribution,
):
    id_cree = adding_poi_through_contribution
    # data for contribution
    data_reseau = {
        "id": id_cree,
    }

    _req, resp = client_basic_user.delete(
        f"/api/pytest/poi/object/exemple_poi/{id_cree}"
    )
    response = resp.json
    assert response["status"] == "deleted"
    edit_id = response["edit_id"]

    # we still have the data
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/exemple_poi"
    )
    assert poi_resp.status == 200
    response = poi_resp.json
    assert len(response) == 1

    # need to accept suggested contribution
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/refused/"
    )
    assert resp.status == 200
    response = resp.json
    assert (
        "message" in response and response["message"] == "Action refusée avec succès !"
    )

    # we don't have the data anymore
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/exemple_poi"
    )
    assert poi_resp.status == 200
    response = poi_resp.json
    assert len(response) == 1


def test_list_contributions_poi(
    region,
    poi,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    adding_poi_through_contribution,
    loop,
):
    # impossible anonymously
    _request, resp = client_anonymous.get("/api/pytest/admin/poi/contributions")
    assert resp.status == 401

    # impossible with a common user
    _req, resp = client_basic_user.get("/api/pytest/admin/poi/contributions")
    assert resp.status == 401

    # with an admin, possible to get list
    _req, resp = client_admin_user.get("/api/pytest/admin/poi/contributions")
    assert resp.status == 200
    response = resp.json
    assert len(response) == 1

    query = f"""INSERT INTO meta.poi_contributions_contacts 
    VALUES('pytest', $1, 'exemple_poi_non_modifiables', true, NOW());"""
    loop.run_until_complete(execute(query, JOHN["mail"]))
    # possible with a common user with sufficient rights
    _req, resp = client_basic_user.get("/api/pytest/admin/poi/contributions")
    # however, no data as the user was not allowed to watch current contribution
    assert resp.status == 200
    response = resp.json
    assert len(response) == 0

    query = f"""INSERT INTO meta.poi_contributions_contacts 
    VALUES('pytest', $1, 'exemple_poi', true, NOW());"""
    loop.run_until_complete(execute(query, JOHN["mail"]))
    # possible with a common user with sufficient rights
    _req, resp = client_basic_user.get("/api/pytest/admin/poi/contributions")
    assert resp.status == 200
    response = resp.json
    assert len(response) == 1

    loop.run_until_complete(
        execute("DELETE FROM meta.poi_contributions_contacts WHERE region = 'pytest';")
    )


def test_deny_contribution_poi(
    region, poi, client_anonymous, client_basic_user, client_admin_user
):
    # data for contribution
    data_reseau = {
        "id": None,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Accepted value",
        },
        "x": 297357,
        "y": 5772605,
    }

    # with a user, possible to create
    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 200
    response = resp.json
    assert response["status"] == "created"
    edit_id = response["edit_id"]

    # need to accept suggested contribution
    # impossible when not admin
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/refused/"
    )
    assert resp.status == 200
    response = resp.json
    assert (
        "message" in response and response["message"] == "Action refusée avec succès !"
    )


def test_history_poi(
    region,
    poi,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    adding_poi_through_contribution,
):
    id_cree = adding_poi_through_contribution
    # data for contribution
    data_reseau = {
        "id": id_cree,
        "properties": {
            "Nom": "Autre modification",
            "Adresse": "Cool",
            "Constrained field": "Accepted value",
        },
        "x": 297357,
        "y": 5772605,
    }

    # with a user, possible to edit
    _req, resp = client_basic_user.put(
        "/api/pytest/poi/object/exemple_poi", json=data_reseau
    )
    assert resp.status == 200
    response = resp.json
    assert response["status"] == "updated"
    edit_id = response["edit_id"]

    # need to accept suggested contribution
    # impossible when not admin
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    response = resp.json
    assert resp.status == 200
    assert (
        "message" in response and response["message"] == "Action accepté avec succès !"
    )

    # data for contribution
    data_reseau = {
        "id": id_cree,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Accepted value",
        },
        "x": 297357,
        "y": 5772605,
    }

    # with a user, possible to create
    _req, resp = client_basic_user.delete(
        f"/api/pytest/poi/object/exemple_poi/{id_cree}"
    )
    assert resp.status == 200
    response = resp.json
    assert response["status"] == "deleted"
    edit_id = response["edit_id"]

    # need to accept suggested contribution
    # impossible when not admin
    _req, resp = client_admin_user.post(
        f"/api/pytest/admin/poi/contribution/{edit_id}/accepted/"
    )
    assert resp.status == 200
    response = resp.json
    assert (
        "message" in response and response["message"] == "Action accepté avec succès !"
    )

    # impossible to access history when not admin
    _req, resp = client_basic_user.get("/api/pytest/poi/layers/history")
    assert resp.status == 401

    # possible to access history when admin
    _req, resp = client_admin_user.get("/api/pytest/poi/layers/history")
    assert resp.status == 200

    # we check that previous actions have been registered inside history
    history = resp.json
    actions_performed_to_check = ["creation", "modification", "suppression"]
    for h in history:
        if h["id"] == id_cree and h["action"] in actions_performed_to_check:
            actions_performed_to_check.remove(h["action"])
    assert len(actions_performed_to_check) == 0


def test_edit_poi_layer_status(
    region, poi, client_anonymous, client_basic_user, client_admin_user
):
    # impossible to edit layer not editable
    _req, resp = client_admin_user.put(
        "/api/pytest/poi/object/exemple_poi_non_modifiables", json={"droit": True}
    )
    assert resp.status == 404

    # impossible to edit non existing layer
    _req, resp = client_admin_user.put(
        "/api/pytest/poi/layer/unexisting_layer/rights", json={"droit": True}
    )
    assert resp.status == 404

    # impossible to edit layer if no data provided
    _req, resp = client_admin_user.put(
        "/api/pytest/poi/layer/exemple_poi_non_modifiables/rights"
    )
    assert resp.status == 404

    # possible to edit layer status
    _req, resp = client_admin_user.put(
        "/api/pytest/poi/layer/exemple_poi_non_modifiables/rights", json={"droit": True}
    )
    assert resp.status == 200

    # not reachable to anonymous users
    data_reseau = {
        "id": None,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Accepted value",
        },
        "x": 297357,
        "y": 5772605,
    }

    # with a user, possible to create
    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi_non_modifiables", json=data_reseau
    )
    assert resp.status == 200
    response = resp.json
    assert response["edit_id"] > 0

    # possible to edit layer status
    _req, resp = client_admin_user.put(
        "/api/pytest/poi/layer/exemple_poi_non_modifiables/rights",
        json={"droit": False},
    )
    assert resp.status == 200

    # not reachable to anonymous users
    data_reseau = {
        "id": None,
        "properties": {
            "Nom": "Réseau test",
            "Adresse": "Quelque part",
            "Constrained field": "Accepted value",
        },
        "x": 297357,
        "y": 5772605,
    }

    # with a user, not possible anymore to edit
    _req, resp = client_basic_user.post(
        "/api/pytest/poi/object/exemple_poi_non_modifiables", json=data_reseau
    )
    assert resp.status == 404


def test_poi_layer(region, client_anonymous, client_basic_user, client_admin_user, poi):
    POI_CSV_FILE = here / "data" / "test_poi.csv"
    EMPTY_PDF = here / "data" / "empty.pdf"
    POI_SVG = here / "data" / "poi.svg"
    nom_table = "test_poi"
    data = {
        "nomEquipement": "Équipement de test",
        "nomTable": nom_table,
        "nomRubrique": "Déchets",
        "couleur": "#420069",
        "typeGeom": "Point",
        "afficherStatut": "true",
        "ancrageIcone": "centre",
        "donneesExportables": "false",
        "creditsDataSources": '[{"name":"AURA-EE","url":"terristory.fr"}]',
        "creditsDataProducers": "[]",
    }
    # not reachable to anonymous users
    _request, resp = client_anonymous.post("/api/pytest/poi/layer/add", data=data)
    assert resp.status == 401
    response = resp.json
    assert response["exception"] == "Unauthorized"

    with (
        open(POI_CSV_FILE, "rb") as f1,
        open(EMPTY_PDF, "rb") as f2,
        open(POI_SVG, "rb") as f3,
        open(POI_SVG, "rb") as f4,
        open(POI_SVG, "rb") as f5,
    ):
        # with an admin user, possible to create
        _req, resp = client_admin_user.post(
            "/api/pytest/poi/layer/add",
            data=data,
            files={
                "fichier": f1,
                "fichierPdf": f2,
                "fichierIcone": f3,
                "fichierIconeProjet": f4,
                "fichierIconeLegende": f5,
            },
        )
        assert resp.status == 200

    # check that layer addition worked
    _request, resp = client_anonymous.get("/api/pytest/poi/layers")
    assert resp.status == 200
    layer = [l for l in resp.json if l["nom"] == nom_table]
    assert len(layer) == 1, "New layer was not found"
    layer = layer[0]
    assert (
        layer["label"],
        layer["nom"],
        layer["theme"],
        layer["couleur"],
        layer["type_geom"],
        layer["ancrage_icone"],
        layer["credits_data_sources"],
        layer["credits_data_producers"],
    ) == (
        data["nomEquipement"],
        data["nomTable"],
        data["nomRubrique"],
        data["couleur"],
        data["typeGeom"],
        data["ancrageIcone"],
        data["creditsDataSources"],
        data["creditsDataProducers"],
    )
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/" + nom_table
    )
    assert poi_resp.status == 401, "POI layer should not be exportable"

    # modification
    data = {
        "nomEquipement": "Équipement de test",
        "nomRubrique": "Déchets",
        "couleur": "#373737",
        "typeGeom": "Point",
        "afficherStatut": "false",
        "ancrageIcone": "milieu_bas",
        "donneesExportables": "true",
        "creditsDataSources": "[]",
        "creditsDataProducers": '[{"name":"AURA-EE","url":"terristory.fr"}]',
    }
    # impossible anonymously
    _request, resp = client_anonymous.put(
        "/api/pytest/poi/layer/" + nom_table, data=data
    )
    assert resp.status == 401
    # impossible with invalid table name
    _req, resp = client_admin_user.put(
        "/api/pytest/poi/layer/unexisting_poi_37564", data=data
    )
    assert resp.status == 401
    # doable with an admin user
    _req, resp = client_admin_user.put("/api/pytest/poi/layer/" + nom_table, data=data)
    assert resp.status == 200
    response = resp.json

    # check that layer edition worked
    _request, resp = client_anonymous.get("/api/pytest/poi/layers")
    assert resp.status == 200
    layer = [l for l in resp.json if l["nom"] == nom_table]
    assert len(layer) == 1, "New layer was not found"
    layer = layer[0]
    assert (
        layer["label"],
        layer["theme"],
        layer["couleur"],
        layer["type_geom"],
        layer["ancrage_icone"],
        layer["credits_data_sources"],
        layer["credits_data_producers"],
    ) == (
        data["nomEquipement"],
        data["nomRubrique"],
        data["couleur"],
        data["typeGeom"],
        data["ancrageIcone"],
        data["creditsDataSources"],
        data["creditsDataProducers"],
    )
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/" + nom_table
    )
    assert poi_resp.status == 200
    poi_resp = poi_resp.json
    poi_resp = json.loads(poi_resp[0]["proprietes"])
    assert poi_resp["nom"] == "poi_1"

    # try exporting for a non-existing zone
    non_existing_area = "non_existing_area"
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/" + nom_table + "?zone=" + non_existing_area
    )
    assert poi_resp.status == 400
    failing_message = poi_resp.json
    assert failing_message["status"] == f"invalid parameter zone: {non_existing_area}"

    # 404 when fetching an unexisting poi layer
    _request, poi_resp = client_anonymous.get(
        "/api/pytest/poi/layer/export/unexisting_poi_76352"
    )
    assert poi_resp.status == 404


def test_deleting_poi_layer(
    region, indicators, client_anonymous, client_admin_user, poi
):
    """
    Test the ability to delete POI layer
    """

    # initial data
    _request, resp = client_anonymous.get("/api/pytest/poi/layers/")
    assert resp.status == 200
    initial_analyses = resp.json
    assert len(initial_analyses) > 0
    poi_to_delete = initial_analyses[-1]

    # not allowed for anonymous user
    _request, resp = client_anonymous.delete(
        f"/api/pytest/poi/layer/{poi_to_delete['id']}/"
    )
    assert resp.status == 401

    # wrong region
    _req, resp = client_admin_user.delete(
        f"/api/auvergne-rhone-alpes/poi/layer/{poi_to_delete['id']}/"
    )
    assert resp.status == 401

    # wrong ID
    _req, resp = client_admin_user.delete(f"/api/pytest/poi/layer/-1/")
    assert resp.status == 404

    # wrong ID again
    _req, resp = client_admin_user.delete("/api/pytest/poi/layer/autre_couche_poi/")
    assert resp.status == 500

    # accepted for admin
    _req, resp = client_admin_user.delete(
        f"/api/pytest/poi/layer/{poi_to_delete['id']}/"
    )
    assert resp.status == 200

    _request, resp = client_anonymous.get("/api/pytest/poi/layers/")
    assert resp.status == 200
    analyses = resp.json
    assert len(analyses) > 0

    # we check that the layer is not present anymore
    assert poi_to_delete["id"] not in [l["id"] for l in analyses]


def test_ordering_poi(region, indicators, client_anonymous, client_admin_user, poi):
    """
    Test the ability to change themes order through API
    """
    # retrieving ui themes from database
    _request, resp = client_anonymous.get("/api/pytest/poi/themes/")
    uithemes_data = resp.json
    uithemes = list(map(lambda x: x["label"], uithemes_data))
    # we reverse the themes order
    uithemes = uithemes[::-1]
    assert resp.status == 200

    data = {
        "themes_analyses_ordres": {},
        "themes_ordres": uithemes,
    }
    # not allowed for anonymous user
    _request, resp = client_anonymous.put("/api/pytest/poi/layers/order/", json=data)
    assert resp.status == 401

    # accepted for admin but fails without data
    _req, resp = client_admin_user.put("/api/pytest/poi/layers/order/")
    assert resp.status == 400
    # accepted for admin
    _req, resp = client_admin_user.put("/api/pytest/poi/layers/order/", json=data)
    assert resp.status == 200

    # we retrieve analyses from api
    _request, resp = client_anonymous.get("/api/pytest/poi/themes/")
    assert resp.status == 200
    analyses = resp.json
    assert len(analyses) > 0

    new_order_themes = list(map(lambda x: x["label"], analyses))
    assert new_order_themes == uithemes


def test_renaming_poi_groups(
    region, indicators, client_anonymous, client_admin_user, poi
):
    """
    Test the ability to rename indicator groups through API
    """
    # calling API without rights
    _request, resp = client_anonymous.put("/api/pytest/poi/theme/rename/")
    assert resp.status == 401

    # calling API without rights on this region
    _req, resp = client_admin_user.put("/api/auvergne-rhone-alpes/poi/theme/rename/")
    assert resp.status == 401

    # renaming the category
    _req, resp = client_admin_user.put(
        "/api/pytest/poi/theme/rename/",
        json={
            "old_name": "Mobilité",
            "new_name": "Mobilité et transports",
        },
    )
    assert resp.status == 200
    uithemes_data = resp.json
    assert "status" in uithemes_data and uithemes_data["status"] == "updated"

    # not allowed for anonymous user
    _request, resp = client_anonymous.get("/api/pytest/poi/themes/")
    assert resp.status == 200
    new_data = resp.json
    assert len(new_data) > 0
    assert (
        new_data[0]["label"] == "Mobilité et transports"
        and new_data[1]["label"] == "Infrastructures"
    ) or (
        new_data[1]["label"] == "Mobilité et transports"
        and new_data[0]["label"] == "Infrastructures"
    )
