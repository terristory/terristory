# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import os

import pandas as pd
import pytest
import requests_mock
from tests.utils import MockSettings, mock_async_function

from terriapi import settings
from terriapi.controller import execute, fetch, get_pg_engine
from terriapi.controller.pcaet_ademe import insert_relative_methodology_pcaet
from terriapi.controller.strategy_actions.constants import RenewableProd


def test_update_pcaet_table(region, mocker, client_admin_user, caplog, loop):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    mocker.patch(
        "terriapi.controller.pcaet_ademe.PCAET_META_TABLE", "pytest.pcaet_meta"
    )
    loop.run_until_complete(
        execute(
            """
        INSERT INTO pytest.pcaet
            VALUES ('202020202', 'previous_pcaet_data', '', 0, 0, TRUE, 0, 'pytest');

        INSERT INTO pytest.departement (nom, code, communes)
            VALUES ('Département de test', '2C', '{"69420"}');
        INSERT INTO pytest.epci (nom, code) VALUES ('EPCI de test', '200000001');
        """
        )
    )

    csv_url_pcaet_pec_seq_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-2/data-files/Demarches_PCAET_V1_pec_seq.csv"
    csv_url_pcaet_enr_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-3/data-files/Demarches_PCAET_V1_enr.csv"
    csv_url_pcaet_polluant_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-4/data-files/Demarches_PCAET_V1_polluant.csv"
    csv_url_pcaet_meta_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-1-entete/data-files/Demarches_PCAET_V1_entete.csv"
    csv_url_pcaet_pec_seq_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-2/data-files/Demarches_PCAET_V2_pec_seq.csv"
    csv_url_pcaet_enr_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-3/data-files/Demarches_PCAET_V2_enr.csv"
    csv_url_pcaet_polluant_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-4/data-files/Demarches_PCAET_V2_polluant.csv"
    csv_url_pcaet_meta_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-1-entete/data-files/Demarches_PCAET_V2_entete.csv"
    # In these CSV, all values are at 1000
    csv_content_pec_seq_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;SIREN collectivites_coporteuses;Collectivités porteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;PEC_Emission_GES_Annee_Compta;PEC_GES_1_1;PEC_GES_1_2;PEC_GES_1_3;PEC_GES_1_4;PEC_GES_1_5;PEC_GES_2_1;PEC_GES_2_2;PEC_GES_2_3;PEC_GES_2_4;PEC_GES_2_5;PEC_GES_3_1;PEC_GES_3_2;PEC_GES_3_3;PEC_GES_3_4;PEC_GES_3_5;PEC_GES_4_1;PEC_GES_4_2;PEC_GES_4_3;PEC_GES_4_4;PEC_GES_4_5;PEC_GES_5_1;PEC_GES_5_2;PEC_GES_5_3;PEC_GES_5_4;PEC_GES_5_5;PEC_GES_6_1;PEC_GES_6_2;PEC_GES_6_3;PEC_GES_6_4;PEC_GES_6_5;PEC_GES_7_1;PEC_GES_7_2;PEC_GES_7_3;PEC_GES_7_4;PEC_GES_7_5;PEC_GES_8_1;PEC_GES_8_2;PEC_GES_8_3;PEC_GES_8_4;PEC_GES_8_5;PEC_Consommation_Annee_Compta;PEC_CONSO_1_1;PEC_CONSO_1_2;PEC_CONSO_1_3;PEC_CONSO_1_4;PEC_CONSO_1_5;PEC_CONSO_2_1;PEC_CONSO_2_2;PEC_CONSO_2_3;PEC_CONSO_2_4;PEC_CONSO_2_5;PEC_CONSO_3_1;PEC_CONSO_3_2;PEC_CONSO_3_3;PEC_CONSO_3_4;PEC_CONSO_3_5;PEC_CONSO_4_1;PEC_CONSO_4_2;PEC_CONSO_4_3;PEC_CONSO_4_4;PEC_CONSO_4_5;PEC_CONSO_5_1;PEC_CONSO_5_2;PEC_CONSO_5_3;PEC_CONSO_5_4;PEC_CONSO_5_5;PEC_CONSO_6_1;PEC_CONSO_6_2;PEC_CONSO_6_3;PEC_CONSO_6_4;PEC_CONSO_6_5;PEC_CONSO_7_1;PEC_CONSO_7_2;PEC_CONSO_7_3;PEC_CONSO_7_4;PEC_CONSO_7_5;PEC_CONSO_8_1;PEC_CONSO_8_2;PEC_CONSO_8_3;PEC_CONSO_8_4;PEC_CONSO_8_5;PEC_GES_CONSO_commentaire
        500;"15/08/2016";"07/09/2019";"11/07/2020";"PCAET";"PCAET";"Ceci est un test";"200069420";"Trifouilly-les-oies";2;"Mise en oeuvre";"Jean";"mail@mail.com";"Michel";"";"06/01/2017";"06/01/2017";"06/01/2017";"O";"";"";1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;""
    """
    csv_content_pec_seq_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;SIREN collectivites_coporteuses;Collectivités porteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;PEC_Emission_GES_Annee_Compta;PEC_GES_1_1;PEC_GES_1_3;PEC_GES_1_6;PEC_GES_1_7;PEC_GES_2_1;PEC_GES_2_3;PEC_GES_2_6;PEC_GES_2_7;PEC_GES_3_1;PEC_GES_3_3;PEC_GES_3_6;PEC_GES_3_7;PEC_GES_4_1;PEC_GES_4_3;PEC_GES_4_6;PEC_GES_4_7;PEC_GES_5_1;PEC_GES_5_3;PEC_GES_5_6;PEC_GES_5_7;PEC_GES_6_1;PEC_GES_6_3;PEC_GES_6_6;PEC_GES_6_7;PEC_GES_7_1;PEC_GES_7_3;PEC_GES_7_6;PEC_GES_7_7;PEC_GES_8_1;PEC_GES_8_3;PEC_GES_8_6;PEC_GES_8_7;PEC_Consommation_Annee_Compta;PEC_CONSO_1_1;PEC_CONSO_1_3;PEC_CONSO_1_6;PEC_CONSO_1_7;PEC_CONSO_2_1;PEC_CONSO_2_3;PEC_CONSO_2_6;PEC_CONSO_2_7;PEC_CONSO_3_1;PEC_CONSO_3_3;PEC_CONSO_3_6;PEC_CONSO_3_7;PEC_CONSO_4_1;PEC_CONSO_4_3;PEC_CONSO_4_6;PEC_CONSO_4_7;PEC_CONSO_5_1;PEC_CONSO_5_3;PEC_CONSO_5_6;PEC_CONSO_5_7;PEC_CONSO_6_1;PEC_CONSO_6_3;PEC_CONSO_6_6;PEC_CONSO_6_7;PEC_CONSO_7_1;PEC_CONSO_7_3;PEC_CONSO_7_6;PEC_CONSO_7_7;PEC_CONSO_8_1;PEC_CONSO_8_3;PEC_CONSO_8_6;PEC_CONSO_8_7;PEC_GES_CONSO_commentaire
        800;"18/12/2019";"18/01/2020";"02/02/2020";"PCAET";"PCAET";"Ceci est un test";"200000008";"Trifouilly-les-oies";3;"Mise en oeuvre";"Christian";"mail@mail.com";"Benoit";"";"02/02/2020";"02/02/2020";"02/02/2020";"O";"";"";1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;""
    """
    csv_content_enr_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;Collectivités porteuses;SIREN collectivites_coporteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;ENR_1_Diag_Annee_Compta;ENR_2_Diag_Annee_Compta;ENR_3_Diag_Annee_Compta;ENR_4_Diag_Annee_Compta;ENR_5_Diag_Annee_Compta;ENR_6_Diag_Annee_Compta;ENR_7_Diag_Annee_Compta;ENR_8_Diag_Annee_Compta;ENR_9_Diag_Annee_Compta;ENR_10_Diag_Annee_Compta;ENR_11_Diag_Annee_Compta;ENR_12_Diag_Annee_Compta;ENR_13_Diag_Annee_Compta;ENR_14_Diag_Annee_Compta;ENR_1_1_Production;ENR_1_2_Production;ENR_1_3_Production;ENR_1_4_Production;ENR_1_5_Production;ENR_2_1_Production;ENR_2_2_Production;ENR_2_3_Production;ENR_2_4_Production;ENR_2_5_Production;ENR_3_1_Production;ENR_3_2_Production;ENR_3_3_Production;ENR_3_4_Production;ENR_3_5_Production;ENR_4_1_Production;ENR_4_2_Production;ENR_4_3_Production;ENR_4_4_Production;ENR_4_5_Production;ENR_5_1_Production;ENR_5_2_Production;ENR_5_3_Production;ENR_5_4_Production;ENR_5_5_Production;ENR_6_1_Production;ENR_6_2_Production;ENR_6_3_Production;ENR_6_4_Production;ENR_6_5_Production;ENR_7_1_Production;ENR_7_2_Production;ENR_7_3_Production;ENR_7_4_Production;ENR_7_5_Production;ENR_8_1_Production;ENR_8_2_Production;ENR_8_3_Production;ENR_8_4_Production;ENR_8_5_Production;ENR_9_1_Production;ENR_9_2_Production;ENR_9_3_Production;ENR_9_4_Production;ENR_9_5_Production;ENR_10_1_Production;ENR_10_2_Production;ENR_10_3_Production;ENR_10_4_Production;ENR_10_5_Production;ENR_11_1_Production;ENR_11_2_Production;ENR_11_3_Production;ENR_11_4_Production;ENR_11_5_Production;ENR_12_1_Production;ENR_12_2_Production;ENR_12_3_Production;ENR_12_4_Production;ENR_12_5_Production;ENR_13_1_Production;ENR_13_2_Production;ENR_13_3_Production;ENR_13_4_Production;ENR_13_5_Production;ENR_14_1_Production;ENR_14_2_Production;ENR_14_3_Production;ENR_14_4_Production;ENR_14_5_Production;ENR_1_2_Consommation;ENR_1_3_Consommation;ENR_1_4_Consommation;ENR_1_5_Consommation;ENR_2_2_Consommation;ENR_2_3_Consommation;ENR_2_4_Consommation;ENR_2_5_Consommation;ENR_3_2_Consommation;ENR_3_3_Consommation;ENR_3_4_Consommation;ENR_3_5_Consommation;ENR_4_2_Consommation;ENR_4_3_Consommation;ENR_4_4_Consommation;ENR_4_5_Consommation;ENR_5_2_Consommation;ENR_5_3_Consommation;ENR_5_4_Consommation;ENR_5_5_Consommation;ENR_6_2_Consommation;ENR_6_3_Consommation;ENR_6_4_Consommation;ENR_6_5_Consommation;ENR_7_2_Consommation;ENR_7_3_Consommation;ENR_7_4_Consommation;ENR_7_5_Consommation;ENR_8_2_Consommation;ENR_8_3_Consommation;ENR_8_4_Consommation;ENR_8_5_Consommation;ENR_9_2_Consommation;ENR_9_3_Consommation;ENR_9_4_Consommation;ENR_9_5_Consommation;ENR_10_2_Consommation;ENR_10_3_Consommation;ENR_10_4_Consommation;ENR_10_5_Consommation;ENR_11_2_Consommation;ENR_11_3_Consommation;ENR_11_4_Consommation;ENR_11_5_Consommation;ENR_12_2_Consommation;ENR_12_3_Consommation;ENR_12_4_Consommation;ENR_12_5_Consommation;ENR_13_2_Consommation;ENR_13_3_Consommation;ENR_13_4_Consommation;ENR_13_5_Consommation;ENR_14_2_Consommation;ENR_14_3_Consommation;ENR_14_4_Consommation;ENR_14_5_Consommation;ENR_valorisation_recuperation_2;ENR_valorisation_recuperation_3;ENR_valorisation_recuperation_4;ENR_valorisation_recuperation_5;ENR_valorisation_stockage_2;ENR_valorisation_stockage_3;ENR_valorisation_stockage_4;ENR_valorisation_stockage_5;ENR_commentaire
        800;"20/10/2017";"10/02/2018";"20/10/2019";"PCAET";"PCAET";"Ceci est un test";"Trifouilly-les-oies";"200000008";3;"Mise en oeuvre";"Jean-Claude";"mail@mail.com";"Richard";"";"20/08/15";"20/02/2018";"01/01/2019";"O";"PLUI";"";1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;"test"
    """
    csv_content_enr_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;SIREN collectivites_coporteuses;Collectivités porteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;ENR_1_Diag_Annee_Compta;ENR_2_Diag_Annee_Compta;ENR_3_Diag_Annee_Compta;ENR_4_Diag_Annee_Compta;ENR_5_Diag_Annee_Compta;ENR_6_Diag_Annee_Compta;ENR_7_Diag_Annee_Compta;ENR_8_Diag_Annee_Compta;ENR_9_Diag_Annee_Compta;ENR_10_Diag_Annee_Compta;ENR_11_Diag_Annee_Compta;ENR_12_Diag_Annee_Compta;ENR_13_Diag_Annee_Compta;ENR_14_Diag_Annee_Compta;ENR_1_1_Production;ENR_1_3_Production;ENR_1_6_Production;ENR_1_7_Production;ENR_2_1_Production;ENR_2_3_Production;ENR_2_6_Production;ENR_2_7_Production;ENR_3_1_Production;ENR_3_3_Production;ENR_3_6_Production;ENR_3_7_Production;ENR_4_1_Production;ENR_4_3_Production;ENR_4_6_Production;ENR_4_7_Production;ENR_5_1_Production;ENR_5_3_Production;ENR_5_6_Production;ENR_5_7_Production;ENR_6_1_Production;ENR_6_3_Production;ENR_6_6_Production;ENR_6_7_Production;ENR_7_1_Production;ENR_7_3_Production;ENR_7_6_Production;ENR_7_7_Production;ENR_13_1_Production;ENR_13_3_Production;ENR_13_6_Production;ENR_13_7_Production;ENR_8_1_Consommation;ENR_8_3_Consommation;ENR_8_6_Consommation;ENR_8_7_Consommation;ENR_9_1_Consommation;ENR_9_3_Consommation;ENR_9_6_Consommation;ENR_9_7_Consommation;ENR_10_1_Consommation;ENR_10_3_Consommation;ENR_10_6_Consommation;ENR_10_7_Consommation;ENR_11_1_Consommation;ENR_11_3_Consommation;ENR_11_6_Consommation;ENR_11_7_Consommation;ENR_12_1_Consommation;ENR_12_3_Consommation;ENR_12_6_Consommation;ENR_12_7_Consommation;ENR_14_1_Consommation;ENR_14_3_Consommation;ENR_14_6_Consommation;ENR_14_7_Consommation;ENR_valorisation_recuperation_3;ENR_valorisation_recuperation_6;ENR_valorisation_recuperation_7;ENR_valorisation_stockage_3;ENR_valorisation_stockage_6;ENR_valorisation_stockage_7;ENR_commentaire
        600;"20/10/2017";"10/02/2018";"20/10/2019";"PCAET";"PCAET";"Ceci est un test";"200000001";"Trifouilly-les-oies";4;"Mise en oeuvre";"Lucas";"mail@mail.com";"Alfred";"";"20/08/15";"20/08/15";"20/08/15";"O";"";"";1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000""
    """
    csv_content_polluant_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;POL_1_1_1;POL_1_1_2;POL_1_1_3;POL_1_1_4;POL_1_1_5;POL_1_1_6;POL_1_2_1;POL_1_2_2;POL_1_2_3;POL_1_2_4;POL_1_2_5;POL_1_2_6;POL_1_3_1;POL_1_3_2;POL_1_3_3;POL_1_3_4;POL_1_3_5;POL_1_3_6;POL_1_4_1;POL_1_4_2;POL_1_4_3;POL_1_4_4;POL_1_4_5;POL_1_4_6;POL_1_5_1;POL_1_5_2;POL_1_5_3;POL_1_5_4;POL_1_5_5;POL_1_5_6;POL_2_1_1;POL_2_1_2;POL_2_1_3;POL_2_1_4;POL_2_1_5;POL_2_1_6;POL_2_2_1;POL_2_2_2;POL_2_2_3;POL_2_2_4;POL_2_2_5;POL_2_2_6;POL_2_3_1;POL_2_3_2;POL_2_3_3;POL_2_3_4;POL_2_3_5;POL_2_3_6;POL_2_4_1;POL_2_4_2;POL_2_4_3;POL_2_4_4;POL_2_4_5;POL_2_4_6;POL_2_5_1;POL_2_5_2;POL_2_5_3;POL_2_5_4;POL_2_5_5;POL_2_5_6;POL_3_1_1;POL_3_1_2;POL_3_1_3;POL_3_1_4;POL_3_1_5;POL_3_1_6;POL_3_2_1;POL_3_2_2;POL_3_2_3;POL_3_2_4;POL_3_2_5;POL_3_2_6;POL_3_3_1;POL_3_3_2;POL_3_3_3;POL_3_3_4;POL_3_3_5;POL_3_3_6;POL_3_4_1;POL_3_4_2;POL_3_4_3;POL_3_4_4;POL_3_4_5;POL_3_4_6;POL_3_5_1;POL_3_5_2;POL_3_5_3;POL_3_5_4;POL_3_5_5;POL_3_5_6;POL_4_1_1;POL_4_1_2;POL_4_1_3;POL_4_1_4;POL_4_1_5;POL_4_1_6;POL_4_2_1;POL_4_2_2;POL_4_2_3;POL_4_2_4;POL_4_2_5;POL_4_2_6;POL_4_3_1;POL_4_3_2;POL_4_3_3;POL_4_3_4;POL_4_3_5;POL_4_3_6;POL_4_4_1;POL_4_4_2;POL_4_4_3;POL_4_4_4;POL_4_4_5;POL_4_4_6;POL_4_5_1;POL_4_5_2;POL_4_5_3;POL_4_5_4;POL_4_5_5;POL_4_5_6;POL_5_1_1;POL_5_1_2;POL_5_1_3;POL_5_1_4;POL_5_1_5;POL_5_1_6;POL_5_2_1;POL_5_2_2;POL_5_2_3;POL_5_2_4;POL_5_2_5;POL_5_2_6;POL_5_3_1;POL_5_3_2;POL_5_3_3;POL_5_3_4;POL_5_3_5;POL_5_3_6;POL_5_4_1;POL_5_4_2;POL_5_4_3;POL_5_4_4;POL_5_4_5;POL_5_4_6;POL_5_5_1;POL_5_5_2;POL_5_5_3;POL_5_5_4;POL_5_5_5;POL_5_5_6;POL_6_1_1;POL_6_1_2;POL_6_1_3;POL_6_1_4;POL_6_1_5;POL_6_1_6;POL_6_2_1;POL_6_2_2;POL_6_2_3;POL_6_2_4;POL_6_2_5;POL_6_2_6;POL_6_3_1;POL_6_3_2;POL_6_3_3;POL_6_3_4;POL_6_3_5;POL_6_3_6;POL_6_4_1;POL_6_4_2;POL_6_4_3;POL_6_4_4;POL_6_4_5;POL_6_4_6;POL_6_5_1;POL_6_5_2;POL_6_5_3;POL_6_5_4;POL_6_5_5;POL_6_5_6;POL_7_1_1;POL_7_1_2;POL_7_1_3;POL_7_1_4;POL_7_1_5;POL_7_1_6;POL_7_2_1;POL_7_2_2;POL_7_2_3;POL_7_2_4;POL_7_2_5;POL_7_2_6;POL_7_3_1;POL_7_3_2;POL_7_3_3;POL_7_3_4;POL_7_3_5;POL_7_3_6;POL_7_4_1;POL_7_4_2;POL_7_4_3;POL_7_4_4;POL_7_4_5;POL_7_4_6;POL_7_5_1;POL_7_5_2;POL_7_5_3;POL_7_5_4;POL_7_5_5;POL_7_5_6;POL_8_1_1;POL_8_1_2;POL_8_1_3;POL_8_1_4;POL_8_1_5;POL_8_1_6;POL_8_2_1;POL_8_2_2;POL_8_2_3;POL_8_2_4;POL_8_2_5;POL_8_2_6;POL_8_3_1;POL_8_3_2;POL_8_3_3;POL_8_3_4;POL_8_3_5;POL_8_3_6;POL_8_4_1;POL_8_4_2;POL_8_4_3;POL_8_4_4;POL_8_4_5;POL_8_4_6;POL_8_5_1;POL_8_5_2;POL_8_5_3;POL_8_5_4;POL_8_5_5;POL_8_5_6;POL_1_Diag_Annee_Compta;POL_2_Diag_Annee_Compta;POL_3_Diag_Annee_Compta;POL_4_Diag_Annee_Compta;POL_5_Diag_Annee_Compta;POL_6_Diag_Annee_Compta;POL_Commentaire
        700;"15/08/2016";"07/09/2019";"11/07/2020";"PCAET";"PCAET";"Ceci est un test";"200069420";"Trifouilly-les-oies";2;"Mise en oeuvre";"Ursule";"mail@mail.com";"Géraldine";"";"20/08/15";"20/08/15";"20/08/15";"O";"";"";1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;2015;2015;2015;2015;2015;2015;""
    """  # polluant_V1 should be overwritten by polluant_V2
    csv_content_polluant_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;POL_1_1_1;POL_1_1_2;POL_1_1_3;POL_1_1_4;POL_1_1_5;POL_1_1_6;POL_1_3_1;POL_1_3_2;POL_1_3_3;POL_1_3_4;POL_1_3_5;POL_1_3_6;POL_1_6_1;POL_1_6_2;POL_1_6_3;POL_1_6_4;POL_1_6_5;POL_1_6_6;POL_1_7_1;POL_1_7_2;POL_1_7_3;POL_1_7_4;POL_1_7_5;POL_1_7_6;POL_2_1_1;POL_2_1_2;POL_2_1_3;POL_2_1_4;POL_2_1_5;POL_2_1_6;POL_2_3_1;POL_2_3_2;POL_2_3_3;POL_2_3_4;POL_2_3_5;POL_2_3_6;POL_2_6_1;POL_2_6_2;POL_2_6_3;POL_2_6_4;POL_2_6_5;POL_2_6_6;POL_2_7_1;POL_2_7_2;POL_2_7_3;POL_2_7_4;POL_2_7_5;POL_2_7_6;POL_3_1_1;POL_3_1_2;POL_3_1_3;POL_3_1_4;POL_3_1_5;POL_3_1_6;POL_3_3_1;POL_3_3_2;POL_3_3_3;POL_3_3_4;POL_3_3_5;POL_3_3_6;POL_3_6_1;POL_3_6_2;POL_3_6_3;POL_3_6_4;POL_3_6_5;POL_3_6_6;POL_3_7_1;POL_3_7_2;POL_3_7_3;POL_3_7_4;POL_3_7_5;POL_3_7_6;POL_4_1_1;POL_4_1_2;POL_4_1_3;POL_4_1_4;POL_4_1_5;POL_4_1_6;POL_4_3_1;POL_4_3_2;POL_4_3_3;POL_4_3_4;POL_4_3_5;POL_4_3_6;POL_4_6_1;POL_4_6_2;POL_4_6_3;POL_4_6_4;POL_4_6_5;POL_4_6_6;POL_4_7_1;POL_4_7_2;POL_4_7_3;POL_4_7_4;POL_4_7_5;POL_4_7_6;POL_5_1_1;POL_5_1_2;POL_5_1_3;POL_5_1_4;POL_5_1_5;POL_5_1_6;POL_5_3_1;POL_5_3_2;POL_5_3_3;POL_5_3_4;POL_5_3_5;POL_5_3_6;POL_5_6_1;POL_5_6_2;POL_5_6_3;POL_5_6_4;POL_5_6_5;POL_5_6_6;POL_5_7_1;POL_5_7_2;POL_5_7_3;POL_5_7_4;POL_5_7_5;POL_5_7_6;POL_6_1_1;POL_6_1_2;POL_6_1_3;POL_6_1_4;POL_6_1_5;POL_6_1_6;POL_6_3_1;POL_6_3_2;POL_6_3_3;POL_6_3_4;POL_6_3_5;POL_6_3_6;POL_6_6_1;POL_6_6_2;POL_6_6_3;POL_6_6_4;POL_6_6_5;POL_6_6_6;POL_6_7_1;POL_6_7_2;POL_6_7_3;POL_6_7_4;POL_6_7_5;POL_6_7_6;POL_7_1_1;POL_7_1_2;POL_7_1_3;POL_7_1_4;POL_7_1_5;POL_7_1_6;POL_7_3_1;POL_7_3_2;POL_7_3_3;POL_7_3_4;POL_7_3_5;POL_7_3_6;POL_7_6_1;POL_7_6_2;POL_7_6_3;POL_7_6_4;POL_7_6_5;POL_7_6_6;POL_7_7_1;POL_7_7_2;POL_7_7_3;POL_7_7_4;POL_7_7_5;POL_7_7_6;POL_8_1_1;POL_8_1_2;POL_8_1_3;POL_8_1_4;POL_8_1_5;POL_8_1_6;POL_8_3_1;POL_8_3_2;POL_8_3_3;POL_8_3_4;POL_8_3_5;POL_8_3_6;POL_8_6_1;POL_8_6_2;POL_8_6_3;POL_8_6_4;POL_8_6_5;POL_8_6_6;POL_8_7_1;POL_8_7_2;POL_8_7_3;POL_8_7_4;POL_8_7_5;POL_8_7_6;POL_1_Diag_Annee_Compta;POL_2_Diag_Annee_Compta;POL_3_Diag_Annee_Compta;POL_4_Diag_Annee_Compta;POL_5_Diag_Annee_Compta;POL_6_Diag_Annee_Compta;TOTAL_POL__1_1;TOTAL_POL__2_1;TOTAL_POL__3_1;TOTAL_POL__4_1;TOTAL_POL__5_1;TOTAL_POL__6_1;TOTAL_POL__1_3;TOTAL_POL__2_3;TOTAL_POL__3_3;TOTAL_POL__4_3;TOTAL_POL__5_3;TOTAL_POL__6_3;TOTAL_POL__1_6;TOTAL_POL__2_6;TOTAL_POL__3_6;TOTAL_POL__4_6;TOTAL_POL__5_6;TOTAL_POL__6_6;TOTAL_POL__1_7;TOTAL_POL__2_7;TOTAL_POL__3_7;TOTAL_POL__4_7;TOTAL_POL__5_7;TOTAL_POL__6_7;POL_Commentaire
        900;"18/12/2019";"18/01/2020";"02/02/2020";"PCAET";"PCAET";"Ceci est un test";"200069420";"Trifouilly-les-oies";2;"Mise en oeuvre";"Georgette";"mail@mail.com";"Marie-Cunégonde";"";"20/08/15";"20/08/15";"20/08/15";"O";"";"";1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;2015;2015;2015;2015;2015;2015;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;9000;""
    """
    csv_content_meta_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches
    500;"15/08/2016";"07/09/2019";"11/07/2020";"PCAET";"PCAET";"Ceci est un test";"200069420";"Trifouilly-les-oies";2;"Mise en oeuvre";"Jean";"mail@mail.com";"Michel";"";"06/01/2017";"06/01/2017";"06/01/2017";"O";"";""
    """
    csv_content_meta_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches
    900;"18/12/2019";"18/01/2020";"02/02/2020";"PCAET";"PCAET";"Ceci est un test";"200069420";"Trifouilly-les-oies";2;"Mise en oeuvre";"Georgette";"mail@mail.com";"Marie-Cunégonde";"";"20/08/15";"20/08/15";"20/08/15";"O";"";""
    """
    csv_content_enr_V2 += csv_content_enr_V2.split("\n")[1]  # Duplicate data in enr_V2

    with requests_mock.Mocker() as req_mock:
        req_mock.get(csv_url_pcaet_pec_seq_ademe_v1, text=csv_content_pec_seq_V1)
        req_mock.get(csv_url_pcaet_pec_seq_ademe_v2, text=csv_content_pec_seq_V2)
        req_mock.get(csv_url_pcaet_enr_ademe_v1, text=csv_content_enr_V1)
        req_mock.get(csv_url_pcaet_enr_ademe_v2, text=csv_content_enr_V2)
        req_mock.get(csv_url_pcaet_polluant_ademe_v1, text=csv_content_polluant_V1)
        req_mock.get(csv_url_pcaet_polluant_ademe_v2, text=csv_content_polluant_V2)
        req_mock.get(csv_url_pcaet_meta_ademe_v1, text=csv_content_meta_V1)
        req_mock.get(csv_url_pcaet_meta_ademe_v2, text=csv_content_meta_V2)

        _request, response = client_admin_user.get("/api/pytest/maj_pcaet_ademe")
        assert response.status == 200
        assert response.json["message"] == "PCAET mis à jour"

    # All previous values were deleted
    rset = loop.run_until_complete(
        fetch("SELECT * FROM pytest.pcaet WHERE siren='202020202'")
    )
    assert len(rset) == 0

    # All new values were inserted
    rset = loop.run_until_complete(fetch("SELECT * FROM pytest.pcaet"))
    pcaet = [dict(res) for res in rset]
    print(pd.DataFrame(pcaet).to_string())
    # #trajectories * #categories * #years
    assert len(pcaet) == 2 * 8 * 5 + 2 * 8 * 4 + 1 * 15 * 5 + 1 * 15 * 4 + 6 * 8 * 4

    # Files were deleted
    list_files = os.listdir(settings.get("api", "pcaet_path"))
    assert not any("pytest_" in f for f in list_files)


def test_update_pcaet_table_empty_values(region, mocker, client_admin_user, loop):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    mocker.patch(
        "terriapi.controller.pcaet_ademe.PCAET_META_TABLE", "pytest.pcaet_meta"
    )
    loop.run_until_complete(
        execute(
            "INSERT INTO pytest.pcaet VALUES ('202020202', 'previous_pcaet_data', '', 0, 0, TRUE, 0, 'pytest');"
        )
    )

    csv_url_pcaet_pec_seq_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-2/data-files/Demarches_PCAET_V1_pec_seq.csv"
    csv_url_pcaet_enr_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-3/data-files/Demarches_PCAET_V1_enr.csv"
    csv_url_pcaet_polluant_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-4/data-files/Demarches_PCAET_V1_polluant.csv"
    csv_url_pcaet_meta_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-1-entete/data-files/Demarches_PCAET_V1_entete.csv"
    csv_url_pcaet_pec_seq_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-2/data-files/Demarches_PCAET_V2_pec_seq.csv"
    csv_url_pcaet_enr_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-3/data-files/Demarches_PCAET_V2_enr.csv"
    csv_url_pcaet_polluant_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-4/data-files/Demarches_PCAET_V2_polluant.csv"
    csv_url_pcaet_meta_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-1-entete/data-files/Demarches_PCAET_V2_entete.csv"
    csv_content_pec_seq_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;SIREN collectivites_coporteuses;Collectivités porteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;PEC_Emission_GES_Annee_Compta;PEC_GES_1_1;PEC_GES_1_2;PEC_GES_1_3;PEC_GES_1_4;PEC_GES_1_5;PEC_GES_2_1;PEC_GES_2_2;PEC_GES_2_3;PEC_GES_2_4;PEC_GES_2_5;PEC_GES_3_1;PEC_GES_3_2;PEC_GES_3_3;PEC_GES_3_4;PEC_GES_3_5;PEC_GES_4_1;PEC_GES_4_2;PEC_GES_4_3;PEC_GES_4_4;PEC_GES_4_5;PEC_GES_5_1;PEC_GES_5_2;PEC_GES_5_3;PEC_GES_5_4;PEC_GES_5_5;PEC_GES_6_1;PEC_GES_6_2;PEC_GES_6_3;PEC_GES_6_4;PEC_GES_6_5;PEC_GES_7_1;PEC_GES_7_2;PEC_GES_7_3;PEC_GES_7_4;PEC_GES_7_5;PEC_GES_8_1;PEC_GES_8_2;PEC_GES_8_3;PEC_GES_8_4;PEC_GES_8_5;PEC_Consommation_Annee_Compta;PEC_CONSO_1_1;PEC_CONSO_1_2;PEC_CONSO_1_3;PEC_CONSO_1_4;PEC_CONSO_1_5;PEC_CONSO_2_1;PEC_CONSO_2_2;PEC_CONSO_2_3;PEC_CONSO_2_4;PEC_CONSO_2_5;PEC_CONSO_3_1;PEC_CONSO_3_2;PEC_CONSO_3_3;PEC_CONSO_3_4;PEC_CONSO_3_5;PEC_CONSO_4_1;PEC_CONSO_4_2;PEC_CONSO_4_3;PEC_CONSO_4_4;PEC_CONSO_4_5;PEC_CONSO_5_1;PEC_CONSO_5_2;PEC_CONSO_5_3;PEC_CONSO_5_4;PEC_CONSO_5_5;PEC_CONSO_6_1;PEC_CONSO_6_2;PEC_CONSO_6_3;PEC_CONSO_6_4;PEC_CONSO_6_5;PEC_CONSO_7_1;PEC_CONSO_7_2;PEC_CONSO_7_3;PEC_CONSO_7_4;PEC_CONSO_7_5;PEC_CONSO_8_1;PEC_CONSO_8_2;PEC_CONSO_8_3;PEC_CONSO_8_4;PEC_CONSO_8_5;PEC_GES_CONSO_commentaire
    """
    csv_content_pec_seq_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;SIREN collectivites_coporteuses;Collectivités porteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;PEC_Emission_GES_Annee_Compta;PEC_GES_1_1;PEC_GES_1_3;PEC_GES_1_6;PEC_GES_1_7;PEC_GES_2_1;PEC_GES_2_3;PEC_GES_2_6;PEC_GES_2_7;PEC_GES_3_1;PEC_GES_3_3;PEC_GES_3_6;PEC_GES_3_7;PEC_GES_4_1;PEC_GES_4_3;PEC_GES_4_6;PEC_GES_4_7;PEC_GES_5_1;PEC_GES_5_3;PEC_GES_5_6;PEC_GES_5_7;PEC_GES_6_1;PEC_GES_6_3;PEC_GES_6_6;PEC_GES_6_7;PEC_GES_7_1;PEC_GES_7_3;PEC_GES_7_6;PEC_GES_7_7;PEC_GES_8_1;PEC_GES_8_3;PEC_GES_8_6;PEC_GES_8_7;PEC_Consommation_Annee_Compta;PEC_CONSO_1_1;PEC_CONSO_1_3;PEC_CONSO_1_6;PEC_CONSO_1_7;PEC_CONSO_2_1;PEC_CONSO_2_3;PEC_CONSO_2_6;PEC_CONSO_2_7;PEC_CONSO_3_1;PEC_CONSO_3_3;PEC_CONSO_3_6;PEC_CONSO_3_7;PEC_CONSO_4_1;PEC_CONSO_4_3;PEC_CONSO_4_6;PEC_CONSO_4_7;PEC_CONSO_5_1;PEC_CONSO_5_3;PEC_CONSO_5_6;PEC_CONSO_5_7;PEC_CONSO_6_1;PEC_CONSO_6_3;PEC_CONSO_6_6;PEC_CONSO_6_7;PEC_CONSO_7_1;PEC_CONSO_7_3;PEC_CONSO_7_6;PEC_CONSO_7_7;PEC_CONSO_8_1;PEC_CONSO_8_3;PEC_CONSO_8_6;PEC_CONSO_8_7;PEC_GES_CONSO_commentaire
    """
    csv_content_enr_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;Collectivités porteuses;SIREN collectivites_coporteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;ENR_1_Diag_Annee_Compta;ENR_2_Diag_Annee_Compta;ENR_3_Diag_Annee_Compta;ENR_4_Diag_Annee_Compta;ENR_5_Diag_Annee_Compta;ENR_6_Diag_Annee_Compta;ENR_7_Diag_Annee_Compta;ENR_8_Diag_Annee_Compta;ENR_9_Diag_Annee_Compta;ENR_10_Diag_Annee_Compta;ENR_11_Diag_Annee_Compta;ENR_12_Diag_Annee_Compta;ENR_13_Diag_Annee_Compta;ENR_14_Diag_Annee_Compta;ENR_1_1_Production;ENR_1_2_Production;ENR_1_3_Production;ENR_1_4_Production;ENR_1_5_Production;ENR_2_1_Production;ENR_2_2_Production;ENR_2_3_Production;ENR_2_4_Production;ENR_2_5_Production;ENR_3_1_Production;ENR_3_2_Production;ENR_3_3_Production;ENR_3_4_Production;ENR_3_5_Production;ENR_4_1_Production;ENR_4_2_Production;ENR_4_3_Production;ENR_4_4_Production;ENR_4_5_Production;ENR_5_1_Production;ENR_5_2_Production;ENR_5_3_Production;ENR_5_4_Production;ENR_5_5_Production;ENR_6_1_Production;ENR_6_2_Production;ENR_6_3_Production;ENR_6_4_Production;ENR_6_5_Production;ENR_7_1_Production;ENR_7_2_Production;ENR_7_3_Production;ENR_7_4_Production;ENR_7_5_Production;ENR_8_1_Production;ENR_8_2_Production;ENR_8_3_Production;ENR_8_4_Production;ENR_8_5_Production;ENR_9_1_Production;ENR_9_2_Production;ENR_9_3_Production;ENR_9_4_Production;ENR_9_5_Production;ENR_10_1_Production;ENR_10_2_Production;ENR_10_3_Production;ENR_10_4_Production;ENR_10_5_Production;ENR_11_1_Production;ENR_11_2_Production;ENR_11_3_Production;ENR_11_4_Production;ENR_11_5_Production;ENR_12_1_Production;ENR_12_2_Production;ENR_12_3_Production;ENR_12_4_Production;ENR_12_5_Production;ENR_13_1_Production;ENR_13_2_Production;ENR_13_3_Production;ENR_13_4_Production;ENR_13_5_Production;ENR_14_1_Production;ENR_14_2_Production;ENR_14_3_Production;ENR_14_4_Production;ENR_14_5_Production;ENR_1_2_Consommation;ENR_1_3_Consommation;ENR_1_4_Consommation;ENR_1_5_Consommation;ENR_2_2_Consommation;ENR_2_3_Consommation;ENR_2_4_Consommation;ENR_2_5_Consommation;ENR_3_2_Consommation;ENR_3_3_Consommation;ENR_3_4_Consommation;ENR_3_5_Consommation;ENR_4_2_Consommation;ENR_4_3_Consommation;ENR_4_4_Consommation;ENR_4_5_Consommation;ENR_5_2_Consommation;ENR_5_3_Consommation;ENR_5_4_Consommation;ENR_5_5_Consommation;ENR_6_2_Consommation;ENR_6_3_Consommation;ENR_6_4_Consommation;ENR_6_5_Consommation;ENR_7_2_Consommation;ENR_7_3_Consommation;ENR_7_4_Consommation;ENR_7_5_Consommation;ENR_8_2_Consommation;ENR_8_3_Consommation;ENR_8_4_Consommation;ENR_8_5_Consommation;ENR_9_2_Consommation;ENR_9_3_Consommation;ENR_9_4_Consommation;ENR_9_5_Consommation;ENR_10_2_Consommation;ENR_10_3_Consommation;ENR_10_4_Consommation;ENR_10_5_Consommation;ENR_11_2_Consommation;ENR_11_3_Consommation;ENR_11_4_Consommation;ENR_11_5_Consommation;ENR_12_2_Consommation;ENR_12_3_Consommation;ENR_12_4_Consommation;ENR_12_5_Consommation;ENR_13_2_Consommation;ENR_13_3_Consommation;ENR_13_4_Consommation;ENR_13_5_Consommation;ENR_14_2_Consommation;ENR_14_3_Consommation;ENR_14_4_Consommation;ENR_14_5_Consommation;ENR_valorisation_recuperation_2;ENR_valorisation_recuperation_3;ENR_valorisation_recuperation_4;ENR_valorisation_recuperation_5;ENR_valorisation_stockage_2;ENR_valorisation_stockage_3;ENR_valorisation_stockage_4;ENR_valorisation_stockage_5;ENR_commentaire
    """
    csv_content_enr_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;SIREN collectivites_coporteuses;Collectivités porteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;ENR_1_Diag_Annee_Compta;ENR_2_Diag_Annee_Compta;ENR_3_Diag_Annee_Compta;ENR_4_Diag_Annee_Compta;ENR_5_Diag_Annee_Compta;ENR_6_Diag_Annee_Compta;ENR_7_Diag_Annee_Compta;ENR_8_Diag_Annee_Compta;ENR_9_Diag_Annee_Compta;ENR_10_Diag_Annee_Compta;ENR_11_Diag_Annee_Compta;ENR_12_Diag_Annee_Compta;ENR_13_Diag_Annee_Compta;ENR_14_Diag_Annee_Compta;ENR_1_1_Production;ENR_1_3_Production;ENR_1_6_Production;ENR_1_7_Production;ENR_2_1_Production;ENR_2_3_Production;ENR_2_6_Production;ENR_2_7_Production;ENR_3_1_Production;ENR_3_3_Production;ENR_3_6_Production;ENR_3_7_Production;ENR_4_1_Production;ENR_4_3_Production;ENR_4_6_Production;ENR_4_7_Production;ENR_5_1_Production;ENR_5_3_Production;ENR_5_6_Production;ENR_5_7_Production;ENR_6_1_Production;ENR_6_3_Production;ENR_6_6_Production;ENR_6_7_Production;ENR_7_1_Production;ENR_7_3_Production;ENR_7_6_Production;ENR_7_7_Production;ENR_13_1_Production;ENR_13_3_Production;ENR_13_6_Production;ENR_13_7_Production;ENR_8_1_Consommation;ENR_8_3_Consommation;ENR_8_6_Consommation;ENR_8_7_Consommation;ENR_9_1_Consommation;ENR_9_3_Consommation;ENR_9_6_Consommation;ENR_9_7_Consommation;ENR_10_1_Consommation;ENR_10_3_Consommation;ENR_10_6_Consommation;ENR_10_7_Consommation;ENR_11_1_Consommation;ENR_11_3_Consommation;ENR_11_6_Consommation;ENR_11_7_Consommation;ENR_12_1_Consommation;ENR_12_3_Consommation;ENR_12_6_Consommation;ENR_12_7_Consommation;ENR_14_1_Consommation;ENR_14_3_Consommation;ENR_14_6_Consommation;ENR_14_7_Consommation;ENR_valorisation_recuperation_3;ENR_valorisation_recuperation_6;ENR_valorisation_recuperation_7;ENR_valorisation_stockage_3;ENR_valorisation_stockage_6;ENR_valorisation_stockage_7;ENR_commentaire
    """
    csv_content_polluant_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;POL_1_1_1;POL_1_1_2;POL_1_1_3;POL_1_1_4;POL_1_1_5;POL_1_1_6;POL_1_2_1;POL_1_2_2;POL_1_2_3;POL_1_2_4;POL_1_2_5;POL_1_2_6;POL_1_3_1;POL_1_3_2;POL_1_3_3;POL_1_3_4;POL_1_3_5;POL_1_3_6;POL_1_4_1;POL_1_4_2;POL_1_4_3;POL_1_4_4;POL_1_4_5;POL_1_4_6;POL_1_5_1;POL_1_5_2;POL_1_5_3;POL_1_5_4;POL_1_5_5;POL_1_5_6;POL_2_1_1;POL_2_1_2;POL_2_1_3;POL_2_1_4;POL_2_1_5;POL_2_1_6;POL_2_2_1;POL_2_2_2;POL_2_2_3;POL_2_2_4;POL_2_2_5;POL_2_2_6;POL_2_3_1;POL_2_3_2;POL_2_3_3;POL_2_3_4;POL_2_3_5;POL_2_3_6;POL_2_4_1;POL_2_4_2;POL_2_4_3;POL_2_4_4;POL_2_4_5;POL_2_4_6;POL_2_5_1;POL_2_5_2;POL_2_5_3;POL_2_5_4;POL_2_5_5;POL_2_5_6;POL_3_1_1;POL_3_1_2;POL_3_1_3;POL_3_1_4;POL_3_1_5;POL_3_1_6;POL_3_2_1;POL_3_2_2;POL_3_2_3;POL_3_2_4;POL_3_2_5;POL_3_2_6;POL_3_3_1;POL_3_3_2;POL_3_3_3;POL_3_3_4;POL_3_3_5;POL_3_3_6;POL_3_4_1;POL_3_4_2;POL_3_4_3;POL_3_4_4;POL_3_4_5;POL_3_4_6;POL_3_5_1;POL_3_5_2;POL_3_5_3;POL_3_5_4;POL_3_5_5;POL_3_5_6;POL_4_1_1;POL_4_1_2;POL_4_1_3;POL_4_1_4;POL_4_1_5;POL_4_1_6;POL_4_2_1;POL_4_2_2;POL_4_2_3;POL_4_2_4;POL_4_2_5;POL_4_2_6;POL_4_3_1;POL_4_3_2;POL_4_3_3;POL_4_3_4;POL_4_3_5;POL_4_3_6;POL_4_4_1;POL_4_4_2;POL_4_4_3;POL_4_4_4;POL_4_4_5;POL_4_4_6;POL_4_5_1;POL_4_5_2;POL_4_5_3;POL_4_5_4;POL_4_5_5;POL_4_5_6;POL_5_1_1;POL_5_1_2;POL_5_1_3;POL_5_1_4;POL_5_1_5;POL_5_1_6;POL_5_2_1;POL_5_2_2;POL_5_2_3;POL_5_2_4;POL_5_2_5;POL_5_2_6;POL_5_3_1;POL_5_3_2;POL_5_3_3;POL_5_3_4;POL_5_3_5;POL_5_3_6;POL_5_4_1;POL_5_4_2;POL_5_4_3;POL_5_4_4;POL_5_4_5;POL_5_4_6;POL_5_5_1;POL_5_5_2;POL_5_5_3;POL_5_5_4;POL_5_5_5;POL_5_5_6;POL_6_1_1;POL_6_1_2;POL_6_1_3;POL_6_1_4;POL_6_1_5;POL_6_1_6;POL_6_2_1;POL_6_2_2;POL_6_2_3;POL_6_2_4;POL_6_2_5;POL_6_2_6;POL_6_3_1;POL_6_3_2;POL_6_3_3;POL_6_3_4;POL_6_3_5;POL_6_3_6;POL_6_4_1;POL_6_4_2;POL_6_4_3;POL_6_4_4;POL_6_4_5;POL_6_4_6;POL_6_5_1;POL_6_5_2;POL_6_5_3;POL_6_5_4;POL_6_5_5;POL_6_5_6;POL_7_1_1;POL_7_1_2;POL_7_1_3;POL_7_1_4;POL_7_1_5;POL_7_1_6;POL_7_2_1;POL_7_2_2;POL_7_2_3;POL_7_2_4;POL_7_2_5;POL_7_2_6;POL_7_3_1;POL_7_3_2;POL_7_3_3;POL_7_3_4;POL_7_3_5;POL_7_3_6;POL_7_4_1;POL_7_4_2;POL_7_4_3;POL_7_4_4;POL_7_4_5;POL_7_4_6;POL_7_5_1;POL_7_5_2;POL_7_5_3;POL_7_5_4;POL_7_5_5;POL_7_5_6;POL_8_1_1;POL_8_1_2;POL_8_1_3;POL_8_1_4;POL_8_1_5;POL_8_1_6;POL_8_2_1;POL_8_2_2;POL_8_2_3;POL_8_2_4;POL_8_2_5;POL_8_2_6;POL_8_3_1;POL_8_3_2;POL_8_3_3;POL_8_3_4;POL_8_3_5;POL_8_3_6;POL_8_4_1;POL_8_4_2;POL_8_4_3;POL_8_4_4;POL_8_4_5;POL_8_4_6;POL_8_5_1;POL_8_5_2;POL_8_5_3;POL_8_5_4;POL_8_5_5;POL_8_5_6;POL_1_Diag_Annee_Compta;POL_2_Diag_Annee_Compta;POL_3_Diag_Annee_Compta;POL_4_Diag_Annee_Compta;POL_5_Diag_Annee_Compta;POL_6_Diag_Annee_Compta;POL_Commentaire
    """
    csv_content_polluant_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;POL_1_1_1;POL_1_1_2;POL_1_1_3;POL_1_1_4;POL_1_1_5;POL_1_1_6;POL_1_3_1;POL_1_3_2;POL_1_3_3;POL_1_3_4;POL_1_3_5;POL_1_3_6;POL_1_6_1;POL_1_6_2;POL_1_6_3;POL_1_6_4;POL_1_6_5;POL_1_6_6;POL_1_7_1;POL_1_7_2;POL_1_7_3;POL_1_7_4;POL_1_7_5;POL_1_7_6;POL_2_1_1;POL_2_1_2;POL_2_1_3;POL_2_1_4;POL_2_1_5;POL_2_1_6;POL_2_3_1;POL_2_3_2;POL_2_3_3;POL_2_3_4;POL_2_3_5;POL_2_3_6;POL_2_6_1;POL_2_6_2;POL_2_6_3;POL_2_6_4;POL_2_6_5;POL_2_6_6;POL_2_7_1;POL_2_7_2;POL_2_7_3;POL_2_7_4;POL_2_7_5;POL_2_7_6;POL_3_1_1;POL_3_1_2;POL_3_1_3;POL_3_1_4;POL_3_1_5;POL_3_1_6;POL_3_3_1;POL_3_3_2;POL_3_3_3;POL_3_3_4;POL_3_3_5;POL_3_3_6;POL_3_6_1;POL_3_6_2;POL_3_6_3;POL_3_6_4;POL_3_6_5;POL_3_6_6;POL_3_7_1;POL_3_7_2;POL_3_7_3;POL_3_7_4;POL_3_7_5;POL_3_7_6;POL_4_1_1;POL_4_1_2;POL_4_1_3;POL_4_1_4;POL_4_1_5;POL_4_1_6;POL_4_3_1;POL_4_3_2;POL_4_3_3;POL_4_3_4;POL_4_3_5;POL_4_3_6;POL_4_6_1;POL_4_6_2;POL_4_6_3;POL_4_6_4;POL_4_6_5;POL_4_6_6;POL_4_7_1;POL_4_7_2;POL_4_7_3;POL_4_7_4;POL_4_7_5;POL_4_7_6;POL_5_1_1;POL_5_1_2;POL_5_1_3;POL_5_1_4;POL_5_1_5;POL_5_1_6;POL_5_3_1;POL_5_3_2;POL_5_3_3;POL_5_3_4;POL_5_3_5;POL_5_3_6;POL_5_6_1;POL_5_6_2;POL_5_6_3;POL_5_6_4;POL_5_6_5;POL_5_6_6;POL_5_7_1;POL_5_7_2;POL_5_7_3;POL_5_7_4;POL_5_7_5;POL_5_7_6;POL_6_1_1;POL_6_1_2;POL_6_1_3;POL_6_1_4;POL_6_1_5;POL_6_1_6;POL_6_3_1;POL_6_3_2;POL_6_3_3;POL_6_3_4;POL_6_3_5;POL_6_3_6;POL_6_6_1;POL_6_6_2;POL_6_6_3;POL_6_6_4;POL_6_6_5;POL_6_6_6;POL_6_7_1;POL_6_7_2;POL_6_7_3;POL_6_7_4;POL_6_7_5;POL_6_7_6;POL_7_1_1;POL_7_1_2;POL_7_1_3;POL_7_1_4;POL_7_1_5;POL_7_1_6;POL_7_3_1;POL_7_3_2;POL_7_3_3;POL_7_3_4;POL_7_3_5;POL_7_3_6;POL_7_6_1;POL_7_6_2;POL_7_6_3;POL_7_6_4;POL_7_6_5;POL_7_6_6;POL_7_7_1;POL_7_7_2;POL_7_7_3;POL_7_7_4;POL_7_7_5;POL_7_7_6;POL_8_1_1;POL_8_1_2;POL_8_1_3;POL_8_1_4;POL_8_1_5;POL_8_1_6;POL_8_3_1;POL_8_3_2;POL_8_3_3;POL_8_3_4;POL_8_3_5;POL_8_3_6;POL_8_6_1;POL_8_6_2;POL_8_6_3;POL_8_6_4;POL_8_6_5;POL_8_6_6;POL_8_7_1;POL_8_7_2;POL_8_7_3;POL_8_7_4;POL_8_7_5;POL_8_7_6;POL_1_Diag_Annee_Compta;POL_2_Diag_Annee_Compta;POL_3_Diag_Annee_Compta;POL_4_Diag_Annee_Compta;POL_5_Diag_Annee_Compta;POL_6_Diag_Annee_Compta;TOTAL_POL__1_1;TOTAL_POL__2_1;TOTAL_POL__3_1;TOTAL_POL__4_1;TOTAL_POL__5_1;TOTAL_POL__6_1;TOTAL_POL__1_3;TOTAL_POL__2_3;TOTAL_POL__3_3;TOTAL_POL__4_3;TOTAL_POL__5_3;TOTAL_POL__6_3;TOTAL_POL__1_6;TOTAL_POL__2_6;TOTAL_POL__3_6;TOTAL_POL__4_6;TOTAL_POL__5_6;TOTAL_POL__6_6;TOTAL_POL__1_7;TOTAL_POL__2_7;TOTAL_POL__3_7;TOTAL_POL__4_7;TOTAL_POL__5_7;TOTAL_POL__6_7;POL_Commentaire
    """
    csv_content_meta_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches
    """
    csv_content_meta_V2 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches
    """

    with requests_mock.Mocker() as req_mock:
        req_mock.get(csv_url_pcaet_pec_seq_ademe_v1, text=csv_content_pec_seq_V1)
        req_mock.get(csv_url_pcaet_pec_seq_ademe_v2, text=csv_content_pec_seq_V2)
        req_mock.get(csv_url_pcaet_enr_ademe_v1, text=csv_content_enr_V1)
        req_mock.get(csv_url_pcaet_enr_ademe_v2, text=csv_content_enr_V2)
        req_mock.get(csv_url_pcaet_polluant_ademe_v1, text=csv_content_polluant_V1)
        req_mock.get(csv_url_pcaet_polluant_ademe_v2, text=csv_content_polluant_V2)
        req_mock.get(csv_url_pcaet_meta_ademe_v1, text=csv_content_meta_V1)
        req_mock.get(csv_url_pcaet_meta_ademe_v2, text=csv_content_meta_V2)

        _request, response = client_admin_user.get("/api/pytest/maj_pcaet_ademe")
        assert response.status == 200
        assert response.json["message"] == "PCAET mis à jour"

    # All previous values were deleted
    rset = loop.run_until_complete(fetch("SELECT * FROM pytest.pcaet"))
    assert len(rset) == 0


def test_update_pcaet_table_wrong_configuration(
    region, mocker, client_admin_user, loop
):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    mocker.patch(
        "terriapi.controller.pcaet_ademe.PCAET_META_TABLE", "pytest.pcaet_meta"
    )
    loop.run_until_complete(
        execute(
            "INSERT INTO pytest.pcaet VALUES ('202020202', 'previous_pcaet_data', '', 0, 0, TRUE, 0, 'pytest');"
        )
    )

    csv_url_pcaet_pec_seq_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-2/data-files/Demarches_PCAET_V1_pec_seq.csv"
    csv_url_pcaet_enr_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-3/data-files/Demarches_PCAET_V1_enr.csv"
    csv_url_pcaet_polluant_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-4/data-files/Demarches_PCAET_V1_polluant.csv"
    csv_url_pcaet_meta_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-1-entete/data-files/Demarches_PCAET_V1_entete.csv"
    csv_url_pcaet_pec_seq_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-2/data-files/Demarches_PCAET_V2_pec_seq.csv"
    csv_url_pcaet_enr_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-3/data-files/Demarches_PCAET_V2_enr.csv"
    csv_url_pcaet_polluant_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-4/data-files/Demarches_PCAET_V2_polluant.csv"
    csv_url_pcaet_meta_ademe_v2 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-(v2)-demarches-partie-1-entete/data-files/Demarches_PCAET_V2_entete.csv"
    csv_content_pec_seq_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;SIREN collectivites_coporteuses;Collectivités porteuses;Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches;PEC_Emission_GES_Annee_Compta;PEC_GES_1_1;PEC_GES_1_2;PEC_GES_1_3;PEC_GES_1_4;PEC_GES_1_5;PEC_GES_2_1;PEC_GES_2_2;PEC_GES_2_3;PEC_GES_2_4;PEC_GES_2_5;PEC_GES_3_1;PEC_GES_3_2;PEC_GES_3_3;PEC_GES_3_4;PEC_GES_3_5;PEC_GES_4_1;PEC_GES_4_2;PEC_GES_4_3;PEC_GES_4_4;PEC_GES_4_5;PEC_GES_5_1;PEC_GES_5_2;PEC_GES_5_3;PEC_GES_5_4;PEC_GES_5_5;PEC_GES_6_1;PEC_GES_6_2;PEC_GES_6_3;PEC_GES_6_4;PEC_GES_6_5;PEC_GES_7_1;PEC_GES_7_2;PEC_GES_7_3;PEC_GES_7_4;PEC_GES_7_5;PEC_GES_8_1;PEC_GES_8_2;PEC_GES_8_3;PEC_GES_8_4;PEC_GES_8_5;PEC_Consommation_Annee_Compta;PEC_CONSO_1_1;PEC_CONSO_1_2;PEC_CONSO_1_3;PEC_CONSO_1_4;PEC_CONSO_1_5;PEC_CONSO_2_1;PEC_CONSO_2_2;PEC_CONSO_2_3;PEC_CONSO_2_4;PEC_CONSO_2_5;PEC_CONSO_3_1;PEC_CONSO_3_2;PEC_CONSO_3_3;PEC_CONSO_3_4;PEC_CONSO_3_5;PEC_CONSO_4_1;PEC_CONSO_4_2;PEC_CONSO_4_3;PEC_CONSO_4_4;PEC_CONSO_4_5;PEC_CONSO_5_1;PEC_CONSO_5_2;PEC_CONSO_5_3;PEC_CONSO_5_4;PEC_CONSO_5_5;PEC_CONSO_6_1;PEC_CONSO_6_2;PEC_CONSO_6_3;PEC_CONSO_6_4;PEC_CONSO_6_5;PEC_CONSO_7_1;PEC_CONSO_7_2;PEC_CONSO_7_3;PEC_CONSO_7_4;PEC_CONSO_7_5;PEC_CONSO_8_1;PEC_CONSO_8_2;PEC_CONSO_8_3;PEC_CONSO_8_4;PEC_CONSO_8_5;PEC_GES_CONSO_commentaire
        500;"15/08/2016";"07/09/2019";"11/07/2020";"PCAET";"PCAET";"Ceci est un test";"200069420";"Trifouilly-les-oies";2;"Mise en oeuvre";"Jean";"mail@mail.com";"Michel";"";"06/01/2017";"06/01/2017";"06/01/2017";"O";"";"";1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;1000;""
    """
    csv_content_meta_V1 = """Id;Date_creation;Date_modification;Date_lancement;Type_demarche;Nom;Description_rapide;"SIREN collectivites_coporteuses";"Collectivités porteuses";Population_couverte;Demarche_etat;Chef_de_projet;Contact;Elu_referent;Commentaire_statut;Date_reception_projet;Date_envoi_avis_dreal;Date_envoi_avis_cr;Oblige;Autres_démarches_hors_EC;Autres_demarches
        500;"15/08/2016";"07/09/2019";"11/07/2020";"PCAET";"PCAET";"Ceci est un test";"200069420";"Trifouilly-les-oies";2;"Mise en oeuvre";"Jean";"mail@mail.com";"Michel";"";"06/01/2017";"06/01/2017";"06/01/2017";"O";"";""
    """

    with requests_mock.Mocker() as req_mock:
        req_mock.get(csv_url_pcaet_pec_seq_ademe_v1, text=csv_content_pec_seq_V1)
        req_mock.get(csv_url_pcaet_pec_seq_ademe_v2, status_code=404)
        req_mock.get(csv_url_pcaet_enr_ademe_v1, text=csv_content_pec_seq_V1)
        req_mock.get(csv_url_pcaet_enr_ademe_v2, status_code=404)
        req_mock.get(csv_url_pcaet_polluant_ademe_v1, status_code=404)
        req_mock.get(csv_url_pcaet_meta_ademe_v1, text=csv_content_meta_V1)
        req_mock.get(csv_url_pcaet_meta_ademe_v2, status_code=404)

        # Wrong path to save the CSV file
        mocker.patch(
            "terriapi.controller.pcaet_ademe.settings",
            MockSettings({"pcaet_path": "/this/path/does/not/exist/"}),
        )
        _request, response = client_admin_user.get("/api/pytest/maj_pcaet_ademe")
        assert response.status == 400
        assert "Unable to save PCAET CSV file" in response.json["message"]

    # Previous values were kept
    rset = loop.run_until_complete(
        fetch("SELECT * FROM pytest.pcaet WHERE siren='202020202'")
    )
    assert len(rset) == 1


def test_update_pcaet_table_wrong_ademe_api(region, mocker, client_admin_user, loop):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    mocker.patch(
        "terriapi.controller.pcaet_ademe.PCAET_META_TABLE", "pytest.pcaet_meta"
    )
    loop.run_until_complete(
        execute(
            "INSERT INTO pytest.pcaet VALUES ('202020202', 'previous_pcaet_data', '', 0, 0, TRUE, 0);"
        )
    )

    csv_url_pcaet_pec_seq_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-2/data-files/Demarches_PCAET_V1_pec_seq.csv"
    csv_url_pcaet_enr_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-3/data-files/Demarches_PCAET_V1_enr.csv"
    csv_url_pcaet_polluant_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-4/data-files/Demarches_PCAET_V1_polluant.csv"
    csv_url_pcaet_meta_ademe_v1 = "https://data.ademe.fr/data-fair/api/v1/datasets/pcaet-demarches-partie-1-entete/data-files/Demarches_PCAET_V1_entete.csv"

    with requests_mock.Mocker() as req_mock:
        req_mock.get(csv_url_pcaet_pec_seq_ademe_v1, status_code=404)
        req_mock.get(csv_url_pcaet_enr_ademe_v1, status_code=404)
        req_mock.get(csv_url_pcaet_polluant_ademe_v1, status_code=404)
        req_mock.get(csv_url_pcaet_meta_ademe_v1, status_code=404)

        _request, response = client_admin_user.get("/api/pytest/maj_pcaet_ademe")
        assert response.status == 400
        assert "No path was found" in response.json["message"]

    # Previous values were kept
    rset = loop.run_until_complete(
        fetch("SELECT * FROM pytest.pcaet WHERE siren='202020202'")
    )
    assert len(rset) == 1


async def test_relative_pcaet_methodology_nonzero_case(mocker, passage_table):
    # Normal, split, fusion and split-fusion in non-zero cases
    # fmt: off
    pcaet_category =     [ 1,     4,    7, 10,      8]
    pcaet_reference =    [10,    20,    4,  1,      5]
    pcaet_objective =    [20,    30,   10, 10,     15]
    history_category =   [ 4,  12, 5,    14,        6]
    history_reference =  [ 6,   2, 4,     8,       10]
    expected_objective = [12,   3, 6,    32,       30]
    pytest_passage_table = {
        4: 4,   12: 12, 5: 5,   16: 14, 17: 14,   6: 6, 19: 6, 20: 6, 21: 6
    }
    # fmt: on
    pytest_passage_table = {
        RenewableProd(key): match for (key, match) in pytest_passage_table.items()
    }
    pytest_passage_fn = lambda key: pytest_passage_table.get(key, -1)
    pcaet = pd.DataFrame(
        {
            "siren": "1",
            "trajectory": "enr_production",
            "category": pcaet_category + pcaet_category,
            "year": [2020] * len(pcaet_category) + [2030] * len(pcaet_category),
            "is_ref": [True] * len(pcaet_category) + [False] * len(pcaet_category),
            "value": pcaet_reference + pcaet_objective,
        }
    )
    mock_async_function(
        "terriapi.controller.regions_configuration.get_configuration",
        [{"id": "pytest", "is_national": False, "enable_air_pollutant": False}],
        mocker,
    )
    history = pd.DataFrame(
        {
            "commune": "00000",
            "annee": 2020,
            "type_prod_enr": history_category,
            "valeur": history_reference,
        }
    )
    engine = get_pg_engine()
    history.to_sql("prod_enr", engine, "pytest", if_exists="append", index=False)

    modified_pcaet = await insert_relative_methodology_pcaet(pcaet)
    modified_pcaet["category"] = modified_pcaet["category"].apply(pytest_passage_fn)
    modified_pcaet = modified_pcaet.set_index(["category", "year"])
    modified_pcaet = modified_pcaet["relative_value"].astype(float)
    modified_pcaet = modified_pcaet.groupby(level=["category", "year"]).sum()

    for category, expected in zip(history_category, expected_objective):
        assert modified_pcaet[(category, 2030)] == expected


async def test_relative_pcaet_methodology_zero_case(mocker, passage_table):
    # Normal and split-fusion in zero cases
    # fmt: off
    pcaet_category =     [ 1,  8]
    pcaet_reference =    [ 0,  0]
    pcaet_objective =    [20, 15]
    history_category =   [ 4,  6]
    history_reference =  [ 0,  0]
    expected_objective = [20, 15]
    pytest_passage_table = {4: 4,   6: 6, 19: 6, 20: 6, 21: 6}
    # fmt: on
    pytest_passage_table = {
        RenewableProd(key): match for (key, match) in pytest_passage_table.items()
    }
    pytest_passage_fn = lambda key: pytest_passage_table.get(key, -1)
    pcaet = pd.DataFrame(
        {
            "siren": "1",
            "trajectory": "enr_production",
            "category": pcaet_category + pcaet_category,
            "year": [2020] * len(pcaet_category) + [2030] * len(pcaet_category),
            "is_ref": [True] * len(pcaet_category) + [False] * len(pcaet_category),
            "value": pcaet_reference + pcaet_objective,
        }
    )
    mock_async_function(
        "terriapi.controller.regions_configuration.get_configuration",
        [{"id": "pytest", "is_national": False, "enable_air_pollutant": False}],
        mocker,
    )
    history = pd.DataFrame(
        {
            "commune": "00000",
            "annee": 2020,
            "type_prod_enr": history_category,
            "valeur": history_reference,
        }
    )
    engine = get_pg_engine()
    history.to_sql("prod_enr", engine, "pytest", if_exists="append", index=False)

    modified_pcaet = await insert_relative_methodology_pcaet(pcaet)
    modified_pcaet["category"] = modified_pcaet["category"].apply(pytest_passage_fn)
    modified_pcaet = modified_pcaet.set_index(["category", "year"])
    modified_pcaet = modified_pcaet["relative_value"].astype(float)
    modified_pcaet = modified_pcaet.groupby(level=["category", "year"]).sum()

    for category, expected in zip(history_category, expected_objective):
        assert modified_pcaet[(category, 2030)] == expected


async def test_relative_pcaet_methodology_inexistant_year(mocker, passage_table):
    # Normal and split-fusion in zero cases
    # TODO: Fix and add tests for split and fusion cases
    # fmt: off
    pcaet_category =     [ 1,     4,    7, 10,      8]
    pcaet_reference =    [10,    10,    2,  2,      5]
    # recalculated_ref:  [15,    20,    6,  4,     10]
    pcaet_objective =    [20,    30,   10,  6,     15]
    history_category =   [ 4,  12, 5,    14,        6]
    history_reference =  [ 6,   2, 4,    20,       20]
    expected_objective = [ 8,   3, 6,    32,       30]
    pytest_passage_table = {
        4: 4,   12: 12, 5: 5,   16: 14, 17: 14,   6: 6, 19: 6, 20: 6, 21: 6
    }
    # fmt: on
    pytest_passage_table = {
        RenewableProd(key): match for (key, match) in pytest_passage_table.items()
    }
    pytest_passage_fn = lambda key: pytest_passage_table.get(key, -1)
    pcaet = pd.DataFrame(
        {
            "siren": "1",
            "trajectory": "enr_production",
            "category": pcaet_category + pcaet_category,
            "year": [2010] * len(pcaet_category) + [2030] * len(pcaet_category),
            "is_ref": [True] * len(pcaet_category) + [False] * len(pcaet_category),
            "value": pcaet_reference + pcaet_objective,
        }
    )
    mock_async_function(
        "terriapi.controller.regions_configuration.get_configuration",
        [{"id": "pytest", "is_national": False, "enable_air_pollutant": False}],
        mocker,
    )
    history = pd.DataFrame(
        {
            "commune": "00000",
            "annee": 2020,
            "type_prod_enr": history_category,
            "valeur": history_reference,
        }
    )
    engine = get_pg_engine()
    history.to_sql("prod_enr", engine, "pytest", if_exists="append", index=False)

    modified_pcaet = await insert_relative_methodology_pcaet(pcaet)
    modified_pcaet["category"] = modified_pcaet["category"].apply(pytest_passage_fn)
    modified_pcaet = modified_pcaet.set_index(["category", "year"])
    modified_pcaet = modified_pcaet["relative_value"].astype(float)
    modified_pcaet = modified_pcaet.groupby(level=["category", "year"]).sum()

    for category, expected in zip(history_category, expected_objective):
        assert modified_pcaet[(category, 2030)] == expected


def _init_db(loop):
    loop.run_until_complete(
        execute(
            """
        INSERT INTO pytest.epci (nom, code, communes) VALUES ('EPCI de test', '200000001', '{''00001''}');
        INSERT INTO pytest.territoire VALUES
            ('00001', '200000001', 'epci', 'EPCI de test'),
            ('00001', '1', 'region', 'PYTEST region');
        INSERT INTO pytest.conso_energetique VALUES ('00001', 2003, 1, 1, 1, 1, 10);
        INSERT INTO pytest.emission_ges VALUES ('00001', 2003, 1, 1, 1, 1, 69);

        INSERT INTO pytest.pcaet VALUES
            ('200000001', 'consommation_ener', 'Sector.RESIDENTIAL', 2003, 40, TRUE, 10, 'pytest'),
            ('200000001', 'consommation_ener', 'Sector.RESIDENTIAL', 2026, 20, FALSE, 5, 'pytest'),
            ('200000001', 'consommation_ener', 'Sector.RESIDENTIAL', 2050, 0, FALSE, 0, 'pytest');
        INSERT INTO pytest.pcaet_meta VALUES
            ('200000001', 'Mise en œuvre', '07/09/2019', '11/07/2020', TRUE);
        """
        )
    )

    return "200000001"


def test_get_nonexisting_pcaet(
    mocker, region, indicators, client_anonymous, passage_table, example_action, loop
):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    _init_db(loop)
    loop.run_until_complete(
        execute("DELETE FROM pytest.pcaet WHERE siren='200000001';")
    )

    _request, response = client_anonymous.get(
        "/api/pytest/pcaet/trajectory/details/consommation_ener?zone=epci&zone_id=200000001",
    )
    assert response.status == 200
    data = response.json
    assert len(data["supra_goals"]) == 0
    assert data["pcaet_trajectory"] is None


def test_get_absolute_pcaet(
    mocker, region, indicators, client_anonymous, passage_table, example_action, loop
):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    _init_db(loop)
    loop.run_until_complete(
        execute(
            "UPDATE regions_configuration SET relative_pcaet='{}' WHERE id='pytest';"
        )
    )

    _request, response = client_anonymous.get(
        "/api/pytest/pcaet/trajectory/details/consommation_ener?zone=epci&zone_id=200000001",
    )
    assert response.status == 200
    data = response.json
    assert len(data["historical_data"]) == 1
    assert data["historical_data"][0]["data"] == [{"annee": 2003, "valeur": 10}]
    assert len(data["pcaet_trajectory"]) == 1
    assert data["pcaet_trajectory"][0]["data"] == [
        {"annee": 2003, "valeur": 40},
        {"annee": 2026, "valeur": 20},
        {"annee": 2050, "valeur": 0},
    ]


def test_get_relative_pcaet(
    mocker, region, indicators, client_anonymous, passage_table, example_action, loop
):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    _init_db(loop)
    loop.run_until_complete(
        execute(
            "UPDATE regions_configuration SET relative_pcaet='{consommation_ener}' WHERE id='pytest';"
        )
    )

    _request, response = client_anonymous.get(
        "/api/pytest/pcaet/trajectory/details/consommation_ener?zone=epci&zone_id=200000001",
    )
    assert response.status == 200
    data = response.json
    assert len(data["historical_data"]) == 1
    assert data["historical_data"][0]["data"] == [{"annee": 2003, "valeur": 10}]
    assert len(data["pcaet_trajectory"]) == 1
    assert data["pcaet_trajectory"][0]["data"] == [
        {"annee": 2003, "valeur": 10},
        {"annee": 2026, "valeur": 5},
        {"annee": 2050, "valeur": 0},
    ]


def test_get_pcaet_aggregation(
    mocker, region, indicators, client_admin_user, passage_table, loop
):
    mocker.patch("terriapi.controller.pcaet_ademe.PCAET_TABLE", "pytest.pcaet")
    mocker.patch(
        "terriapi.controller.pcaet_ademe.PCAET_META_TABLE", "pytest.pcaet_meta"
    )
    _init_db(loop)
    loop.run_until_complete(
        execute(
            """
        CREATE TABLE pytest.population (commune VARCHAR, annee INT, valeur FLOAT);
        INSERT INTO pytest.population VALUES ('00001', 2003, 42)
        """
        )
    )

    _request, response = client_admin_user.get("/api/pytest/pcaet/all")
    assert response.status == 200
    data = response.json
    assert len(data) == 2
    assert data[0]["siren"] == "200000001"
    assert data[0]["population"] == 42
    assert data[0]["consommation_ener_obs"] == {"1": 10}
    assert data[0]["consommation_ener_2026"] == {"1": 20}
    assert data[0]["consommation_ener_2050"] == {"1": 0}
    assert data[1]["siren"] == ""
    assert data[1]["nom"] == "PYTEST region"
