﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


def test_consultation_poi(region, client_basic_user, poi):
    # on consulte la page
    data = {
        "type_territoire": "region",
        "code_territoire": "84",
        "id_utilisateur": -1,
    }
    params = "&".join((k + "=" + str(v) for k, v in data.items()))
    _req, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/poi/layer/consult/Couche de test/true?" + params
    )
    assert resp.status == 200, "Impossible de consulter la page"


def test_consultation_page_autre(region, client_basic_user):
    # test d'une page simple
    data = {"page": "A propos", "details": "", "id_utilisateur": -1}
    _req, resp = client_basic_user.put(
        "/api/auvergne-rhone-alpes/consultation/autre/page/", json=data
    )
    assert resp.status == 200, "Impossible d'enregistrer le suivi d'une page simple"

    # test d'une page avec détails
    data = {
        "page": "Support - PDF",
        "details": "PDF méthodologique",
        "id_utilisateur": -1,
    }
    _req, resp = client_basic_user.put(
        "/api/auvergne-rhone-alpes/consultation/autre/page/", json=data
    )
    assert (
        resp.status == 200
    ), "Impossible d'enregistrer le suivi d'une page avec détails"

    # test d'une page où il manque des informations
    data = {"details": "PDF méthodologique", "id_utilisateur": -1}
    _req, resp = client_basic_user.put(
        "/api/auvergne-rhone-alpes/consultation/autre/page/", json=data
    )
    assert (
        resp.status == 400
    ), "Il n'est pas normal de pouvoir poster sans le nom de la page"

    # test d'une page où il manque des informations
    data = {
        "page": "Voici la page",
        "details": "PDF méthodologique",
    }
    _req, resp = client_basic_user.put(
        "/api/auvergne-rhone-alpes/consultation/autre/page/", json=data
    )
    assert (
        resp.status == 400
    ), "Il n'est pas normal de pouvoir poster sans le nom de la page"
