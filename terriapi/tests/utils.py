import asyncio
import http.client

from terriapi import settings

try:
    from unittest.mock import AsyncMock
except ImportError:
    AsyncMock = False


def mock_async_function(name, return_value, mocker):
    """
    Mocking an asynchronous function using either AsyncMock (if it exists in
    current Python version) or asyncio.Future in other cases.

    Will patch and add return_value as `side_effect` (or `return_value`).

    Parameters
    ----------
    name : str
        the function path to patch (e.g., *module1.file.function_name*)
    return_value : mixed
        the return value to use as output
    mocker : unittest.Mock
        the mocker tool that is able to patch
    """
    if AsyncMock:
        future = AsyncMock(return_value=return_value)
        mocker.patch(name, side_effect=future)
    else:
        future = asyncio.Future()
        future.set_result(return_value)
        mocker.patch(name, return_value=future)


def mock_http_response(mocker, return_value):
    """
    Mock an HTTP response and return the `return_value` given in parameter.

    Use http.client library to create HTTPResponse.

    Parameters
    ----------
    mocker : unittest.Mock
        the mocker tool that is able to Mock
    return_value : mixed
        the object used as return_value for the HTTPResponse.

    Returns
    -------
    http.client.HTTPResponse
        a response object mocked through the mocker.
    """
    # mock l'objet retourné par urllib.request.urlopen --> http.client.HTTPConnection
    http.client.HTTPConnection = mocker.Mock(spec=http.client.HTTPConnection)
    http.client.HTTPResponse = mocker.Mock(spec=http.client.HTTPResponse)
    connection = http.client.HTTPConnection()
    response = http.client.HTTPResponse()
    # contenu renvoyé habituellement par l'API
    response.read.return_value = return_value
    connection.getresponse.return_value = response
    return response


class MockResponseUrllibRequestUrlopen:
    """
    Mock class for urllib request urlopen function.
    """

    def __init__(self, response):
        self.response = response

    def __enter__(self):
        return self.response

    def __exit__(self, exc_type, exc_val, exc_tb):
        return None


class MockSettings:
    def __init__(self, new_settings: dict, section="api"):
        self.section = section
        self.new_settings = new_settings

    def get(self, section, option):
        if section == self.section and option in self.new_settings:
            return self.new_settings[option]
        return settings.get(section, option)
