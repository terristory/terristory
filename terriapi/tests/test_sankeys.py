# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
#
import json
from pathlib import Path

import pandas as pd

from terriapi import utils
from terriapi.controller import fetch

here = Path(__file__).resolve().parent


def test_sankeys_metadata(client_basic_user, sankeys, loop):
    # get user id
    _req, resp = client_basic_user.get(f"/api/pytest/sankey/meta_donnees/?sankey=")
    assert resp.status == 200

    _req, resp = client_basic_user.get(f"/api/pytest/sankey/meta_donnees/")
    assert resp.status == 200
    data = resp.json
    metadata = loop.run_until_complete(
        fetch(
            """SELECT * FROM meta.sankey_metadata
        WHERE region = $1 AND is_regional_default = true""",
            "pytest",
        )
    )
    metadata = dict(metadata[0])

    # we test the variables are equal to normal
    for var, value in data.items():
        if var in ("division_factors", "division_units"):
            if metadata[var] is not None:
                assert json.loads(metadata[var]) == value
            else:
                assert value == {}
        elif var in ("geographical_levels_enabled"):
            assert value == metadata[var].split(",")
        else:
            assert metadata[var] == value


def test_get_sankeys(region, client_basic_user, client_admin_user, sankeys):
    _req, resp = client_basic_user.get(f"/api/pytest/sankey/list/")
    assert resp.status == 401

    _req, resp = client_admin_user.get(f"/api/pytest/sankey/list/")
    assert resp.status == 200
    data = resp.json
    data_tables = set((a["data_table"] for a in data))
    assert data_tables == set((a["data_table"] for a in sankeys.values()))


def test_new_sankey(region, client_basic_user, client_admin_user, sankeys, **kwargs):
    fixture_sankey = {
        "data_table": "other_pytest_sankey_data_table",
        "region": "pytest",
        "year": 2023,
        "introduction_text": "This is another introduction text",
        "unit": "GWh",
        "source": "ORCAE",
        "copyright": False,
        "division_factors": {"region": "1", "epci": "1000"},
        "division_units": {"epci": "MWh"},
        "sankey_name": "This is another Sankey example for pytest region",
        "is_regional_default": False,
        "nb_decimals": 2,
        "geographical_levels_enabled": "epci,region,departement",
    }

    _req, resp = client_basic_user.post(f"/api/pytest/sankey/new/")
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.post(f"/api/pytest/sankey/new/")
    assert resp.status == 400

    _req, resp = client_admin_user.post(f"/api/pytest/sankey/new/", json=fixture_sankey)
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/sankey/list/")
    assert resp.status == 200
    data = resp.json

    # we check the slug is present and the API link has been added
    data_tables = set((a["data_table"] for a in data))
    assert fixture_sankey["data_table"] in data_tables


def test_get_sankey_details(region, client_basic_user, sankeys, **kwargs):
    sankey = sankeys["normal-test"]
    other_sankey = sankeys["other-non-default-sankey"]
    # missing params
    _req, resp = client_basic_user.get(f"/api/pytest/sankey/get/")
    assert resp.status == 404

    # default sankey
    _req, resp = client_basic_user.get(
        f"/api/pytest/sankey/get/?id_utilisateur=1&zone=region&zone_id=1"
    )
    assert resp.status == 200
    default_content = resp.json
    _req, resp = client_basic_user.get(
        f"/api/pytest/sankey/get/?id_utilisateur=1&zone=region&zone_id=1&sankey={sankey['data_table']}"
    )
    assert resp.status == 200
    sankey_content = resp.json
    assert default_content == sankey_content

    # default sankey
    _req, resp = client_basic_user.get(
        f"/api/pytest/sankey/get/?id_utilisateur=1&zone=region&zone_id=1&sankey={other_sankey['data_table']}"
    )
    assert resp.status == 200
    other_sankey_content = resp.json
    assert default_content != other_sankey_content


def test_get_disabled_sankey(region, client_basic_user, sankeys, **kwargs):
    sankey = sankeys["disabled_sankey"]

    _req, resp = client_basic_user.get(
        f"/api/pytest/sankey/get/?id_utilisateur=1&zone=region&zone_id=1&sankey={sankey['data_table']}"
    )
    assert resp.status == 200
    sankey_content = resp.json
    assert sankey_content["statut"] == "non disponible"


def test_get_confid_sankey(region, client_basic_user, sankeys, **kwargs):
    sankey = sankeys["confid-sankey"]

    _req, resp = client_basic_user.get(
        f"/api/pytest/sankey/get/?id_utilisateur=1&zone=region&zone_id=1&sankey={sankey['data_table']}"
    )
    assert resp.status == 200
    sankey_content = resp.json
    assert (
        sankey_content["links"]["link0"]["value"]["display_value"] == "confidentielle"
    )


def test_new_sankey_is_default(
    region, client_basic_user, client_admin_user, sankeys, **kwargs
):
    fixture_sankey = {
        "data_table": "other_pytest_sankey_data_table",
        "region": "pytest",
        "year": 2023,
        "introduction_text": "This is another introduction text",
        "unit": "GWh",
        "source": "ORCAE",
        "copyright": False,
        "division_factors": {"region": "1", "epci": "1000"},
        "division_units": {"epci": "MWh"},
        "sankey_name": "This is another Sankey example for pytest region",
        "is_regional_default": True,
        "nb_decimals": 2,
        "geographical_levels_enabled": "epci,region,departement",
    }

    _req, resp = client_basic_user.post(f"/api/pytest/sankey/new/")
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.post(f"/api/pytest/sankey/new/")
    assert resp.status == 400

    _req, resp = client_admin_user.post(f"/api/pytest/sankey/new/", json=fixture_sankey)
    assert resp.status == 200

    _req, resp = client_admin_user.get(
        f"/api/pytest/sankey/get/?id_utilisateur=1&zone=region&zone_id=1"
    )
    assert resp.status == 200


def test_update_sankey(region, client_basic_user, client_admin_user, sankeys, **kwargs):
    fixture_sankey = {
        "region": "pytest",
        "year": 2023,
        "introduction_text": "This is another introduction text",
        "unit": "GWh",
        "source": "ORCAE",
        "copyright": False,
        "division_factors": {"region": "1", "epci": "1000"},
        "division_units": {"epci": "MWh"},
        "sankey_name": "This is another Sankey example for pytest region",
        "is_regional_default": False,
        "nb_decimals": 2,
        "geographical_levels_enabled": "epci,region,departement",
    }
    existing_link = sankeys["normal-test"]
    existing_data_table = existing_link["data_table"]
    existing_sankey_name = existing_link["sankey_name"]

    _req, resp = client_basic_user.put(
        f"/api/pytest/sankey/update/{existing_data_table}/"
    )
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.put(
        f"/api/pytest/sankey/update/{existing_data_table}/"
    )
    assert resp.status == 400

    # wrong existing_data_table
    _req, resp = client_admin_user.put(
        f"/api/pytest/sankey/update/not_existing_data_table/", json=fixture_sankey
    )
    assert resp.status == 404

    _req, resp = client_admin_user.put(
        f"/api/pytest/sankey/update/{existing_data_table}/", json=fixture_sankey
    )
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/sankey/list/")
    assert resp.status == 200
    data = resp.json

    # we check the slug is still present
    data_tables = set((a["data_table"] for a in data))
    assert existing_data_table in data_tables

    # we just check the table name has been edited present
    sankey_names = set((a["sankey_name"] for a in data))
    # not present anymore
    assert existing_sankey_name not in sankey_names
    # has been replaced
    assert fixture_sankey["sankey_name"] in sankey_names


def test_update_layout_sankey(
    region, client_basic_user, client_admin_user, sankeys, **kwargs
):
    existing_link = sankeys["normal-test"]
    existing_data_table = existing_link["data_table"]

    _req, resp = client_basic_user.put(
        f"/api/pytest/sankey/layout/{existing_data_table}/"
    )
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.put(
        f"/api/pytest/sankey/layout/{existing_data_table}/"
    )
    assert resp.status == 404

    # wrong existing_data_table
    _req, resp = client_admin_user.put(
        f"/api/pytest/sankey/layout/not_existing_data_table/",
        data={"template": "json"},
    )
    assert resp.status == 404

    # we just check the table name has been edited present
    with open(here / "data" / "new_sankey_layout.json", "r") as fd:
        layout_new_json = json.load(fd)

    # we check we have a different layout first
    _req, resp = client_admin_user.get(
        f"/api/pytest/sankey/layout/{existing_data_table}/"
    )
    assert resp.status == 200
    data = resp.json
    assert data != layout_new_json

    with open(here / "data" / "new_sankey_layout.json", "rb") as fd:
        _req, resp = client_admin_user.put(
            f"/api/pytest/sankey/layout/{existing_data_table}/", files={"template": fd}
        )
        assert resp.status == 200

    # we did update with the new layout
    _req, resp = client_admin_user.get(
        f"/api/pytest/sankey/layout/{existing_data_table}/"
    )
    assert resp.status == 200
    data = resp.json
    assert data == layout_new_json


def test_update_data_sankey(
    region, client_basic_user, client_admin_user, sankeys, loop, **kwargs
):
    existing_link = sankeys["normal-test"]
    existing_data_table = existing_link["data_table"]

    _req, resp = client_basic_user.put(
        f"/api/pytest/sankey/data/{existing_data_table}/"
    )
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.put(
        f"/api/pytest/sankey/data/{existing_data_table}/"
    )
    assert resp.status == 404

    # wrong existing_data_table
    _req, resp = client_admin_user.put(
        f"/api/pytest/sankey/data/not_existing_data_table/",
        data={"template": "json"},
    )
    assert resp.status == 404

    # we just check the table name has been edited present
    # code column is varchar
    data_new_csv = pd.read_csv(
        here / "data" / "sankey_data.csv", sep=";", dtype={"code": str}
    )
    data_new_csv = data_new_csv.to_dict("records")
    existing_data = loop.run_until_complete(
        fetch(
            f"""SELECT * FROM pytest.{existing_data_table}""",
        )
    )
    existing_data = [dict(row) for row in existing_data]
    assert data_new_csv != existing_data

    with open(here / "data" / "sankey_data.csv", "rb") as fd:
        _req, resp = client_admin_user.put(
            f"/api/pytest/sankey/data/{existing_data_table}/", files={"data_file": fd}
        )
        assert resp.status == 200

    existing_data = loop.run_until_complete(
        fetch(
            f"""SELECT * FROM pytest.{existing_data_table}""",
        )
    )
    existing_data = [dict(row) for row in existing_data]
    assert data_new_csv == existing_data


def test_delete_sankey(region, client_basic_user, client_admin_user, sankeys, **kwargs):
    existing_link = sankeys["normal-test"]
    existing_data_table = existing_link["data_table"]

    _req, resp = client_basic_user.delete(
        f"/api/pytest/sankey/delete/{existing_data_table}/"
    )
    assert resp.status == 401

    # wrong data_table
    _req, resp = client_admin_user.delete(
        f"/api/pytest/sankey/delete/not_existing_data_table/"
    )
    assert resp.status == 404

    _req, resp = client_admin_user.delete(
        f"/api/pytest/sankey/delete/{existing_data_table}/"
    )
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/sankey/list/")
    assert resp.status == 200
    data = resp.json

    # we check the slug is still present
    data_tables = set((a["data_table"] for a in data))
    assert existing_data_table not in data_tables


# we test the parse table name function
# even if it was already tested in external_api tests
# as it could be disabled
def test_parse_table_name():
    assert utils.parse_table_name("test") == "test"
    assert utils.parse_table_name("test&&&") == "test"
    assert utils.parse_table_name("te-st-with-dash") == "te_st_with_dash"
    assert utils.parse_table_name("try to ' insert code") == "try_to_insert_code"
    long_str = "varchar_way_too_long_to_be_a_fittable_table_name"
    assert utils.parse_table_name(long_str) == long_str[0:40]
