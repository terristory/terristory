# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pytest

from terriapi.controller import compute_formula
from terriapi.controller.donnees_pour_representation import (
    FabriqueRepresentationsTableauBord,
    Representations,
)


@pytest.mark.parametrize(
    "level,scale,other_parameters,expected",
    [
        [
            "region",
            "commune",
            {
                "disabled_for_macro_level": "",
                "disabled_for_zone": "",
                "only_for_zone": "",
            },
            True,
        ],
        [
            "region",
            "commune",
            {
                "disabled_for_macro_level": "region",
                "disabled_for_zone": "",
                "only_for_zone": "",
            },
            False,
        ],
        [
            "region",
            "commune",
            {
                "disabled_for_macro_level": "departement,epci",
                "disabled_for_zone": "commune",
                "only_for_zone": "",
            },
            True,
        ],
        [
            "region",
            "epci",
            {
                "disabled_for_macro_level": "departement,epci",
                "disabled_for_zone": "epci,commune",
                "only_for_zone": "",
            },
            True,
        ],
        [
            "departement",
            "epci",
            {
                "disabled_for_macro_level": "region,epci",
                "disabled_for_zone": "",
                "only_for_zone": "epci,commune",
            },
            True,
        ],
        [
            "departement",
            "epci",
            {
                "disabled_for_macro_level": "region,epci",
                "disabled_for_zone": "",
                "only_for_zone": "commune",
            },
            True,
        ],
    ],
)
def test_unit_enabling_disabling_indicators_in_dashboards(
    level, scale, other_parameters, expected
):
    caracteristiques_indicateur = {
        "id": 1,
        "filtre_initial_modalites_par_categorie": [],
        "region": "region",
        "schema_region": "schema_region",
        "definition_indicateur": {
            "ratio": "",
            "coeff": "",
            "indicateur": "",
        },
        "years": [],
    }

    caracteristiques_indicateur = dict(
        **caracteristiques_indicateur, **other_parameters
    )

    representation = FabriqueRepresentationsTableauBord(
        caracteristiques_indicateur, scale, level, "1", "pie"
    )
    assert representation.afficher_indicateur_maille == expected


@pytest.mark.parametrize(
    "level,scale,other_parameters,expected",
    [
        [
            "region",
            "commune",
            {
                "disabled_for_macro_level": "",
                "disabled_for_zone": "",
                "only_for_zone": "",
            },
            True,
        ],
        [
            "region",
            "commune",
            {
                "disabled_for_macro_level": "region",
                "disabled_for_zone": "",
                "only_for_zone": "",
            },
            False,
        ],
        [
            "region",
            "commune",
            {
                "disabled_for_macro_level": "departement,epci",
                "disabled_for_zone": "commune",
                "only_for_zone": "",
            },
            False,
        ],
        [
            "region",
            "epci",
            {
                "disabled_for_macro_level": "departement,epci",
                "disabled_for_zone": "epci,commune",
                "only_for_zone": "",
            },
            False,
        ],
        [
            "departement",
            "epci",
            {
                "disabled_for_macro_level": "region,epci",
                "disabled_for_zone": "",
                "only_for_zone": "epci,commune",
            },
            True,
        ],
        [
            "departement",
            "epci",
            {
                "disabled_for_macro_level": "region,epci",
                "disabled_for_zone": "",
                "only_for_zone": "commune",
            },
            False,
        ],
    ],
)
def test_unit_enabling_disabling_indicators_in_map(
    level, scale, other_parameters, expected
):
    caracteristiques_indicateur = {
        "id": 1,
        "filtre_initial_modalites_par_categorie": [],
        "region": "region",
        "schema_region": "schema_region",
        "definition_indicateur": {
            "ratio": "",
            "coeff": "",
            "indicateur": "",
        },
        "years": [],
    }

    caracteristiques_indicateur = dict(
        **caracteristiques_indicateur, **other_parameters
    )

    representation = Representations(caracteristiques_indicateur, scale, level, "1")
    assert representation.afficher_indicateur_maille == expected


@pytest.mark.parametrize(
    "formula,expected",
    [
        [
            "population",
            {
                "indicateur": "population",
                "coeff": "",
                "ratio": "",
            },
        ],
        [
            "population*1000",
            {
                "indicateur": "population",
                "coeff": "*1000",
                "ratio": "",
            },
        ],
        [
            "utcatf_stock/maille.superficie",
            {
                "indicateur": "utcatf_stock",
                "coeff": "",
                "ratio": "/maille.superficie",
            },
        ],
        [
            "population/1000",
            {
                "indicateur": "population",
                "coeff": "/1000",
                "ratio": "",
            },
        ],
        [
            "population/1000/maille.superficie",
            {
                "indicateur": "population",
                "coeff": "/1000",
                "ratio": "/maille.superficie",
            },
        ],
        [
            "population/maille.superficie*0.0001",
            {
                "indicateur": "population",
                "coeff": "*0.0001",
                "ratio": "/maille.superficie",
            },
        ],
        [
            "population*0.0001/maille.superficie",
            {
                "indicateur": "population",
                "coeff": "*0.0001",
                "ratio": "/maille.superficie",
            },
        ],
        [
            "population/maille.superficie/1000",
            {
                "indicateur": "population",
                "coeff": "/1000",
                "ratio": "/maille.superficie",
            },
        ],
        [
            "population/maille.superficie/1000.0*12.5",
            {
                "indicateur": "population",
                "coeff": "*12.5/1000.0",
                "ratio": "/maille.superficie",
            },
        ],
        [
            "actif_1564/maille.population_1564::float*100",
            {
                "indicateur": "actif_1564",
                "coeff": "*100",
                "ratio": "/maille.population_1564",
            },
        ],
        [
            "conso_energetique*1000000/maille.emploi_tertiaire",
            {
                "indicateur": "conso_energetique",
                "coeff": "*1000000",
                "ratio": "/maille.emploi_tertiaire",
            },
        ],
    ],
)
def test_unit_generic_formula_handling(formula, expected):
    results = compute_formula(formula)
    for key, value in expected.items():
        assert results[key] == value


@pytest.mark.parametrize(
    "formula,expected",
    [
        [
            "utcatf_stock/superficie",
            {
                "indicateur": "utcatf_stock",
                "coeff": "",
                "ratio": "",
            },
        ],
        [
            "population/1000/1000/1000",
            {
                "indicateur": "population",
                "coeff": "/1000",
                "ratio": "",
            },
        ],
        [
            "population' UNION PATATE '/maille.superficie",
            {
                "indicateur": "population",
                "coeff": "",
                "ratio": "/maille.superficie",
            },
        ],
        [
            "0.0001*population/maille.superficie*0.0001",
            {
                "indicateur": "",
                "coeff": "",
                "ratio": "",
            },
        ],
    ],
)
def test_unit_failing_generic_formula_handling(formula, expected):
    results = compute_formula(formula)
    for key, value in expected.items():
        assert results[key] == value
