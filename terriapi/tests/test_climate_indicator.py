﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


def test_analysis_climate(region, client_basic_user):
    zone, maille, zone_id = "region", "epci", "84"

    # we get all analyses
    _req, resp = client_basic_user.get("/api/auvergne-rhone-alpes/analysis")
    assert resp.status == 200
    content = resp.json
    id_analyse = -1
    for analysis in content:
        if analysis["data_type"] == "climat":
            id_analyse = analysis["id"]
            break
    assert id_analyse > 0

    # we get the climatic analysis
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={
            "zone": zone,
            "maille": maille,
            "zone_id": zone_id,
            "annee": 2016,
            "provenance": "test",
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 200
    content = resp.json
    assert "stations_mesures" in content
