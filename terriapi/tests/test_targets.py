﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json

import pytest


@pytest.fixture
def data_target():
    titre = "Ceci est un test"
    start_year, end_year = 2015, 2050
    years = range(start_year, end_year + 1, 1)
    edited_years = [2018, 2030, 2038, 2041, 2044, 2050]
    values = [
        0.00,
        -2.90,
        -5.70,
        -8.60,
        -11.30,
        -14.10,
        -16.80,
        -19.50,
        -22.30,
        -25.00,
        -27.80,
        -30.50,
        -33.20,
        -36.00,
        -38.70,
        -41.40,
        -41.90,
        -42.40,
        -42.90,
        -43.40,
        -43.90,
        -44.40,
        -44.80,
        -45.30,
        -40.10,
        -34.90,
        -29.70,
        -42.20,
        -54.60,
        -67.10,
        -70.62,
        -74.13,
        -77.65,
        -81.16,
        -84.68,
        -88.19,
    ]
    objs = [
        {
            "annee": year,
            "valeur": values[i],
            "id": f"{i}{titre}",
            "annee_modifiee": year in edited_years,
        }
        for i, year in enumerate(years)
    ]

    params = {
        "graphiques": "DataSet.CONSUMPTION",
        "real_table": "conso_energetique",
        "annee": start_year,
        "titre": titre,
        "couleur": "#3a7c36",
        "derniereAnneeProjection": end_year,
        "description": "Ceci est la description du test",
        "filter": [],
    }

    territories = [
        {"type_territoire": "epci", "code_territoire": "1"},
    ]

    return {"obj": objs, "params": params, "link": territories}


@pytest.fixture
def data_updated_target():
    titre = "Ceci est un test"
    start_year, end_year = 2015, 2050
    years = range(start_year, end_year + 1, 1)
    edited_years = [2018, 2030, 2038, 2041, 2044, 2048]
    values = [
        0.00,
        -2.90,
        -5.70,
        -8.60,
        -11.30,
        -14.10,
        -16.80,
        -19.50,
        -22.30,
        -25.00,
        -27.80,
        -30.50,
        -33.20,
        -36.00,
        -38.70,
        -41.40,
        -41.90,
        -42.40,
        -42.90,
        -43.40,
        -43.90,
        -44.40,
        -44.80,
        -45.30,
        -40.10,
        -34.90,
        -29.70,
        -42.20,
        -54.60,
        -67.10,
        -70.62,
        -74.13,
        -77.65,
        -81.16,
        -81.16,
        -81.16,
    ]
    objs = [
        {
            "annee": year,
            "valeur": values[i],
            "id": f"{i}{titre}",
            "annee_modifiee": year in edited_years,
        }
        for i, year in enumerate(years)
    ]

    params = {
        "graphiques": "DataSet.CONSUMPTION",
        "real_table": "conso_energetique",
        "annee": start_year,
        "titre": titre,
        "couleur": "#3a7c36",
        "derniereAnneeProjection": end_year,
        "description": "Ceci est la description du test mis à jour",
        "filter": [],
    }

    territories = [
        {"type_territoire": "epci", "code_territoire": ""},
    ]
    return {"obj": objs, "params": params, "link": territories}


@pytest.fixture
def target(region, indicators, client_admin_user, data_target, passage_table):
    """
    Create a trajectory with admin user
    """
    _req, resp = client_admin_user.put("/api/pytest/supra/goals", json=data_target)
    assert resp.status == 200
    content = resp.json
    yield content["supra_goal_id"]
    # we delete what we just created
    _req, resp = client_admin_user.delete(
        f"/api/pytest/supra/goals/{content['supra_goal_id']}"
    )
    assert resp.status == 200


def test_protection_api_parameters(
    region, indicators, client_basic_user, client_admin_user
):
    # we test a fake region
    _req, resp = client_basic_user.get(
        f"/api/this-is-'also-a-fake-'region/this_is_most_probably_a_fake_table/annees"
    )
    assert resp.status == 401
    # we test a fake table
    _req, resp = client_basic_user.get(
        f"/api/pytest/this_is_most_probably_a_fake_table/annees"
    )
    assert resp.status == 404


def test_checks_some_failing_urls(
    region, indicators, client_basic_user, client_admin_user
):
    # we test a failing graph
    _req, resp = client_admin_user.get(f"/api/pytest/supra/goals/-1")
    assert resp.status == 404
    content = resp.json
    assert content["message"] == "Objectif inexistant !"
    # we test a failing graph
    _req, resp = client_admin_user.delete(f"/api/pytest/supra/goals/-1")
    assert resp.status == 404
    content = resp.json
    assert content["message"] == "Objectif inexistant !"


def test_check_association(
    region, indicators, client_basic_user, client_admin_user, passage_table
):
    # we test a failing graph
    _req, resp = client_basic_user.get(f"/api/pytest/pcaet/trajectories/list")
    assert resp.status == 200
    content = resp.json
    # we check we have the three tables defined in passage table for pytest region
    assert set([i["table"] for i in content]) == {
        "DataSet.PRODUCTION",
        "DataSet.CONSUMPTION",
        "DataSet.EMISSIONS",
    }
    assert set([i["name"] for i in content]) == {
        "Production d'énergie renouvelable",
        "Consommation d'énergie",
        "Émissions de GES",
    }


def test_years_configurations(region, indicators, client_basic_user):
    tables = ["conso_energetique"]

    for table in tables:
        # get user id
        _req, resp = client_basic_user.get(f"/api/pytest/{table}/annees")
        assert resp.status == 200
        data = resp.json
        # we have at list one data
        assert len(data) > 0


def test_targets_management_rights(
    region, indicators, client_basic_user, client_admin_user, target, data_target
):
    # we try creating without authorization
    _req, resp = client_basic_user.put(f"/api/pytest/supra/goals")
    assert resp.status == 401


def test_targets_data(region, indicators, client_anonymous, client_admin_user, target):
    _req, resp = client_admin_user.get(f"/api/pytest/supra/goals/{target}/")
    assert resp.status == 200
    data = resp.json

    _request, resp = client_anonymous.get(f"/api/pytest/supra/goals/{target}/")
    assert resp.status == 200
    data = resp.json

    # we check we have the right number of years data
    first_year, last_year = (
        data["goal_meta"]["annee_reference"],
        data["goal_meta"]["derniere_annee_projection"],
    )
    assert len(data["goal_values"]) == last_year - first_year + 1


def test_target_creation(
    region, indicators, client_anonymous, client_admin_user, target, data_target
):
    _req, resp = client_admin_user.get(f"/api/pytest/supra/goals/{target}/")
    assert resp.status == 200

    _req, resp = client_admin_user.put("/api/pytest/supra/goals", json=data_target)
    assert resp.status == 400
    content = resp.json
    assert (
        content["message"]
        == "Il existe déjà un objectif associé à ce graphique qui porte ce nom"
    )


def test_targets_management_epci(
    region, indicators, client_basic_user, client_admin_user, target, data_target
):
    """
    This test checks that the targets management system is functioning.
    """
    # the data related to the target handled here
    params_link = {
        "zone": "epci",
        "zone_id": "1",
        "nom_territoire": "Territoire de test",
        "id_utilisateur": -1,
    }

    # get user id
    _req, resp = client_admin_user.get(f"/api/pytest/supra/goals")
    assert resp.status == 200
    data = resp.json

    # we have at least one data
    assert len(data) > 0
    # we do have the target created beforehand
    assert (
        len(
            [
                d
                for d in data
                if d["titre"] in data_target["params"]["titre"]
                and d["graphique"] == data_target["params"]["graphiques"]
                and d["match"] == data_target["params"]["real_table"]
            ]
        )
        == 1
    )

    _req, resp = client_admin_user.get(f"/api/pytest/supra/goals/{target}")
    affectations = resp.json["assignations"]
    # we have an affectation
    assert len(affectations) == 1
    assert (
        affectations[0]["type_territoire"] == "epci"
        and affectations[0]["code_territoire"] == "1"
    )

    # we check that targets are included in trajectories
    _req, resp = client_admin_user.get(
        "/api/pytest/pcaet/trajectory/details/consommation_ener", params=params_link
    )
    assert resp.status == 200
    content = resp.json

    # we have information regarding our trajectory
    trajectories_data_saved = content["supra_goals"]
    related_data_trajectory = [
        d
        for d in trajectories_data_saved
        if d["titre"] == data_target["params"]["titre"]
        and d["couleur"] == data_target["params"]["couleur"]
        and d["annee_reference"] == data_target["params"]["annee"]
    ]
    assert len(related_data_trajectory) == 1

    # we have the right affectations
    links = json.loads(related_data_trajectory[0]["affectation"])
    assert "epci" in links and links["epci"] == "1"


def test_update_target_values(
    region,
    indicators,
    client_admin_user,
    target,
    data_target,
    data_updated_target,
    passage_table,
):
    """
    Update a target
    """
    _req, resp = client_admin_user.put(
        f"/api/pytest/supra/goals/{target}/",
        json=data_updated_target,
    )
    assert resp.status == 200
    content = resp.json
    assert content["message"] == "Objectif mis à jour"

    _req, resp = client_admin_user.get(f"/api/pytest/supra/goals/{target}/")
    data = resp.json["goal_values"]
    metadata = resp.json["goal_meta"]
    assert resp.status == 200
    assert len(data) > 0
    assert metadata["titre"] == data_updated_target["params"]["titre"]
    assert metadata["description"] == data_updated_target["params"]["description"]
    data_2050 = next(filter(lambda x: x["annee"] == 2050, data))
    data_updated_target_2050 = next(
        filter(lambda x: x["annee"] == 2050, data_updated_target["obj"])
    )
    assert data_2050["valeur"] == data_updated_target_2050["valeur"]

    # the data related to the target handled here
    params_link = {
        "zone": "epci",
        "zone_id": "200000172",
        "nom_territoire": "Territoire de test",
        "id_utilisateur": -1,
    }

    # we check that targets are included in trajectories
    _req, resp = client_admin_user.get(
        "/api/pytest/pcaet/trajectory/details/consommation_ener", params=params_link
    )
    content = resp.json
    assert resp.status == 200

    # we have information regarding our trajectory
    trajectories_data_saved = content["supra_goals"]
    related_data_trajectory = [
        d
        for d in trajectories_data_saved
        if d["titre"] == data_target["params"]["titre"]
        and d["couleur"] == data_target["params"]["couleur"]
        and d["annee_reference"] == data_target["params"]["annee"]
    ]
    assert len(related_data_trajectory) == 1

    # we have the right affectations
    links = json.loads(related_data_trajectory[0]["affectation"])
    assert "epcis" in links
