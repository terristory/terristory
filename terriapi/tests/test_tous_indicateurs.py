﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from itertools import islice

import pytest
from numpy import isclose

from terriapi.controller import fetch


def get_api_values(client_anonymous):
    """Demande à l'API les résultats des analyses
    et teste chacune des valeurs issues de la table résultat des tests

    """
    assert 1 == 1
    # 1 - lire dans tests.resultat les lignes pour l'indicateur testé
    # query = "select indicateur, region, params, resultat from tests.resultats order by indicateur"
    # res = await fetch(query)
    # for r in res :
    #     id = r["indicateur"]
    #     region = r["region"]
    #     params_url = [json.loads(r["params"])]
    #     resultat = json.loads(r["resultat"])
    #
    #     # 2 - pour chaque params, jouer l'url et comparer le résultat avec ce qui est attendu
    #     # Lancement du calcul via l'api
    #     terrivalues = await asyncio.gather(
    #         *[client_anonymous.post("/api/{region}/analysis/{id}/data".format(region=region, id=id), params=params)
    #         for params in params_url]
    #     )
    #     terrivalues = await asyncio.gather(*[res.json() for res in terrivalues])
    #     # assert 1 == 1
    #     for i in range(len(terrivalues[0]["map"])):
    #         if (terrivalues[0]["map"][i]["val"] != None and resultat[0]["map"][i]["val"] != None) and (type(terrivalues[0]["map"][i]["val"]) != str and type(resultat[0]["map"][i]["val"]) != str):
    #             if float(terrivalues[0]["map"][i]["val"]) == float(resultat[0]["map"][i]["val"]):
    #                 assert float(terrivalues[0]["map"][i]["val"]) == float(resultat[0]["map"][i]["val"])
    #         if 'x' in terrivalues[0]["map"][i].keys() and 'y' in terrivalues[0]["map"][i].keys() and 'code' in terrivalues[0]["map"][i].keys() and 'nom' in terrivalues[0]["map"][i].keys():
    #             assert float(terrivalues[0]["map"][i]["x"]) == float(resultat[0]["map"][i]["x"])
    #             assert float(terrivalues[0]["map"][i]["y"]) == float(resultat[0]["map"][i]["y"])
    #             assert terrivalues[0]["map"][i]["code"] == resultat[0]["map"][i]["code"]
    #             assert terrivalues[0]["map"][i]["nom"] == resultat[0]["map"][i]["nom"]


def test_tous_indicateurs(client_anonymous, loop):
    get_api_values(client_anonymous)
