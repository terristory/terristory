# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import asyncio

import pytest
from sqlalchemy.sql import text

from terriapi import controller


def test_basic(app, loop):
    pass


def test_basic_1(app, loop):
    pass


def test_basic_2(app, loop):
    pass


def test_basic_3(app, loop):
    pass


def test_client_anonymous(app, global_ctx, client_anonymous, loop):
    client_anonymous.get("/")


def test_client_anonymous_1(app, global_ctx, client_anonymous, loop):
    client_anonymous.get("/")


def test_client_anonymous_2(app, global_ctx, client_anonymous, loop):
    client_anonymous.get("/")


def test_client_anonymous_3(app, global_ctx, client_anonymous, loop):
    client_anonymous.get("/")


def test_client_basic_user(app, client_basic_user, loop):
    client_basic_user.get("/")


def test_two_clients(app, client_anonymous, client_basic_user, loop):
    client_anonymous.get("/")
    client_basic_user.get("/")


def test_clients_correct_auth(app, client_anonymous, client_basic_user, loop):
    req, res = client_anonymous.get("/api/pytest/auth/me")
    req, res = client_basic_user.get("/api/pytest/auth/me")


def test_client_second_basic_user(app, client_second_basic_user, loop):
    client_second_basic_user.get("/")


def test_db(app, db, loop):
    pass


def test_db_1(app, db, loop):
    pass


def test_db_2(app, db, loop):
    pass


def test_db_3(app, db, loop):
    pass


def test_db_query(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_db_query_1(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_db_query_2(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_db_query_3(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_controller_db_query(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_controller_db_query_1(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_controller_db_query_2(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_controller_db_query_3(app, db, loop):
    loop.run_until_complete(controller.execute("SELECT * FROM utilisateur"))


def test_controller_db_fetch(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_controller_db_fetch_1(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_controller_db_fetch_2(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_controller_db_fetch_3(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_main_structure(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_main_structure_1(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_main_structure_2(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_main_structure_3(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_second_basic(app, global_ctx, client_anonymous, loop):
    pass


def test_second_bis_with_db(app, db, client_anonymous, loop):
    pass


def test_simple_query_with_client(app, db, loop):
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))
    loop.run_until_complete(controller.fetch("SELECT * FROM utilisateur"))


def test_third_with_client(app, db, region, loop):
    pass


def test_real_client(app, db, region, client_basic_user, loop):
    client_basic_user.get("/")


def test_region(db, loop):
    pass


def test_global_ctx(global_ctx):
    pass


def test_loop_differences_b(app, loop):
    assert id(loop) == id(asyncio.get_event_loop())


def test_loop_differences_3(app, loop):
    assert id(loop) == id(asyncio.get_event_loop())
