# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from unittest.mock import AsyncMock

import pytest

import terriapi.controller.regions_configuration as regions_configuration


def test_create_dashboard(client_admin_user, dashboard):
    """Création d'un nouveau tableau de bord en base de données."""
    _req, resp = client_admin_user.get("/api/pytest/my/dashboards")
    assert resp.status == 200
    content = resp.json
    assert dashboard in [x["id"] for x in content]


def test_dashboard_same_elements_must_fail(client_admin_user, dashboard):
    """Création de deux tableaux de bord avec même (titre, mail, région).

    On vérifie que ça échoue avec le bon message d'erreur.
    """
    # on récupère les données du tableau déjà en base.
    _req, resp = client_admin_user.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200
    payload = resp.json
    # on tente d'enregistrer le même contenu dans un nouveau tableau.
    _req, resp = client_admin_user.post("/api/pytest/tableau", json=payload)
    assert resp.status == 400
    content = resp.json
    assert content["message"] == "Un tableau de bord existe déjà pour vous avec ce nom."


def test_assignments_dashboard(client_admin_user, dashboard):
    """On affecte une zone (type de territoire) à un tableau de bord."""
    payload = {"zone": "departement"}
    _req, resp = client_admin_user.put(
        f"/api/pytest/tableau/{dashboard}/affectation", json=payload
    )
    assert resp.status == 200
    content = resp.json
    assert content["message"] == "Tableau de bord publié."
    # Si on souhaite affecter la même chose, on obtient un code HTTP différent.
    _req, resp = client_admin_user.put(
        f"/api/pytest/tableau/{dashboard}/affectation", json=payload
    )
    assert resp.status == 400
    # Avec la liste des tableaux associés à un type de territoire, on vérifie que l'affectation s'est bien passée.
    _req, resp = client_admin_user.get(
        "/api/pytest/public/dashboards", params={"zone": "departement"}
    )
    assert resp.status == 200
    content = resp.json
    assert len(content) > 0
    assert dashboard in [x["id"] for x in content]


def test_rights_dashboard(client_basic_user, client_admin_user, dashboard, loop):
    # on vérifie que le tableau de bord existe bien
    _req, resp = client_admin_user.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200, "On n'a accès au tableau avec l'utilisateur original."

    regional_settings = loop.run_until_complete(
        regions_configuration.get_configuration("pytest")
    )
    allow_sharing_dashboard_by_url = regional_settings[0][
        "allow_sharing_dashboard_by_url"
    ]

    # on vérifie que l'autre utilisateur n'y a pas accès
    _req, resp = client_basic_user.get(f"/api/pytest/tableau/{dashboard}")
    if allow_sharing_dashboard_by_url:
        assert (
            resp.status == 200
        ), "On n'a pas accès au tableau d'un autre utilisateur alors que la configuration est faite."
    else:
        assert resp.status == 404, "On a accès au tableau avec le mauvais utilisateur."

    # on vérifie que le tableau de bord ne peut être pas publié par la mauvaise personne
    _req, resp = client_basic_user.put(
        f"/api/pytest/tableau/{dashboard}/affectation?zone=departement&zone_id=01",
        json={"zone": "departement", "zone_id": "01"},
    )
    assert (
        resp.status == 401
    ), "Il est possible de publier un tableau avec la mauvaise personne."


def test_unauthorized_dashboard_deletion(
    client_basic_user, client_admin_user, dashboard
):
    # on vérifie que le tableau de bord existe bien
    _req, resp = client_admin_user.get("/api/pytest/my/dashboards")
    assert resp.status == 200, "La liste ne fonctionne pas."
    content = resp.json
    assert dashboard in [x["id"] for x in content]

    # on tente de supprimer avec le mauvais utilisateur
    _req, resp = client_basic_user.delete(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 400, "La suppression a fonctionné, ce n'est pas normal"


def test_publishing_dashboard(client_basic_user, client_admin_user, dashboard, loop):
    regional_settings = loop.run_until_complete(
        regions_configuration.get_configuration("pytest")
    )
    allow_sharing_dashboard_by_url = regional_settings[0][
        "allow_sharing_dashboard_by_url"
    ]

    # on tente d'accéder avec le mauvais utilisateur => on devrait pouvoir
    _req, resp = client_basic_user.get(
        f"/api/pytest/tableau/{dashboard}?zone=departement&maille=commune&zone_id=01&id_tableau={dashboard}"
    )
    if allow_sharing_dashboard_by_url:
        assert resp.status == 200, "Le tableau n'est accessible pour tout le monde."
    else:
        assert (
            resp.status == 404
        ), "Le tableau est accessible pour le mauvais utilisateur."

    # on vérifie que le tableau de bord peut être publié
    _req, resp = client_admin_user.put(
        f"/api/pytest/tableau/{dashboard}/affectation?zone=departement&zone_id=01",
        json={"zone": "departement", "zone_id": "01"},
    )
    assert resp.status == 200, "Impossible de publier le tableau."

    # on tente d'accéder avec le mauvais utilisateur => on devrait pouvoir
    _req, resp = client_basic_user.get(
        f"/api/pytest/tableau/{dashboard}?zone=departement&maille=commune&zone_id=01&id_tableau={dashboard}"
    )
    assert resp.status == 200, "La publication ne fonctionne pas."

    # on vérifie qu'il est possible de dépublier
    _req, resp = client_admin_user.delete(
        f"/api/pytest/tableau/{dashboard}/depublication?zone=departement&zone_id=01"
    )
    assert resp.status == 200, "Impossible de dépublier le tableau."


def test_dashboard_deletion(client_admin_user, dashboard):
    # we try deleting in the wrong region
    _req, resp = client_admin_user.delete(
        f"/api/auvergne-rhone-alpes/tableau/{dashboard}"
    )
    assert resp.status == 400

    _req, resp = client_admin_user.delete(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200


def test_dashboard_lists(client_basic_user, dashboard):
    _req, resp = client_basic_user.get(
        f"/api/pytest/public/dashboards/", params={"zone": "epci"}
    )
    assert resp.status == 200
    content = resp.json
    # we have one published dashboard by admin
    assert len(content) == 1

    _req, resp = client_basic_user.get(
        f"/api/pytest/public/dashboards/", params={"zone": "region"}
    )
    assert resp.status == 200
    content = resp.json
    assert len(content) == 0


def test_update_dashboard(
    client_basic_user, client_admin_user, dashboard, dashboard_fixtures
):
    """Teste si la mise à jour d'un tableau de bord fonctionne convenablement
    Parameters
    ----------
    client_admin_user: dict
        Dictionnaire qui indique les caracteristiques d'une utilisatrice de profil « admin »
    dashboard: entier
        Identifiant du tableau de bord retourné par la fonction fixture « dashboard »
    dashboard_fixtures: dict
        Caractéristiques d'un tableau d'un tableau de bord comprenant un titre, une description et une liste de lignes
    """
    reponse_json = dashboard_fixtures
    _req, resp = client_admin_user.post(
        f"/api/pytest/tableau/{dashboard}", json=reponse_json
    )
    assert resp.status == 200

    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/{dashboard}", json=reponse_json
    )
    assert resp.status == 401


def test_consult_dashboard(
    client_anonymous,
    client_basic_user,
    client_admin_user,
    dashboard,
    dashboard_fixtures,
    mocker,
):
    # we try consulting when it is not possible to share by URL
    async_get_config_mock = AsyncMock(
        return_value=[{"allow_sharing_dashboard_by_url": False}]
    )
    mocker.patch(
        "terriapi.controller.regions_configuration.get_configuration",
        side_effect=async_get_config_mock,
    )
    # we retrieve data from dashboard with any user
    _request, resp = client_anonymous.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 404

    # we retrieve data from dashboard with any user
    _req, resp = client_basic_user.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 404

    # we try again consulting when it is now possible to share by URL
    async_get_config_mock = AsyncMock(
        return_value=[{"allow_sharing_dashboard_by_url": True}]
    )
    mocker.patch(
        "terriapi.controller.regions_configuration.get_configuration",
        side_effect=async_get_config_mock,
    )
    # we retrieve data from dashboard with any user
    _request, resp = client_anonymous.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200

    # we retrieve data from dashboard with any user
    _req, resp = client_basic_user.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200

    # we retrieve data from dashboard
    _req, resp = client_admin_user.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200
    payload = resp.json

    # do we have the right number of categories
    assert len(payload.get("thematiques", [])) == len(
        dashboard_fixtures.get("thematiques", [])
    )

    # we test representations and their data
    for i_cat, category in enumerate(payload.get("thematiques", [])):
        # we check we have the right number of graphs
        category_ref = dashboard_fixtures["thematiques"][i_cat]
        assert len(category.get("graphiques", [])) == len(
            category_ref.get("graphiques", [])
        )

        for graph in category.get("graphiques", []):
            id_analysis = graph["indicateur_id"]
            representation = graph["representation"]

            # we get the graphs results for this analysis
            req, resp_analysis = client_admin_user.post(
                f"/api/pytest/analysis/{id_analysis}/graphique/{representation}",
                params={"zone": "region", "zone_id": "1", "maille": "commune"},
                json={},
            )
            assert resp_analysis.status == 200
            content = resp_analysis.json
            if representation == "pie":
                assert "charts" in content
                assert "unite" in content
                assert "confid" in content
            elif representation == "line":
                assert "analyse" in content
                assert "unite" in content


def test_sharing_dashboard(
    client_admin_user, client_basic_user, dashboard, dashboard_fixtures
):
    # Sharing a dashboard to non-existant users is impossible
    _req, resp = client_admin_user.post(
        f"/api/pytest/tableau/{dashboard}/share",
        json={"emails": "nonexsting@downloadmoreram.com\n"},
    )
    assert resp.status == 400

    # Share a dashboard to another user
    _req, resp = client_admin_user.post(
        f"/api/pytest/tableau/{dashboard}/share",
        json={"emails": "pytest@terristory-nowhere.dev\n"},
    )
    assert resp.status == 200

    # A user can access a shared dashboard, and it is present in the list of dashboards
    _req, resp = client_basic_user.get(f"/api/pytest/my/dashboards")
    assert resp.status == 200
    content = resp.json
    assert dashboard in [x["id"] for x in content], "shared dashboard is not listed"
    _req, resp = client_basic_user.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200, "Shared dashboard cannot be accessed by new user"

    # It is possible to modify a shared dashboard
    reponse_json = dashboard_fixtures
    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/{dashboard}", json=reponse_json
    )
    assert resp.status == 200

    # Other users cannot share a dashboard, even if it's shared to them
    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/{dashboard}/share",
        json={"emails": "alice@terristory-wonder.land\n"},
    )
    assert resp.status == 401

    # Other users cannot delete a dashboard, even if it's shared to them
    _req, resp = client_basic_user.delete(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 400

    # Remove the dashboard from the client_basic_user profile
    _req, resp = client_basic_user.delete(f"/api/pytest/tableau/{dashboard}/share/")
    assert resp.status == 200

    # The dashboard cannot be accessed anymore
    _req, resp = client_basic_user.delete(f"/api/pytest/tableau/{dashboard}/share/")
    assert resp.status == 400
    _req, resp = client_basic_user.get(f"/api/pytest/my/dashboards")
    assert resp.status == 200
    content = resp.json
    assert dashboard not in [x["id"] for x in content]

    # The owner of the dashboard cannot un-share themself
    _req, resp = client_admin_user.delete(f"/api/pytest/tableau/{dashboard}/share/")
    assert resp.status == 400


def test_sharing_readonly_dashboard(
    client_admin_user, client_basic_user, dashboard, dashboard_fixtures
):
    # Share a dashboard to another user
    _req, resp = client_admin_user.post(
        f"/api/pytest/tableau/{dashboard}/share/",
        json={"emails": "pytest@terristory-nowhere.dev\n", "is_readonly_share": True},
    )
    assert resp.status == 200

    # A user can access a shared dashboard, and it is present in the list of dashboards
    _req, resp = client_basic_user.get(f"/api/pytest/my/dashboards")
    assert resp.status == 200
    content = resp.json
    assert dashboard in [x["id"] for x in content], "shared dashboard is not listed"
    _req, resp = client_basic_user.get(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 200, "Shared dashboard cannot be accessed by new user"

    # It is impossible to modify a shared dashboard
    reponse_json = dashboard_fixtures
    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/{dashboard}", json=reponse_json
    )
    assert resp.status == 401

    # But possible to duplicate
    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/dupliquer/{dashboard}", json={"title": "New title"}
    )
    assert resp.status == 200
    # only once
    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/dupliquer/{dashboard}", json={"title": "New title"}
    )
    assert resp.status == 400
    # not existing table
    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/dupliquer/-1", json={"title": "New title"}
    )
    assert resp.status == 404

    # Other users cannot share a dashboard, even if it's shared to them
    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/{dashboard}/share",
        json={"emails": "alice@terristory-wonder.land\n"},
    )
    assert resp.status == 401

    # Other users cannot delete a dashboard, even if it's shared to them
    _req, resp = client_basic_user.delete(f"/api/pytest/tableau/{dashboard}")
    assert resp.status == 400

    # Remove the dashboard from the client_basic_user profile
    _req, resp = client_basic_user.delete(
        f"/api/pytest/tableau/{dashboard}/share/?readonly=true"
    )
    assert resp.status == 200
    _req, resp = client_basic_user.get(f"/api/pytest/my/dashboards")
    assert resp.status == 200
    content = resp.json
    assert dashboard not in [x["id"] for x in content]


@pytest.mark.parametrize(
    "type_object", ["link-launcher", "didactic-file-launcher", "logo"]
)
def test_create_dashboard_without_rights(
    client_basic_user, indicators, client_admin_user, type_object, dashboard_fixtures
):
    """Création d'un nouveau tableau de bord en base de données."""
    _, analyses = indicators

    _fixtures = dashboard_fixtures.copy()
    _fixtures["thematiques"] = [
        {
            "titre_thematique": "Test with " + type_object,
            "description_thematique": "Impossible",
            "graphiques": [
                {
                    "indicateur_id": analyses[0],
                    "representation": type_object,
                }
            ],
        }
    ]
    _req, resp = client_basic_user.post("/api/pytest/tableau", json=_fixtures)
    assert resp.status == 401
    content = resp.json
    assert (
        content.get("message", "Wrong message")
        == "Vous ne pouvez créer un tableau avec des éléments réservés aux administrateurs."
    )
    _req, resp = client_admin_user.post("/api/pytest/tableau", json=_fixtures)
    assert resp.status == 200

    _req, resp = client_basic_user.post("/api/pytest/tableau", json=dashboard_fixtures)
    assert resp.status == 200
    content = resp.json
    assert "tableau_id" in content
    dashboard_id = content["tableau_id"]

    _req, resp = client_basic_user.post(
        f"/api/pytest/tableau/{dashboard_id}", json=_fixtures
    )
    assert resp.status == 401
    content = resp.json
    assert (
        content.get("message", "Wrong message")
        == "Vous ne pouvez créer un tableau avec des éléments réservés aux administrateurs."
    )


def test_create_dashboard_with_wrong_objects(
    indicators, client_admin_user, dashboard_fixtures
):
    """Création d'un nouveau tableau de bord en base de données."""
    _fixtures = {
        "thematiques": [
            {
                "titre_thematique": "Test with ",
                "description_thematique": "Impossible",
                "graphiques": [
                    {
                        "representation": "text-block",
                        "content": "# Introduction\n\nThis is a new paragraph **with bold text**.\n\n&gt; We could also combine with external links: [like here](https://terristory.fr).",
                    },
                ],
            }
        ],
        "titre": "A dashboard with some markdown",
        "description": "**It does work, doesn't it?**\n\nWe hope!",
    }

    _req, resp = client_admin_user.post("/api/pytest/tableau", json=_fixtures)
    assert resp.status == 200
    content = resp.json
    assert "tableau_id" in content
    dashboard_id = content["tableau_id"]

    # we check the dashboard has the right content
    _req, resp = client_admin_user.get("/api/pytest/my/dashboards")
    assert resp.status == 200
    content = resp.json
    dashboard_content = next((x for x in content if x["id"] == dashboard_id), {})
    assert dashboard_content.get("description", "") == _fixtures["description"]

    _fixtures = {
        "thematiques": [
            {
                "titre_thematique": "Test with ",
                "description_thematique": "Impossible",
                "graphiques": [
                    {
                        "representation": "text-block",
                        "content": '# Introduction\n\nThis is an ![image](https://terristory.fr/img/logo_aura_ee.png).\n\n&gt; This is also an image <img src="https://terristory.fr/img/logo_aura_ee.png" />.',
                    }
                ],
            }
        ],
        "titre": "A dashboard with some markdown and pictures!",
        "description": 'This contains an ![image](https://terristory.fr/img/logo_aura_ee.png) and another picture here : <img src="https://terristory.fr/img/logo_aura_ee.png" />',
    }

    _req, resp = client_admin_user.post("/api/pytest/tableau", json=_fixtures)
    assert resp.status == 200
    content = resp.json
    assert "tableau_id" in content
    dashboard_id = content["tableau_id"]

    # we check the dashboard has the right content
    _req, resp = client_admin_user.get("/api/pytest/my/dashboards")
    assert resp.status == 200
    content = resp.json
    dashboard_content = next((x for x in content if x["id"] == dashboard_id), {})

    # we check pictures got sanitized
    assert (
        dashboard_content.get("description", "")
        == "This contains an ![image](https://terristory.fr/img/logo_aura_ee.png) and another picture here : "
    )
    assert (
        dashboard_content.get("thematiques", [{}])[0]
        .get("graphiques", [{}])[0]
        .get("content", "")
        == "# Introduction\n\nThis is an ![image](https://terristory.fr/img/logo_aura_ee.png).\n\n&gt; This is also an image ."
    )
