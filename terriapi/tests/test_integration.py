﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

"""Test pour l'intégration et validation de la données
"""

from pathlib import Path

import pytest
import yaml

from terriapi import exceptions
from terriapi.controller import fetch
from terriapi.integration import (
    create_indicator,
    create_indicator_data_table,
    update_data,
    validation,
)

_here = Path(__file__).absolute().parent
DATADIR = _here / "data"
# proche des données conso énergétique
SAMPLE = DATADIR / "integration_sample.csv"
MISSING_HEADER_SAMPLE = DATADIR / "integration_missing_required_header_sample.csv"
WRONG_HEADER_SAMPLE = DATADIR / "integration_wrong_header_sample.csv"
MISSING_FIELD_SAMPLE = DATADIR / "integration_missing_field_sample.csv"
META = DATADIR / "integration_meta.yaml"
SCHEMA = "pytest"


def test_normalize():
    """Fonction de normalisation pour un nom de colonne ou un nom de table."""
    # pas d'accent
    assert validation.normalize("année") == "annee"
    # pas de majuscule
    assert validation.normalize("Année") == "annee"
    # pas de '-'
    assert validation.normalize("prod-ENR") == "prod_enr"
    # pas d'espace
    assert validation.normalize("valeur absolue") == "valeur_absolue"


def test_lecture_en_tete():
    """On teste le retour de la fonction qui retourne juste les en-têtes d'un fichier CSV."""
    headers = validation.lecture_headers(SAMPLE)
    expected = {
        "commune",
        "annee",
        "secteur",
        "energie",
        "usage",
        "other_cat",
        "valeur",
    }
    assert set(headers) == expected


def test_verifie_headers():
    """Vérification de l'en-tête d'un fichier CSV."""
    is_valid = validation.verifie_headers(SAMPLE, ("commune", "annee", "valeur"))
    assert is_valid
    assert is_valid.message == "OK"


def test_verifie_headers_champs_requis():
    """On vérifie qu'il y a les champs requis dans l'en-tête d'un fichier CSV."""
    is_valid = validation.verifie_headers(
        MISSING_HEADER_SAMPLE, ("commune", "annee", "valeur")
    )
    assert not is_valid
    expected = f"Le champ 'annee' doit être contenu dans l'en-tête du fichier CSV {MISSING_HEADER_SAMPLE}"
    assert is_valid.message == expected


def test_verifie_table_colonne_table_inexistante(indicators, loop):
    """On vérifie que le nom de la table à mettre à jour existe."""
    is_valid = loop.run_until_complete(
        validation.verifie_table_colonne(SCHEMA, "wrong_table", SAMPLE)
    )
    assert not is_valid
    expected = f"La table 'wrong_table' du schéma '{SCHEMA}' n'existe pas"
    assert is_valid.message == expected


def test_verifie_table_colonne_mauvais_nom_header(indicators, loop):
    """On vérifie que les noms des colonnes de la table sont identiques aux en-têtes du fichiers CSV."""
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    is_valid = loop.run_until_complete(
        validation.verifie_table_colonne(SCHEMA, analysis_name, WRONG_HEADER_SAMPLE)
    )
    assert not is_valid
    colonnes = [
        "commune",
        "annee",
        "energie",
        "secteur",
        "other_cat",
        "usage",
        "valeur",
    ]
    headers = validation.lecture_headers(WRONG_HEADER_SAMPLE)
    expected = f"En-tête et colonnes sont différents. Table {set(colonnes)} - fichier {set(headers)}"
    assert is_valid.message == expected


def test_verifie_table_colonne_manque_un_champs(indicators, loop):
    """On vérifie qu'il y a bien le même nombre de champs entre le fichier CSV et la table."""
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    is_valid = loop.run_until_complete(
        validation.verifie_table_colonne(SCHEMA, analysis_name, MISSING_FIELD_SAMPLE)
    )
    assert not is_valid
    expected = "En-tête et colonnes doivent avoir le même nombre d'éléments. table (7) - fichier (6)"
    assert is_valid.message == expected


def test_copy_table_indicator(indicators, loop):
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    loop.run_until_complete(
        update_data(
            schema=SCHEMA,
            table=analysis_name,
            fpath=SAMPLE,
            colonnes_obligatoires=("commune", "annee", "valeur"),
        )
    )
    rset = loop.run_until_complete(
        fetch(f"select count(*) from {SCHEMA}.{analysis_name}")
    )
    assert rset[0]["count"] == 52


def test_copy_table_indicator_but_different_columns(indicators, loop):
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    with pytest.raises(
        exceptions.ForceUpdateAllowedException,
    ):
        loop.run_until_complete(
            update_data(
                schema=SCHEMA,
                table=analysis_name,
                fpath=WRONG_HEADER_SAMPLE,
                colonnes_obligatoires=("commune", "annee", "valeur"),
            )
        )


def test_copy_table_indicator_but_different_columns_forced(indicators, loop):
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    loop.run_until_complete(
        update_data(
            schema=SCHEMA,
            table=analysis_name,
            fpath=WRONG_HEADER_SAMPLE,
            colonnes_obligatoires=("commune", "annee", "valeur"),
            is_national=False,
            force_update=True,
        )
    )
    rset = loop.run_until_complete(
        fetch(f"select count(*) from {SCHEMA}.{analysis_name}")
    )
    assert rset[0]["count"] == 9


def test_existing_table(indicators, loop):
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    is_valid = loop.run_until_complete(
        validation.existence_table(SCHEMA, analysis_name)
    )
    assert not is_valid
    assert (
        is_valid.message == f"La table {analysis_name} du schéma {SCHEMA} existe déjà"
    )


def test_existing_category(indicators, loop):
    categories_valides = ["secteur", "usage", "energie"]
    is_valid = loop.run_until_complete(
        validation.existence_categorie(SCHEMA, categories_valides)
    )
    assert is_valid
    categories_valides = ["jazz", "blues"]
    is_valid = loop.run_until_complete(
        validation.existence_categorie(SCHEMA, categories_valides)
    )
    assert not is_valid
    assert (
        is_valid.message
        == f"La catégorie 'jazz' n'est pas présente pour la région {SCHEMA}"
    )


def test_create_existing_table_fails(indicators, loop):
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    categories = ["secteur", "usage", "energie"]
    with pytest.raises(
        exceptions.ValidationError,
        match=f"La table {analysis_name} du schéma pytest existe déjà",
    ):
        loop.run_until_complete(
            create_indicator_data_table(
                SCHEMA, analysis_name, categories, True, False, True
            )
        )
    res = loop.run_until_complete(
        fetch(
            f"""SELECT EXISTS (
            SELECT FROM information_schema.tables 
            WHERE table_schema = $1
            AND table_name = $2
        )""",
            SCHEMA,
            analysis_name,
        )
    )
    assert res[0]["exists"]


def test_create_existing_table_but_forced(indicators, loop):
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    categories = ["secteur", "usage", "energie"]
    loop.run_until_complete(
        create_indicator_data_table(
            SCHEMA, analysis_name, categories, True, False, True, forced=True
        )
    )
    res = loop.run_until_complete(
        fetch(
            f"""SELECT EXISTS (
            SELECT FROM information_schema.tables 
            WHERE table_schema = $1
            AND table_name = $2
        )""",
            SCHEMA,
            analysis_name,
        )
    )
    assert res[0]["exists"]


def test_create_indicator_with_different_categories_than_metadata(indicators, loop):
    analyses_names, _ = indicators
    analysis_name = analyses_names[0]
    metadata = yaml.load(open(META), Loader=yaml.SafeLoader)
    # on change une catégorie
    expected = "Les catégories dans les headers ne sont pas cohérentes avec les catégories dans les métadonnées (remplies depuis cet écran ou l'écran de gestion des données)"
    metadata["categories"] = [
        {"category": "secteur", "title": "Par secteur", "type": "pie"},
        {"category": "wrong_categorie", "title": "", "type": "pie"},
    ]
    with pytest.raises(exceptions.ValidationError) as e:
        loop.run_until_complete(
            create_indicator(SCHEMA, SCHEMA, analysis_name, metadata, SAMPLE, "")
        )
    assert str(e.value) == expected


# TODO rajouter un test pour vérifier la mise à jour des metadonnées d'un indicateur
