# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


def test_page_parameters(
    region, indicators, client_anonymous, analysis_page_parameters
):
    """
    Test exporting a common dataset table from API entrypoint. Call the API without rights
    and then with the admin rights but with a wrong table name. Will eventually
    call the API with admin rights and correct table name, thus getting a csv
    file.

    Parameters
    ----------
    region : str
        region fixtures
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    """
    # we finally check an invalid region triggers an error
    _request, resp = client_anonymous.get(
        f"/api/invalid_region/analysis/page/parameters/suivi_energetique"
    )
    assert resp.status == 401

    # we finally check an invalid page gives nothing
    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/page/parameters/no_page_valid"
    )
    assert resp.status == 404

    # we finally check the real graph types
    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/page/parameters/suivi_energetique"
    )
    assert resp.status == 200
    data = resp.json

    assert len(data.get("parameters", [])) > 0
    assert data["parameters"]["fictive_chart_enabled"]
    assert not data["parameters"]["other_fictive_chart_disabled"]


def test_pages_enabled(region, indicators, client_anonymous, analysis_page_parameters):
    """
    Test exporting a common dataset table from API entrypoint. Call the API without rights
    and then with the admin rights but with a wrong table name. Will eventually
    call the API with admin rights and correct table name, thus getting a csv
    file.

    Parameters
    ----------
    region : str
        region fixtures
    indicateur : str
        table name created inside pytest schema
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    # we check right page was retrieved from API
    _request, resp = client_anonymous.get(f"/api/pytest/analysis/pages/enabled/")
    assert resp.status == 200
    data = resp.json
    assert len(data.get("pages", [])) == 1
    assert "suivi_energetique" in data["pages"]
