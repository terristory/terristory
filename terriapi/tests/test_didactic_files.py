# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
from pathlib import Path

here = Path(__file__).resolve().parent


def test_data_source_logo(
    region, indicators, client_anonymous, client_admin_user, didactic_file_logo_sources
):
    logo = here / "data" / "logo.png"
    data = [
        {
            "id": 0,
            "name": "logo.png",
            "extension": "png",
            "url": "wwww.test.fr/epci/200070407",
            "region": "pytest",
            "logo_path": "/test/logo.png",
            "enabled_zone": "epci,region",
            "default_url": "wwww.test.fr",
        }
    ]

    id = data[0]["id"]
    # awith an anonymous user, possible to display
    _request, resp = client_anonymous.get("/api/pytest/logo_sources/list")
    assert resp.status == 200

    # not reachable to anonymous users
    _request, resp = client_anonymous.put(
        "/api/pytest/logo_sources/add", files={"sources": json.dumps(data)}
    )
    assert resp.status == 401

    # Try to get details about a non existing logo
    _request, resp = client_anonymous.get(f"/api/pytest/{id}/logo_sources")
    assert resp.status == 404

    # Try to delete a logo without the correct rights
    _request, resp = client_anonymous.delete(f"/api/pytest/{id}/logo_sources")
    assert resp.status == 401

    # Try to update a logo without the correct rights
    _request, resp = client_anonymous.put(
        f"/api/pytest/{id}/logo_sources/update", files={"sources": json.dumps(data)}
    )
    assert resp.status == 401

    # with an admin user, possible to display
    _req, resp = client_admin_user.get("/api/pytest/logo_sources/list")
    assert resp.status == 200

    # with an admin user, possible to create
    with open(logo, "rb") as logo_img:
        _req, resp = client_admin_user.put(
            "/api/pytest/logo_sources/add",
            data={"sources": json.dumps(data)},
            files={"file" + str(id): logo_img},
        )
        assert resp.status == 200
        result = resp.json
        assert len(result) > 0

    # get data from database
    _req, resp = client_admin_user.get("/api/pytest/logo_sources/list")
    list_result = resp.json

    token = next(f for f in list_result if f["name"] == data[0]["name"])
    # check if data has been added correctly to database
    assert (token["name"], token["url"]) == (
        data[0]["name"],
        data[0]["url"],
    )

    id = token["id"]
    # try to get details about a logo
    _request, resp = client_anonymous.get(f"/api/pytest/{id}/logo_sources")
    assert resp.status == 200
    details_logo = resp.json

    # check if data has been correctly retrieved from database
    assert (details_logo["name"], details_logo["url"]) == (
        data[0]["name"],
        data[0]["url"],
    )

    # try to update a logo with the correct rights
    data = [
        {
            "name": "logo.png",
            "extension": "png",
            "url": "wwww.blob.fr",
            "region": "pytest",
            "logo_path": "/test/logo.png",
            "enabled_zone": "",
        }
    ]
    _req, resp = client_admin_user.put(
        f"/api/pytest/{id}/logo_sources/update", data={"sources": json.dumps(data)}
    )
    assert resp.status == 200
    content = resp.json
    assert content["message"] == "Logo modifié"

    # try to get details about a logo with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/{id}/logo_sources")
    assert resp.status == 200
    details_logo = resp.json

    # check if data has been correctly updated and retrieved from database
    assert (details_logo["name"], details_logo["url"]) == (
        data[0]["name"],
        data[0]["url"],
    )

    # try to delete a wrong logo with the correct rights
    _req, resp = client_admin_user.delete(f"/api/pytest/-1/logo_sources")
    assert resp.status == 404

    # try to delete a logo with the correct rights
    _req, resp = client_admin_user.delete(f"/api/pytest/{id}/logo_sources")
    assert resp.status == 200

    # try to get details about a logo with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/{id}/logo_sources")
    assert resp.status == 404


def test_actions_list_didactic_files(
    region, indicators, client_anonymous, client_admin_user, didactic_file_actions
):
    # try to get list of actions types without the correct rights
    _request, resp = client_anonymous.get(f"/api/pytest/actions/list")
    assert resp.status == 401

    # try to get list of actions types with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/actions/list")
    assert resp.status == 200
    actions = resp.json
    assert len(actions) == 1
    assert actions[0].get("type", None) == didactic_file_actions


def test_didactic_files_list(
    region, indicators, client_anonymous, client_admin_user, didactic_file_list
):
    # try to get list of didactic files without the correct rights
    _request, resp = client_anonymous.get(f"/api/pytest/didactic_file/list")
    assert resp.status == 401

    # try to get list of didactic files with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/didactic_file/list")
    assert resp.status == 200
    actions = resp.json
    assert len(actions) == len(didactic_file_list)


def test_didactic_file_details(
    region, indicators, client_anonymous, client_admin_user, didactic_file_list
):
    didactic_file = didactic_file_list[0]
    id_example = didactic_file["id"]
    # try to get details of a didactic file with anonymous user
    _request, resp = client_anonymous.get(f"/api/pytest/{id_example}/didactic_file")
    assert resp.status == 200

    # try to get details of a didactic file with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/{id_example}/didactic_file")
    assert resp.status == 200
    res_didactic_file = resp.json

    # we check metadata
    assert (
        res_didactic_file["metadata"]["fileTitle"] == didactic_file["metadata"]["title"]
    )
    assert (
        res_didactic_file["metadata"]["description"]
        == didactic_file["metadata"]["description"]
    )

    categories = list(res_didactic_file["data"].keys())
    # we sort by key as they are supposed to be equal to order
    categories.sort()
    # we check that the categories are in the right order
    for cat1Id, cat2Id in zip(categories[:-1], categories[1:]):
        cat1 = res_didactic_file["data"][cat1Id]
        cat2 = res_didactic_file["data"][cat2Id]
        assert cat1["categoryOrder"] < cat2["categoryOrder"]

    # TODO: test order for sub categories?


def test_didactic_file_deletion(
    region, indicators, client_anonymous, client_admin_user, didactic_file_list
):
    didactic_file = didactic_file_list[0]
    id_example = didactic_file["id"]
    # try to get a didactic file with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/{id_example}/didactic_file")
    assert resp.status == 200

    # try to delete a didactic file without the correct rights
    _request, resp = client_anonymous.delete(f"/api/pytest/{id_example}/didactic_file")
    assert resp.status == 401

    # try to delete a didactic file with wrong ID
    _req, resp = client_admin_user.delete(f"/api/pytest/-1/didactic_file")
    assert resp.status == 400

    # try to delete a didactic file with the correct rights
    _req, resp = client_admin_user.delete(f"/api/pytest/{id_example}/didactic_file")
    assert resp.status == 200

    # try to get a didactic file with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/{id_example}/didactic_file")
    assert resp.status == 404


def test_didactic_file_add_and_update(
    region, indicators, client_anonymous, client_admin_user, didactic_file_list
):
    new_file = {
        "data": {
            "0": {
                "categoryTitle": "First category",
                "id": 0,
                "subCategory": {
                    "0": {
                        "subCategoryTitle": "First sub sub",
                        "typeAnalysis": "analysis",
                        "id": 1,
                        "section": {
                            "0": {
                                "id": 1,
                                "sectionTitle": "First section",
                                "type": "analysis",
                                "sectionComment": "Comment first section first sub category",
                                "analysis": {},
                            }
                        },
                    },
                    "1": {
                        "subCategoryTitle": "Second sub sub",
                        "typeAnalysis": "analysis",
                        "id": 2,
                        "section": {
                            "0": {
                                "id": 1,
                                "sectionTitle": "First section",
                                "type": "analysis",
                                "sectionComment": "Comment first section second sub category",
                                "analysis": {},
                            },
                        },
                    },
                },
            },
            "1": {"categoryTitle": "Second category", "id": 3, "subCategory": {}},
        },
        "metadata": {
            "fileTitle": "Test de nouvelle fiche didactique",
            "description": "Ceci est un test",
        },
    }
    # try to get a didactic file without the correct rights
    _request, resp = client_anonymous.put(
        f"/api/pytest/didactic_file/add", data={"didacticFile": json.dumps(new_file)}
    )
    assert resp.status == 401

    # try to add a new file without parameters
    _req, resp = client_admin_user.put(f"/api/pytest/didactic_file/add")
    assert resp.status == 400

    # try to add a new file with correct rights
    _req, resp = client_admin_user.put(
        f"/api/pytest/didactic_file/add", data={"didacticFile": json.dumps(new_file)}
    )
    assert resp.status == 200
    res_didactic_file = resp.json
    assert res_didactic_file["message"] == "Fiche enregistrée"

    # try to get list of didactic files with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/didactic_file/list")
    assert resp.status == 200
    files = resp.json
    token = next(f for f in files if f["title"] == new_file["metadata"]["fileTitle"])
    assert token["description"] == new_file["metadata"]["description"]

    # Editing the file
    id_new_file = token["id"]
    new_file["metadata"]["fileTitle"] = "Modification du test de fiche didactique"
    new_file["data"]["1"], new_file["data"]["0"] = (
        new_file["data"]["0"],
        new_file["data"]["1"],
    )

    # try to add a new file with correct rights
    _req, resp = client_admin_user.put(
        f"/api/pytest/{id_new_file}/didactic_file/update",
        data={"didacticFile": json.dumps(new_file)},
    )
    assert resp.status == 200
    res_didactic_file = resp.json
    assert res_didactic_file["message"] == f"Fiche modifiée"

    # try to get list of didactic files with the correct rights
    _req, resp = client_admin_user.get(f"/api/pytest/didactic_file/list")
    assert resp.status == 200
    files = resp.json
    token = next(f for f in files if f["title"] == new_file["metadata"]["fileTitle"])
    assert token["description"] == new_file["metadata"]["description"]
