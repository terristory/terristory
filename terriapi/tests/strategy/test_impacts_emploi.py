﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import csv
from copy import deepcopy
from json import dumps as jdumps
from pathlib import Path

import pytest

from terriapi.controller import fetch
from terriapi.controller.strategy_actions.loaders.actions_data import (
    get_user_action_params,
)

here = Path(__file__).resolve().parent


@pytest.fixture
def example_impacts_parameters(loop):
    """
    Get impacts parameters example variables
    """
    payload = {}
    payload["trajectoires_cibles"] = []
    actions = loop.run_until_complete(get_user_action_params("auvergne-rhone-alpes"))
    with (here / ".." / "data" / "impact_variables_utilisateurs.csv").open() as lines:
        next(lines)
        for line in lines:
            cols = [field.strip() for field in line.split(";")]
            payload[cols[0]] = {
                "2025": float(cols[3].replace(",", ".")),
                "2026": float(cols[3].replace(",", ".")),
            }

        all_actions = ["1", "2", "3", "3b", "4", "5", "6", "6b", "7"]

        payload["actions"] = {
            "2025": all_actions,
            # ajout d'une année supplémentaire pour pouvoir tester les impacts pérennes
            "2026": all_actions,
        }
        # pas de part locale pour ce payload
        payload["economique"] = {}
        payload["advanced"] = {}
        payload["yearAdvancedParams"] = {}
        # TODO: to fix here?
        for act in all_actions:
            # on ajoute l'entrée pour les paramètres non économiques
            # mais on les laisse vide
            payload["advanced"][act] = {key: {} for key in actions.get(act, {})}
    return payload


@pytest.fixture
async def job_impact_payload1(example_impacts_parameters):
    """
    Get local job impact ratio
    """
    # copie du payload pour la part locale
    payload_locale = deepcopy(example_impacts_parameters)

    with (here / ".." / "data" / "impact_part_locale.csv").open() as lines:
        next(lines)
        for line in lines:
            cols = [field.strip() for field in line.split(";")]
            payload_locale["economique"][cols[0]] = float(
                cols[5].replace("%", "").replace(",", ".")
            )
    return payload_locale


@pytest.fixture
async def job_impact_payload2(example_impacts_parameters):
    """
    Get local job impact ratio
    """
    # copie du payload pour la part locale
    payload_locale = deepcopy(example_impacts_parameters)

    with (here / ".." / "data" / "impact_part_locale.csv").open() as lines:
        next(lines)
        for line in lines:
            cols = [field.strip() for field in line.split(";")]
            payload_locale["economique"][cols[0]] = float(
                cols[5].replace("%", "").replace(",", ".")
            )
    return payload_locale


@pytest.fixture
async def job_impact_payload3(example_impacts_parameters):
    """
    Get local job impact ratio
    """
    # copie du payload pour la part locale
    payload_locale = deepcopy(example_impacts_parameters)

    with (here / ".." / "data" / "impact_part_locale.csv").open() as lines:
        next(lines)
        for line in lines:
            cols = [field.strip() for field in line.split(";")]
            payload_locale["economique"][cols[0]] = float(
                cols[5].replace("%", "").replace(",", ".")
            )
    return payload_locale


# résultats attendus d'impact emploi avec la part territoire calculée par défaut
with (here / ".." / "data" / "impact_emploi_part_territoire.csv").open() as lines:
    next(lines)
    reader = csv.DictReader(
        lines,
        delimiter=";",
        fieldnames=(
            "type_territoire",
            "nom_territoire",
            "type_impact",
            "temps_impact",
            "va_totale",
            "nb_emploi",
        ),
    )
    impact_emploi_terr = [row for row in reader]

# résultats attendus d'impact emploi avec la part locale donnée par l'utilisateur
with (here / ".." / "data" / "impact_emploi_part_locale.csv").open() as lines:
    next(lines)
    reader = csv.DictReader(
        lines,
        delimiter=";",
        fieldnames=(
            "type_territoire",
            "nom_territoire",
            "type_impact",
            "temps_impact",
            "va_totale1",
            "nb_emploi1",
            "va_totale2",
            "nb_emploi2",
            "va_totale3",
            "nb_emploi3",
        ),
    )
    impact_emploi_locale = [row for row in reader]


@pytest.fixture
async def epci(client_anonymous):
    """
    retourne l'identifiant d'epci de l'agglo Roannaise)
    """
    epci = await fetch(
        "select code from auvergne_rhone_alpes.epci where code = $1", "200035731"
    )
    assert epci[0]
    return epci[0]["code"]


def test_status(region, client_anonymous, epci, example_impacts_parameters):
    """Une requête d'impact emploi doit renvoyer une réponse avec
    un contenu en json non vide
    """
    uri = "/api/auvergne-rhone-alpes/actions?zone=epci&zone_id={}&type_action=test&id_utilisateur=-1"
    _request, resp = client_anonymous.post(
        uri.format(epci), data=jdumps(example_impacts_parameters)
    )
    assert resp.status == 200
    json = resp.json
    assert len(json) > 0


def test_epci_roannais(region, client_anonymous, epci, example_impacts_parameters):
    uri = "/api/auvergne-rhone-alpes/actions?zone=epci&zone_id={}&id_utilisateur=-1"
    _request, resp = client_anonymous.post(
        uri.format(epci), data=jdumps(example_impacts_parameters)
    )
    assert resp.status == 200
    json = resp.json
    assert "impact_emplois" in json
    assert "2025" in json["impact_emplois"]
    assert "direct" in json["impact_emplois"]["2025"]
    assert "region" in json["impact_emplois"]["2025"]["direct"]
    assert "ponctuel" in json["impact_emplois"]["2025"]["direct"]["region"]
    assert "va_totale" in json["impact_emplois"]["2025"]["direct"]["region"]["ponctuel"]
    assert "2026" in json["impact_emplois"]
    assert "direct" in json["impact_emplois"]["2026"]
    assert "region" in json["impact_emplois"]["2026"]["direct"]
    assert "ponctuel" in json["impact_emplois"]["2026"]["direct"]["region"]
    assert "va_totale" in json["impact_emplois"]["2026"]["direct"]["region"]["ponctuel"]


@pytest.mark.skip
@pytest.mark.parametrize(
    "tv",
    impact_emploi_terr,
    ids=[
        "{}-{}-{}".format(
            elem["nom_territoire"], elem["type_impact"], elem["temps_impact"]
        )
        for elem in impact_emploi_terr
    ],
)
def test_job_impact_part_territoire(
    region, client_anonymous, epci, tv, example_impacts_parameters, loop
):
    """Teste les impacts emplois de quelques territoires
    avec la part territoire par défaut
    """
    terr = loop.run_until_complete(
        fetch(
            "select code from auvergne_rhone_alpes.{} where nom = $1".format(
                tv["type_territoire"]
            ),
            tv["nom_territoire"],
        )
    )
    terr_id = terr[0]["code"]
    uri = (
        "/api/auvergne-rhone-alpes/actions?zone={}&zone_id={}&id_utilisateur=-1".format(
            tv["type_territoire"], terr_id
        )
    )
    _request, resp = client_anonymous.post(
        uri.format(epci), data=jdumps(example_impacts_parameters)
    )
    json = resp.json
    json = json["impact_emplois"]["2026"]
    assert json[tv["type_impact"]][tv["type_territoire"]][tv["temps_impact"]][
        "va_totale"
    ] == pytest.approx(float(tv["va_totale"]), abs=1e-2)
    assert json[tv["type_impact"]][tv["type_territoire"]][tv["temps_impact"]][
        "nb_emploi_total"
    ] == pytest.approx(int(tv["nb_emploi"]))


@pytest.mark.skip
@pytest.mark.parametrize(
    "tv",
    impact_emploi_locale,
    ids=[
        "{}-{}-{}".format(
            elem["nom_territoire"], elem["type_impact"], elem["temps_impact"]
        )
        for elem in impact_emploi_locale
    ],
)
def test_job_impact_part_locale(
    client_anonymous,
    epci,
    tv,
    job_impact_payload1,
    job_impact_payload2,
    job_impact_payload3,
    loop,
):
    """Teste les impacts emplois de quelques territoires
    avec la part locale définie dans le fichier impact_part_locale.csv

    FIXME: Test qui ne passe pas :
    region;Auvergne-Rhône-Alpes;indirect;perenne;0.305;4;0.195;3;0.183;2
    0.199 != 0.195
    """
    terr = loop.run_until_complete(
        fetch(
            "select code from auvergne_rhone_alpes.{} where nom = $1".format(
                tv["type_territoire"]
            ),
            tv["nom_territoire"],
        )
    )
    terr_id = terr[0]["code"]
    for payload, it in (
        (job_impact_payload1, 1),
        (job_impact_payload2, 2),
        (job_impact_payload3, 3),
    ):
        uri = "/api/auvergne-rhone-alpes/actions?zone={}&zone_id={}".format(
            tv["type_territoire"], terr_id
        )
        _request, resp = client_anonymous.post(uri.format(epci), data=jdumps(payload))
        json = resp.json
        json = json["2026"]
        assert json[tv["type_impact"]][tv["type_territoire"]][tv["temps_impact"]][
            "va_totale"
        ] == pytest.approx(float(tv["va_totale{}".format(it)]), abs=1e-2)
        assert json[tv["type_impact"]][tv["type_territoire"]][tv["temps_impact"]][
            "nb_emploi_total"
        ] == pytest.approx(int(tv["nb_emploi{}".format(it)]))


def test_status_error(client_anonymous, epci):
    uri = "/api/auvergne-rhone-alpes/actions?zone=epci&zone_id={}&id_utilisateur=-1".format(
        epci
    )
    _request, resp = client_anonymous.post(uri)
    assert resp.status == 404
