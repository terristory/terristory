﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import os
import shutil
import tempfile

import pandas as pd
import pytest
from openpyxl import load_workbook

import terriapi.controller.regions_configuration as regions_configuration
from terriapi.controller import actions as controller_actions
from terriapi.controller.actions import (
    diagnostic_polluants_atmospheriques,
    export_excel,
)
from terriapi.controller.strategy_actions import RenewableProd, Sector
from terriapi.controller.suivi_trajectoire import _graphe_format

# id de la région
REGION = "auvergne-rhone-alpes"

zone = "departement"
zone_id = "69D"  # Rhône

years = [str(i) for i in range(2019, 2022)]

user_params = {
    "1_nb_logements": {
        "2021": "50",
        "2022": "50",
        "2023": "40",
        "2024": "30",
        "2025": "0",
        "2026": "0",
        "2027": "0",
        "2028": "0",
        "2029": "0",
        "2030": "0",
        "2031": "0",
        "2032": "0",
        "2032": "0",
        "2033": "0",
        "2034": "0",
        "2035": "0",
        "2036": "0",
        "2037": "0",
        "2038": "0",
        "2039": "0",
        "2040": "0",
        "2041": "0",
        "2042": "30",
        "2043": "0",
        "2044": "0",
        "2045": "0",
        "2046": "0",
        "2047": "0",
        "2048": "0",
        "2049": "0",
        "2050": "0",
    },
    "1_surface_moy_logement": {
        "2021": "60",
        "2022": "60",
        "2023": "75",
        "2024": "75",
        "2025": "0",
        "2026": "0",
        "2027": "0",
        "2028": "0",
        "2029": "0",
        "2030": "0",
        "2031": "0",
        "2032": "0",
        "2032": "0",
        "2033": "0",
        "2034": "0",
        "2035": "0",
        "2036": "0",
        "2037": "0",
        "2038": "0",
        "2039": "0",
        "2040": "0",
        "2041": "0",
        "2042": "30",
        "2043": "0",
        "2044": "0",
        "2045": "0",
        "2046": "0",
        "2047": "0",
        "2048": "0",
        "2049": "0",
        "2050": "0",
    },
    "12_puiss_installee": {
        "2021": "0",
        "2022": "0",
        "2023": "0",
        "2024": "50",
        "2025": "0",
        "2026": "0",
        "2027": "0",
        "2028": "0",
        "2029": "0",
        "2030": "0",
        "2031": "0",
        "2032": "0",
        "2032": "0",
        "2033": "0",
        "2034": "0",
        "2035": "0",
        "2036": "0",
        "2037": "0",
        "2038": "0",
        "2039": "0",
        "2040": "0",
        "2041": "0",
        "2042": "0",
        "2043": "20",
        "2044": "0",
        "2045": "0",
        "2046": "0",
        "2047": "0",
        "2048": "0",
        "2049": "0",
        "2050": "0",
    },
}

action_years = {
    "2018": ["1", "12"],
    "2019": ["1", "12"],
    "2020": ["1", "12"],
    "2021": ["1", "12"],
    "2022": ["1", "12"],
    "2023": ["1", "12"],
    "2024": ["1", "12"],
    "2025": ["1", "12"],
    "2026": ["1", "12"],
    "2027": ["1", "12"],
    "2028": ["1", "12"],
    "2029": ["1", "12"],
    "2030": ["1", "12"],
    "2031": ["1", "12"],
    "2032": ["1", "12"],
    "2033": ["1", "12"],
    "2034": ["1", "12"],
    "2035": ["1", "12"],
    "2036": ["1", "12"],
    "2037": ["1", "12"],
    "2038": ["1", "12"],
    "2039": ["1", "12"],
    "2040": ["1", "12"],
    "2041": ["1", "12"],
    "2042": ["1", "12"],
    "2043": ["1", "12"],
    "2044": ["1", "12"],
    "2045": ["1", "12"],
    "2046": ["1", "12"],
    "2047": ["1", "12"],
    "2048": ["1", "12"],
    "2049": ["1", "12"],
    "2050": ["1", "12"],
}

advanced_params = {
    "1": {
        "Répartition des rénovations par niveau de performance énergétique": {
            "1": 20,
            "2": 20,
            "3": 30,
            "4": 30,
        },
        "Gains attribués au chauffage": {"1": 90},
    }
}


def dataframe_to_graphe_format(impact):
    """
    XXX(dag): en attendant de faire le refactoring de https://gitlab.com/terristory/terristory/-/issues/901
    """
    # TODO(dag): refactoring de la fonction d'export pour utiliser directement les DataFrame retournés par la fonction 'impact_total'
    # On parcourt chaque dictionnaire pour passer les DataFrame au format de graphe
    # attendu par l'application web, identique aux structures de données pour la
    # fonction d'export Excel.
    for type_resultat in ["energie_economisee", "emission_ges"]:
        for type_graphe in ("secteur", "energie"):
            data = impact[type_resultat][type_graphe]
            graphe = _graphe_format(data)
            impact[type_resultat][type_graphe] = graphe
    # Même chose pour la prod EnR (il n'y a que les types d'énergie, pas les secteurs)
    data = impact["energie_produite"]["energie"]
    graphe = _graphe_format(data)
    impact["energie_produite"]["energie"] = graphe
    return impact


@pytest.mark.parametrize(
    "current_region,current_zone,current_zone_id",
    [
        ("auvergne-rhone-alpes", "departement", "69D"),
        ("occitanie", "epci", "246600423"),
        ("paysdelaloire", "epci", "200083392"),
        ("bretagne", "epci", "200069391"),
    ],
)
@pytest.mark.export_actions
def test_export_actions(
    region, client_anonymous, current_region, current_zone, current_zone_id, loop
):
    """Calcul d'impact total et test d'export Excel"""
    region_config = loop.run_until_complete(
        regions_configuration.get_configuration(current_region)
    )
    ges_export_excel = region_config[0]["ges_export_excel"]
    ui_show_plan_actions = region_config[0]["ui_show_plan_actions"]
    if not ui_show_plan_actions:
        pytest.skip("Strategy module disabled in this region.")
    dispatcher = loop.run_until_complete(
        controller_actions.compute_total_impact(
            current_region,
            current_zone,
            current_zone_id,
            years,
            action_years,
            user_params,
            advanced_params,
            {},
        )
    )
    # we execute
    outputs_by_action = loop.run_until_complete(dispatcher.execute_all())
    results = loop.run_until_complete(
        dispatcher.merge_outputs(outputs_by_action, remove_passage_data=False)
    )

    polluants = loop.run_until_complete(
        diagnostic_polluants_atmospheriques(
            current_region, current_zone, current_zone_id
        )
    )
    filename = "/tmp/test-export-pcaet-ademe.xlsx"
    with tempfile.NamedTemporaryFile(mode="w+b") as fp:
        output_file = loop.run_until_complete(
            export_excel(current_region, results, polluants, fp.name, ges_export_excel)
        )
        shutil.copy(output_file, filename)

        # Test de la présence de valeur dans les bonnes cases du fichier
        wb = load_workbook(filename)
        sheet_ranges = wb["1.GES et Conso énergie"]

        emission_ges = results["emission_ges"]["secteur"]
        energie_economisee = results["energie_economisee"]["secteur"]
        max_year_energie_economisee = results["energie_economisee"]["max_annee"]
        energie_produite = results["energie_produite"]["energie"]
        max_year_energie_produite = results["energie_produite"]["max_annee"]

        # consumption
        ref_value_resid_conso = energie_economisee.loc[
            (energie_economisee["SECTOR"] == Sector.RESIDENTIAL)
        ]

        # here we get last year value. Since we have no prospective inside strategy module
        # + we haven't inserted any input for actions before 2024, we have no difference between
        # last year of diagnosis and last year of actions (constant value throughout the period)
        # we therefore can use last year data as comparison
        ref_value_resid_conso = ref_value_resid_conso.loc[
            ref_value_resid_conso["annee"] == max_year_energie_economisee,
            "valeur",
        ]
        assert sheet_ranges["D6"].value != "Indisponible"
        assert sheet_ranges["D6"].value == pytest.approx(
            float(ref_value_resid_conso), 1e-5
        ), "Valeur de consommation résidentielle invalide"  # Résidentiel

        if ges_export_excel:
            # emissions
            ref_value_resid_ges = emission_ges.loc[
                (emission_ges["SECTOR"] == Sector.RESIDENTIAL)
            ]
            ref_value_resid_ges = ref_value_resid_ges.loc[
                (ref_value_resid_ges["annee"] == max_year_energie_economisee),
                "valeur",
            ]
            assert sheet_ranges["C6"].value != "Indisponible"
            assert sheet_ranges["C6"].value == pytest.approx(
                float(ref_value_resid_ges), 1e-5
            ), "Valeur d'émissions résidentielles invalide"  # Résidentiel

        # EnR values
        sheet_ranges_enr = wb["3.ENR"]
        # Valeurs onglet 3.ENR en MWh (les valeurs retournées par les actions sont en
        # GWh)
        # we retrieve the real value supposedly used for hydro power production
        res_values_hydro = energie_produite.loc[
            (energie_produite["RENEWABLEPROD"] == RenewableProd.HYDRO_LOW)
            | (energie_produite["RENEWABLEPROD"] == RenewableProd.HYDRO_HIGH),
        ]
        res_value_hydro = res_values_hydro.loc[
            res_values_hydro["annee"] == max_year_energie_produite, "valeur"
        ].sum()
        assert sheet_ranges_enr["D8"].value != "Indisponible"
        assert sheet_ranges_enr["D8"].value == pytest.approx(
            float(res_value_hydro), 1e-5
        ), "Valeur de production hydraulique invalide"
        res_value_hydro = res_values_hydro.loc[
            res_values_hydro["annee"] == 2026, "valeur"
        ].sum()
        assert sheet_ranges_enr["D25"].value == pytest.approx(
            float(res_value_hydro), 1e-5
        ), "Valeur d'objectif à 2026 hydraulique invalide"

        # target at 2050
        res_target_hydro = res_values_hydro.loc[
            res_values_hydro["annee"] == 2050, "valeur"
        ].sum()
        assert sheet_ranges_enr["F25"].value == pytest.approx(
            float(res_target_hydro), 1e-5
        ), "L'objectif 2050 hydraulique n'est pas valide"

        if polluants:
            # COVNM emissions for residential
            data_covnm = polluants["polluants_atmospheriques"][
                "DataSet.POLLUTANT_COVNM"
            ]
            ref_covnm_resid = data_covnm.loc[
                (data_covnm["SECTOR"] == Sector.RESIDENTIAL)
            ]
            ref_covnm_resid = ref_covnm_resid.loc[
                ref_covnm_resid["annee"] == ref_covnm_resid["annee"].max(),
                "valeur",
            ]
            # corresponding sell in sheet
            sheet_ranges_polluants = wb["5.Polluants atmosphériques"]
            assert sheet_ranges_polluants["G7"].value == pytest.approx(
                float(ref_covnm_resid), 1e-5
            ), "La valeur COVNM du résidentiel n'est pas valide"
        os.remove(filename)


@pytest.mark.parametrize(
    "current_region",
    [
        "auvergne-rhone-alpes",
        "occitanie",
        "paysdelaloire",
    ],
)
@pytest.mark.export_actions
def test_export_actions_confid(region, client_anonymous, current_region, loop):
    region_config = loop.run_until_complete(
        regions_configuration.get_configuration(current_region)
    )
    ges_export_excel = region_config[0]["ges_export_excel"]
    ui_show_plan_actions = region_config[0]["ui_show_plan_actions"]
    if not ui_show_plan_actions:
        pytest.skip("Strategy module disabled in this region.")
    confid = {
        "emission_ges": {
            "secteur": pd.DataFrame(
                [
                    {
                        "annee": 2021,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 75,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2026,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 75,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2030,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 75,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2050,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 80,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                ]
            ),
            "energie": pd.DataFrame(
                [
                    {
                        "annee": 2021,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 40,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2026,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 40,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2030,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 40,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2050,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 30,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                ]
            ),
            "max_annee": 2021,
        },
        "energie_produite": {
            "max_annee": 2021,
            "energie": pd.DataFrame(
                [
                    {
                        "annee": 2021,
                        "nom": "Production du solaire thermique",
                        "RENEWABLEPROD": RenewableProd.SOLAR_THER,
                        "valeur": 75,
                        "confidentiel": "non",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2026,
                        "nom": "Production du solaire thermique",
                        "RENEWABLEPROD": RenewableProd.SOLAR_THER,
                        "valeur": 75,
                        "confidentiel": "non",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2030,
                        "nom": "Production du solaire thermique",
                        "RENEWABLEPROD": RenewableProd.SOLAR_THER,
                        "valeur": 75,
                        "confidentiel": "non",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2050,
                        "nom": "Production du solaire thermique",
                        "RENEWABLEPROD": RenewableProd.SOLAR_THER,
                        "valeur": 80,
                        "confidentiel": "non",
                        "couleur": "#007d75",
                    },
                ]
            ),
        },
        "energie_economisee": {
            "secteur": pd.DataFrame(
                [
                    {
                        "annee": 2021,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 75,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2026,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 75,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2030,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 75,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2050,
                        "nom": "Autres : Gestion des déchets, Industrie hors branche énergie",
                        "SECTOR": "Sector.WASTE_MANAGEMENT,Sector.INDUSTRY_WITHOUT_ENERGY",
                        "valeur": 80,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                ]
            ),
            "energie": pd.DataFrame(
                [
                    {
                        "annee": 2021,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 40,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2026,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 40,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2030,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 40,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                    {
                        "annee": 2050,
                        "nom": "Autres : Gaz, Produits pétroliers",
                        "COMMODITY": "Commodity.GAS,Commodity.OIL",
                        "valeur": 30,
                        "confidentiel": "oui",
                        "couleur": "#007d75",
                    },
                ]
            ),
            "max_annee": 2021,
        },
        "covnm": None,
        "nh3": None,
        "nox": None,
        "sox": None,
        "pm10": None,
        "pm25": None,
        "facture_energetique": {},
        "retombees_fiscales": {},
        "mix_energetique": {},
        "indice_consommation": None,
        "difference_conso": None,
        "emission_co2_par_industrie": None,
        "emission_co2_par_sous_action": None,
    }
    filename = "/tmp/test-export-pcaet-ademe.xlsx"
    with tempfile.NamedTemporaryFile(mode="w+b") as fp:
        output_file_confid = loop.run_until_complete(
            export_excel(current_region, confid, False, fp.name, ges_export_excel)
        )
        shutil.copy(output_file_confid, filename)
        wb = load_workbook(filename)
        sheet_ranges = wb["1.GES et Conso énergie"]
        assert sheet_ranges["C12"].value == "Confidentiel"
        assert sheet_ranges["H12"].value == "Confidentiel"
        os.remove(filename)
