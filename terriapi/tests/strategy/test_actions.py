# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
from pathlib import Path

import pandas as pd
import pytest

import terriapi.controller.regions_configuration as regions_configuration
from terriapi.controller.strategy_actions.actions_dispatcher import ActionsDispatcher
from terriapi.controller.strategy_actions.loaders import (
    DefaultInputsLoader,
    InputsLoader,
)
from terriapi.controller.strategy_actions.loaders.actions_data import (
    get_actions_list,
    get_advanced_params_list,
    get_subactions_list,
)

here = Path(__file__).resolve().parent


def test_handling_missing_advanced_inputs(region, client_basic_user, loop):
    region = "auvergne-rhone-alpes"
    zone = "region"
    zone_id = "84"
    years = {"2022", "2023", "2024"}
    action_year = {"2022": ["1"], "2023": ["1"], "2024": ["1"]}
    action_params = {
        "1_nb_logements": {"2022": 0, "2023": 10, "2024": 20},
        "1_surface_moy_logement": {"2022": 0, "2023": 10, "2024": 20},
    }
    advanced_params = {
        "1": {
            "Répartition des rénovations par niveau de performance énergétique": {
                "0": 40,
                "1": 50,
                "2": 60,
                "3": 60,
            },
            "Gains attribués au chauffage": {"4": 80},
        }
    }
    advanced_economic_params = {
        "3": 5.7,
        "4": 8,
        "5": 8,
        "6": 20,
        "7": 20,
        "8": 11,
        "9": 11,
        "10": 11,
        "11": 11,
        "12": 20,
        "13": 20,
        "14": 20,
        "15": 20,
        "16": 11,
    }
    advanced_params_years = {}
    regional_config = {}

    dispatcher = ActionsDispatcher(region, zone, zone_id)
    # we handle static params
    loop.run_until_complete(dispatcher.load_required_params())

    # we handle user inputs
    defaults_inputs = DefaultInputsLoader(region, zone, zone_id)
    # TODO: use required params to specify which params to load
    defaults_inputs.set_defaults(
        loop.run_until_complete(get_actions_list(region, zone, zone_id)),
        loop.run_until_complete(get_subactions_list(region, zone, zone_id)),
        loop.run_until_complete(get_advanced_params_list(region, zone, zone_id)),
    )
    inputs = InputsLoader(region, zone, zone_id)
    inputs.load_inputs(
        years,
        action_year,
        action_params,
        advanced_params,
        advanced_economic_params,
        advanced_params_years,
        regional_config,
    )
    dispatcher.merge_inputs(defaults_inputs, inputs)
    full_resulting_inputs = dispatcher.inputs["1"]

    assert "advanced" in full_resulting_inputs
    assert "advanced" in full_resulting_inputs["advanced"]
    assert "1" in full_resulting_inputs["advanced"]["advanced"]

    missing_inputs = InputsLoader(region, zone, zone_id)
    missing_inputs.load_inputs(
        years,
        action_year,
        action_params,
        {},
        advanced_economic_params,
        advanced_params_years,
        regional_config,
    )
    dispatcher.merge_inputs(defaults_inputs, missing_inputs)
    missing_resulting_inputs = dispatcher.inputs["1"]

    assert "advanced" in missing_resulting_inputs
    assert "advanced" in missing_resulting_inputs["advanced"]
    assert "1" in missing_resulting_inputs["advanced"]["advanced"]
    assert (
        missing_resulting_inputs["advanced"]["advanced"]["1"].keys()
        == full_resulting_inputs["advanced"]["advanced"]["1"].keys()
    )


def test_without_url_parameters(client_basic_user):
    _request, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/actions", json={}
    )
    assert resp.status == 400


with open(here / "strategy_parameters_for_single_comparisons.json", "r") as f:
    queries_parameters = json.loads(f.read())

with open(here / "strategy_parameters_for_advanced_tests.json", "r") as f:
    advanced_parameters = json.loads(f.read())


def sum_nested_dict(di):
    if isinstance(di, dict):
        return sum(sum_nested_dict(d) for d in di.values())
    else:
        return float(di)


@pytest.mark.parametrize(
    "region_name,zone,zone_id,action",
    [
        pytest.param(
            *territory,
            action_key,
            marks=(
                pytest.mark.strategy_exhaustive
                if conf.get("is_for_exhaustive", False)
                else pytest.mark.strategy_light
            ),
        )
        for action_key, conf in queries_parameters.items()
        for territory in [
            ["auvergne-rhone-alpes", "region", "84"],
            ["occitanie", "region", "76"],
            ["paysdelaloire", "region", "52"],
            ["bretagne", "region", "53"],
            ["auvergne-rhone-alpes", "epci", "200011773"],
            ["occitanie", "epci", "243400819"],
            ["paysdelaloire", "epci", "200060010"],
            ["bretagne", "epci", "200072452"],
            ["auvergne-rhone-alpes", "commune", "42218"],
        ]
    ],
)
def test_running_all_actions(
    client_basic_user, region, region_name, zone, zone_id, action, loop
):
    regional_settings = loop.run_until_complete(
        regions_configuration.get_configuration(region_name)
    )
    ui_show_plan_actions = regional_settings[0]["ui_show_plan_actions"]
    if not ui_show_plan_actions:
        pytest.skip("Strategy module disabled in this region.")

    parameters = queries_parameters[action]["params"]
    if "key" in queries_parameters[action]:
        actions_enabled = loop.run_until_complete(get_actions_list(region_name))
        is_action_enabled = queries_parameters[action]["key"] in [
            a["numero"] for a in actions_enabled
        ]
        if not is_action_enabled:
            pytest.skip("Action disabled inside region")
    if "keys" in queries_parameters[action]:
        actions_enabled = loop.run_until_complete(get_actions_list(region_name))
        are_actions_enabled = set([a["numero"] for a in actions_enabled]).intersection(
            set(queries_parameters[action]["keys"])
        )
        if len(are_actions_enabled) == 0:
            pytest.skip("Actions disabled inside region")

    _request, resp = client_basic_user.post(
        f"/api/{region_name}/actions?zone={zone}&zone_id={zone_id}&id_utilisateur=-1&rounding=false",
        json=parameters,
    )
    assert resp.status == 200
    resp = resp.json
    assert "failed_actions" not in resp, "Some actions failed: " + ", ".join(
        [a.get("action_key", str(a)) for a in resp["failed_actions"]]
    )

    max_year = 2050
    if not "expected" in queries_parameters[action]:
        return
    expected_values = queries_parameters[action]["expected"]
    if "expected_" + region_name in queries_parameters[action]:
        expected_values = {
            **expected_values,
            **queries_parameters[action]["expected_" + region_name],
        }
    for key, expected in expected_values.items():
        assert key in resp

        is_economy_related = key in [
            "facture_energetique",
            "retombees_fiscales",
            "impact_emplois",
        ]
        is_climate_related = key in [
            "energie_economisee",
            "energie_produite",
            "emission_ges",
        ]
        expected_behaviour = expected["behaviour"]
        if expected_behaviour == "empty":
            assert len(resp[key]) == 0, "Results for " + key + " should be empty"
        elif is_climate_related and expected_behaviour == "constant":
            first_future_year = int(resp[key]["max_annee"]) + 1
            assert first_future_year < max_year
            for cat, data in resp[key].items():
                if cat == "max_annee" or cat == "is_empty":
                    continue
                assert len(data) > 0, "Empty data for cat " + cat
                for modality in data:
                    ref_value = None

                    assert len(modality["data"]) > 0, (
                        "Empty data for modality " + modality["nom"] + "in cat " + cat
                    )
                    modality["data"].sort(key=lambda x: x["annee"])
                    for y_data in modality["data"]:
                        if y_data["annee"] == first_future_year:
                            ref_value = y_data["valeur"]
                        elif y_data["annee"] > first_future_year:
                            assert (
                                y_data["valeur"] == ref_value
                            ), f"Values are not constant for {key} in cat {cat} in modality {modality['nom']} in year {str(y_data['annee'])} compared to {first_future_year}"
        elif is_climate_related and expected_behaviour in [
            "decreasing",
            "increasing",
        ]:
            first_future_year = int(resp[key]["max_annee"]) + 1
            assert first_future_year < max_year

            expected_years = expected.get("years", [])
            expected_years.sort()
            # remove potential duplicate
            if first_future_year in expected_years:
                expected_years.remove(first_future_year)

            for cat, data in resp[key].items():
                if cat == "max_annee" or cat == "is_empty":
                    continue
                assert len(data) > 0, "Empty data for cat " + cat
                sum_values = {int(y): 0.0 for y in expected_years}
                sum_values[first_future_year] = 0.0
                for modality in data:
                    assert len(modality["data"]) > 0, (
                        "Empty data for modality " + modality["nom"] + "in cat " + cat
                    )
                    modality["data"].sort(key=lambda x: x["annee"])
                    for y_data in modality["data"]:
                        if int(y_data["annee"]) not in sum_values:
                            continue
                        sum_values[int(y_data["annee"])] += float(y_data["valeur"])
                for y1, y2 in zip(
                    [first_future_year] + expected_years[:-1], expected_years
                ):
                    if expected_behaviour == "decreasing":
                        assert (
                            sum_values[y2] < sum_values[y1]
                        ), f"Data for {cat} is not decreasing for {key} in year {y2} compared to {y1}"
                    elif expected_behaviour == "increasing":
                        assert (
                            sum_values[y2] > sum_values[y1]
                        ), f"Data for {cat} is not increasing for {key} in year {y2} compared to {y1}"
        elif is_economy_related and expected_behaviour == "filled":
            expected_years = expected["years"]
            if key == "facture_energetique":
                for year, data in resp[key].items():
                    if int(year) not in expected_years:
                        continue
                    assert data != 0, (
                        "Not filled data for facture_energetique in year " + year
                    )
            elif key == "retombees_fiscales":
                for year, data in resp[key].items():
                    if int(year) not in expected_years:
                        continue
                    assert sum(data.values()) != 0, (
                        "Not filled data for retombees_fiscales in year " + year
                    )
            elif key == "impact_emplois":
                for year, data in resp[key].items():
                    if int(year) not in expected_years:
                        continue
                    if expected.get("investissement", "not_null") == "not_null":
                        assert float(data["investissement"]) != 0, (
                            "Not filled investments for impact_emplois in year " + year
                        )
                    if not expected.get("only_indirect", False) and not expected.get(
                        "only_inv", False
                    ):
                        assert sum_nested_dict(data["direct"]) != 0, (
                            "Not filled direct impact for impact_emplois in year "
                            + year
                        )
                    if not expected.get("only_direct", False) and not expected.get(
                        "only_inv", False
                    ):
                        assert sum_nested_dict(data["indirect"]) != 0, (
                            "Not filled indirect impact for impact_emplois in year "
                            + year
                        )
        elif is_economy_related and expected_behaviour == "null":
            if key == "facture_energetique":
                for year, data in resp[key].items():
                    assert data == 0, (
                        "Not null data for facture_energetique in year " + year
                    )
            elif key == "retombees_fiscales":
                for year, data in resp[key].items():
                    assert sum(data.values()) == 0, (
                        "Not null data for retombees_fiscales in year " + year
                    )
            elif key == "impact_emplois":
                for year, data in resp[key].items():
                    if expected.get("investissement", "null") == "null":
                        assert float(data["investissement"]) == 0, (
                            "Not filled investments for impact_emplois in year " + year
                        )
                    assert sum_nested_dict(data["direct"]) == 0, (
                        "Not filled direct impact for impact_emplois in year " + year
                    )
                    assert sum_nested_dict(data["indirect"]) == 0, (
                        "Not filled indirect impact for impact_emplois in year " + year
                    )


def test_meta_descriptor_api(
    client_basic_user, client_admin_user, client_global_admin_user
):
    # not authorized for plain users
    _request, resp = client_basic_user.get(
        f"/api/auvergne-rhone-alpes/actions/requisites"
    )
    assert resp.status == 401

    # not authorized for non regional admin
    _request, resp = client_admin_user.get(
        f"/api/auvergne-rhone-alpes/actions/requisites"
    )
    assert resp.status == 401

    # authorized for regional admin
    _request, resp = client_global_admin_user.get(
        f"/api/auvergne-rhone-alpes/actions/requisites"
    )
    assert resp.status == 200


def test_admin_passage_table(
    client_basic_user, client_admin_user, client_global_admin_user
):
    # not authorized for plain users
    _request, resp = client_basic_user.get(f"/api/auvergne-rhone-alpes/passage/table")
    assert resp.status == 401

    # not authorized for non regional admin
    _request, resp = client_admin_user.get(f"/api/auvergne-rhone-alpes/passage/table")
    assert resp.status == 401

    # authorized for regional admin
    _request, resp = client_global_admin_user.get(
        f"/api/auvergne-rhone-alpes/passage/table"
    )
    assert resp.status == 200


@pytest.mark.parametrize(
    "region_name,zone,zone_id",
    [
        territory
        for territory in [
            ["auvergne-rhone-alpes", "epci", "200011773"],
            ["occitanie", "epci", "243400819"],
            # action disabled in this case ["paysdelaloire", "epci", "200060010"],
            ["bretagne", "epci", "200072452"],
            ["auvergne-rhone-alpes", "commune", "42218"],
        ]
    ],
)
def test_advanced_param_effect(
    client_basic_user, region, region_name, zone, zone_id, loop
):
    regional_settings = loop.run_until_complete(
        regions_configuration.get_configuration(region_name)
    )
    ui_show_plan_actions = regional_settings[0]["ui_show_plan_actions"]
    if not ui_show_plan_actions:
        pytest.skip("Strategy module disabled in this region.")

    default_data = advanced_parameters["advanced_effect"]["default"]
    different_data = advanced_parameters["advanced_effect"]["different"]

    _request, resp = client_basic_user.post(
        f"/api/{region_name}/actions?zone={zone}&zone_id={zone_id}&id_utilisateur=-1",
        json=default_data,
    )
    assert resp.status == 200
    default_resp = resp.json

    _request, resp = client_basic_user.post(
        f"/api/{region_name}/actions?zone={zone}&zone_id={zone_id}&id_utilisateur=-1",
        json={**default_data, **different_data},
    )
    assert resp.status == 200
    different_resp = resp.json
    assert json.dumps(default_resp) != json.dumps(different_resp)

    # facture_energetique (values are negative)
    assert -sum(default_resp["facture_energetique"].values()) < -sum(
        different_resp["facture_energetique"].values()
    )
