﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import copy
import datetime
import json
import os
import time
from pathlib import Path

import pandas as pd
import pytest

import terriapi.controller.regions_configuration as regions_configuration
from terriapi import settings
from terriapi.controller import analyse as controller_analyse
from terriapi.controller import fetch, strategy_export
from terriapi.controller.mesure_audience import (
    MesureDAudience,
    MesureDAudienceActions,
    MesureDAudienceAnalysesTerritoriales,
    MesureDAudienceIndicateurs,
)
from terriapi.conversion_perimetres.conversion import get_perimeter_conversion_table
from terriapi.integration import get_table_perimeter_year

here = Path(__file__).resolve().parent


def test_connection_pg(client_anonymous, db, loop):
    """
    Test de la connection postgres
    """
    resp = loop.run_until_complete(fetch("select 1 as res"))
    assert resp[0]["res"] == 1


@pytest.mark.parametrize(
    "uri",
    [
        "/api/version",
        "/api/auvergne-rhone-alpes/zone",
        "/api/auvergne-rhone-alpes/analysis",
        "/api/auvergne-rhone-alpes/zone/commune",
        "/api/auvergne-rhone-alpes/actions/subactions",
        "/api/auvergne-rhone-alpes/poi/layers",
    ],
)
def test_statut_ok(client_anonymous, uri):
    """
    Chaque requête doit renvoyer un statut HTTP 200 et un contenu json non vide
    """
    _request, resp = client_anonymous.get(uri)
    assert resp.status == 200
    json = resp.json
    assert len(json) > 0


@pytest.mark.parametrize(
    "uri",
    [
        "/api/auvergne-rhone-alpes/actions/",
        "/api/auvergne-rhone-alpes/actions/params/advanced",
    ],
)
def test_statut_parametres_avances(client_anonymous, uri):
    """
    Les paramètres avancés de l'impact emploi nécessitent une zone et un identifiant
    """
    _request, resp = client_anonymous.get(
        uri, params={"zone": "epci", "zone_id": "200000172"}
    )
    assert resp.status == 200
    json = resp.json
    assert len(json) > 0


@pytest.mark.parametrize(
    "zone,maille,zone_id",
    [
        ("region", "epci", "84"),
        ("region", "departement", "84"),
        ("departement", "commune", "69D"),
        ("teposcv", "commune", "TEPOSCV-1"),
        ("scot", "commune", "00001"),
        ("pnr", "commune", "FR8000001"),
    ],
)
def test_analyses_tout_territoire(
    client_basic_user, client_admin_user, zone, maille, zone_id
):
    """
    Statut requête sur toutes les analyses (combinaisons tous territoires/mailles)
    """
    # XXX dag : adapter à toutes les régions
    # Récupérer la première analyse de la région
    _req, resp = client_basic_user.get("/api/auvergne-rhone-alpes/analysis")
    assert resp.status == 200
    content = resp.json
    id_analyse = content[0]["id"]
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={
            "zone": zone,
            "maille": maille,
            "zone_id": zone_id,
            "annee": 2016,
            "provenance": "test",
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 200


def test_statut_analyse_ko(client_basic_user, client_admin_user):
    """
    Statut requête sur la première sans zone ou sans maille
    """
    # XXX dag : adapter à toutes les régions
    # Récupérer la première analyse de la région
    _req, resp = client_basic_user.get("/api/auvergne-rhone-alpes/analysis")
    assert resp.status == 200
    content = resp.json
    id_analyse = content[0]["id"]

    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        json={"filtres_categorie": {}},
    )
    assert resp.status == 404
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        json={"filtres_categorie": {}},
    )
    assert resp.status == 404
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={"zone": "region", "provenance": "test"},
    )
    assert resp.status == 404
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={"zone": "region", "maille": "commune", "provenance": "test"},
    )
    assert resp.status == 404
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={"zone": "region", "maille": "commune", "zone_id": "84"},
    )
    assert resp.status == 404
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={"maille": "commune", "provenance": "test"},
    )
    assert resp.status == 404
    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={
            "zone": "region",
            "maille": "commune",
            "zone_id": "84",
            "provenance": "test",
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 200


def test_statut_analyse_filtre(client_basic_user, client_admin_user):
    """
    Statut requête sur la première analyse avec filtrage sur un epci
    """
    # XXX dag : adapter à toutes les régions
    # Récupérer la première analyse de la région
    _req, resp = client_basic_user.get("/api/auvergne-rhone-alpes/analysis")
    assert resp.status == 200
    content = resp.json
    id_analyse = content[0]["id"]

    _req, resp = client_basic_user.post(
        f"/api/auvergne-rhone-alpes/analysis/{id_analyse}/data",
        params={
            "zone": "epci",
            "maille": "commune",
            "zone_id": "200000172",
            "annee": 2016,
            "provenance": "test",
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 200


def test_liste_strategie_territoriale(region, client_basic_user, scenario):
    _req, resp = client_basic_user.get("/api/user/pytest/scenario/list")
    assert resp.status == 200
    scenario_list = resp.json
    assert len(scenario_list) > 0
    assert isinstance(scenario_list[0], dict)
    scenario = scenario_list[0]
    # on vérifie l'ensemble des clés
    expected = set(
        [
            "id",
            "mail",
            "titre",
            "description",
            "partage",
            "partage_par",
            "publique",
            "zone_type",
            "zone_id",
            "derniere_modif",
            "nom_territoire",
        ]
    )
    assert set(scenario.keys()) == expected


def test_creer_plan_action(
    region, client_basic_user, strat_params, passage_table, example_action, indicators
):
    """Tester la création d'un plan d'action"""
    valeurs = {"{}".format(year): 0 for year in range(2019, 2051)}
    # ensemble des actions
    _req, resp = client_basic_user.get(
        "/api/pytest/actions", params={"zone": "region", "zone_id": "1"}
    )
    assert resp.status == 200
    actions = resp.json
    assert "actions" in actions
    # paramètres pour chaque action
    _req, resp = client_basic_user.get("/api/pytest/actions/subactions")
    assert resp.status == 200
    params = resp.json
    # paramètres dits "avancés"
    _req, resp = client_basic_user.get(
        "/api/pytest/actions/params/advanced",
        params={"zone_id": "1", "zone": "region"},
    )
    assert resp.status == 200
    avancés = resp.json
    data = {param["nom"]: valeurs for param in params}
    # on remplit certaines valeurs de paramètres d'action
    # les actions concernées
    #  - rénovations résidentielles (1)
    #  - Centrale photovolatique au sol (3)
    #  - Installation solaire thermique (4b)
    data["actions"] = ["1", "3", "4b"]
    data["1_nb_logements"].update(
        {
            "2019": "150",
            "2020": "150",
            "2021": 0,
            "2022": "200",
            "2023": "200",
            "2024": 0,
            "2025": "200",
            "2026": "300",
        }
    )
    data["1_surface_moy_logement"].update(
        {
            "2019": "50",
            "2020": "50",
            "2021": 0,
            "2022": "50",
            "2023": "50",
            "2024": 0,
            "2025": "40",
            "2026": "40",
        }
    )
    data["3_puiss_crete"].update({"2020": "2", "2021": "2", "2022": "4", "2023": "5"})
    data["4b_nb_batiment_collectif_renouv"].update(
        {"2025": "50", "2026": "50", "2027": "50"}
    )
    data["4b_surf_capteurs_bati"].update({"2025": "5", "2026": "5", "2027": "5"})
    # on garde les paramètres avancés dit aussi "économiques" par défaut
    data["advanced"] = avancés["actions"]
    # paramètres économiques
    data["economique"] = {
        "111": 76.0,
        "112": 57.7,
        "113": 76.3,
        "114": 57.7,
        "115": 57.7,
        "116": 57.7,
    }
    # trajectoires cibles
    data["trajectoires"] = {
        "emission_ges": {"{}".format(year): 55_000 for year in range(2019, 2030)},
        "energie_economisee": {
            "{}".format(year): 220_000 for year in range(2019, 2030)
        },
        "energie_produite": {"{}".format(year): 40_000 for year in range(2019, 2030)},
    }
    # on ajoute le titre, la description et les zones géographiques
    data["titre"] = "test région département"
    data["description"] = "test région"
    data["zone_type"] = "region#departement"
    data["nom_territoire"] = "Cantal"
    data["region"] = "pytest"
    data["zone_id"] = None
    _req, resp = client_basic_user.post("/api/user/pytest/scenario", json=data)
    assert resp.status == 200, "La création du scénario a échoué."
    scenario = resp.json

    # on vérifie que le scénario a bien été créé
    _req, resp = client_basic_user.get("/api/user/pytest/scenario/list")
    assert resp.status == 200, "La liste de scénarios ne fonctionne plus."
    scenario_list = resp.json

    # on vérifie que le titre correspond
    scenario_cree = [v for v in scenario_list if v["titre"] == data["titre"]]
    assert (
        len(scenario_cree) == 1
    ), "Le scénario n'est pas présent dans la liste (par titre) !"

    # on vérifie que l'ID correspond
    scenario_cree = [v for v in scenario_list if v["id"] == scenario["scenario_id"]]
    assert (
        len(scenario_cree) == 1
    ), "Le scénario n'est pas présent dans la liste (par id) !"


def test_rollback_creation_plan_action(
    client_basic_user, strat_params, passage_table, example_action, scenario, indicators
):
    """Tester la création d'un plan d'action"""
    valeurs = {"{}".format(year): 0 for year in range(2019, 2051)}
    # paramètres pour chaque action
    _req, resp = client_basic_user.get("/api/pytest/actions/subactions")
    assert resp.status == 200, "La récupération des paramètres de scénario a échoué"
    params = resp.json
    data = {param["nom"]: valeurs for param in params}
    # on remplit certaines valeurs de paramètres d'action
    # les actions concernées
    #  - rénovations résidentielles (1)
    #  - Centrale photovolatique au sol (3)
    #  - Installation solaire thermique (4b)
    data["actions"] = ["1", "3", "4b"]
    data["1_nb_logements"].update(
        {
            "2019": "150",
            "2020": "150",
            "2021": 0,
            "2022": "200",
            "2023": "200",
            "2024": 0,
            "2025": "200",
            "2026": "300",
        }
    )
    data["1_surface_moy_logement"].update(
        {
            "2019": "50",
            "2020": "50",
            "2021": 0,
            "2022": "50",
            "2023": "50",
            "2024": 0,
            "2025": "40",
            "2026": "40",
        }
    )
    data["3_puiss_crete"].update({"2020": "2", "2021": "2", "2022": "4", "2023": "5"})
    data["4b_nb_batiment_collectif_renouv"].update(
        {"2025": "50", "2026": "50", "2027": "50"}
    )
    data["4b_surf_capteurs_bati"].update({"2025": "5", "2026": "5", "2027": "5"})
    # on teste en fournissant des paramètres qui ne sont pas un dictionnaire
    data["advanced"] = 1111
    # paramètres économiques
    data["economique"] = {
        "111": 76.0,
        "112": 57.7,
        "113": 76.3,
        "114": 57.7,
        "115": 57.7,
        "116": 57.7,
    }
    # trajectoires cibles
    data["trajectoires"] = {
        "emission_ges": {"{}".format(year): 55_000 for year in range(2019, 2030)},
        "energie_economisee": {
            "{}".format(year): 220_000 for year in range(2019, 2030)
        },
        "energie_produite": {"{}".format(year): 40_000 for year in range(2019, 2030)},
    }
    # on ajoute le titre, la description et les zones géographiques
    data["titre"] = "failure test with wrong advanced parameters"
    data["description"] = "test région"
    data["zone_type"] = "region#departement"
    data["nom_territoire"] = "Cantal"
    data["region"] = "pytest"
    data["zone_id"] = None
    _req, resp = client_basic_user.post("/api/user/pytest/scenario", json=data)
    assert resp.status == 500, "La création du scénario n'a pas échoué comme prévu."

    # on vérifie que le scénario n'a pas été créé
    _req, resp = client_basic_user.get("/api/user/pytest/scenario/list")
    assert resp.status == 200, "La liste de scénarios n'est plus accessible"
    scenario_list = resp.json
    # on vérifie qu'on n'a pas le scénario dans la liste
    assert (
        len([v for v in scenario_list if v["titre"] == data["titre"]]) == 0
    ), "Le scénario est bien présent dans la liste, le rollback n'a pas eu lieu"


def test_recuperer_plan_action(client_basic_user, scenario):
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    action = resp.json
    assert "params" in action
    assert "advanced" in action
    assert "trajectoires" in action
    assert len(action["trajectoires"]) == 3
    assert "emission_ges" in action["trajectoires"]
    assert "energie_produite" in action["trajectoires"]
    assert "energie_economisee" in action["trajectoires"]


def test_mise_a_jour_plan_action(client_basic_user, scenario):
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    data = resp.json
    # on met à jour certains champs
    data["titre"] = "mise_a_jour"
    data["trajectoires"]["energie_produite"]["2020"] = 50_000
    _req, resp = client_basic_user.put(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"]), json=data
    )
    assert resp.status == 200
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    action = resp.json
    assert action["titre"] == "mise_a_jour"
    assert action["trajectoires"]["energie_produite"]["2019"] == 40_000
    assert action["trajectoires"]["energie_produite"]["2020"] == 50_000
    assert action["trajectoires"]["energie_produite"]["2021"] == 40_000


def test_existence_strategie(client_basic_user, scenario):
    """On test l'existence d'une stratégie et du code HTTP si elle n'existe pas."""
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    _req, resp = client_basic_user.get("/api/user/pytest/scenario/{}".format(-12))
    assert resp.status == 404
    message = resp.json
    assert message == {"message": "Scénario non trouvé."}


def test_droit_lecture_strategie(client_basic_user, client_admin_user, scenario):
    """Une autre utilisatrice tente d'accéder à une stratégie dont elle n'est pas l'auteure."""
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    # on essaie d'accéder au scénario avec une autre utilisatrice.
    _req, resp = client_admin_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 401
    message = resp.json
    assert message == {
        "user_id": "alice@terristory-wonder.land",
        "message": "Vous n'êtes pas autorisé·e à voir ces informations.",
    }


def test_droit_lecture_strategie_publique(
    client_basic_user, client_admin_user, scenario
):
    """Une autre utilisatrice tente d'accéder à une stratégie publique dont elle n'est pas l'auteure."""
    # l'utilisateur ne peut partager s'il n'a pas les droits.
    _req, resp = client_basic_user.post(
        "/api/user/pytest/scenario/{}/publier".format(scenario["scenario_id"]),
        json={"publique": False},
    )
    assert resp.status == 401
    content = resp.json
    assert content["message"] == "Vous n'êtes pas autorisé·e à publier ce scénario."
    # on donne le droit de publication au premier utilisateur aka 'client_basic_user'
    _req, resp = client_admin_user.put(
        "/api/user/pytest/update",
        json={"login": "pytest@terristory-nowhere.dev", "publication": True},
    )
    assert resp.status == 200
    # on a besoin de refresh l'access token pour avoir la mise à jour du payload et
    # donc la bonne valeur publication=True
    # on récupère d'abord le refresh token
    refresh_token = client_basic_user.session.cookies.get("refresh_token", False)
    assert refresh_token
    # on demande de rafraichir l'access token (et donc la mise à jour du payload)
    _req, resp = client_basic_user.post(
        "/api/pytest/auth/refresh", json={"refresh_token": refresh_token}
    )
    tokens = resp.json
    # On met à jour les cookies d'authent à la session du client_basic_user Web de test.
    client_basic_user.set_cookies(tokens)
    # Le user 'client_basic_user' peut désormais rendre publique sa stratégie. publique=false
    # est la valeur actuelle (dans l'UI). On souhaite la mettre à vraie.
    _req, resp = client_basic_user.post(
        "/api/user/pytest/scenario/{}/publier".format(scenario["scenario_id"]),
        json={"publique": False},
    )
    assert resp.status == 200
    # on essaie d'accéder au scénario avec une autre utilisatrice aka 'client_admin_user'
    _req, resp = client_admin_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    content = resp.json
    assert "params" in content
    assert "advanced" in content
    assert "trajectoires" in content
    # on récupère la liste des stratégies. L'id de celle partagée doit être visible
    _req, resp = client_admin_user.get("/api/user/pytest/scenario/list")
    assert resp.status == 200
    strategies = resp.json
    assert scenario["scenario_id"] in [x["id"] for x in strategies]


def test_impossible_publier_strategie_si_non_auteur(client_admin_user, scenario):
    """On ne peut pas publier une stratégie qui ne nous appartient pas."""
    # NOTE: l'utilisatrice 'client_admin_user' a déjà les droits de publication (cf la
    # fixture associée)
    _req, resp = client_admin_user.post(
        "/api/user/pytest/scenario/{}/publier".format(scenario["scenario_id"]),
        json={"publique": False},
    )
    assert resp.status == 401
    content = resp.json
    assert content["message"] == "Vous n'êtes pas autorisé·e à publier ce scénario."


def test_impossible_partager_strategie_si_non_auteur(client_admin_user, scenario):
    """On ne peut pas publier une stratégie qui ne nous appartient pas."""
    _req, resp = client_admin_user.post(
        f"/api/user/pytest/scenario/{scenario['scenario_id']}/partage",
        json={"emails": "pytest@terristory-nowhere.dev\n"},
    )
    assert resp.status == 401
    content = resp.json
    assert content["message"] == "Vous n'êtes pas autorisé·e à partager ce scénario."


def test_droit_lecture_strategie_partagee(
    client_basic_user, client_admin_user, scenario
):
    """L'utilisatrice doit avoir accès à une stratégie qui la a été partagée."""
    _req, resp = client_admin_user.get("/api/pytest/auth/me")
    alice = resp.json
    alice = alice["me"]
    # On partage la stratégie avec Alice.
    _req, resp = client_basic_user.post(
        f"/api/user/pytest/scenario/{scenario['scenario_id']}/partage",
        json={"emails": alice["user_id"]["mail"] + "\n"},
    )
    assert resp.status == 200
    # on récupère la liste des stratégies. L'id de celle partagée doit être visible
    _req, resp = client_admin_user.get("/api/user/pytest/scenario/list")
    assert resp.status == 200
    strategies = resp.json
    assert scenario["scenario_id"] in [x["id"] for x in strategies]
    # on essaie d'accéder au scénario avec une autre utilisatrice aka 'client_admin_user'
    _req, resp = client_admin_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    content = resp.json
    assert "params" in content
    assert "advanced" in content
    assert "trajectoires" in content


def test_partage_strategie_avec_email_non_trouve(client_basic_user, scenario):
    """On ne peut pas partager un scénario si on spécifie des emails qui n'existent pas en base."""
    # On partage la stratégie en mettant des emails qui n'existent pas en base.
    _req, resp = client_basic_user.post(
        f"/api/user/pytest/scenario/{scenario['scenario_id']}/partage",
        json={"emails": "\n".join(["big.nowhere@ellroy.dev", "jazz@blues.io"])},
    )
    assert resp.status == 400
    content = resp.json
    assert (
        content["message"]
        == "Des emails sont inexistants dans la base des utilisateurs"
    )


def test_desactivation_partage_strategie(
    client_basic_user, client_admin_user, scenario
):
    """Quand on désactive le partage d'une stratégie publique ou partagée, on n'y a plus accès."""
    _req, resp = client_admin_user.get("/api/pytest/auth/me")
    alice = resp.json
    alice = alice["me"]
    # On partage d'abord la stratégie avec Alice.
    _req, resp = client_basic_user.post(
        f"/api/user/pytest/scenario/{scenario['scenario_id']}/partage",
        json={"emails": alice["user_id"]["mail"] + "\n"},
    )
    assert resp.status == 200
    # Alice désactive le partage de cette stratégie
    _req, resp = client_admin_user.post(
        "/api/user/pytest/scenario/{}/desactive".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    # On vérifie qu'il n'est plus dans la liste des stratégies.
    _req, resp = client_admin_user.get("/api/user/pytest/scenario/list")
    # Soit la liste est vide (status 404), soit il y a des stratégies, mais pas celle
    # désactivée.
    assert resp.status in (200, 404)
    if resp.status == 200:
        strategies = resp.json
        assert scenario["scenario_id"] not in [x["id"] for x in strategies]
    if resp.status == 404:
        content = resp.json
        assert content["message"] == "Scénario non trouvés."
    # Ni accessible en lecture
    _req, resp = client_admin_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 404
    content = resp.json
    assert content["message"] == "Scénario non trouvé."


def test_droit_de_mise_a_jour_strategie(client_basic_user, client_admin_user, scenario):
    """Une autre utilisatrice tente de mettre à jour une stratégie dont elle n'est pas l'auteure."""
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    data = resp.json
    # on met à jour certains champs
    data["titre"] = "mise_a_jour"
    data["trajectoires"]["energie_produite"]["2020"] = 50_000
    # on essaie d'accéder au scénario avec une autre utilisatrice.
    _req, resp = client_admin_user.put(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"]), json=data
    )
    assert resp.status == 401
    message = resp.json
    assert message == {
        "user_id": "alice@terristory-wonder.land",
        "message": "Vous n'êtes pas autorisé·e à modifier ces informations.",
    }


def test_droit_de_suppression_strategie(
    client_basic_user, client_basic_user_in_aura, client_admin_user, scenario
):
    """Une autre utilisatrice tente de mettre supprimer une stratégie dont elle n'est pas l'auteure."""
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    # on essaie d'accéder au scénario avec une autre utilisatrice.
    _req, resp = client_basic_user_in_aura.delete(
        "/api/user/pytest/scenario/{}".format(scenario["scenario_id"])
    )
    assert resp.status == 401
    message = resp.json
    assert message == {
        "user_id": "alice@terristory-wonder.land",
        "message": "Vous n'êtes pas autorisé·e à supprimer ces informations.",
    }


def test_action_export_excel_ademe(client_basic_user, scenario):
    _req, resp = client_basic_user.get(
        "/api/user/pytest/scenario/{}/".format(scenario["scenario_id"])
    )
    assert resp.status == 200
    content = resp.json
    # Pour le calcul, nul besoin de certains paramètres. En particulier pour lancer
    # un calcul, on passe d'une liste d'identifiants (stocké) à un dict par an et par
    # identifiant de stratégie
    strategie_ids = content.pop("actions")
    data = {
        "actions": {str(year): strategie_ids for year in range(2019, 2051)},
        "advanced": {},
    }
    data["trajectoires_cibles"] = []
    # Dans le POST du calcul, on doit avoir les noms et les paramètres des actions
    # dans corps de la requête, et non dans le champ 'params'.
    data.update({x["nom"]: x["valeur"] for x in content.pop("params")})
    for key, value in content.items():
        if isinstance(value, dict):
            data[key] = value
    _req, resp = client_basic_user.post(
        "/api/auvergne-rhone-alpes/actions/export",
        params={
            "zone": "epci",
            "zone_id": "200000172",
            "type_action": "test",
            "id_utilisateur": 1,
        },
        json=data,
    )
    assert resp.status == 200
    content = resp.content
    assert len(content) > 0


def test_suivi_trajectoire(region, client_basic_user, scenario):
    # we test failed queries (because arguments are missing)
    _req, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/analysis/suivi_trajectoire",
        params={"zone": "epci", "zone_id": "200000172", "type_action": "test"},
    )
    assert resp.status == 404
    _req, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/analysis/suivi_trajectoire",
        params={
            "zone": "epci",
            "type_action": "test",
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 404
    _req, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/analysis/suivi_trajectoire",
        params={
            "zone_id": "200000172",
            "type_action": "test",
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 404

    # lookup trajectory results
    _req, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/analysis/suivi_trajectoire",
        params={
            "zone": "epci",
            "zone_id": "200000172",
            "type_action": "test",
            "id_utilisateur": -1,
            "sankey": "conso_energetique_sankey",
        },
    )
    assert resp.status == 200
    content = resp.json
    assert len(content) > 0

    # we test we have correct values
    assert "conso_energetique" in content
    assert "prod_enr" in content
    assert "sankey" in content

    # lookup trajectory results
    _req, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/analysis/suivi_trajectoire",
        params={
            "zone": "epci",
            "zone_id": "200000172",
            "type_action": "test",
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 200
    content = resp.json
    assert len(content) > 0

    # we test we have correct values
    assert "sankey" in content
    assert content["sankey"].get("statut", False) == "type de sankey non précisé"


def test_activation_analyse(client_basic_user, client_admin_user, disabled_indicator):
    """Test l'activation d'une analyse.

    On teste aussi le fait que l'utilisateur admin et non authentifié puisse voir
    cette analyses activée.
    """
    # On teste l'activation de l'analyse désactivée
    _req, resp = client_admin_user.get(
        f"/api/pytest/analysis/{disabled_indicator}/activate"
    )
    assert resp.status == 200
    # En tant qu'admin je peux toujours la voir
    _req, resp = client_admin_user.get("/api/pytest/analysis")
    assert resp.status == 200
    content = resp.json
    analyse_ids = [a["id"] for a in content]
    assert disabled_indicator in analyse_ids

    # En tant que simple utilisateur, je peux aussi la voir
    _req, resp = client_basic_user.get("/api/pytest/analysis")
    assert resp.status == 200
    content = resp.json
    analyse_ids = [a["id"] for a in content]
    assert disabled_indicator in analyse_ids


def test_desactivation_analyse(client_basic_user, client_admin_user, enabled_indicator):
    """On teste la désactivation d'une analyse.

    Seule l'utilisatrice l'admin peut la voir. L'utilisateur non authentifié ne peut pas la voir.
    """
    # On teste la désactivation de l'analyse activée
    _req, resp = client_admin_user.get(
        f"/api/pytest/analysis/{enabled_indicator}/deactivate"
    )
    assert resp.status == 200
    # En tant qu'admin je peux toujours la voir
    _req, resp = client_admin_user.get("/api/pytest/analysis")
    assert resp.status == 200
    content = resp.json
    analyse_ids = [a["id"] for a in content]
    assert enabled_indicator in analyse_ids

    # En tant que simple utilisateur, je ne peux plus la voir
    _req, resp = client_basic_user.get("/api/pytest/analysis")
    assert resp.status == 200
    content = resp.json
    analyse_ids = [a["id"] for a in content]
    assert enabled_indicator not in analyse_ids


def test_impossible_desactiver_analyse_pour_simple_utilisateur(
    client_basic_user, enabled_indicator
):
    # En tant que simple utilisateur, je ne peux pas désactiver une analyse
    _req, resp = client_basic_user.get(
        f"/api/auvergne-rhone-alpes/analysis/{enabled_indicator}/deactivate"
    )
    assert resp.status == 401


def test_deleting_category(region, indicators, client_anonymous, client_admin_user):
    """
    Test the ability to change themes order through API
    """
    data_table_to_delete = "other_cat"

    # not allowed for anonymous user
    _request, resp = client_anonymous.delete(
        f"/api/pytest/categorie/{data_table_to_delete}/"
    )
    assert resp.status == 401

    # accepted for admin but fails in wrong region
    _req, resp = client_admin_user.delete(
        f"/api/auvergne-rhone-alpes/categorie/{data_table_to_delete}/"
    )
    assert resp.status == 401

    # accepted for admin but unknown table
    _req, resp = client_admin_user.delete(
        "/api/pytest/categorie/non_existing_category/"
    )
    assert resp.status == 404

    # accepted for admin but protected category
    _req, resp = client_admin_user.delete("/api/pytest/categorie/secteur/")
    assert resp.status == 400
    content = resp.json
    assert "message" in content and "protégée" in content["message"]

    # accepted for admin => but didn't confirm
    _req, resp = client_admin_user.delete(
        f"/api/pytest/categorie/{data_table_to_delete}/"
    )
    assert resp.status == 200
    content = resp.json
    # we make sure we have the right numbers
    # 0 as we didn't associate any chart (yet) in conftest
    assert "related_charts" in content and content["related_charts"] == 0
    # 4 as we have 4 indicators in conftest with other_cat column
    assert "related_datatables" in content and content["related_datatables"] == 5

    # we check it still is in the list of categories
    _request, resp = client_anonymous.get("/api/pytest/categories/")
    assert resp.status == 200
    dict_categories = resp.json
    assert data_table_to_delete in dict_categories

    # accepted for admin and did confirm
    _req, resp = client_admin_user.delete(
        f"/api/pytest/categorie/{data_table_to_delete}/?confirm=true"
    )
    assert resp.status == 200

    # we check it is not in the list of categories anymore
    _request, resp = client_anonymous.get("/api/pytest/categories/")
    assert resp.status == 200
    dict_categories = resp.json
    assert data_table_to_delete not in dict_categories


def test_representations_possibles(
    client_admin_user, data_for_perimeter_conversion, indicators
):
    """Teste si les indicateurs sont associées aux bonnes représentations"""
    _req, resp = client_admin_user.get("/api/pytest/analysis")
    liste_definitions_indicateurs = resp.json
    for definition_indicateur in liste_definitions_indicateurs:
        if definition_indicateur["id"] == -7:
            assert set(
                map(
                    lambda x: x["arg"],
                    definition_indicateur["representations_possibles"],
                )
            ) == set(
                (
                    "line",
                    "bar",
                    "pie",
                    "radar",
                    "marqueur-svg",
                    "analysis-launcher",
                    "map",
                )
            )
        if definition_indicateur["id"] == -9:
            assert set(
                map(
                    lambda x: x["arg"],
                    definition_indicateur["representations_possibles"],
                )
            ) == set(
                [
                    "line",
                    "bar",
                    "pie",
                    "radar",
                    "marqueur-svg",
                    "analysis-launcher",
                    "map",
                ]
            )


def test_auth_admin(client_anonymous, client_basic_user):
    # not reachable to anonymous users
    _request, resp = client_anonymous.put(
        "/api/auvergne-rhone-alpes/date_perimetre", json={}
    )
    assert resp.status == 401
    response = resp.json
    assert response["exception"] == "Unauthorized"

    # even with a user
    _req, resp = client_basic_user.put(
        "/api/auvergne-rhone-alpes/date_perimetre", json={}
    )
    assert resp.status == 401


def test_plan_action_effet_reel(client_basic_user, region):
    """Tester la création d'un plan d'action"""
    # on teste d'envoyer tout ça
    _req, resp = client_basic_user.post(
        "/api/auvergne-rhone-alpes/actions",
        params={"zone": "region", "zone_id": "84", "id_utilisateur": -1},
        json={
            "actions": {},
            "advanced": {},
            "economique": {},
            "trajectoires_cibles": [],
        },
    )
    assert resp.status == 200
    ref_results = resp.json

    data = {}
    # on remplit certaines valeurs de paramètres d'action
    # les actions concernées
    #  - rénovations résidentielles (1)
    data["actions"] = {str(year): ["1"] for year in range(2023, 2051)}

    # on dit qu'il y a des améliorations jusqu'à 2030
    data["1_nb_logements"] = {
        "2019": 0,
        "2020": 0,
        "2021": 0,
        "2022": 0,
        "2023": 0,
        "2024": 0,
        "2025": 0,
        "2026": 0,
        "2027": 0,
        "2028": 0,
        "2029": 10000,
        "2030": 15000,
        "2031": 18000,
        "2032": 20000,
        "2033": 30000,
        "2034": 50000,
        "2035": 100000,
        "2036": 150000,
        "2037": 200000,
        "2038": 250000,
        "2039": 300000,
        "2040": 350000,
        "2041": 0,
        "2042": 0,
        "2043": 0,
        "2044": 0,
        "2045": 0,
        "2046": 0,
        "2047": 0,
        "2048": 0,
        "2049": 0,
        "2050": 0,
    }

    data["1_surface_moy_logement"] = {
        "2019": 0,
        "2020": 0,
        "2021": 0,
        "2022": 0,
        "2023": 0,
        "2024": 0,
        "2025": 0,
        "2026": 0,
        "2027": 0,
        "2028": 0,
        "2029": 60,
        "2030": 60,
        "2031": 60,
        "2032": 60,
        "2033": 60,
        "2034": 60,
        "2035": 60,
        "2036": 60,
        "2037": 60,
        "2038": 60,
        "2039": 60,
        "2040": 60,
        "2041": 0,
        "2042": 0,
        "2043": 0,
        "2044": 0,
        "2045": 0,
        "2046": 0,
        "2047": 0,
        "2048": 0,
        "2049": 0,
        "2050": 0,
    }
    # on garde les paramètres avancés dit aussi "économiques" par défaut
    data["advanced"] = {
        "1": {
            "Gains attribués au chauffage": {"4": 90},
            "Répartition des rénovations par niveau de performance énergétique": {
                "0": 10,
                "1": 10,
                "2": 35,
                "3": 45,
            },
        }
    }
    # paramètres économiques
    data["economique"] = {
        "3": 76.3,
        "4": 100,
        "5": 100,
        "6": 100,
        "7": 100,
        "8": 56.6,
        "9": 56.6,
        "10": 56.6,
        "11": 56.6,
        "12": 62.3,
        "13": 62.3,
        "14": 62.3,
        "15": 62.3,
        "16": 62.3,
    }
    # trajectoires cibles
    data["trajectoires_cibles"] = []

    # on teste d'envoyer tout ça
    _req, resp = client_basic_user.post(
        "/api/auvergne-rhone-alpes/actions",
        params={"zone": "region", "zone_id": "84", "id_utilisateur": -1},
        json=data,
    )
    results = resp.json
    assert resp.status == 200

    def find(lst, key, value):
        for i, dic in enumerate(lst):
            if dic[key] == value:
                return i
        raise ValueError(f"Couldn't find value {value} for key {key} in list {lst}")

    id_resid = find(ref_results["energie_economisee"]["secteur"], "nom", "Résidentiel")
    ref_res_data = ref_results["energie_economisee"]["secteur"][id_resid]["data"]
    res_data = results["energie_economisee"]["secteur"][id_resid]["data"]

    # on vérifie qu'avant 2018 tout va bien
    id_2020 = find(ref_res_data, "annee", 2020)
    for i in range(id_2020 + 1):
        assert round(ref_res_data[i]["valeur"], 3) == round(res_data[i]["valeur"], 3)

    # on vérifie qu'il y a bien eu des améliorations
    id_2029 = find(ref_res_data, "annee", 2029)
    id_2040 = find(ref_res_data, "annee", 2040)
    for i in range(id_2029, id_2040):
        assert round(res_data[i + 1]["valeur"], 3) < round(res_data[i]["valeur"], 3)

    # on vérifie qu'on a une baisse jusqu'après la fin des améliorations
    for i in range(id_2040 + 1, len(res_data) - 1):
        assert round(res_data[i + 1]["valeur"], 3) == round(res_data[i]["valeur"], 3)


def test_export_results_strategy(region, client_anonymous, client_basic_user, loop):
    regional_settings = loop.run_until_complete(
        regions_configuration.get_configuration("auvergne-rhone-alpes")
    )
    export_excel_results = regional_settings[0]["export_excel_results"]

    results = {
        "results": {},
        "title": "",
        "source": "",
        "territory_name": "",
    }

    _request, resp = client_anonymous.post(
        "/api/auvergne-rhone-alpes/strategie/results/export",
        params={"zone": "region", "zone_id": "84", "id_utilisateur": -1},
        json=results,
    )
    # no results given
    assert resp.status == 400

    with open(here / "data" / "results_territorial_strategy.json", "r") as f:
        results = json.load(f)

    _request, resp = client_anonymous.post(
        "/api/auvergne-rhone-alpes/strategie/results/export",
        params={"zone": "region", "zone_id": "84", "id_utilisateur": -1},
        json=results,
    )
    if export_excel_results:
        assert resp.status == 200
    else:
        assert resp.status == 400

    with open(here / "data" / "results_territorial_strategy.json", "r") as f:
        results = json.load(f)
    for key in results["results"]:
        results_ = copy.deepcopy(results)
        del results_["results"][key]
        _request, resp = client_anonymous.post(
            "/api/auvergne-rhone-alpes/strategie/results/export",
            params={
                "zone": "region",
                "zone_id": "84",
                "id_utilisateur": -1,
            },
            json=results_,
        )

        if export_excel_results:
            assert resp.status == 200
            xls = pd.ExcelFile(resp.content, engine="openpyxl")
            _check_excel_results_file(xls, results_)
        else:
            assert resp.status == 400


def _check_excel_results_file(xls_file, results):
    """
    Asserts results are present in file

    Parameters
    ----------
    xls_file : byte streamer
        the main excel file to test
    results : dict
        the data results that need to be checked
    """
    df_params = pd.read_excel(xls_file, xls_file.sheet_names[0])
    assert results["title"] in df_params.values
    assert results["source"] in df_params.values
    assert results["territory_name"] in df_params.values


def test_export_strategy_results_file_management(
    region, client_admin_user, client_basic_user, loop
):
    """
    Tests the management of territorial strategy export template file.
    """
    # try to access template file without right access
    _req, resp = client_basic_user.get("/api/pytest/get/strategie/template/")
    assert resp.status == 401

    # get file with right access
    _req, resp = client_admin_user.get("/api/pytest/get/strategie/template/")
    assert resp.status == 200

    # we check we received an excel file (a priori)
    assert (
        resp.headers["Content-Type"]
        == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    )

    with open(strategy_export.EXCEL_EXPORT_IMPACTS, "rb") as f:
        # get file with right access
        _req, resp = client_admin_user.post(
            "/api/pytest/update/strategie/template/", files={"file": f}
        )
        assert resp.status == 200

    with open(strategy_export.EXCEL_EXPORT_IMPACTS, "rb") as f:
        # get file with right access
        _req, resp = client_admin_user.get("/api/pytest/get/strategie/template/")
        assert resp.status == 200

        # we check the file was uploaded!
        assert f.read() == resp.content


def test_region_list(client_anonymous, loop):
    _request, resp = client_anonymous.get("/api/region")
    assert resp.status == 200
    content = resp.json

    real_data = loop.run_until_complete(regions_configuration.get_configuration())
    assert len(real_data) == len(content)


def test_themes_region(client_anonymous, loop):
    region = "auvergne-rhone-alpes"
    _request, resp = client_anonymous.get(f"/api/{region}/analysis/uithemes")
    assert resp.status == 200
    content = resp.json

    ui_themes = loop.run_until_complete(
        fetch(
            """select COUNT(distinct(ui_theme)) as count from meta.indicateur
        where region = $1""",
            region,
        )
    )
    assert ui_themes[0]["count"] == len(content)


def test_list_all_trajectories(client_anonymous, passage_table):
    region = "auvergne-rhone-alpes"
    _req, resp = client_anonymous.get(f"/api/{region}/pcaet/trajectories/list")
    assert resp.status == 200
    data = resp.json
    assert len(data) > 0


def test_update_pop_up_accueil(region, client_admin_user, client_anonymous):
    """
    Test the ability to update splash screen popup and associated carousel.
    """
    assert settings.has_option(
        "api", "img_pop_up_accueil_path"
    ), "Need to specify img_pop_up_accueil_path parameter (and create corresponding folder) inside terriapi.ini api section."

    path_to_api_file = settings.get("api", "img_pop_up_accueil_path")
    assert os.path.exists(
        path_to_api_file
    ), "Folder specified in img_pop_up_accueil_path doesn't exist."

    data = {
        "texteMarkdown": "This a test **from** the main pytest script.",
        "tableauImg": [],
    }

    # not allowed for anonymous user
    _request, resp = client_anonymous.put("/api/pytest/mise_a_jour_pop_up/", json=data)
    assert resp.status == 401

    # accepted for admin but fails without data
    _req, resp = client_admin_user.put("/api/pytest/mise_a_jour_pop_up/")
    assert resp.status == 400
    _req, resp = client_admin_user.put("/api/pytest/mise_a_jour_pop_up/", json={})
    assert resp.status == 400

    # accepted for admin
    _req, resp = client_admin_user.put("/api/pytest/mise_a_jour_pop_up/", json=data)
    assert resp.status == 200
    response = resp.json
    assert "status" in response and response["status"] == "updated"

    # we add a real image
    data["tableauImg"] = [
        [
            {
                "data_url": "data:image/png;base64,...",
                "file": {},
            }
        ]
    ]
    _req, resp = client_admin_user.put("/api/pytest/mise_a_jour_pop_up/", json=data)
    assert resp.status == 200
    response = resp.json
    assert "status" in response and response["status"] == "updated"

    # testing we have not image as we removed them
    _request, resp = client_anonymous.get("/api/pytest/images_carousel/")
    assert resp.status == 200
    res_img = resp.json
    assert (
        res_img["images"][0][0]["url"] == "/img/img_carousel/pytest_1_1_carrousel.png"
    )


def test_pop_up_carousel(region, client_anonymous):
    """
    Test the ability to retrieve carousel related images
    """
    assert settings.has_option(
        "api", "img_pop_up_accueil_path"
    ), "Need to specify img_pop_up_accueil_path parameter (and create corresponding folder) inside terriapi.ini api section."

    path_to_api_file = settings.get("api", "img_pop_up_accueil_path")
    assert os.path.exists(
        path_to_api_file
    ), "Folder specified in img_pop_up_accueil_path doesn't exist."

    # check we do have access to images carousel api point
    _request, resp = client_anonymous.get("/api/pytest/images_carousel/")
    res = resp.json
    assert resp.status == 200
    assert "images" in res


def test_changelog(region, client_anonymous):
    """
    Test the access to the changelog related information
    """
    _request, resp = client_anonymous.get("/api/changelog")
    assert resp.status == 200
    changelog = resp.json

    assert "content" in changelog
    assert "last_version" in changelog
    assert "last_version_date" in changelog


def test_territorialsynthesis_notes(region, client_anonymous):
    """
    Testing territorialsynthesis main entry point
    """
    _request, resp = client_anonymous.get(
        "/api/auvergne-rhone-alpes/territorialsynthesis/notes/epci/200000172"
    )
    assert resp.status == 200
    error_msg = resp.content
    assert len(error_msg) > 0

    # trying again
    _request, resp = client_anonymous.get(
        "/api/auvergne-rhone-alpes/territorialsynthesis/notes/epci/200000172",
        params={
            "id_utilisateur": -1,
        },
    )
    assert resp.status == 200
    territorialsynthesis_res = resp.json
    assert len(territorialsynthesis_res) > 0


def test_deleting_analysis(
    region, indicators, client_anonymous, client_admin_user, passage_table
):
    """
    Test the ability to change themes order through API
    """

    # initial data
    _request, resp = client_anonymous.get("/api/pytest/analysis/")
    assert resp.status == 200
    initial_analyses = resp.json
    assert len(initial_analyses) > 0
    analysis_to_delete = next(
        i["id"] for i in initial_analyses if i["data"] == "other_table"
    )

    # not allowed for anonymous user
    _request, resp = client_anonymous.delete(
        f"/api/pytest/analysis/{analysis_to_delete}/"
    )
    assert resp.status == 401

    # accepted for admin but fails without data
    _req, resp = client_admin_user.delete(
        f"/api/auvergne-rhone-alpes/analysis/{analysis_to_delete}/"
    )
    assert resp.status == 401

    # accepted for admin but non existing data
    _req, resp = client_admin_user.delete("/api/pytest/analysis/-1/")
    assert resp.status == 404

    # accepted for admin but wrong input
    _req, resp = client_admin_user.delete("/api/pytest/analysis/autre_couche_poi/")
    assert resp.status == 500

    # accepted for admin but data table protected
    protected_ind = next(
        i["id"] for i in initial_analyses if i["data"] == "conso_energetique"
    )
    _req, resp = client_admin_user.delete(f"/api/pytest/analysis/{protected_ind}/")
    assert resp.status == 400
    content = resp.json
    assert "message" in content and "protégé" in content["message"]

    # accepted for admin => but didn't confirm
    _req, resp = client_admin_user.delete(f"/api/pytest/analysis/{analysis_to_delete}/")
    assert resp.status == 200
    content = resp.json
    assert "related_dashboards" in content

    _request, resp = client_anonymous.get("/api/pytest/analysis/")
    assert resp.status == 200
    analyses = resp.json
    assert len(analyses) > 0

    # we check that the layer is not present anymore
    assert analysis_to_delete in [l["id"] for l in analyses]

    # accepted for admin and did confirm
    _req, resp = client_admin_user.delete(
        f"/api/pytest/analysis/{analysis_to_delete}/?confirm=true"
    )
    assert resp.status == 200

    _request, resp = client_anonymous.get("/api/pytest/analysis/")
    assert resp.status == 200
    analyses = resp.json
    assert len(analyses) > 0

    # we check that the layer is not present anymore
    assert analysis_to_delete not in [l["id"] for l in analyses]


def test_deleting_data_table(
    region, indicators, client_anonymous, client_admin_user, passage_table
):
    """
    Test the ability to change themes order through API
    """
    data_table_to_delete = "other_table"

    # not allowed for anonymous user
    _request, resp = client_anonymous.delete(
        f"/api/pytest/analysis/data/{data_table_to_delete}/"
    )
    assert resp.status == 401

    # accepted for admin but fails in wrong region
    _req, resp = client_admin_user.delete(
        f"/api/auvergne-rhone-alpes/analysis/{data_table_to_delete}/"
    )
    assert resp.status == 401

    # accepted for admin but unknown table
    _req, resp = client_admin_user.delete("/api/pytest/analysis/data/autre_couche_poi/")
    assert resp.status == 404

    # accepted for admin but data table protected
    _req, resp = client_admin_user.delete(
        f"/api/pytest/analysis/data/conso_energetique/"
    )
    assert resp.status == 400
    content = resp.json
    assert "message" in content and "protégé" in content["message"]

    # accepted for admin => but didn't confirm
    _req, resp = client_admin_user.delete(
        f"/api/pytest/analysis/data/{data_table_to_delete}/"
    )
    assert resp.status == 200
    content = resp.json
    assert (
        "related_tables" in content
        and content["related_tables"] == "fifth-pytest-analysis"
    )

    # not allowed for anonymous user
    _request, resp = client_anonymous.get("/api/pytest/analysis/data/")
    assert resp.status == 401

    # accepted for admin but fails in wrong region
    _req, resp = client_admin_user.get("/api/auvergne-rhone-alpes/analysis/data/")
    assert resp.status == 401

    # accepted here, retrieve list of data tables and check we didn't delete anythin
    _req, resp = client_admin_user.get("/api/pytest/analysis/data/")
    assert resp.status == 200
    list_tables = resp.json
    assert len(list_tables) > 0
    associated_indicators = next(
        v["indicateurs"] for v in list_tables if v["nom"] == data_table_to_delete
    )
    assert associated_indicators is not None
    associated_indicators = associated_indicators.split(", ")

    # accepted for admin and did confirm
    _req, resp = client_admin_user.delete(
        f"/api/pytest/analysis/data/{data_table_to_delete}/?confirm=true"
    )
    assert resp.status == 200

    _req, resp = client_admin_user.get("/api/pytest/analysis/data/")
    assert resp.status == 200
    list_tables = resp.json
    assert len(list_tables) > 0
    results = [
        v["indicateurs"] for v in list_tables if v["nom"] == data_table_to_delete
    ]
    assert len(results) == 0

    _req, resp = client_admin_user.get("/api/pytest/analysis/")
    assert resp.status == 200
    analyses = resp.json
    assert len(analyses) > 0
    # we retrieve the analyses that were linked to the data table deleted
    # to be sure they were disabled
    corresponding_info = set(
        v["active"] for v in analyses if v["nom"] in associated_indicators
    )
    assert len(corresponding_info) == 1 and corresponding_info.pop() is False


def test_ui_theme_ordering(region, indicators, client_anonymous, client_admin_user):
    """
    Test the ability to change themes order through API
    """
    # retrieving ui themes from database
    _request, resp = client_anonymous.get("/api/pytest/analysis/uithemes/")
    uithemes_data = resp.json
    uithemes = list(map(lambda x: x["label"], uithemes_data))
    assert resp.status == 200

    data = {
        "themes_analyses_ordres": {},
        "themes_ordres": uithemes,
    }
    # not allowed for anonymous user
    _request, resp = client_anonymous.put(
        "/api/pytest/analysis/uitheme/order/", json=data
    )
    assert resp.status == 401

    # accepted for admin but fails without data
    _req, resp = client_admin_user.put("/api/pytest/analysis/uitheme/order/")
    assert resp.status == 400
    # accepted for admin
    _req, resp = client_admin_user.put("/api/pytest/analysis/uitheme/order/", json=data)
    assert resp.status == 200

    # we retrieve analyses from api
    _request, resp = client_anonymous.get("/api/pytest/analysis/")
    assert resp.status == 200
    analyses = resp.json
    assert len(analyses) > 0

    analyses.sort(key=lambda x: x["ordre_ui_theme"])
    new_order_themes = set(map(lambda x: x["ui_theme"], analyses))
    assert new_order_themes == set(uithemes)


def test_indicator_group_renaming(
    region, indicators, client_anonymous, client_admin_user
):
    """
    Test the ability to rename indicator groups through API
    """
    # calling API without rights
    _request, resp = client_anonymous.put("/api/pytest/analysis/uitheme/rename/")
    assert resp.status == 401

    # calling API without rights on this region
    _req, resp = client_admin_user.put(
        "/api/auvergne-rhone-alpes/analysis/uitheme/rename/"
    )
    assert resp.status == 401

    # renaming groups
    _req, resp = client_admin_user.put(
        "/api/pytest/analysis/uitheme/rename/",
        json={
            "old_name": "test_category",
            "new_name": "new_category_name",
        },
    )
    assert resp.status == 200
    uithemes_data = resp.json
    assert "status" in uithemes_data and uithemes_data["status"] == "updated"

    # checking it was updated
    _request, resp = client_anonymous.get("/api/pytest/analysis/uithemes/")
    assert resp.status == 200
    new_data = resp.json
    assert len(new_data) > 0
    assert (
        new_data[0]["label"] == "new_category_name"
        and new_data[1]["label"] == "second_category"
    ) or (
        new_data[1]["label"] == "new_category_name"
        and new_data[0]["label"] == "second_category"
    )


def test_static_files_list(region, static_file, client_anonymous, client_admin_user):
    """
    Test retrieving static files list from API entrypoint. Call the API without rights
    and then with the admin rights. Check that the list does contain the static file
    inserted as fixture by conftest.

    Parameters
    ----------
    region : str
        region fixtures
    static_file : int
        static file fixture ID
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    # we test without rights
    _request, resp = client_anonymous.get("/api/pytest/static/files/")
    assert resp.status == 401

    # real call
    _req, resp = client_admin_user.get("/api/pytest/static/files/")
    assert resp.status == 200

    # checking we do have at least one file
    static_files = resp.json
    assert len(static_files) > 0

    # checking we do have at least the file that was supposed to be added
    assert (
        len(
            [
                file
                for file in static_files.get("data", [])
                if file["file_id"] == static_file
            ]
        )
        == 1
    )


def test_static_file_update(region, static_file, client_anonymous, client_admin_user):
    """
    Test static file update API point. Try to access the API entrypoint without the good
    rights, without parameters and then with a correct file. Check that the file related
    data were correctly updated in the database.

    Will sleep for 1.05 second to be sure to have different update_date after and before.

    Parameters
    ----------
    region : str
        region fixtures
    static_file : int
        static file fixture ID
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    static_file_id = int(static_file)
    # we test without rights
    _request, resp = client_anonymous.put(f"/api/pytest/static/file/{static_file_id}/")
    assert resp.status == 401

    # we test without parameters
    _req, resp = client_admin_user.put(f"/api/pytest/static/file/{static_file_id}/")
    assert resp.status == 400

    # get update date before updating
    _req, resp = client_admin_user.get("/api/pytest/static/files/")
    assert resp.status == 200
    static_files = resp.json
    current_static_file_update_date = next(
        (
            f["update_date"]
            for f in static_files.get("data", [])
            if f["file_id"] == static_file
        )
    )

    # we sleep to make sure we have not same update date before and after
    time.sleep(1.05)

    # we test with parameters
    EMPTY_PDF = here / "data" / "empty.pdf"
    with open(EMPTY_PDF, "rb") as f:
        # get file with right access
        _req, resp = client_admin_user.put(
            f"/api/pytest/static/file/{static_file_id}/", files={"file": f}
        )
        assert resp.status == 200
        static_files = resp.json
        # we check file was clearly updated
        assert static_files["message"] == "Fichier bien mis à jour !"
        assert "new_date" in static_files
        assert datetime.datetime.strptime(
            static_files["new_date"], "%d/%m/%Y, %H:%M:%S"
        ) > datetime.datetime.strptime(
            current_static_file_update_date, "%d/%m/%Y, %H:%M:%S"
        )


def test_analysis_data_table_export(
    region, indicators, client_anonymous, client_admin_user
):
    """
    Test exporting a common dataset table from API entrypoint. Call the API without rights
    and then with the admin rights but with a wrong table name. Will eventually
    call the API with admin rights and correct table name, thus getting a csv
    file.

    Parameters
    ----------
    region : str
        region fixtures
    indicateur : str
        table name created inside pytest schema
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    table_names, _ = indicators
    table_name = table_names[0]
    # we test without rights
    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/data/export/{table_name}/"
    )
    assert resp.status == 401

    # we test with a wrong table name
    _req, resp = client_admin_user.get(
        f"/api/pytest/analysis/data/export/unexisting_{table_name}/"
    )
    assert resp.status == 401
    data = resp.json
    assert "message" in data and "(table inexistante)" in data["message"]

    # real call
    _req, resp = client_admin_user.get(
        f"/api/pytest/analysis/data/export/{table_name}/"
    )
    assert resp.status == 200

    # we check we have (normally a csv file)
    assert resp.content_type == "text/csv"

    # we check we have a non empty file
    data = resp.content
    assert len(data) > 0


def test_region_scenario_list(
    region,
    client_basic_user,
    client_admin_user,
    client_global_admin_user,
    client_anonymous,
    scenario,
):
    """
    Test retrieving the list of all the scenarios of the user's region.

    Parameters
    ----------
    region : str
        region fixtures
    client_basic_user : pytest_sanic.utils.TestClient
        a non admin user (created the scenario, region pytest)
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights (region pytest)
    client_global_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights (region auvergne-rhone-alpes)
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    scenario: dict
        scenario fixture
    """
    scenario_region = "pytest"
    # Test the endpoint with an anonymous user
    _request, resp = client_anonymous.get(f"/api/{scenario_region}/scenarios")
    assert resp.status == 401

    # Test the endpoint with a non admin user
    _req, resp = client_basic_user.get(f"/api/{scenario_region}/scenarios")
    assert resp.status == 401

    # Test the endpoint with an admin user of the correct region
    _req, resp = client_global_admin_user.get(f"/api/{scenario_region}/scenarios")
    assert resp.status == 200
    scenarios = resp.json
    # the scenario is returned
    scenarios = [s for s in scenarios if s["id"] == scenario["scenario_id"]]
    assert len(scenarios) == 1

    # Test with an invalid region
    _req, resp = client_global_admin_user.get(f"/api/notavalidregion/scenarios")
    assert resp.status == 401

    # The scenario doesn't appear for the region pytest (which has no scenarios)
    # resp = client_admin_user.get(f"/api/{scenario_region}/scenarios")
    # assert resp.status == 404

    # Test the endpoint with an admin user of the wrong region (auvergne-rhone-alpes)
    # Can't access scenarios of a different region
    _req, resp = client_admin_user.get(f"/api/auvergne-rhone-alpes/scenarios")
    assert resp.status == 401


def test_region_scenario_empty_list(region, client_basic_user, client_admin_user):
    """
    Test retrieving the list of all the scenarios of the user's region.

    Parameters
    ----------
    region : str
        region fixtures
    client_basic_user : pytest_sanic.utils.TestClient
        a non admin user (created the scenario, region pytest)
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights (region pytest)
    client_global_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights (region auvergne-rhone-alpes)
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    scenario: dict
        scenario fixture
    """
    # The scenario doesn't appear for the region pytest (which has no scenarios)
    _req, resp = client_admin_user.get(f"/api/pytest/scenarios")
    assert resp.status == 404


def test_changing_perimeter_year(
    region, indicators, client_anonymous, client_admin_user, loop
):
    """Test changing (without conversion) the perimeter year of a data table through the API.

    Parameters
    ----------
    region : _type_
        _description_
    indicateur : _type_
        _description_
    client_anonymous : _type_
        A client_basic_user with anonymous user rights
    client_admin_user : _type_
        A client_basic_user with admin user rights
    """

    table_name = "conso_energetique"
    region = "pytest"

    # Change table perimeter to 2023 :

    # Test with an anonymous user
    _request, resp = client_anonymous.put(
        f"/api/{region}/date_perimetre",
        json={"annee_perimetre": "2023", "nom": table_name},
    )
    assert resp.status == 401

    # Test with an admin user of a different region
    _req, resp = client_admin_user.put(
        f"/api/wrongregion/date_perimetre",
        json={"annee_perimetre": "2023", "nom": table_name},
    )
    assert resp.status == 401

    # Test with an admin user of the correct region
    _req, resp = client_admin_user.put(
        f"/api/{region}/date_perimetre",
        json={"annee_perimetre": "2023", "nom": table_name},
    )
    assert resp.status == 200

    # The perimeter date was changed in the database
    new_perimeter_year = loop.run_until_complete(
        get_table_perimeter_year(region, table_name)
    )
    assert new_perimeter_year == 2023

    # The modification was logged to the history table
    query = f"""select count(*) FROM {region}.historique_indicateurs
    where nom_donnee = '{table_name}'
    and autres_informations = 'Nouvelle année : {2023}'
    and "action" = 'Changement de la date du périmètre d''une donnée'
    """
    resp = loop.run_until_complete(fetch(query))
    assert len(resp) == 1


def test_data_table_perimeter_conversion(
    mocker,
    data_for_perimeter_conversion,
    client_admin_user,
    client_anonymous,
    client_basic_user,
    loop,
):
    """Tests the conversion of a valid and convertible data table through the API"""

    table_name = "test_data"
    region = "pytest"
    # Mock to point to 'pytest_table_passage' schema for conversion tables
    mocker.patch.object(
        get_perimeter_conversion_table,
        "__defaults__",
        ("pytest_table_passage",),
    )

    # Without the proper rights (as a normal user)
    _req, resp = client_basic_user.put(
        "/api/pytest/conversion_perimetre",
        json={
            "tableName": table_name,
            "initialPerimeterYear": "2020",
            "targetPerimeterYear": "2022",
            "aggregationFunction": "sum",
        },
    )
    assert resp.status == 401

    # Without the proper rights (as an anonymous user)
    _request, resp = client_anonymous.put(
        "/api/pytest/conversion_perimetre",
        json={
            "tableName": table_name,
            "initialPerimeterYear": "2020",
            "targetPerimeterYear": "2022",
            "aggregationFunction": "sum",
        },
    )
    assert resp.status == 401

    # Try to convert a table from a different region
    _req, resp = client_admin_user.put(
        "/api/other_region/conversion_perimetre",
        json={
            "tableName": table_name,
            "initialPerimeterYear": "2020",
            "targetPerimeterYear": "2022",
            "aggregationFunction": "sum",
        },
    )
    assert resp.status == 401

    # with proper rights
    _req, resp = client_admin_user.put(
        "/api/pytest/conversion_perimetre",
        json={
            "tableName": table_name,
            "initialPerimeterYear": "2020",
            "targetPerimeterYear": "2022",
            "aggregationFunction": "sum",
        },
    )
    assert resp.status == 200

    # The perimeter year was changed
    new_perimeter_year = loop.run_until_complete(
        get_table_perimeter_year(region, table_name)
    )
    assert new_perimeter_year == 2022

    # The modification was logged to the history table
    query = f"""select * FROM {region}.historique_indicateurs
    where nom_donnee = '{table_name}'
    and "action" = 'Conversion des données dans un nouveau périmètre'
    """
    resp = loop.run_until_complete(fetch(query))
    assert len(resp) == 1

    # The table was properly converted and saved to the database.
    # (We only check that we have the correct number of rows. The conversion function is already unit tested in test_perimeter_conversion.py)
    rset = loop.run_until_complete(fetch(f"select count(*) from {region}.{table_name}"))
    n_rows = rset[0][0]
    assert n_rows == 6

    # With the proper rights, but the initial year in the request is different
    # from the perimeter year saved in the database
    _req, resp = client_admin_user.put(
        "/api/pytest/conversion_perimetre",
        json={
            "tableName": table_name,
            "initialPerimeterYear": "2005",
            "targetPerimeterYear": "2022",
            "aggregationFunction": "sum",
        },
    )
    assert resp.status == 409


def test_converting_perimeter_of_non_data_table(
    mocker, data_for_perimeter_conversion, client_admin_user
):
    """Verifies that a random table that isn't configured in meta.perimetre_geographique can't be converted"""

    table_name = "historique_indicateurs"

    # Without the proper rights (as a normal user)
    _req, resp = client_admin_user.put(
        "/api/pytest/conversion_perimetre",
        json={
            "tableName": table_name,
            "initialPerimeterYear": "2020",
            "targetPerimeterYear": "2022",
            "aggregationFunction": "sum",
        },
    )
    assert resp.status == 404


def test_non_convertible_table_perimeter_conversion(
    mocker, data_for_perimeter_conversion, client_admin_user
):
    """Tests that table formats that aren't supported by the conversion function can't be converted :
    - pixel data tables
    - flow data tables
    - tables without a 'commune' column
    """

    for table_name in ["pixel_data", "epci_data"]:
        _req, resp = client_admin_user.put(
            f"/api/pytest/conversion_perimetre",
            json={
                "tableName": table_name,
                "initialPerimeterYear": "2020",
                "targetPerimeterYear": "2022",
                "aggregationFunction": "sum",
            },
        )
        assert resp.status == 400
        json = resp.json
        expected_message = f"La table {table_name} ne peut pas être convertie dans un nouveau périmètre. Elle ne contient pas les colonnes obligatoires (commune, valeur) ou contient des données spéciales de type 'pixel' qui ne sont pas encore gérées par la conversion."
        assert expected_message in json["message"]


def test_data_units_list(region, client_basic_user, data_units):
    _req, resp = client_basic_user.get(f"/api/{region}/data/units/")
    assert resp.status == 200
    content = resp.json
    names = set([u["unit_name"] for u in content])
    ref_names = set([u[2] for u in data_units])
    assert names == ref_names


def test_data_units_use(region, client_basic_user, indicators, data_units):
    _, added_indicators_id = indicators
    params = {
        "zone": "region",
        "maille": "commune",
        "zone_id": "1",
        "provenance": "test",
        "id_utilisateur": -1,
    }

    _req, resp = client_basic_user.post(
        f"/api/{region}/analysis/{added_indicators_id[0]}/data/",
        json={},
        params={"unit": -2, **params},
    )
    assert resp.status == 200
    content_before = resp.json

    # check with GWh
    _req, resp = client_basic_user.post(
        f"/api/{region}/analysis/{added_indicators_id[0]}/data/",
        json={},
        params={"unit": -1, **params},
    )
    assert resp.status == 200
    content_after = resp.json

    # default val is in MWh at commune level => if we choose other unit => GWh
    assert content_before["map"][0]["val"] == 1000.0 * content_after["map"][0]["val"]


def test_data_units_use_with_ratio(region, client_basic_user, indicators, data_units):
    _, added_indicators_id = indicators
    params = {
        "zone": "region",
        "maille": "commune",
        "zone_id": "1",
        "provenance": "test",
        "id_utilisateur": -1,
    }

    # we consult an indicator with a ratio
    _req, resp = client_basic_user.post(
        f"/api/{region}/analysis/{added_indicators_id[9]}/data/",
        json={},
        params={"unit": -2, **params},
    )
    assert resp.status == 200
    content_before = resp.json

    # check with GWh
    _req, resp = client_basic_user.post(
        f"/api/{region}/analysis/{added_indicators_id[9]}/data/",
        json={},
        params={"unit": -1, **params},
    )
    assert resp.status == 200
    content_after = resp.json

    # default val is in MWh at commune level => if we choose other unit => GWh
    assert content_before["map"][0]["val"] == 1000.0 * content_after["map"][0]["val"]
