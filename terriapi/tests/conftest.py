# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import logging

import pytest
import slugify
from sanic import Sanic
from sanic.worker.loader import AppLoader
from sanic_jwt import Initialize
from sanic_testing import TestManager

from terriapi import auth, controller, settings, user
from terriapi.controller.user import create_user
from terriapi.database import cleanup_db, setup_db
from terriapi.server import create_app

from .fixtures.common import SCHEMA
from .fixtures.content import *
from .fixtures.misc import *
from .fixtures.structure import *
from .fixtures.users import *

FORMAT = "%(asctime)s :: %(levelname)s :: %(name)s :: %(funcName)s : %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)

Sanic.test_mode = True


@pytest.fixture(scope="session")
def app():
    loader = AppLoader(factory=create_app)
    app = loader.load()
    app.prepare(dev=True, debug=True, ssl=None)
    yield app


@pytest.fixture
async def db(app, loop):
    controller.db = await setup_db(app, loop, "function")
    yield controller.db
    await cleanup_db(app, loop, "function")


@pytest.fixture(autouse=True)
async def main_structure(app, db):
    """Create the main schema and drops main tables"""
    # async with controller.db.acquire() as conn:
    await controller.execute(f"DROP SCHEMA IF EXISTS {SCHEMA} CASCADE")
    await controller.execute(f"DELETE FROM regions_configuration WHERE id = $1", SCHEMA)

    await controller.execute(f"CREATE SCHEMA {SCHEMA}")

    query_layers = f"""INSERT INTO regions_configuration 
    (env, id, url, export_excel_results, ui_show_plan_actions) 
    VALUES('test', $1, $1, false, true), ('dev', $1, $1, true, true) returning id"""
    await controller.execute(query_layers, SCHEMA)

    yield SCHEMA

    await controller.execute(f"DROP SCHEMA IF EXISTS {SCHEMA} CASCADE")
    await controller.execute(f"DELETE FROM regions_configuration WHERE id = $1", SCHEMA)


@pytest.fixture()
def global_ctx(app, db, main_structure):
    yield True


@pytest.fixture(scope="function", autouse=True)
def configure_logging():
    logger = logging.getLogger("sanic.root")
    handler = logging.StreamHandler()
    formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    yield
    logger.removeHandler(handler)
    handler.close()
