# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
#
from terriapi.controller import fetch

# @app.route(r"/api/<region>/order/zone", methods=["PUT"])
# @app.route(r"/api/<region>/toggle/zone", methods=["PUT"])
# @app.route(r"/api/<region>/delete/zone", methods=["DELETE"])
# @app.route(r"/api/<region>/territorial/levels")
# @app.route(r"/api/<region>/zone/<name:string>")
# @app.route(r"/api/<region:string>/zone/<zone_type:string>/<zone_id:string>/geometry")


def test_zone_list(client_basic_user, zones, loop):
    _req, resp = client_basic_user.get(f"/api/pytest/zone/")
    assert resp.status == 200
    data = resp.json
    metadata = loop.run_until_complete(
        fetch(
            "SELECT * FROM pytest.zone",
        )
    )
    metadata = dict(metadata[0])

    # we test the variables are equal to normal
    for var, value in data[0].items():
        assert metadata[var] == value


def test_territorial_levels(client_basic_user, zones, loop):
    _req, resp = client_basic_user.get(f"/api/pytest/territorial/levels")
    assert resp.status == 200
    data = set(resp.json)
    metadata = loop.run_until_complete(
        fetch(
            "SELECT DISTINCT type_territoire FROM pytest.territoire",
        )
    )
    metadata = set(d["type_territoire"] for d in metadata)

    # we test the variables are equal to normal
    assert metadata == data


def test_get_zone_details(region, client_basic_user, client_admin_user, zones):
    # wrong name
    _req, resp = client_basic_user.get(
        f"/api/pytest/zone/this ' is a try of SQL injection/"
    )
    assert resp.status == 401

    # right level and right key
    _req, resp = client_basic_user.get(f"/api/pytest/zone/region/")
    assert resp.status == 200
    data = resp.json
    assert len(data) > 0


def test_new_zone(region, client_basic_user, client_admin_user, zones, **kwargs):
    fixture_zone = {
        "name": "This is a new zone example for pytest region",
        "level": "departement",
        "maille": "epci",
    }

    _req, resp = client_basic_user.post(f"/api/pytest/add/zone/")
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.post(f"/api/pytest/add/zone/")
    assert resp.status == 404

    _req, resp = client_admin_user.post(f"/api/pytest/add/zone/", json=fixture_zone)
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/zone/")
    assert resp.status == 200
    data = resp.json

    # we check the zone is present and the zone has been added
    libelles = set((a["libelle"] for a in data))
    assert fixture_zone["name"] in libelles


def test_update_zone(region, client_basic_user, client_admin_user, zones, **kwargs):
    existing_link = zones[0]
    existing_libelle = existing_link["libelle"]
    fixture_zone = {
        "old_name": existing_libelle,
        "name": "This is an updated zone for pytest region",
        "level": "departement",
        "maille": "epci",
    }

    _req, resp = client_basic_user.put(f"/api/pytest/edit/zone/", json=fixture_zone)
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.put(f"/api/pytest/edit/zone/")
    assert resp.status == 404

    # wrong existing_libelle
    _req, resp = client_admin_user.put(
        f"/api/pytest/edit/zone/",
        json={**fixture_zone, **{"old_name": "wrong name"}},
    )
    assert resp.status == 400

    _req, resp = client_admin_user.put(f"/api/pytest/edit/zone/", json=fixture_zone)
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/zone/")
    assert resp.status == 200
    data = resp.json

    # we just check the table name has been edited present
    zone_names = set((a["libelle"] for a in data))
    # not present anymore
    assert existing_libelle not in zone_names
    # has been replaced
    assert fixture_zone["name"] in zone_names


def test_delete_zone(region, client_basic_user, client_admin_user, zones, **kwargs):
    existing_link = zones[0]
    existing_libelle = existing_link["libelle"]
    slug = existing_link["nom"] + "-" + existing_link["maille"]

    # wrong auth level
    _req, resp = client_basic_user.delete(f"/api/pytest/delete/zone/" + slug)
    assert resp.status == 401

    # wrong libelle
    _req, resp = client_admin_user.delete(
        f"/api/pytest/delete/zone/" + "not_existing_libelle"
    )
    assert resp.status == 400

    # no param
    _req, resp = client_admin_user.delete(f"/api/pytest/delete/zone/")
    assert resp.status == 404

    _req, resp = client_admin_user.delete(f"/api/pytest/delete/zone/" + slug)
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/zone/")
    assert resp.status == 200
    data = resp.json

    # we check the zone is still present
    libelles = set((a["libelle"] for a in data))
    assert existing_libelle not in libelles


def test_order_zone(region, client_basic_user, client_admin_user, zones, **kwargs):
    existing_link = zones[0]
    existing_libelle = existing_link["libelle"]

    _req, resp = client_admin_user.get(f"/api/pytest/zone/")
    assert resp.status == 200
    data = resp.json

    # we check the zone is well present and ordered
    libelles = [a["libelle"] for a in data]
    assert existing_libelle in libelles
    assert libelles[0] == existing_libelle

    # wrong auth level
    _req, resp = client_basic_user.put(
        f"/api/pytest/order/zone/", json={"name": existing_libelle, "direction": "up"}
    )
    assert resp.status == 401

    # wrong libelle
    _req, resp = client_admin_user.put(
        f"/api/pytest/order/zone/",
        json={"name": "not_existing_libelle", "direction": "up"},
    )
    assert resp.status == 400

    # missing param
    _req, resp = client_admin_user.put(
        f"/api/pytest/order/zone/", json={"name": "not_existing_libelle"}
    )
    assert resp.status == 404

    # no param
    _req, resp = client_admin_user.put(f"/api/pytest/order/zone/")
    assert resp.status == 404

    # wrong direction
    _req, resp = client_admin_user.put(
        f"/api/pytest/order/zone/", json={"name": existing_libelle, "direction": "up"}
    )
    assert resp.status == 400

    # right
    _req, resp = client_admin_user.put(
        f"/api/pytest/order/zone/", json={"name": existing_libelle, "direction": "down"}
    )
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/zone/")
    assert resp.status == 200
    data = resp.json

    # we check the zone is still present
    libelles = [a["libelle"] for a in data]
    assert existing_libelle in libelles
    assert libelles[1] == existing_libelle


def test_toggle_zone(region, client_basic_user, client_admin_user, zones, **kwargs):
    existing_link = zones[0]
    existing_libelle = existing_link["libelle"]
    existing_hide = existing_link["hide"]

    # wrong auth level
    _req, resp = client_basic_user.put(
        f"/api/pytest/toggle/zone/", json={"name": existing_libelle}
    )
    assert resp.status == 401

    # no param
    _req, resp = client_admin_user.put(f"/api/pytest/toggle/zone/")
    assert resp.status == 404

    _req, resp = client_admin_user.put(
        f"/api/pytest/toggle/zone/", json={"name": existing_libelle}
    )
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/zone/")
    assert resp.status == 200
    data = resp.json

    # we check the zone is still present
    libelles = set((a["libelle"] for a in data))
    assert existing_libelle in libelles
    new_data = next(a for a in data if a["libelle"] == existing_libelle)
    assert new_data["hide"] is not existing_hide
