# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import requests_mock

from terriapi import settings
from terriapi.controller import fetch

from .conftest import ALICE, ALICE_IN_AURA, JOHN


def test_auth_user(region, client_anonymous, client_basic_user):
    """
    Test the ability to log in and out a user and get their info if they are connected
    """
    _request, resp = client_anonymous.get("/api/pytest/auth/me")
    assert resp.status == 401

    _request, resp = client_anonymous.post(
        "/api/pytest/auth/",
        json={
            "login": "pytest@terristory-nowhere.dev",
            "password": JOHN["password"],
            "region": "pytest",
        },
    )
    assert resp.status == 200

    # we update the client session
    client_anonymous.set_cookies(resp.json)

    _request, resp = client_anonymous.get("/api/pytest/auth/me")
    assert resp.status == 200
    me = (resp.json)["me"]
    assert me["est_admin"] == (JOHN["profil"] == "admin")
    assert me["prenom"] == JOHN["prenom"]
    assert me["nom"] == JOHN["nom"]
    assert me["mail"] == JOHN["mail"]
    assert me["organisation"] == JOHN["organisation"]
    assert me["fonction"] == JOHN["fonction"]
    assert me["territoire_predilection"] == JOHN["territoire_predilection"]
    assert (
        me["utiliser_territoire_predilection"]
        == JOHN["utiliser_territoire_predilection"]
    )
    assert me["territoire"] == JOHN["territoire"]

    _request, resp = client_anonymous.get("/api/pytest/auth/logout")
    assert resp.status == 200

    # we update the client session to "logout" (cf. logics of JWT authentication, one cannot be really logged out until the end of the validity of the tokens)
    client_anonymous.clear_cookies()

    _request, resp = client_anonymous.get("/api/pytest/auth/me")
    assert resp.status == 401


def test_get_user(client_basic_user, client_admin_user):
    # do not provide login
    _req, resp = client_basic_user.get("/api/user/pytest/get")
    assert resp.status == 400

    # provide login but wrong login
    _req, resp = client_basic_user.get(
        "/api/user/pytest/get",
        params={"login": "not_existing_pytest@terristory-nowhere.dev"},
    )
    assert resp.status == 404

    # provide login but wrong auth
    _req, resp = client_basic_user.get(
        "/api/user/pytest/get", params={"login": "alice@terristory-wonder.land"}
    )
    assert resp.status == 401

    # right auth and right login
    _req, resp = client_basic_user.get(
        "/api/user/pytest/get", params={"login": "pytest@terristory-nowhere.dev"}
    )
    assert resp.status == 200
    data = resp.json
    assert data.get("mail", "Wrong") == "pytest@terristory-nowhere.dev"

    # check admin can also do it
    _req, resp = client_admin_user.get(
        "/api/user/pytest/get", params={"login": "pytest@terristory-nowhere.dev"}
    )
    assert resp.status == 200
    data = resp.json
    assert data.get("mail", "Wrong") == "pytest@terristory-nowhere.dev"


def test_get_users(region, client_basic_user, client_admin_user):
    # wrong auth level
    _req, resp = client_basic_user.get("/api/user/pytest/list")
    assert resp.status == 401

    # right auth level, wrong region
    _req, resp = client_admin_user.get("/api/user/auvergne-rhone-alpes/list")
    assert resp.status == 401

    # right auth level
    _req, resp = client_admin_user.get("/api/user/pytest/list")
    assert resp.status == 200
    data = resp.json
    assert len(data) > 0


def test_create_delete_user(
    region, client_anonymous, client_basic_user, client_admin_user, mocker, loop
):
    loop.run_until_complete(
        fetch(
            "DELETE FROM utilisateur WHERE mail='created.user@terristory-nowhere.dev'"
        )
    )
    # try creating an account without captcha
    _request, resp = client_anonymous.post(
        f"/api/user/{region}/create",
        json=JOHN,
    )
    assert resp.status == 401

    def fake_settings(group, setting):
        if setting == "captcha_secret_key":
            return "fake_secret_key"
        else:
            return "dev"

    with requests_mock.Mocker() as mock:
        mocker.patch("terriapi.settings.get", side_effect=fake_settings)
        mock.post(
            "https://www.google.com/recaptcha/api/siteverify?secret=fake_secret_key&response=fake_token",
            json={"success": True},
            status_code=200,
        )

        # try creating an already existing account
        _request, resp = client_anonymous.post(
            f"/api/user/{region}/create",
            json={**JOHN, "captcha_token": "fake_token"},
        )
        assert resp.status == 400

        _request, resp = client_anonymous.post(
            "/api/user/pytest/create",
            json={
                "mail": "created.user@terristory-nowhere.dev",
                "prenom": "Created",
                "nom": "User",
                "organisation": "org",
                "fonction": "func",
                "territoire_predilection": '{"zoneId":"region","zoneMaille":"epci"}',
                "utiliser_territoire_predilection": False,
                "territoire": "01004",
                "rgpd": True,
                "active": True,
                "actif": True,
                "captcha_token": "fake_token",
            },
        )
        assert resp.status == 200

    # Check the creation of an inactive account
    _req, resp = client_admin_user.get(
        "/api/user/pytest/get", params={"login": "created.user@terristory-nowhere.dev"}
    )
    assert resp.status == 200
    data = resp.json
    assert data["mail"] == "created.user@terristory-nowhere.dev"
    assert data["actif"] == False

    # Delete the newly created user with users without auth
    _req, resp = client_anonymous.delete(
        "/api/user/pytest/delete/created.user@terristory-nowhere.dev/"
    )
    assert resp.status == 401
    _req, resp = client_basic_user.delete(
        "/api/user/pytest/delete/created.user@terristory-nowhere.dev/"
    )
    assert resp.status == 401
    # Delete the newly created user
    _req, resp = client_admin_user.delete(
        "/api/user/pytest/delete/unexisting-user-in-pytest-region/"
    )
    assert resp.status == 404
    _req, resp = client_admin_user.delete(
        "/api/user/pytest/delete/created.user@terristory-nowhere.dev/"
    )
    assert resp.status == 200
    _req, resp = client_admin_user.get(
        "/api/user/pytest/get", params={"login": "created.user@terristory-nowhere.dev"}
    )
    assert resp.status == 404


def test_create_delete_user_as_admin(
    region, client_anonymous, client_basic_user, client_admin_user, loop
):
    loop.run_until_complete(
        fetch(
            "DELETE FROM utilisateur WHERE mail='created.user@terristory-nowhere.dev'"
        )
    )
    # without rights
    _request, resp = client_anonymous.post(
        f"/api/user/{region}/admin/create",
    )
    assert resp.status == 401
    _req, resp = client_basic_user.post(
        f"/api/user/{region}/admin/create",
    )
    assert resp.status == 401

    _req, resp = client_admin_user.post(
        "/api/user/pytest/admin/create",
        json={
            "mail": "created.user@terristory-nowhere.dev",
            "prenom": "Created",
            "nom": "User",
            "organisation": "org",
            "fonction": "func",
            "territoire_predilection": '{"zoneId":"region","zoneMaille":"epci"}',
            "utiliser_territoire_predilection": False,
            "territoire": "01004",
            "profil": "utilisateur",
            "rgpd": True,
            "actif": True,
        },
    )
    assert resp.status == 200

    # Check the creation of an inactive account
    _req, resp = client_admin_user.get(
        "/api/user/pytest/get", params={"login": "created.user@terristory-nowhere.dev"}
    )
    assert resp.status == 200
    data = resp.json
    assert data["mail"] == "created.user@terristory-nowhere.dev"
    assert data["actif"] == True


def test_update_user(
    region, client_anonymous, client_basic_user, client_admin_user, loop
):
    loop.run_until_complete(
        fetch(
            "DELETE FROM utilisateur WHERE mail='created.user@terristory-nowhere.dev'"
        )
    )
    # without data
    _req, resp = client_admin_user.put(
        f"/api/user/{region}/update",
    )
    assert resp.status == 400
    # with data but no matching login
    _req, resp = client_admin_user.put(
        f"/api/user/{region}/update",
        json={"login": "unexisting-account-in-pytest-region"},
    )
    assert resp.status == 404
    # with data but no matching login
    _req, resp = client_basic_user.put(
        f"/api/user/{region}/update",
        json={"login": ALICE["mail"]},
    )
    assert resp.status == 401

    # Update with right user
    _req, resp = client_basic_user.put(
        "/api/user/pytest/update",
        json={
            "login": JOHN["mail"],
            "prenom": "Created",
            "nom": "User",
            "organisation": "org",
            "fonction": "func",
            "territoire_predilection": '{"zoneId":"region","zoneMaille":"epci"}',
            "utiliser_territoire_predilection": False,
            "territoire": "01004",
            "rgpd": True,
            "active": True,
            "actif": True,
        },
    )
    assert resp.status == 200
    # Update with admin
    _req, resp = client_admin_user.put(
        "/api/user/pytest/update",
        json={
            "login": JOHN["mail"],
            "prenom": "Created",
            "nom": "User",
            "organisation": "org",
            "fonction": "func",
            "territoire_predilection": '{"zoneId":"region","zoneMaille":"epci"}',
            "utiliser_territoire_predilection": False,
            "territoire": "01004",
            "rgpd": True,
            "active": True,
            "actif": True,
        },
    )
    assert resp.status == 200


def test_activate_disactivate_user(
    region,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    client_basic_user_in_aura,
):
    # Deactivate the account
    _req, resp = client_admin_user.post(
        "/api/user/pytest/deactivate",
        json={"login": "pytest@terristory-nowhere.dev"},
    )
    assert resp.status == 200
    _request, resp = client_anonymous.post(
        "/api/pytest/auth/",
        json={
            "login": "pytest@terristory-nowhere.dev",
            "password": JOHN["password"],
            "region": "pytest",
        },
    )
    assert resp.status == 401

    # without data
    _req, resp = client_admin_user.post(
        "/api/user/pytest/activate",
    )
    assert resp.status == 400

    # unexisting login
    _req, resp = client_admin_user.post(
        "/api/user/pytest/activate", json={"login": "unexisting-login-in-pytest-region"}
    )
    assert resp.status == 404

    # Activate the account
    _req, resp = client_admin_user.post(
        "/api/user/pytest/activate", json={"login": "pytest@terristory-nowhere.dev"}
    )
    assert resp.status == 200
    _req, resp = client_admin_user.get(
        "/api/user/pytest/get", params={"login": "pytest@terristory-nowhere.dev"}
    )
    assert resp.status == 200
    data = resp.json
    assert data["actif"] == True

    # Deactivate the account
    _req, resp = client_admin_user.post(
        "/api/user/pytest/deactivate",
    )
    assert resp.status == 400
    _req, resp = client_admin_user.post(
        "/api/user/pytest/deactivate", json={"login": "unexisting-login-pytest-region"}
    )
    assert resp.status == 404
    _req, resp = client_admin_user.post(
        "/api/user/auvergne-rhone-alpes/deactivate",
        json={"login": ALICE_IN_AURA["mail"]},
    )
    assert resp.status == 401


def test_change_password(
    region, client_anonymous, client_basic_user, client_admin_user
):
    # Deactivate the account
    _request, resp = client_anonymous.post(
        "/api/user/pytest/setpassword",
    )
    assert resp.status == 401

    # empty query
    _req, resp = client_basic_user.post(
        "/api/user/pytest/setpassword",
    )
    assert resp.status == 400
    # empty password
    _req, resp = client_basic_user.post(
        "/api/user/pytest/setpassword",
        json={"new_password": ""},
    )
    assert resp.status == 400

    _req, resp = client_basic_user.post(
        "/api/user/pytest/setpassword",
        json={"new_password": "this_is_an_empty_password"},
    )
    assert resp.status == 200
    _req, resp = client_admin_user.post(
        "/api/user/pytest/setpassword",
        json={"new_password": "this_is_an_empty_password"},
    )
    assert resp.status == 200


def test_forgotten_password(
    region, client_anonymous, client_basic_user, client_admin_user
):
    # Deactivate the account
    _request, resp = client_anonymous.post(
        "/api/user/pytest/password",
    )
    assert resp.status == 400

    _request, resp = client_anonymous.post(
        "/api/user/pytest/password",
        json={"mail": "unexisting-login-in-pytest-region"},
    )
    assert resp.status == 404

    _request, resp = client_anonymous.post(
        "/api/user/pytest/password",
        json={"mail": "pytest@terristory-nowhere.dev"},
    )
    assert resp.status == 200


# TODO: def test_edit_user():
