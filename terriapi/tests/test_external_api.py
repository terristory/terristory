# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
#
import requests_mock
import slugify

from terriapi.utils import parse_table_name


def test_get_external_apis(region, client_basic_user, client_admin_user, external_apis):
    _req, resp = client_basic_user.get(f"/api/pytest/get/external/api/list/")
    assert resp.status == 401

    _req, resp = client_admin_user.get(f"/api/pytest/get/external/api/list/")
    assert resp.status == 200
    data = resp.json
    slugs = set((a["slug"] for a in data))
    assert slugs == set((a["slug"] for a in external_apis.values()))


def test_call_get_external_api(
    region, client_basic_user, client_admin_user, external_apis, **kwargs
):
    json_get_api = external_apis["normal-test"]

    # wrong user
    _req, resp = client_basic_user.get(
        f"/api/pytest/call/external/api/{json_get_api['slug']}/"
    )
    assert resp.status == 401

    # wrong slug
    _req, resp = client_admin_user.get(
        f"/api/pytest/call/external/api/not_existing_slug/"
    )
    assert resp.status == 404

    with requests_mock.Mocker() as mock:
        full_response = [{"test": "full_response", "col": 10.20, "annee": 2020}]
        mock.get(
            json_get_api["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{json_get_api['slug']}/"
        )
        assert resp.status == 200
        data = resp.json
        assert data.get("status", "failed") == "success"
        assert data.get("full_response", {}) == full_response

    csv_get_api = external_apis["csv-get-test"]

    with requests_mock.Mocker() as mock:
        full_response = """test;col;annee
other_epci;22;2020
full_response;10.20;2020"""
        mock.get(
            csv_get_api["url"],
            text=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{csv_get_api['slug']}/"
        )
        assert resp.status == 200
        data = resp.json
        assert data.get("status", "failed") == "success"
        assert data.get("full_response", {}) == [
            {"test": "other_epci", "col": "22", "annee": "2020"},
            {"test": "full_response", "col": "10.20", "annee": "2020"},
        ]

    subkeys_api = external_apis["subkeys-test"]

    with requests_mock.Mocker() as mock:
        full_response = {
            "test": {"next": [{"test": "full_response", "col": 10.20, "annee": 2020}]}
        }
        mock.get(
            subkeys_api["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{subkeys_api['slug']}/"
        )
        assert resp.status == 200
        data = resp.json
        assert data.get("status", "failed") == "success"
        assert data.get("full_response", {}) == full_response


def test_filters_call_get_external_api(
    region, client_basic_user, client_admin_user, external_apis, **kwargs
):
    json_get_api = external_apis["filters-test"]

    with requests_mock.Mocker() as mock:
        full_response = [
            {"departement": "rhone", "test": "1", "col": 10.20, "annee": 2020},
            {"departement": "rhone", "test": "2", "col": 20.20, "annee": 2020},
            {"departement": "isere", "test": "3", "col": 30.20, "annee": 2020},
        ]
        mock.get(
            json_get_api["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{json_get_api['slug']}/"
        )
        assert resp.status == 200
        data = resp.json
        assert data.get("status", "failed") == "success"
        assert data.get("full_response", {}) == full_response


def test_missing_params_call_external_api(
    region, client_basic_user, client_admin_user, external_apis, **kwargs
):
    no_data_table = external_apis["no-data-table"]

    with requests_mock.Mocker() as mock:
        full_response = [{"test": "full_response", "col": 10.20, "annee": 2020}]
        mock.get(
            no_data_table["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{no_data_table['slug']}/"
        )
        assert resp.status == 400
        data = resp.json
        assert data.get("status", "success") == "failed"
        assert (
            data.get("message", "") == "Aucune table de donnée définie pour cette API"
        )

    with requests_mock.Mocker() as mock:
        mock.get(
            no_data_table["url"],
            json={},
            status_code=404,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{no_data_table['slug']}/"
        )
        assert resp.status == 400
        data = resp.json
        assert data.get("status", "success") == "failed"
        assert data.get("message", "") == "La requête a échoué !"

    wrong_format = external_apis["wrong-format"]

    with requests_mock.Mocker() as mock:
        full_response = [{"test": "full_response", "col": 10.20, "annee": 2020}]
        mock.get(
            wrong_format["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{wrong_format['slug']}/"
        )
        assert resp.status == 400
        data = resp.json
        assert data.get("status", "success") == "failed"
        assert (
            data.get("message", "")
            == "Le format de sortie n'est pas autorisé (application/json ou text/csv)."
        )

    wrong_geographical_col = external_apis["invalid-geographic-column"]

    with requests_mock.Mocker() as mock:
        full_response = [{"test": "full_response", "col": 10.20, "annee": 2020}]
        mock.get(
            wrong_geographical_col["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{wrong_geographical_col['slug']}/"
        )
        assert resp.status == 400
        data = resp.json
        assert data.get("status", "success") == "failed"
        assert data.get("message", "") == "L'échelle géographique n'est pas valide."

    wrong_method = external_apis["wrong-method"]

    with requests_mock.Mocker() as mock:
        full_response = [{"test": "full_response", "col": 10.20, "annee": 2020}]
        mock.get(
            wrong_method["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{wrong_method['slug']}/"
        )
        assert resp.status == 400
        data = resp.json
        assert data.get("status", "success") == "failed"
        assert (
            data.get("message", "")
            == "La méthode de requête n'est pas autorisée (GET ou POST)."
        )


def test_call_post_external_api(region, client_admin_user, external_apis, **kwargs):
    post_api = external_apis["post-test"]

    with requests_mock.Mocker() as mock:
        full_response = [{"test": "full_response", "col": 10.20, "annee": 2020}]
        mock.post(
            post_api["url"],
            json=full_response,
            status_code=200,
        )

        _req, resp = client_admin_user.get(
            f"/api/pytest/call/external/api/{post_api['slug']}/"
        )
        assert resp.status == 200
        data = resp.json
        assert data.get("status", "failed") == "success"
        assert data.get("full_response", {}) == full_response


def test_new_external_api(
    region, client_basic_user, client_admin_user, external_apis, **kwargs
):
    fixture_api = {
        "region": "pytest",
        "name": "This is a fake title",
        "actor": "PyTEST",
        "url": "http://this.does.not.exist.really",
        "key": "patate",
        "format": "json",
        "method": "GET",
        "table_name": "Fake title not slugified with é characters and too long!",
        "perimeter_year": "2022",
        "details_column": "test",
        "subkeys": "",
        "geographical_column": "epci",
        "post_data": "{}",
        "columns": '{"col": "valeur", "test": "commune", "annee": "annee"}',
        "filters": "{}",
        "date_maj": "2023-04-20",
    }

    _req, resp = client_basic_user.post(f"/api/pytest/external/api/new/")
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.post(f"/api/pytest/external/api/new/")
    assert resp.status == 400

    _req, resp = client_admin_user.post(
        f"/api/pytest/external/api/new/", json=fixture_api
    )
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/get/external/api/list/")
    assert resp.status == 200
    data = resp.json

    # we check the slug is present and the API link has been added
    slugs = set((a["slug"] for a in data))
    slug_fixture = slugify.slugify(fixture_api["name"])
    assert slug_fixture in slugs

    # we just check the table name is present
    table_names = set((a["table_name"] for a in data))
    table_name = parse_table_name(fixture_api["table_name"])
    assert table_name in table_names


def test_update_external_api(
    region, client_basic_user, client_admin_user, external_apis, **kwargs
):
    fixture_api = {
        "region": "pytest",
        "name": "This is a fake title",
        "actor": "PyTEST",
        "url": "http://this.does.not.exist.really",
        "key": "patate",
        "format": "json",
        "method": "GET",
        "table_name": "Fake title not slugified with é characters and too long!",
        "perimeter_year": "2022",
        "details_column": "test",
        "subkeys": "",
        "geographical_column": "epci",
        "post_data": "{}",
        "columns": '{"col": "valeur", "test": "commune", "annee": "annee"}',
        "filters": "{}",
        "date_maj": "2023-04-20",
    }
    existing_link = external_apis["normal-test"]
    slug = slugify.slugify(existing_link["name"])

    _req, resp = client_basic_user.put(f"/api/pytest/external/api/update/{slug}/")
    assert resp.status == 401

    # missing input data
    _req, resp = client_admin_user.put(f"/api/pytest/external/api/update/{slug}/")
    assert resp.status == 400

    # wrong slug
    _req, resp = client_admin_user.put(
        f"/api/pytest/external/api/update/not_existing_slug/", json=fixture_api
    )
    assert resp.status == 404

    _req, resp = client_admin_user.put(
        f"/api/pytest/external/api/update/{slug}/", json=fixture_api
    )
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/get/external/api/list/")
    assert resp.status == 200
    data = resp.json

    # we check the slug is still present
    slugs = set((a["slug"] for a in data))
    assert slug in slugs

    # we just check the table name has been edited present
    table_names = set((a["table_name"] for a in data))
    table_name = parse_table_name(fixture_api["table_name"])
    assert table_name in table_names


def test_delete_external_api(
    region, client_basic_user, client_admin_user, external_apis, **kwargs
):
    existing_link = external_apis["normal-test"]
    slug = slugify.slugify(existing_link["name"])

    _req, resp = client_basic_user.delete(f"/api/pytest/external/api/delete/{slug}/")
    assert resp.status == 401

    # wrong slug
    _req, resp = client_admin_user.delete(
        f"/api/pytest/external/api/delete/not_existing_slug/"
    )
    assert resp.status == 404

    _req, resp = client_admin_user.delete(f"/api/pytest/external/api/delete/{slug}/")
    assert resp.status == 200

    _req, resp = client_admin_user.get(f"/api/pytest/get/external/api/list/")
    assert resp.status == 200
    data = resp.json

    # we check the slug is still present
    slugs = set((a["slug"] for a in data))
    assert slug not in slugs


# we test the parse table name function
# even if it was already tested in external_api tests
# as it could be disabled
def test_parse_table_name():
    assert parse_table_name("test") == "test"
    assert parse_table_name("test&&&") == "test"
    assert parse_table_name("te-st-with-dash") == "te_st_with_dash"
    assert parse_table_name("try to ' insert code") == "try_to_insert_code"
    long_str = "varchar_way_too_long_to_be_a_fittable_table_name"
    assert parse_table_name(long_str) == long_str[0:40]
