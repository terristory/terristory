# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pytest

from terriapi import controller

from ..common import SCHEMA


@pytest.fixture
async def poi():
    """Création d'une table de données d'un POI dans le schéma de test 'pytest'.

    Renvoie le nom de la table.
    """
    query_layers = f"""create table {SCHEMA}_poi.layer (
        id SERIAL NOT NULL,
        nom character varying UNIQUE,
        label character varying,
        couleur character varying,
        modifiable boolean,
        theme character varying,
        type_installation character varying,
        type_geom character varying,
        statut boolean,
        ancrage_icone character varying,
        theme_order integer DEFAULT 0,
        order_in_theme integer DEFAULT 0,
        donnees_exportables boolean,
        credits_data_sources character varying,
        credits_data_producers character varying
    )"""
    query_layers_structure_constraints = f"""CREATE TABLE {SCHEMA}_poi.layers_structure (
            layer_name varchar NOT NULL,
            field_name varchar NOT NULL,
            field_type varchar NULL DEFAULT 'text'::character varying,
            details json NULL,
            CONSTRAINT layers_structure_pkey PRIMARY KEY (layer_name, field_name),
            CONSTRAINT layers_structure_layer_name_fkey FOREIGN KEY (layer_name) REFERENCES {SCHEMA}_poi.layer(nom) ON DELETE CASCADE ON UPDATE CASCADE
        );"""
    query_historic = f"""
    CREATE TABLE IF NOT EXISTS {SCHEMA}_poi.historique
    (
        id integer NOT NULL,
        layer character varying NOT NULL,
        mail character varying,
        action character varying,
        properties_precedent json,
        properties_courant json,
        geom_precedent geometry(Point,3857),
        geom_courant geometry(Point,3857),
        mise_a_jour timestamp without time zone,
        region character varying
    )"""
    update_meta = f"""INSERT INTO {SCHEMA}_poi.layer 
        (nom, label, couleur, modifiable, theme,
            type_installation, type_geom, statut, ancrage_icone, theme_order, order_in_theme, donnees_exportables)
        VALUES ('exemple_poi', 'Réseaux de chaleur', '#FF3232', true, 'Infrastructures',
            '', 'Point', true, '', 0, 0, true),
            ('exemple_poi_non_modifiables', 'Réseaux de chaleur non modifiable', '#FF3232', false, 'Infrastructures',
            '', 'Point', true, '', 0, 1, true),
            ('autre_couche_poi', 'Infrastructures cyclables', '#02FFBE', false, 'Mobilité',
            '', 'Line', true, '', 1, 0, true)
      returning id"""

    tablename_editable = "exemple_poi"
    query_editable = f"""create table {SCHEMA}_poi.{tablename_editable} (
        id SERIAL NOT NULL,
        properties json,
        geom geometry(Point, 3857)
    )"""
    tablename_non_editable = "exemple_poi_non_modifiables"
    query_not_editable = f"""create table {SCHEMA}_poi.{tablename_non_editable} (
        id SERIAL NOT NULL,
        properties json,
        geom geometry(Point, 3857)
    )"""
    tablename_non_linear = "autre_couche_poi"
    query_non_linear = f"""create table {SCHEMA}_poi.{tablename_non_linear} (
        id SERIAL NOT NULL,
        properties json,
        geom geometry(LineString, 3857)
    )"""
    await controller.execute(f"DROP SCHEMA IF EXISTS {SCHEMA}_poi CASCADE")
    await controller.execute(f"create schema {SCHEMA}_poi")
    await controller.execute(query_layers)
    await controller.execute(query_layers_structure_constraints)
    await controller.execute(query_historic)
    await controller.execute(query_editable)
    await controller.execute(query_not_editable)
    await controller.execute(query_non_linear)
    rset = await controller.fetch(update_meta)
    poi_layers_id = [r["id"] for r in rset]
    yield tablename_editable, poi_layers_id
    await controller.execute(
        f"DELETE FROM meta.poi_proposed_changes WHERE region = $1", "pytest"
    )
    await controller.execute(
        "DELETE FROM meta.poi_contributions_contacts WHERE region = $1", "pytest"
    )
    await controller.execute(f"DROP SCHEMA {SCHEMA}_poi cascade")


@pytest.fixture
async def poi_structure_constraints(poi):
    # TODO: remove date de modification field if having a constraint implies the field is required
    query = f"""INSERT INTO {SCHEMA}_poi.layers_structure VALUES
        ('exemple_poi', 'Constrained field', 'select', '["Accepted value", "Other accepted value"]'::json),
        ('exemple_poi', 'Date de modification', 'date', '{{}}'::json);"""
    await controller.execute(query)
    yield "exemple_poi"
    await controller.fetch(f"DELETE FROM {SCHEMA}_poi.layers_structure;")
