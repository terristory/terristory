# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json

import pytest

from terriapi import controller

from ..common import SCHEMA


@pytest.fixture
def didactic_file_logo_sources(loop):
    """
    Insert a new static file for region pytest inside `meta.data_source_logo` table.
    """

    query = """insert into meta.data_source_logo (url, name, logo_path, extension, region) 
    VALUES ('www.terristory.fr', 'Ceci est un logo', '/tmp/test.png', 'bmp', 'pytest')
      returning id"""
    rset = loop.run_until_complete(controller.fetch(query))

    yield rset

    loop.run_until_complete(
        controller.fetch("DELETE FROM meta.data_source_logo where region = 'pytest'")
    )


@pytest.fixture
def didactic_file_actions(loop):
    """
    Insert a new fake action for region pytest inside `strategie_territoire.action_details` table.
    """

    query = f"""insert into strategie_territoire.action_details 
    (naf, numero, type, type_hyp, 
    parametres, phase_projet, maillon, valeur,
    cout_global, unite, part_francaise, id_source, remuneration, categorie,
    category_class, action_eco, category_order, action_order, regions) 
    VALUES (393, '4b', 'ACTION EXEMPLE', 'maintenance', 
    NULL, 'Phase de production', 'Exploitation / maintenance', NULL, 
    12, '€/m² de capteur', 1.0, 11, 0, 'Actions Production renouvelable',
    'prod_renouv', NULL, 5, 5, $1)
      returning type"""
    rset = loop.run_until_complete(controller.fetch(query, [SCHEMA]))

    yield rset[0]["type"]

    loop.run_until_complete(
        controller.fetch(
            f"DELETE FROM strategie_territoire.action_details where regions = $1",
            [SCHEMA],
        )
    )


@pytest.fixture
def didactic_file_list(loop, didactic_file_actions):
    """
    Insert a new static file for region pytest inside `meta.didactic_file` table.
    """
    files = [
        {
            "metadata": {
                "title": "Titre 1",
                "description": "Description 1",
            },
            "categories": [
                {
                    "title": "Category 3 for file 1",
                    "category_order": 2,
                    "subcategories": [
                        {
                            "title": "Sub category 2",
                            "type": "actions",
                            "subcategory_order": 1,
                            "section": [
                                {
                                    "title": "Section 1 for sub-category 2",
                                    "type": "analysis",
                                    "comment": "COMMENT",
                                    "section_order": 1,
                                    "analysis": {
                                        "ANALYSIS EXEMPLE": "ANALYSIS EXEMPLE"
                                    },
                                },
                                {
                                    "title": "Section 2 for sub-category 2",
                                    "type": "analysis",
                                    "comment": "COMMENT",
                                    "section_order": 2,
                                    "analysis": {
                                        "ANALYSIS EXEMPLE": "ANALYSIS EXEMPLE"
                                    },
                                },
                            ],
                        },
                        {
                            "title": "Sub category 1",
                            "type": "actions",
                            "subcategory_order": 0,
                            "section": [
                                {
                                    "title": "Section 1 for sub category 1",
                                    "type": "action",
                                    "comment": "COMMENT",
                                    "section_order": 1,
                                    "analysis": {"ACTIONS EXEMPLE": "ACTIONS EXEMPLE"},
                                }
                            ],
                        },
                    ],
                },
                {
                    "title": "Category 1 for file 1",
                    "category_order": 0,
                    "subcategories": [],
                },
                {
                    "title": "Category 2 for file 1",
                    "category_order": 1,
                    "subcategories": [
                        {
                            "title": "Sub category 1",
                            "type": "actions",
                            "subcategory_order": 0,
                        },
                    ],
                },
            ],
        },
        {
            "metadata": {
                "title": "Titre 2",
                "description": "Description 2",
            },
            "categories": [
                {
                    "title": "Category 1 for file 2",
                    "category_order": 0,
                    "subcategories": [
                        {
                            "title": "Sub category 1",
                            "type": "actions",
                            "subcategory_order": 0,
                        },
                    ],
                },
                {
                    "title": "Category 2 for file 2",
                    "category_order": 1,
                    "subcategories": [
                        {
                            "title": "Sub category 1",
                            "type": "actions",
                            "subcategory_order": 0,
                        },
                    ],
                },
            ],
        },
    ]

    files_id = []
    for file in files:
        query = """insert into meta.didactic_file (title, description, region) 
        VALUES ($1, $2, 'pytest') returning id"""
        rset = loop.run_until_complete(
            controller.fetch(
                query,
                file["metadata"]["title"],
                file["metadata"]["description"],
            )
        )

        file_id = rset[0]["id"]
        file_data = {"id": file_id, "metadata": file["metadata"], "categories": []}

        for category in file["categories"]:
            query_categories = """insert into meta.didactic_file_category (title, didactic_file_id, category_order) 
            VALUES ($1, $2, $3) returning id"""
            rset_categories = loop.run_until_complete(
                controller.fetch(
                    query_categories,
                    category["title"],
                    file_id,
                    category["category_order"],
                )
            )
            cat_id = rset_categories[0]["id"]
            subcategories = []
            for subcategory in category["subcategories"]:
                query_subcategories = """insert into meta.didactic_file_sub_category 
                (title, type, category_id, subcategory_order) 
                VALUES ($1, $2, $3, $4) returning id"""
                rset_subcategories = loop.run_until_complete(
                    controller.fetch(
                        query_subcategories,
                        subcategory["title"],
                        subcategory["type"],
                        cat_id,
                        subcategory["subcategory_order"],
                    )
                )
                subcat_id = rset_subcategories[0]["id"]
                sections = []
                if "section" in subcategory:
                    for section in subcategory["section"]:
                        sql_sub_cat = """
                            INSERT INTO meta.didactic_file_section
                            (title, comment, type, analysis, section_order, subcategory_id)
                            VALUES ($1, $2, $3, $4, $5, $6) returning id
                            """
                        rset_sections = loop.run_until_complete(
                            controller.fetch(
                                sql_sub_cat,
                                section["title"],
                                section["comment"],
                                section["type"],
                                json.dumps(section["analysis"]),
                                section["section_order"],
                                subcat_id,
                            )
                        )

                        sections.append(
                            {"id": rset_sections[0]["id"], "metadata": section}
                        )
                subcategories.append(
                    {"id": subcat_id, "metadata": subcategory, "section": sections}
                )
            file_data["categories"].append(
                {"id": cat_id, "metadata": category, "subcategories": subcategories}
            )
        files_id.append(file_data)

    yield files_id

    loop.run_until_complete(
        controller.fetch("DELETE FROM meta.didactic_file where region = 'pytest'")
    )
