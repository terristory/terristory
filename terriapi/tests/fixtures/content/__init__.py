# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
import os

import pytest
import slugify

from terriapi import controller, settings

from ..common import here
from .dashboard import *
from .didactic import *
from .indicators import *
from .poi import *
from .strategy import *


@pytest.fixture
async def static_file():
    """
    Insert a new static file for region pytest inside `meta.static_files` table.
    """

    query = """insert into meta.static_files (name, file_name, region, update_date) 
    VALUES ('Création tableau de bord', 'creation_tableau_de_bord',
    'pytest', CURRENT_TIMESTAMP)
      returning file_id"""
    rset = await controller.fetch(query)

    doc_id = rset[0]["file_id"]
    yield doc_id

    await controller.fetch("DELETE FROM meta.static_files where region = 'pytest'")


@pytest.fixture
async def analysis_page_parameters():
    """Inserts a page configuration for the graphics of the territorial analysis page."""
    await controller.fetch(
        "DELETE FROM meta.analysis_page_parameters WHERE region_id = 'pytest'"
    )
    query = """INSERT INTO meta.analysis_page_parameters 
      (region_id, env_name, page_name, graph_type, status, update_date)
      VALUES ('pytest', 'dev', 'suivi_energetique', 'fictive_chart_enabled', true, NOW()),
      ('pytest', 'dev', 'suivi_energetique', 'other_fictive_chart_disabled', false, NOW()),
      ('pytest', 'test', 'suivi_energetique', 'fictive_chart_enabled', true, NOW()),
      ('pytest', 'test', 'suivi_energetique', 'other_fictive_chart_disabled', false, NOW()),
      ('pytest', 'prod', 'suivi_energetique', 'fictive_chart_enabled', true, NOW()),
      ('pytest', 'prod', 'suivi_energetique', 'other_fictive_chart_disabled', false, NOW())"""
    await controller.execute(query)
    yield True
    await controller.fetch(
        "DELETE FROM meta.analysis_page_parameters WHERE region_id = 'pytest'"
    )


@pytest.fixture
async def external_apis():
    """Inserts a few examples of external APIs for pytest region."""
    base = {
        "region": "pytest",
        "name": "Test external API",
        "slug": "test-external-api",
        "actor": "PyTEST",
        "url": "http://this.does.not.exist.really",
        "key": "patate",
        "format": "json",
        "method": "GET",
        "table_name": "new_table_from_external_api",
        "perimeter_year": "2022",
        "details_column": "test",
        "subkeys": "",
        "geographical_column": "epci",
        "post_data": "{}",
        "columns": '{"col": "valeur", "test": "epci", "annee": "annee"}',
        "filters": "{}",
        "date_maj": "2023-04-20",
    }

    def c(other_dict):
        return {
            **base,
            **other_dict,
        }

    data = {
        "normal-test": base,
        "csv-get-test": c(
            {
                "name": "CSV Test external API",
                "slug": "csv-test-external-api",
                "format": "csv",
            }
        ),
        "wrong-format": c(
            {
                "slug": "wrong-format",
                "format": "other/wrong/format",
            }
        ),
        "no-data-table": c(
            {
                "name": "No data table external API",
                "slug": "no-data-table-test-external-api",
                "table_name": "",
            }
        ),
        "post-test": c(
            {
                "name": "Other test external API",
                "slug": "other-test-external-api",
                "method": "POST",
                "post_data": '{"group":"test"}',
            }
        ),
        "filters-test": c(
            {
                "slug": "filters-test",
                "filters": '{"departement": "rhone"}',
            }
        ),
        "subkeys-test": c(
            {
                "slug": "subkeys-test",
                "subkeys": "test,next",
                "fixed_year": 2022,
            }
        ),
        "invalid-geographic-column": c(
            {
                "slug": "invalid-geographic-column",
                "geographical_column": "wrong --''po''n-é",
            }
        ),
        "wrong-method": c(
            {
                "slug": "wrong-method",
                "method": "DELETE",
            }
        ),
    }

    rows = []
    for row in data.values():
        rows.append(
            "('" + "', '".join([row[key] for key in data["normal-test"].keys()]) + "')"
        )
    values = ", ".join(rows)
    query = f"""INSERT INTO meta.external_api 
    ({", ".join(data["normal-test"].keys())})
    VALUES
        {values}
    ;"""
    await controller.execute(query)
    yield data
    await controller.fetch("DELETE FROM meta.external_api WHERE region = 'pytest'")


@pytest.fixture
async def sankeys(region):
    """Inserts a few sankeys for pytest region."""
    base = {
        "data_table": "test_sankey_data_table",
        "region": "pytest",
        "year": "2022",
        "introduction_text": "This is an introduction text",
        "unit": "GWh",
        "source": "ORCAE",
        "copyright": "false",
        "division_factors": "{}",
        "division_units": "{}",
        "sankey_name": "This is a Sankey example for pytest region",
        "is_regional_default": "true",
        "nb_decimals": "0",
        "geographical_levels_enabled": "epci,region,departement",
    }

    def c(other_dict):
        return {
            **base,
            **other_dict,
        }

    data = {
        "normal-test": base,
        "other-non-default-sankey": c(
            {
                "data_table": "other_sankey_table",
                "is_regional_default": "false",
                "sankey_name": "This is another sankey",
                "nb_decimals": "1",
                "division_factors": json.dumps({"epci": 1000}),
                "division_units": json.dumps({"epci": "MWh"}),
            }
        ),
        "disabled_sankey": c(
            {
                "data_table": "disabled_sankey_data_table",
                "is_regional_default": "false",
                "sankey_name": "This is a disabled sankey",
                "nb_decimals": "1",
                "geographical_levels_enabled": "",
            }
        ),
        "confid-sankey": c(
            {
                "data_table": "confid_sankey_data_table",
                "is_regional_default": "false",
                "sankey_name": "This is a confid sankey",
                "nb_decimals": "1",
                "geographical_levels_enabled": "epci,region,departement",
                "year": "2023",
            }
        ),
    }

    query = f"""CREATE TABLE pytest.sankey_confidentiality (
        annee integer,
        type_territoire character varying,
        id_polluant double precision,
        code_territoire character varying,
        energie character varying,
        secteur character varying,
        val_mwh double precision,
        confidentiel character varying
    );"""
    await controller.execute(query)
    query = f"""INSERT INTO pytest.sankey_confidentiality VALUES(
        2023, 'region', '133', '1', 'node0', 'node1', 10, 'Oui'
    );"""
    await controller.execute(query)
    rows = []
    for row in data.values():
        # we insert the config inside sankey meta data table
        rows.append(
            "('" + "', '".join([row[key] for key in data["normal-test"].keys()]) + "')"
        )

        # we create corresponding data table
        data_table = row["data_table"]
        query = f"""CREATE TABLE pytest.{data_table} (
            source character varying,
            target character varying,
            colors character varying,
            code character varying,
            valeur double precision
        );"""
        await controller.execute(query)

        # we also insert some data inside the table
        query = f"""INSERT INTO pytest.{data_table} 
            VALUES('node0', 'node1', 'red', '00000', 15);"""
        await controller.execute(query)

        # we upload the layout from basic config
        main_path_to_files = settings.get("api", "upload_other_files", fallback="/tmp")
        filename = (
            f"{main_path_to_files}/{region}_{slugify.slugify(row['data_table'])}.json"
        )
        layout_name = (
            "sankey_layout.json"
            if row["is_regional_default"] == "true"
            else "new_sankey_layout.json"
        )
        with open(here / "data" / layout_name, "r") as f:
            with open(filename, "w") as fw:
                fw.write(f.read())

    values = ", ".join(rows)
    query = f"""INSERT INTO meta.sankey_metadata 
    ({", ".join(data["normal-test"].keys())})
    VALUES
        {values}
    ;"""
    await controller.execute(query)
    yield data
    await controller.fetch("DELETE FROM meta.sankey_metadata WHERE region = 'pytest'")
    await controller.fetch("DROP TABLE pytest.sankey_confidentiality")
    for row in data.values():
        main_path_to_files = settings.get("api", "upload_other_files", fallback="/tmp")
        filename = (
            f"{main_path_to_files}/{region}_{slugify.slugify(row['data_table'])}.json"
        )
        if os.path.isfile(filename):
            os.remove(filename)
