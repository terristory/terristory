# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pytest

from terriapi import controller

from ..cleaning_tools import clean_indicators_tables
from ..common import SCHEMA


@pytest.fixture
async def indicators(app, db):
    """Création d'une table de données d'indicateur dans le schéma de test 'pytest'.

    Renvoie le nom de la table.

    On ajoute la fixture 'client_anonymous' afin de démarrer une application Web de test
    et ainsi avoir le Pool de connexion à la DB qui fonctionne (via controller.db)
    """
    # we clean indicators table
    await clean_indicators_tables(db)
    query_historic = f"""
    CREATE TABLE IF NOT EXISTS {SCHEMA}.historique_indicateurs
    (
        id SERIAL PRIMARY KEY,
        nom_donnee character varying NOT NULL,
        mail character varying,
        action character varying,
        date_maj timestamp without time zone,
        autres_informations text,
        region character varying
    )"""
    await controller.execute(query_historic)

    tablenames = ["conso_energetique", "emission_ges", "other_table"]
    # we create data tables
    for tablename in tablenames:
        query = f"""create table {SCHEMA}.{tablename} (
        commune varchar(5)
        , annee int
        , energie int
        , secteur int
        , other_cat int
        , usage int
        , valeur float
        )
        """
        await controller.execute(query)
        # we insert raw data in the data tables
        insert_data_raw_table = f"""insert into {SCHEMA}.{tablename} VALUES 
            ('00000', 2000, 1, 1, 1, 1, 10.0),
            ('00000', 2001, 1, 1, 1, 1, 20.0),
            ('00000', 2002, 1, 1, 1, 1, 30.0),
            ('00000', 2003, 1, 1, 1, 1, 40.0),
            ('00000', 2000, 1, 2, 1, 1, 10.0),
            ('00000', 2001, 1, 2, 1, 1, 20.0),
            ('00000', 2002, 1, 2, 1, 1, 30.0),
            ('00000', 2003, 1, 2, 1, 1, 40.0)
            """
        await controller.execute(insert_data_raw_table)
        # we associate the data table with a perimeter
        insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
            VALUES ($1, 'pytest', 2020, false)"""
        await controller.execute(insert_perimeter, tablename)

    query = f"""create table {SCHEMA}.prod_enr (
    commune varchar(5)
    , annee int
    , other_cat int
    , type_prod_enr int
    , valeur float
    )
    """
    await controller.execute(query)
    # we insert raw data in the data tables
    insert_data_raw_table = f"""insert into {SCHEMA}.prod_enr VALUES 
        ('00000', 2000, 1, 1, 10.0),
        ('00000', 2001, 1, 1, 20.0),
        ('00000', 2002, 1, 1, 30.0),
        ('00000', 2003, 1, 1, 40.0),
        ('00000', 2000, 1, 2, 10.0),
        ('00000', 2001, 1, 2, 20.0),
        ('00000', 2002, 1, 2, 30.0),
        ('00000', 2003, 1, 2, 40.0)
        """
    await controller.execute(insert_data_raw_table)
    # we associate the data table with a perimeter
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ('prod_enr', 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter)

    # Create data tables at other level
    query = f"""create table {SCHEMA}.conso_energetique_epci (
    epci varchar(5)
    , annee int
    , energie int
    , secteur int
    , other_cat int
    , usage int
    , valeur float
    )
    """
    await controller.execute(query)
    # we insert raw data in the data tables
    insert_data_raw_table = f"""insert into {SCHEMA}.conso_energetique_epci VALUES 
        ('1', 2000, 1, 1, 1, 1, 10.0),
        ('1', 2001, 1, 1, 1, 1, 20.0),
        ('1', 2002, 1, 1, 1, 1, 30.0),
        ('1', 2003, 1, 1, 1, 1, 40.0),
        ('1', 2000, 1, 2, 1, 1, 10.0),
        ('1', 2001, 1, 2, 1, 1, 20.0),
        ('1', 2002, 1, 2, 1, 1, 30.0),
        ('1', 2003, 1, 2, 1, 1, 40.0)
        """
    await controller.execute(insert_data_raw_table)

    # we associate the data table with a perimeter
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "conso_energetique_epci")

    # Create pixel data tables at other level
    query = f"""create table {SCHEMA}.indicator_pixels_data (
    commune varchar(5), valeur float, annee int, geom int
    )
    """
    await controller.execute(query)
    # we insert raw data in the data tables
    insert_data_raw_table = f"""insert into {SCHEMA}.indicator_pixels_data VALUES 
        ('00000', 20.0, 2003, 1),
        ('00000', 20.0, 2002, 21),
        ('00000', 30.0, 2003, 2),
        ('00000', 30.0, 2002, 22),
        ('00001', 25, 2003, 1),
        ('00001', 25, 2002, 21),
        ('00001', 35, 2003, 2),
        ('00001', 35, 2002, 22);
        """
    await controller.execute(insert_data_raw_table)
    # we associate the data table with a perimeter
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "indicator_pixels_data")

    # Create pixels cat data tables at other level
    query = f"""create table {SCHEMA}.indicator_pixels_cat_data (
    commune varchar(5), valeur float, annee int, geom int
    )
    """
    await controller.execute(query)
    # we insert raw data in the data tables
    insert_data_raw_table = f"""insert into {SCHEMA}.indicator_pixels_cat_data VALUES 
        ('00000', 1, 2003, 1),
        ('00000', 1, 2002, 21),
        ('00000', 2, 2003, 2),
        ('00001', 1, 2002, 21),
        ('00001', 2, 2003, 2),
        ('00001', 2, 2002, 22);
        """
    await controller.execute(insert_data_raw_table)
    # we associate the data table with a perimeter
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "indicator_pixels_cat_data")

    # other specific tables

    # weighted layer
    query = f"""create table {SCHEMA}.weighted_data (
    commune varchar(5)
    , annee int
    , modalite_id int
    , type_categorie varchar
    , valeur float
    )
    """
    await controller.execute(query)
    # we insert raw data in the data tables
    insert_data_raw_table = f"""insert into {SCHEMA}.weighted_data VALUES 
        ('00000', 2000, 1, 'hebdomadaire', 20.0),
        ('00000', 2000, 1, 'quotidien', 10.0),
        ('00000', 2001, 1, 'hebdomadaire', 20.0),
        ('00000', 2001, 1, 'quotidien', 20.0),
        ('00000', 2002, 1, 'hebdomadaire', 30.0),
        ('00000', 2002, 1, 'quotidien', 60.0),
        ('00000', 2003, 1, 'hebdomadaire', 50.0),
        ('00000', 2003, 1, 'quotidien', 200.0),
        ('00000', 2000, 2, 'hebdomadaire', 20.0),
        ('00000', 2000, 2, 'quotidien', 10.0),
        ('00000', 2001, 2, 'hebdomadaire', 20.0),
        ('00000', 2001, 2, 'quotidien', 20.0),
        ('00000', 2002, 2, 'hebdomadaire', 30.0),
        ('00000', 2002, 2, 'quotidien', 60.0),
        ('00000', 2003, 2, 'hebdomadaire', 50.0),
        ('00000', 2003, 2, 'quotidien', 200.0)
        """
    await controller.execute(insert_data_raw_table)

    # we associate the data table with a perimeter
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "weighted_data")

    # job accessibility
    query = f"""create table {SCHEMA}.job_accessibility (
        commune character varying,
        valeur double precision,
        csp integer,
        modalite character varying,
        valeur_filtre double precision,
        rapport double precision,
        nbre_actifs double precision,
        annee integer
    )
    """
    await controller.execute(query)
    # we insert raw data in the data tables
    insert_data_raw_table = f"""insert into {SCHEMA}.job_accessibility VALUES 
        ('00000', 100, 1, 'Workers', 10, 0.7, 10, 2000),
        ('00000', 200, 2, 'Other workers', 10, 0.7, 20, 2000),
        ('00000', 200, 1, 'Workers', 20, 0.2, 30, 2000),
        ('00000', 400, 2, 'Other workers', 20, 0.2, 40, 2000),
        ('00000', 300, 1, 'Workers', 30, 0.5, 50, 2000),
        ('00000', 600, 2, 'Other workers', 30, 0.5, 100, 2000)
        """
    await controller.execute(insert_data_raw_table)

    # we associate the data table with a perimeter
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "job_accessibility")

    # we create indicators
    insert_analyses = []
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit, confidentiel)
      values (-3, 'pytest-analysis', 'conso_energetique', '#007f7b', '#007f7b', 1, '{{2003,2002}}', true, 'test_category', 1, '{SCHEMA}', 'GWh', 'conso_energetique')
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit)
      values (-4, 'second-pytest-analysis', 'conso_energetique/10000', '#ff1133', '#ffbb33', 1, '{{2003,2002}}', true, 'second_category', 2, '{SCHEMA}', 'GWh')
      returning id"""
    )

    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit)
      values (-5, 'third-pytest-analysis', 'prod_enr', '#ff1133', '#ffbb33', 1, '{{2003,2002}}', true, 'test_category', 1, '{SCHEMA}', 'GWh')
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit)
      values (-6, 'fourth-pytest-analysis', 'emission_ges', '#ff1133', '#ffbb33', 1, '{{2003,2002}}', true, 'test_category', 3, '{SCHEMA}', 'GWh')
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit)
      values (-7, 'fifth-pytest-analysis', 'other_table', '#ff1133', '#ffbb33', 1, '{{2003,2002}}', true, 'second_category', 1, '{SCHEMA}', 'GWh')
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit, moyenne_ponderee)
      values (-8, 'weighted-analysis', 'weighted_data', '#ff1133', '#ffbb33', 1, '{{2003,2002}}', true, 'second_category', 1, '{SCHEMA}', 'GWh', true)
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit, type, data_type)
      values (-9, 'job_accessibility-analysis', 'job_accessibility', '#ff1133', '#ffbb33', 1, '{{2003,2002}}', true, 'second_category', 1, '{SCHEMA}', 'GWh', 'circle', 'accessibilite_emploi')
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit, only_for_zone)
      values (-10, 'pytest-analysis at epci level', 'conso_energetique_epci', '#007f7b', '#007f7b', 1, '{{2003,2002}}', true, 'test_category', 1, '{SCHEMA}', 'GWh', 'epci')
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit)
      values (-11, 'pixel data indicator', 'indicator_pixels_data', '#007f7b', '#007f7b', 1, '{{2003,2002}}', true, 'test_category', 1, '{SCHEMA}', 'GWh')
      returning id"""
    )
    insert_analyses.append(
        f"""insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, years, active, ui_theme, ordre_ui_theme, region, unit, isratio)
      values (-12, 'pytest-analysis-with-ratio', 'conso_energetique/maille.prod_enr', '#007f7b', '#007f7b', 1, '{{2003,2002}}', true, 'test_category', 1, '{SCHEMA}', 'GWh', true)
      returning id"""
    )
    # General query for charts
    charts_queries = f"""insert into meta.chart (indicateur, titre, type, categorie, visible, ordre, is_single_select)
            values ($1, 'Par energie', 'pie', 'energie', true, 1, false),
            ($1, 'Par secteur', 'pie', 'secteur', true, 1, false),
            ($1, 'Par usage', 'pie', 'usage', true, 1, false);"""

    added_indicators_id = []
    for analysis in insert_analyses:
        rset = await controller.fetch(analysis)
        analysis_id = rset[0]["id"]
        added_indicators_id.append(analysis_id)

        rset = await controller.execute(charts_queries, analysis_id)

    # we create modalities for the data
    update_categorie = f"""insert into meta.categorie (nom, modalite, modalite_id, region, couleur, ordre)
      values
       ('secteur', 'Résidentiel', 1, $1, 'red', 2),
       ('secteur', 'Transport', 2, $1, 'blue', 1),
       ('other_cat', 'Résidentiel', 1, $1, 'red', 1),
       ('other_cat', 'Transport', 2, $1, 'blue', 2),
       ('type_prod_enr', 'Eolien', 1, $1, 'red', 1),
       ('type_prod_enr', 'Hydraulique', 2, $1, 'blue', 2),
       ('energie', 'Électricité', 1, $1, 'red', 1),
       ('energie', 'Gaz', 2, $1, 'blue', 2),
       ('quotidien', 'Longue distance', 1, $1, 'red', 1),
       ('quotidien', 'Courte distance', 2, $1, 'blue', 2),
       ('hebdomadaire', 'Longue distance', 1, $1, 'red', 1),
       ('hebdomadaire', 'Courte distance', 2, $1, 'blue', 2),
       ('csp', 'Workers', 1, $1, 'red', 1),
       ('csp', 'Other workers', 2, $1, 'blue', 2),
       ('usage', 'Chauffage', 1, $1, 'red', 1),
       ('usage', 'Froid', 2, $1, 'blue', 2),
       ('indicator_pixels_cat_data', 'Great category', 1, $1, 'green', 1),
       ('indicator_pixels_cat_data', 'Another great category', 2, $1, 'yellow', 2)
    """
    await controller.execute(update_categorie, SCHEMA)

    # we yield valuable data
    yield tablenames, added_indicators_id

    # we clean indicators table
    await clean_indicators_tables(db)


@pytest.fixture
async def enabled_indicator(db, indicators, region):
    """Insère une nouvelle analyse (active) pour les tests"""
    query = """insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, active, region)
      values (-1, 'test-active-analysis', 'test', '#007f7b', '#007f7b', 1, true, $1)
      returning id"""
    rset = await controller.fetch(query, SCHEMA)
    analyse_id = rset[0]["id"]
    yield analyse_id
    await controller.fetch("DELETE FROM meta.indicateur where id=$1", analyse_id)


@pytest.fixture
async def disabled_indicator(db, indicators, region):
    """Insère une nouvelle analyse (inactive) pour les tests"""
    query = """insert into meta.indicateur (id, nom, data, color_start, color_end, decimals, active, region)
      values (-2, 'test-inactive-analysis', 'test', '#007f7b', '#007f7b', 1, false, $1)
      returning id"""
    rset = await controller.fetch(query, SCHEMA)
    analyse_id = rset[0]["id"]
    yield analyse_id
    await controller.fetch("DELETE FROM meta.indicateur where id=$1", analyse_id)


@pytest.fixture
async def data_units(db, region, indicators):
    """Inserts a few examples of external APIs for pytest region."""
    # first cleans table content
    await controller.fetch("DELETE FROM meta.data_units WHERE region = 'pytest';")
    # then proceeds
    units = [
        ["-1", "Énergie", "GWh", "1.0"],
        ["-2", "Énergie", "MWh", "1000.0"],
        ["-3", "Énergie", "TWh", "0.001"],
        ["-4", "Énergie", "toe", "85.984522785899"],
        ["-5", "Énergie", "PJ", "0.0036"],
    ]
    units_links = [
        ["-3", "-1", "", ""],
        ["-3", "-2", "commune", "region,departement"],
        ["-3", "-3", "", "commune"],
        ["-3", "-4", "", ""],
        ["-3", "-5", "", "commune"],
        ["-12", "-1", "", ""],
        ["-12", "-2", "commune", "region,departement"],
        ["-12", "-3", "", "commune"],
        ["-12", "-4", "", ""],
        ["-12", "-5", "", "commune"],
    ]

    rows = []
    for row in units:
        rows.append("('pytest', '" + "', '".join(row) + "')")
    values = ", ".join(rows)
    query = f"""INSERT INTO meta.data_units (region, unit_id, unit_type, unit_name, conversion_factor)
    VALUES {values};"""
    await controller.execute(query)
    rows = []
    for row in units_links:
        rows.append("('" + "', '".join(row) + "')")
    values = ", ".join(rows)
    query = f"""INSERT INTO meta.data_units_indicators_association VALUES {values};"""
    await controller.execute(query)
    yield units
    await controller.fetch("DELETE FROM meta.data_units WHERE region = 'pytest';")


@pytest.fixture
async def confid(db, region):
    confid_rules = {
        "maille": [
            (
                "01",
                "departement",
                2002,
                "conso_energetique",
                '{"secteur": "*", "energie": "*"}',
            ),
            (
                "00000",
                "commune",
                2002,
                "conso_energetique",
                '{"secteur": "*", "energie": "*"}',
            ),
        ],
        "camembert": [
            ("02", "departement", 2000, "conso_energetique", "A"),
            ("02", "departement", 2001, "conso_energetique", "B"),
            ("02", "departement", 2002, "conso_energetique", "C"),
            ("02", "departement", 2003, "conso_energetique", "D"),
        ],
    }

    query_territories = f"""INSERT INTO {SCHEMA}.confid_maille 
        (code, type_territoire, annee, type_analyse, conditions)
        VALUES($1, $2, $3, $4, $5::jsonb)
        """
    await controller.executemany(query_territories, confid_rules["maille"])

    query_territories = f"""INSERT INTO {SCHEMA}.confid_camembert 
        (code, type_territoire, annee, type_analyse, type_confid)
        VALUES($1, $2, $3, $4, $5)
        """
    await controller.executemany(query_territories, confid_rules["camembert"])

    yield confid_rules

    await controller.execute(f"TRUNCATE {SCHEMA}.confid_maille")
    await controller.execute(f"TRUNCATE {SCHEMA}.confid_camembert")
