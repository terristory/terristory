# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd
import pytest

from terriapi import controller

from ..common import here


@pytest.fixture
async def passage_table(region):
    await controller.fetch(
        "DELETE FROM strategie_territoire.passage_table WHERE region = 'pytest';"
    )
    df = pd.read_csv(here / "data" / "passage_table.csv", sep=";")
    engine = controller.get_pg_engine()
    df.to_sql(
        "passage_table",
        con=engine,
        index=False,
        schema="strategie_territoire",
        if_exists="append",
    )
    yield 1
    await controller.fetch(
        "DELETE FROM strategie_territoire.passage_table WHERE region = 'pytest';"
    )


@pytest.fixture
async def example_action(region):
    # clean
    await controller.fetch(
        "DELETE FROM strategie_territoire.action_status WHERE region = $1", region
    )
    await controller.fetch(
        "DELETE FROM strategie_territoire.action_subactions_status WHERE region = $1",
        region,
    )
    query = f"""
    INSERT INTO strategie_territoire.action_status VALUES
        ($1, '1', 'residential_refurbishment', true, 0, true, true, 2022, 2050),
        ($1, '3', 'pv_ground_based', true, 0, true, true, 2022, 2050),
        ($1, '4b', 'solar_thermal/combine', true, 0, true, true, 2022, 2050),
        ($1, '4', 'solar_thermal/ecs', true, 0, true, true, 2022, 2050);
    """
    await controller.execute(query, region)
    await controller.execute(
        """INSERT INTO strategie_territoire.action_subactions_status
        SELECT s.region, s.action_number, sb.nom, true
        FROM strategie_territoire.action_subactions sb
        LEFT JOIN strategie_territoire.action_status s ON s.action_number = sb.action_number
        WHERE s.region = $1;""",
        region,
    )
    yield "residential_refurbishment"
    await controller.execute(
        f"""DELETE FROM strategie_territoire.action_status WHERE region=$1;""",
        region,
    )
    await controller.execute(
        f"""DELETE FROM strategie_territoire.action_subactions_status WHERE region=$1;""",
        region,
    )


@pytest.fixture
def scenario(client_basic_user):
    """Fixture qui va créer puis retourner un scénario"""
    valeurs = {"{}".format(year): 0 for year in range(2019, 2051)}
    # ensemble des actions from AuRA region
    _request, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/actions", params={"zone": "region", "zone_id": "84"}
    )
    assert resp.status == 200
    actions = resp.json
    assert "actions" in actions
    # paramètres pour chaque action
    _request, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/actions/subactions"
    )
    assert resp.status == 200
    params = resp.json
    # paramètres dits "avancés"
    _request, resp = client_basic_user.get(
        "/api/auvergne-rhone-alpes/actions/params/advanced",
        params={"zone_id": "84", "zone": "region"},
    )
    assert resp.status == 200
    avancés = resp.json
    data = {param["nom"]: valeurs for param in params}
    # on remplit certaines valeurs de paramètres d'action
    # les actions concernées
    #  - rénovations résidentielles (1)
    #  - Centrale photovolatique au sol (3)
    #  - Installation solaire thermique (4b)
    data["actions"] = ["1", "3", "4b"]
    data["1_nb_logements"].update(
        {
            "2019": "150",
            "2020": "150",
            "2021": 0,
            "2022": "200",
            "2023": "200",
            "2024": 0,
            "2025": "200",
            "2026": "300",
        }
    )
    data["1_surface_moy_logement"].update(
        {
            "2019": "50",
            "2020": "50",
            "2021": 0,
            "2022": "50",
            "2023": "50",
            "2024": 0,
            "2025": "40",
            "2026": "40",
        }
    )
    data["3_puiss_crete"].update({"2020": "2", "2021": "2", "2022": "4", "2023": "5"})
    data["4b_nb_batiment_collectif_renouv"].update(
        {"2025": "50", "2026": "50", "2027": "50"}
    )
    data["4b_surf_capteurs_bati"].update({"2025": "5", "2026": "5", "2027": "5"})
    # on garde les paramètres avancés dit aussi "économiques" par défaut
    data["advanced"] = avancés["actions"]
    # paramètres économiques
    data["economique"] = {
        "111": 76.0,
        "112": 57.7,
        "113": 76.3,
        "114": 57.7,
        "115": 57.7,
        "116": 57.7,
    }
    # trajectoires cibles
    data["trajectoires"] = {
        "emission_ges": {"{}".format(year): 55_000 for year in range(2019, 2030)},
        "energie_economisee": {
            "{}".format(year): 220_000 for year in range(2019, 2030)
        },
        "energie_produite": {"{}".format(year): 40_000 for year in range(2019, 2030)},
    }
    # on ajoute le titre, la description et les zones géographiques
    data["titre"] = "test région département"
    data["description"] = "test région"
    data["zone_type"] = "region#departement"
    data["nom_territoire"] = "Cantal"
    data["region"] = "pytest"
    # XXX(dag) : pas encore compris pourquoi depuis le browser on a 'null' dans ce champs
    data["zone_id"] = None
    data["reference_year"] = 2018
    _request, resp = client_basic_user.post("/api/user/pytest/scenario", json=data)
    assert resp.status == 200
    scenario = resp.json
    return scenario


@pytest.fixture
async def strat_params():
    """
    Insert a new fake action for region pytest inside `strategie_territoire.action_details` table.
    """

    query = """INSERT INTO strategie_territoire.params_user_action_params  
    (id, action, region, libelle, nom, unite, valeur) 
    (
        SELECT id, action, 'pytest', libelle, nom, unite, valeur 
        FROM strategie_territoire.params_user_action_params  
        WHERE region LIKE 'auvergne%'
    )"""
    await controller.execute(query)
    query = """INSERT INTO strategie_territoire.params_user_action_params_years  
    (id, action, region, libelle, nom) 
    (
        SELECT id, action, 'pytest', libelle, nom
        FROM strategie_territoire.params_user_action_params_years  
        WHERE region LIKE 'auvergne%'
    )"""
    await controller.execute(query)

    yield []

    await controller.execute(
        "DELETE FROM strategie_territoire.params_user_action_params WHERE region = 'pytest'"
    )
    await controller.execute(
        "DELETE FROM strategie_territoire.params_user_action_params_years WHERE region = 'pytest'"
    )
