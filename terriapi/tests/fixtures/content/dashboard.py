# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pytest

from terriapi import controller

from ..common import SCHEMA


@pytest.fixture
async def dashboard_fixtures(indicators):
    _, analyses = indicators
    return {
        "titre": "Nom tableau bord",
        "description": "une description",
        "thematiques": [
            {
                "titre_thematique": "Énergie renouvelable",
                "description_thematique": "Objectif territoire à énergie positive",
                "graphiques": [
                    {
                        "indicateur_id": analyses[0],
                        "representation": "pie",
                        "categories": ["type_prod_enr"],
                    },
                    {
                        "indicateur_id": analyses[0],
                        "representation": "pie",
                        "categories": ["secteur_solaire_thermique"],
                    },
                ],
            },
            {
                "titre_thematique": "consommation",
                "description_thematique": "Niveau de maîtrise de consommation de l'énergie",
                "graphiques": [
                    {
                        "indicateur_id": analyses[1],
                        "representation": "pie",
                        "categories": ["secteur", "energie"],
                    },
                    {
                        "indicateur_id": analyses[1],
                        "representation": "line",
                        "categories": ["secteur", "usage"],
                    },
                ],
            },
        ],
    }


@pytest.fixture
def dashboard(client_admin_user, region, indicators, dashboard_fixtures, loop):
    """Création d'un tableau de bord avec un compte admin"""
    loop.run_until_complete(
        controller.fetch("DELETE FROM meta.tableau_bord WHERE region = $1", SCHEMA)
    )
    _request, resp = client_admin_user.post(
        "/api/pytest/tableau", json=dashboard_fixtures
    )
    assert resp.status == 200
    content = resp.json

    _request, resp = client_admin_user.put(
        f"/api/pytest/tableau/{content['tableau_id']}/affectation",
        json={"zone": "epci"},
    )
    assert resp.status == 200

    yield content["tableau_id"]
