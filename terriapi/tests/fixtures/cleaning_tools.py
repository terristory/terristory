# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from terriapi import controller

from .common import SCHEMA


async def clean_indicators_tables(db):
    await controller.execute(
        "DELETE FROM meta.chart c WHERE (SELECT region FROM meta.indicateur i WHERE i.id = c.indicateur) = $1;",
        SCHEMA,
    )
    await controller.execute(
        "DELETE FROM meta.perimetre_geographique WHERE region = $1", SCHEMA
    )
    await controller.execute("DELETE FROM meta.indicateur WHERE region = $1", SCHEMA)
    await controller.execute("DELETE FROM meta.categorie WHERE region = $1", SCHEMA)


async def clean_region_tables(db):
    # await controller.execute("DELETE FROM utilisateur WHERE region = $1", SCHEMA)
    # await controller.execute("DELETE FROM regions_configuration WHERE id = $1", SCHEMA)

    # await controller.execute(
    #     f"DELETE FROM regions_configuration WHERE id = $1 AND env IN ('test', 'dev')",
    #     SCHEMA,
    # )
    await controller.execute(
        f"DELETE FROM strategie_territoire.supra_goals WHERE region = $1", SCHEMA
    )
    await clean_ip_tables(db)
    await controller.execute(
        f"DELETE FROM strategie_territoire.supra_goals WHERE region = $1", SCHEMA
    )
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.territoire")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.zone")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.region")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.epci")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.departement")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.confid_maille")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.confid_camembert")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.commune")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.commune_fr")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.chiffres_cle")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.pcaet")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.pcaet_meta")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.siren_assignment")


async def clean_ip_tables(db):
    tables = [
        "consultations.actions_cochees",
        "consultations.poi",
        "consultations.consultations_indicateurs",
        "consultations.analyses_territoriales",
        "consultations.tableaux_bords",
        "consultations.autres_pages",
    ]
    for table in tables:
        await controller.execute(
            f"DELETE FROM {table} WHERE region = 'pytest' OR id_utilisateur = -1",
        )
    await controller.execute(
        f"DELETE FROM consultations.ip_localisation WHERE id = -1",
    )
