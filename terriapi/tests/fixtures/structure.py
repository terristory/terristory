# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pytest
from sanic.log import logger

from terriapi import controller

from .cleaning_tools import clean_ip_tables, clean_region_tables
from .common import SCHEMA


@pytest.fixture
async def region(db, main_structure, client_anonymous):
    """Création d'une table de données d'un POI dans le schéma de test 'pytest'.

    Renvoie le nom de la table.

    On ajoute la fixture 'client_anonymous' afin de démarrer une application Web de test
    et ainsi avoir le Pool de connexion à la DB qui fonctionne (via controller.db)
    """
    await clean_region_tables(db)
    envname = SCHEMA
    await clean_ip_tables(db)

    query_ip_localisation = f"""INSERT INTO consultations.ip_localisation 
    (id, ip, commune, region_selectionnee) 
    VALUES(-1, '0.0.0.0', $1, $1) returning id"""
    await controller.fetch(query_ip_localisation, SCHEMA)

    query_territories_table = f"""CREATE TABLE {SCHEMA}.territoire (
        commune character varying,
        code character varying,
        type_territoire character varying,
        nom character varying
    )"""
    await controller.execute(query_territories_table)
    query_zones_table = f"""CREATE TABLE {SCHEMA}.zone (
        nom character varying,
        maille character varying,
        libelle character varying,
        ordre integer,
        hide boolean
    )"""
    await controller.execute(query_zones_table)
    query_regional_table = f"""CREATE TABLE {SCHEMA}.region (
        nom character varying,
        code character varying,
        communes character varying[],
        geom geometry(MultiPolygon,3857),
        siren character varying,
        x double precision,
        y double precision
    )"""
    await controller.execute(query_regional_table)
    query_epci_table = f"""CREATE TABLE {SCHEMA}.epci (
        nom character varying,
        code character varying,
        siren character varying,
        communes character varying[],
        geom geometry(MultiPolygon,3857),
        x double precision,
        y double precision
    )"""
    await controller.execute(query_epci_table)
    query_departement_table = f"""CREATE TABLE {SCHEMA}.departement (
        nom character varying,
        code character varying,
        siren character varying,
        communes character varying[],
        geom geometry(MultiPolygon,3857),
        x double precision,
        y double precision
    )"""
    await controller.execute(query_departement_table)
    query_confid = f"""CREATE TABLE {SCHEMA}.confid_maille (
        code character varying,
        type_territoire character varying,
        annee integer,
        type_analyse character varying,
        conditions jsonb
    )"""
    await controller.execute(query_confid)
    query_confid = f"""CREATE TABLE {SCHEMA}.confid_camembert (
        code character varying,
        type_territoire character varying,
        annee integer,
        type_analyse character varying,
        type_confid character varying
    )"""
    await controller.execute(query_confid)
    query_confid = f"""CREATE TABLE {SCHEMA}.commune (
        nom character varying,
        x double precision,
        y double precision,
        population integer,
        geom geometry(MultiPolygon,3857),
        code character varying
    )"""
    await controller.execute(query_confid)
    # for indicators using other cities
    query_confid = f"""CREATE TABLE {SCHEMA}.commune_fr (
        nom character varying,
        x double precision,
        y double precision,
        statut character varying,
        geom geometry(MultiPolygon,3857),
        code character varying
    )"""
    await controller.execute(query_confid)
    query_key_figures = f"""create table {SCHEMA}.chiffres_cle (
        ordre integer,
        nom_analyse character varying,
        donnee character varying,
        nom_table_analyse character varying,
        unite character varying
    )
    """
    await controller.execute(query_key_figures)
    query_pcaet = f"""CREATE TABLE pytest.pcaet (
        siren VARCHAR, trajectory VARCHAR, category VARCHAR, year INT,
        value DOUBLE PRECISION, is_ref BOOL, relative_value DOUBLE PRECISION,
        region VARCHAR
    );
    """
    await controller.execute(query_pcaet)
    query_pcaet = f"""CREATE TABLE pytest.pcaet_meta (
        siren VARCHAR, statut VARCHAR, date_lancement VARCHAR,
        date_modification VARCHAR, oblige BOOL
    )
    """
    await controller.execute(query_pcaet)
    query_pcaet = f"""CREATE TABLE pytest.siren_assignment
        (zone_type VARCHAR, zone_id VARCHAR, siren VARCHAR, epcis VARCHAR[]);
    """
    await controller.execute(query_pcaet)
    query_pcaet = (
        f"""CREATE UNIQUE INDEX ON pytest.siren_assignment (zone_type, zone_id);"""
    )
    await controller.execute(query_pcaet)
    query_function_part_territoire = f"""CREATE FUNCTION {SCHEMA}.part_territoire(
        territoire character varying,
        territoire_id character varying,
        OUT a88 integer,
        OUT part_terr numeric
    ) RETURNS SETOF record LANGUAGE sql AS $_$
    select
        1 as a88,
        2::numeric as part_terr
        $_$;
    """
    await controller.execute(query_function_part_territoire)
    query_historic_ind = f"""
    CREATE TABLE IF NOT EXISTS {SCHEMA}.historique_indicateurs
    (
        id SERIAL PRIMARY KEY,
        nom_donnee character varying NOT NULL,
        mail character varying,
        action character varying,
        date_maj timestamp without time zone,
        autres_informations text,
        region character varying
    )"""
    await controller.execute(query_historic_ind)
    query_territories = f"""INSERT INTO {SCHEMA}.territoire 
        VALUES ('00000', '1', 'region', 'PYTEST region'),
        ('00000', '00000', 'commune', 'PYTEST commune'),
        ('00000', '01', 'departement', 'PYTEST departement'),
        ('00000', '02', 'departement', 'PYTEST departement'),
        ('00000', '1', 'epci', 'PYTEST EPCI')"""
    await controller.execute(query_territories)
    query_territories = f"""INSERT INTO {SCHEMA}.region (nom, code, communes, geom, siren, x, y)
        VALUES('pytest', '1', '{{00000}}', NULL, 'fake-siren', 1, 1)"""
    await controller.execute(query_territories)
    query_territories = f"""INSERT INTO {SCHEMA}.commune (nom, code, geom, x, y, population)
        VALUES('pytest', '00000', NULL, 1, 2, 10)"""
    await controller.execute(query_territories)
    query_territories = f"""INSERT INTO {SCHEMA}.commune_fr (nom, code, geom, x, y)
        VALUES('pytest', '00000', NULL, 1, 2)"""
    await controller.execute(query_territories)
    query_territories = f"""INSERT INTO {SCHEMA}.departement (nom, code, communes, geom, siren, x, y)
        VALUES('pytest', '01', '{{00000}}', NULL, '', 1, 1),
        ('pytest', '02', '{{00000}}', NULL, '', 2, 2)"""
    await controller.execute(query_territories)
    query_territories = f"""INSERT INTO {SCHEMA}.epci (nom, code, communes, geom, siren, x, y)
        VALUES('pytest epci', '1', '{{00000}}', NULL, '', 3, 3);"""
    await controller.execute(query_territories)

    yield envname

    # we drop and clean everything
    await clean_region_tables(db)
