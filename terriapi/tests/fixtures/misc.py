# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json

import pytest
import slugify
from sanic import Sanic
from sanic.log import logger
from sanic.worker.loader import AppLoader
from sanic_jwt import Initialize
from sanic_testing import TestManager

from terriapi import auth, controller, settings, user
from terriapi.controller.donnees_pour_representation import DonneesPourRepresentations
from terriapi.controller.indicateur_tableau_bord import IndicateurTableauBord
from terriapi.controller.user import create_user
from terriapi.database import setup_db
from terriapi.server import create_app

from .common import SCHEMA


@pytest.fixture
async def create_resultats(app, client_anonymous):
    """Crée des résultats de tests si nécessaire"""
    logger.info("Vérification de l'existance de la table des résultats")
    query = "SELECT count(*) FROM information_schema.tables WHERE table_schema = 'tests' and table_name = 'resultats'"
    test_table_exists = await controller.fetch(query)
    if test_table_exists[0]["count"] == 0:
        query = "create schema if not exists tests"
        await controller.fetch(query)
        query = """
            create table tests.resultats (
                id serial primary key,
                region character varying COLLATE pg_catalog.default,
                indicateur integer references meta.indicateur(id),
                params json,
                resultat json,
                date_maj date
            )"""
        await controller.fetch(query)
        logger.info("Table de résultats créée")
    else:
        logger.info("Table de résultats existante")

    # Récupération de la liste des régions
    query = "select distinct(region) from meta.indicateur where concatenation is null AND region <> $1 order by region"
    regions = await controller.fetch(query, SCHEMA)
    for r in regions:
        region = r["region"]

        # Récupération des mailles disponibles pour la région courante
        query = "select distinct(type_territoire) as maille from {schema}.territoire".format(
            schema=region.replace("-", "_")
        )
        res = await controller.fetch(query)
        mailles = res

        # Récupération du code région
        query = "select code from {schema}.region".format(
            schema=region.replace("-", "_")
        )
        res = await controller.fetch(query)
        code_region = res[0]["code"]

        # Vérification de la présence des résultats dans cette table pour chaque indicateur
        query = f"""select id, nom, type,
        (select max(years) FROM unnest(years) as years) as annee,
        data_type
        FROM meta.indicateur 
        WHERE region = '{region}' AND concatenation IS NULL
        ORDER BY region, nom"""
        indicateurs = await controller.fetch(query)
        for indicateur in indicateurs:
            id = indicateur["id"]
            nom = indicateur["nom"]
            annee = indicateur["annee"]
            if indicateur["type"] == "wms_feed":
                continue
            if not annee:
                annee = 2017
            # Vérification des résultats pour cet indicateur
            query = (
                "select count(*) from tests.resultats where indicateur = {id}".format(
                    id=id
                )
            )
            res = await controller.fetch(query)
            if res[0]["count"] == 0:
                # Génération des résultats pour cet indicateur si nécessaire
                logger.info(
                    "[{region}] Génération des résultats de tests pour l'indicateur {nom} ({id})".format(
                        region=region, nom=nom, id=id
                    )
                )

                for m in mailles:
                    maille = m["maille"]
                    logger.info("Maille {maille}".format(maille=maille))
                    url_params = []
                    indicateur = IndicateurTableauBord(region, id)
                    metadonnees = await indicateur.meta_donnees_indicateur()
                    donnees_pour_representations = DonneesPourRepresentations(
                        metadonnees, maille, "region", code_region
                    )
                    terrivalues_brutes = (
                        await donnees_pour_representations.donnees_finales(
                            "test", 0, {}
                        )
                    )
                    terrivalues = [terrivalues_brutes]
                    url_params = []
                    url_params.append(
                        [
                            ("id_utilisateur", 1),
                            ("zone", "region"),
                            ("maille", maille),
                            ("zone_id", code_region),
                            ("addcode", "1"),
                            ("annee", annee),
                            ("nochart", "1"),
                            ("provenance", "test"),
                        ]
                    )
                    # Sauvegarde des params et résultats
                    query = "insert into tests.resultats (region, indicators, params, resultat, date_maj) values ($1, $2, $3, $4, now())"
                    res = await controller.fetch(
                        query,
                        region,
                        id,
                        json.dumps(url_params[0]),
                        # we remove nan values causing postgresql to crash
                        json.dumps(terrivalues, default=str).replace(" NaN", ' "NaN"'),
                    )
            else:
                logger.info(
                    "[{region}] Résultats déjà calculés l'indicateur {nom} ({id})".format(
                        region=region, nom=nom, id=id
                    )
                )
    return


@pytest.fixture
async def common_data(app):
    """
    Insert a new static file for region pytest inside `meta.static_files` table.
    """
    table_name = "pytest_temp_table"
    await controller.execute(
        "DELETE FROM meta.perimetre_geographique WHERE region = 'france' and nom_table = $1",
        table_name,
    )
    await controller.fetch("DROP TABLE IF EXISTS france.pytest_temp_table")

    query = f"""create table france.{table_name} (
      commune varchar(5),
      annee int,
      valeur float
    )
    """
    await controller.execute(query)
    query = f"""insert into france.{table_name} VALUES 
        ('00000', 2000, 10.0),
        ('00000', 2001, 20.0),
        ('00000', 2002, 30.0),
        ('00000', 2003, 40.0)
        """
    await controller.execute(query)
    query = """insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
    VALUES ($1, 'france', 2022, false)"""
    await controller.execute(query, table_name)
    yield table_name

    await controller.execute(
        "DELETE FROM meta.perimetre_geographique WHERE region = 'france' and nom_table = $1",
        table_name,
    )
    await controller.fetch("DROP TABLE france.pytest_temp_table")


@pytest.fixture
async def data_for_perimeter_conversion(app, region, client_anonymous):
    """Creates the necessary tables to test perimeter conversion.

    Relies on the 'pytest' region created by the fixtures 'region' and 'indicateur'.

    Tables created in the 'pytest' schema:
    'test_data' : a valid and convertible data table
    'flow_data' : a flow data table that isn't convertible
    'pixel_data' : a pixel data table that isn't convertible
    'epci_data' : a data table at the level 'epci' that isn't convertible

        Yields
    ------
    str
        name of the data table created
    """
    conversion_table_test_schema = SCHEMA + "_table_passage"
    await controller.execute(
        """DELETE FROM meta.perimetre_geographique WHERE region = $1 AND nom_table IN ('test_data', 'flow_data', 'pixel_data', 'epci_data')""",
        SCHEMA,
    )
    await controller.execute(
        f"DROP SCHEMA IF EXISTS {conversion_table_test_schema} CASCADE"
    )
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.test_data")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.flow_data")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.pixel_data")
    await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.epci_data")

    # create a valid and convertible data table
    query = f"""create table {SCHEMA}.test_data (
        commune varchar(5)
        , valeur float
        , cat1 int
        , cat2 int
        , annee int
        )
        """
    await controller.execute(query)
    # populate data table
    insert_data_query = f"""insert into {SCHEMA}.test_data VALUES
            ('00001', 3, 1, 2, 2020),
            ('00001', 32, 1, 3, 2020),
            ('00001', 3, 2, 4, 2020),
            ('00002', 7, 1, 2, 2020),
            ('00002', 4, 2, 4, 2020),
            ('00003', 123.45, 1, 2, 2020),
            ('00003', 1, 2, 4, 2020),
            ('00004', 457, 1, 2, 2020),
            ('00005', 348, 1, 2, 2020),
            ('00006', 100, 1, 2, 2020),
            ('00007', 50, 1, 2, 2020),
            ('00008', 33, 1, 2, 2020),
            ('00009', 223, 1, 2, 2020),
            ('00001', 23.3, 1, 2, 2019),
            ('00002', 6.7, 1, 2, 2019)
            """
    await controller.execute(insert_data_query)
    # we associate the data table with a perimeter
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "test_data")

    # create a flow data table (with column 'commune_dest')
    query = f"""create table {SCHEMA}.flow_data (
        commune varchar(5)
        , commune_dest varchar(5)
        , valeur float
        , valeur_filtre float
        , cat1 int
        , cat2 int
        , annee int
        )
        """
    await controller.execute(query)
    # populate data table
    insert_data_query = f"""insert into {SCHEMA}.flow_data VALUES
            ('00001', '00002', 3, 4, 1, 2, 2020);
            """
    await controller.execute(insert_data_query)
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "flow_data")

    # create a pixel data table (with column 'geom')
    query = f"""create table {SCHEMA}.pixel_data (
        commune varchar(5)
        , geom public.geometry(multipolygon, 3857) NULL
        , valeur float
        , id_carreau varchar(254) NULL
        , annee int
        )
        """
    await controller.execute(query)
    # populate data table
    insert_data_query = f"""insert into { SCHEMA }.pixel_data
    VALUES (
        '03159',
        'SRID=3857;MULTIPOLYGON (((304349.1591771252 5828719.514326057, 304335.6064733846 5828863.843653444, 304479.39924740745 5828878.011531962, 304492.9495815964 5828733.681699297, 304349.1591771252 5828719.514326057)))',
        100629.00232908141,
        '100mN26038E37616',
        2020
    );
    """
    await controller.execute(insert_data_query)
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "pixel_data")

    # create a data table at the level 'epci' (no 'commune' column)
    query = f"""create table {SCHEMA}.epci_data (
        epci varchar(9)
        , valeur float
        , valeur_filtre float
        , cat1 int
        , cat2 int
        , annee int
        )
        """
    await controller.execute(query)
    # populate data table
    insert_data_query = f"""insert into {SCHEMA}.epci_data VALUES
            ('200123354', 3, 3, 1, 2, 2020);
            """
    await controller.execute(insert_data_query)
    insert_perimeter = f"""insert into meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre)
        VALUES ($1, 'pytest', 2020, false)"""
    await controller.execute(insert_perimeter, "epci_data")

    # Create test schema for conversion tables
    await controller.execute(f"create schema {conversion_table_test_schema};")
    # create 2020 => 2021 conversion tables
    query_create_conversion_table = f"""CREATE TABLE {conversion_table_test_schema}.passage_2020_2021 (
        cod2020 varchar,
        cod2021 varchar,
        annee varchar,
        typemodif varchar,
        ratio varchar);
        """
    await controller.execute(query_create_conversion_table)
    insert_data_query = f"""insert into {conversion_table_test_schema}.passage_2020_2021 VALUES
            ('00001', '00001', '2021', 'f','1'),
            ('00002', '00001', '2021', 'f','1'),
            ('00003', '00003', '2021', 'f', '1'),
            ('00004', '00003', '2021', 'f', '1'),
            ('00005', '00003', '2021', 'f', '1'),
            ('00006', '00007', '2021', 'd', '0.6'),
            ('00006', '00008', '2021', 'd', '0.4'),
            ('00009', '00010', '2021', 'c', '1')
            """
    await controller.execute(insert_data_query)
    # create second conversion table
    query_create_conversion_table = f"""CREATE TABLE {conversion_table_test_schema}.passage_2021_2022 (
    cod2021 varchar,
    cod2022 varchar,
    annee varchar,
    typemodif varchar,
    ratio varchar);
    """
    await controller.execute(query_create_conversion_table)
    insert_data_query = f"""insert into {conversion_table_test_schema}.passage_2021_2022 VALUES
            ('00001', '00001', '2022', 'f', '1'),
            ('00003', '00001', '2022', 'f', '1'),
            ('00007', '00008', '2022', 'd', '0.3'),
            ('00007', '00001', '2022', 'd', '0.3'),
            ('00007', '00010', '2022', 'd', '0.4')
            """
    await controller.execute(insert_data_query)

    yield True

    # Drop the created tables and schema
    for table_name in ["test_data", "flow_data", "pixel_data"]:
        await controller.execute(
            f"DELETE FROM meta.perimetre_geographique WHERE region = 'pytest' and nom_table = '{table_name}'"
        )
        await controller.execute(f"DROP TABLE IF EXISTS {SCHEMA}.{table_name} CASCADE")

    await controller.execute(
        f"DROP SCHEMA IF EXISTS {conversion_table_test_schema} CASCADE"
    )


@pytest.fixture
async def zones(app, region):
    """Inserts a few zones for pytest region."""
    data = [
        {
            "nom": "region",
            "maille": "commune",
            "libelle": "Region - commune",
            "ordre": "1",
            "hide": "false",
        },
        {
            "nom": "region",
            "maille": "epci",
            "libelle": "Region - epci",
            "ordre": "2",
            "hide": "false",
        },
        {
            "nom": "epci",
            "maille": "epci",
            "libelle": "epci - epci",
            "ordre": "3",
            "hide": "false",
        },
        {
            "nom": "commune",
            "maille": "commune",
            "libelle": "Maille communale",
            "ordre": "4",
            "hide": "true",
        },
    ]

    rows = []
    for row in data:
        rows.append("('" + "', '".join([row[key] for key in data[0].keys()]) + "')")
    values = ", ".join(rows)
    query = f"""INSERT INTO pytest.zone 
    ({", ".join(data[0].keys())})
    VALUES
        {values}
    ;"""
    await controller.execute(query)
    yield data
    await controller.fetch("TRUNCATE pytest.zone;")
