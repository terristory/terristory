# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import random
import string

import pytest
from sanic.log import logger
from sanic_testing.reusable import ReusableClient

from terriapi import controller
from terriapi.controller.user import create_user

from .common import SCHEMA


def generate_random_pass():
    letters = string.ascii_letters + string.digits
    random_string = "".join(random.choice(letters) for _ in range(20))
    return random_string


# données utilisateur pour le test.
JOHN = {
    "prenom": "John",
    "nom": "Doe",
    "password": generate_random_pass(),
    "mail": "pytest@terristory-nowhere.dev",
    "organisation": "Consortium",
    "fonction": "dev",
    "territoire_predilection": '{"zoneType":"region", "zoneMaille":"epci"}',
    "utiliser_territoire_predilection": False,
    "territoire": "01001",
    "region": "pytest",
    "profil": "utilisateur",
    "actif": True,
    "rgpd": "",
}
JOHN_2 = {
    "prenom": "John 2",
    "nom": "Doe 2",
    "password": generate_random_pass(),
    "mail": "pytest2@terristory-nowhere.dev",
    "organisation": "Consortium",
    "fonction": "dev",
    "territoire_predilection": '{"zoneType":"region", "zoneMaille":"epci"}',
    "utiliser_territoire_predilection": False,
    "territoire": "01002",
    "region": "pytest",
    "profil": "utilisateur",
    "actif": True,
    "rgpd": "",
}
ALICE_IN_AURA = {
    "prenom": "John",
    "nom": "Doe",
    "password": generate_random_pass(),
    "mail": "alice@terristory-wonder.land",
    "organisation": "Consortium",
    "fonction": "dev",
    "territoire_predilection": '{"zoneType":"region", "zoneMaille":"epci"}',
    "utiliser_territoire_predilection": False,
    "territoire": "01001",
    "region": "auvergne-rhone-alpes",
    "profil": "utilisateur",
    "actif": True,
    "rgpd": "",
}
ALICE = {
    "prenom": "Alice",
    "nom": "Caroll",
    "password": generate_random_pass(),
    "mail": "alice@terristory-wonder.land",
    "organisation": "Lewis Inc.",
    "fonction": "dreamer",
    "territoire_predilection": '{"zoneType":"region", "zoneMaille":"epci"}',
    "utiliser_territoire_predilection": False,
    "territoire": "01002",
    "region": "pytest",
    "profil": "admin",
    "actif": True,
    "rgpd": "",
}
BOB = {
    "prenom": "Bob",
    "nom": "Bib",
    "password": generate_random_pass(),
    "mail": "bob@terristory-wonder.land",
    "organisation": "AURA-EE",
    "fonction": "Boss",
    "territoire_predilection": '{"zoneType":"region", "zoneMaille":"epci"}',
    "utiliser_territoire_predilection": False,
    "territoire": "01003",
    "region": "auvergne-rhone-alpes",
    "profil": "admin",
    "global_admin": True,
    "actif": True,
    "rgpd": "",
}


def gestion_session_utilisateur(loop, request, db, region, client, user):
    """
    Create a test user, add it to the database and delete it when the test is done.
    """

    def delete_pytest_data():
        query = """
        DELETE FROM utilisateur_scenario
        where mail = $1 AND region = $2;
        """
        loop.run_until_complete(db.execute(query, user["mail"], user["region"]))
        query = "DELETE FROM utilisateur where mail = $1 and region = $2"
        loop.run_until_complete(db.execute(query, user["mail"], user["region"]))

    delete_pytest_data()

    # we copy to avoid problems while deleting the global_admin key
    _user = user.copy()

    # first we check we need to register as global admin
    is_global_admin = _user.get("global_admin", False)
    if "global_admin" in _user:
        del _user["global_admin"]

    saved_db = controller.db
    controller.db = db
    # we create the user
    loop.run_until_complete(create_user(**_user))
    controller.db = saved_db

    # if it was indeed a global admin, we update the db
    if is_global_admin:
        loop.run_until_complete(
            db.execute(
                "UPDATE utilisateur SET global_admin = true WHERE mail = $1 and region = $2",
                _user["mail"],
                _user["region"],
            )
        )
    # La fonction + 'addfinalizer' permettent d'effacer l'utilisateur test quand (1)
    # on sort de la fonction test ou (2) qu'une exception est déclenchée.
    # request.addfinalizer(delete_pytest_data)
    # récupération des tokens d'accès pour l'authentification JWT.
    req, resp = client.post(
        f"/api/{_user['region']}/auth",
        json={"login": _user["mail"], "password": _user["password"]},
    )
    tokens = resp.json
    return tokens


class MockedClient:
    def __init__(self, client: ReusableClient, tokens: dict = {}):
        self.client = client
        self.tokens = tokens

    @property
    def session(self):
        return self.client._session

    def _enable_auth(self):
        self.client._session.cookies.clear()
        self.client._session.cookies.update(self.tokens)

    def set_cookies(self, cookies):
        self.tokens = {**self.tokens, **cookies}

    def clear_cookies(self):
        self.tokens = {}

    def _parse_parameters(self, args, kwargs):
        kwargs["timeout"] = kwargs.get("timeout", None)
        return args, kwargs

    def get(self, *args, **kwargs):
        self._enable_auth()
        args, kwargs = self._parse_parameters(args, kwargs)
        return self.client.get(*args, **kwargs)

    def post(self, *args, **kwargs):
        self._enable_auth()
        args, kwargs = self._parse_parameters(args, kwargs)
        return self.client.post(*args, **kwargs)

    def put(self, *args, **kwargs):
        self._enable_auth()
        args, kwargs = self._parse_parameters(args, kwargs)
        return self.client.put(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self._enable_auth()
        # args, kwargs = self._parse_parameters(args, kwargs)
        return self.client.delete(*args, **kwargs)


@pytest.fixture()
def client_basic_user(request, app, db, global_ctx, loop, basic_client):
    """Fixture qui retourne une instance de client web de test pour **chaque** fonction test."""
    # on crée un client web. Permet de démarrer un serveur de test sur l'app
    # Sanic. Permet aussi d'appeler le setup_db pour assigner un Pool de connexions à
    # 'controller.db'
    tokens = gestion_session_utilisateur(loop, request, db, SCHEMA, basic_client, JOHN)
    # on retourne une instance du client web de test.
    client = MockedClient(basic_client, tokens)
    yield client


@pytest.fixture()
def client_second_basic_user(request, app, db, global_ctx, loop, basic_client):
    """Fixture qui retourne une instance de client web de test pour **chaque** fonction test."""
    # on crée un client web. Permet de démarrer un serveur de test sur l'app
    # Sanic. Permet aussi d'appeler le setup_db pour assigner un Pool de connexions à
    # 'controller.db'
    tokens = gestion_session_utilisateur(
        loop, request, db, SCHEMA, basic_client, JOHN_2
    )
    # on retourne une instance du client web de test.
    client = MockedClient(basic_client, tokens)
    yield client


@pytest.fixture()
def client_basic_user_in_aura(request, app, db, global_ctx, loop, basic_client):
    """Fixture qui retourne une instance de client web de test pour **chaque** fonction test."""
    # on crée un client web. Permet de démarrer un serveur de test sur l'app
    # Sanic. Permet aussi d'appeler le setup_db pour assigner un Pool de connexions à
    # 'controller.db'
    tokens = gestion_session_utilisateur(
        loop, request, db, SCHEMA, basic_client, ALICE_IN_AURA
    )
    # on retourne une instance du client web de test.
    client = MockedClient(basic_client, tokens)
    yield client


@pytest.fixture()
def client_admin_user(request, app, db, global_ctx, loop, basic_client):
    """Fixture qui retourne une instance de client web de test pour **chaque** fonction test."""
    # on crée un client web. Permet de démarrer un serveur de test sur l'app
    # Sanic. Permet aussi d'appeler le setup_db pour assigner un Pool de connexions à
    # 'controller.db'
    tokens = gestion_session_utilisateur(loop, request, db, SCHEMA, basic_client, ALICE)
    # on retourne une instance du client web de test.
    client = MockedClient(basic_client, tokens)
    yield client


@pytest.fixture()
def client_global_admin_user(request, app, db, global_ctx, loop, basic_client):
    """Fixture qui retourne une instance de client web de test pour **chaque** fonction test."""
    # on crée un client web. Permet de démarrer un serveur de test sur l'app
    # Sanic. Permet aussi d'appeler le setup_db pour assigner un Pool de connexions à
    # 'controller.db'
    tokens = gestion_session_utilisateur(loop, request, db, SCHEMA, basic_client, BOB)
    # on retourne une instance du client web de test.
    client = MockedClient(basic_client, tokens)
    yield client


@pytest.fixture()
def basic_client(app, loop):
    """Fixture utilisée pour un client_basic_user sans authentification."""

    port = random.randint(5000, 65000)
    nb_tries = 0
    while nb_tries < 20:
        try:
            logger.info("Trying to start client on port %s", port)
            client = ReusableClient(app, loop=loop, port=port)
            with client:
                yield client
            break
        except OSError:
            port += 1
            port = port % 60000 + 5000
            nb_tries += 1


@pytest.fixture()
def client_anonymous(app, loop, basic_client):
    """Fixture utilisée pour un client_basic_user sans authentification."""
    client = MockedClient(basic_client)
    yield client
