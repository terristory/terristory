# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import copy

import pytest

from terriapi.controller import donnees_pour_representation as data_controller

# Representations
# DonneesPourRepresentations
# DonneesPourRepresentationCartoIndicateurFiltre
# DonneesPourRepresentationCartoIndicateurSimple
# DonneesPourRepresentationCartoIndicateurFlux
# DonneesPourRepresentationCartoIndicateurDataRatio
# DonneesPourRepresentationCartoIndicateurAutreMaille
# DonneesPourRepresentationCartoMoyennePonderee
# DonneesPourRepresentationCartoIndicateurClimat
# DonneesPourRepresentationDiagramme
# DonneesPourRepresentationDiagrammeCategoriesIndependantes
# DonneesPourRepresentationCourbesEmpilees
# DonneesPourRepresentationJauge
# DonneesPourRepresentationJaugeCirculaire
# DonneesPourRepresentationHistogramme
# DonneesPourRepresentationHistogrammeIndicateurUnique
# DonneesPourRepresentationHistogrammeDataRatio
# DonneesPourRepresentationMarqueurSVG
# DonneesPourRepresentationPixels
# DonneesPourRepresentationPixelsCategory
# DonneesPourRepresentationNomSimple
# FabriqueRepresentations
# FabriqueRepresentationsTableauBord
CONF = {
    "nom": "Consommation d'énergie",
    "data": "conso_energetique",
    "type": "circle",
    "color_start": "#007f7F",
    "color_end": "#007f7F",
    "unit": "GWh",
    "disabled_for_zone": "",
    "only_for_zone": "",
    "isratio": False,
    "filter": "",
    "decimals": 0,
    "data_ratio": None,
    "years": [2000, 2001, 2002, 2003],
    "region": "pytest",
    "data_deuxieme_representation": None,
    "valeur_filtre_defaut": None,
    "concatenation": None,
    "data_type": None,
    "moyenne_ponderee": None,
    "afficher_calcul_et_donnees_table": None,
    "titre_graphiques_indicateurs": None,
    "disabled_for_macro_level": "",
    "charts": [
        {
            "type": "pie",
            "ordre": 1,
            "titre": "Par secteur",
            "visible": True,
            "categorie": "secteur",
            "data_type": None,
            "indicateur": 1,
        },
        {
            "type": "pie",
            "ordre": 2,
            "titre": "Par energie",
            "visible": True,
            "categorie": "energie",
            "data_type": None,
            "indicateur": 1,
        },
    ],
    "definition_indicateur": {
        "indicateur": "conso_energetique",
        "coeff": "",
        "ratio": "",
    },
    "schema_region": "pytest",
    "filtre_initial_modalites_par_categorie": {
        "secteur": [
            {"filtre_categorie": "secteur.Résidentiel"},
            {"filtre_categorie": "secteur.Transport"},
        ],
        "energie": [
            {"filtre_categorie": "energie.Électricité"},
            {"filtre_categorie": "energie.Gaz"},
        ],
    },
}

zone = "departement"
maille = "commune"
zone_id = "01"


def _p(id):
    c = copy.deepcopy(CONF)
    c["id"] = id
    c["charts"][0]["indicateur"] = id
    c["charts"][1]["indicateur"] = id
    return c


def test_basic(region, indicators, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 for all sectors => cf. indicators fixtures
    assert results["map"][0]["val"] == 60.0

    # test without specific year
    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )

    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2003 for all sectors => cf. indicators fixtures
    assert results["map"][0]["val"] == 80.0


def test_basic_confid(region, indicators, confid, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["confidentiel"] = "conso_energetique"

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 for all sectors  => cf. indicators fixtures
    assert results["map"][0]["val"] == "confidentielle"


def test_circ_diagram_confid(region, indicators, confid, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["confidentiel"] = "conso_energetique"
    _zone_id = "02"
    filters = {
        "secteur": [
            {"filtre_categorie": "secteur.Résidentiel"},
            {"filtre_categorie": "secteur.Transport"},
        ],
        "energie": [
            {"filtre_categorie": "energie.Électricité"},
            {"filtre_categorie": "energie.Gaz"},
        ],
    }

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, _zone_id, specific_year=2000
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            filters,
            False,
        )
    )
    assert "confid" in results
    assert results["confid"]["charts"] == "A"
    # sector
    assert "confidentiel" not in results["charts"][0]["data"]
    # energy
    assert "confidentiel" not in results["charts"][1]["data"]

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, _zone_id, specific_year=2001
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            filters,
            False,
        )
    )
    assert "confid" in results
    assert results["confid"]["charts"] == "B"
    # sector
    assert "confidentiel" not in results["charts"][0]["data"]
    # energy
    assert "confidentiel" in results["charts"][1]["data"]

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, _zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            filters,
            False,
        )
    )
    assert "confid" in results
    assert results["confid"]["charts"] == "C"
    # sector
    assert "confidentiel" in results["charts"][0]["data"]
    # energy
    assert "confidentiel" not in results["charts"][1]["data"]

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, _zone_id, specific_year=2003
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            filters,
            False,
        )
    )
    assert "confid" in results
    assert results["confid"]["charts"] == "D"
    # sector
    assert "confidentiel" in results["charts"][0]["data"]
    # energy
    assert "confidentiel" in results["charts"][1]["data"]


def test_circ_filter_diagram_confid(region, indicators, confid, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["confidentiel"] = "conso_energetique"
    _zone_id = "02"

    # when we have full filters => no confidentiality
    filters = {
        "secteur": [
            {"filtre_categorie": "secteur.Résidentiel"},
            {"filtre_categorie": "secteur.Transport"},
        ],
        "energie": [
            {"filtre_categorie": "energie.Électricité"},
            {"filtre_categorie": "energie.Gaz"},
        ],
    }

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, _zone_id, specific_year=2000
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            filters,
            False,
        )
    )
    assert "confid" in results
    assert results["confid"]["charts"] == "A"
    # sector
    assert "confidentiel" not in results["charts"][0]["data"]
    # energy
    assert "confidentiel" not in results["charts"][1]["data"]

    # if we change anything in the filters => confidentiality
    filters = {
        "secteur": [
            {"filtre_categorie": "secteur.Résidentiel"},
        ],
        "energie": [
            {"filtre_categorie": "energie.Gaz"},
        ],
    }

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, _zone_id, specific_year=2000
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            filters,
            False,
        )
    )
    assert "confid" in results
    assert results["confid"]["charts"] == "D"
    # sector
    assert "confidentiel" in results["charts"][0]["data"]
    # energy
    assert "confidentiel" in results["charts"][1]["data"]


def test_user_filter(region, indicators, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            {
                "secteur": [
                    {"filtre_categorie": "secteur.Résidentiel"},
                ]
            },
            False,
        )
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 for all sectors  => cf. indicators fixtures
    assert results["map"][0]["val"] == 30.0

    # test with all unchecked
    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur", 0, {"secteur": []}, False
        )
    )

    assert "map" in results
    assert len(results["map"]) == 1
    # has filtered every sector
    assert results["map"][0]["val"] == 0.0


def test_default_filter(region, indicators, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["filter"] = "secteur.Résidentiel"

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            "indicateur",
            None,
            {
                "secteur": [
                    {"filtre_categorie": "secteur.Résidentiel"},
                ]
            },
            False,
        )
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 for all sectors  => cf. indicators fixtures
    assert results["map"][0]["val"] == 30.0


@pytest.mark.parametrize(
    "disabled_for_macro_level,specific_year,check_result",
    [
        ["departement,epci", 2002, None],
        ["departement", 2002, None],
        ["commune", None, 80.0],
    ],
)
def test_level_disabled(
    disabled_for_macro_level, specific_year, check_result, region, indicators, loop
):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["disabled_for_macro_level"] = disabled_for_macro_level

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=specific_year
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )

    if check_result is None:
        assert results == {}
    else:
        assert "map" in results
        assert len(results["map"]) == 1
        # value for 2003 for all sectors  => cf. indicators fixtures
        assert results["map"][0]["val"] == check_result


@pytest.mark.parametrize(
    "only_for_zone,specific_year,check_result",
    [
        ["departement,epci", 2002, None],
        ["departement", 2002, None],
        ["commune", None, 40.0],
    ],
)
def test_level_limitations(
    only_for_zone, specific_year, check_result, region, indicators, loop
):
    _, added_indicators_id = indicators

    # we test with right only_for_zone
    conf = _p(added_indicators_id[0])
    conf["only_for_zone"] = only_for_zone

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=specific_year
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )

    if check_result is None:
        assert results == {}
    else:
        assert "map" in results
        assert len(results["map"]) == 1
        # value for 2003 for all sectors  => cf. indicators fixtures
        assert float(results["map"][0]["val"]) == check_result


@pytest.mark.parametrize(
    "disabled_for_zone,check_result",
    [["commune", None], ["commune,epci", None], ["departement,epci", 80.0]],
)
def test_maille_limitations(disabled_for_zone, check_result, region, loop, indicators):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["disabled_for_zone"] = disabled_for_zone

    # test without specific year
    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )

    if check_result is None:
        assert results == {}
    else:
        assert "map" in results
        assert len(results["map"]) == 1
        # value for 2002 for all sectors  => cf. indicators fixtures
        assert results["map"][0]["val"] == check_result


def test_weighted_average(region, indicators, confid, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[5])
    conf["moyenne_ponderee"] = True
    conf["data"] = "weighted_data"
    conf["filtre_initial_modalites_par_categorie"] = {
        "hebdomadaire": [
            {"filtre_categorie": "hebdomadaire.Longue distance"},
            {"filtre_categorie": "hebdomadaire.Courte distance"},
        ],
        "quotidien": [
            {"filtre_categorie": "quotidien.Longue distance"},
            {"filtre_categorie": "quotidien.Courte distance"},
        ],
    }
    conf["definition_indicateur"]["indicateur"] = "weighted_data"
    conf["charts"][0] = {
        "type": "pie",
        "ordre": 1,
        "titre": "Recours hebdomadaire",
        "visible": True,
        "categorie": "hebdomadaire",
        "data_type": None,
        "indicateur": added_indicators_id[5],
    }
    conf["charts"].append(
        {
            "type": "pie",
            "ordre": 2,
            "titre": "Recours quotidien",
            "visible": True,
            "categorie": "quotidien",
            "data_type": None,
            "indicateur": added_indicators_id[5],
        }
    )

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 => cf. indicators fixtures at weighted_data definition
    assert results["map"][0]["val"] == 45.0  # (30 + 60) / 2


def test_second_representation(region, indicators, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["data_deuxieme_representation"] = "valeur"

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    assert len(results["map"]) == 1
    assert "donnees_deuxieme_representation" in results
    assert "total" in results
    assert "bornes_filtre" in results
    assert "val_proportion" in results["total"]
    # value for 2002 => cf. indicators fixtures
    assert results["map"][0]["val"] == 60.0

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2002
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 40, {}, False)
    )
    # As the filter value used is higher than any value at city level => 0
    assert results["map"][0]["val"] == 0.0


def test_accessibility_analysis(region, indicators, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[6])
    conf["conso_energetique"] = "job_accessibility"
    conf["data_deuxieme_representation"] = "valeur_filtre"
    conf["definition_indicateur"]["indicateur"] = "job_accessibility"
    conf["data_type"] = "accessibilite_emploi"
    conf["type"] = "circle"
    conf["charts"] = [
        {
            "type": "histogramme",
            "ordre": None,
            "titre": "Par catégorie socio-professionnelle",
            "visible": True,
            "categorie": "csp",
            "data_type": None,
            "indicateur": added_indicators_id[6],
        }
    ]
    conf["filtre_initial_modalites_par_categorie"] = {
        "csp": [
            {"filtre_categorie": "csp.Workers"},
            {"filtre_categorie": "csp.Other workers"},
        ]
    }
    # job_accessibility
    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id, specific_year=2000
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    # sum of all workers in all categories and all type of jobs/distance
    assert results["map"][0]["nbre_actifs"] == 250
    assert results["map"][0]["val_filtre"] == 1800  # ?
    assert "donnees_par_categorie" in results
    assert "csp" in results["donnees_par_categorie"]
    # both csp values are equal to 600
    assert results["donnees_par_categorie"]["csp"][0]["val_filtre"] == 600
    assert results["donnees_par_categorie"]["csp"][1]["val_filtre"] == 1200

    _zone = "region"
    _maille = "epci"
    _zone_id = "1"
    # job_accessibility
    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, _maille, _zone, _zone_id, specific_year=2000
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    # sum of all workers in all categories and all type of jobs/distance
    assert results["map"][0]["nbre_actifs"] == 150
    assert results["map"][0]["val"] == 500  # ?
    assert "donnees_par_categorie" in results
    assert "csp" in results["donnees_par_categorie"]
    # both csp values are equal to 600
    assert results["donnees_par_categorie"]["csp"][0]["val_filtre"] == 300
    assert results["donnees_par_categorie"]["csp"][1]["val_filtre"] == 600


def test_basic_regional_level(region, indicators, loop):
    _, added_indicators_id = indicators

    _zone = "region"
    _maille = "commune"
    _zone_id = "1"
    conf = _p(added_indicators_id[0])

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, _maille, _zone, _zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 for all sectors at regional level => cf. indicators fixtures
    assert results["map"][0]["val"] == 80.0


def test_basic_only_for_zone(region, indicators, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[7])
    conf["only_for_zone"] = "epci"
    # conf["data"] = "conso_energetique_epci"
    # conf["definition_indicateur"]["indicateur"] = "conso_energetique_epci"

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert len(results) == 0

    _zone = "region"
    _maille = "epci"
    _zone_id = "1"

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, _maille, _zone, _zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 for all sectors at regional level => cf. indicators fixtures
    assert results["map"][0]["val"] == 40.0


@pytest.mark.parametrize(
    "zone, maille, zone_id",
    [
        [zone, maille, zone_id],
        ["region", "epci", "1"],
    ],
)
def test_ratio(zone, maille, zone_id, region, indicators, loop):
    _, added_indicators_id = indicators

    conf = _p(added_indicators_id[0])
    conf["ratio"] = True
    conf["data"] = "conso_energetique/maille.prod_enr"
    conf["definition_indicateur"]["indicateur"] = "conso_energetique"
    conf["definition_indicateur"]["ratio"] = "/maille.prod_enr"

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert "map" in results
    assert len(results["map"]) == 1
    # value for 2002 => as we made the ratio and data are strictly equals (cf. indicators fixtures.py)
    # we have 1.0 value
    assert results["map"][0]["val"] == 1.0


def test_non_existing_graph_mode(region, indicators, loop):
    _, added_indicators_id = indicators
    conf = _p(added_indicators_id[0])
    donnees_pour_representations = data_controller.FabriqueRepresentationsTableauBord(
        conf, maille, zone, zone_id, "non_existing_graph_mode"
    )
    with pytest.raises(ValueError) as excp:
        loop.run_until_complete(
            donnees_pour_representations.donnees_finales("tableau_de_bord", {})
        )
    assert str(excp.value) == "Invalid representation mode"


@pytest.mark.parametrize(
    "zone, maille, zone_id",
    [
        [zone, maille, zone_id],
        ["region", "epci", "1"],
    ],
)
@pytest.mark.parametrize(
    "representation,is_data_ratio,check_key,result_is_dict",
    [
        ["line", False, "analyse", True],
        ["line-years", False, "analyse", True],
        ["pie", False, "charts", True],
        ["jauge", False, "svg", True],
        ["jauge", True, "svg", True],
        ["jauge-circulaire", False, "analyse", False],
        ["jauge-circulaire", True, "analyse", False],
        ["histogramme-data-ratio", True, "indicateur", True],
        ["marqueur-svg", False, "valeur", True],
        ["analysis-launcher", False, "id_indicateur", True],
    ],
)
def test_all_graphs(
    zone,
    maille,
    zone_id,
    representation,
    is_data_ratio,
    check_key,
    result_is_dict,
    region,
    indicators,
    loop,
):
    _, added_indicators_id = indicators
    conf = _p(added_indicators_id[0])

    # if we need to apply a data ratio here
    if is_data_ratio:
        conf["data_ratio"] = added_indicators_id[1]

    donnees_pour_representations = data_controller.FabriqueRepresentationsTableauBord(
        conf, maille, zone, zone_id, representation
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("tableau_de_bord", {})
    )
    if result_is_dict:
        assert len(results) > 0
        assert check_key in results
    else:
        assert results > 0

    # TOFIX: It doesn't really make sense to have a double ratio definition
    if is_data_ratio:
        return ""

    conf = _p(added_indicators_id[0])
    conf["ratio"] = True
    conf["data"] = "conso_energetique/maille.prod_enr"
    conf["definition_indicateur"]["indicateur"] = "conso_energetique"
    conf["definition_indicateur"]["ratio"] = "/maille.prod_enr"

    donnees_pour_representations = data_controller.FabriqueRepresentationsTableauBord(
        conf, maille, zone, zone_id, representation
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("tableau_de_bord", {})
    )
    if result_is_dict:
        assert len(results) > 0
        assert check_key in results
    else:
        assert results > 0


def test_graph_historical_curves(region, indicators, loop):
    representation = "courbes-historiques"

    _, added_indicators_id = indicators
    conf = _p(added_indicators_id[0])
    conf["data_ratio"] = added_indicators_id[1]

    donnees_pour_representations = data_controller.FabriqueRepresentationsTableauBord(
        conf, maille, zone, zone_id, representation
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("tableau_de_bord", {})
    )
    assert len(results) > 0
    assert added_indicators_id[0] in results
    assert added_indicators_id[1] in results


@pytest.mark.parametrize(
    "representation",
    ["pie", "line", "bar"],
)
def test_category_order(region, indicators, representation, loop):
    _, added_indicators_id = indicators
    conf = _p(added_indicators_id[0])

    donnees_pour_representations = data_controller.FabriqueRepresentationsTableauBord(
        conf, maille, zone, zone_id, representation
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("tableau_de_bord", {})
    )

    if representation == "pie":
        labels = next(
            cat["labels"] for cat in results["charts"] if cat["name"] == "secteur"
        )
        assert labels[0] == "Transport"
        assert labels[1] == "Résidentiel"
    else:
        secteur = results["analyse"]["secteur"]
        for key in ["traj", "categories"]:
            assert secteur[key][0]["nom"] == "Transport"
            assert secteur[key][1]["nom"] == "Résidentiel"


def test_pixel_data(region, indicators, loop):
    _, added_indicators_id = indicators
    conf = _p(added_indicators_id[8])
    conf["type"] = "pixels"
    conf["data"] = "indicator_pixels_data"
    conf["definition_indicateur"]["indicateur"] = "indicator_pixels_data"
    conf["charts"] = []

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert len(results) > 0
    assert "min_max_valeurs" in results
    assert "min" in results["min_max_valeurs"]
    assert "max" in results["min_max_valeurs"]
    # cf. indicators fixtures.py => min value inserted inside pixels_data table for current city
    assert results["min_max_valeurs"]["min"] == 20.0
    assert results["min_max_valeurs"]["max"] == 30.0


def test_pixel_cat_data(region, indicators, loop):
    _, added_indicators_id = indicators
    conf = _p(added_indicators_id[8])
    conf["type"] = "pixels_cat"
    conf["data"] = "indicator_pixels_cat_data"
    conf["definition_indicateur"]["indicateur"] = "indicator_pixels_cat_data"
    conf["charts"] = []

    donnees_pour_representations = data_controller.DonneesPourRepresentations(
        conf, maille, zone, zone_id
    )
    results = loop.run_until_complete(
        donnees_pour_representations.donnees_finales("indicateur", 0, {}, False)
    )
    assert len(results) > 0
    assert "distinct_values" in results
    # cf. indicators fixtures.py => how many values were associated with pixels_data_cat in meta.categorie
    assert len(results["distinct_values"]) == 2
    assert "couleur" in results["distinct_values"][0]
