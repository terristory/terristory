﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import re
from pathlib import Path

import numpy as np
import pandas as pd
import pytest

from terriapi.controller import fetch
from terriapi.controller.donnees_pour_representation import (
    DonneesPourRepresentationCartoIndicateurFiltre,
    DonneesPourRepresentations,
)
from terriapi.controller.indicateur_tableau_bord import IndicateurTableauBord

here = Path(__file__).resolve().parent


@pytest.mark.test_migrations_pendulaires_communes_etrangers
def test_migrations_pendulaires_communes_etrangers(client_anonymous, loop):
    code_territoire = "240100750"
    echelle = "epci"
    maille = "commune"
    # TODO: generalize here and retrieve real ID instead of stating it
    id_indicateur = 33
    region = "auvergne-rhone-alpes"
    provenance = "carto"
    filtre = 0
    filtres_categorie = {}

    indicateur = IndicateurTableauBord(region, id_indicateur)
    metadonnees = loop.run_until_complete(indicateur.meta_donnees_indicateur())
    donnees_pour_representations = DonneesPourRepresentations(
        metadonnees, maille, echelle, code_territoire
    )
    resultats = loop.run_until_complete(
        donnees_pour_representations.donnees_finales(
            provenance, filtre, filtres_categorie
        )
    )
    nombre_communes_suisse = 0
    nombre_communes_allemagne = 0
    for donnee in resultats["map"]:
        if donnee["code_d"][0:2] == "SU":
            nombre_communes_suisse += 1
        if donnee["code_d"][0:2] == "AL":
            nombre_communes_allemagne += 1

    assert nombre_communes_suisse == 747
    assert nombre_communes_allemagne == 4


async def obtention_resultats(
    code_territoire,
    echelle,
    maille,
    id_indicateur,
    region,
    provenance,
    filtre,
    filtres_categorie,
    specific_year=None,
):
    indicateur = IndicateurTableauBord(region, id_indicateur)
    metadonnees = await indicateur.meta_donnees_indicateur()
    donnees_pour_representations = DonneesPourRepresentations(
        metadonnees, maille, echelle, code_territoire, specific_year=specific_year
    )
    resultats = await donnees_pour_representations.donnees_finales(
        provenance, filtre, filtres_categorie
    )
    return resultats


@pytest.mark.test_confid
def test_confid_covnm_type_d(client_anonymous, indicators, confid, loop):
    code_territoire = "01"
    echelle = "departement"
    maille = "commune"
    # TODO: generalize here and retrieve real ID instead of stating it
    _, added_indicators_id = indicators
    indicator_id = added_indicators_id[0]
    region = "pytest"
    provenance = "carto"
    year = 2002
    filtre = 0
    filtres_categorie = {}
    resultats = loop.run_until_complete(
        obtention_resultats(
            code_territoire,
            echelle,
            maille,
            indicator_id,
            region,
            provenance,
            filtre,
            filtres_categorie,
            specific_year=year,
        )
    )

    # we retrieve confidential EPCI
    schema = region.replace("-", "_")
    data_confid_communes = loop.run_until_complete(
        fetch(
            f"""SELECT DISTINCT c.code FROM {schema}.confid_maille c
        LEFT JOIN {schema}.{echelle} e ON c.code = ANY(e.communes)
        WHERE type_analyse = (SELECT data FROM meta.indicateur WHERE id = $3)
        AND e.code = $1
        AND annee = $2""",
            code_territoire,
            year,
            indicator_id,
        )
    )
    liste_communes_confidentielles = list(
        map(lambda x: x["code"], data_confid_communes)
    )

    assert resultats["confid"]["charts"] == "D"
    for i in resultats["map"]:
        if i["code"] in liste_communes_confidentielles:
            assert i["val"] == "confidentielle"
    l_territoire_confid = [
        x["code"] for x in resultats["map"] if x["val"] == "confidentielle"
    ]
    assert set(l_territoire_confid) == set(liste_communes_confidentielles)


@pytest.mark.parametrize(
    "year, answer, answer_after_filters, charts_disabled",
    [
        [2000, "A", "D", []],
        [2001, "B", "B", ["energie", "usage"]],
        [2002, "C", "C", ["secteur", "usage"]],
        [2003, "D", "D", []],
    ],
)
@pytest.mark.test_confid
def test_confid_conso_type_a(
    client_anonymous,
    indicators,
    confid,
    year,
    answer,
    answer_after_filters,
    charts_disabled,
    loop,
):
    code_territoire = "02"
    echelle = "departement"
    maille = "commune"
    # TODO: generalize here and retrieve real ID instead of stating it
    _, added_indicators_id = indicators
    indicator_id = added_indicators_id[0]
    region = "pytest"
    provenance = "carto"
    filtre = 0
    filtres_categorie = {
        "secteur": [
            {"filtre_categorie": "secteur.Résidentiel"},
            {"filtre_categorie": "secteur.Transport"},
        ],
        "energie": [
            {"filtre_categorie": "energie.Électricité"},
            {"filtre_categorie": "energie.Gaz"},
        ],
        "usage": [
            {"filtre_categorie": "usage.Chauffage"},
            {"filtre_categorie": "usage.Froid"},
        ],
        "other_cat": [
            {"filtre_categorie": "other_cat.Résidentiel"},
            {"filtre_categorie": "other_cat.Transport"},
        ],
    }

    resultats = loop.run_until_complete(
        obtention_resultats(
            code_territoire,
            echelle,
            maille,
            indicator_id,
            region,
            provenance,
            filtre,
            filtres_categorie,
            specific_year=year,
        )
    )
    assert resultats["confid"]["charts"] == answer

    filtres_categorie = {
        "secteur": [
            {"filtre_categorie": "secteur.Résidentiel"},
        ],
        "energie": [
            {"filtre_categorie": "energie.Électricité"},
        ],
        "usage": [
            {"filtre_categorie": "usage.Chauffage"},
        ],
        "other_cat": [
            {"filtre_categorie": "other_cat.Résidentiel"},
            {"filtre_categorie": "other_cat.Transport"},
        ],
    }
    resultats = loop.run_until_complete(
        obtention_resultats(
            code_territoire,
            echelle,
            maille,
            indicator_id,
            region,
            provenance,
            filtre,
            filtres_categorie,
            specific_year=year,
        )
    )
    assert resultats["confid"]["charts"] == answer_after_filters
    if len(charts_disabled) > 0:
        for chart_disabled in charts_disabled:
            charts = [c for c in resultats["charts"] if c["name"] == chart_disabled]
            assert len(charts) == 1, (
                "No chart available for column specified " + chart_disabled
            )
            chart = charts[0]
            assert set(chart["data"]) == {"confidentiel"}
    # TODO: check here that changing filters did change something?


@pytest.mark.test_confid
def test_confid_region_conso(client_anonymous, loop):
    # TODO: generalize here and retrieve real ID instead of stating it
    id_indicateur = 1
    region = "auvergne-rhone-alpes"
    code_territoire = "84"
    provenance = "carto"
    filtre = 0
    filtres_categorie = {
        "energie": [
            {"filtre_categorie": "energie.Combustibles Minéraux Solides"},
            {"filtre_categorie": "energie.Électricité"},
            {"filtre_categorie": "energie.EnR thermiques"},
            {"filtre_categorie": "energie.Non-énergétique"},
            {"filtre_categorie": "energie.Produits pétroliers"},
            {"filtre_categorie": "energie.Organo-carburants"},
            {"filtre_categorie": "energie.Gaz"},
            {"filtre_categorie": "energie.Non identifié"},
            {"filtre_categorie": "energie.Chauffage et froid urbain"},
            {"filtre_categorie": "energie.Déchets"},
        ],
        "secteur": [
            {"filtre_categorie": "secteur.Industrie hors branche énergie"},
        ],
        "usage": [
            {"filtre_categorie": "usage.Chauffage"},
            {"filtre_categorie": "usage.Eau Chaude Sanitaire"},
            {"filtre_categorie": "usage.Installations agricoles"},
            {"filtre_categorie": "usage.Cuisson"},
            {"filtre_categorie": "usage.Eclairage"},
            {"filtre_categorie": "usage.Froid"},
            {"filtre_categorie": "usage.Autres usages"},
            {"filtre_categorie": "usage.Climatisation"},
            {"filtre_categorie": "usage.Eclairage public"},
            {"filtre_categorie": "usage.Electricité spécifique"},
            {"filtre_categorie": "usage.Cheptels"},
            {"filtre_categorie": "usage.Cultures"},
            {"filtre_categorie": "usage.Engins agricoles"},
            {"filtre_categorie": "usage.Transport de marchandises"},
            {"filtre_categorie": "usage.Transport de personnes"},
            {"filtre_categorie": "usage.Industriel"},
            {"filtre_categorie": "usage.Matière première"},
        ],
    }
    echelle = "region"
    maille = "epci"
    year = 2019
    resultats = loop.run_until_complete(
        obtention_resultats(
            code_territoire,
            echelle,
            maille,
            id_indicateur,
            region,
            provenance,
            filtre,
            filtres_categorie,
            specific_year=year,
        )
    )

    # we retrieve confidential EPCI
    schema = region.replace("-", "_")
    data_confid_epci = loop.run_until_complete(
        fetch(
            f"""SELECT DISTINCT code FROM {schema}.confid_maille
        WHERE conditions->>'energie' = '*' AND conditions->>'secteur' = 'Industrie hors branche énergie'
        AND type_analyse = 'conso_energetique'
        AND type_territoire = $1
        AND annee = $2""",
            maille,
            year,
        )
    )
    liste_epci_confidentiels = list(map(lambda x: x["code"], data_confid_epci))

    for i in resultats["map"]:
        if i["code"] in liste_epci_confidentiels:
            assert i["val"] == "confidentielle"

    l_territoire_confid = [
        x["code"] for x in resultats["map"] if x["val"] == "confidentielle"
    ]
    assert set(l_territoire_confid) == set(liste_epci_confidentiels)


@pytest.fixture
def analysis_data(loop):
    return {
        "nomIndicateur": "This is a test indicator",
        "nomTable": " ",
        "filtre": " ",
        "parent": "undefined",
        "parentCategory": "{}",
        "type": "circle",
        "isRatio": "false",
        "couleurDebut": "#f13f13",
        "couleurFin": "#8c88c8",
        "representation_details": "{}",
        "unite": "GWh",
        "decimales": "2",
        "annees": "2013,2018-2021",
        "anneesEstimees": "2013",
        "year_selection_input_type": "selection",
        "theme": "new category",
        "categories": "[]",
        "confidentiel": " ",
        "donneesExportables": " ",
        "onlyForZone": " ",
        "disabledForZone": " ",
        "disabledForMacroLevel": " ",
        "disabled_in_dashboard": "false",
        "credits_analysis_producers": """[{"name":"Wikipédia","url":"https://wikipedia.org/"}]""",
        "credits_data_sources": "[]",
        "credits_data_producers": "[]",
        # default value
        "anneePerimetreGeographique": "undefined",
        # formula
        "dataCustom": "conso_energetique*0.001",
        "donneesExistantes": "true",
        "classification_config": "{}",
        "display_total": "true",
    }


def test_add_analysis_through_api(
    region,
    indicators,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    analysis_data,
    loop,
):
    # testing with wrong users
    _request, resp = client_anonymous.post("/api/pytest/analysis/ajouter")
    assert resp.status == 401
    _req, resp = client_basic_user.post("/api/pytest/analysis/ajouter")
    assert resp.status == 401
    # wrong region
    _req, resp = client_admin_user.post("/api/auvergne-rhone-alpes/analysis/ajouter")
    assert resp.status == 401

    # no data sent
    _req, resp = client_admin_user.post("/api/pytest/analysis/ajouter")
    assert resp.status == 400

    # right user, right region
    _req, resp = client_admin_user.post(
        "/api/pytest/analysis/ajouter", data=analysis_data
    )
    assert resp.status == 200
    content = resp.json
    assert "message" in content and content["message"] == "Indicateur ajouté"
    assert "id" in content
    indicator_id = content["id"]

    # test that the analysis is not visible yet
    _request, resp = client_anonymous.get("/api/pytest/analysis")
    assert resp.status == 200
    content = resp.json
    assert list(filter(lambda x: x["id"] == indicator_id, content)) == []

    # test that the analysis was successfully added
    _req, resp = client_admin_user.get("/api/pytest/analysis")
    assert resp.status == 200
    content = resp.json
    assert next(filter(lambda x: x["id"] == indicator_id, content))

    # wrong year data
    wrong_year_data = analysis_data.copy()
    wrong_year_data["annees"] = "aaaa"
    _req, resp = client_admin_user.post(
        "/api/pytest/analysis/ajouter", data=wrong_year_data
    )
    assert resp.status == 400
    content = resp.json
    assert "status" in content and content["status"] == "error"


def test_add_analysis_with_file_through_api(
    region, indicators, client_anonymous, client_admin_user, analysis_data, loop
):
    file_analysis_data = analysis_data.copy()
    file_analysis_data["nomIndicateur"] = "This is another test with csv data"
    file_analysis_data["donneesExistantes"] = "false"
    file_analysis_data["nomTable"] = "pytest_table_example"
    file_analysis_data["dataCustom"] = ""
    with open(here / "data/data_example.csv", "rb") as csv_file:
        # right user, right region
        _req, resp = client_admin_user.post(
            "/api/pytest/analysis/ajouter",
            data=file_analysis_data,
            files={"fichier": csv_file},
        )
        assert resp.status == 200
        content = resp.json
        assert "message" in content and content["message"] == "Indicateur ajouté"
        assert "id" in content

    _req, resp = client_admin_user.get(
        "/api/pytest/analysis/data/export/" + file_analysis_data["nomTable"]
    )
    assert resp.status == 200
    assert resp.content_type == "text/csv"
    csv_lines = resp.content.decode("utf-8").split("\n")
    assert len(csv_lines) == 8  # header + 6 lines of data + empty line


def test_update_analysis_through_api(
    region,
    indicators,
    client_anonymous,
    client_basic_user,
    client_admin_user,
    analysis_data,
    loop,
):
    # let's first add an analysis
    _req, resp = client_admin_user.post(
        "/api/pytest/analysis/ajouter", data=analysis_data
    )
    assert resp.status == 200
    content = resp.json
    assert "id" in content
    indicator_id = content["id"]

    # testing with wrong users
    _request, resp = client_anonymous.put(f"/api/pytest/analysis/{indicator_id}")
    assert resp.status == 401
    _req, resp = client_basic_user.put(f"/api/pytest/analysis/{indicator_id}")
    assert resp.status == 401
    # wrong region
    _req, resp = client_admin_user.put(
        f"/api/auvergne-rhone-alpes/analysis/{indicator_id}"
    )
    assert resp.status == 401

    # wrong ID
    _req, resp = client_admin_user.put(
        f"/api/pytest/analysis/-1",
        data={"fake": "data"},
    )
    assert resp.status == 404

    # no data sent
    _req, resp = client_admin_user.put(f"/api/pytest/analysis/{indicator_id}")
    assert resp.status == 400

    # right user, right region
    new_analysis_data = analysis_data.copy()
    new_analysis_data["nomIndicateur"] = "This is a test indicator updated"
    new_analysis_data["type"] = "choropleth"
    _req, resp = client_admin_user.put(
        f"/api/pytest/analysis/{indicator_id}", data=new_analysis_data
    )
    assert resp.status == 200

    # test that the analysis was successfully added
    _req, resp = client_admin_user.get("/api/pytest/analysis")
    assert resp.status == 200
    content = resp.json
    indicator = next(filter(lambda x: x["id"] == indicator_id, content))
    assert indicator["type"] == new_analysis_data["type"]
    assert indicator["nom"] == new_analysis_data["nomIndicateur"]

    # wrong year data
    wrong_year_data = analysis_data.copy()
    wrong_year_data["annees"] = "aaaa"
    _req, resp = client_admin_user.put(
        f"/api/pytest/analysis/{indicator_id}", data=wrong_year_data
    )
    assert resp.status == 400
    content = resp.json
    assert "status" in content and content["status"] == "error"


def test_export_analysis(
    region, client_anonymous, client_admin_user, analysis_data, loop
):
    # Add a non exportable analysis with data
    file_analysis_data = analysis_data.copy()
    file_analysis_data["donneesExistantes"] = "false"
    file_analysis_data["nomTable"] = "pytest_table_example"
    file_analysis_data["dataCustom"] = ""
    with open(here / "data" / "data_example.csv", "rb") as csv_file:
        _req, resp = client_admin_user.post(
            "/api/pytest/analysis/ajouter",
            data=file_analysis_data,
            files={"fichier": csv_file},
        )

    assert resp.status == 200
    content = resp.json
    assert "id" in content
    indicator_id = content["id"]

    _request, resp = client_anonymous.get(f"/api/pytest/analysis/export/{indicator_id}")
    assert resp.status == 400
    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/export/{indicator_id}?zone=region&maille=commune&zone_id=1"
    )
    assert resp.status == 401

    # Make the analysis exportable for maille commune
    new_analysis_data = analysis_data.copy()
    new_analysis_data["dataCustom"] = "pytest_table_example"
    new_analysis_data["donneesExportables"] = "epci,commune"
    client_admin_user.put(
        f"/api/pytest/analysis/{indicator_id}", data=new_analysis_data
    )

    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/export/{indicator_id}?zone=commune&maille=commune&zone_id=00000&format=csv"
    )
    assert resp.status == 200
    assert resp.content_type == "text/csv"
    csv_lines = resp.content.decode("utf-8").split("\n")
    # TODO: assert len(csv_lines) == 8  # header + 6 lines of data + empty line

    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/export/{indicator_id}?zone=region&maille=commune&zone_id=1&format=csv&annee=2020"
    )
    assert resp.status == 200
    assert resp.content_type == "text/csv"
    csv_lines = resp.content.decode("utf-8").split("\n")
    # TODO: assert len(csv_lines) == 3  # header + 1 line of data + empty line

    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/export/{indicator_id}?zone=region&maille=commune&zone_id=1&format=excel"
    )
    assert resp.status == 200
    assert re.compile("^application/.*\\.sheet$").match(resp.content_type)


def test_ajouter_pourcentage_des_donnees_non_filtrees(loop):
    """Test the DonneesPourRepresentationCartoIndicateurFiltre._ajouter_pourcentage_des_donnees_non_filtrees() function
    that calculates the percentage of the filtered data compared to the non filtered data.

    Verify that :
        - a new column "val_applats" containing the calculated percentage is added
        - the percentage is correctly calculated and rounded
        - missing an 0 values are handled correctly
        - an undefined nan percentage is converted to None (null in JSON) in the result
    """
    df_filtered_data = pd.DataFrame(
        {
            "code": ["codeepci1", "codeepci2", "codeepci3", "codeepci4", "codeepci5"],
            "nom": [
                "CC 1",
                "CC 2",
                "EPCI avec donnée filtrée",
                "EPCI avec donnée manquante",
                "EPCI avec valeur à 0",
            ],
            "x": [3.14, 2.71, 7.71, 7.7, 1.7],
            "y": [3.51, 2.47, 8.25, 8.2, 3.2],
            "val": [31.2, 41.5, np.nan, np.nan, 0],
        }
    )

    df_unfiltered_data = pd.DataFrame(
        {
            "code": ["codeepci1", "codeepci2", "codeepci3", "codeepci4", "codeepci5"],
            "nom": [
                "CC 1",
                "CC 2",
                "EPCI avec donnée filtrée",
                "EPCI avec donnée manquante",
                "EPCI avec valeur à 0",
            ],
            "x": [3.14, 2.71, 7.71, 7.7, 1.7],
            "y": [3.51, 2.47, 8.25, 8.2, 3.2],
            "val": [100, 200, 100, np.nan, 0],
        }
    )

    result = DonneesPourRepresentationCartoIndicateurFiltre._ajouter_pourcentage_des_donnees_non_filtrees(
        df_filtered_data, df_unfiltered_data
    )

    expected_result = [
        # percentage is correctly calculated and rounded
        {
            "code": "codeepci1",
            "nom": "CC 1",
            "x": 3.14,
            "y": 3.51,
            "val": 31.2,
            "val_applats": 31,
        },
        {
            "code": "codeepci2",
            "nom": "CC 2",
            "x": 2.71,
            "y": 2.47,
            "val": 41.5,
            "val_applats": 21,
        },
        # a nan value in the filtered data is interpreted as a 0 if there is a value in the unfiltered data
        {
            "code": "codeepci3",
            "nom": "EPCI avec donnée filtrée",
            "x": 7.71,
            "y": 8.25,
            "val": 0.0,
            "val_applats": 0.0,
        },
        # for a territory with no data (filtered or unfiltered), the percentage and the value are None
        {
            "code": "codeepci4",
            "nom": "EPCI avec donnée manquante",
            "x": 7.7,
            "y": 8.2,
            "val": None,
            "val_applats": None,
        },
        # for a 0 value (filtered and unfiltered), the percentage is not defined and should be None
        {
            "code": "codeepci5",
            "nom": "EPCI avec valeur à 0",
            "x": 1.7,
            "y": 3.2,
            "val": 0.0,
            "val_applats": None,
        },
    ]

    assert result == expected_result
