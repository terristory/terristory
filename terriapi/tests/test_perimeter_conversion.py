# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import pandas as pd
import pytest
from pandas.testing import assert_frame_equal

from terriapi.conversion_perimetres.conversion import convert_df_to_perimeter


@pytest.fixture
def input_data_table():
    """A data table to test perimeter conversion.

    Yields
    ------
    DataFrame
        Mock data table
    """
    columns = [
        "commune",
        "valeur",
        "second_value_column",
        "categorie1",
        "categorie2",
        "annee",
    ]
    data = [
        ["00001", 3, 3.3, 1, 2, 2020],
        ["00001", 32, 452.6, 1, 3, 2020],
        ["00001", 3, 3, 2, 4, 2020],
        ["00002", 7, 7.7, 1, 2, 2020],
        ["00002", 4, 5, 2, 4, 2020],
        ["00003", 123.45, 66, 1, 2, 2020],
        ["00003", 1, 1, 2, 4, 2020],
        ["00004", 457, 663, 1, 2, 2020],
        ["00005", 348, 56.2, 1, 2, 2020],
        ["00006", 100, 43.2, 1, 2, 2020],
        ["00007", 50, 30, 1, 2, 2020],
        ["00008", 33, 78.2, 1, 2, 2020],
        ["00009", 223, 41.12, 1, 2, 2020],
        ["00001", 23.3, 1, 1, 2, 2019],
        ["00002", 6.7, 2, 1, 2, 2019],
    ]
    df = pd.DataFrame(columns=columns, data=data)
    yield df


def mock_get_perimeter_conversion_table(initial_year, targe_year):
    """Returns a mocked perimeter conversion table for testing purposes.
    Available tables : 2020=>2021, 2021=>2022

    2020 => 2021 :
    - 00001 and 00002 are merged into 00001
    - 00003, 00004 and 00005 are merged into 00003
    - 00006 is split between 00007 (60%) and 00008 (40%)
    - 00009 is changed to 00010

    2021 => 2022
    - 00001 and 00003 are merged into 00001
    - 00007 is split between 00001 (30%), 00008 (30%) and 00010 (40%)

    Parameters
    ----------
    initial_year : int
        _description_
    targe_year : int
        _description_

    Returns
    -------
    DataFrame
        The perimeter conversion table
    """
    conversion_table_2020_2021 = pd.DataFrame(
        columns=["cod2020", "cod2021", "ratio"],
        data=[
            ["00001", "00001", 1],
            ["00002", "00001", 1],
            ["00003", "00003", 1],
            ["00004", "00003", 1],
            ["00005", "00003", 1],
            ["00006", "00007", 0.6],
            ["00006", "00008", 0.4],
            ["00009", "00010", 1],
        ],
    )
    conversion_table_2021_2022 = pd.DataFrame(
        columns=["cod2021", "cod2022", "ratio"],
        data=[
            ["00001", "00001", 1],
            ["00003", "00001", 1],
            ["00007", "00008", 0.3],
            ["00007", "00001", 0.3],
            ["00007", "00010", 0.4],
        ],
    )
    conversion_tables = {
        "conversion_table_2020_2021": conversion_table_2020_2021,
        "conversion_table_2021_2022": conversion_table_2021_2022,
    }

    return conversion_tables[f"conversion_table_{initial_year}_{targe_year}"]


@pytest.mark.parametrize(
    "initial_year,target_year,expected",
    [
        (
            2020,
            2021,
            pd.DataFrame(
                columns=[
                    "commune",
                    "valeur",
                    "second_value_column",
                    "categorie1",
                    "categorie2",
                    "annee",
                ],
                data=[
                    ["00001", 10, 11, 1, 2, 2020],
                    ["00001", 32, 452.6, 1, 3, 2020],
                    ["00001", 7, 8, 2, 4, 2020],
                    ["00003", 928.45, 785.2, 1, 2, 2020],
                    ["00003", 1, 1, 2, 4, 2020],
                    ["00007", 110, 55.92, 1, 2, 2020],
                    ["00008", 73, 95.48, 1, 2, 2020],
                    ["00010", 223, 41.12, 1, 2, 2020],
                    ["00001", 30, 3, 1, 2, 2019],
                ],
            ),
        ),
        (
            2020,
            2022,
            pd.DataFrame(
                columns=[
                    "commune",
                    "valeur",
                    "second_value_column",
                    "categorie1",
                    "categorie2",
                    "annee",
                ],
                data=[
                    ["00001", 971.45, 812.976, 1, 2, 2020],
                    ["00001", 32, 452.6, 1, 3, 2020],
                    ["00001", 8, 9, 2, 4, 2020],
                    ["00008", 106, 112.256, 1, 2, 2020],
                    ["00010", 267, 63.488, 1, 2, 2020],
                    ["00001", 30, 3, 1, 2, 2019],
                ],
            ),
        ),
    ],
)
def test_data_perimeter_conversion(
    mocker, input_data_table, initial_year, target_year, expected, loop
):
    """Unit test for the convert_df_to_perimeter() function.
    Tests conversion over one (2020=>2021) and multiple years (2020=>2022).
    """
    mocker.patch(
        "terriapi.conversion_perimetres.conversion.get_perimeter_conversion_table",
        side_effect=mock_get_perimeter_conversion_table,
    )
    df_converted = loop.run_until_complete(
        convert_df_to_perimeter(
            input_data_table,
            initial_year,
            target_year,
            ["valeur", "second_value_column"],
            ["commune"],
            "sum",
        )
    )
    # sort DataFrames before comparing, because row order doesn't matter
    df_converted = df_converted.sort_values(
        by=df_converted.columns.tolist()
    ).reset_index(drop=True)
    expected = expected.sort_values(by=expected.columns.tolist()).reset_index(drop=True)
    assert_frame_equal(df_converted, expected)


@pytest.fixture
def input_flow_data_table():
    """A flow data table to test perimeter conversion.

    Yields
    ------
    DataFrame
        Mock data table
    """
    columns = [
        "commune",
        "commune_dest",
        "valeur",
        "categorie1",
        "categorie2",
        "annee",
    ]
    data = [
        ["00001", "00002", 527, 1, 2, 2020],
        ["00001", "00003", 696, 1, 2, 2020],
        ["00001", "00006", 555, 1, 2, 2020],
        ["00002", "00001", 550, 1, 2, 2020],
        ["00002", "00003", 670, 1, 2, 2020],
        ["00002", "00001", 180, 1, 2, 2020],
        ["00002", "00004", 703, 1, 2, 2020],
        ["00001", "00007", 1, 1, 2, 2020],
        ["00006", "00007", 689, 1, 2, 2020],
        ["00006", "00002", 859, 1, 2, 2020],
        ["00001", "00009", 983, 1, 2, 2020],
        ["00002", "00009", 786, 1, 2, 2020],
        ["00001", "00002", 527, 1, 3, 2020],
        ["00001", "00003", 696, 1, 3, 2020],
        ["00001", "00006", 555, 1, 3, 2020],
        ["00002", "00001", 550, 1, 3, 2020],
        ["00002", "00003", 670, 1, 3, 2020],
        ["00002", "00001", 180, 1, 3, 2020],
        ["00002", "00004", 703, 1, 3, 2020],
        ["00001", "00007", 1, 1, 3, 2020],
        ["00006", "00007", 689, 1, 3, 2020],
        ["00006", "00002", 859, 1, 3, 2020],
        ["00001", "00009", 983, 1, 3, 2020],
        ["00002", "00009", 786, 1, 3, 2020],
    ]
    df = pd.DataFrame(columns=columns, data=data)
    yield df


@pytest.mark.parametrize(
    "initial_year,target_year,expected",
    [
        (
            2020,
            2022,
            pd.DataFrame(
                columns=[
                    "commune",
                    "commune_dest",
                    "valeur",
                    "categorie1",
                    "categorie2",
                    "annee",
                ],
                data=[
                    ["00001", "00001", 3618.026, 1, 2, 2020],
                    ["00001", "00008", 359.406, 1, 2, 2020],
                    ["00001", "00010", 1952.208, 1, 2, 2020],
                    ["00008", "00001", 618.106, 1, 2, 2020],
                    ["00008", "00008", 119.886, 1, 2, 2020],
                    ["00008", "00010", 159.848, 1, 2, 2020],
                    ["00010", "00001", 255.768, 1, 2, 2020],
                    ["00010", "00008", 49.608, 1, 2, 2020],
                    ["00010", "00010", 66.144, 1, 2, 2020],
                    ["00001", "00001", 3618.026, 1, 3, 2020],
                    ["00001", "00008", 359.406, 1, 3, 2020],
                    ["00001", "00010", 1952.208, 1, 3, 2020],
                    ["00008", "00001", 618.106, 1, 3, 2020],
                    ["00008", "00008", 119.886, 1, 3, 2020],
                    ["00008", "00010", 159.848, 1, 3, 2020],
                    ["00010", "00001", 255.768, 1, 3, 2020],
                    ["00010", "00008", 49.608, 1, 3, 2020],
                    ["00010", "00010", 66.144, 1, 3, 2020],
                ],
            ),
        ),
        (
            2020,
            2021,
            pd.DataFrame(
                columns=[
                    "commune",
                    "commune_dest",
                    "valeur",
                    "categorie1",
                    "categorie2",
                    "annee",
                ],
                data=[
                    ["00001", "00001", 1257, 1, 2, 2020],
                    ["00001", "00003", 2069, 1, 2, 2020],
                    ["00001", "00007", 334, 1, 2, 2020],
                    ["00001", "00008", 222, 1, 2, 2020],
                    ["00007", "00007", 413.4, 1, 2, 2020],
                    ["00007", "00001", 515.4, 1, 2, 2020],
                    ["00008", "00007", 275.6, 1, 2, 2020],
                    ["00008", "00001", 343.6, 1, 2, 2020],
                    ["00001", "00010", 1769, 1, 2, 2020],
                    ["00001", "00001", 1257, 1, 3, 2020],
                    ["00001", "00003", 2069, 1, 3, 2020],
                    ["00001", "00007", 334, 1, 3, 2020],
                    ["00001", "00008", 222, 1, 3, 2020],
                    ["00007", "00007", 413.4, 1, 3, 2020],
                    ["00007", "00001", 515.4, 1, 3, 2020],
                    ["00008", "00007", 275.6, 1, 3, 2020],
                    ["00008", "00001", 343.6, 1, 3, 2020],
                    ["00001", "00010", 1769, 1, 3, 2020],
                ],
            ),
        ),
    ],
)
def test_data_perimeter_conversion_with_flow_data(
    mocker, input_flow_data_table, initial_year, target_year, expected, loop
):
    """Unit test for the convert_df_to_perimeter() function.
    Tests conversion of a flow data table (i.e. with "commune" and "commune_dest" columns)
    """
    mocker.patch(
        "terriapi.conversion_perimetres.conversion.get_perimeter_conversion_table",
        side_effect=mock_get_perimeter_conversion_table,
    )
    df_converted = loop.run_until_complete(
        convert_df_to_perimeter(
            input_flow_data_table,
            initial_year,
            target_year,
            ["valeur"],
            ["commune", "commune_dest"],
            "sum",
        )
    )
    # sort DataFrames before comparing, because row order doesn't matter
    df_converted = df_converted.sort_values(
        by=df_converted.columns.tolist()
    ).reset_index(drop=True)
    expected = expected.sort_values(by=expected.columns.tolist()).reset_index(drop=True)
    assert_frame_equal(df_converted, expected)


@pytest.mark.parametrize(
    "initial_year,target_year,expected",
    [
        (
            2020,
            2021,
            pd.DataFrame(
                columns=[
                    "commune",
                    "valeur",
                    "categorie1",
                    "annee",
                ],
                data=[
                    ["00001", 12.5, 1, 2020],
                    ["00003", 25, 1, 2020],
                    ["00007", 37.5, 1, 2020],
                    ["00008", 45, 1, 2020],
                    ["00010", 45, 1, 2020],
                ],
            ),
        ),
        (
            2020,
            2022,
            pd.DataFrame(
                columns=[
                    "commune",
                    "valeur",
                    "categorie1",
                    "annee",
                ],
                data=[
                    ["00001", 25, 1, 2020],
                    ["00008", 41.25, 1, 2020],
                    ["00010", 41.25, 1, 2020],
                ],
            ),
        ),
    ],
)
def test_data_perimeter_conversion_with_mean_aggregation(
    mocker, initial_year, target_year, expected, loop
):
    mocker.patch(
        "terriapi.conversion_perimetres.conversion.get_perimeter_conversion_table",
        side_effect=mock_get_perimeter_conversion_table,
    )
    # input data
    df_input_data = pd.DataFrame(
        columns=[
            "commune",
            "valeur",
            "categorie1",
            "annee",
        ],
        data=[
            ["00001", 10, 1, 2020],
            ["00002", 15, 1, 2020],
            ["00003", 30, 1, 2020],
            ["00004", 20, 1, 2020],
            ["00005", 25, 1, 2020],
            ["00006", 40, 1, 2020],
            ["00007", 35, 1, 2020],
            ["00008", 50, 1, 2020],
            ["00009", 45, 1, 2020],
        ],
    )
    df_converted = loop.run_until_complete(
        convert_df_to_perimeter(
            df_input_data,
            initial_year,
            target_year,
            ["valeur"],
            ["commune"],
            "mean",
        )
    )
    # sort DataFrames before comparing, because row order doesn't matter
    df_converted = df_converted.sort_values(
        by=df_converted.columns.tolist()
    ).reset_index(drop=True)
    expected = expected.sort_values(by=expected.columns.tolist()).reset_index(drop=True)
    assert_frame_equal(df_converted, expected)
