# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
import numbers
from pathlib import Path

import pytest

from terriapi.controller.simulator.mobility.simulator_mobility import SimulatorMobility

here = Path(__file__).resolve().parent


@pytest.mark.parametrize(
    "region_slug,region_code",
    [
        ("auvergne-rhone-alpes", "84"),
        ("occitanie", "76"),
        ("paysdelaloire", "52"),
        ("bretagne", "53"),
    ],
)
def test_if_configuration_exists_all_region(
    client_anonymous, client_admin_user, region, region_slug, region_code
):
    zone, maille = "region", "epci"
    # get configuration with ananomys user
    _request, resp = client_anonymous.get(
        f"/api/{region_slug}/mobility/simulator",
        params={
            "zone": zone,
            "maille": maille,
            "zone_id": region_code,
        },
    )
    assert resp.status == 200
    result = resp.json
    assert len(result) > 0
    # Check if the result contains all params
    params = [
        "metadata",
        "generic_input_param",
        "specific_input_param",
        "generic_inter_param",
        "intermediate_compute",
        "actions_params",
        "impacts_params",
        "activated_impacts",
    ]
    assert [param in result.keys() for param in params]

    # Check if the parameters are not empty
    assert [len(result[param]) > 0 for param in params]


def test_get_configuration(loop):
    zone, maille = "region", "epci"

    with open(here / "data" / "data_simulator_mobility.json", "r") as f:
        results = json.load(f)

    # First scenario : all impacts are enabled and level = 1
    ## get region configuration (which categories, levers and impacts are activated)
    activated_impacts = results[0]["activated_impacts"]
    ## get generic data (consumption, emission factors, energy bill)
    generic_input_param = results[0]["generic_input_param"]
    ## get generic data necesary for interm compute (consumption, emission factors, energy bill)
    generic_inter_param = results[0]["generic_inter_param"]
    level = results[0]["metadata"]["level"]

    ## get specific data for the seleted region
    specific_input_param = results[0]["specific_input_param"]

    simulator_mobility_level1 = SimulatorMobility(
        "pytest", maille, zone, "14", "mobility"
    )

    ## get actions default params
    actions_params = simulator_mobility_level1.get_default_settings(
        specific_input_param, generic_input_param
    )

    ## get intermediate calculations for energy, GHG, energy bill and pollutant impacts
    intermediate_calcul = loop.run_until_complete(
        simulator_mobility_level1.inter_calcul(
            specific_input_param,
            generic_input_param,
            generic_inter_param,
            level,
            activated_impacts,
        )
    )

    ## get impacts default params
    impacts_params = simulator_mobility_level1.get_default_impacts(
        intermediate_calcul, level, activated_impacts
    )

    ## check if all actions exists in actions_params
    actions = ["1a", "1b", "2a", "3a", "4a", "4b", "5a", "6a", "7a", "8a", "9a"]
    assert [action in actions_params.keys() for action in actions]

    ## check if (min, max, default) values are number for simple actions
    assert [
        isinstance(action["default"], numbers.Number)
        for action in actions_params.values()
        if action["type"] == "simple"
    ]

    ## check if default values are dictionary for complex actions
    assert [
        isinstance(action["default"], dict)
        for action in actions_params.values()
        if action["type"] == "complex"
    ]

    ## check if intermediate calculation (consumption, ghg, energy bill and air pollutants) exists
    assert [
        key in intermediate_calcul.keys()
        for key in ["consumption", "ghg", "energy_bill", "covnm", "pm10", "pm25", "nox"]
    ]

    ## Check if intermediate calculation (consumption, ghg, energy_bill) are not empty
    assert [
        len(intermediate_calcul[key]) > 0
        for key in ["consumption", "ghg", "energy_bill", "covnm", "pm10", "pm25", "nox"]
    ]

    ## check if intermediate calculation before action exists and not empty
    for item in ["consumption", "ghg", "energy_bill", "covnm", "pm10", "pm25", "nox"]:
        final_energy_result_keys = intermediate_calcul[item].keys()
        assert [
            key in final_energy_result_keys for key in ["before", "move_differently"]
        ]
        assert [
            len(intermediate_calcul[item][key]) > 0
            for key in ["before", "move_differently"]
        ]

    ## check if all impacts exists in impacts_params
    impacts = ["impact-ener", "impact-carbone", "impact-facture-ener", "impact-atmo"]

    assert [impact in impacts_params.keys() for impact in impacts]

    ## check if default values are number for simple impacts ("impact-ener", "impact-carbone" and "impact-facture-ener")
    assert [
        isinstance(impact["default"], numbers.Number)
        for impact in impacts_params.values()
        if impact["type"] == "simple"
    ]

    ## check if default value is a dict for complex impacts ("impact-atmo")
    assert [
        isinstance(impact["default"], dict)
        for impact in impacts_params.values()
        if impact["type"] == "complex"
    ]

    # Second scenario : not all impacts are enabled and level = 0

    ## get region configuration (which categories, levers and impacts are activated)
    activated_impacts = results[1]["activated_impacts"]
    ## get generic data (consumption, emission factors, energy bill)
    generic_input_param = results[1]["generic_input_param"]
    ## get generic data necesary for interm compute (consumption, emission factors, energy bill)
    generic_inter_param = results[1]["generic_inter_param"]
    level = results[1]["metadata"]["level"]

    ## get specific data for the seleted region
    specific_input_param = results[1]["specific_input_param"]

    simulator_mobility_level0 = SimulatorMobility(
        "pytest", maille, zone, "15", "mobility"
    )

    ## get intermediate calculations for energy, GHG, energy bill and pollutant impacts
    intermediate_calcul = loop.run_until_complete(
        simulator_mobility_level0.inter_calcul(
            specific_input_param,
            generic_input_param,
            generic_inter_param,
            level,
            activated_impacts,
        )
    )

    ## get impacts default params
    impacts_params = simulator_mobility_level0.get_default_impacts(
        intermediate_calcul, level, activated_impacts
    )

    ## check if intermediate calculation for ghg and energy bill exists
    assert [key in intermediate_calcul.keys() for key in ["ghg", "energy_bill"]]
    ## check if intermediate calculation for consumption doesn't exist
    assert [
        key not in intermediate_calcul.keys()
        for key in ["covnm", "pm10", "pm25", "nox", "consumption"]
    ]

    ## Check if intermediate calculation (ghg, energy_bill) are not empty
    assert [len(intermediate_calcul[key]) > 0 for key in ["ghg", "energy_bill"]]

    ## check if intermediate calculation before action exists and not empty
    for item in ["ghg", "energy_bill"]:
        final_energy_result_keys = intermediate_calcul[item].keys()
        assert [
            key in final_energy_result_keys for key in ["before", "move_differently"]
        ]
        assert [
            len(intermediate_calcul[item][key]) > 0
            for key in ["before", "move_differently"]
        ]

    ## check if "impact-carbone", "impact-facture-ener" exists in impacts_params
    impacts = ["impact-carbone", "impact-facture-ener"]

    assert [impact in impacts_params.keys() for impact in impacts]

    ## check if default values are number for simple impacts ("impact-ener" and "impact-carbone" )

    assert [
        isinstance(impact["default"], numbers.Number)
        for impact in impacts_params.values()
        if len(impact.keys()) > 0 and impact["type"] == "simple"
    ]

    ## check if "impact-ener" and "impact-atmo" are empty
    assert [
        len(impacts_params[impact].keys()) == 0 in impacts_params.keys()
        for impact in ["impact-ener", "impact-atmo"]
    ]
