﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from urllib.error import URLError

from tests.utils import MockResponseUrllibRequestUrlopen, mock_http_response

from terriapi.controller import fetch


def test_registre_layers(
    region, indicators, client_anonymous, client_admin_user, mocker, caplog
):
    # we first test using a wrong URL
    mocker.patch(
        "terriapi.controller.donnees_registre.urllib.request.urlopen",
        return_value=URLError("Erreur"),
    )

    # we test without the right authorization
    _request, resp = client_anonymous.get("/api/pytest/registre")
    assert resp.status == 401

    # then as admin
    _req, resp = client_admin_user.get("/api/pytest/registre")
    assert "L'appel de l'API du registre RTE n'a pas fonctionné." in caplog.text

    # we then test using real URL
    mocker.patch(
        "terriapi.controller.donnees_registre.urllib.request.urlopen",
        return_value=MockResponseUrllibRequestUrlopen(
            mock_http_response(
                mocker,
                b"""{"datasets":[{"datasetid":"example","metas":{"title":"Example","data_processed":"2023-01-01"}}]}""",
            )
        ),
    )
    _req, resp = client_admin_user.get("/api/pytest/registre")
    data = resp.json
    assert resp.status == 200
    assert "liste_tables_rte" in data
    assert len(data["liste_tables_rte"]) > 0

    # once we got the data, we try getting the reasults
    table_example = data["liste_tables_rte"][0]["jeu_de_donnees"]

    mocker.patch(
        "terriapi.controller.donnees_registre.urllib.request.urlopen",
        return_value=MockResponseUrllibRequestUrlopen(
            mock_http_response(
                mocker,
                b"""statut;annee_ouverture;exploitant;rue;code_postal;commune;departement;nom_region;carburant;paiement;acces_pl;mois_ouverture;pays;gnl;gnc;biognc;latitude;longitude;paiement_cash;paiement_cb;paiement_carte_abonne;projet;ouvert;statut_carburant;id;geo;coordonnees_gps;code_region;kpi_ouverture
exemple""",
            )
        ),
    )
    # we test without the right authorization
    _request, resp = client_anonymous.get(f"/api/registre/pytest/{table_example}")
    assert resp.status == 401

    # then as admin
    _req, resp = client_admin_user.get(f"/api/registre/pytest/{table_example}")
    assert resp.status == 200
    data = resp.json
    assert "liste_colonnes" in data
    assert "valeurs_par_colonnes" in data
    assert "donnees_finales" in data
    assert "liste_tables_dispo" in data


def test_integration_registre_layer(
    region, indicators, client_anonymous, client_admin_user, loop
):
    table_example = "consommation-annuelle-brute-regionale"

    # not allowed for anonymous user
    _request, resp = client_anonymous.put(
        f"/api/registre/pytest/{table_example}/integration"
    )
    assert resp.status == 401

    data = {
        "donnees": [
            {
                "annee": "2014",
                "code_commune": "12345",
                "consommation_brute_gaz_grtgaz": "59609",
                "statut_grtgaz": "Définitif",
                "consommation_brute_electricite_rte": "44988",
                "statut_rte": "Définitif",
            },
            {
                "annee": "2015",
                "code_commune": "12345",
                "consommation_brute_gaz_grtgaz": "59132",
                "statut_grtgaz": "Définitif",
                "consommation_brute_electricite_rte": "45000",
                "statut_rte": "Définitif",
            },
            {
                "annee": "2016",
                "code_commune": "12345",
                "consommation_brute_gaz_grtgaz": "58110",
                "statut_grtgaz": "En cours",
                "consommation_brute_electricite_rte": "46210",
                "statut_rte": "Temporaire",
            },
        ],
        "categorie": [
            {
                "region": "pytest",
                "colonne": "statut_grtgaz",
                "association": {
                    "Définitif": {
                        "modalite": "Définitif",
                        "modaliteId": 1,
                        "couleur": "#1265CA",
                    },
                    "En cours": {
                        "modalite": "En cours",
                        "modaliteId": 2,
                        "couleur": "#FF1234",
                    },
                },
            },
            {
                "region": "pytest",
                "colonne": "statut_rte",
                "association": {
                    "Définitif": {
                        "modalite": "Définitif",
                        "modaliteId": 1,
                        "couleur": "#1265CA",
                    },
                    "Temporaire": {
                        "modalite": "Temporaire",
                        "modaliteId": 2,
                        "couleur": "#A2C412",
                    },
                },
            },
        ],
        "nomTable": "test_pytest_rte_integration",
        "colAnnee": "annee",
        "colCommune": "code_commune",
        "colValeur": "consommation_brute_electricite_rte",
    }

    # put data inside regional database
    _req, resp = client_admin_user.put(
        f"/api/registre/pytest/{table_example}/integration", json=data
    )
    assert resp.status == 200
    res_integration = resp.json
    assert res_integration.get("message", "Wrong") == "Données intégrées"

    metadata = loop.run_until_complete(
        fetch("""SELECT * FROM pytest.test_pytest_rte_integration""")
    )
    assert len(metadata) == len(data["donnees"])
