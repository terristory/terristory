# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.


def test_territorial_necessary_layers(
    region, indicators, client_anonymous, client_admin_user
):
    """
    Test retrieving ...

    TODO: test we have at least correct layers (and create those in conftest)

    Parameters
    ----------
    region : str
        region fixtures
    common_data : str
        table name created inside pytest schema
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    # we test without rights
    _request, resp = client_anonymous.get("/api/pytest/donnees_territoriales/")
    assert resp.status == 401

    # real call
    _req, resp = client_admin_user.get("/api/pytest/donnees_territoriales/")
    assert resp.status == 200


def test_geographical_data(region, indicators, client_anonymous, client_admin_user):
    """
    Test retrieving ...

    TODO: test we have at least minimal layers (and create those in conftest)

    Parameters
    ----------
    region : str
        region fixtures
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    # we test without rights
    _request, resp = client_anonymous.get("/api/pytest/donnees_geographiques/")
    assert resp.status == 401

    # real call
    _req, resp = client_admin_user.get("/api/pytest/donnees_geographiques/")
    assert resp.status == 200


def test_common_dataset_list(
    region, indicators, common_data, client_anonymous, client_admin_user
):
    """
    Test retrieving common dataset list from API entrypoint. Call the API without rights
    and then with the admin rights.

    Parameters
    ----------
    region : str
        region fixtures
    common_data : str
        table name created inside pytest schema
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    # we test without rights
    _request, resp = client_anonymous.get(
        "/api/pytest/analysis/donnees_communes/france/"
    )
    assert resp.status == 401

    # we test with wrong global region
    _req, resp = client_admin_user.get(
        "/api/pytest/analysis/donnees_communes/wrong_global_region/"
    )
    assert resp.status == 404

    # real call
    _req, resp = client_admin_user.get("/api/pytest/analysis/donnees_communes/france/")
    assert resp.status == 200

    data = resp.json
    assert len(data) > 0
    assert len(next(f for f in data if f["nom"] == common_data)) > 0


def test_common_dataset_layer_export(
    region, common_data, client_anonymous, client_admin_user
):
    """
    Test exporting a common dataset table from API entrypoint. Call the API without rights
    and then with the admin rights but with a wrong table name. Will eventually
    call the API with admin rights and correct table name, thus getting a csv
    file.

    Parameters
    ----------
    region : str
        region fixtures
    common_data : str
        table name created inside pytest schema
    client_anonymous : pytest_sanic.utils.TestClient
        a client_basic_user with anonymous user rights
    client_admin_user : pytest_sanic.utils.TestClient
        a client_basic_user with admin rights
    """
    # we test without rights
    _request, resp = client_anonymous.get(
        f"/api/pytest/analysis/donnees_communes/france/export/{common_data}/"
    )
    assert resp.status == 401

    # we test with a wrong table name
    _req, resp = client_admin_user.get(
        f"/api/pytest/analysis/donnees_communes/france/export/unexisting_{common_data}/"
    )
    assert resp.status == 404
    data = resp.json
    assert "message" in data and data["message"].startswith(
        "Impossible de trouver la table de données"
    )

    # real call
    _req, resp = client_admin_user.get(
        f"/api/pytest/analysis/donnees_communes/france/export/{common_data}/"
    )
    assert resp.status == 200

    # we check we have (normally a csv file)
    assert resp.content_type == "text/csv"

    # we check we have a non empty file
    data = resp.content
    assert len(data) > 0
