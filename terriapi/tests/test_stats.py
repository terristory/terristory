# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.
from unittest.mock import AsyncMock, Mock

import pytest

from terriapi.controller import visiting_stats


@pytest.fixture
def dashboard_fixtures():
    return {
        "titre": "Nom tableau bord",
        "description": "une description",
        "thematiques": [
            {
                "titre_thematique": "Énergie renouvelable",
                "description_thematique": "Objectif territoire à énergie positive",
                "graphiques": [
                    {
                        "indicateur_id": 46,
                        "type": "pie",
                        "categories": ["type_prod_enr"],
                    },
                    {
                        "indicateur_id": 27,
                        "type": "pie",
                        "categories": ["secteur_solaire_thermique"],
                    },
                ],
            },
            {
                "titre_thematique": "consommation",
                "description_thematique": "Niveau de maîtrise de consommation de l'énergie",
                "graphiques": [
                    {
                        "indicateur_id": 1,
                        "type": "pie",
                        "categories": ["secteur", "energie"],
                    },
                    {
                        "indicateur_id": 15,
                        "type": "line",
                        "categories": ["secteur", "usage"],
                    },
                ],
            },
        ],
    }


@pytest.fixture
def dashboard(client_admin_user, dashboard_fixtures):
    """Création d'un tableau de bord avec un compte admin"""
    _req, resp = client_admin_user.post("/api/pytest/tableau", json=dashboard_fixtures)
    assert resp.status == 200
    content = resp.json
    yield content["tableau_id"]


def test_suivi_consultation_other_page(region, client_admin_user, client_anonymous):
    # test without data
    _request, resp = client_anonymous.put("/api/pytest/consultation/autre/page/")
    assert resp.status == 400

    # not allowed without data
    data = {"page": "the moon", "id_utilisateur": -1}
    _request, resp = client_anonymous.put(
        "/api/pytest/consultation/autre/page/", json=data
    )
    assert resp.status == 200


def test_suivi_consultation_tableaux_bords(
    region, client_admin_user, client_anonymous, dashboard
):
    # test without data
    _request, resp = client_anonymous.put("/api/pytest/consultation/tableau/bord/")
    assert resp.status == 400

    # not allowed without data
    data = {
        "tableau_id": -1,
        "id_utilisateur": -1,
        "code_territoire": "1",
        "type_territoire": "1",
    }
    _request, resp = client_anonymous.put(
        "/api/pytest/consultation/tableau/bord/", json=data
    )
    assert resp.status == 400
    assert (resp.json)[
        "message"
    ] == "Impossible de trouver le tableau de bord spécifié."

    _req, resp = client_admin_user.put(
        f"/api/pytest/tableau/{dashboard}/affectation?zone=departement&zone_id=01",
        json={"zone": "departement", "zone_id": "01"},
    )
    assert resp.status == 200

    _request, resp = client_anonymous.get(
        "/api/pytest/public/dashboards/?zone=departement&zone_id=01"
    )
    assert resp.status == 200

    data_dashboard = resp.json
    assert len(data_dashboard) > 0
    id_dashboard = data_dashboard[0]["id"]

    # allowed with data
    data = {
        "tableau_id": id_dashboard,
        "id_utilisateur": -1,
        "code_territoire": "01",
        "type_territoire": "departement",
    }
    _request, resp = client_anonymous.put(
        "/api/pytest/consultation/tableau/bord/", json=data
    )
    assert resp.status == 200


def test_admin_stats_types_panel(region, client_admin_user, client_anonymous):
    """
    Check the access to stats types and compare to existing parameter.

    Parameters
    ----------
    region : str
        the region considered
    client_admin_user : pytest_sanic.utils.TestClient
        tool to call the API using admin user
    client_anonymous : pytest_sanic.utils.TestClient
        tool to call the API using anonymous user
    """
    # getting stats types without being authorized
    _request, resp = client_anonymous.get("/api/pytest/stats/types/")
    assert resp.status == 401

    # getting stats types
    _req, resp = client_admin_user.get("/api/pytest/stats/types/")
    assert resp.status == 200
    data = resp.json
    stats_types = data.get("stats_types", [])
    assert set(stats_types) == set(visiting_stats.STAT_TYPES)


class MockDataRepresentations:
    def donnees_finales(self, *args, **kwargs):
        return {}


def visit_page(stat_type, user_id, client_admin_user, **kwargs):
    """
    Allows to simulate a visit to a certain type of page for each type of stats.

    Allowed stats types are currently:
    - territoires
    - tableaux_de_bords
    - utilisateurs
    - indicateurs
    - poi
    - autres_pages

    Each will call the API to simulate a visit, either directly or sometimes by
    using a Mocker for some DB calls.

    Parameters
    ----------
    stat_type : str
        stat type amongst allowed types (see description)
    user_id : int
        current user id used to register the visit
    client_admin_user : pytest_sanic.utils.TestClient
        tool to call the API using admin user

    Returns
    -------
    callable
        function to parse stats results from calling the visiting stats api.
        For example, on territories, the function returned is selecting the first
        object in the list returned that verifies specific condition on territory
        (code = 1 and type = region).
    """
    if stat_type in ("territoires", "tableaux_de_bords", "utilisateurs"):
        data = {
            "tableau_id": kwargs.get("tableau_bord", 1),
            "id_utilisateur": user_id,
            "code_territoire": "1",
            "type_territoire": "region",
        }
        _req, resp = client_admin_user.put(
            "/api/pytest/consultation/tableau/bord/", json=data
        )
        assert resp.status == 200
        if stat_type == "territoires":

            def _get_regional_stats(data):
                return next(
                    (
                        item
                        for item in data
                        if item["code"] == "1" and item["type_territoire"] == "region"
                    ),
                    {},
                ).get("nbre_selection", 0.0)

            return _get_regional_stats
        elif stat_type == "tableaux_de_bords":

            def _get_regional_stats(data):
                return next(
                    (
                        item
                        for item in data
                        if item["tableau_bord_id"] == kwargs.get("tableau_bord", 1)
                    ),
                    {},
                ).get("nbre", 0.0)

            return _get_regional_stats
        elif stat_type == "utilisateurs":

            def _get_regional_stats(data):
                return next(
                    (item for item in data if item["type"] == "doublon"),
                    {},
                ).get("count", 0.0)

            return _get_regional_stats
    elif stat_type == "indicateurs":
        analysis = kwargs.get("indicateur")
        mocker = kwargs.get("mocker", Mock())

        mocker.patch(
            "terriapi.api.DonneesPourRepresentations",
            return_value=MockDataRepresentations(),
        )

        data = {
            "id_utilisateur": user_id,
            "zone": "region",
            "maille": "region",
            "zone_id": "1",
            "provenance": "carto",
            "filtre": 0,
            "reinitialiserFiltres": "false",
        }
        _req, resp = client_admin_user.post(
            f"/api/pytest/analysis/{analysis}/data/", params=data
        )

        def _get_regional_stats(data):
            return next(
                (item for item in data if item["nom_indicateur"] == "pytest-analysis"),
                {},
            ).get("nbre", 0.0)

        return _get_regional_stats
    elif stat_type == "poi":
        poi, _ = kwargs.get("poi", ("fail", 0))
        mocker = kwargs.get("mocker", Mock())

        # not allowed without data
        data = {
            "type_territoire": "region",
            "code_territoire": "1",
            "id_utilisateur": user_id,
        }
        _req, resp = client_admin_user.get(
            f"/api/pytest/poi/layer/consult/{poi}/true/", params=data
        )
        assert resp.status == 200

        def _get_regional_stats(data):
            return next(
                (item for item in data if item["nom_poi"] == poi),
                {},
            ).get("nbre", 0.0)

        return _get_regional_stats
    elif stat_type == "autres_pages":
        # not allowed without data
        params = {"page": "the moon", "id_utilisateur": user_id}
        _req, resp = client_admin_user.put(
            "/api/pytest/consultation/autre/page/", json=params
        )
        assert resp.status == 200

        def _get_regional_stats(data):
            return next(
                (item for item in data if item["page"] == params["page"]),
                {},
            ).get("nbre", 0.0)

        return _get_regional_stats
    return lambda x: 0.0


@pytest.mark.parametrize(
    "stat_type",
    [
        "territoires",
        "tableaux_de_bords",
        "utilisateurs",
        "indicateurs",
        "poi",
        "autres_pages",
    ],
)
def test_admin_stats_panel(
    region,
    client_admin_user,
    client_anonymous,
    stat_type,
    dashboard,
    indicators,
    poi,
    mocker,
):
    assert (
        stat_type in visiting_stats.STAT_TYPES
    ), f"Wrong parameter for current stat test with {stat_type}"

    # test without authorization
    _request, resp = client_anonymous.get(f"/api/pytest/stats/{stat_type}/2022-01-01")
    assert resp.status == 401

    # test with authorization but wrong date
    _req, resp = client_admin_user.get(f"/api/pytest/stats/{stat_type}/<from_date>")
    assert resp.status == 400
    resp_msg = resp.json
    assert "message" in resp_msg and resp_msg["message"].startswith(
        "Le format de la date de début"
    )

    # test with authorization and right date but wrong to_date
    _req, resp = client_admin_user.get(
        f"/api/pytest/stats/{stat_type}/2022-01-01?to_date=aezazeaze"
    )
    assert resp.status == 400
    resp_msg = resp.json
    assert "message" in resp_msg and resp_msg["message"].startswith(
        "Le format de la date de fin"
    )

    # test with authorization and right date
    _req, resp = client_admin_user.get(f"/api/pytest/stats/{stat_type}/2022-01-01")
    assert resp.status == 200
    resp_msg = resp.json
    stats_before = resp_msg["data"]

    # get user id
    _req, resp = client_admin_user.get("/api/pytest/1/id_utilisateur/")
    assert resp.status == 200, "Impossible de récupérer l'ID de l'utilisateur"
    data = resp.json
    assert "id_utilisateur" in data, "Il manque l'information dans les résultats"
    user_id = data.get("id_utilisateur", 1)

    _, analyses_id = indicators

    # visits the right page corresponding to current stats type
    get_regional_stats = visit_page(
        stat_type,
        user_id,
        client_admin_user,
        **{
            "tableau_bord": dashboard,
            "indicateur": analyses_id[0],
            "poi": poi,
            "other_user": client_anonymous,
            "mocker": mocker,
        },
    )

    # retrieve new stats
    _req, resp = client_admin_user.get(f"/api/pytest/stats/{stat_type}/2022-01-01")
    assert resp.status == 200
    resp_msg = resp.json
    stats_after = resp_msg["data"]

    # we check before and after the vist the stats
    nb_visits_before = get_regional_stats(stats_before)
    nb_visits_after = get_regional_stats(stats_after)
    assert nb_visits_before < nb_visits_after


@pytest.mark.parametrize(
    "stat_type",
    [
        "territoires",
        "tableaux_de_bords",
        "utilisateurs",
        "indicateurs",
        "poi",
        "autres_pages",
    ],
)
def test_admin_stats_panel_date_interval(
    region,
    client_admin_user,
    client_anonymous,
    stat_type,
    dashboard,
    indicators,
    poi,
    mocker,
):
    assert (
        stat_type in visiting_stats.STAT_TYPES
    ), f"Wrong parameter for current stat test with {stat_type}"

    # test without authorization
    _request, resp = client_anonymous.get(f"/api/pytest/stats/{stat_type}/2022-01-01")
    assert resp.status == 401

    # test with authorization but wrong date
    _req, resp = client_admin_user.get(f"/api/pytest/stats/{stat_type}/<from_date>")
    assert resp.status == 400
    resp_msg = resp.json
    assert "message" in resp_msg and resp_msg["message"].startswith(
        "Le format de la date de début"
    )

    # test with authorization and right date but wrong to_date
    _req, resp = client_admin_user.get(
        f"/api/pytest/stats/{stat_type}/2022-01-01?to_date=aezazeaze"
    )
    assert resp.status == 400
    resp_msg = resp.json
    assert "message" in resp_msg and resp_msg["message"].startswith(
        "Le format de la date de fin"
    )

    # test with authorization and right date
    _req, resp = client_admin_user.get(f"/api/pytest/stats/{stat_type}/2022-01-01")
    assert resp.status == 200
    resp_msg = resp.json
    data_at_least_one_year = resp_msg["data"]

    # test with authorization and smaller date interval
    _req, resp = client_admin_user.get(
        f"/api/pytest/stats/{stat_type}/2022-01-01?to_date=2022-01-10"
    )
    assert resp.status == 200
    resp_msg = resp.json
    data_smaller_interval_year = resp_msg["data"]

    # we check that we have fewer visits
    assert len(data_at_least_one_year) >= len(data_smaller_interval_year)


def test_admin_stats_all_regions(
    region,
    client_admin_user,
    client_anonymous,
    client_global_admin_user,
    dashboard,
    indicators,
    poi,
    mocker,
):
    # test without authorization
    _request, resp = client_anonymous.get(f"/api/pytest/all/stats/2022-01-01")
    assert resp.status == 401
    # test without authorization
    _req, resp = client_admin_user.get(f"/api/pytest/all/stats/2022-01-01")
    resp_msg = resp.json
    assert resp.status == 403

    # test with authorization but wrong date
    _req, resp = client_global_admin_user.get(f"/api/pytest/all/stats/<from_date>")
    assert resp.status == 400
    resp_msg = resp.json
    assert "message" in resp_msg and resp_msg["message"].startswith(
        "Le format de la date de début"
    )

    # test with authorization and right date but wrong to_date
    _req, resp = client_global_admin_user.get(
        f"/api/pytest/all/stats/2022-01-01?to_date=aezazeaze"
    )
    assert resp.status == 400
    resp_msg = resp.json
    assert "message" in resp_msg and resp_msg["message"].startswith(
        "Le format de la date de fin"
    )

    async_get_list_regions = AsyncMock(return_value=["pytest"])
    mocker.patch(
        "terriapi.controller.regions_configuration.get_non_national_regions",
        side_effect=async_get_list_regions,
    )

    # test with authorization and right date
    _req, resp = client_global_admin_user.get(f"/api/pytest/all/stats/2022-01-01")
    assert resp.status == 200
    resp_msg = resp.json

    for stat_region in resp_msg:
        assert len(resp_msg[stat_region]) == len(visiting_stats.STAT_TYPES)
