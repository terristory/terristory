# TerriSTORY - API

## Installation

Requis : Python 3.8 dans un virtualenv.

l'installation se résume à:

    $ pip install -e .

Pour un serveur de développement, qui permet d'ajouter le module pour gérer les requêtes CROSS-ORIGIN

    $ pip install -e .[dev]

## Configuration

un fichier `terriapi.ini.sample` est à recopier en `terriapi.ini`.
Ce fichier contient tous les paramètres nécessaires à remplir pour que l'API et ses scripts fonctionnent (accès à la base de donnée, envoi de mail...).

## Chargement de données

Ajout d'un utilisateur actif et administrateur:

    $ terriapi-user --prenom foo --nom bar --password alice --mail foo@bar --organisation bob --profil admin --actif

Mise à jour des PCAET:

    $ terriapi-pcaet

Envoi du mail de CHANGELOG aux administrateurs :

    $ terriapi-send-mail --env prod --changelog ./CHANGELOG.md

Regénération des éléments du module de synthèse territoriale :

    $ terriapi-territorialsynthesis-notes

## Service d'API

L'API se lance simplement avec

    $ terriapi-serve

En local il ne faut pas oublier d'activer le mode CORS pour que le front puisse recevoir les infos de l'API.

    $ terriapi-serve --cors

## Tests

Un ensemble de tests permet de s'assurer que l'API répond bien à toutes les requêtes,
il faut avoir installé `terriapi` en mode dev (`pip install -e .[dev]`), et il suffit ensuite de lancer la commande suivante
dans l'environnement virtuel Python :

    $ pytest

## Formatage des données

### Convention de nommage

L'importeur de données se base sur des noms de fichiers normalisés.
Ces fichiers doivent respecter le format suivant : `nom-table_YYYY-MM-DD.{csv | shp}`
le nom de la table ne doit contenir que des `-` comme séparateurs et ne contenir aucun caractère accentué ni majuscule.

Si une nouvelle version des données est à livrer, il suffit d'incrémenter la date en suffixe.

### Encodage

- Les données attributaires des shapefile sont actuellement en `LATIN-1`.
- Les données CSV sont encodées en UTF-8.
