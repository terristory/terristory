--
-- PostgreSQL database dump
--

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

\c fixture;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: meta; Type: SCHEMA; Schema: -; Owner: terristory
--

DROP SCHEMA IF EXISTS meta;
CREATE SCHEMA meta;


ALTER SCHEMA meta OWNER TO terristory;

--
-- Name: affectation_objectifs; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.affectation_objectifs (
    titre character varying NOT NULL,
    graphique character varying NOT NULL,
    region character varying NOT NULL,
    type_territoire character varying NOT NULL,
    code_territoire character varying NOT NULL
);


ALTER TABLE meta.affectation_objectifs OWNER TO terristory;

--
-- Name: COLUMN affectation_objectifs.titre; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.affectation_objectifs.titre IS 'Titre de l''objectif (SRADDET, REPOS, SNBC etc.)';


--
-- Name: COLUMN affectation_objectifs.graphique; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.affectation_objectifs.graphique IS 'Graphique auquel ajouter l''objectif';


--
-- Name: COLUMN affectation_objectifs.region; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.affectation_objectifs.region IS 'Région pour laquelle l''objectif est saisi';


--
-- Name: COLUMN affectation_objectifs.type_territoire; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.affectation_objectifs.type_territoire IS 'Maille géographique pour laquelle on affiche les objectifs en mode valeur';


--
-- Name: COLUMN affectation_objectifs.code_territoire; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.affectation_objectifs.code_territoire IS 'Code du territoire pour laquelle on affiche les objectifs en mode valeur';


--
-- Name: association_graphique_table; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.association_graphique_table (
    graphique character varying,
    "table" character varying,
    region character varying
);


ALTER TABLE meta.association_graphique_table OWNER TO terristory;

--
-- Name: COLUMN association_graphique_table.graphique; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.association_graphique_table.graphique IS 'Graphique d''une trajectoire cible';


--
-- Name: COLUMN association_graphique_table."table"; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.association_graphique_table."table" IS 'Table nécessaire à la construction du graphique';


--
-- Name: COLUMN association_graphique_table.region; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.association_graphique_table.region IS 'Région concernée par cette association graphique / table';


--
-- Name: categorie; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.categorie (
    nom character varying NOT NULL,
    modalite character varying NOT NULL,
    modalite_id integer NOT NULL,
    couleur character varying(7),
    ordre integer,
    region character varying NOT NULL
);


ALTER TABLE meta.categorie OWNER TO terristory;

--
-- Name: TABLE categorie; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON TABLE meta.categorie IS 'Table de description des catégories';


--
-- Name: COLUMN categorie.nom; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.categorie.nom IS 'Nom de la catégorie';


--
-- Name: COLUMN categorie.modalite; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.categorie.modalite IS 'Modalité faisant partie d''une catégorie';


--
-- Name: COLUMN categorie.modalite_id; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.categorie.modalite_id IS 'Identifiant de la modalité';


--
-- Name: COLUMN categorie.couleur; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.categorie.couleur IS 'Couleur associée à la modalité au sein d''une catégorie';


--
-- Name: COLUMN categorie.ordre; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.categorie.ordre IS 'Ordre d''affichage de la légende des modalités';


--
-- Name: COLUMN categorie.region; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.categorie.region IS 'Identifiant de la région';


--
-- Name: cesba; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.cesba (
    indicateur_id integer NOT NULL,
    numero character varying,
    theme character varying,
    "order" character varying NOT NULL,
    CONSTRAINT cesba_order_check CHECK ((("order")::text = ANY (ARRAY[('asc'::character varying)::text, ('desc'::character varying)::text])))
);


ALTER TABLE meta.cesba OWNER TO terristory;

--
-- Name: TABLE cesba; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON TABLE meta.cesba IS 'Table des indicateurs sur lesquels on va calculer une note CESBA';


--
-- Name: COLUMN cesba.indicateur_id; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.cesba.indicateur_id IS 'Identifiant de l''indicateur';


--
-- Name: COLUMN cesba.numero; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.cesba.numero IS 'Numéro CESBA';


--
-- Name: COLUMN cesba.theme; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.cesba.theme IS 'Nom du thème CESBA';


--
-- Name: COLUMN cesba."order"; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.cesba."order" IS 'Ordre de classement pour les notes CESBA';


--
-- Name: chart; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.chart (
    indicateur integer NOT NULL,
    titre text NOT NULL,
    type character varying NOT NULL,
    categorie character varying NOT NULL,
    visible boolean NOT NULL,
    ordre integer,
    data_type character varying
);


ALTER TABLE meta.chart OWNER TO terristory;

--
-- Name: TABLE chart; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON TABLE meta.chart IS 'Table de description des graphes';


--
-- Name: COLUMN chart.titre; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.chart.titre IS 'Titre du graphique';


--
-- Name: COLUMN chart.type; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.chart.type IS 'Type de graphe: pie, hbar, ...';


--
-- Name: COLUMN chart.categorie; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.chart.categorie IS 'Réference à la catégorie';


--
-- Name: COLUMN chart.visible; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.chart.visible IS 'Visible dans l''interface ?';


--
-- Name: donnees_sankey; Type: TABLE; Schema: meta; Owner: postgres
--

CREATE TABLE meta.sankey_metadata (
    data_table character varying,
    region character varying,
    year integer,
    introduction_text text,
    unit character varying,
    source text,
    copyright boolean
);


ALTER TABLE meta.sankey_metadata OWNER TO postgres;

--
-- Name: indicateur; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.indicateur (
    id integer NOT NULL,
    nom character varying NOT NULL,
    data character varying NOT NULL,
    type character varying,
    color_start character varying(7) NOT NULL,
    color_end character varying(7) NOT NULL,
    unit character varying,
    methodo_pdf character varying,
    confidentiel character varying,
    disabled_for_zone character varying,
    only_for_zone character varying,
    isratio boolean DEFAULT false,
    filter character varying,
    decimals integer DEFAULT 2 NOT NULL,
    ui_theme character varying,
    data_ratio integer,
    years integer[],
    display_total boolean DEFAULT true NOT NULL,
    active boolean DEFAULT false NOT NULL,
    region character varying NOT NULL,
    data_deuxieme_representation character varying,
    valeur_filtre_defaut character varying,
    titre_legende character varying,
    titre_legende_deuxieme_representation character varying,
    concatenation integer[],
    ordre_ui_theme integer,
    ordre_indicateur integer,
    data_type character varying,
    donnees_exportables character varying,
    titre_dans_infobulle character varying,
    afficher_proportion boolean,
    titre character varying,
    moyenne_ponderee boolean,
    afficher_calcul_et_donnees_table boolean,
    titre_graphiques_indicateurs character varying
);


ALTER TABLE meta.indicateur OWNER TO terristory;

--
-- Name: TABLE indicateur; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON TABLE meta.indicateur IS 'table de description des indicateurs';


--
-- Name: COLUMN indicateur.id; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.id IS 'Identifiant';


--
-- Name: COLUMN indicateur.nom; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.nom IS 'nom de l''analyse';


--
-- Name: COLUMN indicateur.data; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.data IS 'référence de la table des inicdateurs souhaités avec éventuellement coefficients associés à la maille';


--
-- Name: COLUMN indicateur.type; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.type IS 'Type de l''analyse: circle, chroropleth';


--
-- Name: COLUMN indicateur.color_start; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.color_start IS 'Couleur de départ pour l''affichage de l''analyse sur la carte';


--
-- Name: COLUMN indicateur.color_end; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.color_end IS 'Couleur de fin pour l''affichage de l''analyse sur la carte';


--
-- Name: COLUMN indicateur.unit; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.unit IS 'Unité des valeurs';


--
-- Name: COLUMN indicateur.methodo_pdf; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.methodo_pdf IS 'Nom du fichier de méthodologie (PDF)';


--
-- Name: COLUMN indicateur.confidentiel; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.confidentiel IS 'Donnée pouvant être confidentielle';


--
-- Name: COLUMN indicateur.disabled_for_zone; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.disabled_for_zone IS 'L''analyse est no nsélectionnable si le maillon courant est égal à cette valeur';


--
-- Name: COLUMN indicateur.only_for_zone; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.only_for_zone IS 'L''analyse est sélectionnable uniquement';


--
-- Name: COLUMN indicateur.isratio; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.isratio IS 'Détermine si la valeur de l''indicateur est un ratio pour la prise en compte côté front';


--
-- Name: COLUMN indicateur.filter; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.filter IS 'Filtres à appliquer par défaut pour cette analyse';


--
-- Name: COLUMN indicateur.decimals; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.decimals IS 'Nombre de décimales après la virgule';


--
-- Name: COLUMN indicateur.ui_theme; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.ui_theme IS 'Thème de l''indicateur pour l''interface';


--
-- Name: COLUMN indicateur.data_ratio; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.data_ratio IS 'Identifiant d''analyse à soustraire à l''analyse courante pour chaque territoire';


--
-- Name: COLUMN indicateur.years; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.years IS 'Liste des années disponibles dans les données';


--
-- Name: COLUMN indicateur.display_total; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.display_total IS 'Détermine si on doit afficher le total pour cette analyse ou pas';


--
-- Name: COLUMN indicateur.active; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.active IS 'Précise si l''analyse est active (visible) ou non';


--
-- Name: COLUMN indicateur.region; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.indicateur.region IS 'Identifiant de la région pour laquelle cette analyse est dédiée';


--
-- Name: indicateur_id_seq; Type: SEQUENCE; Schema: meta; Owner: terristory
--

CREATE SEQUENCE meta.indicateur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE meta.indicateur_id_seq OWNER TO terristory;

--
-- Name: indicateur_id_seq; Type: SEQUENCE OWNED BY; Schema: meta; Owner: terristory
--

ALTER SEQUENCE meta.indicateur_id_seq OWNED BY meta.indicateur.id;


--
-- Name: objectifs; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.objectifs (
    titre character varying,
    annee integer,
    valeur double precision,
    graphique character varying NOT NULL,
    annee_modifiee boolean,
    id character varying NOT NULL,
    region character varying NOT NULL
);


ALTER TABLE meta.objectifs OWNER TO terristory;

--
-- Name: COLUMN objectifs.titre; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.objectifs.titre IS 'Titre de l''objectif (SRADDET, REPOS, SNBC etc.)';


--
-- Name: COLUMN objectifs.annee; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.objectifs.annee IS 'Année';


--
-- Name: COLUMN objectifs.valeur; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.objectifs.valeur IS 'Année';


--
-- Name: COLUMN objectifs.graphique; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.objectifs.graphique IS 'Graphique auquel ajouter l''objectif';


--
-- Name: COLUMN objectifs.annee_modifiee; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.objectifs.annee_modifiee IS 'L''année a-t-elle été modifiée ?';


--
-- Name: COLUMN objectifs.id; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.objectifs.id IS 'Identifiant unique';


--
-- Name: COLUMN objectifs.region; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.objectifs.region IS 'Région pour laquelle l''objectif est saisi';


--
-- Name: parametres_objectifs; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.parametres_objectifs (
    titre character varying NOT NULL,
    annee_reference integer,
    graphique character varying NOT NULL,
    couleur character varying,
    region character varying NOT NULL,
    derniere_annee_projection integer,
    description character varying
);


ALTER TABLE meta.parametres_objectifs OWNER TO terristory;

--
-- Name: COLUMN parametres_objectifs.titre; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.parametres_objectifs.titre IS 'Titre de l''objectif (SRADDET, REPOS, SNBC etc.)';


--
-- Name: COLUMN parametres_objectifs.annee_reference; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.parametres_objectifs.annee_reference IS 'Année de référence';


--
-- Name: COLUMN parametres_objectifs.graphique; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.parametres_objectifs.graphique IS 'Graphique où ajouter cet objectif';


--
-- Name: COLUMN parametres_objectifs.couleur; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.parametres_objectifs.couleur IS 'Couleur de la trajectoire cible';


--
-- Name: COLUMN parametres_objectifs.region; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.parametres_objectifs.region IS 'Région où l''objectif est saisi';


--
-- Name: perimetre_geographique; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.perimetre_geographique (
    nom_table text NOT NULL,
    region character varying NOT NULL,
    date_perimetre integer,
    donnees_registre boolean
);


ALTER TABLE meta.perimetre_geographique OWNER TO terristory;

--
-- Name: tableau_affectation; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.tableau_affectation (
    tableau integer NOT NULL,
    zone character varying NOT NULL,
    zone_id character varying
);


ALTER TABLE meta.tableau_affectation OWNER TO terristory;

--
-- Name: COLUMN tableau_affectation.tableau; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_affectation.tableau IS 'Identifiant du tableau de bord';


--
-- Name: COLUMN tableau_affectation.zone; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_affectation.zone IS 'Type de territoire';


--
-- Name: COLUMN tableau_affectation.zone_id; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_affectation.zone_id IS 'Identifiant du territoire si tableau de bord spécifique à un territoire donné';


--
-- Name: tableau_bord; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.tableau_bord (
    id integer NOT NULL,
    titre character varying NOT NULL,
    mail character varying NOT NULL,
    description text,
    region character varying NOT NULL,
    date timestamp without time zone DEFAULT '2021-01-06 17:31:38.92126'::timestamp without time zone NOT NULL
);


ALTER TABLE meta.tableau_bord OWNER TO terristory;

--
-- Name: COLUMN tableau_bord.id; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_bord.id IS 'Identifiant';


--
-- Name: COLUMN tableau_bord.titre; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_bord.titre IS 'Titre du tableau de bord';


--
-- Name: COLUMN tableau_bord.mail; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_bord.mail IS 'Mail de l''utilisateur·rice';


--
-- Name: COLUMN tableau_bord.description; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_bord.description IS 'Description du tableau de bord';


--
-- Name: COLUMN tableau_bord.region; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_bord.region IS 'Identifiant de la région';


--
-- Name: COLUMN tableau_bord.date; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_bord.date IS 'Date de création/mise à jour';


--
-- Name: tableau_bord_id_seq; Type: SEQUENCE; Schema: meta; Owner: terristory
--

CREATE SEQUENCE meta.tableau_bord_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE meta.tableau_bord_id_seq OWNER TO terristory;

--
-- Name: tableau_bord_id_seq; Type: SEQUENCE OWNED BY; Schema: meta; Owner: terristory
--

ALTER SEQUENCE meta.tableau_bord_id_seq OWNED BY meta.tableau_bord.id;


--
-- Name: tableau_thematique; Type: TABLE; Schema: meta; Owner: terristory
--

CREATE TABLE meta.tableau_thematique (
    id integer NOT NULL,
    titre character varying,
    tableau integer NOT NULL,
    graphiques json NOT NULL,
    description character varying,
    ordre integer
);


ALTER TABLE meta.tableau_thematique OWNER TO terristory;

--
-- Name: COLUMN tableau_thematique.id; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_thematique.id IS 'Identifiant';


--
-- Name: COLUMN tableau_thematique.titre; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_thematique.titre IS 'Titre';


--
-- Name: COLUMN tableau_thematique.tableau; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_thematique.tableau IS 'Identifiant du tableau de bord';


--
-- Name: COLUMN tableau_thematique.graphiques; Type: COMMENT; Schema: meta; Owner: terristory
--

COMMENT ON COLUMN meta.tableau_thematique.graphiques IS 'Ensemble des graphiques';


--
-- Name: tableau_thematique_id_seq; Type: SEQUENCE; Schema: meta; Owner: terristory
--

CREATE SEQUENCE meta.tableau_thematique_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE meta.tableau_thematique_id_seq OWNER TO terristory;

--
-- Name: tableau_thematique_id_seq; Type: SEQUENCE OWNED BY; Schema: meta; Owner: terristory
--

ALTER SEQUENCE meta.tableau_thematique_id_seq OWNED BY meta.tableau_thematique.id;


--
-- Name: indicateur id; Type: DEFAULT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.indicateur ALTER COLUMN id SET DEFAULT nextval('meta.indicateur_id_seq'::regclass);


--
-- Name: tableau_bord id; Type: DEFAULT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_bord ALTER COLUMN id SET DEFAULT nextval('meta.tableau_bord_id_seq'::regclass);


--
-- Name: tableau_thematique id; Type: DEFAULT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_thematique ALTER COLUMN id SET DEFAULT nextval('meta.tableau_thematique_id_seq'::regclass);


--
-- Data for Name: affectation_objectifs; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.affectation_objectifs (titre, graphique, region, type_territoire, code_territoire) FROM stdin;
1000001 1101101 1100010 1101001 1110100 1101001 1101111 1101110 100000 1110100 1100101 1110010 1110010 1101001 1110100 1101111 1101001 1110010 1100101 100000 110010 110000 110011 110000	conso_energetique	auvergne-rhone-alpes	departements	
1000001 1101101 1100010 1101001 1110100 1101001 1101111 1101110 100000 1110100 1100101 1110010 1110010 1101001 1110100 1101111 1101001 1110010 1100101 100000 110010 110000 110011 110000	conso_energetique	auvergne-rhone-alpes	regions	
1010010 1000101 1010000 1001111 1010011	conso_energetique	auvergne-rhone-alpes	epci	200073096
1010010 1000101 1010000 1001111 1010011	conso_energetique	auvergne-rhone-alpes	epcis	
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	conso_energetique	auvergne-rhone-alpes	regions	
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	prod_enr	auvergne-rhone-alpes	regions	
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	emission_ges	auvergne-rhone-alpes	regions	
1110100 1100101 1110011 1110100	conso_energetique	auvergne-rhone-alpes	regions	
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	prod_enr	auvergne-rhone-alpes	epcis	
\.


--
-- Data for Name: association_graphique_table; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.association_graphique_table (graphique, "table", region) FROM stdin;
Consommation d'énergie	conso_energetique	auvergne-rhone-alpes
Production EnR	prod_enr	auvergne-rhone-alpes
Émissions de gaz à effet de serre	emission_ges	auvergne-rhone-alpes
Émissions de COVNM	air_polluant_covnm	auvergne-rhone-alpes
Émissions de NOx	air_polluant_nox	auvergne-rhone-alpes
Émissions de SO2	air_polluant_sox	auvergne-rhone-alpes
Émissions de PM2.5	air_polluant_pm25	auvergne-rhone-alpes
Émissions de PM10	air_polluant_pm10	auvergne-rhone-alpes
Émissions de NH3	air_polluant_nh3	auvergne-rhone-alpes
\.


--
-- Data for Name: categorie; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.categorie (nom, modalite, modalite_id, couleur, ordre, region) FROM stdin;
altitude	[0-500m[	1	#0f5117	1	auvergne-rhone-alpes
altitude	[500-1000m[	2	#c0d362	2	auvergne-rhone-alpes
altitude	[1000-1500m[	3	#f29400	3	auvergne-rhone-alpes
altitude	[1500-2000m[	4	#a74d47	4	auvergne-rhone-alpes
altitude	>2000m	5	#cfd1d2	5	auvergne-rhone-alpes
contrainte_photovoltaique	Non	1	#1fa22e	1	auvergne-rhone-alpes
contrainte_photovoltaique	Oui	2	#ff0000	2	auvergne-rhone-alpes
energie	Combustibles Minéraux Solides	1	#91d4f2	7	auvergne-rhone-alpes
energie	Électricité	2	#006ab0	4	auvergne-rhone-alpes
energie	ENR thermiques	3	#ff0000	1	auvergne-rhone-alpes
energie	Non-énergétique	4	#f29400	5	auvergne-rhone-alpes
old_energie	Produits pétroliers	5	#cfd1d2	6	auvergne-rhone-alpes
old_energie	Gaz	7	#1a171b	9	auvergne-rhone-alpes
old_energie	Chauffage et froid urbain	9	#fecf44	8	auvergne-rhone-alpes
old_energie	Déchets	10	#616161	10	auvergne-rhone-alpes
enjeu	Pas de contrainte	1	#92d050	1	auvergne-rhone-alpes
enjeu	Point de vigilance	3	#ffff00	2	auvergne-rhone-alpes
enjeu	Zone d'exclusion potentielle	2	#fecf44	3	auvergne-rhone-alpes
enjeu	Point de vigilance et zone d'exclusion potentielle	4	#ff9900	4	auvergne-rhone-alpes
enjeu	Contraintes fortes	5	#ff0000	5	auvergne-rhone-alpes
enjeu	Zone d'exclusion - Implantation interdite	7	#000000	7	auvergne-rhone-alpes
essence	Forêts de feuillus	1	#c0d362	1	auvergne-rhone-alpes
essence	Forêts mixtes	2	#fecf44	3	auvergne-rhone-alpes
essence	Forêts de résineux	3	#a74d47	2	auvergne-rhone-alpes
facture_energie	Combustibles Minéraux Solides	1	#91d4f2	7	auvergne-rhone-alpes
facture_energie	Électricité	2	#006ab0	4	auvergne-rhone-alpes
facture_energie	ENR thermiques	3	#ff0000	1	auvergne-rhone-alpes
facture_energie	Non-énergétique	4	#f29400	5	auvergne-rhone-alpes
facture_energie	Produits pétroliers	5	#cfd1d2	6	auvergne-rhone-alpes
facture_energie	Gaz	7	#1a171b	9	auvergne-rhone-alpes
facture_energie	Chauffage et froid urbain	9	#fecf44	8	auvergne-rhone-alpes
facture_energie	Déchets	10	#616161	10	auvergne-rhone-alpes
facture_secteur	Résidentiel	1	#fecf44	3	auvergne-rhone-alpes
facture_secteur	Agriculture, sylviculture et aquaculture	2	#1fa22E	1	auvergne-rhone-alpes
facture_secteur	Tertiaire	3	#f29400	7	auvergne-rhone-alpes
facture_secteur	Gestion des déchets	4	#933588	2	auvergne-rhone-alpes
facture_secteur	Industrie hors branche énergie	5	#cfd1d2	5	auvergne-rhone-alpes
facture_secteur	Autres transports	7	#91d4f2	6	auvergne-rhone-alpes
facture_usage	Chauffage	1	#e73836	1	auvergne-rhone-alpes
facture_usage	Eau Chaude Sanitaire	2	#f4b025	2	auvergne-rhone-alpes
facture_usage	Installations agricoles	9	#76933c	12	auvergne-rhone-alpes
facture_usage	Cuisson	3	#e71d73	3	auvergne-rhone-alpes
facture_usage	Eclairage	16	#ffff00	8	auvergne-rhone-alpes
facture_usage	Froid	7	#91d4f2	5	auvergne-rhone-alpes
facture_usage	Autres usages	5	#575756	17	auvergne-rhone-alpes
facture_usage	Climatisation	11	#8db4e2	7	auvergne-rhone-alpes
facture_usage	Eclairage public	17	#fcd5b4	9	auvergne-rhone-alpes
facture_usage	Electricité spécifique	4	#009cdd	4	auvergne-rhone-alpes
facture_usage	Cheptels	14	#93c127	13	auvergne-rhone-alpes
facture_usage	Cultures	15	#2ab33f	14	auvergne-rhone-alpes
facture_usage	Engins agricoles	10	#908958	11	auvergne-rhone-alpes
facture_usage	Transport de marchandises	13	#a74d47	16	auvergne-rhone-alpes
facture_usage	Transport de personnes	12	#007f7a	15	auvergne-rhone-alpes
orientation_photovoltaique	Est-Ouest	1	#f29400	1	auvergne-rhone-alpes
orientation_photovoltaique	Sud	2	#ff0000	2	auvergne-rhone-alpes
orientation_photovoltaique	Toit plat	3	#fecf44	3	auvergne-rhone-alpes
pente	[0-10%[	1	#1fa22e	1	auvergne-rhone-alpes
pente	[10-20%[	2	#c0d362	2	auvergne-rhone-alpes
pente	[20-30%[	3	#fecf44	3	auvergne-rhone-alpes
pente	[30-40%[	4	#f29400	4	auvergne-rhone-alpes
pente	[40-50%[	5	#b36e00	5	auvergne-rhone-alpes
pente_eolien	[0-5%[	1	#fef0d9	1	auvergne-rhone-alpes
pente_eolien	[5-10%[	2	#fdcc8a	2	auvergne-rhone-alpes
type_cmbl	Chauffage urbain	1	#fecf44	1	auvergne-rhone-alpes
type_cmbl	Pompes à chaleur électricité	2	#ff0000	2	auvergne-rhone-alpes
type_cmbl	Chauffage au fioul	3	#cfd1d2	3	auvergne-rhone-alpes
type_cmbl	Chauffage au gaz	4	#1a171b	4	auvergne-rhone-alpes
type_cmbl	Chauffage électrique	5	#006ab0	5	auvergne-rhone-alpes
type_cmbl	Chaudière – autres	6	#5B3C11	6	auvergne-rhone-alpes
type_cmbl	Autres	7	#1fa22E	7	auvergne-rhone-alpes
pente_eolien	[10-15%[	3	#fc8d59	3	auvergne-rhone-alpes
pente_eolien	[15-20%[	4	#e34a33	4	auvergne-rhone-alpes
pente_eolien	>20%	5	#b30000	5	auvergne-rhone-alpes
propriete	Forêts privées	1	#f29400	1	auvergne-rhone-alpes
propriete	Forêts publiques	2	#006ab0	2	auvergne-rhone-alpes
secteur	Résidentiel	1	#fecf44	3	auvergne-rhone-alpes
secteur	Agriculture, sylviculture et aquaculture	2	#1fa22E	1	auvergne-rhone-alpes
secteur	Tertiaire	3	#f29400	7	auvergne-rhone-alpes
secteur	Gestion des déchets	4	#933588	2	auvergne-rhone-alpes
old_secteur	Industrie hors branche énergie	5	#cfd1d2	5	auvergne-rhone-alpes
old_secteur	Autres transports	7	#91d4f2	6	auvergne-rhone-alpes
secteur_clap17	Agriculture, sylviculture et pêche	1	#1fa22e	1	auvergne-rhone-alpes
secteur_clap17	industrie, énergie, eau, déchets	2	#512800	2	auvergne-rhone-alpes
secteur_clap17	Industrie agroalimentaire et tabac	3	#6f0d07	3	auvergne-rhone-alpes
secteur_clap17	Cokéfaction et raffinage	4	#ba160c	4	auvergne-rhone-alpes
secteur_clap17	Fabrication équipements et machines	5	#c7443c	5	auvergne-rhone-alpes
secteur_clap17	Fabrication autres produits industriels	7	#f4b266	7	auvergne-rhone-alpes
secteur_clap17	Commerce	9	#022d56	9	auvergne-rhone-alpes
secteur_clap17	Transports et entreposage	10	#03396c	10	auvergne-rhone-alpes
secteur_clap17	Hébergement et restauration	11	#356089	11	auvergne-rhone-alpes
secteur_clap17	Information et communication	12	#398084	12	auvergne-rhone-alpes
secteur_clap17	Activités financières et d'assurance	13	#49a4aa	13	auvergne-rhone-alpes
secteur_clap17	Activités immobilières	14	#329ad2	14	auvergne-rhone-alpes
secteur_clap17	Activités scientifiques et techniques, services	15	#7fc0e3	15	auvergne-rhone-alpes
secteur_clap17	Administration, enseignement, santé, social	16	#FFB90F	17	auvergne-rhone-alpes
secteur_clap17	Autres activités de services	17	#cce5f3	16	auvergne-rhone-alpes
secteur_clap5	Agriculture, sylviculture, pêche	1	#1fa22e	1	auvergne-rhone-alpes
secteur_clap5	Industrie	2	#6f0d07	2	auvergne-rhone-alpes
secteur_clap5	Construction	3	#AAAEBC	3	auvergne-rhone-alpes
secteur_clap5	Commerce, transports et services divers	4	#356089	4	auvergne-rhone-alpes
secteur_clap5	Administration publique, enseignement, santé et action sociale	5	#FFB90F	5	auvergne-rhone-alpes
secteur_solaire_thermique	Industriel	1	#fecf44	1	auvergne-rhone-alpes
secteur_solaire_thermique	Résidentiel individuel	3	#cfd1d2	2	auvergne-rhone-alpes
secteur_solaire_thermique	Résidentiel collectif	2	#f29400	3	auvergne-rhone-alpes
type_collectes	Ordures ménagères et assimilées	1	#0070c0	1	auvergne-rhone-alpes
type_collectes	Déchets collectés en déchèterie	2	#ff9900	2	auvergne-rhone-alpes
type_collectes	Autres déchets collectés	3	#548235	3	auvergne-rhone-alpes
type_dechets	Emballages et papiers (collecte)	4	#bdd7ee	4	auvergne-rhone-alpes
type_dechets	Autres déchets hors déchèterie (collecte)	5	#ffc000	5	auvergne-rhone-alpes
an_cons	[1000, 1918]	1	#B76E0F	1	auvergne-rhone-alpes
type_dechets	Biodéchets y compris déchets verts (collecte)	1	#78ca7a	1	auvergne-rhone-alpes
type_dechets	Ordures ménagères résiduelles (collecte)	3	#a6a6a6	3	auvergne-rhone-alpes
type_dechets	Autre déchets en déchèterie y compris déchets verts	7	#7030a0	7	auvergne-rhone-alpes
type_dechets	Verre (collecte)	2	#216c0a	2	auvergne-rhone-alpes
old_secteur	Industrie branche énergie	8	#d1d3ac	8	auvergne-rhone-alpes
an_cons	[1919, 1945]	2	#C08232	2	auvergne-rhone-alpes
type_photovoltaique	Bâtiment administratif	1	#fecf44	1	auvergne-rhone-alpes
type_photovoltaique	Bâtiment agricole	2	#1fa22e	2	auvergne-rhone-alpes
type_photovoltaique	Bâtiment commercial	3	#f29400	3	auvergne-rhone-alpes
type_photovoltaique	Bâtiment indifférencié collectif	4	#006ab0	4	auvergne-rhone-alpes
type_photovoltaique	Bâtiment indifférencié individuel	5	#91d4f2	5	auvergne-rhone-alpes
an_cons	[1946, 1970]	3	#C8934F	3	auvergne-rhone-alpes
type_photovoltaique	Parkings	7	#a74d47	7	auvergne-rhone-alpes
an_cons	[1971, 1990]	4	#CEA166	4	auvergne-rhone-alpes
type_prod_enr	Production du solaire thermique	2	#f29400	8	auvergne-rhone-alpes
type_prod_enr	Production nette des PAC	1	#e32d61	9	auvergne-rhone-alpes
type_prod_enr	Production photovoltaïque	3	#fecf44	1	auvergne-rhone-alpes
type_prod_enr	Production éolienne	4	#91d4f2	2	auvergne-rhone-alpes
type_prod_enr	Production hydroélectrique - puiss inf 4,5 MW	5	#21a5ff	3	auvergne-rhone-alpes
type_prod_enr	Production hydroélectrique - puiss sup 4,5 MW	12	#006ab0	4	auvergne-rhone-alpes
type_prod_enr	Valorisation électrique des déchets	9	#933588	5	auvergne-rhone-alpes
type_prod_enr	Valorisation thermique des déchets	10	#491b44	10	auvergne-rhone-alpes
secteur_clap17	Industrie branche énergie	8	#AAAEBC	8	auvergne-rhone-alpes
type_prod_enr	Valorisation électrique du biogaz	7	#908958	6	auvergne-rhone-alpes
type_prod_enr	Autre valorisation électrique renouvelable	11	#cfd1d2	7	auvergne-rhone-alpes
type_occsol	prairies	2	#c0d362	2	auvergne-rhone-alpes
usage	Chauffage	1	#e73836	1	auvergne-rhone-alpes
usage	Eau Chaude Sanitaire	2	#f4b025	2	auvergne-rhone-alpes
usage	Cuisson	3	#e71d73	3	auvergne-rhone-alpes
usage	Electricité spécifique	4	#009cdd	4	auvergne-rhone-alpes
old_usage	Autres usages	5	#575756	17	auvergne-rhone-alpes
old_usage	Industriel	6	#cfd1d2	10	auvergne-rhone-alpes
old_usage	Froid	7	#91d4f2	5	auvergne-rhone-alpes
old_usage	Industrie branche énergie	8	#006ab0	6	auvergne-rhone-alpes
old_usage	Installations agricoles	9	#76933c	12	auvergne-rhone-alpes
old_usage	Engins agricoles	10	#908958	11	auvergne-rhone-alpes
old_usage	Climatisation	11	#8db4e2	7	auvergne-rhone-alpes
old_usage	Transport de personnes	12	#007f7a	15	auvergne-rhone-alpes
old_usage	Transport de marchandises	13	#a74d47	16	auvergne-rhone-alpes
old_usage	Cheptels	14	#93c127	13	auvergne-rhone-alpes
old_usage	Cultures	15	#2ab33f	14	auvergne-rhone-alpes
old_usage	Eclairage	16	#ffff00	8	auvergne-rhone-alpes
old_usage	Eclairage public	17	#fcd5b4	9	auvergne-rhone-alpes
old_usage	Matière première	18	#583831	16	auvergne-rhone-alpes
an_cons	[1991, 2005]	5	#D7B589	5	auvergne-rhone-alpes
trans_ppal	Deux-roues motorisé	1	#fecf44	1	auvergne-rhone-alpes
facture_energie	Industrie branche énergie	8	#933588	3	auvergne-rhone-alpes
facture_usage	Industrie branche énergie	8	#006ab0	6	auvergne-rhone-alpes
facture_secteur	Industrie branche énergie	8	#d1d3ac	8	auvergne-rhone-alpes
csp	Agriculteurs exploitants	1	#fecf44	1	auvergne-rhone-alpes
csp	Professions Intermédiaires	2	#1fa22E	2	auvergne-rhone-alpes
csp	Cadres et professions intellectuelles supérieures	3	#f29400	3	auvergne-rhone-alpes
csp	Ouvriers	4	#933588	4	auvergne-rhone-alpes
csp	Artisans, commerçants et chefs d'entreprise	5	#cfd1d2	5	auvergne-rhone-alpes
type_menage	Personne seule	1	#fecf44	1	auvergne-rhone-alpes
type_menage	Famille monoparentale	2	#1fa22E	2	auvergne-rhone-alpes
type_menage	Autres cas	3	#f29400	3	auvergne-rhone-alpes
type_menage	Famille-couple à un actif	4	#933588	4	auvergne-rhone-alpes
type_menage	Famille-couple à deux actifs	5	#cfd1d2	5	auvergne-rhone-alpes
trans_ppal	Voiture, camion, fourgonnette	2	#1fa22E	2	auvergne-rhone-alpes
trans_ppal	Marche à pied	3	#f29400	3	auvergne-rhone-alpes
trans_ppal	Transports en commun	4	#933588	4	auvergne-rhone-alpes
enjeu	Contraintes fortes et  zone d'exclusion potentielle	6	#c00000	6	auvergne-rhone-alpes
csp	Employés	6	#a74d47	6	auvergne-rhone-alpes
facture_energie	Organo-carburants	6	#c0d362	2	auvergne-rhone-alpes
facture_secteur	Transport routier	6	#a74d47	4	auvergne-rhone-alpes
facture_usage	Industriel	6	#cfd1d2	10	auvergne-rhone-alpes
pente	[50-60%[	6	#663f00	6	auvergne-rhone-alpes
old_energie	Organo-carburants	6	#c0d362	2	auvergne-rhone-alpes
type_prod_enr	Valorisation thermique du biogaz	8	#0f5117	11	auvergne-rhone-alpes
type_prod_enr	Valorisation par injection	13	#73D511	13	auvergne-rhone-alpes
type_occsol	cultures	1	#fecf44	1	auvergne-rhone-alpes
type_occsol	forêts	3	#0f5117	3	auvergne-rhone-alpes
type_occsol	vignobles	4	#a74d47	4	auvergne-rhone-alpes
type_occsol	vergers	5	#f29400	5	auvergne-rhone-alpes
type_prod_enr	Valorisation du bois et autres biomasses solides	6	#a74d47	12	auvergne-rhone-alpes
secteur_clap17	Fabrication de matériels de transport	6	#ed7f00	6	auvergne-rhone-alpes
type_dechets	Gravats en déchèterie	6	#bf8f00	6	auvergne-rhone-alpes
type_photovoltaique	Bâtiment industriel	6	#cfd1d2	6	auvergne-rhone-alpes
old_secteur	Transport routier	6	#a74d47	4	auvergne-rhone-alpes
trans_ppal	Vélo (y compris à assistance électrique)	6	#cfd1d2	6	auvergne-rhone-alpes
trans_ppal	Pas de transport : travail au domicile	5	#a74d47	5	auvergne-rhone-alpes
an_cons	[2006, 2100]	6	#E0C9AC	6	auvergne-rhone-alpes
et_ener	A	1	#00ff00	1	auvergne-rhone-alpes
et_ener	B	2	#4dff00	2	auvergne-rhone-alpes
et_ener	C	3	#b3ff00	3	auvergne-rhone-alpes
et_ener	D	4	#ffff00	4	auvergne-rhone-alpes
et_ener	E	5	#ffb300	5	auvergne-rhone-alpes
et_ener	F	6	#ff4d00	6	auvergne-rhone-alpes
et_ener	G	7	#ff0000	7	auvergne-rhone-alpes
old_energie	Non identifié	8	#933588	3	auvergne-rhone-alpes
type_logement	Appartement	1	#E58F65	1	auvergne-rhone-alpes
type_logement	Maison	2	#D05353	2	auvergne-rhone-alpes
carburant	Essence	1	#91d4f2	1	auvergne-rhone-alpes
carburant	Diésel	2	#006ab0	2	auvergne-rhone-alpes
carburant	Électricité	3	#ff0000	3	auvergne-rhone-alpes
carburant	GPL	4	#f29400	4	auvergne-rhone-alpes
carburant	GNV	5	#cfd1d2	5	auvergne-rhone-alpes
type_vehicule	Voitures particulières	3	#ff0000	4	auvergne-rhone-alpes
type_vehicule	Autocars	4	#f29400	5	auvergne-rhone-alpes
type_vehicule	Deux roues	5	#cfd1d2	6	auvergne-rhone-alpes
hebdomadaire	Supermarchés et hypermarchés	1	#1a171b	1	auvergne-rhone-alpes
hebdomadaire	Réparation automobile	2	#fecf44	2	auvergne-rhone-alpes
hebdomadaire	Bibliothèques	3	#616161	3	auvergne-rhone-alpes
hebdomadaire	Postes	4	#c0d362	4	auvergne-rhone-alpes
hebdomadaire	Pharmacies	5	#933588	5	auvergne-rhone-alpes
hebdomadaire	Médecins généralistes et maisons de santé pluridisciplinaires	6	#91d4f2	6	auvergne-rhone-alpes
hebdomadaire	Terrains de grands jeux (football, rugby, etc)	7	#006ab0	7	auvergne-rhone-alpes
hebdomadaire	Salles multisports (Gymnases)	8	#ff0000	8	auvergne-rhone-alpes
hebdomadaire	Bassins de natation	9	#f29400	9	auvergne-rhone-alpes
quotidien	Boulangeries	1	#91d4f2	1	auvergne-rhone-alpes
quotidien	Librairies	2	#006ab0	2	auvergne-rhone-alpes
quotidien	Ecoles	3	#ff0000	3	auvergne-rhone-alpes
quotidien	Crèches	4	#f29400	4	auvergne-rhone-alpes
quotidien	Superettes et épiceries	5	#cfd1d2	5	auvergne-rhone-alpes
type_vehicule	Véhicules utilitaires légers (autres usages)	8	#006ab0	3	auvergne-rhone-alpes
type_vehicule	Tramway	7	#93c127	8	auvergne-rhone-alpes
type_vehicule	Bus urbains	6	#2ab33f	7	auvergne-rhone-alpes
type_vehicule	Véhicules utilitaires légers (marchandises)	2	#a74d47	2	auvergne-rhone-alpes
type_vehicule	Poids lourds (marchandises)	1	#91d4f2	1	auvergne-rhone-alpes
type_infrastructure_cyclable	Routes	1	#91d4f2	1	auvergne-rhone-alpes
type_infrastructure_cyclable	Bandes cyclables	2	#006ab0	2	auvergne-rhone-alpes
type_infrastructure_cyclable	Pistes cyclables	3	#ff0000	3	auvergne-rhone-alpes
type_infrastructure_cyclable	Voies partagées (bus)	4	#f29400	4	auvergne-rhone-alpes
type_infrastructure_cyclable	Voies partagées (voitures)	5	#cfd1d2	5	auvergne-rhone-alpes
type_infrastructure_cyclable	Voies partagées (piétons)	6	#91d4f2	6	auvergne-rhone-alpes
filiere_methanisation	CIVE	1	#1fa22e	1	auvergne-rhone-alpes
filiere_methanisation	Déjections liées à l'élevage	2	#a74d47	2	auvergne-rhone-alpes
filiere_methanisation	Résidus de cultures	3	#fecf44	3	auvergne-rhone-alpes
filiere_methanisation	Assainissement collectif	4	#f29400	4	auvergne-rhone-alpes
filiere_methanisation	Biodéchets ménagers (collecte sélective)	5	#006ab0	5	auvergne-rhone-alpes
filiere_methanisation	Distribution	6	#f29400	6	auvergne-rhone-alpes
filiere_methanisation	restauration collective santé social	7	#EECFA1	7	auvergne-rhone-alpes
filiere_methanisation	restauration collective scolaire	8	#B0E2FF	8	auvergne-rhone-alpes
filiere_methanisation	Restauration commerciale	9	#933588	9	auvergne-rhone-alpes
filiere_methanisation	Déchets verts	10	#0f5117	10	auvergne-rhone-alpes
filiere_methanisation	Petits commerces	11	#e32d61	11	auvergne-rhone-alpes
utilisation_potentiel	Potentiel restant	1	#a74d47	2	auvergne-rhone-alpes
utilisation_potentiel	Potentiel utilisé	2	#1fa22e	1	auvergne-rhone-alpes
\.


--
-- Data for Name: chart; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.chart (indicateur, titre, type, categorie, visible, ordre, data_type) FROM stdin;
3	Par filières de production	pie	type_prod_enr	t	\N	\N
2	Par secteurs	pie	secteur	t	\N	\N
2	Par type d'énergie	pie	energie	t	\N	\N
2	Par usage	pie	usage	t	\N	\N
4	Par filière de production	pie	type_prod_enr	t	\N	\N
5	Par filières de production	hbar	type_prod_enr	t	\N	\N
6	Par secteurs	pie	secteur	t	\N	\N
6	Par type d'énergie	pie	energie	t	\N	\N
6	Par usages	pie	usage	t	\N	\N
1	Par secteurs	pie	secteur	t	\N	\N
1	Par type d'énergie	pie	energie	t	\N	\N
1	Par usages	pie	usage	t	\N	\N
\.


--
-- Data for Name: indicateur; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.indicateur (id, nom, data, type, color_start, color_end, unit, methodo_pdf, confidentiel, disabled_for_zone, only_for_zone, isratio, filter, decimals, ui_theme, data_ratio, years, display_total, active, region, data_deuxieme_representation, valeur_filtre_defaut, titre_legende, titre_legende_deuxieme_representation, concatenation, ordre_ui_theme, ordre_indicateur, data_type, donnees_exportables, titre_dans_infobulle, afficher_proportion, titre, moyenne_ponderee, afficher_calcul_et_donnees_table, titre_graphiques_indicateurs) FROM stdin;
4	Production EnR/hab	prod_enr*1000000/maille.population	choropleth	#ffffff	#007f7b	KWh/hab	Production_ENR_hab.pdf				f		0	Production d'énergie	\N	{2020}	t	f	auvergne-rhone-alpes	\N	\N	\N	\N	\N	10	\N	\N	\N	\N	\N	\N	\N	\N	\N
5	Part Enr/Consommation d'énergie	prod_enr	choropleth	#ffffff	#006961	%	Part_prod-enr_consommation.pdf	null			t		2	Production d'énergie	1	{2019,2018,2017,2016,2015,2014,2013,2012,2011}	t	t	auvergne-rhone-alpes	\N	\N	\N	\N	\N	10	2	\N	commune,departement,epci,scot,teposcv,crte	\N	\N	\N	\N	\N	\N
6	Émissions GES	emission_ges	circle	#007f7b	#007f7b	kteqco2	emission_ges.pdf	emission_ges			f		2	Émissions de GES	\N	{2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000,1990}	t	t	auvergne-rhone-alpes	\N	\N	\N	\N	\N	2	\N	\N	commune,departement,epci,scot,teposcv,crte	\N	\N	\N	\N	\N	\N
2	Consommation d'énergie résidentiel / hab	conso_energetique*1000000/maille.population	choropleth	#ffffff	#006961	kWh/hab	Consommation_residentiel_habitant.pdf	conso_energetique			t	secteur.Résidentiel	0	Consommation d'énergie	\N	{2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000,1990}	t	t	auvergne-rhone-alpes	\N	\N	\N	\N	\N	1	3	\N	commune,departement,epci,scot,teposcv,crte	\N	\N	\N	\N	\N	\N
3	Production ENR	prod_enr	circle	#007f7b	#007f7b	GWh	Production_ENR.pdf	null			f		2	Production d'énergie	\N	{2020,2019,2018,2017,2016,2015,2014,2013,2012,2011}	t	t	auvergne-rhone-alpes	\N	\N	\N	\N	\N	10	1	\N	commune,departement,epci,scot,teposcv,crte	\N	\N	\N	\N	\N	\N
1	Consommation d'énergie	conso_energetique	circle	#ffffff	#007f7b	GWh	2022105_16322467_20220901-compte-rendu-gt-technique-tests-1.pdf	conso_energetique			f		0	Consommation d'énergie	\N	{2019,2018,2017,2016,2015,2014,2013,2012,2011,2010,2005,2000,1990}	t	t	auvergne-rhone-alpes	\N	\N	\N	\N	\N	1	1	\N	commune,departement,epci,scot,teposcv,crte	\N	\N	\N	\N	\N	\N
\.

--
-- Data for Name: cesba; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.cesba (indicateur_id, numero, theme, "order") FROM stdin;
2	B1.3	B - Énergie / Consommation de ressources	desc
5	B1.20	B - Énergie / Consommation de ressources	asc
\.


--
-- Data for Name: objectifs; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.objectifs (titre, annee, valeur, graphique, annee_modifiee, id, region) FROM stdin;
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2015	0	Consommation d'énergie	f	0SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2016	-7.5	Consommation d'énergie	f	1SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2017	-15	Consommation d'énergie	f	2SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2018	-22.5	Consommation d'énergie	f	3SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2019	-30	Consommation d'énergie	t	4SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2020	-30	Consommation d'énergie	t	5SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2021	-30	Consommation d'énergie	f	6SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2022	-30	Consommation d'énergie	f	7SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2023	-30	Consommation d'énergie	f	8SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2024	-30	Consommation d'énergie	f	9SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2025	-30	Consommation d'énergie	f	10SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2026	-30	Consommation d'énergie	f	11SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2027	-30	Consommation d'énergie	f	12SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2028	-30	Consommation d'énergie	f	13SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2029	-30	Consommation d'énergie	f	14SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2030	-30	Consommation d'énergie	f	15SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2015	0	Production EnR	f	0SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2016	4.15	Production EnR	f	1SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2017	8.31	Production EnR	f	2SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2018	12.46	Production EnR	f	3SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2019	16.61	Production EnR	f	4SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2020	20.75	Production EnR	f	5SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2021	24.91	Production EnR	f	6SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2022	29.06	Production EnR	f	7SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2023	33.21	Production EnR	f	8SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2024	37.36	Production EnR	f	9SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2025	41.52	Production EnR	f	10SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2026	45.67	Production EnR	f	11SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2027	49.82	Production EnR	f	12SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2028	53.96	Production EnR	f	13SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2029	58.12	Production EnR	f	14SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2030	62.27	Production EnR	t	15SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2015	0	Émissions de gaz à effet de serre	f	0SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2016	-1.07	Émissions de gaz à effet de serre	f	1SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2017	-2.14	Émissions de gaz à effet de serre	f	2SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2018	-3.21	Émissions de gaz à effet de serre	f	3SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2019	-4.27	Émissions de gaz à effet de serre	f	4SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2020	-5.34	Émissions de gaz à effet de serre	t	5SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2021	-10.6	Émissions de gaz à effet de serre	f	6SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2022	-15.9	Émissions de gaz à effet de serre	f	7SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2023	-21.1	Émissions de gaz à effet de serre	f	8SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2024	-26.4	Émissions de gaz à effet de serre	f	9SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2025	-31.7	Émissions de gaz à effet de serre	f	10SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2026	-36.9	Émissions de gaz à effet de serre	f	11SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2027	-42.2	Émissions de gaz à effet de serre	f	12SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2028	-47.5	Émissions de gaz à effet de serre	f	13SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2029	-52.7	Émissions de gaz à effet de serre	f	14SRADDET 2030	auvergne-rhone-alpes
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2030	-58	Émissions de gaz à effet de serre	t	15SRADDET 2030	auvergne-rhone-alpes
\.


--
-- Data for Name: parametres_objectifs; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.parametres_objectifs (titre, annee_reference, graphique, couleur, region, derniere_annee_projection, description) FROM stdin;
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2015	Émissions de gaz à effet de serre	#ff0000	auvergne-rhone-alpes	2030	
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2015	Consommation d'énergie	#ff0000	auvergne-rhone-alpes	2030	
1010011 1010010 1000001 1000100 1000100 1000101 1010100 100000 110010 110000 110011 110000	2015	Production EnR	#ff0000	auvergne-rhone-alpes	2030	
\.


--
-- Data for Name: perimetre_geographique; Type: TABLE DATA; Schema: meta; Owner: terristory
--

COPY meta.perimetre_geographique (nom_table, region, date_perimetre, donnees_registre) FROM stdin;
prod_enr	auvergne-rhone-alpes	2021	\N
taux_pauvrete	auvergne-rhone-alpes	2021	\N
emission_ges	auvergne-rhone-alpes	2021	\N
conso_energetique	auvergne-rhone-alpes	2021	\N
\.


--
-- Data for Name: tableau_affectation; Type: TABLE DATA; Schema: meta; Owner: terristory
--

-- COPY meta.tableau_affectation (tableau, zone, zone_id) FROM stdin;
-- \.


--
-- Data for Name: tableau_bord; Type: TABLE DATA; Schema: meta; Owner: terristory
--

-- COPY meta.tableau_bord (id, titre, mail, description, region, date) FROM stdin;
-- \.


-- --
-- -- Data for Name: tableau_thematique; Type: TABLE DATA; Schema: meta; Owner: terristory
-- --

-- COPY meta.tableau_thematique (id, titre, tableau, graphiques, description, ordre) FROM stdin;
-- \.


--
-- Name: indicateur_id_seq; Type: SEQUENCE SET; Schema: meta; Owner: terristory
--

SELECT pg_catalog.setval('meta.indicateur_id_seq', 11781, true);


--
-- Name: tableau_bord_id_seq; Type: SEQUENCE SET; Schema: meta; Owner: terristory
--

SELECT pg_catalog.setval('meta.tableau_bord_id_seq', 6547, true);


--
-- Name: tableau_thematique_id_seq; Type: SEQUENCE SET; Schema: meta; Owner: terristory
--

SELECT pg_catalog.setval('meta.tableau_thematique_id_seq', 13977, true);


--
-- Name: affectation_objectifs affectation_objectifs_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.affectation_objectifs
    ADD CONSTRAINT affectation_objectifs_pkey PRIMARY KEY (titre, graphique, region, type_territoire, code_territoire);


--
-- Name: categorie categorie_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.categorie
    ADD CONSTRAINT categorie_pkey PRIMARY KEY (region, nom, modalite_id);


--
-- Name: chart chart_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.chart
    ADD CONSTRAINT chart_pkey PRIMARY KEY (indicateur, categorie);


--
-- Name: indicateur indicateur_nom_region_key; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.indicateur
    ADD CONSTRAINT indicateur_nom_region_key UNIQUE (nom, region);


--
-- Name: indicateur indicateur_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.indicateur
    ADD CONSTRAINT indicateur_pkey PRIMARY KEY (id);


--
-- Name: objectifs objectifs_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.objectifs
    ADD CONSTRAINT objectifs_pkey PRIMARY KEY (id, graphique, region);


--
-- Name: parametres_objectifs parametres_objectifs_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.parametres_objectifs
    ADD CONSTRAINT parametres_objectifs_pkey PRIMARY KEY (titre, graphique, region);


--
-- Name: perimetre_geographique pk_perimetre_geographique; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.perimetre_geographique
    ADD CONSTRAINT pk_perimetre_geographique PRIMARY KEY (nom_table, region);


--
-- Name: tableau_bord tableau_bord_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_bord
    ADD CONSTRAINT tableau_bord_pkey PRIMARY KEY (id);


--
-- Name: tableau_bord tableau_bord_titre_mail_region_key; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_bord
    ADD CONSTRAINT tableau_bord_titre_mail_region_key UNIQUE (titre, mail, region);


--
-- Name: tableau_thematique tableau_thematique_pkey; Type: CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_thematique
    ADD CONSTRAINT tableau_thematique_pkey PRIMARY KEY (id);


--
-- Name: chart_indicateur; Type: INDEX; Schema: meta; Owner: terristory
--

CREATE INDEX chart_indicateur ON meta.chart USING btree (indicateur);


--
-- Name: meta_categorie_modalite; Type: INDEX; Schema: meta; Owner: terristory
--

CREATE INDEX meta_categorie_modalite ON meta.categorie USING btree (modalite);


--
-- Name: meta_categorie_modalite_id; Type: INDEX; Schema: meta; Owner: terristory
--

CREATE INDEX meta_categorie_modalite_id ON meta.categorie USING btree (modalite_id);


--
-- Name: meta_categorie_region; Type: INDEX; Schema: meta; Owner: terristory
--

CREATE INDEX meta_categorie_region ON meta.categorie USING btree (region);


--
-- Name: cesba cesba_indicateur_id_fkey; Type: FK CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.cesba
    ADD CONSTRAINT cesba_indicateur_id_fkey FOREIGN KEY (indicateur_id) REFERENCES meta.indicateur(id);


--
-- Name: chart chart_indicateur_fkey; Type: FK CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.chart
    ADD CONSTRAINT chart_indicateur_fkey FOREIGN KEY (indicateur) REFERENCES meta.indicateur(id);


--
-- Name: objectifs fk_titre_objectif; Type: FK CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.objectifs
    ADD CONSTRAINT fk_titre_objectif FOREIGN KEY (titre, graphique, region) REFERENCES meta.parametres_objectifs(titre, graphique, region);


--
-- Name: indicateur indicateur_data_ratio_fkey; Type: FK CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.indicateur
    ADD CONSTRAINT indicateur_data_ratio_fkey FOREIGN KEY (data_ratio) REFERENCES meta.indicateur(id);


--
-- Name: tableau_affectation tableau_affectation_tableau_fkey; Type: FK CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_affectation
    ADD CONSTRAINT tableau_affectation_tableau_fkey FOREIGN KEY (tableau) REFERENCES meta.tableau_bord(id) ON DELETE CASCADE;


--
-- Name: tableau_bord tableau_bord_mail_fkey; Type: FK CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_bord
    ADD CONSTRAINT tableau_bord_mail_fkey FOREIGN KEY (mail) REFERENCES public.utilisateur(mail) ON DELETE CASCADE;


--
-- Name: tableau_thematique tableau_thematique_tableau_fkey; Type: FK CONSTRAINT; Schema: meta; Owner: terristory
--

ALTER TABLE ONLY meta.tableau_thematique
    ADD CONSTRAINT tableau_thematique_tableau_fkey FOREIGN KEY (tableau) REFERENCES meta.tableau_bord(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

