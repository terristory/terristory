﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import asyncio
import configparser
import subprocess
import sys
from pathlib import Path

from generate_data import *
from sqlalchemy import create_engine


def get_settings():
    here = Path().resolve().parent / "terriapi"

    home = Path.home() / ".terriapi.ini"
    settings = configparser.ConfigParser(allow_no_value=True)
    if home.exists():
        settings.read(home)
    elif (here / "terriapi.ini").exists():
        settings.read(here / "terriapi.ini")
    else:
        print(
            "ERREUR: aucun fichier de configuration .terriapi.ini dans {}".format(
                Path.home()
            )
        )
        print(
            "ERREUR: aucun fichier de configuration terriapi.ini dans {}".format(here)
        )
        print(
            "Choisissez l'un des deux emplacements et recopier le fichier d'exemple terriapi.ini.sample pour démarrer"
        )
        sys.exit(1)
    return settings


def connect_db(settings):
    database = "fixture"
    host = settings.get("api", "pg_host")
    user = settings.get("api", "pg_user")
    password = settings.get("api", "pg_password")
    port = settings.get("api", "pg_port")
    engine = create_engine(
        f"""postgresql://{user}:{password}@{host}:{port}/{database}"""
    )
    return engine


def create_users():
    """
    Create two users to be used in tests:
    * Alice Bob, username : user, password: user
    * Foo Bar, username : admin, password: admin
    """
    create_user = """terriapi-user --prenom Alice --nom Bob --password=user --mail user --profil utilisateur --actif --region auvergne-rhone-alpes --organisation TerriSTORY"""
    process = subprocess.Popen(create_user.split(" "))
    process.communicate()

    create_admin = """terriapi-user --prenom Foo --nom Bar --password=admin --mail admin --profil admin --actif --region auvergne-rhone-alpes --organisation TerriSTORY"""
    process = subprocess.Popen(create_admin.split(" "))
    process.communicate()


async def main():
    # create_users()
    settings = get_settings()
    engine = connect_db(settings)
    await generate_all(engine)
    await generate_geographical_geom(engine)
    await generate_poi(engine)


if __name__ == "__main__":
    asyncio.run(main())
