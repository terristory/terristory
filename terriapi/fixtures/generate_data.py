﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
import math

import numpy as np
import pandas as pd


async def generate_all(engine):
    # we get cities boundaries
    with engine.connect() as conn:
        res = conn.execute("""SELECT * FROM auvergne_rhone_alpes.commune""")
    communes = {x["code"]: dict(x) for x in res}

    # data list
    data_required = {
        "conso_energetique": {
            "min": 0,
            "max": 2500,
            "categories": ["secteur", "usage", "energie"],
        },
        "prod_enr": {
            "min": -50,
            "max": 3000,
            "categories": ["type_prod_enr"],
        },
        "emission_ges": {
            "min": 10,
            "max": 1000,
            "categories": ["secteur", "usage", "energie"],
        },
    }

    # categories data
    with engine.connect() as conn:
        res_cat = conn.execute(
            """
            SELECT nom, ARRAY_AGG(modalite_id) AS modalites
            FROM meta.categorie GROUP BY nom
        """
        )
    categories_values = {x["nom"]: x["modalites"] for x in res_cat}

    nb_cities = len(communes)

    for data, params in data_required.items():
        print("Fills indicator data:", data)
        # we retrieve associated possible values for categories linked to this data
        enabled_categories = params.get("categories", [])
        enabled_values_categories = [
            categories_values.get(cat, []) for cat in enabled_categories
        ]
        # we create the possible combinations of existing categories
        nb_combinations = math.prod(map(len, enabled_values_categories))
        combinations = np.array(np.meshgrid(*enabled_values_categories))
        combinations = combinations.reshape(
            (len(enabled_categories), nb_combinations)
        ).T

        # we create an array for all cities concerned
        array_cities = np.tile(combinations, (nb_cities, 1))

        # we create indexes
        index_cities = np.array([[c] * nb_combinations for c in communes])

        # and we fill it with current data
        df = pd.DataFrame(array_cities, index=index_cities.ravel())
        df.columns = enabled_categories
        df.index.name = "commune"
        df = df.reset_index(level=0)

        # we compute random values for this array
        rand_values = np.random.uniform(
            params.get("min", 0.0),
            params.get("max", 1.0),
            size=(len(df)),
        )
        df["valeur"] = rand_values
        df["annee"] = 2019

        # we add more randomness by changing randomly boundaries
        for i, cat in enumerate(enabled_categories):
            for one_cat in enabled_values_categories[i]:
                cat_name = enabled_categories[i]
                df.loc[df[cat_name] == one_cat, "valeur"] *= np.random.uniform(0.2, 2)

        # we save the data to the table
        with engine.connect() as conn:
            conn.execute(f"""TRUNCATE auvergne_rhone_alpes.{data}""")
        df.to_sql(
            data, engine, schema="auvergne_rhone_alpes", if_exists="append", index=False
        )


async def generate_geographical_geom(engine):
    """
    Generate the geographical data related to geographical divisions
    """
    with engine.connect() as conn:
        conn.execute(
            """
            TRUNCATE auvergne_rhone_alpes.departement;
            TRUNCATE auvergne_rhone_alpes.territoire;
            TRUNCATE auvergne_rhone_alpes.region;
        """
        )
        conn.execute(
            """INSERT INTO auvergne_rhone_alpes.departement
            SELECT
                CONCAT('Département ', LEFT(code, 2)) AS nom,
                LEFT(code, 2) AS code,
                ARRAY_AGG(code) AS communes,
                SUM(population) as population,
                0 as x,
                0 as y,
                ST_Multi(ST_Union(geom)) AS geom,
                NULL as siren
            FROM auvergne_rhone_alpes.commune
            GROUP BY LEFT(code, 2)"""
        )
        conn.execute(
            """
            UPDATE auvergne_rhone_alpes.departement
            SET x = ST_X(ST_Centroid(geom)),
                y = ST_Y(ST_Centroid(geom))"""
        )
        conn.execute(
            """
            INSERT INTO auvergne_rhone_alpes.territoire
                SELECT
                    code as commune,
                    LEFT(code, 2) AS code,
                    'departement' as type_territoire,
                    CONCAT('Département ', LEFT(code, 2)) as nom
                FROM auvergne_rhone_alpes.commune;"""
        )

        conn.execute(
            """INSERT INTO auvergne_rhone_alpes.region
            SELECT
                'Région Auvergne-Rhône-Alpes' AS nom,
                '84' AS code,
                ARRAY_AGG(code) AS communes,
                SUM(population) as population,
                0 as x,
                0 as y,
                ST_Multi(ST_Union(geom)) AS geom,
                NULL as siren
            FROM auvergne_rhone_alpes.commune"""
        )
        conn.execute(
            """
        UPDATE auvergne_rhone_alpes.region
        SET x = ST_X(ST_Centroid(geom)),
            y = ST_Y(ST_Centroid(geom))"""
        )
        conn.execute(
            """
            INSERT INTO auvergne_rhone_alpes.territoire
                SELECT
                    code as commune,
                    '84' as code,
                    'region' as type_territoire,
                    'Région Auvergne-Rhône-Alpes' as nom
                FROM auvergne_rhone_alpes.commune;"""
        )


async def generate_poi(engine):
    layers = {
        "bornes_hydrogene": {
            "nom": "Bornes hydrogènes",
            "theme": "Carburants alternatifs",
            "properties": {"nom": "Borne aléatoirement générée"},
            "max": 30,
        },
        "decheteries": {
            "nom": "Déchèteries",
            "theme": "Déchets",
            "properties": {
                "nom": "Déchèterie aléatoirement générée",
            },
            "type": [
                "Déchèteries mixtes",
                "Déchèteries particuliers",
                "Déchèteries professionnelles",
            ],
            "max": 150,
        },
    }

    with engine.connect() as conn:
        res = conn.execute("SELECT code, x, y FROM auvergne_rhone_alpes.departement;")
        departements = res.fetchall()

        # we empty the layer table
        conn.execute("""TRUNCATE auvergne_rhone_alpes_poi.layer""")

    for layer, params in layers.items():
        print("Fills poi layer:", layer)
        all_df = []
        for dep in departements:
            nb_pois = np.random.randint(params.get("min", 20), params.get("max", 150))
            xs = np.random.normal(dep["x"], 0.07 * dep["x"], size=(nb_pois,))
            ys = np.random.normal(dep["y"], 0.008 * dep["y"], size=(nb_pois,))
            df = pd.DataFrame([range(nb_pois), xs, ys]).transpose()
            df.columns = ["id", "x", "y"]
            if "type" in params:
                params["properties"]["type"] = params["type"][
                    np.random.randint(0, len(params["type"]))
                ]
            df["properties"] = json.dumps(params["properties"])
            all_df.append(df)

        pd.concat(all_df).to_sql(
            layer + "_data",
            engine,
            schema="auvergne_rhone_alpes_poi",
            if_exists="replace",
            index=False,
        )

        with engine.connect() as conn:
            conn.execute(f"""TRUNCATE auvergne_rhone_alpes_poi.{layer}""")
            conn.execute(
                f"""INSERT INTO auvergne_rhone_alpes_poi.{layer}
                SELECT id, properties::json, ST_SetSRID(ST_MakePoint(x, y), 3857) AS geom
                FROM auvergne_rhone_alpes_poi.{layer}_data"""
            )

            types_installations = "NULL"
            if "type" in params:
                types_installations = f"'{','.join(params['type'])}'"
            conn.execute(
                f"""INSERT INTO auvergne_rhone_alpes_poi.layer 
                (nom, label, couleur, modifiable, theme, type_installation, type_geom, statut, ancrage_icone) 
                VALUES ('{layer}', '{params['nom']}', '#6feb76', true, '{params['theme']}', {types_installations}, 'Point', true, NULL);"""
            )
