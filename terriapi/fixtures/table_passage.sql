--
-- PostgreSQL database dump
--

\c fixture;

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: table_passage; Type: SCHEMA; Schema: -; Owner: terristory
--

CREATE SCHEMA table_passage;


ALTER SCHEMA table_passage OWNER TO terristory;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: passage_2008_2009; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2008_2009 (
    cod2008 character varying,
    cod2009 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2008_2009 OWNER TO terristory;

--
-- Name: passage_2009_2010; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2009_2010 (
    cod2009 character varying,
    cod2010 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2009_2010 OWNER TO terristory;

--
-- Name: passage_2010_2011; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2010_2011 (
    cod2010 character varying,
    cod2011 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2010_2011 OWNER TO terristory;

--
-- Name: passage_2011_2012; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2011_2012 (
    cod2011 character varying,
    cod2012 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2011_2012 OWNER TO terristory;

--
-- Name: passage_2012_2013; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2012_2013 (
    cod2012 character varying,
    cod2013 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2012_2013 OWNER TO terristory;

--
-- Name: passage_2013_2014; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2013_2014 (
    cod2013 character varying,
    cod2014 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2013_2014 OWNER TO terristory;

--
-- Name: passage_2014_2015; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2014_2015 (
    cod2014 character varying,
    cod2015 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2014_2015 OWNER TO terristory;

--
-- Name: passage_2015_2016; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2015_2016 (
    cod2015 character varying,
    cod2016 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2015_2016 OWNER TO terristory;

--
-- Name: passage_2016_2017; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2016_2017 (
    cod2016 character varying,
    cod2017 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2016_2017 OWNER TO terristory;

--
-- Name: passage_2017_2018; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2017_2018 (
    cod2017 character varying,
    cod2018 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2017_2018 OWNER TO terristory;

--
-- Name: passage_2018_2019; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2018_2019 (
    cod2018 character varying,
    cod2019 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2018_2019 OWNER TO terristory;

--
-- Name: passage_2019_2020; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2019_2020 (
    cod2019 character varying,
    cod2020 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2019_2020 OWNER TO terristory;

--
-- Name: passage_2020_2021; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2020_2021 (
    cod2020 character varying,
    cod2021 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2020_2021 OWNER TO terristory;

--
-- Name: passage_2021_2022; Type: TABLE; Schema: table_passage; Owner: terristory
--

CREATE TABLE table_passage.passage_2021_2022 (
    cod2021 character varying,
    cod2022 character varying,
    annee character varying,
    typemodif character varying,
    ratio character varying
);


ALTER TABLE table_passage.passage_2021_2022 OWNER TO terristory;

--
-- PostgreSQL database dump complete
--

