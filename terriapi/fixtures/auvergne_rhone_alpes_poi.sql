--
-- PostgreSQL database dump
--

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

\c fixture;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: auvergne_rhone_alpes_poi; Type: SCHEMA; Schema: -; Owner: terristory
--

CREATE SCHEMA auvergne_rhone_alpes_poi;


ALTER SCHEMA auvergne_rhone_alpes_poi OWNER TO terristory;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: bornes_hydrogene; Type: TABLE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes_poi.bornes_hydrogene (
    id integer NOT NULL,
    properties json,
    geom public.geometry(Point,3857)
);


ALTER TABLE auvergne_rhone_alpes_poi.bornes_hydrogene OWNER TO terristory;

--
-- Name: bornes_hydrogene_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE SEQUENCE auvergne_rhone_alpes_poi.bornes_hydrogene_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes_poi.bornes_hydrogene_id_seq OWNER TO terristory;

--
-- Name: bornes_hydrogene_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER SEQUENCE auvergne_rhone_alpes_poi.bornes_hydrogene_id_seq OWNED BY auvergne_rhone_alpes_poi.bornes_hydrogene.id;


--
-- Name: decheteries; Type: TABLE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes_poi.decheteries (
    id integer NOT NULL,
    properties json,
    geom public.geometry(Point,3857)
);


ALTER TABLE auvergne_rhone_alpes_poi.decheteries OWNER TO terristory;

--
-- Name: decheteries_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE SEQUENCE auvergne_rhone_alpes_poi.decheteries_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes_poi.decheteries_id_seq OWNER TO terristory;

--
-- Name: decheteries_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER SEQUENCE auvergne_rhone_alpes_poi.decheteries_id_seq OWNED BY auvergne_rhone_alpes_poi.decheteries.id;


--
-- Name: historique; Type: TABLE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes_poi.historique (
    id integer NOT NULL,
    layer character varying NOT NULL,
    utilisateur character varying,
    action character varying,
    properties_precedent json,
    properties_courant json,
    geom_precedent public.geometry(Point,3857),
    geom_courant public.geometry(Point,3857),
    mise_a_jour timestamp without time zone
);


ALTER TABLE auvergne_rhone_alpes_poi.historique OWNER TO terristory;

--
-- Name: layer; Type: TABLE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes_poi.layer (
    id integer NOT NULL,
    nom character varying,
    label character varying,
    couleur character varying,
    modifiable boolean,
    rubrique character varying,
    type_installation character varying,
    type_geom character varying,
    statut boolean,
    ancrage_icone character varying
);


ALTER TABLE auvergne_rhone_alpes_poi.layer OWNER TO terristory;

--
-- Name: layer_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE SEQUENCE auvergne_rhone_alpes_poi.layer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes_poi.layer_id_seq OWNER TO terristory;

--
-- Name: layer_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER SEQUENCE auvergne_rhone_alpes_poi.layer_id_seq OWNED BY auvergne_rhone_alpes_poi.layer.id;



--
-- Name: res_lin; Type: TABLE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes_poi.res_lin (
    id integer NOT NULL,
    x double precision,
    y double precision,
    properties json,
    geom public.geometry(MultiLineString,3857)
);


ALTER TABLE auvergne_rhone_alpes_poi.res_lin OWNER TO terristory;

--
-- Name: res_lin_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE SEQUENCE auvergne_rhone_alpes_poi.res_lin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes_poi.res_lin_id_seq OWNER TO terristory;

--
-- Name: res_lin_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER SEQUENCE auvergne_rhone_alpes_poi.res_lin_id_seq OWNED BY auvergne_rhone_alpes_poi.res_lin.id;

--
-- Name: bornes_hydrogene id; Type: DEFAULT; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes_poi.bornes_hydrogene ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes_poi.bornes_hydrogene_id_seq'::regclass);


--
-- Name: decheteries id; Type: DEFAULT; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes_poi.decheteries ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes_poi.decheteries_id_seq'::regclass);


--
-- Name: layer id; Type: DEFAULT; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes_poi.layer ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes_poi.layer_id_seq'::regclass);


--
-- Name: res_lin id; Type: DEFAULT; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes_poi.res_lin ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes_poi.res_lin_id_seq'::regclass);

--
-- Name: bornes_hydrogene_geom_idx; Type: INDEX; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE INDEX bornes_hydrogene_geom_idx ON auvergne_rhone_alpes_poi.bornes_hydrogene USING gist (geom);


--
-- Name: decheteries_geom_idx; Type: INDEX; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE INDEX decheteries_geom_idx ON auvergne_rhone_alpes_poi.decheteries USING gist (geom);

--
-- Name: res_lin_geom_idx; Type: INDEX; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

CREATE INDEX res_lin_geom_idx ON auvergne_rhone_alpes_poi.res_lin USING gist (geom);


--
-- Name: historique historique_utilisateur_fkey; Type: FK CONSTRAINT; Schema: auvergne_rhone_alpes_poi; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes_poi.historique
    ADD CONSTRAINT historique_utilisateur_fkey FOREIGN KEY (utilisateur) REFERENCES public.utilisateur(mail) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- PostgreSQL database dump complete
--

