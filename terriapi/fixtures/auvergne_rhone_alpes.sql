--
-- PostgreSQL database dump
--

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

\c fixture;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: auvergne_rhone_alpes; Type: SCHEMA; Schema: -; Owner: terristory
--

CREATE SCHEMA auvergne_rhone_alpes;


ALTER SCHEMA auvergne_rhone_alpes OWNER TO terristory;

--
-- Name: ajout_territoire(character varying); Type: FUNCTION; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE FUNCTION auvergne_rhone_alpes.ajout_territoire(territoire character varying) RETURNS void
    LANGUAGE plpgsql
    AS $$
begin
    execute format('drop table if exists %s cascade', territoire);
    execute format('
        create table %s (
            id serial primary key
            , code varchar
            , nom varchar
            , communes varchar[]
            , population int
            , population_sinoe int
            , population_1564 float
            , actif_1564 float
            , taux_emploi float
            , emploi_tertiaire float
            , emploi_tout_secteur float
            , superficie float
            , logement float
            , etalement_urbain float
            , taux_etalement_urbain float
            , surface_naturelle float
            , domicile_travail int
            , facture_energetique float
            , x float
            , y float
            , geom geometry(multipolygon, 3857)
        )', territoire);
    execute format('
        insert into %s (nom, code, communes, population, population_sinoe, population_1564, actif_1564, taux_emploi,
                        emploi_tertiaire, emploi_tout_secteur,
                        superficie, logement, etalement_urbain, taux_etalement_urbain, surface_naturelle, domicile_travail, facture_energetique, x, y, geom)
        with tmp as (
        select
            t.nom_territoire as nom
            , t.code_territoire as code
            , array_agg(c.code) as communes
            , sum(population) as population
            , sum(population_sinoe) as population_sinoe
            , sum(population_1564) as population_1564
            , sum(actif_1564) as actif_1564
            , sum(actif_1564) / sum(population_1564) * 100 as taux_emploi
            , sum(emploi_tertiaire) as emploi_tertiaire
            , sum(emploi_tout_secteur) as emploi_tout_secteur
            , sum(superficie) as superficie
            , sum(logement) as logement
            , sum(etalement_urbain) as etalement_urbain
            , sum(etalement_urbain) / sum(population_1564) * 100 as taux_etalement_urbain
            , sum(surface_naturelle) as surface_naturelle
            , sum(domicile_travail) as domicile_travail
            , sum(facture_energetique) as facture_energetique
            , st_multi(st_union(c.geom)) as geom
        from import_auvergne_rhone_alpes.territoire t
        join commune c on c.code = t.code_commune
        where t.type_territoire = ''%s''
        group by t.nom_territoire, t.code_territoire
        ), filter as (
        select
            nom
            , code
            , communes
            , tmp.geom
            , population
            , population_sinoe
            , population_1564
            , actif_1564
            , taux_emploi
            , emploi_tertiaire
            , emploi_tout_secteur
            , superficie
            , logement
            , etalement_urbain
            , taux_etalement_urbain
            , surface_naturelle
            , domicile_travail
            , facture_energetique
            , rank() over (partition by code order by st_area(dump.geom) desc) as rank
            , st_pointonsurface(dump.geom)::geometry(point, 3857) as center
        from tmp,
        st_dump(geom) as dump
        )
        select
            nom
            , code
            , communes
            , population
            , population_sinoe
            , population_1564
            , actif_1564
            , taux_emploi
            , emploi_tertiaire
            , emploi_tout_secteur
            , superficie
            , logement
            , etalement_urbain
            , taux_etalement_urbain
            , surface_naturelle
            , domicile_travail
            , facture_energetique
            , st_x(center) as x
            , st_y(center) as y
            , geom
        from filter
        where rank = 1;
    ', territoire, territoire);
    execute format('create index on %s using gist (geom)', territoire);
    execute format('create index on %s using gin (communes)', territoire);
end
$$;


ALTER FUNCTION auvergne_rhone_alpes.ajout_territoire(territoire character varying) OWNER TO terristory;

--
-- Name: FUNCTION ajout_territoire(territoire character varying); Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: terristory
--

COMMENT ON FUNCTION auvergne_rhone_alpes.ajout_territoire(territoire character varying) IS 'fonction permettant de créer une table d''un type de territoire à partir des communes';


--
-- Name: part_territoire(character varying, character varying); Type: FUNCTION; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE FUNCTION auvergne_rhone_alpes.part_territoire(territoire character varying, territoire_id character varying, OUT a88 integer, OUT part_terr numeric) RETURNS SETOF record
    LANGUAGE sql
    AS $_$
                                with tmp as (
                                    select
                                        sum(nbter.nb_emploi) nbter
                                        , n.a88::int
                                        , a.emploi_interieur_salarie_nb_pers as eisnp
                                        , a.emploi_interieur_total_nb_pers * 1000 as eitnb
                                        , (a.emploi_interieur_salarie_nb_pers / a.emploi_interieur_total_nb_pers) as pse
                                    from auvergne_rhone_alpes.nb_emploi_territoire_view nbter
                                    join strategie_territoire.naf n on n.a88 = nbter.a88
                                    join strategie_territoire.a88 a on a.naf = n.id
                                    where nbter.territoire = $1 and nbter.territoire_id = $2
                                    group by n.a88, a.emploi_interieur_salarie_nb_pers, a.emploi_interieur_total_nb_pers
                                ), sums as (
                                    select
                                        sum(nbter / pse) as nbterpse
                                        , (select sum(emploi_interieur_total_nb_pers) * 1000 from strategie_territoire.a88) as seitnb
                                    from tmp
                                )
                                select
                                    t.a88
                                    , least(round(
                                        ((t.nbter / t.pse) / s.nbterpse / (t.eitnb / s.seitnb))
                                        * power(log(2.0, (1+s.nbterpse/s.seitnb)::numeric), 0.3) -- pourcentage
                                     * 100, 6), 100)
                                from tmp t, sums s
                                order by 1
                            $_$;


ALTER FUNCTION auvergne_rhone_alpes.part_territoire(territoire character varying, territoire_id character varying, OUT a88 integer, OUT part_terr numeric) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;


--
-- Name: cesba_notes; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.cesba_notes (
    type_territoire character varying NOT NULL,
    id_territoire character varying NOT NULL,
    analysis integer NOT NULL,
    value double precision,
    note double precision,
    CONSTRAINT cesba_notes_note_check CHECK ((((note >= (0)::double precision) AND (note <= (5)::double precision)) OR NULL::boolean))
);


ALTER TABLE auvergne_rhone_alpes.cesba_notes OWNER TO terristory;

--
-- Name: TABLE cesba_notes; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: terristory
--

COMMENT ON TABLE auvergne_rhone_alpes.cesba_notes IS 'table listant les valeurs et notes des territoires par indicateur';


--
-- Name: cesba_stats; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.cesba_stats (
    type_territoire character varying NOT NULL,
    analysis integer NOT NULL,
    median double precision,
    median_note double precision,
    vmin double precision,
    vmax double precision,
    CONSTRAINT cesba_stats_median_note_check CHECK ((((median_note >= (0)::double precision) AND (median_note <= (5)::double precision)) OR NULL::boolean))
);


ALTER TABLE auvergne_rhone_alpes.cesba_stats OWNER TO terristory;

--
-- Name: TABLE cesba_stats; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: terristory
--

COMMENT ON TABLE auvergne_rhone_alpes.cesba_stats IS 'statistiques des notes pour chaque couple type de territoire / analyse';


--
-- Name: chiffres_cle; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.chiffres_cle (
    ordre integer,
    nom_analyse character varying,
    donnee character varying,
    nom_table_analyse character varying,
    unite character varying
);


ALTER TABLE auvergne_rhone_alpes.chiffres_cle OWNER TO terristory;

--
-- Name: commune; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.commune (
    nom character varying,
    code character varying NOT NULL,
    population double precision,
    x double precision,
    y double precision,
    geom public.geometry(MultiPolygon,3857),
    siren character varying
);


ALTER TABLE auvergne_rhone_alpes.commune OWNER TO postgres;

--
-- Name: confid_camembert; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.confid_camembert (
    code character varying,
    type_territoire character varying,
    type_analyse character varying,
    type_confid character varying,
    annee integer,
    CONSTRAINT type_confid_chk CHECK (((type_confid)::text = ANY (ARRAY[('A'::character varying)::text, ('B'::character varying)::text, ('C'::character varying)::text, ('D'::character varying)::text])))
);


ALTER TABLE auvergne_rhone_alpes.confid_camembert OWNER TO terristory;

--
-- Name: confid_maille; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.confid_maille (
    code character varying NOT NULL,
    type_territoire character varying,
    type_analyse character varying,
    annee integer,
    secteur character varying,
    energie character varying,
    facture_secteur character varying,
    facture_energie character varying
);


ALTER TABLE auvergne_rhone_alpes.confid_maille OWNER TO terristory;

--
-- Name: conso_energetique; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.conso_energetique (
    secteur bigint,
    energie bigint,
    usage bigint,
    commune text,
    valeur double precision,
    annee bigint
);


ALTER TABLE auvergne_rhone_alpes.conso_energetique OWNER TO postgres;

--
-- Name: data_chaleur; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.data_chaleur (
    id integer NOT NULL,
    station_id integer NOT NULL,
    nom character varying,
    annee integer,
    libelle_mois character varying,
    valeur double precision NOT NULL
);


ALTER TABLE auvergne_rhone_alpes.data_chaleur OWNER TO terristory;

--
-- Name: data_chaleur_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE SEQUENCE auvergne_rhone_alpes.data_chaleur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes.data_chaleur_id_seq OWNER TO terristory;

--
-- Name: data_chaleur_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER SEQUENCE auvergne_rhone_alpes.data_chaleur_id_seq OWNED BY auvergne_rhone_alpes.data_chaleur.id;

--
-- Name: departement; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.departement (
    nom character varying,
    code character varying NOT NULL,
    communes character varying[],
    population double precision,
    x double precision,
    y double precision,
    geom public.geometry(MultiPolygon,3857),
    siren character varying
);


ALTER TABLE auvergne_rhone_alpes.departement OWNER TO postgres;

--
-- Name: emission_ges; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.emission_ges (
    secteur bigint,
    energie bigint,
    usage bigint,
    commune text,
    valeur double precision,
    annee bigint
);


ALTER TABLE auvergne_rhone_alpes.emission_ges OWNER TO postgres;

--
-- Name: energie; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.energie (
    id integer NOT NULL,
    nom character varying,
    couleur character varying,
    ordre integer
);


ALTER TABLE auvergne_rhone_alpes.energie OWNER TO terristory;

--
-- Name: etat_conversion_perimetre; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.etat_conversion_perimetre (
    conversion_reussie boolean
);


ALTER TABLE auvergne_rhone_alpes.etat_conversion_perimetre OWNER TO terristory;

--
-- Name: global_irradiation; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.global_irradiation (
    code character varying,
    irradiation_systfixe double precision,
    irradiation_systsuivi double precision
);


ALTER TABLE auvergne_rhone_alpes.global_irradiation OWNER TO terristory;

--
-- Name: historique_indicateurs; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.historique_indicateurs (
    id integer NOT NULL,
    nom_donnee character varying,
    utilisateur character varying,
    action character varying,
    date_maj timestamp without time zone,
    autres_informations text
);


ALTER TABLE auvergne_rhone_alpes.historique_indicateurs OWNER TO postgres;

--
-- Name: COLUMN historique_indicateurs.nom_donnee; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: postgres
--

COMMENT ON COLUMN auvergne_rhone_alpes.historique_indicateurs.nom_donnee IS 'Nom de la donnée';


--
-- Name: COLUMN historique_indicateurs.utilisateur; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: postgres
--

COMMENT ON COLUMN auvergne_rhone_alpes.historique_indicateurs.utilisateur IS 'Utilisateur';


--
-- Name: COLUMN historique_indicateurs.action; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: postgres
--

COMMENT ON COLUMN auvergne_rhone_alpes.historique_indicateurs.action IS 'Action effectuée';


--
-- Name: COLUMN historique_indicateurs.date_maj; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: postgres
--

COMMENT ON COLUMN auvergne_rhone_alpes.historique_indicateurs.date_maj IS 'Date et heure de l''action';


--
-- Name: COLUMN historique_indicateurs.autres_informations; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: postgres
--

COMMENT ON COLUMN auvergne_rhone_alpes.historique_indicateurs.autres_informations IS 'Autres informations relatives à cette MAJ';


--
-- Name: historique_indicateurs_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE SEQUENCE auvergne_rhone_alpes.historique_indicateurs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes.historique_indicateurs_id_seq OWNER TO postgres;

--
-- Name: historique_indicateurs_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER SEQUENCE auvergne_rhone_alpes.historique_indicateurs_id_seq OWNED BY auvergne_rhone_alpes.historique_indicateurs.id;

--
-- Name: mapping_data_chaleur; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.mapping_data_chaleur (
    station_id integer NOT NULL,
    code_territoire character varying NOT NULL,
    nom_territoire character varying NOT NULL,
    indicateur character varying
);


ALTER TABLE auvergne_rhone_alpes.mapping_data_chaleur OWNER TO terristory;

--
-- Name: COLUMN mapping_data_chaleur.station_id; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes.mapping_data_chaleur.station_id IS 'L''identifiant de la station de mesure';


--
-- Name: COLUMN mapping_data_chaleur.code_territoire; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes.mapping_data_chaleur.code_territoire IS 'Code du territoire';


--
-- Name: COLUMN mapping_data_chaleur.nom_territoire; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes.mapping_data_chaleur.nom_territoire IS 'Nom du territoire ';


--
-- Name: COLUMN mapping_data_chaleur.indicateur; Type: COMMENT; Schema: auvergne_rhone_alpes; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes.mapping_data_chaleur.indicateur IS 'Nom indicateur';


--
-- Name: mediane_niveau_vie; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.mediane_niveau_vie (
    epci text,
    valeur double precision,
    annee bigint
);


ALTER TABLE auvergne_rhone_alpes.mediane_niveau_vie OWNER TO postgres;

--
-- Name: region; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.region (
    nom character varying,
    code character varying NOT NULL,
    communes character varying[],
    population double precision,
    x double precision,
    y double precision,
    geom public.geometry(MultiPolygon,3857),
    siren character varying
);


ALTER TABLE auvergne_rhone_alpes.region OWNER TO postgres;

--
-- Name: teposcv; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.teposcv (
    nom character varying,
    code character varying NOT NULL,
    communes character varying[],
    population double precision,
    x double precision,
    y double precision,
    geom public.geometry(MultiPolygon,3857),
    siren character varying
);


ALTER TABLE auvergne_rhone_alpes.teposcv OWNER TO postgres;


CREATE TABLE IF NOT EXISTS auvergne_rhone_alpes.clap
(
    id integer,
    naf integer,
    commune character varying COLLATE pg_catalog."default",
    nb_etab integer,
    effectif integer
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS auvergne_rhone_alpes.clap
    OWNER to terristory;
-- Index: auvergne_rhone_alpes_clap_idx

CREATE INDEX IF NOT EXISTS auvergne_rhone_alpes_clap_idx
    ON auvergne_rhone_alpes.clap USING btree
    (id ASC NULLS LAST)
    TABLESPACE pg_default;

--
-- Name: nb_emploi_territoire_view; Type: MATERIALIZED VIEW; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE MATERIALIZED VIEW auvergne_rhone_alpes.nb_emploi_territoire_view AS
 SELECT 'commune'::text AS territoire,
    c.commune AS territoire_id,
    n.a88,
    sum(c.effectif) AS nb_emploi
   FROM (auvergne_rhone_alpes.clap c
     JOIN strategie_territoire.naf n ON ((n.id = c.naf)))
  GROUP BY c.commune, n.a88
UNION ALL
 SELECT 'region'::text AS territoire,
    t.code AS territoire_id,
    n.a88,
    sum(c.effectif) AS nb_emploi
   FROM (((auvergne_rhone_alpes.clap c
     JOIN auvergne_rhone_alpes.commune co ON (((co.code)::text = (c.commune)::text)))
     JOIN strategie_territoire.naf n ON ((n.id = c.naf)))
     JOIN auvergne_rhone_alpes.region t ON ((t.communes && ARRAY[co.code])))
  GROUP BY t.code, n.a88
UNION ALL
 SELECT 'departement'::text AS territoire,
    t.code AS territoire_id,
    n.a88,
    sum(c.effectif) AS nb_emploi
   FROM (((auvergne_rhone_alpes.clap c
     JOIN auvergne_rhone_alpes.commune co ON (((co.code)::text = (c.commune)::text)))
     JOIN strategie_territoire.naf n ON ((n.id = c.naf)))
     JOIN auvergne_rhone_alpes.departement t ON ((t.communes && ARRAY[co.code])))
  GROUP BY t.code, n.a88
UNION ALL
 SELECT 'teposcv'::text AS territoire,
    t.code AS territoire_id,
    n.a88,
    sum(c.effectif) AS nb_emploi
   FROM (((auvergne_rhone_alpes.clap c
     JOIN auvergne_rhone_alpes.commune co ON (((co.code)::text = (c.commune)::text)))
     JOIN strategie_territoire.naf n ON ((n.id = c.naf)))
     JOIN auvergne_rhone_alpes.teposcv t ON ((t.communes && ARRAY[co.code])))
  GROUP BY t.code, n.a88
  WITH NO DATA;


ALTER TABLE auvergne_rhone_alpes.nb_emploi_territoire_view OWNER TO postgres;

--
-- Name: population; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.population (
    commune text,
    valeur double precision,
    annee bigint
);


ALTER TABLE auvergne_rhone_alpes.population OWNER TO postgres;

--
-- Name: prod_enr; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.prod_enr (
    commune text,
    type_prod_enr bigint,
    valeur double precision,
    annee bigint
);


ALTER TABLE auvergne_rhone_alpes.prod_enr OWNER TO postgres;

--
-- Name: prod_enr_mapping; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.prod_enr_mapping (
    action character varying,
    cle character varying,
    libelle character varying
);


ALTER TABLE auvergne_rhone_alpes.prod_enr_mapping OWNER TO terristory;

--
-- Name: secteur; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.secteur (
    nom character varying,
    couleur character varying,
    ordre integer,
    id integer NOT NULL
);


ALTER TABLE auvergne_rhone_alpes.secteur OWNER TO terristory;

--
-- Name: secteur_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE SEQUENCE auvergne_rhone_alpes.secteur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes.secteur_id_seq OWNER TO terristory;

--
-- Name: secteur_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER SEQUENCE auvergne_rhone_alpes.secteur_id_seq OWNED BY auvergne_rhone_alpes.secteur.id;


--
-- Name: secteur_mapping; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.secteur_mapping (
    action character varying,
    libelle character varying
);


ALTER TABLE auvergne_rhone_alpes.secteur_mapping OWNER TO terristory;

--
-- Name: taux_pauvrete; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE TABLE auvergne_rhone_alpes.taux_pauvrete (
    epci text,
    valeur double precision,
    annee bigint
);


ALTER TABLE auvergne_rhone_alpes.taux_pauvrete OWNER TO postgres;

--
-- Name: territoire; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.territoire (
    commune character varying,
    code character varying,
    type_territoire character varying,
    nom character varying
);


ALTER TABLE auvergne_rhone_alpes.territoire OWNER TO terristory;

--
-- Name: type_prod_enr; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.type_prod_enr (
    id integer,
    nom character varying,
    couleur character varying,
    ordre integer
);


ALTER TABLE auvergne_rhone_alpes.type_prod_enr OWNER TO terristory;

--
-- Name: usage; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.usage (
    id integer NOT NULL,
    nom character varying,
    couleur character varying,
    ordre integer
);


ALTER TABLE auvergne_rhone_alpes.usage OWNER TO terristory;

--
-- Name: zone; Type: TABLE; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes.zone (
    nom character varying,
    maille character varying,
    libelle character varying
);


ALTER TABLE auvergne_rhone_alpes.zone OWNER TO terristory;

--
-- Name: data_chaleur id; Type: DEFAULT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.data_chaleur ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes.data_chaleur_id_seq'::regclass);

--
-- Name: historique_indicateurs id; Type: DEFAULT; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER TABLE ONLY auvergne_rhone_alpes.historique_indicateurs ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes.historique_indicateurs_id_seq'::regclass);

--
-- Name: secteur id; Type: DEFAULT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.secteur ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes.secteur_id_seq'::regclass);


--
-- Name: cesba_notes cesba_notes_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.cesba_notes
    ADD CONSTRAINT cesba_notes_pkey PRIMARY KEY (type_territoire, id_territoire, analysis);


--
-- Name: cesba_stats cesba_stats_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.cesba_stats
    ADD CONSTRAINT cesba_stats_pkey PRIMARY KEY (type_territoire, analysis);


--
-- Name: commune commune_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER TABLE ONLY auvergne_rhone_alpes.commune
    ADD CONSTRAINT commune_pkey PRIMARY KEY (code);


--
-- Name: data_chaleur data_chaleur_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.data_chaleur
    ADD CONSTRAINT data_chaleur_pkey PRIMARY KEY (id);


--
-- Name: departement departement_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER TABLE ONLY auvergne_rhone_alpes.departement
    ADD CONSTRAINT departement_pkey PRIMARY KEY (code);


--
-- Name: energie energie_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.energie
    ADD CONSTRAINT energie_pkey PRIMARY KEY (id);


--
-- Name: historique_indicateurs historique_indicateurs_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER TABLE ONLY auvergne_rhone_alpes.historique_indicateurs
    ADD CONSTRAINT historique_indicateurs_pkey PRIMARY KEY (id);


--
-- Name: mapping_data_chaleur mapping_data_chaleur_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.mapping_data_chaleur
    ADD CONSTRAINT mapping_data_chaleur_pkey PRIMARY KEY (station_id, code_territoire);



--
-- Name: region region_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER TABLE ONLY auvergne_rhone_alpes.region
    ADD CONSTRAINT region_pkey PRIMARY KEY (code);



--
-- Name: teposcv teposcv_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER TABLE ONLY auvergne_rhone_alpes.teposcv
    ADD CONSTRAINT teposcv_pkey PRIMARY KEY (code);


--
-- Name: confid_maille uniq_confid_maille; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.confid_maille
    ADD CONSTRAINT uniq_confid_maille UNIQUE (code, type_territoire, type_analyse, annee, secteur, energie);


--
-- Name: territoire uniq_territoire; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.territoire
    ADD CONSTRAINT uniq_territoire UNIQUE (commune, code, type_territoire);


--
-- Name: usage usage_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.usage
    ADD CONSTRAINT usage_pkey PRIMARY KEY (id);


--
-- Name: aura_conso_nrj_annee; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE INDEX aura_conso_nrj_annee ON auvergne_rhone_alpes.conso_energetique USING btree (annee);


--
-- Name: aura_conso_nrj_commune; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE INDEX aura_conso_nrj_commune ON auvergne_rhone_alpes.conso_energetique USING btree (commune);


--
-- Name: aura_conso_nrj_energie; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE INDEX aura_conso_nrj_energie ON auvergne_rhone_alpes.conso_energetique USING btree (energie);


--
-- Name: aura_conso_nrj_secteur; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE INDEX aura_conso_nrj_secteur ON auvergne_rhone_alpes.conso_energetique USING btree (secteur);


--
-- Name: aura_conso_nrj_usage; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE INDEX aura_conso_nrj_usage ON auvergne_rhone_alpes.conso_energetique USING btree (usage);

--
-- Name: auvergne_rhone_alpes_confid_camembert_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_confid_camembert_idx ON auvergne_rhone_alpes.confid_camembert USING btree (code);


--
-- Name: auvergne_rhone_alpes_confid_camembert_type_analyse_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_confid_camembert_type_analyse_idx ON auvergne_rhone_alpes.confid_camembert USING btree (type_analyse);


--
-- Name: auvergne_rhone_alpes_confid_camembert_type_confid_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_confid_camembert_type_confid_idx ON auvergne_rhone_alpes.confid_camembert USING btree (type_confid);


--
-- Name: auvergne_rhone_alpes_confid_camembert_type_territoire_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_confid_camembert_type_territoire_idx ON auvergne_rhone_alpes.confid_camembert USING btree (type_territoire);


--
-- Name: auvergne_rhone_alpes_global_irradiation_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_global_irradiation_idx ON auvergne_rhone_alpes.global_irradiation USING btree (code);

--
-- Name: auvergne_rhone_alpes_secteur_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_secteur_idx ON auvergne_rhone_alpes.secteur USING btree (id);

--
-- Name: auvergne_rhone_alpes_type_prod_enr_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_type_prod_enr_idx ON auvergne_rhone_alpes.type_prod_enr USING btree (id);


--
-- Name: auvergne_rhone_alpes_zone_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_zone_idx ON auvergne_rhone_alpes.zone USING btree (nom);


--
-- Name: auvergne_rhone_alpes_zone_maille_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX auvergne_rhone_alpes_zone_maille_idx ON auvergne_rhone_alpes.zone USING btree (maille);


--
-- Name: chiffres_cle_ordre_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX chiffres_cle_ordre_idx ON auvergne_rhone_alpes.chiffres_cle USING btree (ordre);


--
-- Name: confid_camembert_code_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX confid_camembert_code_idx ON auvergne_rhone_alpes.confid_camembert USING btree (code);


--
-- Name: confid_maille_annee_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX confid_maille_annee_idx ON auvergne_rhone_alpes.confid_maille USING btree (annee);


--
-- Name: confid_maille_code_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX confid_maille_code_idx ON auvergne_rhone_alpes.confid_maille USING btree (code);

--
-- Name: nb_emploi_territoire_view_territoire_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: postgres
--

CREATE INDEX nb_emploi_territoire_view_territoire_idx ON auvergne_rhone_alpes.nb_emploi_territoire_view USING btree (territoire);


--
-- Name: territoire_code_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX territoire_code_idx ON auvergne_rhone_alpes.territoire USING btree (code);


--
-- Name: territoire_commune_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX territoire_commune_idx ON auvergne_rhone_alpes.territoire USING btree (commune);


--
-- Name: territoire_type_territoire_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX territoire_type_territoire_idx ON auvergne_rhone_alpes.territoire USING btree (type_territoire);


--
-- Name: usage_id_idx; Type: INDEX; Schema: auvergne_rhone_alpes; Owner: terristory
--

CREATE INDEX usage_id_idx ON auvergne_rhone_alpes.usage USING btree (id);


--
-- Name: cesba_notes cesba_notes_analysis_fkey; Type: FK CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.cesba_notes
    ADD CONSTRAINT cesba_notes_analysis_fkey FOREIGN KEY (analysis) REFERENCES meta.indicateur(id);


--
-- Name: cesba_stats cesba_stats_analysis_fkey; Type: FK CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.cesba_stats
    ADD CONSTRAINT cesba_stats_analysis_fkey FOREIGN KEY (analysis) REFERENCES meta.indicateur(id);


--
-- Name: data_chaleur data_chaleur_station_id_fkey; Type: FK CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.data_chaleur
    ADD CONSTRAINT data_chaleur_station_id_fkey FOREIGN KEY (station_id) REFERENCES auvergne_rhone_alpes_station_mesure.station_mesure(id);


--
-- Name: historique_indicateurs historique_indicateurs_utilisateur_fkey; Type: FK CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: postgres
--

ALTER TABLE ONLY auvergne_rhone_alpes.historique_indicateurs
    ADD CONSTRAINT historique_indicateurs_utilisateur_fkey FOREIGN KEY (utilisateur) REFERENCES public.utilisateur(mail) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: mapping_data_chaleur mapping_data_chaleur_station_id_fkey; Type: FK CONSTRAINT; Schema: auvergne_rhone_alpes; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes.mapping_data_chaleur
    ADD CONSTRAINT mapping_data_chaleur_station_id_fkey FOREIGN KEY (station_id) REFERENCES auvergne_rhone_alpes_station_mesure.station_mesure(id);



CREATE TABLE IF NOT EXISTS auvergne_rhone_alpes.commune_fr
(
    nom character varying COLLATE pg_catalog."default",
    code character varying COLLATE pg_catalog."default" NOT NULL,
    statut character varying COLLATE pg_catalog."default",
    geom public.geometry(MultiPolygon,3857),
    x double precision,
    y double precision
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS auvergne_rhone_alpes.commune_fr
    OWNER to postgres;
-- Index: commune_fr_code_idx

-- DROP INDEX IF EXISTS auvergne_rhone_alpes.commune_fr_code_idx;

CREATE UNIQUE INDEX IF NOT EXISTS commune_fr_code_idx
    ON auvergne_rhone_alpes.commune_fr USING btree
    (code COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: commune_fr_code_idx1

-- DROP INDEX IF EXISTS auvergne_rhone_alpes.commune_fr_code_idx1;

CREATE UNIQUE INDEX IF NOT EXISTS commune_fr_code_idx1
    ON auvergne_rhone_alpes.commune_fr USING btree
    (code COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: commune_fr_code_idx2

-- DROP INDEX IF EXISTS auvergne_rhone_alpes.commune_fr_code_idx2;

CREATE UNIQUE INDEX IF NOT EXISTS commune_fr_code_idx2
    ON auvergne_rhone_alpes.commune_fr USING btree
    (code COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: commune_fr_geom_idx

-- DROP INDEX IF EXISTS auvergne_rhone_alpes.commune_fr_geom_idx;

CREATE INDEX IF NOT EXISTS commune_fr_geom_idx
    ON auvergne_rhone_alpes.commune_fr USING gist
    (geom)
    TABLESPACE pg_default;

--
-- PostgreSQL database dump complete
--

