DROP DATABASE IF EXISTS fixture;
CREATE DATABASE fixture;
ALTER DATABASE fixture OWNER TO terristory;

\c fixture

CREATE EXTENSION postgis;