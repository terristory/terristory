--
-- PostgreSQL database dump
--

\c fixture;

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';

--
-- Name: alembic_version; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.alembic_version (
    version_num character varying(32) NOT NULL
);


ALTER TABLE public.alembic_version OWNER TO terristory;

--
-- Name: pcaet_ademe; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.pcaet (
    siren character varying NOT NULL,
    trajectory character varying NOT NULL,
    category integer NOT NULL,
    year integer NOT NULL,
    value double precision,
    is_ref boolean,
    variation double precision,
);


ALTER TABLE public.pcaet OWNER TO terristory;

--
-- Name: profil_utilisateur; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.profil_utilisateur (
    id integer NOT NULL,
    nom character varying NOT NULL
);


ALTER TABLE public.profil_utilisateur OWNER TO terristory;

--
-- Name: profil_utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: terristory
--

CREATE SEQUENCE public.profil_utilisateur_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profil_utilisateur_id_seq OWNER TO terristory;

--
-- Name: profil_utilisateur_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: terristory
--

ALTER SEQUENCE public.profil_utilisateur_id_seq OWNED BY public.profil_utilisateur.id;


--
-- Name: refresh_token; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.refresh_token (
    mail character varying NOT NULL,
    token character varying NOT NULL,
    creation_date timestamp without time zone DEFAULT now()
);


ALTER TABLE public.refresh_token OWNER TO terristory;

--
-- Name: regions_configuration; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.regions_configuration (
    id character varying NOT NULL,
    env character varying NOT NULL,
    url character varying,
    label character varying,
    theme character varying,
    valeur_defaut_territoire boolean,
    titre_graph_ges character varying,
    ges_export_excel boolean,
    source_suivi_traj character varying,
    cesba_notes character varying,
    cesba_elements character varying,
    lien_a_propos character varying,
    lien_faq character varying,
    map_init_center double precision[],
    map_init_zoom integer,
    map_min_zoom integer,
    map_extent double precision[],
    map_init_extent double precision[],
    footer json,
    ui_show_login boolean,
    ui_show_poi boolean,
    ui_show_analyse boolean,
    ui_show_plan_actions boolean,
    ui_show_tableau_bord boolean,
    splash_screen_custom character varying,
    actu character varying,
    contact_email character varying,
    contact_resp_rgpd character varying,
    ordre_affichage integer,
    ui_show_sankey boolean,
    export_excel_results boolean,
    allow_sharing_dashboard_by_url boolean,
    auto_select_maille boolean,
);


ALTER TABLE public.regions_configuration OWNER TO terristory;

--
-- Name: TABLE regions_configuration; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON TABLE public.regions_configuration IS 'table de configurations pour les régions';


--
-- Name: COLUMN regions_configuration.id; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.id IS 'identifiant de la région';


--
-- Name: COLUMN regions_configuration.env; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.env IS 'environnement: dev, test ou prod';


--
-- Name: COLUMN regions_configuration.url; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.url IS 'url de la région';


--
-- Name: COLUMN regions_configuration.label; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.label IS 'nom de la région';


--
-- Name: COLUMN regions_configuration.theme; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.theme IS 'thème graphique (couleurs) de la région';


--
-- Name: COLUMN regions_configuration.map_init_center; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.map_init_center IS 'centre de la région (carte)';


--
-- Name: COLUMN regions_configuration.map_init_zoom; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.map_init_zoom IS 'zoom initial de la carte';


--
-- Name: COLUMN regions_configuration.map_min_zoom; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.map_min_zoom IS 'zoom minimal autorisé';


--
-- Name: COLUMN regions_configuration.map_extent; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.map_extent IS 'étendue maximale de la région (minx, miny, maxx, maxy)';


--
-- Name: COLUMN regions_configuration.map_init_extent; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.map_init_extent IS 'étendue initiale';


--
-- Name: COLUMN regions_configuration.footer; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.footer IS 'définition du footer de l''application';


--
-- Name: COLUMN regions_configuration.ui_show_login; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.ui_show_login IS 'menu login: oui/non';


--
-- Name: COLUMN regions_configuration.ui_show_poi; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.ui_show_poi IS 'menu poi: oui/non';


--
-- Name: COLUMN regions_configuration.ui_show_analyse; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.ui_show_analyse IS 'menu analyses: oui/non';


--
-- Name: COLUMN regions_configuration.ui_show_plan_actions; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.ui_show_plan_actions IS 'menu plan d''action: oui/non';


--
-- Name: COLUMN regions_configuration.splash_screen_custom; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.splash_screen_custom IS 'splashscreen phrase personnalisée';


--
-- Name: COLUMN regions_configuration.actu; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.actu IS 'splashscreen: phrase d''actualité';


--
-- Name: COLUMN regions_configuration.contact_email; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.contact_email IS 'email de contact';


--
-- Name: COLUMN regions_configuration.contact_resp_rgpd; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.regions_configuration.contact_resp_rgpd IS 'responsable rgpd';

--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.utilisateur (
    prenom character varying NOT NULL,
    nom character varying NOT NULL,
    mail character varying NOT NULL,
    password character varying,
    organisation character varying NOT NULL,
    fonction character varying,
    territoire character varying,
    profil character varying NOT NULL,
    actif boolean NOT NULL,
    connexion integer DEFAULT 0,
    derniere_connexion timestamp without time zone,
    publication boolean DEFAULT false NOT NULL,
    region character varying,
    acces_indicateurs_desactives boolean,
    global_admin boolean DEFAULT false,
    rgpd character varying
);


ALTER TABLE public.utilisateur OWNER TO terristory;

--
-- Name: utilisateur_scenario; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.utilisateur_scenario (
    id integer NOT NULL,
    titre character varying,
    mail character varying NOT NULL,
    description character varying,
    zone_type character varying,
    zone_id character varying,
    actions character varying[],
    derniere_modif timestamp without time zone DEFAULT now(),
    partage text[],
    partage_desactive text[],
    publique boolean DEFAULT false NOT NULL,
    nom_territoire character varying
);


ALTER TABLE public.utilisateur_scenario OWNER TO terristory;

--
-- Name: utilisateur_scenario_id_seq; Type: SEQUENCE; Schema: public; Owner: terristory
--

CREATE SEQUENCE public.utilisateur_scenario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utilisateur_scenario_id_seq OWNER TO terristory;

--
-- Name: utilisateur_scenario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: terristory
--

ALTER SEQUENCE public.utilisateur_scenario_id_seq OWNED BY public.utilisateur_scenario.id;


--
-- Name: utilisateur_scenario_params; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.utilisateur_scenario_params (
    id integer NOT NULL,
    action_param integer NOT NULL,
    scenario integer NOT NULL,
    valeur character varying
);


ALTER TABLE public.utilisateur_scenario_params OWNER TO terristory;

--
-- Name: utilisateur_scenario_params_advanced; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.utilisateur_scenario_params_advanced (
    id integer NOT NULL,
    action character varying NOT NULL,
    scenario integer NOT NULL,
    params_avances jsonb
);


ALTER TABLE public.utilisateur_scenario_params_advanced OWNER TO terristory;

--
-- Name: utilisateur_scenario_params_advanced_id_seq; Type: SEQUENCE; Schema: public; Owner: terristory
--

CREATE SEQUENCE public.utilisateur_scenario_params_advanced_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utilisateur_scenario_params_advanced_id_seq OWNER TO terristory;

--
-- Name: utilisateur_scenario_params_advanced_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: terristory
--

ALTER SEQUENCE public.utilisateur_scenario_params_advanced_id_seq OWNED BY public.utilisateur_scenario_params_advanced.id;


--
-- Name: utilisateur_scenario_params_id_seq; Type: SEQUENCE; Schema: public; Owner: terristory
--

CREATE SEQUENCE public.utilisateur_scenario_params_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utilisateur_scenario_params_id_seq OWNER TO terristory;

--
-- Name: utilisateur_scenario_params_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: terristory
--

ALTER SEQUENCE public.utilisateur_scenario_params_id_seq OWNED BY public.utilisateur_scenario_params.id;


--
-- Name: utilisateur_scenario_trajectoire_cible; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.utilisateur_scenario_trajectoire_cible (
    id integer NOT NULL,
    nom character varying NOT NULL,
    scenario integer NOT NULL,
    valeur jsonb
);


ALTER TABLE public.utilisateur_scenario_trajectoire_cible OWNER TO terristory;

--
-- Name: utilisateur_scenario_trajectoire_cible_id_seq; Type: SEQUENCE; Schema: public; Owner: terristory
--

CREATE SEQUENCE public.utilisateur_scenario_trajectoire_cible_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utilisateur_scenario_trajectoire_cible_id_seq OWNER TO terristory;

--
-- Name: utilisateur_scenario_trajectoire_cible_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: terristory
--

ALTER SEQUENCE public.utilisateur_scenario_trajectoire_cible_id_seq OWNED BY public.utilisateur_scenario_trajectoire_cible.id;


--
-- Name: version; Type: TABLE; Schema: public; Owner: terristory
--

CREATE TABLE public.version (
    numero character varying NOT NULL,
    upgraded timestamp without time zone DEFAULT now(),
    reloaded timestamp without time zone DEFAULT now()
);


ALTER TABLE public.version OWNER TO terristory;

--
-- Name: COLUMN version.numero; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.version.numero IS 'Numéro de version du modèle de donnée';


--
-- Name: COLUMN version.upgraded; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.version.upgraded IS 'Date à laquelle la migration a été effectuée';


--
-- Name: COLUMN version.reloaded; Type: COMMENT; Schema: public; Owner: terristory
--

COMMENT ON COLUMN public.version.reloaded IS 'Dernière date à laquelle le modèle a été rechargé en base';


--
-- Name: profil_utilisateur id; Type: DEFAULT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.profil_utilisateur ALTER COLUMN id SET DEFAULT nextval('public.profil_utilisateur_id_seq'::regclass);


--
-- Name: utilisateur_scenario id; Type: DEFAULT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario ALTER COLUMN id SET DEFAULT nextval('public.utilisateur_scenario_id_seq'::regclass);


--
-- Name: utilisateur_scenario_params id; Type: DEFAULT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_params ALTER COLUMN id SET DEFAULT nextval('public.utilisateur_scenario_params_id_seq'::regclass);


--
-- Name: utilisateur_scenario_params_advanced id; Type: DEFAULT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_params_advanced ALTER COLUMN id SET DEFAULT nextval('public.utilisateur_scenario_params_advanced_id_seq'::regclass);


--
-- Name: utilisateur_scenario_trajectoire_cible id; Type: DEFAULT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_trajectoire_cible ALTER COLUMN id SET DEFAULT nextval('public.utilisateur_scenario_trajectoire_cible_id_seq'::regclass);


--
-- Name: alembic_version alembic_version_pkc; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.alembic_version
    ADD CONSTRAINT alembic_version_pkc PRIMARY KEY (version_num);


--
-- Name: pcaet pcaet_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.pcaet
    ADD CONSTRAINT pcaet_pkey PRIMARY KEY (siren, trajectory, category, year);


--
-- Name: profil_utilisateur profil_utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.profil_utilisateur
    ADD CONSTRAINT profil_utilisateur_pkey PRIMARY KEY (nom);


--
-- Name: refresh_token refresh_token_mail_key; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.refresh_token
    ADD CONSTRAINT refresh_token_mail_key UNIQUE (mail);


--
-- Name: regions_configuration region_conguration_pk; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.regions_configuration
    ADD CONSTRAINT region_conguration_pk PRIMARY KEY (id, env);


--
-- Name: utilisateur utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (mail);


--
-- Name: utilisateur_scenario_params_advanced utilisateur_scenario_params_advanced_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_params_advanced
    ADD CONSTRAINT utilisateur_scenario_params_advanced_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_scenario_params utilisateur_scenario_params_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_params
    ADD CONSTRAINT utilisateur_scenario_params_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_scenario utilisateur_scenario_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario
    ADD CONSTRAINT utilisateur_scenario_pkey PRIMARY KEY (id);


--
-- Name: utilisateur_scenario_trajectoire_cible utilisateur_scenario_trajectoire_cible_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_trajectoire_cible
    ADD CONSTRAINT utilisateur_scenario_trajectoire_cible_pkey PRIMARY KEY (id);


--
-- Name: version version_pkey; Type: CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.version
    ADD CONSTRAINT version_pkey PRIMARY KEY (numero);


--
-- Name: refresh_token refresh_token_mail_fkey; Type: FK CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.refresh_token
    ADD CONSTRAINT refresh_token_mail_fkey FOREIGN KEY (mail) REFERENCES public.utilisateur(mail) ON DELETE CASCADE;


--
-- Name: utilisateur utilisateur_profil_fkey; Type: FK CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT utilisateur_profil_fkey FOREIGN KEY (profil) REFERENCES public.profil_utilisateur(nom);


--
-- Name: utilisateur_scenario utilisateur_scenario_mail_fkey; Type: FK CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario
    ADD CONSTRAINT utilisateur_scenario_mail_fkey FOREIGN KEY (mail) REFERENCES public.utilisateur(mail) ON DELETE CASCADE;


--
-- Name: utilisateur_scenario_params_advanced utilisateur_scenario_params_advanced_scenario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_params_advanced
    ADD CONSTRAINT utilisateur_scenario_params_advanced_scenario_fkey FOREIGN KEY (scenario) REFERENCES public.utilisateur_scenario(id) ON DELETE CASCADE;


--
-- Name: utilisateur_scenario_params utilisateur_scenario_params_scenario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_params
    ADD CONSTRAINT utilisateur_scenario_params_scenario_fkey FOREIGN KEY (scenario) REFERENCES public.utilisateur_scenario(id) ON DELETE CASCADE;


--
-- Name: utilisateur_scenario_trajectoire_cible utilisateur_scenario_trajectoire_cible_scenario_fkey; Type: FK CONSTRAINT; Schema: public; Owner: terristory
--

ALTER TABLE ONLY public.utilisateur_scenario_trajectoire_cible
    ADD CONSTRAINT utilisateur_scenario_trajectoire_cible_scenario_fkey FOREIGN KEY (scenario) REFERENCES public.utilisateur_scenario(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

