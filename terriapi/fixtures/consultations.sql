--
-- PostgreSQL database dump
--

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

\c fixture;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: consultations; Type: SCHEMA; Schema: -; Owner: terristory
--

CREATE SCHEMA consultations;


ALTER SCHEMA consultations OWNER TO terristory;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: actions_cochees; Type: TABLE; Schema: consultations; Owner: terristory
--

CREATE TABLE consultations.actions_cochees (
    id integer NOT NULL,
    region character varying NOT NULL,
    liste_actions character varying[],
    liste_trajectoires_cibles character varying[],
    type_action character varying,
    date timestamp without time zone DEFAULT '2021-02-12 09:43:13.736317'::timestamp without time zone NOT NULL,
    code_territoire character varying,
    type_territoire character varying,
    id_utilisateur integer
);


ALTER TABLE consultations.actions_cochees OWNER TO terristory;

--
-- Name: COLUMN actions_cochees.id; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.actions_cochees.id IS 'Identifiant';


--
-- Name: COLUMN actions_cochees.region; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.actions_cochees.region IS 'Nom de la région';


--
-- Name: COLUMN actions_cochees.liste_actions; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.actions_cochees.liste_actions IS 'Liste des actions cochées avant le lancement du calcul';


--
-- Name: COLUMN actions_cochees.liste_trajectoires_cibles; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.actions_cochees.liste_trajectoires_cibles IS 'Liste des trajectoires cibles activées avant le lancement du calcul';


--
-- Name: COLUMN actions_cochees.type_action; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.actions_cochees.type_action IS 'Lancement d''un calcul ou export ADEME ?';


--
-- Name: COLUMN actions_cochees.date; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.actions_cochees.date IS 'Date de l''activité';


--
-- Name: actions_cochees_id_seq; Type: SEQUENCE; Schema: consultations; Owner: terristory
--

CREATE SEQUENCE consultations.actions_cochees_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultations.actions_cochees_id_seq OWNER TO terristory;

--
-- Name: actions_cochees_id_seq; Type: SEQUENCE OWNED BY; Schema: consultations; Owner: terristory
--

ALTER SEQUENCE consultations.actions_cochees_id_seq OWNED BY consultations.actions_cochees.id;


--
-- Name: analyses_territoriales; Type: TABLE; Schema: consultations; Owner: terristory
--

CREATE TABLE consultations.analyses_territoriales (
    id integer NOT NULL,
    id_utilisateur integer,
    region character varying NOT NULL,
    code_territoire character varying,
    type_territoire character varying,
    page character varying,
    date timestamp without time zone DEFAULT '2021-02-12 14:28:11.726407'::timestamp without time zone NOT NULL
);


ALTER TABLE consultations.analyses_territoriales OWNER TO terristory;

--
-- Name: COLUMN analyses_territoriales.id; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.analyses_territoriales.id IS 'Identifiant';


--
-- Name: COLUMN analyses_territoriales.region; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.analyses_territoriales.region IS 'Nom de la région';


--
-- Name: COLUMN analyses_territoriales.code_territoire; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.analyses_territoriales.code_territoire IS 'Code INSEE du territoire';


--
-- Name: COLUMN analyses_territoriales.type_territoire; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.analyses_territoriales.type_territoire IS 'Type de territoire sélectionné (Région, EPCI etc.)';


--
-- Name: COLUMN analyses_territoriales.page; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.analyses_territoriales.page IS 'Nom de la page à laquelle l''utilisateur.rice accède (suivi des indicateurs territoriaux ou suivi trajectoires)';


--
-- Name: COLUMN analyses_territoriales.date; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.analyses_territoriales.date IS 'Date de l''activité';


--
-- Name: analyses_territoriales_id_seq; Type: SEQUENCE; Schema: consultations; Owner: terristory
--

CREATE SEQUENCE consultations.analyses_territoriales_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultations.analyses_territoriales_id_seq OWNER TO terristory;

--
-- Name: analyses_territoriales_id_seq; Type: SEQUENCE OWNED BY; Schema: consultations; Owner: terristory
--

ALTER SEQUENCE consultations.analyses_territoriales_id_seq OWNED BY consultations.analyses_territoriales.id;


--
-- Name: autres_pages; Type: TABLE; Schema: consultations; Owner: postgres
--

CREATE TABLE consultations.autres_pages (
    id integer NOT NULL,
    id_utilisateur integer,
    region character varying NOT NULL,
    page character varying,
    details character varying,
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE consultations.autres_pages OWNER TO postgres;

--
-- Name: COLUMN autres_pages.id; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.autres_pages.id IS 'Identifiant';


--
-- Name: COLUMN autres_pages.region; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.autres_pages.region IS 'Nom de la région';


--
-- Name: COLUMN autres_pages.page; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.autres_pages.page IS 'Nom de la page à laquelle l''utilisateur.rice accède (suivi des indicateurs territoriaux ou suivi trajectoires)';


--
-- Name: COLUMN autres_pages.details; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.autres_pages.details IS 'Autres informations relatives à la page consultées';


--
-- Name: COLUMN autres_pages.date; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.autres_pages.date IS 'Date de l''activité';


--
-- Name: autres_pages_id_seq; Type: SEQUENCE; Schema: consultations; Owner: postgres
--

CREATE SEQUENCE consultations.autres_pages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultations.autres_pages_id_seq OWNER TO postgres;

--
-- Name: autres_pages_id_seq; Type: SEQUENCE OWNED BY; Schema: consultations; Owner: postgres
--

ALTER SEQUENCE consultations.autres_pages_id_seq OWNED BY consultations.autres_pages.id;


--
-- Name: consultations_indicateurs; Type: TABLE; Schema: consultations; Owner: terristory
--

CREATE TABLE consultations.consultations_indicateurs (
    id integer NOT NULL,
    provenance character varying NOT NULL,
    region character varying NOT NULL,
    id_indicateur integer,
    date timestamp without time zone DEFAULT '2021-02-12 09:43:13.736317'::timestamp without time zone NOT NULL,
    code_territoire character varying,
    type_territoire character varying,
    id_utilisateur integer
);


ALTER TABLE consultations.consultations_indicateurs OWNER TO terristory;

--
-- Name: COLUMN consultations_indicateurs.id; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.consultations_indicateurs.id IS 'Identifiant';


--
-- Name: COLUMN consultations_indicateurs.provenance; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.consultations_indicateurs.provenance IS 'Page depuis laquelle l''url a été lancée';


--
-- Name: COLUMN consultations_indicateurs.region; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.consultations_indicateurs.region IS 'Nom de la région';


--
-- Name: COLUMN consultations_indicateurs.id_indicateur; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.consultations_indicateurs.id_indicateur IS 'identifiant de l''indicateur';


--
-- Name: COLUMN consultations_indicateurs.date; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.consultations_indicateurs.date IS 'Date de l''activité';


--
-- Name: consultations_indicateurs_id_seq; Type: SEQUENCE; Schema: consultations; Owner: terristory
--

CREATE SEQUENCE consultations.consultations_indicateurs_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultations.consultations_indicateurs_id_seq OWNER TO terristory;

--
-- Name: consultations_indicateurs_id_seq; Type: SEQUENCE OWNED BY; Schema: consultations; Owner: terristory
--

ALTER SEQUENCE consultations.consultations_indicateurs_id_seq OWNED BY consultations.consultations_indicateurs.id;


--
-- Name: ip_localisation; Type: TABLE; Schema: consultations; Owner: terristory
--

CREATE TABLE consultations.ip_localisation (
    id integer NOT NULL,
    ip character varying,
    commune character varying,
    region_selectionnee character varying
);


ALTER TABLE consultations.ip_localisation OWNER TO terristory;

--
-- Name: COLUMN ip_localisation.id; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.ip_localisation.id IS 'Identifiant';


--
-- Name: COLUMN ip_localisation.ip; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.ip_localisation.ip IS 'Adresse IP de l''utilisateur.rice';


--
-- Name: COLUMN ip_localisation.commune; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.ip_localisation.commune IS 'Nom de la commune depuis laquelle la personne utilise l''application';


--
-- Name: ip_localisation_id_seq; Type: SEQUENCE; Schema: consultations; Owner: terristory
--

CREATE SEQUENCE consultations.ip_localisation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultations.ip_localisation_id_seq OWNER TO terristory;

--
-- Name: ip_localisation_id_seq; Type: SEQUENCE OWNED BY; Schema: consultations; Owner: terristory
--

ALTER SEQUENCE consultations.ip_localisation_id_seq OWNED BY consultations.ip_localisation.id;


--
-- Name: poi; Type: TABLE; Schema: consultations; Owner: terristory
--

CREATE TABLE consultations.poi (
    id integer NOT NULL,
    id_utilisateur integer,
    region character varying NOT NULL,
    code_territoire character varying,
    type_territoire character varying,
    nom_couche character varying,
    cochee boolean,
    date timestamp without time zone DEFAULT '2021-02-12 14:28:11.726407'::timestamp without time zone NOT NULL
);


ALTER TABLE consultations.poi OWNER TO terristory;

--
-- Name: COLUMN poi.id; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.poi.id IS 'Identifiant';


--
-- Name: COLUMN poi.region; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.poi.region IS 'Nom de la région';


--
-- Name: COLUMN poi.code_territoire; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.poi.code_territoire IS 'Code INSEE du territoire';


--
-- Name: COLUMN poi.type_territoire; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.poi.type_territoire IS 'Type de territoire sélectionné (Région, EPCI etc.)';


--
-- Name: COLUMN poi.nom_couche; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.poi.nom_couche IS 'Nom de la couche POI';


--
-- Name: COLUMN poi.cochee; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.poi.cochee IS 'La personne a-t-elle activé ou désactivé la couche ?';


--
-- Name: COLUMN poi.date; Type: COMMENT; Schema: consultations; Owner: terristory
--

COMMENT ON COLUMN consultations.poi.date IS 'Date de l''activité';


--
-- Name: poi_id_seq; Type: SEQUENCE; Schema: consultations; Owner: terristory
--

CREATE SEQUENCE consultations.poi_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultations.poi_id_seq OWNER TO terristory;

--
-- Name: poi_id_seq; Type: SEQUENCE OWNED BY; Schema: consultations; Owner: terristory
--

ALTER SEQUENCE consultations.poi_id_seq OWNED BY consultations.poi.id;


--
-- Name: tableaux_bords; Type: TABLE; Schema: consultations; Owner: postgres
--

CREATE TABLE consultations.tableaux_bords (
    id integer NOT NULL,
    id_utilisateur integer,
    region character varying NOT NULL,
    code_territoire character varying,
    type_territoire character varying,
    tableau_bord_id integer,
    tableau_bord_nom character varying,
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE consultations.tableaux_bords OWNER TO postgres;

--
-- Name: COLUMN tableaux_bords.id; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.tableaux_bords.id IS 'Identifiant';


--
-- Name: COLUMN tableaux_bords.region; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.tableaux_bords.region IS 'Nom de la région';


--
-- Name: COLUMN tableaux_bords.code_territoire; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.tableaux_bords.code_territoire IS 'Code INSEE du territoire';


--
-- Name: COLUMN tableaux_bords.type_territoire; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.tableaux_bords.type_territoire IS 'Type de territoire sélectionné (Région, EPCI etc.)';


--
-- Name: COLUMN tableaux_bords.tableau_bord_id; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.tableaux_bords.tableau_bord_id IS 'ID du tableau de bord';


--
-- Name: COLUMN tableaux_bords.tableau_bord_nom; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.tableaux_bords.tableau_bord_nom IS 'Nom du tableau de bord';


--
-- Name: COLUMN tableaux_bords.date; Type: COMMENT; Schema: consultations; Owner: postgres
--

COMMENT ON COLUMN consultations.tableaux_bords.date IS 'Date de l''activité';


--
-- Name: tableaux_bords_id_seq; Type: SEQUENCE; Schema: consultations; Owner: postgres
--

CREATE SEQUENCE consultations.tableaux_bords_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE consultations.tableaux_bords_id_seq OWNER TO postgres;

--
-- Name: tableaux_bords_id_seq; Type: SEQUENCE OWNED BY; Schema: consultations; Owner: postgres
--

ALTER SEQUENCE consultations.tableaux_bords_id_seq OWNED BY consultations.tableaux_bords.id;


--
-- Name: actions_cochees id; Type: DEFAULT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.actions_cochees ALTER COLUMN id SET DEFAULT nextval('consultations.actions_cochees_id_seq'::regclass);


--
-- Name: analyses_territoriales id; Type: DEFAULT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.analyses_territoriales ALTER COLUMN id SET DEFAULT nextval('consultations.analyses_territoriales_id_seq'::regclass);


--
-- Name: autres_pages id; Type: DEFAULT; Schema: consultations; Owner: postgres
--

ALTER TABLE ONLY consultations.autres_pages ALTER COLUMN id SET DEFAULT nextval('consultations.autres_pages_id_seq'::regclass);


--
-- Name: consultations_indicateurs id; Type: DEFAULT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.consultations_indicateurs ALTER COLUMN id SET DEFAULT nextval('consultations.consultations_indicateurs_id_seq'::regclass);


--
-- Name: ip_localisation id; Type: DEFAULT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.ip_localisation ALTER COLUMN id SET DEFAULT nextval('consultations.ip_localisation_id_seq'::regclass);


--
-- Name: poi id; Type: DEFAULT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.poi ALTER COLUMN id SET DEFAULT nextval('consultations.poi_id_seq'::regclass);


--
-- Name: tableaux_bords id; Type: DEFAULT; Schema: consultations; Owner: postgres
--

ALTER TABLE ONLY consultations.tableaux_bords ALTER COLUMN id SET DEFAULT nextval('consultations.tableaux_bords_id_seq'::regclass);


--
-- Name: actions_cochees actions_cochees_pkey; Type: CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.actions_cochees
    ADD CONSTRAINT actions_cochees_pkey PRIMARY KEY (id);


--
-- Name: analyses_territoriales analyses_territoriales_pkey; Type: CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.analyses_territoriales
    ADD CONSTRAINT analyses_territoriales_pkey PRIMARY KEY (id);


--
-- Name: autres_pages autres_pages_pkey; Type: CONSTRAINT; Schema: consultations; Owner: postgres
--

ALTER TABLE ONLY consultations.autres_pages
    ADD CONSTRAINT autres_pages_pkey PRIMARY KEY (id);


--
-- Name: consultations_indicateurs consultations_indicateurs_pkey; Type: CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.consultations_indicateurs
    ADD CONSTRAINT consultations_indicateurs_pkey PRIMARY KEY (id);


--
-- Name: ip_localisation ip_localisation_pkey; Type: CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.ip_localisation
    ADD CONSTRAINT ip_localisation_pkey PRIMARY KEY (id);


--
-- Name: poi poi_pkey; Type: CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.poi
    ADD CONSTRAINT poi_pkey PRIMARY KEY (id);


--
-- Name: tableaux_bords tableaux_bords_pkey; Type: CONSTRAINT; Schema: consultations; Owner: postgres
--

ALTER TABLE ONLY consultations.tableaux_bords
    ADD CONSTRAINT tableaux_bords_pkey PRIMARY KEY (id);


--
-- Name: actions_cochees actions_cochees_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.actions_cochees
    ADD CONSTRAINT actions_cochees_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES consultations.ip_localisation(id) ON DELETE CASCADE;


--
-- Name: analyses_territoriales analyses_territoriales_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.analyses_territoriales
    ADD CONSTRAINT analyses_territoriales_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES consultations.ip_localisation(id) ON DELETE CASCADE;


--
-- Name: autres_pages autres_pages_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: consultations; Owner: postgres
--

ALTER TABLE ONLY consultations.autres_pages
    ADD CONSTRAINT autres_pages_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES consultations.ip_localisation(id) ON DELETE CASCADE;


--
-- Name: consultations_indicateurs consultations_indicateurs_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.consultations_indicateurs
    ADD CONSTRAINT consultations_indicateurs_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES consultations.ip_localisation(id) ON DELETE CASCADE;


--
-- Name: poi poi_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: consultations; Owner: terristory
--

ALTER TABLE ONLY consultations.poi
    ADD CONSTRAINT poi_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES consultations.ip_localisation(id) ON DELETE CASCADE;


--
-- Name: tableaux_bords tableaux_bords_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: consultations; Owner: postgres
--

ALTER TABLE ONLY consultations.tableaux_bords
    ADD CONSTRAINT tableaux_bords_id_utilisateur_fkey FOREIGN KEY (id_utilisateur) REFERENCES consultations.ip_localisation(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

