--
-- PostgreSQL database dump
--

\c fixture

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

--
-- Name: auvergne_rhone_alpes_station_mesure; Type: SCHEMA; Schema: -; Owner: terristory
--

CREATE SCHEMA auvergne_rhone_alpes_station_mesure;


ALTER SCHEMA auvergne_rhone_alpes_station_mesure OWNER TO terristory;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: station_mesure; Type: TABLE; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

CREATE TABLE auvergne_rhone_alpes_station_mesure.station_mesure (
    id integer NOT NULL,
    code character varying NOT NULL,
    nom character varying NOT NULL,
    si_homogeneiseex boolean,
    data_chaleur boolean,
    data_temperature boolean,
    data_gel boolean,
    data_enneigement boolean,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    altitude double precision NOT NULL,
    departement integer,
    si_homogeneisee boolean
);


ALTER TABLE auvergne_rhone_alpes_station_mesure.station_mesure OWNER TO terristory;

--
-- Name: COLUMN station_mesure.id; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.id IS 'l''identifiant de la station de mesure';


--
-- Name: COLUMN station_mesure.code; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.code IS 'Code station de mesure';


--
-- Name: COLUMN station_mesure.nom; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.nom IS 'Nom de la station de mesure';


--
-- Name: COLUMN station_mesure.data_chaleur; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.data_chaleur IS 'Si la station fournit des données chaleur';


--
-- Name: COLUMN station_mesure.data_temperature; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.data_temperature IS 'Si la station fournit des données température';


--
-- Name: COLUMN station_mesure.data_gel; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.data_gel IS 'Si la station fournit des données gel';


--
-- Name: COLUMN station_mesure.data_enneigement; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.data_enneigement IS 'Si la station fournit des données enneigement';


--
-- Name: COLUMN station_mesure.latitude; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.latitude IS 'Coordonnée latitude de la station de mesure';


--
-- Name: COLUMN station_mesure.longitude; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.longitude IS 'Coordonnée longitude de la station de mesure';


--
-- Name: COLUMN station_mesure.altitude; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.altitude IS 'l''altitude de la station de meteo exprimé en mètre';


--
-- Name: COLUMN station_mesure.departement; Type: COMMENT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

COMMENT ON COLUMN auvergne_rhone_alpes_station_mesure.station_mesure.departement IS 'Code département';


--
-- Name: station_mesure_id_seq; Type: SEQUENCE; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

CREATE SEQUENCE auvergne_rhone_alpes_station_mesure.station_mesure_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auvergne_rhone_alpes_station_mesure.station_mesure_id_seq OWNER TO terristory;

--
-- Name: station_mesure_id_seq; Type: SEQUENCE OWNED BY; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

ALTER SEQUENCE auvergne_rhone_alpes_station_mesure.station_mesure_id_seq OWNED BY auvergne_rhone_alpes_station_mesure.station_mesure.id;


--
-- Name: station_mesure id; Type: DEFAULT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes_station_mesure.station_mesure ALTER COLUMN id SET DEFAULT nextval('auvergne_rhone_alpes_station_mesure.station_mesure_id_seq'::regclass);


--
-- Name: station_mesure station_mesure_pkey; Type: CONSTRAINT; Schema: auvergne_rhone_alpes_station_mesure; Owner: terristory
--

ALTER TABLE ONLY auvergne_rhone_alpes_station_mesure.station_mesure
    ADD CONSTRAINT station_mesure_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

