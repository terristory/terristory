insert into regions_configuration (id, env, url, label, theme, valeur_defaut_territoire, titre_graph_ges, ges_export_excel, source_suivi_traj, cesba_notes, cesba_elements, lien_a_propos, lien_faq, map_init_center, map_init_zoom, map_min_zoom, map_extent, map_init_extent, footer, ui_show_login, ui_show_poi, ui_show_analyse, ui_show_plan_actions, ui_show_tableau_bord, splash_screen_custom, actu, contact_email,contact_resp_rgpd, ordre_affichage)
select  'auvergne-rhone-alpes',
        'dev',
        'auvergnerhonealpes',
        'Auvergne-Rhône-Alpes',
        'vert-aura-ee',
        false,
        'émissions GES',
        true,
        '{
        "source": "Source de l''historique : ORCAE 2020",
        "source_impacts": "Historique",
        "position_annotation": -50,
        "titre_graphique": "Données corrigées des variations climatiques en GWh"
        }',
        '<div id="part1">
            <p>
            Les indicateurs regroupés sur cette page ont été calculés dans le cadre du projet européen CESBA_Alps, pour les territoires d’Auvergne-Rhône-Alpes. Ces indicateurs donnent une vue d’ensemble des atouts et des faiblesses du territoire dans le domaine de l’énergie et de l’environnement.
            La valeur normalisée (comprise entre 0 et 5) renseigne sur la performance du territoire pour chacun des indicateurs. Une valeur proche de 5 signifie que le territoire se rapproche de ce qui est souhaitable*.
            </p>
            <ul>
                <li> Si l''histogramme du territoire est <span className="notes-green">vert</span>, cela signifie pour cet indicateur que le territoire est plus performant ou égal à la médiane des territoires de même type (epci, TEPOS,…)</li>
                <li>Si l''histogramme du territoire est <span className="notes-orange">orange</span>, cela signifie que le territoire est moins performant pour cet indicateur que la moyenne des territoires de même type</li>
            </ul>
            <i className=notes-infos>*la situation souhaitable est bien sûr relative. Ici nous avons pour chaque indicateur défini ce qui était souhaitable (stocker beaucoup de carbone, émettre peu de GES...) puis nous avons pris comme référence de calcul pour un indicateur donné ce qui se fait de mieux sur la région au sein des territoires de même type</i>
        </div>',
        '{
        "cesba-logo": [
                        {
                        "logo-entete": "<img src=./img/logo_CESBA-Alps_landscape.png alt=\"CESBA\" />",
                        "logo-pied": "<span class=\"centered-logos\"><img src=./img/logo_aura.png alt=\"aura\" /></tab><img src=./img/logo_ue.png alt=\"europe\" /></span>"
                        }
                      ],
        "titre": "Passeport CESBA_Alps",
        "pied": "Le projet CESBA_Alps est soutenu financièrement par l''Union européenne dans le cadre du programme Interreg Espace Alpin et du Fonds Européen de Développement régional (FEDER). Les avis exprimés n’engagent que les auteurs et ne sauraient être considérés comme constituant une prise de position officielle de la Commission Européenne. Ni la Commission Européenne ni aucune personne agissant au nom de la Commission n’est responsable de l’usage qui pourrait être fait des informations données ici."
        }',
        'a_propos',
        'faq.html',
        '{4.53607827974281, 45.5146709483912}',
        8,
        7,
        '{-364451.750864, 5146963.736611,1401549.350637, 6235427.019392}',
        '{-364451.750864, 5146963.736611,1401549.350637, 6235427.019392}',
        '{
            "left": [
                {
                "title": "Diffusé par :",
                "logos": [
                    {
                    "img": "logo_aura_ee.png",
                    "title": "Auvergne-Rhône-Alpes Energie Environnement"
                    }
                ]
                }
            ],
            "right" : [
                {
                "title": "Avec le soutien de :",
                "logos": [
                    {
                    "img": "logo_aura.png",
                    "title": "La région Auvergne-Rhône-Alpes"
                    },
                    {
                    "img": "logo_ademe.png",
                    "title": "Ademe"
                    },
                    {
                    "img": "logo_ue.png",
                    "title": "Union europeenne"
                    }
                ]
                }
            ]
            }',
        true,
        true,
        true,
        true,
        true,
        ', proposé par l’Agence régionale Auvergne-Rhône-Alpes Énergie-Environnement',
        '',
        'terristory@auvergnerhonealpes-ee.fr',
        'AURA-EE',
        1;


-- INSERT INTO auvergne_rhone_alpes.zone (nom, maille, libelle) VALUES ('region', 'epci', 'Région - maille EPCI');
INSERT INTO auvergne_rhone_alpes.zone (nom, maille, libelle) VALUES ('region', 'departement', 'Région - maille département');
INSERT INTO auvergne_rhone_alpes.zone (nom, maille, libelle) VALUES ('departement', 'commune', 'Département - maille commune');
-- -- INSERT INTO auvergne_rhone_alpes.zone (nom, maille, libelle) VALUES ('epci', 'commune', 'EPCI - maille commune');
-- -- INSERT INTO auvergne_rhone_alpes.zone (nom, maille, libelle) VALUES ('departement', 'epci', 'Département - maille EPCI');


INSERT INTO public.profil_utilisateur (id, nom) VALUES (1, 'admin');
INSERT INTO public.profil_utilisateur (id, nom) VALUES (2, 'utilisateur');

