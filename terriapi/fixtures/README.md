# Pour importer des données d'essai

Pour importer les données, se placer dans ce dossier et dans l'environnement virtuel python où sont installées les commandes TerriSTORY :

```bash
psql -h localhost -U postgres < init.sql
psql -d fixture -h localhost -U postgres < public.sql
psql -d fixture -h localhost -U postgres < meta.sql
psql -d fixture -h localhost -U postgres < table_passage.sql
psql -d fixture -h localhost -U postgres < consultations.sql
psql -d fixture -h localhost -U postgres < strategie_territoire.sql
psql -d fixture -h localhost -U postgres < auvergne_rhone_alpes_station_mesure.sql
psql -d fixture -h localhost -U postgres < auvergne_rhone_alpes.sql
psql -d fixture -h localhost -U postgres < auvergne_rhone_alpes_poi.sql
psql -d fixture -h localhost -U postgres < static_fixtures.sql
psql -d fixture -h localhost -U postgres < boundaries_cities.sql
```

Puis on ajoute les valeurs aléatoires (attention, il faut se placer dans l'environnement virtuel permettant de lancer TerriSTORY)

```bash
python import_fixtures.py
```

# Pour vérifier / tester

Pour utiliser la base ainsi générée, il faut utiliser la base de données `fixture` à la place de celle `api`. Pour cela, il faut modifier le fichier `terriapi.ini` ainsi que la commande lancée pour fournir les couches géographiques (à base de docker), par exemple :

`docker run --net host oslandia/postile postile --pguser postgres --pgpassword postgres --pgdatabase fixture --pghost localhost --pgport 5432 --listen-port 8081 --cors`