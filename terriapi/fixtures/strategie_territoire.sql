--
-- PostgreSQL database dump
--

\c fixture;

-- Dumped from database version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.12 (Ubuntu 12.12-0ubuntu0.20.04.1)

--
-- Name: strategie_territoire; Type: SCHEMA; Schema: -; Owner: terristory
--

CREATE SCHEMA strategie_territoire;


ALTER SCHEMA strategie_territoire OWNER TO terristory;

--
-- Name: nows(character varying); Type: FUNCTION; Schema: strategie_territoire; Owner: terristory
--

CREATE FUNCTION strategie_territoire.nows(field character varying) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
begin
    return regexp_replace(regexp_replace(field, '^\s+', ''), '\s+$', '');
end
$_$;


ALTER FUNCTION strategie_territoire.nows(field character varying) OWNER TO terristory;

--
-- Name: tonumeric(character varying); Type: FUNCTION; Schema: strategie_territoire; Owner: terristory
--

CREATE FUNCTION strategie_territoire.tonumeric(field character varying) RETURNS numeric
    LANGUAGE plpgsql
    AS $$
begin
    return replace(replace(field, ',', '.'), ' ', '')::numeric;
end
$$;


ALTER FUNCTION strategie_territoire.tonumeric(field character varying) OWNER TO terristory;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: naf; Type: TABLE; Schema: strategie_territoire; Owner: terristory
--

CREATE TABLE strategie_territoire.naf (
    id integer,
    cpa character varying,
    a88 integer,
    a732 character varying,
    libelle character varying
);


ALTER TABLE strategie_territoire.naf OWNER TO terristory;

--
-- Name: a732; Type: TABLE; Schema: strategie_territoire; Owner: terristory
--

CREATE TABLE strategie_territoire.a732 (
    id integer,
    naf integer NOT NULL,
    valeur_ajoutee numeric,
    production_totale numeric,
    marge_commerciale numeric,
    impots_taxes numeric,
    salaires_traitements numeric,
    charges_patronales numeric,
    subventions_exploitations numeric,
    excedent_brut_exploitation numeric,
    effectifs_salaries_etp numeric
);


ALTER TABLE strategie_territoire.a732 OWNER TO terristory;

--
-- Name: a88; Type: TABLE; Schema: strategie_territoire; Owner: terristory
--

CREATE TABLE strategie_territoire.a88 (
    id integer,
    naf integer NOT NULL,
    prod_branche_prix_courants numeric,
    conso_intermediaire_prix_courants numeric,
    remuneration_salaire_prix_courants numeric,
    emploi_interieur_total_etp numeric,
    emploi_interieur_salarie_etp numeric,
    emploi_interieur_total_nb_pers numeric,
    emploi_interieur_salarie_nb_pers numeric
);


ALTER TABLE strategie_territoire.a88 OWNER TO terristory;

--
-- Name: action; Type: TABLE; Schema: strategie_territoire; Owner: terristory
--

CREATE TABLE strategie_territoire.action (
    id integer,
    naf integer,
    numero character varying,
    type character varying,
    type_hyp character varying,
    parametres character varying,
    phase_projet character varying,
    maillon character varying,
    valeur character varying,
    cout_global numeric,
    unite character varying,
    part_francaise numeric,
    id_source character varying,
    remuneration integer,
    categorie character varying,
    category_class character varying,
    action_eco boolean,
    category_order integer,
    action_order integer,
    regions character varying[]
);


ALTER TABLE strategie_territoire.action OWNER TO terristory;

--
-- Name: action_params; Type: TABLE; Schema: strategie_territoire; Owner: terristory
--

CREATE TABLE strategie_territoire.action_params (
    id integer NOT NULL,
    action character varying,
    nom character varying,
    label character varying,
    unite character varying,
    choices character varying[],
    ordre integer
);


ALTER TABLE strategie_territoire.action_params OWNER TO terristory;

--
-- Name: action_params_id_seq; Type: SEQUENCE; Schema: strategie_territoire; Owner: terristory
--

CREATE SEQUENCE strategie_territoire.action_params_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE strategie_territoire.action_params_id_seq OWNER TO terristory;

--
-- Name: action_params_id_seq; Type: SEQUENCE OWNED BY; Schema: strategie_territoire; Owner: terristory
--

ALTER SEQUENCE strategie_territoire.action_params_id_seq OWNED BY strategie_territoire.action_params.id;


--
-- Name: ci_prod; Type: MATERIALIZED VIEW; Schema: strategie_territoire; Owner: terristory
--

CREATE MATERIALIZED VIEW strategie_territoire.ci_prod AS
 WITH first AS (
         SELECT n.a732,
            ((1)::numeric - (a.valeur_ajoutee / (a.production_totale + a.marge_commerciale))) AS part_ci_prod,
            'a732'::text AS type
           FROM (strategie_territoire.a732 a
             JOIN strategie_territoire.naf n ON ((n.id = a.naf)))
          WHERE ((a.valeur_ajoutee IS NOT NULL) AND (a.production_totale IS NOT NULL) AND (a.marge_commerciale IS NOT NULL))
        )
 SELECT n.a732,
    (a.conso_intermediaire_prix_courants / a.prod_branche_prix_courants) AS part_ci_prod,
    'a88'::text AS type
   FROM ((strategie_territoire.naf n
     JOIN strategie_territoire.a88 a ON ((a.id = n.a88)))
     LEFT JOIN first f ON (((n.a732)::text = (f.a732)::text)))
  WHERE ((n.a732 IS NOT NULL) AND (f.a732 IS NULL))
UNION ALL
 SELECT first.a732,
    first.part_ci_prod,
    first.type
   FROM first
  WITH NO DATA;


ALTER TABLE strategie_territoire.ci_prod OWNER TO terristory;

--
-- Name: effectif_cpa_france; Type: MATERIALIZED VIEW; Schema: strategie_territoire; Owner: terristory
--

CREATE MATERIALIZED VIEW strategie_territoire.effectif_cpa_france AS
 SELECT n.cpa,
    (sum(a.emploi_interieur_total_nb_pers) * (1000)::numeric) AS effectif_cpa
   FROM (strategie_territoire.a88 a
     JOIN strategie_territoire.naf n ON ((n.id = a.naf)))
  GROUP BY n.cpa
  WITH NO DATA;


ALTER TABLE strategie_territoire.effectif_cpa_france OWNER TO terristory;

--
-- Name: emploi_va; Type: MATERIALIZED VIEW; Schema: strategie_territoire; Owner: terristory
--

CREATE MATERIALIZED VIEW strategie_territoire.emploi_va AS
 WITH first AS (
         SELECT n.a732,
            ((a.effectifs_salaries_etp / (b.emploi_interieur_salarie_etp / b.emploi_interieur_total_etp)) / a.valeur_ajoutee) AS ratio_emploi_va
           FROM ((strategie_territoire.a732 a
             JOIN strategie_territoire.naf n ON ((n.id = a.naf)))
             JOIN strategie_territoire.a88 b ON ((b.id = n.a88)))
          WHERE ((a.valeur_ajoutee IS NOT NULL) AND (a.valeur_ajoutee <> (0)::numeric) AND (a.effectifs_salaries_etp IS NOT NULL) AND (b.emploi_interieur_salarie_etp IS NOT NULL) AND (b.emploi_interieur_total_etp IS NOT NULL))
        )
 SELECT n.a732,
    (a.emploi_interieur_total_etp / (a.prod_branche_prix_courants - a.conso_intermediaire_prix_courants)) AS ratio_emploi_va
   FROM ((strategie_territoire.naf n
     JOIN strategie_territoire.a88 a ON ((a.id = n.a88)))
     LEFT JOIN first f ON (((f.a732)::text = (n.a732)::text)))
  WHERE ((n.a732 IS NOT NULL) AND (f.a732 IS NULL))
UNION ALL
 SELECT first.a732,
    first.ratio_emploi_va
   FROM first
  WITH NO DATA;


ALTER TABLE strategie_territoire.emploi_va OWNER TO terristory;

--
-- Name: emploi_va_cpa; Type: MATERIALIZED VIEW; Schema: strategie_territoire; Owner: terristory
--

CREATE MATERIALIZED VIEW strategie_territoire.emploi_va_cpa AS
 SELECT n.cpa,
    (sum(a.emploi_interieur_total_etp) / (sum(a.prod_branche_prix_courants) - sum(a.conso_intermediaire_prix_courants))) AS ratio_emploi_va
   FROM (strategie_territoire.naf n
     JOIN strategie_territoire.a88 a ON ((a.id = n.a88)))
  WHERE (n.a732 IS NULL)
  GROUP BY n.cpa
  ORDER BY n.cpa
  WITH NO DATA;


ALTER TABLE strategie_territoire.emploi_va_cpa OWNER TO terristory;

--
-- Name: meta_action; Type: TABLE; Schema: strategie_territoire; Owner: terristory
--

CREATE TABLE strategie_territoire.meta_action (
    numero character varying,
    impact_eco boolean,
    avec_params boolean
);


ALTER TABLE strategie_territoire.meta_action OWNER TO terristory;

--
-- Name: parc_tcp; Type: TABLE; Schema: strategie_territoire; Owner: postgres
--

CREATE TABLE strategie_territoire.parc_tcp (
    id integer NOT NULL,
    type_vehicule character varying,
    commune character varying,
    energie character varying,
    valeur integer
);


ALTER TABLE strategie_territoire.parc_tcp OWNER TO postgres;

--
-- Name: parc_tcp_id_seq; Type: SEQUENCE; Schema: strategie_territoire; Owner: postgres
--

CREATE SEQUENCE strategie_territoire.parc_tcp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE strategie_territoire.parc_tcp_id_seq OWNER TO postgres;

--
-- Name: parc_tcp_id_seq; Type: SEQUENCE OWNED BY; Schema: strategie_territoire; Owner: postgres
--

ALTER SEQUENCE strategie_territoire.parc_tcp_id_seq OWNED BY strategie_territoire.parc_tcp.id;


--
-- Name: va_prod; Type: MATERIALIZED VIEW; Schema: strategie_territoire; Owner: terristory
--

CREATE MATERIALIZED VIEW strategie_territoire.va_prod AS
 WITH first AS (
         SELECT n.a732,
            (a.valeur_ajoutee / (a.production_totale + a.marge_commerciale)) AS part_va_prod,
            'a732'::text AS type
           FROM (strategie_territoire.a732 a
             JOIN strategie_territoire.naf n ON ((n.id = a.naf)))
          WHERE ((a.valeur_ajoutee IS NOT NULL) AND (a.production_totale IS NOT NULL) AND (a.marge_commerciale IS NOT NULL))
        )
 SELECT n.a732,
    ((a.prod_branche_prix_courants - a.conso_intermediaire_prix_courants) / a.prod_branche_prix_courants) AS part_va_prod,
    'a88'::text AS type
   FROM ((strategie_territoire.naf n
     JOIN strategie_territoire.a88 a ON ((a.id = n.a88)))
     LEFT JOIN first f ON (((n.a732)::text = (f.a732)::text)))
  WHERE ((n.a732 IS NOT NULL) AND (f.a732 IS NULL))
UNION ALL
 SELECT first.a732,
    first.part_va_prod,
    first.type
   FROM first
  WITH NO DATA;


ALTER TABLE strategie_territoire.va_prod OWNER TO terristory;

--
-- Name: action_params id; Type: DEFAULT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.action_params ALTER COLUMN id SET DEFAULT nextval('strategie_territoire.action_params_id_seq'::regclass);


--
-- Name: parc_tcp id; Type: DEFAULT; Schema: strategie_territoire; Owner: postgres
--

ALTER TABLE ONLY strategie_territoire.parc_tcp ALTER COLUMN id SET DEFAULT nextval('strategie_territoire.parc_tcp_id_seq'::regclass);


--
-- Name: a732 a732_id_key; Type: CONSTRAINT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.a732
    ADD CONSTRAINT a732_id_key UNIQUE (id);


--
-- Name: a88 a88_id_key; Type: CONSTRAINT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.a88
    ADD CONSTRAINT a88_id_key UNIQUE (id);


--
-- Name: action_params action_params_pkey; Type: CONSTRAINT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.action_params
    ADD CONSTRAINT action_params_pkey PRIMARY KEY (id);


--
-- Name: naf naf_id_key; Type: CONSTRAINT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.naf
    ADD CONSTRAINT naf_id_key UNIQUE (id);


--
-- Name: parc_tcp parc_tcp_pkey; Type: CONSTRAINT; Schema: strategie_territoire; Owner: postgres
--

ALTER TABLE ONLY strategie_territoire.parc_tcp
    ADD CONSTRAINT parc_tcp_pkey PRIMARY KEY (id);


--
-- Name: a732 a732_naf_fkey; Type: FK CONSTRAINT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.a732
    ADD CONSTRAINT a732_naf_fkey FOREIGN KEY (naf) REFERENCES strategie_territoire.naf(id);


--
-- Name: a88 a88_naf_fkey; Type: FK CONSTRAINT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.a88
    ADD CONSTRAINT a88_naf_fkey FOREIGN KEY (naf) REFERENCES strategie_territoire.naf(id);


--
-- Name: action action_naf_fkey; Type: FK CONSTRAINT; Schema: strategie_territoire; Owner: terristory
--

ALTER TABLE ONLY strategie_territoire.action
    ADD CONSTRAINT action_naf_fkey FOREIGN KEY (naf) REFERENCES strategie_territoire.naf(id);


--
-- Name: TABLE parc_tcp; Type: ACL; Schema: strategie_territoire; Owner: postgres
--

GRANT ALL ON TABLE strategie_territoire.parc_tcp TO terristory;


--
-- Name: SEQUENCE parc_tcp_id_seq; Type: ACL; Schema: strategie_territoire; Owner: postgres
--

GRANT ALL ON SEQUENCE strategie_territoire.parc_tcp_id_seq TO terristory;


--
-- PostgreSQL database dump complete
--

