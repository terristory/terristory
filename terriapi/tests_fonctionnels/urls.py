﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

from utils import settings_functional_test

disabled_urls = [
    "aura_dev",
    "pdl_dev",
    "occ_dev",
    "aura_test",
    "pdl_test",
    "occ_test",
    "aura_prod",
    "pdl_prod",
    "occ_prod",
]

urls = {
    "aura_prod": {
        "disable": "aura_prod" in disabled_urls,
        "env_name": "Prod - AURA",
        "root_url": "http://auvergnerhonealpes.terristory.fr",
        "protected": False,
        "urls": [
            {
                "nom": "conso_energetique_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=1&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "conso_energetique_hab_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=15&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "conso_energetique_hab_poi_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=15&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes&theme=Déchets&installation=decheteries-isdnd-centre_tri",
                "class": "chartsContainer",
            },
        ],
    },
    "aura_test": {
        "env_name": "Test - AURA",
        "disable": "aura_test" in disabled_urls,
        "root_url": "http://auvergnerhonealpes.test.terristory.fr/",
        "protected": True,
        "authentication_url": "https://"
        + settings_functional_test.get("nginx", "user_server_test")
        + ":"
        + settings_functional_test.get("nginx", "password_server_test")
        + "@auvergnerhonealpes.test.terristory.fr/",
        "urls": [
            {
                "nom": "conso_energetique_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=1&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "conso_energetique_hab_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=15&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "conso_energetique_hab_poi_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=15&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes&theme=Déchets&installation=decheteries-isdnd-centre_tri",
                "class": "chartsContainer",
            },
        ],
    },
    "aura_dev": {
        "disable": "aura_dev" in disabled_urls,
        "root_url": "https://auvergnerhonealpes.dev.terristory.fr/",
        "protected": True,
        "authentication_url": "https://"
        + settings_functional_test.get("nginx", "user_server_dev")
        + ":"
        + settings_functional_test.get("nginx", "password_server_dev")
        + "@auvergnerhonealpes.dev.terristory.fr/",
        "urls": [
            {
                "nom": "conso_energetique_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=1&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "suivi_energetique_region_epci",
                "url": "suivi_energetique?zone=region&maille=epci&zone_id=84",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            {
                "nom": "conso_energetique_hab_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=15&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "conso_energetique_hab_poi_region_epci",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=15&theme=Consommation d'énergie&nom_territoire=Auvergne-Rhône-Alpes&theme=Déchets&installation=decheteries-isdnd-centre_tri",
                "class": "chartsContainer",
            },
            {
                "nom": "restitution_tableau_bord_region_epci",
                "url": "restitution_tableaux_bord?zone=region&maille=epci&zone_id=84&id_tableau=50&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "block-row",
                "scroll": [
                    "scrollHeight",
                ],
            },
            {
                "nom": "suivi_energetique_departement_epci_ardeche",
                "url": "suivi_energetique?zone=departement&maille=epci&zone_id=07&nom_territoire=Ard%C3%A8che",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            {
                "nom": "diagramme_sankey_region_epci",
                "url": "diagramme_sankey?zone=epci&maille=commune&zone_id=200011773&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration&sankey=conso_energetique_sankey",
                "class": "loader",
                "type_waiting": "absent",
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            {
                "nom": "diagramme_sankey_region_epci_biogaz",
                "url": "diagramme_sankey?zone=epci&maille=commune&zone_id=200011773&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration&sankey=sankey_biogaz",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "suivi_energetique_region_epci_commune_annemasse",
                "url": "suivi_energetique?zone=epci&maille=commune&zone_id=200011773&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            {
                "nom": "territorialsynthesis_epci_annemasse",
                "url": "territorialsynthesis?zone=epci&maille=commune&zone_id=200011773&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Facture énergétique résidentiel / hab",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=21&theme=Facture%20%C3%A9nerg%C3%A9tique&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Émissions GES / hab",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=30&theme=%C3%89missions%20de%20GES&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Part EnR/Consommation d'énergie",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=23&theme=Production%20d%27%C3%A9nergie&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Production EnR/hab",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=11352&theme=Production%20d%27%C3%A9nergie&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Potentiel d'implantation de l'éolien",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=25&theme=Potentiels%20ENR&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Emplois dans la construction",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=39&theme=%C3%89conomie%20et%20soci%C3%A9t%C3%A9&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_DPE Logements : étiquette énergie",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=5137&theme=B%C3%A2timents&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Quantité de déchets collectés par les collectivités /hab.DGF",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=2936&theme=Gestion%20des%20d%C3%A9chets&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Nombre de composteurs par maison individuelle",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=12229&theme=Gestion%20des%20d%C3%A9chets&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Nombre de ménages en précarité énergétique mobilité quotidienne",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=12127&theme=Pr%C3%A9carit%C3%A9%20%C3%A9nerg%C3%A9tique&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Besoins résidentiels et tertiaires en chaleur",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=12324&theme=Chaleur%20renouvelable&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Potentiels géothermiques sondes",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=12326&theme=Chaleur%20renouvelable&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Potentiels géothermiques nappes",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=12325&theme=Chaleur%20renouvelable&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Besoins tertiaires en chaleur",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=12244&theme=Chaleur%20renouvelable&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_epci_commune_annemasse_Besoins industriels en chaleur",
                "url": "?zone=epci&maille=commune&zone_id=200011773&analysis=12403&theme=Chaleur%20renouvelable&nom_territoire=CA%20Annemasse-Les%20Voirons-Agglom%C3%A9ration",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Besoins industriels en chaleur",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=12403&theme=Chaleur%20renouvelable&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Accessibilité aux services",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=6253&theme=Mobilit%C3%A9&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_Accessibilité à l’emploi",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=8370&theme=Mobilit%C3%A9&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_Migrations pendulaires",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=33&theme=Mobilit%C3%A9&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_Parts modales des déplacements domicile - travail",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=34&theme=Mobilit%C3%A9&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_Kilomètres parcourus pour le transport des marchandises",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=6355&theme=Mobilit%C3%A9&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_Labellisation des territoires (ADEME)",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=12323&theme=Environnement&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Taux d'étalement urbain",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=44&theme=Environnement&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Part des surfaces en agriculture bio",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=45&theme=Environnement&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Températures",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=3415&theme=Climat&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Enneigement",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=3412&theme=Climat&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Journées estivales",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=3416&theme=Climat&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "indicateur_region_epci_Part EnR/Consommation d'énergie",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=23&theme=Production%20d%27%C3%A9nergie&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_COVNM / hab",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=8&theme=Polluants%20atmosph%C3%A9riques&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_Consommation d'énergie / hab",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=15&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
            {
                "nom": "indicateur_region_epci_Consommation d'énergie",
                "url": "?zone=region&maille=epci&zone_id=84&analysis=1&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Auvergne-Rh%C3%B4ne-Alpes",
                "class": "chartsContainer",
            },
        ],
    },
    "pdl_dev": {
        "env_name": "Développement - Pays de la Loire",
        "disable": "pdl_dev" in disabled_urls,
        "root_url": "https://teo-paysdelaloire.dev.terristory.fr/",
        "protected": True,
        "authentication_url": "https://"
        + settings_functional_test.get("nginx", "user_server_dev")
        + ":"
        + settings_functional_test.get("nginx", "password_server_dev")
        + "@teo-paysdelaloire.dev.terristory.fr/",
        "urls": [
            {
                "nom": "conso_energetique_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire&nom_territoire=Pays%20de%20la%20Loire",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            {
                "nom": "population_municipale_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=10996&theme=D%C3%A9mographie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "nombre_menages_precarite_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=3704&theme=Précarité%20énergétique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "part_menages_precarite_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=2813&theme=Précarité%20énergétique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "tableau_bord_416_region",
                "url": "paysdelaloire/restitution_tableaux_bord?zone=region&maille=epci&zone_id=52&id_tableau=6476&nom_territoire=Pays%20de%20la%20Loire",
                "class": "block-row",
                "type_waiting": "present",
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            {
                "nom": "prod_energie_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=11162&theme=Production%20d'énergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Consommation d’énergie (EPCI)",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "chartsContainer",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-3-Electricité",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Consommation d’énergie (Département)",
                "url": "?zone=region&maille=departement&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-6-Gaz Naturel",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Emissions de gaz à effet de serre",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9773&theme=%C3%89missions%20de%20gaz%20%C3%A0%20effet%20de%20serre&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-0-Agriculture",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Consommation couverte par la production d’EnR :",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=11162&theme=Production%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Puits de carbone :",
                "url": "?zone=region&maille=departement&zone_id=52&analysis=10430&theme=Puits%20de%20carbone&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Quantité de déchets ménagers et assimilés collectés / hab.INSEE",
                "url": "?zone=region&maille=epci&zone_id=52&theme=D%C3%A9chets&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Part de ménages en précarité énergétique logement :",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=2813&theme=Pr%C3%A9carit%C3%A9%20%C3%A9nerg%C3%A9tique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Centres de tri :",
                "url": "?region=paysdelaloire&zone=region&maille=epci&zone_id=52&nom_territoire=Pays%20de%20la%20Loire&theme=D%C3%A9chets&installation=centres_tri",
                "class": "loader",
                "type_waiting": "absent",
            },
        ],
    },
    "pdl_prod": {
        "disable": "pdl_prod" in disabled_urls,
        "env_name": "Production - Pays de la Loire",
        "root_url": "https://teo-paysdelaloire.terristory.fr/",
        "protected": False,
        "urls": [
            {
                "nom": "conso_energetique_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire&nom_territoire=Pays%20de%20la%20Loire",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            {
                "nom": "nombre_menages_precarite_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=3704&theme=Précarité%20énergétique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "part_menages_precarite_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=2813&theme=Précarité%20énergétique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "tableau_bord_416_region",
                "url": "restitution_tableaux_bord?zone=region&maille=epci&zone_id=52&id_tableau=416&nom_territoire=Pays%20de%20la%20Loire",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "prod_energie_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=11162&theme=Production%20d'énergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Consommation d’énergie (EPCI)",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-3-Electricité",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Consommation d’énergie (Département)",
                "url": "?zone=region&maille=departement&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-6-Gaz Naturel",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Emissions de gaz à effet de serre",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9773&theme=%C3%89missions%20de%20gaz%20%C3%A0%20effet%20de%20serre&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-0-Agriculture",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Consommation couverte par la production d’EnR :",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=11162&theme=Production%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Puits de carbone :",
                "url": "?zone=region&maille=departement&zone_id=52&analysis=10430&theme=Puits%20de%20carbone&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Quantité de déchets ménagers et assimilés collectés / hab.INSEE",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=10709&theme=D%C3%A9chets&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Part de ménages en précarité énergétique logement :",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=2813&theme=Pr%C3%A9carit%C3%A9%20%C3%A9nerg%C3%A9tique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Centres de tri :",
                "url": "?region=paysdelaloire&zone=region&maille=epci&zone_id=52&nom_territoire=&theme=D%C3%A9chets&installation=centres_tri",
                "class": "poi-legend",
                "type_waiting": "at_least(4)",  # two by legend elements
            },
            {
                "nom": "Indicateurs clés – Maine et Loire :",
                "url": "restitution_tableaux_bord?zone=departement&maille=epci&zone_id=49&id_tableau=416&nom_territoire=Maine-et-Loire",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
        ],
    },
    "pdl_test": {
        "env_name": "Test - Pays de la Loire",
        "disable": "pdl_test" in disabled_urls,
        "root_url": "https://teo-paysdelaloire.test.terristory.fr/",
        "protected": True,
        "authentication_url": "https://"
        + settings_functional_test.get("nginx", "user_server_test")
        + ":"
        + settings_functional_test.get("nginx", "password_server_test")
        + "@teo-paysdelaloire.test.terristory.fr/",
        "urls": [
            {
                "nom": "conso_energetique_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire&nom_territoire=Pays%20de%20la%20Loire",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            {
                "nom": "nombre_menages_precarite_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=3704&theme=Précarité%20énergétique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "part_menages_precarite_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=2813&theme=Précarité%20énergétique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "tableau_bord_416_region",
                "url": "restitution_tableaux_bord?zone=region&maille=epci&zone_id=52&id_tableau=416&nom_territoire=Pays%20de%20la%20Loire",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "prod_energie_region_epci",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=11162&theme=Production%20d'énergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Consommation d’énergie (EPCI)",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-3-Electricité",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Consommation d’énergie (Département)",
                "url": "?zone=region&maille=departement&zone_id=52&zone_id=52&analysis=9774&theme=Consommation%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-6-Gaz Naturel",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Emissions de gaz à effet de serre",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=9773&theme=%C3%89missions%20de%20gaz%20%C3%A0%20effet%20de%20serre&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
                "actions": [
                    {
                        "type": "click",
                        "id": "legende-0-Agriculture",
                        "class": "loader",
                        "type_waiting": "absent",
                    }
                ],
            },
            {
                "nom": "Consommation couverte par la production d’EnR :",
                "url": "?zone=region&maille=epci&zone_id=52&zone_id=52&analysis=11162&theme=Production%20d%27%C3%A9nergie&nom_territoire=Pays%20de%20la%20Loire&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Puits de carbone :",
                "url": "?zone=region&maille=departement&zone_id=52&analysis=10430&theme=Puits%20de%20carbone&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Quantité de déchets ménagers et assimilés collectés / hab.INSEE",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=10709&theme=D%C3%A9chets&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Part de ménages en précarité énergétique logement :",
                "url": "?zone=region&maille=epci&zone_id=52&analysis=2813&theme=Pr%C3%A9carit%C3%A9%20%C3%A9nerg%C3%A9tique&nom_territoire=Pays%20de%20la%20Loire",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "Centres de tri :",
                "url": "?region=paysdelaloire&zone=region&maille=epci&zone_id=52&nom_territoire=&theme=D%C3%A9chets&installation=centres_tri",
                "class": "poi-legend",
                "type_waiting": "at_least(4)",  # two by legend elements
            },
            {
                "nom": "Indicateurs clés – Maine et Loire :",
                "url": "restitution_tableaux_bord?zone=departement&maille=epci&zone_id=49&id_tableau=416&nom_territoire=Maine-et-Loire",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
        ],
    },
    "occ_prod": {
        "disable": "occ_prod" in disabled_urls,
        "env_name": "Production - Occitanie",
        "root_url": "https://arec-occitanie.terristory.fr/",
        "protected": False,
        "urls": [
            # INDICATEURS
            {
                "nom": "prod_enr_region",
                "url": "?zone=region&maille=departement&zone_id=76&analysis=56&theme=Production%20d%27%C3%A9nergie%20renouvelable&nom_territoire=Occitanie",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            {
                "nom": "prod_enr_epci",
                "url": "?zone=epci&maille=epci&zone_id=200066355&analysis=56&theme=Production%20d%27%C3%A9nergie%20renouvelable&nom_territoire=CA%20S%C3%A8te%20Agglop%C3%B4le%20M%C3%A9diterran%C3%A9e",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            {
                "nom": "part_enr_conso_region",
                "url": "?zone=region&maille=scot&zone_id=76&analysis=53&theme=Production%20d%27%C3%A9nergie%20renouvelable&nom_territoire=Occitanie",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            # EQUIPEMENTS
            {
                "nom": "equip_borne_irve",
                "url": "?region=occitanie&zone=departement&maille=epci&zone_id=32&nom_territoire=Gers&theme=Mobilit%C3%A9s%20alternatives&installation=borne_irve",
                "class": "poi-legend",
                "type_waiting": "at_least(2)",  # two by legend elements
            },
            {
                "nom": "equip_plusieurs",
                "url": "?region=occitanie&zone=departement&maille=epci&zone_id=32&nom_territoire=Gers&theme=Installations%20ENR&installation=geothermie-reseau_chaleur-bornes_hydrogene-borne_irve-bornes_bio_gnv-friche-eolien-methanisation",
                "class": "poi-legend",
                "type_waiting": "at_least(4)",  # two by legend elements
            },
            # SUIV ENERGETIQUE
            {
                "nom": "suivi_energetique_departement",
                "url": "suivi_energetique?zone=departement&maille=epci&zone_id=32&nom_territoire=Gers",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            # territorialsynthesis
            {
                "nom": "territorialsynthesis_petr_rouergue",
                "url": "territorialsynthesis?zone=petr&maille=epci&zone_id=petr04&nom_territoire=PETR%20du%20Haut%20Rouergue",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "territorialsynthesis_scot_nord_toulouse",
                "url": "territorialsynthesis?zone=scot&maille=commune&zone_id=2360&nom_territoire=Nord%20toulousain%20-%20SCOT%20en%20r%C3%A9vision%20:%20d%C3%A9lib%C3%A9ration%20prise",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "territorialsynthesis_departement_gers",
                "url": "territorialsynthesis?zone=departement&maille=epci&zone_id=32&nom_territoire=Gers",
                "class": "loader",
                "type_waiting": "absent",
            },
            # TABLEAUX BORDS
            {
                "nom": "tableau_bord_region_general",
                "url": "restitution_tableaux_bord?zone=region&maille=epci&zone_id=76&id_tableau=850&nom_territoire=Occitanie",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_departement_haute_garonne",
                "url": "restitution_tableaux_bord?zone=departement&maille=scot&zone_id=31&id_tableau=643&nom_territoire=Haute-Garonne",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_departement_haute_garonne",
                "url": "restitution_tableaux_bord?zone=departement&maille=scot&zone_id=31&id_tableau=851&nom_territoire=Haute-Garonne",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_petr_rouergue",
                "url": "restitution_tableaux_bord?zone=petr&maille=commune&zone_id=petr04&id_tableau=851&nom_territoire=PETR%20du%20Haut%20Rouergue",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_commune_salanque",
                "url": "restitution_tableaux_bord?zone=scot&maille=commune&zone_id=10468&id_tableau=645&nom_territoire=Corbi%C3%A8res,%20Salanque,%20M%C3%A9diterran%C3%A9e%20-%20SCOT%20en%20%C3%A9laboration%20:%20d%C3%A9lib%C3%A9ration%20prise",
                "class": "block-row",
                "type_waiting": "at_least(5)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_departement_pnr_lot",
                "url": "restitution_tableaux_bord?zone=departement&maille=pnr&zone_id=46&id_tableau=643&nom_territoire=Lot",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
        ],
    },
    "occ_test": {
        "env_name": "Test - Occitanie",
        "disable": "occ_test" in disabled_urls,
        "root_url": "https://arec-occitanie.test.terristory.fr/",
        "protected": True,
        "authentication_url": "https://"
        + settings_functional_test.get("nginx", "user_server_test")
        + ":"
        + settings_functional_test.get("nginx", "password_server_test")
        + "@arec-occitanie.test.terristory.fr/",
        "urls": [
            # INDICATEURS
            {
                "nom": "prod_enr_region",
                "url": "?zone=region&maille=departement&zone_id=76&analysis=56&theme=Production%20d%27%C3%A9nergie%20renouvelable&nom_territoire=Occitanie",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            {
                "nom": "prod_enr_epci",
                "url": "?zone=epci&maille=epci&zone_id=200066355&analysis=56&theme=Production%20d%27%C3%A9nergie%20renouvelable&nom_territoire=CA%20S%C3%A8te%20Agglop%C3%B4le%20M%C3%A9diterran%C3%A9e",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            {
                "nom": "part_enr_conso_region",
                "url": "?zone=region&maille=scot&zone_id=76&analysis=53&theme=Production%20d%27%C3%A9nergie%20renouvelable&nom_territoire=Occitanie",
                "class": "chartsContainer",
                "type_waiting": "present",
            },
            # EQUIPEMENTS
            {
                "nom": "equip_borne_irve",
                "url": "?region=occitanie&zone=departement&maille=epci&zone_id=32&nom_territoire=Gers&theme=Mobilit%C3%A9s%20alternatives&installation=borne_irve",
                "class": "poi-legend",
                "type_waiting": "at_least(2)",  # two by legend elements
            },
            {
                "nom": "equip_plusieurs",
                "url": "?region=occitanie&zone=departement&maille=epci&zone_id=32&nom_territoire=Gers&theme=Installations%20ENR&installation=geothermie-reseau_chaleur-bornes_hydrogene-borne_irve-bornes_bio_gnv-friche-eolien-methanisation",
                "class": "poi-legend",
                "type_waiting": "at_least(4)",  # two by legend elements
            },
            # SUIV ENERGETIQUE
            {
                "nom": "suivi_energetique_departement",
                "url": "suivi_energetique?zone=departement&maille=epci&zone_id=32&nom_territoire=Gers",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/2",
                    "scrollHeight",
                ],
            },
            # territorialsynthesis
            {
                "nom": "territorialsynthesis_petr_rouergue",
                "url": "territorialsynthesis?zone=petr&maille=epci&zone_id=petr04&nom_territoire=PETR%20du%20Haut%20Rouergue",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "territorialsynthesis_scot_nord_toulouse",
                "url": "territorialsynthesis?zone=scot&maille=commune&zone_id=2360&nom_territoire=Nord%20toulousain%20-%20SCOT%20en%20r%C3%A9vision%20:%20d%C3%A9lib%C3%A9ration%20prise",
                "class": "loader",
                "type_waiting": "absent",
            },
            {
                "nom": "territorialsynthesis_departement_gers",
                "url": "territorialsynthesis?zone=departement&maille=epci&zone_id=32&nom_territoire=Gers",
                "class": "loader",
                "type_waiting": "absent",
            },
            # TABLEAUX BORDS
            {
                "nom": "tableau_bord_region_general",
                "url": "restitution_tableaux_bord?zone=region&maille=epci&zone_id=76&id_tableau=850&nom_territoire=Occitanie",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_departement_haute_garonne",
                "url": "restitution_tableaux_bord?zone=departement&maille=scot&zone_id=31&id_tableau=643&nom_territoire=Haute-Garonne",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_departement_haute_garonne",
                "url": "restitution_tableaux_bord?zone=departement&maille=scot&zone_id=31&id_tableau=851&nom_territoire=Haute-Garonne",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_petr_rouergue",
                "url": "restitution_tableaux_bord?zone=petr&maille=commune&zone_id=petr04&id_tableau=851&nom_territoire=PETR%20du%20Haut%20Rouergue",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_commune_salanque",
                "url": "restitution_tableaux_bord?zone=scot&maille=commune&zone_id=10468&id_tableau=645&nom_territoire=Corbi%C3%A8res,%20Salanque,%20M%C3%A9diterran%C3%A9e%20-%20SCOT%20en%20%C3%A9laboration%20:%20d%C3%A9lib%C3%A9ration%20prise",
                "class": "block-row",
                "type_waiting": "at_least(5)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
            {
                "nom": "tableau_bord_departement_pnr_lot",
                "url": "restitution_tableaux_bord?zone=departement&maille=pnr&zone_id=46&id_tableau=643&nom_territoire=Lot",
                "class": "block-row",
                "type_waiting": "at_least(8)",  # two by graph
                "scroll": [
                    "scrollHeight/3",
                    "scrollHeight",
                ],
            },
        ],
    },
}
