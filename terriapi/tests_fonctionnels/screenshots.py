﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import argparse
import hashlib
import json
import sys
import time

import slugify
from selenium import webdriver
from selenium.common.exceptions import UnexpectedAlertPresentException
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from tests_fonctionnels.urls import urls as all_urls
from webdriver_manager.firefox import GeckoDriverManager


def _parse_args(args):
    parser = argparse.ArgumentParser(description="Captures d'écran automatiques")
    parser.add_argument(
        "--state",
        type=str,
        required=True,
        choices=("before", "after"),
        help="Est-ce avant ou après la MAJ ?",
    )
    args = parser.parse_args()
    return args


args = _parse_args(sys.argv[1:])
state = args.state

opts = webdriver.FirefoxOptions()
opts.add_argument("--width=1920")
opts.add_argument("--height=1080")
browser = webdriver.Firefox(
    service=Service(GeckoDriverManager().install()), options=opts
)


class Screenshot:
    def __init__(self, **kwargs):
        self.params = kwargs

        if "scrolling" in kwargs:
            self.params["scrolling"] = json.dumps(self.params["scrolling"])
        if "actions" in kwargs:
            self.params["actions"] = json.dumps(self.params["actions"])

    def save(self, headers=None):
        if headers is not None:
            return ";".join([str(self.params[header]) for header in headers])
        return ";".join(self.params.values())

    def get_headers(self):
        return self.params.keys()


def wait_for_action(browser, type_waiting, class_name):
    # WebDriverWait attend que la balise soit présente pour passer à la suite du script.
    # Si après le temps imparti, la balise n'est toujours pas sur l'interface, une erreur est levée.
    if type_waiting == "present":
        WebDriverWait(browser, 15).until(
            EC.presence_of_element_located((By.CLASS_NAME, class_name))
        )
    elif type_waiting == "absent":
        WebDriverWait(browser, 15).until(
            EC.invisibility_of_element_located((By.CLASS_NAME, class_name))
        )
    elif type_waiting.startswith("at_least"):
        nb_blocks = type_waiting[len("at_least(") : -1]

        def waiting_func(driver):
            list_blocks = driver.find_elements(By.CLASS_NAME, class_name)
            return len(list_blocks) >= int(nb_blocks)

        WebDriverWait(browser, 20).until(lambda driver: waiting_func(driver))


screenshots_taken = []
i = 0
for root_url_name, params in all_urls.items():
    urls = params.get("urls", [])
    disable = params.get("disable", False)
    env_name = params.get("env_name", root_url_name)
    if disable:
        continue
    root_url = params["root_url"]
    print("Handles", root_url)

    protected = params.get("protected", False)
    if protected:
        print("\t Needs Authentication")
        authentication_url = params["authentication_url"]
        # connexion à l'application terristory
        res = browser.get(authentication_url)
        print("\t Authentication accepted")

    for j, param_url in enumerate(urls):
        url = param_url["url"]
        id_screenshot = hashlib.md5(url.encode()).hexdigest()
        class_name = param_url.get("class", "chartsContainer")
        nom = slugify.slugify(param_url.get("nom", j))
        type_waiting = param_url.get("type_waiting", "present")

        print("\t", nom)
        print("\t - Goes to", url)
        print("\t\t ", root_url + url)

        # we can try more than one time
        tries = 0
        max_tries = 3
        succeeded = False
        while tries < max_tries and not succeeded:
            try:
                # Loads page
                browser.get(root_url + url)
                wait_for_action(browser, type_waiting, class_name)
                succeeded = True

                # on attend la fin des animations
                time.sleep(2)

                browser.save_screenshot(
                    f"output/{state}_screenshot_{id_screenshot}_{root_url_name}_{nom}.png"
                )

            # another try is authorized when exception is raised
            except UnexpectedAlertPresentException as e:
                print("\t\t" + ("##" * 40))
                print("\t\t" + ("\n\t\t".join(str(e).split("\n"))))
                print("\t\t" + ("##" * 40))
                print(f"\t\t Tries reloading ({tries+1} try)")
                print("\t\t" + ("##" * 40))
                time.sleep(1)
                browser.refresh()
            tries += 1

        if not succeeded:
            print("---" * 40)
            print("Skips")
            screenshots_taken.append(
                Screenshot(
                    id="FAILED",
                    env=env_name,
                    name=nom,
                    root_url=root_url,
                    used_authentication=True,
                    final_url=root_url + url,
                    output_file="FAILED",
                    nb_tries=tries,
                    scrolling=[],
                    actions=[],
                )
            )
            # create alert object
            alert = Alert(browser)

            # get alert text
            print("Error message:", alert.text)
            print("---" * 40)

            # accept the alert
            alert.accept()
            continue

        # if we have any scrolling to do to take other screenshots
        scrolling = param_url.get("scroll", [])
        for k, scroll in enumerate(scrolling):
            html = browser.find_element(
                by=By.CSS_SELECTOR, value=".widgets.full-screen-widget"
            )
            browser.execute_script(f"window.scrollTo(0, document.body.{scroll})", html)
            time.sleep(2)
            browser.save_screenshot(
                f"output/{state}_screenshot_{id_screenshot}_{root_url_name}_{nom}_scroll{k}.png"
            )
            print("\t\t Scrolls to", scroll)

        actions = param_url.get("actions", [])
        for k, action in enumerate(actions):
            # if we had some scrolling to do before
            if k == 0 and len(scrolling) > 0:
                browser.execute_script(f"window.scrollTo(0, 0);", html)
                time.sleep(2)

            # we select the object
            object_to_use = browser.find_element(by=By.ID, value=action["id"])
            # and then apply the action
            if action["type"] == "click":
                object_to_use.click()
            else:
                continue

            # if we need to wait for some class / actions
            if "type_waiting" in action and "class_name" in action:
                wait_for_action(browser, action["type_waiting"], action["class_name"])
            time.sleep(2)

            browser.save_screenshot(
                f"output/{state}_screenshot_{id_screenshot}_{root_url_name}_{nom}_action{k}.png"
            )
        print("---" * 50)

        screenshots_taken.append(
            Screenshot(
                id=id_screenshot,
                env=env_name,
                name=nom,
                root_url=root_url,
                used_authentication=True,
                final_url=root_url + url,
                output_file=f"output/{state}_screenshot_{id_screenshot}_{root_url_name}_{nom}.png",
                nb_tries=tries,
                scrolling=scrolling,
                actions=actions,
            )
        )

    print("###" * 50)
    i += 1

browser.close()

headers = (
    "id",
    "env",
    "name",
    "root_url",
    "final_url",
    "output_file",
    "used_authentication",
    "nb_tries",
    "scrolling",
    "actions",
)
output = [
    ";".join(
        (
            "#",
            "Environnement",
            "Nom",
            "Racine",
            "URL finale",
            "Fichier sortant",
            "Authentification nécessaire",
            "Nombre d'essais",
            "Scrolling sur la page",
            "Actions",
        )
    )
]
for screenshot in screenshots_taken:
    output.append(screenshot.save(headers))
with open(f"output/{state}_summary.csv", "w") as f:
    f.write("\n".join(output))
