# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json
import logging
import os
import time

import jsondiff as jd
import requests
import slugify
from utils import check_consistency_files

logging.basicConfig(
    level=logging.INFO,
    format="",
    handlers=[
        logging.FileHandler("output/strategie_territoriale/comparisons/overall.log"),
        logging.StreamHandler(),
    ],
)
with open("config/strategy_parameters_for_single_comparisons.json", "r") as f:
    queries_parameters = json.loads(f.read())

OUTPUT_PATH = "output/strategie_territoriale/"
REGION = "auvergne-rhone-alpes"
REGION = "occitanie"
REGION = "paysdelaloire"
ONLY_ACTIONS = [
    "Action 1",
    "Action 2",
    "Action 3 PV sol",
    "Action 3b (éolien)",
    "Action 4 (Solaire thermique ECS)",
    "Action 4b (Solaire thermique combine)",
    "Action 5 (Heating network)",
    "Action 6 (Métha Cogé)",
    "Action 6b (Métha inj)",
    "Action 7 (Bike lanes)",
    "Action 8 (MixEnergHeatNet)",
    "Action 10a (PV roof)",
    "Action 10b (PV roof)",
    "Action 10c (PV roof)",
    "Action 12 (Hydro)",
    "Action 13 (Wood)",
    "Action 14 (EffInd)",
    "Action 15 (EffAgr)",
    "Action 16 (HeatConvRes)",
    "Action 17 (HeatConvTer)",
    "Action 18 (ReducTrip)",
    "Action 19 (Carpooling)",
    "Action 20 (Motor change pub)",
    "Action 21 (RedEmInd)",
    "Action 22 (RedEmAgr)",
    "Full query example",
]


def SEP():
    logging.info("-" * 30)


s = requests.session()

BASE_URL = "http://localhost:8080/api/auvergne-rhone-alpes/84/id_utilisateur"
if REGION == "occitanie":
    BASE_URL = "http://localhost:8080/api/occitanie/76/id_utilisateur"
if REGION == "paysdelaloire":
    BASE_URL = "http://localhost:8080/api/paysdelaloire/52/id_utilisateur"
user_response = s.get(BASE_URL)

user = user_response.json()["id_utilisateur"]
zone = "epci"
zone_id = "200011773"
if REGION == "occitanie":
    zone_id = "243400819"
if REGION == "paysdelaloire":
    zone_id = "200060010"

url_1 = f"http://localhost:8080/api/{REGION}/actions?zone={zone}&zone_id={zone_id}&id_utilisateur={user}"
url_2 = f"http://localhost:8080/api/{REGION}/actions?zone={zone}&zone_id={zone_id}&id_utilisateur={user}"


def execute_plan(action_key, url, moment, parameters):
    start_time = time.time()
    req = s.post(url, json=parameters)
    eluded_time = time.time() - start_time
    logging.info(f"\t took {eluded_time} seconds {moment}")
    if req.status_code != 200:
        logging.info(f"\t ERREUR dans la requête *{moment}*")
        return False
    else:
        res = json.loads(req.content.decode("utf-8"))
        with open(f"{OUTPUT_PATH}results/{moment}_{action_key}.json", "w") as f:
            json.dump(res, f, indent=4)
        return res


def retrieve_results():
    for action in queries_parameters:
        if len(ONLY_ACTIONS) > 0 and action["name"] not in ONLY_ACTIONS:
            continue
        logging.info("Calls for " + action["name"])
        action_key = slugify.slugify(action["name"])
        parameters = json.loads(action["parameters"])

        SEP()
        res_before = execute_plan(action_key, url_1, "before", parameters)
        res_after = execute_plan(action_key, url_2, "after", parameters)
        SEP()
        if not res_before or not res_after:
            logging.info("\t skip to next action")
            logging.info("")
            continue


def raw_json_comparison():
    for action in queries_parameters:
        if len(ONLY_ACTIONS) > 0 and action["name"] not in ONLY_ACTIONS:
            continue
        logging.info("Calls for", action["name"])
        action_key = slugify.slugify(action["name"])

        if not os.path.isfile(f"{OUTPUT_PATH}results/before_{action_key}.json"):
            logging.info(f"\t Missing before results")
            SEP()
            continue
        if not os.path.isfile(f"{OUTPUT_PATH}results/after_{action_key}.json"):
            logging.info(f"\t Missing after results")
            continue

        diffs = check_consistency_files(
            f"{OUTPUT_PATH}results/before_{action_key}.json",
            f"{OUTPUT_PATH}results/after_{action_key}.json",
        )

        i = 0
        all_diffs = []
        for diff in diffs:
            # When first line is True, it means there was no diff
            if i == 0 and diff == True:
                break
            i += 1
            # we save the diffs to display them in final file
            all_diffs.append(diff)
        # if we have more than zero diff, save it somewhere
        if i > 0:
            logging.info(f"\t {i} diffs found !")
            # we save the summary and the results
            with open(OUTPUT_PATH + "comparisons/raw_" + action_key + ".txt", "w") as f:
                f.write("".join(all_diffs))
        else:
            logging.info("\t No diff!")
        SEP()


def fine_json_comparison():
    for action in queries_parameters:
        if len(ONLY_ACTIONS) > 0 and action["name"] not in ONLY_ACTIONS:
            continue
        logging.info("Calls for " + action["name"])
        action_key = slugify.slugify(action["name"])

        if not os.path.isfile(f"{OUTPUT_PATH}results/before_{action_key}.json"):
            logging.info(f"\t Missing before results")
            SEP()
            break
        with open(f"{OUTPUT_PATH}results/before_{action_key}.json", "r") as f:
            res_before = json.load(f)

        if not os.path.isfile(f"{OUTPUT_PATH}results/after_{action_key}.json"):
            logging.info(f"\t Missing after results")
            break
        with open(f"{OUTPUT_PATH}results/after_{action_key}.json", "r") as f:
            res_after = json.load(f)

        logging.info("\n- Checking energy bill")
        before_energy_bill = res_before.get("facture_energetique", {})
        after_energy_bill = res_after.get("facture_energetique", {})
        no_diff = True
        for year, val in before_energy_bill.items():
            if year in after_energy_bill and after_energy_bill[year] == val:
                continue
            else:
                no_diff = False
                logging.info(
                    f"\t {year} : {val} -> {after_energy_bill.get(year, None)}"
                )
        if no_diff:
            logging.info("\t No diff!")
            SEP()

        logging.info("\n- Checking tax revenue")
        before_tax_rev = res_before.get("retombees_fiscales", {})
        after_tax_rev = res_after.get("retombees_fiscales", {})
        no_diff = True
        for year, vals in before_tax_rev.items():
            levels_diff = []
            for level in vals:
                if (
                    year in after_tax_rev
                    and level in after_tax_rev[year]
                    and after_tax_rev[year][level] == vals[level]
                ):
                    continue
                else:
                    no_diff = False
                    levels_diff.append(
                        f"\t\t {level} : {vals[level]} -> {after_tax_rev.get(year, {}).get(level, None)}"
                    )
            if len(levels_diff) > 0:
                logging.info(f"\t {year} :")
                logging.info("\n".join(levels_diff))
        if no_diff:
            logging.info("\t No diff!")
            SEP()

        logging.info("\n- Checking impact emplois")
        before_jobs = res_before.get("impact_emplois", {})
        after_jobs = res_after.get("impact_emplois", {})
        no_diff = True
        for year, vals in before_jobs.items():
            levels_diff = []
            if (
                year in after_jobs
                and "investissement" in after_jobs[year]
                and after_jobs[year]["investissement"] == vals["investissement"]
            ):
                continue
            else:
                no_diff = False
                levels_diff.append(
                    f"\t\t investissement : {vals['investissement']} -> {after_jobs.get(year, {}).get('investissement', None)}"
                )

            for type_impact in ["direct", "indirect"]:
                vals_jobs = (
                    after_jobs.get(year, {}).get(type_impact, {}).get("region", [])
                )
                for geography in ["region", "epci"]:
                    for level in vals_jobs:
                        if (
                            year in after_jobs
                            and type_impact in after_jobs[year]
                            and geography in after_jobs[year][type_impact]
                            and level in after_jobs[year][type_impact][geography]
                            and after_jobs[year][type_impact][geography][level]
                            == vals[type_impact][geography][level]
                        ):
                            continue
                        else:
                            no_diff = False

                            old_val = vals[type_impact][geography][level]
                            new_val = (
                                after_jobs.get(year, {})
                                .get(type_impact, {})
                                .get(geography, {})
                                .get(level, {})
                            )
                            levels_diff.append(f"\t\t {level} / {geography} :")
                            levels_diff.append(
                                f"\t\t\t\t va_totale : {old_val['va_totale']} -> {new_val.get('va_totale', None)}"
                            )
                            levels_diff.append(
                                f"\t\t\t\t nb_emploi_total : {old_val['nb_emploi_total']} -> {new_val.get('nb_emploi_total', None)}"
                            )
            if len(levels_diff) > 0:
                logging.info(f"\t {year} :")
                logging.info("\n".join(levels_diff))
        if no_diff:
            logging.info("\t No diff!")
            SEP()

        sector_vector_keys = [
            # "impact_emplois",
            "energie_economisee",
            "energie_produite",
            "emission_ges",
            "covnm",
            "nh3",
            "sox",
            "nox",
            "pm10",
            "pm25",
        ]

        for key in sector_vector_keys:
            logging.info("\n- Checking " + key)
            before = res_before.get(key, {})
            after = res_after.get(key, {})

            subkeys = ["secteur", "energie"]
            no_diff = True
            for subkey in subkeys:
                logging.info("\t - " + subkey)
                before_data = before.get(subkey, {})
                after_data = after.get(subkey, {})

                before_values = set(d.get("nom", None) for d in before_data)
                after_values = set(d.get("nom", None) for d in after_data)
                if before_values != after_values:
                    lost = before_values.difference(after_values)
                    if len(lost) > 0:
                        logging.info(f"\t lost {', '.join(lost)}")
                        no_diff = False
                    added = after_values.difference(before_values)
                    if len(added) > 0:
                        logging.info(f"\t added {', '.join(added)}")
                        no_diff = False

                common_categories = before_values.intersection(after_values)
                for category_name in common_categories:
                    cat_before_data = next(
                        d for d in before_data if d["nom"] == category_name
                    )
                    cat_after_data = next(
                        d for d in after_data if d["nom"] == category_name
                    )

                    if cat_after_data.get("confidentiel", None) != cat_before_data.get(
                        "confidentiel", None
                    ):
                        logging.info(f"\t confidentiality changed")

                    before_data_parsed = {}
                    for vals in cat_before_data["data"]:
                        year = vals["annee"]
                        val = vals["valeur"]
                        before_data_parsed[year] = val
                    after_data_parsed = {}
                    for vals in cat_after_data["data"]:
                        year = vals["annee"]
                        val = vals["valeur"]
                        after_data_parsed[year] = val

                    diffs = jd.diff(before_data_parsed, after_data_parsed)
                    if len(diffs) == 0:
                        continue
                    no_diff = False
                    logging.info(f"\t\t {category_name}")
                    for year, diff in diffs.items():
                        logging.info(
                            f"\t\t\t {year} : {before_data_parsed.get(year, '')} -> {after_data_parsed.get(year, '')}"
                        )
            if no_diff:
                logging.info("\t No diff!")
                SEP()


def parse_whole_log():
    full_log_path = f"{OUTPUT_PATH}comparisons/overall.log"
    with open(full_log_path, "r") as f:
        full_log = f.read()
    logs_by_action = full_log.split("Calls for Action ")
    for action in logs_by_action[1:]:
        action_key = action.split("\n")[0]
        with open(f"{OUTPUT_PATH}comparisons/action-{action_key}.log", "w") as f:
            f.write(action)


retrieve_results()
fine_json_comparison()
parse_whole_log()
