﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import configparser
import difflib

import pandas as pd
import requests

settings_functional_test = configparser.ConfigParser(allow_no_value=True)
settings_functional_test.read("config/config_tests_fonctionnels.ini")
VERBOSE = settings_functional_test.get("tests", "verbose") == "yes"

REGIONS_LIST_URL = "/api/region/"
REGION_DETAIL_URL = "/api/<region>/zone/region"

URLS_TO_TEST = {
    "analyses_list": "/api/<region>/analysis/",
    "analysis": "/api/<region>/analysis/<id_analyse>/data?zone=region&maille=epci&zone_id=<id_region>&provenance=carto&id_utilisateur=<id_utilisateur>&reinitialiserFiltres=True",
    "utilisateur": "/api/<region>/<id_region>/id_utilisateur",
}

SERVERS_CONFIG = {
    "auths": {
        "dev": (
            settings_functional_test.get("nginx", "user_server_dev"),
            settings_functional_test.get("nginx", "password_server_dev"),
        ),
        "test": (
            settings_functional_test.get("nginx", "user_server_test"),
            settings_functional_test.get("nginx", "password_server_test"),
        ),
    },
    "root_urls": {
        "local": settings_functional_test.get("root_urls", "local"),
        "dev": settings_functional_test.get("root_urls", "dev"),
        "test": settings_functional_test.get("root_urls", "test"),
        "prod": settings_functional_test.get("root_urls", "prod"),
    },
}


def regionalize(env, url, region="", region_url="", region_id="", **kwargs):
    """
    Transforms url from general to regional (e.g., /api/<region>/analysis to
    /api/paysdelaloire/analysis).

    Uses servers config dict with various parameters related to regional
    specificities.

    :param env: the env considered
    :type env: (dev|test|prod)
    :param url: the url to parse
    :type url: string
    :param region: the region considered
    :type region: string
    :return: the url parsed
    :rtype: string
    """

    parsed_url = SERVERS_CONFIG["root_urls"][env].replace(
        "<region>", region_url
    ).replace("<raw_region>", region) + url.replace("<region>", region).replace(
        "<id_region>", region_id
    )

    for k, v in kwargs.items():
        parsed_url = parsed_url.replace(f"<{k}>", str(v))

    return parsed_url


def check_consistency_files(filename_1, filename_2):
    """
    Checks the differences between two text files

    Parameters
    ----------
    filename_1 : string
        the path to first file
    filename_2 : string
        the path to second file
    """
    with open(filename_1) as file_1:
        file_1_text = file_1.readlines()

    with open(filename_2) as file_2:
        file_2_text = file_2.readlines()

    if file_1_text == file_2_text:
        return True, ""

    diffs = difflib.unified_diff(
        file_1_text,
        file_2_text,
        fromfile="Avant la MAJ",
        tofile="Après la MAJ",
        lineterm="",
    )
    # Find and print the diff:
    return diffs


def call_api(url, auth=None):
    """
    Calls API url and returns the result given

    Parameters
    ----------
    url : string
        the URL to call

    Returns
    -------
    dict
        the json is compiled to a dict
    """
    response = requests.get(url, auth=auth)
    json = response.json()
    return json


def retrieve_api_data_for_analysis(
    analysis_id, user_id, env, region, region_url, region_id, id_file, auth=None
):
    """
    Retrieves the API results

    Parameters
    ----------
    env : (dev|test|prod)
        the env considered
    output_filename : string
        the path where to save the files
    region : string
        the region considered
    """
    url = regionalize(
        env,
        URLS_TO_TEST["analysis"],
        region,
        region_url,
        region_id,
        **{"id_analyse": analysis_id, "id_utilisateur": user_id},
    )
    if VERBOSE:
        print(url)
    try:
        data_analysis = requests.post(url, data={}, auth=auth)
        data_analysis = data_analysis.json()
        data_map = pd.DataFrame(data_analysis.get("map", {}))
        data_charts = pd.DataFrame(data_analysis.get("charts", {}))
        url_map = f"{settings_functional_test.get('tests', 'path_file_saved')}test_{id_file}_{env}_{region}_map.csv"
        data_map.to_csv(url_map)
        url_charts = f"{settings_functional_test.get('tests', 'path_file_saved')}test_{id_file}_{env}_{region}_charts.csv"
        data_charts.to_csv(url_charts)
    except Exception as e:
        parsed_msg = "\n\t".join(str(e).split("\n"))
        print(
            f"""{'!'*50}
    Exception occurred for url :
        {url}
    Message:
        {parsed_msg}
    Just skipping...
{'!'*50}"""
        )
        return False
    return url_map
