﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import os

import pandas as pd
import utils

if __name__ == "__main__":
    url_files_saved = (
        utils.settings_functional_test.get("tests", "path_file_saved")
        + f"files_saved_before.csv"
    )
    df_before = pd.read_csv(url_files_saved)
    url_files_saved = (
        utils.settings_functional_test.get("tests", "path_file_saved")
        + f"files_saved_after.csv"
    )
    df_after = pd.read_csv(url_files_saved)

    # we then call both APIs and compare them
    output = []
    for idrow, data_df_after in df_after.iterrows():
        data_df_before = df_before.loc[
            (df_before["env"] == data_df_after["env"])
            & (df_before["region"] == data_df_after["region"])
            & (df_before["region_url"] == data_df_after["region_url"])
            & (df_before["region_id"] == data_df_after["region_id"])
            & (df_before["analysis_nom"] == data_df_after["analysis_nom"])
            & (df_before["analysis_data"] == data_df_after["analysis_data"])
            & (df_before["analysis_id"] == data_df_after["analysis_id"]),
            :,
        ]
        if len(data_df_before) == 0:
            summary = f"""{"="*50}
Couldn't reach API before on:
    * analysis: {data_df_after["analysis_nom"]} (#{data_df_after["analysis_id"]})
    * formula: {data_df_after["analysis_data"]}
    * region: {data_df_after["region"]}
    * region_url: {data_df_after["region_url"]}
    * region_id: {data_df_after["region_id"]}
"""
            output.append(summary)
            print(summary)
            continue
        data_df_before = data_df_before.iloc[0, :]
        file_url_after = data_df_after["file_url"]
        file_url_before = data_df_before["file_url"]

        if (not file_url_after or file_url_after == "False") and os.path.exists(
            file_url_before.replace("_before_", "_after_")
        ):
            file_url_after = file_url_before.replace("_before_", "_after_")

        if (not file_url_before or file_url_before == "False") and os.path.exists(
            file_url_after.replace("_after_", "_before_")
        ):
            file_url_before = file_url_after.replace("_after_", "_before_")

        if not file_url_after or file_url_after == "False":
            summary = f"""{"="*50}
Couldn't reach API after on:
    * analysis: {data_df_after["analysis_nom"]} (#{data_df_after["analysis_id"]})
    * formula: {data_df_after["analysis_data"]}
    * region: {data_df_after["region"]}
    * region_url: {data_df_after["region_url"]}
    * region_id: {data_df_after["region_id"]}
"""
            output.append(summary)
            print(summary)
        elif not file_url_before or file_url_before == "False":
            summary = f"""{"="*50}
Couldn't reach API before on:
    * analysis: {data_df_before["analysis_nom"]} (#{data_df_before["analysis_id"]})
    * formula: {data_df_before["analysis_data"]}
    * region: {data_df_before["region"]}
    * region_url: {data_df_before["region_url"]}
    * region_id: {data_df_before["region_id"]}
"""
            output.append(summary)
            print(summary)
        else:
            # after getting the data, we compare them
            diffs = utils.check_consistency_files(file_url_before, file_url_after)

            i = 0
            all_diffs = []
            for diff in diffs:
                # When first line is True, it means there was no diff
                if i == 0 and diff == True:
                    break
                i += 1
                # we save the diffs to display them in final file
                all_diffs.append(diff.strip())
            # if we have more than zero diff, save it somewhere
            if i > 0:
                # do something with diffs
                summary = f"""{"="*50}
{i} differences found on:
    * analysis: {data_df_after["analysis_nom"]} (#{data_df_after["analysis_id"]})
    * formula: {data_df_after["analysis_data"]}
    * region: {data_df_after["region"]}
    * region_url: {data_df_after["region_url"]}
    * region_id: {data_df_after["region_id"]}
"""
                # we save the summary and the results
                output.append(summary + ("-" * 50) + "\n" + "\n".join(all_diffs))
                # we display a shorter version for information
                print(
                    f"""{summary}
Details can be found by comparing {file_url_before} and {file_url_after} using `diff` for example:
    diff -u {file_url_before} {file_url_after}"""
                )

    # we save information in a file
    url_diffs_results = (
        utils.settings_functional_test.get("tests", "path_file_saved")
        + f"diffs_results.csv"
    )
    # we print a final message about the resulting summary file
    print(f"{'='*50}\nA summary of all diffs can be found in {url_diffs_results}")
    with open(url_diffs_results, "w") as f:
        f.write("\n".join(output))
