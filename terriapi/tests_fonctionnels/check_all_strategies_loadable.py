﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import asyncio
import json
import random
import re
import subprocess
import time
from pathlib import Path

import asyncpg
import pytest
import requests
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait
from webdriver_manager.firefox import GeckoDriverManager

from terriapi import controller, settings

# generate random string of 30 characters
authorized_chars = (
    "abcdefghijklmnopqrstuvwxyz01234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ_*-+?!@#$%^&*()"
)
PASSWORD = "".join(random.choice(authorized_chars) for i in range(30))
MAIL = "this_is_a_test@terristory.fr"

checked_strategies_path = Path("output/check_strategies.json")
if checked_strategies_path.exists():
    with open(checked_strategies_path, "r") as f:
        already_checked = json.load(f)
else:
    with open(checked_strategies_path, "w") as f:
        json.dump({}, f)
    already_checked = {}
print("already_checked", already_checked)
print("already_checked.keys()", already_checked.keys())

REGION = "occitanie"
REGION = "paysdelaloire"
REGION = "bretagne"
REGION = "auvergne-rhone-alpes"

CHECK_ALL = False


@pytest.fixture
async def env_TestStrategies():
    # connexion à la base de données, sinon impossible d'utiliser fetch.
    loop = asyncio.get_event_loop()
    controller.db = await asyncpg.create_pool(
        database=settings.get("api", "pg_name"),
        host=settings.get("api", "pg_host"),
        user=settings.get("api", "pg_user"),
        password=settings.get("api", "pg_password"),
        port=settings.get("api", "pg_port"),
        min_size=int(settings.get("api", "pg_max_connection")),
        max_size=int(settings.get("api", "pg_max_connection")),
        loop=loop,
        server_settings={"application_name": "terriapi"},
    )
    await controller.fetch("DELETE FROM utilisateur WHERE mail = $1", MAIL)
    # création d'un utilisateur pour le test.
    bashCommand = (
        "terriapi-user --mail "
        + MAIL
        + " --prenom Foo --nom Bar --territoire 'Territory' --password '"
        + PASSWORD
        + "' --fonction 'Chargé de mission' --region "
        + REGION
        + " --actif --profil admin --organisation 'Foo bar team'"
    )
    subprocess.run(bashCommand, shell=True)

    await controller.execute(
        "UPDATE utilisateur SET global_admin = true WHERE mail = $1", MAIL
    )
    await controller.execute(
        "UPDATE utilisateur_scenario SET partage = array_append(partage, $1) WHERE region = $2 AND NOT($1 = ANY(partage))",
        MAIL,
        REGION,
    )
    await controller.execute(
        "UPDATE utilisateur_scenario SET partage = $1 WHERE region = $2 AND partage IS NULL",
        [MAIL],
        REGION,
    )
    ids = await controller.fetch(
        "SELECT id, titre, zone_type, zone_id FROM utilisateur_scenario WHERE region = $2 AND $1 = ANY(partage)",
        MAIL,
        REGION,
    )
    yield [dict(i) for i in ids]
    await controller.execute(
        "UPDATE utilisateur_scenario SET partage = ARRAY_REMOVE(partage, $1) WHERE region = $2 AND $1 = ANY(partage)",
        MAIL,
        REGION,
    )
    await controller.execute(
        """
        delete from utilisateur
        where mail = $1;
    """,
        MAIL,
    )


def save_current_state(path, content):
    with open(path, "w") as f:
        json.dump(content, f, indent=4)


class TestAllStrategies:
    async def test_all_strategies_loadable(self, env_TestStrategies):
        list_strategies = env_TestStrategies
        print("list_strategies", list_strategies)

        # connexion à l'application terristory

        opts = webdriver.FirefoxOptions()
        opts.add_argument("--width=1920")
        opts.add_argument("--height=1080")
        self.browser = webdriver.Firefox(
            service=Service(GeckoDriverManager().install()), options=opts
        )

        self.browser.get("http://localhost:3000")

        # WebDriverWait attend que la balise soit présente pour passer à la suite du script. Si après le temps imparti, la balise n'est toujours pas sur l'interface, une erreur est levée.
        WebDriverWait(self.browser, 5).until(
            EC.presence_of_element_located((By.CLASS_NAME, "btn-login"))
        )
        # recherche un element par le nom de sa classe
        login_bouton = self.browser.find_element(by=By.CLASS_NAME, value="btn-login")
        # clique sur l'élément
        login_bouton.click()
        email = self.browser.find_element(by=By.ID, value="login")
        mdp = self.browser.find_element(by=By.ID, value="password")
        # rempli les éléments
        email.send_keys(MAIL)
        mdp.send_keys(PASSWORD)
        connexion = self.browser.find_element(by=By.ID, value="se-connecter")
        connexion.click()

        for strategy in list_strategies:
            print("Tries strategy ", strategy)
            print("-" * 40)
            if already_checked.get(str(strategy["id"]), False) and (
                not CHECK_ALL
                or already_checked.get(str(strategy["id"]), False).startswith(
                    "Succeeded"
                )
            ):
                print("Scenario already checked", strategy)
                continue
            print("\t ...Checking", strategy)
            self.browser.get(
                f"http://localhost:3000/edition_strategie/{strategy['id']}"
            )

            WebDriverWait(self.browser, 15).until(
                EC.presence_of_element_located((By.XPATH, "//span[text()='Impacts']"))
            )

            go_to_impact = self.browser.find_element(by=By.ID, value="react-tabs-4")
            go_to_impact.click()
            time.sleep(3)

            try:
                error = self.browser.find_element(
                    by=By.CLASS_NAME, value="alert-danger"
                )
                print("error", error)
                print("-" * 40)
                print("\n\n\n\n\n\n\n")
                already_checked[strategy["id"]] = (
                    "Input error in scenario " + json.dumps(strategy)
                )
                save_current_state(checked_strategies_path, already_checked)
                continue
            except NoSuchElementException:
                pass

            WebDriverWait(self.browser, 5).until(
                EC.presence_of_element_located((By.TAG_NAME, "details"))
            )
            time.sleep(1)
            actions_activated = self.browser.find_element(
                by=By.TAG_NAME, value="details"
            )
            nb_actions = re.search(
                r"Actions activées \((\d+)/(\d+)\)", actions_activated.text.strip()
            )
            nb_actions_activated = int(nb_actions.group(1))

            if nb_actions_activated == 0:
                already_checked[strategy["id"]] = (
                    "No action enabled in scenario " + json.dumps(strategy)
                )
                save_current_state(checked_strategies_path, already_checked)
                continue

            WebDriverWait(self.browser, 15).until(
                EC.element_to_be_clickable(
                    (By.XPATH, "//button[text()='Calculer les impacts']")
                )
            ).click()

            try:
                WebDriverWait(self.browser, 60).until(
                    EC.presence_of_element_located((By.CLASS_NAME, "tscard"))
                )
            except TimeoutException:
                print("Error in scenario", strategy)
                already_checked[strategy["id"]] = (
                    "Couldn't compute results " + json.dumps(strategy)
                )
                save_current_state(checked_strategies_path, already_checked)
                continue

            try:
                error = self.browser.find_element(
                    by=By.CLASS_NAME, value="alert-warning"
                )
                print("Error in scenario", strategy)
                already_checked[strategy["id"]] = "Error in scenario " + json.dumps(
                    strategy
                )
            except NoSuchElementException:
                print("Succeeded to load scenario", strategy)
                already_checked[strategy["id"]] = (
                    "Succeeded to load scenario " + json.dumps(strategy)
                )
                save_current_state(checked_strategies_path, already_checked)

        self.browser.close()
