# Exécution des tests fonctionnels

Deux catégories de tests sont proposés ici :

* les récupérations de données d'API pour comparaison avant et après MAJ
* les captures d'écran automatiques pour comparaison avant et après MAJ

## Comparaisons des résultats de l'API

Pour voir un exemple, on pourra se référer au procédé de déploiement de TerriSTORY qui intègre déjà ces outils. Par exemple, dans le fichier /.gitlab-ci.yml avec les étapes **test_data_api_dev** et **check_data_api_dev**.

## Captures d'écran pour contrôle visuel des résultats

Pour ce faire, il faut se placer dans le dossier courant et exécuter les commandes suivantes,
en se plaçant sur une machine avec une interface graphique et dans un environnement virtuel 
avec les librairies de développement installées en Python (selenium notamment) :

* Avant la MAJ (en ne touchant pas à la fenêtre firefox qui s'ouvre automatiquement) :

```bash
python screenshots.py --state before
```

* Après la MAJ :

```bash
python screenshots.py --state after
python compare_screenshots.py 
```

Cela va créer un fichier **comparaison.html** dans le dossier *output/* avec une visualisation des 
captures d'écran avant et après la MAJ.

Les urls utilisées sont configurables dans le fichier **urls.py** dans ce même dossier.