﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import json

import jinja2
import pandas as pd
import slugify

before_data = pd.read_csv("output/before_summary.csv", sep=";")
after_data = pd.read_csv("output/after_summary.csv", sep=";")


env_names = set(before_data["Environnement"].unique()).union(
    set(after_data["Environnement"].unique())
)

for env in env_names:
    screenshots_names = set(before_data["Nom"].unique()).union(
        set(after_data["Nom"].unique())
    )
    screenshots = []
    for screenshot in screenshots_names:
        before_ = before_data.loc[
            (before_data["Nom"] == screenshot) & (before_data["Environnement"] == env)
        ]
        after_ = after_data.loc[
            (after_data["Nom"] == screenshot) & (after_data["Environnement"] == env)
        ]
        if len(before_) == 0 and len(after_) == 0:
            continue
        assert len(before_) + len(after_) < 3

        if len(before_) == 0:
            reference = after_
        else:
            reference = before_

        env = reference.loc[:, "Environnement"].iloc[0]
        root = reference.loc[:, "Racine"].iloc[0]
        url = reference.loc[:, "URL finale"].iloc[0]

        before_file = before_.loc[:, "Fichier sortant"].iloc[0].replace("output/", "")
        after_file = after_.loc[:, "Fichier sortant"].iloc[0].replace("output/", "")

        before_nb_tries = before_.loc[:, "Nombre d'essais"].iloc[0]
        after_nb_tries = after_.loc[:, "Nombre d'essais"].iloc[0]

        scrolling = reference.loc[:, "Scrolling sur la page"].iloc[0]
        if scrolling != "[]":
            heights = json.loads(scrolling)
        else:
            heights = []

        actions = reference.loc[:, "Actions"].iloc[0]
        if actions != "[]":
            actions_performed = json.loads(actions)
        else:
            actions_performed = []

        screenshots.append(
            {
                "name": screenshot,
                "env": env,
                "root_url": root,
                "root_url": root,
                "url": url,
                "before_file": before_file,
                "after_file": after_file,
                "before_nb_tries": before_nb_tries,
                "after_nb_tries": after_nb_tries,
                "scrollings": heights,
                "actions_performed": actions_performed,
            }
        )

    environment = jinja2.Environment(loader=jinja2.FileSystemLoader("."))
    template = environment.get_template("template_comparison_screenshots.html")
    content = template.render(screenshots=screenshots)
    with open(
        f"output/comparaison_{slugify.slugify(env)}.html", mode="w", encoding="utf-8"
    ) as message:
        message.write(content)
        print(f"... wrote comparison file.")
