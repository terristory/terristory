﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import argparse
import sys

import pandas as pd
import utils


def _parse_args(args):
    parser = argparse.ArgumentParser(description="Récupération des données d'une API")
    parser.add_argument(
        "--env",
        type=str,
        required=False,
        default=None,
        choices=("dev", "test", "prod", "local"),
        help="Environnement testé",
    )
    parser.add_argument(
        "--state",
        type=str,
        required=False,
        default="before",
        choices=("before", "after"),
        help="Etat actuel de l'API",
    )
    args = parser.parse_args()
    return args


def retrieve_regions_list(env="dev"):
    """
    Retrieve the list of regions from API

    Parameters
    ----------
    env : str, optional
        environment, by default "dev"

    Returns
    -------
    list
        the regions available
    """
    url = utils.regionalize(env, utils.REGIONS_LIST_URL)
    url = url.replace("://.", "://")
    regions_list = utils.call_api(
        url, auth=utils.SERVERS_CONFIG["auths"].get(env, None)
    )
    return [
        {key: region[key] for key in ["id", "env", "label", "url"]}
        for region in regions_list
        if not region.get("is_national", False)
    ]


def retrieve_region_details(region, region_url, env="dev"):
    """
    Retrieve the detail of a region from API

    Parameters
    ----------
    region : str
        region considered
    region_url : str
        region url considered
    env : str, optional
        environment, by default "dev"

    Returns
    -------
    list
        the regions available
    """
    url = utils.regionalize(env, utils.REGION_DETAIL_URL, region, region_url)
    region_detail = utils.call_api(
        url, auth=utils.SERVERS_CONFIG["auths"].get(env, None)
    )
    if isinstance(region_detail, list):
        return region_detail[0]
    # TODO: remove this as it is deprecated and only for transition
    else:
        return region_detail["features"][0]["properties"]


def retrieve_data_api(region, region_url, region_id, env="dev", state="before"):
    """
    Tests the API consistency between two different websites on the list of analysis
    available for a specific region

    Parameters
    ----------
    region : str
        the region to test
    """
    user_id = 0
    if env not in utils.SERVERS_CONFIG.get("root_urls", []):
        raise ValueError(
            "Env is not correct (allowed: "
            + ", ".join(utils.SERVERS_CONFIG.get("root_urls", []))
        )

    # we first retrieve user id and analyses list
    user_id = utils.call_api(
        utils.regionalize(
            env, utils.URLS_TO_TEST["utilisateur"], region, region_url, region_id
        ),
        auth=utils.SERVERS_CONFIG["auths"].get(env, None),
    )["id_utilisateur"]

    env_analysis = utils.call_api(
        utils.regionalize(
            env, utils.URLS_TO_TEST["analyses_list"], region, region_url, region_id
        ),
        auth=utils.SERVERS_CONFIG["auths"].get(env, None),
    )

    urls = []
    # we then call both APIs and compare them
    for analysis in env_analysis:
        analysis_data = analysis["data"]
        # if both env have the analysis available
        # we use env ID for saving files
        id_for_file = analysis["id"]

        if analysis["type"] == "wms_feed":
            continue

        if utils.VERBOSE:
            print("Testing...", analysis_data, "with env", env)

        url_map = utils.retrieve_api_data_for_analysis(
            analysis["id"],
            user_id,
            env,
            region,
            region_url,
            region_id,
            f"{state}_{id_for_file}",
            auth=utils.SERVERS_CONFIG["auths"].get(env, None),
        )
        if utils.VERBOSE:
            print(f"\t ...retrieved data for {env}")
        urls.append(
            {
                "state": state,
                "env": env,
                "region": region,
                "region_url": region_url,
                "region_id": region_id,
                "analysis_id": analysis["id"],
                "analysis_nom": analysis["nom"],
                "analysis_data": analysis["data"],
                "analysis_years": analysis["years"],
                "user_id": user_id,
                "file_url": url_map,
            }
        )
    return urls


if __name__ == "__main__":
    args = _parse_args(sys.argv[1:])
    env = args.env
    state = args.state
    if env is None:
        env = utils.settings_functional_test.get("tests", "env")
    regions = retrieve_regions_list(env)

    files = []
    for region in regions:
        region_name = region["id"]
        region_url = region["url"]
        if region_url in utils.settings_functional_test.get(
            "tests", "regions_disabled"
        ).split(","):
            continue
        region_detail = retrieve_region_details(region_name, region_url, env)
        region_id = region_detail["code"]
        if utils.VERBOSE:
            print(region["label"])
        files += retrieve_data_api(
            region_name, region_url, region_id, env=env, state=state
        )

    url_files_saved = (
        utils.settings_functional_test.get("tests", "path_file_saved")
        + f"files_saved_{state}.csv"
    )
    pd.DataFrame(files).to_csv(url_files_saved)
