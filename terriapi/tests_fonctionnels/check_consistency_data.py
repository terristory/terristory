﻿# TerriSTORY®
#
# Copyright © 2022 AURA-EE
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# A copy of the GNU Affero General Public License should be present along
# with this program at the root of current repository. If not, see
# http://www.gnu.org/licenses/.

import utils
from tests_fonctionnels import SERVERS_CONFIG, URLS_TO_TEST, VERBOSE


def compare_two_apis(env_1="dev", env_2="prod"):
    """
    Tests the API consistency between two different websites on the list of analysis
    available for a specific region

    Parameters
    ----------
    region : str
        the region to test
    """
    region = "auvergne-rhone-alpes"
    user_ids = {}
    analyses_list = {}
    if env_1 not in SERVERS_CONFIG.get(
        "root_urls", []
    ) or env_2 not in SERVERS_CONFIG.get("root_urls", []):
        raise ValueError(
            "Env is not correct (allowed: "
            + ", ".join(SERVERS_CONFIG.get("root_urls", []))
        )

    # we first retrieve user id and analyses list
    for env in [env_1, env_2]:
        user_ids[env] = utils.call_api(
            utils.regionalize(
                env, URLS_TO_TEST["utilisateur"], region, "auvergnerhonealpes", "84"
            ),
            auth=SERVERS_CONFIG["auths"].get(env, None),
        )["id_utilisateur"]

        env_analysis = utils.call_api(
            utils.regionalize(
                env, URLS_TO_TEST["analyses_list"], region, "auvergnerhonealpes", "84"
            ),
            auth=SERVERS_CONFIG["auths"].get(env, None),
        )
        for analysis in env_analysis:
            # initializes the available env related
            if analysis["data"] not in analyses_list:
                analyses_list[analysis["data"]] = {}
            analyses_list[analysis["data"]][env] = analysis

    # we then call both APIs and compare them
    for analysis_data, analysis in analyses_list.items():
        # if both env have the analysis available
        if env_1 in analysis and env_2 in analysis:
            # we use env_2 IDs for saving files
            id_for_file = analysis[env_2]["id"]

            if VERBOSE:
                print(
                    "Testing...",
                    analysis_data,
                    "with envs",
                    " - ".join(analysis.keys()),
                )

            url_map_1 = utils.retrieve_api_data_for_analysis(
                analysis[env_1]["id"],
                user_ids[env_1],
                env_1,
                "auvergne-rhone-alpes",
                "auvergnerhonealpes",
                "84",
                id_for_file,
                auth=SERVERS_CONFIG["auths"].get(env_1, None),
            )
            if VERBOSE:
                print(f"\t ...retrieved data for {env_1}")

            url_map_2 = utils.retrieve_api_data_for_analysis(
                analysis[env_2]["id"],
                user_ids[env_2],
                env_2,
                "auvergne-rhone-alpes",
                "auvergnerhonealpes",
                "84",
                id_for_file,
                auth=SERVERS_CONFIG["auths"].get(env_2, None),
            )
            if VERBOSE:
                print(f"\t ...retrieved data for {env_2}")

            # after getting the data, we compare them
            diffs = utils.check_consistency_files(url_map_1, url_map_2)
            i = 0
            for diff in diffs:
                if i == 0 and diff == True:
                    break
                i += 1
            # if we have more than zero diff, save it somewhere
            if i > 0:
                # do something with diffs
                if VERBOSE:
                    print(f"\t ... {i} ERRORS !")
                    print(f"\t ... `meld {url_map_1} {url_map_2}` ")


if __name__ == "__main__":
    compare_two_apis()
