# TerriSTORY

<img src="graphics/logo_terristory.jpg"  width="350px" />

[![pipeline status](https://gitlab.com/terristory/terristory/badges/master/pipeline.svg)](https://gitlab.com/terristory/terristory/-/commits/master)
[![coverage report](https://gitlab.com/terristory/terristory/badges/master/coverage.svg?key_text=back+coverage&key_width=100&job=test_api)](https://gitlab.com/terristory/terristory/-/commits/master)
[![coverage report](https://gitlab.com/terristory/terristory/badges/master/coverage.svg?key_text=front+coverage&key_width=100&job=test_front)](https://gitlab.com/terristory/terristory/-/commits/master)

Le code source de l'application TerriSTORY® est stocké sur <a href="https://gitlab.com/terristory/terristory/" target="_blank">un dépôt Gitlab</a>.

## Contexte

TerriSTORY® est un **outil partenarial d’aide au pilotage de la transition des territoires**, co-construit avec les territoires. Il rassemble une vingtaine d’acteurs nationaux et régionaux ayant une mission de service public ou d’intérêt général.

Interface de **visualisation interactive de données opendata multithématiques** (énergie, climat, mobilité, économie...), TerriSTORY® permet de mieux comprendre son territoire, d'identifier les leviers d’actions prioritaires et de simuler des scenarios prospectifs. À **vocation pédagogique**, il constitue un support de dialogue entre acteurs.

Pour tout savoir sur TerriSTORY®, rendez-vous sur le site <a href="https://terristory.fr" target="_blank">terristory.fr</a>.

## Documentation

La documentation du projet est disponible sur <a href="https://docs.terristory.fr/" target="_blank">https://docs.terristory.fr/</a>.

## Licence 

Ce code est rendu disponible sous la licence publique générale GNU Affero (ou aGPL) version 3. Vous pouvez vous référer au fichier [LICENSE](LICENSE.md) pour le contenu de la licence. 

## Contribuer au projet

Vous pouvez contribuer à faire évoluer TerriSTORY® de plusieurs façons :

- En signalant tout problème, bug, comportement inhabituel ou non désirable sur <a href="https://gitlab.com/terristory/terristory/-/issues/new?issuable_template=bug_report" target="_blank">ce lien</a> ;
- En proposant une piste d'amélioration ou une nouvelle fonctionnalité sur <a href="https://gitlab.com/terristory/terristory/-/issues/new?issuable_template=new_feature" target="_blank">ce lien</a>.

Attention, avant de signaler un bug ou de proposer une amélioration, pensez à bien vous assurer qu'un autre ticket n'a pas déjà été créé dans la <a href="https://gitlab.com/terristory/terristory/-/issues" target="_blank">base disponible sur gitlab</a> pour le même problème ou la même idée.

### Si vous êtes développeur

Les pages suivantes de la documentation technique peuvent vous intéresser :

- le <a href="https://docs.terristory.fr/installation.html" target="_blank">guide d'installation</a>
- la <a href="https://docs.terristory.fr/notice_contribution.html" target="_blank">notice de contribution</a>
- la <a href="https://docs.terristory.fr/architecture_logicielle.html" target="_blank">description technique du système</a>
- quelques <a href="https://docs.terristory.fr/tools.html" target="_blank">outils pratiques</a>

Si vous souhaitez soumettre vos modifications ou vos ajouts au projet principal de TerriSTORY®, il vous faudra passer par une *fork* puis par une *demande de fusion* ou *pull request*. Dans ce cas, vous serez contacté(e) par AURA-EE, propriétaire du code source de la plateforme, pour procéder à la signature :

- d'un document certifiant que vous êtes bien à l'origine du code et que vous disposez des droits nécessaires. Ce document, appelé *Developer Certificate of Origin* (DCO), est accessible à l'[adresse suivante](docs/legal/dco.md).
- d'un document dans lequel vous cédez la propriété de votre code au propriétaire de TerriSTORY®, AURA-EE. Ce document est accessible à l'[adresse suivante](docs/legal/cla.md).

### Architecture technique

![](graphics/archi.png)

### Application TerriAPI

Se référer au [fichier README dédié](terriapi/README.md).

### Application WEB

Se référer au [fichier README dédié](front/README.md).

### Outillage de configuration et déploiement serveur

Se référer au <a href="https://gitlab.com/terristory/terristory-ansible/" target="_blank">dépot dédié</a>.
