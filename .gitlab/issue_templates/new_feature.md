## Résumé

(Résumez de façon concise la proposition de fonctionnalité)

## Quel est le comportement souhaité ?

(Détaillez les étapes d'utilisation ou le rendu que devrait avoir la fonctionnalité dans la démarche utilisateur)

## Éléments complémentaires

(Insérez ici tout document supplémentaire qui faciliterait la compréhension - images, documents, cahiers des charges, références, etc.)

/label ~A_chiffrer
