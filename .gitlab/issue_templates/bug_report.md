## Résumé

(Résumez de façon concise le bug rencontré)

## Étapes de reproduction du problème

(Comment le problème peut être reproduit - ceci est très important pour une bonne résolution)

## Quel est le comportement actuel ?

(Ce qui se passe réellement)

## Quel est le comportement correct attendu ?

(Ce que vous devriez voir à la place)

## Captures d'écran ou autres éléments

(Insérez ici tout document supplémentaire qui faciliterait la compréhension - captures d'écran, données utilisées pour reproduire le bug, etc.)

/label ~BUG ~A_chiffrer
