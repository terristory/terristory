/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

// add Vitest functions here globally
import { expect, afterEach } from "vitest";
import { cleanup } from "@testing-library/react";
import * as matchers from "@testing-library/jest-dom/matchers";

// Extend Vitest's expect method with methods from react-testing-library
expect.extend(matchers);

// Mock window.print function
beforeAll(() => {
    window.print = vi.fn(); // Mock the print function
    Object.defineProperty(window, "location", {
        writable: true,
        value: {
            ...window.location,
            href: "",
            assign: vi.fn(),
            replace: vi.fn(),
        },
    });

    // Mock history.pushState and history.replaceState
    window.history.pushState = vi.fn();
    window.history.replaceState = vi.fn();
});

// Run cleanup after each test case (e.g., clearing jsdom)
afterEach(() => {
    cleanup();
});
