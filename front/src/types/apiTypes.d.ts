/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
interface DataPoint {
    annee: number;
    valeur: number;
}

interface CategoryOptionHistoryData {
    nom: string;
    couleur: string;
    confidentiel: "oui" | "non";
    data: DataPoint[];
}

interface CategoryHistoryData {
    traj: CategoryOptionHistoryData[];
    categories: CategoryOptionHistoryData[];
    evolution: boolean;
}

interface Analyse {
    [categoryName: string]: CategoryHistoryData;
}

/**
 * Format for API endpoint : api/<region>/analysis/<id>/graphique/bar?...
 */
export interface IndicatorHistoryDataApiResponse {
    analyse: Analyse;
    unite: string;
}

/**
 *  A "chart" config for an indicator from the meta.chart table.
 */
export interface IndicatorChartConfig {
    indicateur: number;
    titre: string;
    description: string | null;
    type: ChartType;
    categorie: string;
    visible: boolean;
    ordre: number | null;
    data_type: string | null;
    is_single_select: boolean;
    datasets: Object[];
    chart_options: ChartOptionsByType<IndicatorChartConfig["type"]>;
}

export type ChartType =
    | "pie"
    | "hbar"
    | "barline"
    | "histogramme"
    | "slider"
    | "selection"
    | "switch-button"
    | "history";

export type ChartOptionsByType<T extends ChartType> = T extends "history"
    ? HistoryChartOptions
    : Record<string, never>;

export interface HistoryChartOptions {
    type: "line" | "bar";
    stacked: boolean;
}
