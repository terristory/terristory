"use strict";

// Require the original module to not be mocked...
const originalModule = await vi.importActual("../utils.js");

module.exports = {
    __esModule: true, // Use it when dealing with esModules
    ...originalModule,
    exportToCSV: vi.fn(() => {}),
    consulteAutrePage: vi.fn(() => {}),
};
