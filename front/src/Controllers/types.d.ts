/**
 * The key should be "category_name" or "<___all_category_name"
 * The filtre_categorie string value should be "<category_name>.<value>"
 */
export interface IndicatorCategoriesValues {
    [key: string]: Array<{ filtre_categorie: string }>;
}
