/**
 * Mock the API tool, used to call backend API. No URL is really called and results for
 * each URL can be mocked.
 *
 * ## How to use?
 *
 *   >     import Api from "../../Controllers/Api";
 *   >     vi.mock("../../Controllers/Api.js");
 *
 * Then we create mock results
 *
 *   > const MOCK_URLS_RESULTS = [
 *   >    {
 *   >        url: "this is an URL",
 *   >        results: mixedResult,
 *   >    },
 *   > ];
 *
 * Here, `mixedResult` can be either an object or array, or a callable that takes
 * two parameters (the data sent to the URL) to compute the results.
 *
 * And we need to add the results
 *
 *   >     beforeEach(() => {
 *   >         Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
 *   >     });
 */

"use strict";

// Require the original module
const Api = await vi.importMock("../Api.js");

// This is a custom function that our tests can use during setup to specify
// what the url used in callApi will give as results
// cf. https://jestjs.io/docs/manual-mocks#examples
let mockUrlsResults = [];
const __setMockUrlsResults = vi.fn().mockImplementation((newMockUrlsResults) => {
    mockUrlsResults = [];
    newMockUrlsResults.forEach((urlData) => {
        const { url, results, fail } = urlData;
        if (results === undefined && !fail) console.error("fill data from url : ", url);
        mockUrlsResults.push({ url: url, results: results, fail: fail });
    });
});

// the real function will not call an API anymore but instead look up the
// urls available in mockUrlsResults to check if url asked match any.
// If that's the case, will resolve the Promise with the results associated.
// Otherwise, will just reject the Promise.
const callApi = vi.fn().mockImplementation((url, data, type) => {
    const promise = new Promise((resolve, reject) => {
        process.nextTick(() => {
            mockUrlsResults.forEach((element) => {
                if (url.startsWith(element.url)) {
                    // we authorize functions
                    // i.e., results can be a function that uses data and type object
                    // to compute real results
                    const results =
                        element.results instanceof Function
                            ? element.results(data, type)
                            : element.results;
                    if (element.fail) {
                        reject(results);
                    } else {
                        resolve(results);
                    }
                    return;
                }
            });
            reject({});
        });
    });
    promise.abort = () => {};
    return promise;
});

// we export the mocked module
Api.__setMockUrlsResults = __setMockUrlsResults;
Api.callApi = callApi;

export default Api;
