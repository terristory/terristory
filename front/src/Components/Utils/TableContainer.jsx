/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React from "react";
import {
    useReactTable,
    flexRender,
    getCoreRowModel,
    getPaginationRowModel,
    getSortedRowModel,
    getFilteredRowModel,
} from "@tanstack/react-table";
import "bootstrap/dist/css/bootstrap.min.css";

import styles from "../../style/TableContainer.module.css";

/**
 * @param {Object} props
 * @param {import("@tanstack/react-table").ColumnDef<any>[]} props.columns
 * @param {any[]} props.data
 * @param {(row: import("@tanstack/react-table").Row) => object} [props.getTrProps]
 * @param {(row: import("@tanstack/react-table").Cell) => object} [props.getCellProps]
 * @param {number} [props.defaultPageSize=10]
 * @param {string} [props.tableClassName] Additional CSS classes to be applied to the table element.
 * @param {string} [props.containerClassName]  Additional CSS classes to be applied to the container wrapping the table element.
 *
 *
 * @returns
 */
const TableContainer = (props) => {
    const { columns, data } = props;

    const [columnFilters, setColumnFilters] = React.useState([]);
    const [pagination, setPagination] = React.useState({
        pageIndex: 0,
        pageSize: props.defaultPageSize ?? 10,
    });

    let config = {
        columns,
        data: data ?? [],
        getFilteredRowModel: getFilteredRowModel(), //client side filtering
        onColumnFiltersChange: setColumnFilters,
        getCoreRowModel: getCoreRowModel(),
        getSortedRowModel: getSortedRowModel(), //order doesn't matter anymore!
        state: {
            columnFilters,
        },
    };
    if (!props.disablePagination) {
        config.state = {
            ...config.state,
            pagination,
        };
        config = {
            ...config,
            getPaginationRowModel: getPaginationRowModel(),
            onPaginationChange: setPagination,
        };
    }

    let pageSizesList = props.pageSizesList ?? [10, 20, 50];
    if (props.defaultPageSize && !pageSizesList.includes(props.defaultPageSize)) {
        pageSizesList.push(props.defaultPageSize);
        pageSizesList.sort();
    }

    const tableInstance = useReactTable(config);

    return (
        // If you're curious what props we get as a result of calling our getter functions (getTableProps(), getRowProps())
        // Feel free to use console.log()  This will help you better understand how react table works underhood.
        <>
            <div className={`table-responsive ${props.containerClassName}`}>
                <table
                    className={`table table-borderless ${props.tableClassName} ${styles.TableContainer}`}
                >
                    <thead>
                        {tableInstance.getHeaderGroups().map((headerGroup) => (
                            <tr key={"tr-h-" + headerGroup.id}>
                                {headerGroup.headers.map((header) => (
                                    <th scope="col" key={"th-h-" + header.id}>
                                        {header.isPlaceholder ? null : (
                                            <>
                                                <div
                                                    {...{
                                                        className:
                                                            header.column.getCanSort()
                                                                ? `cursor-pointer select-none ${styles.TableHeader}`
                                                                : styles.TableHeader,
                                                        onClick:
                                                            header.column.getToggleSortingHandler(),
                                                    }}
                                                >
                                                    {flexRender(
                                                        header.column.columnDef.header,
                                                        header.getContext()
                                                    )}
                                                    {{
                                                        asc: " ↑",
                                                        desc: " ↓",
                                                    }[header.column.getIsSorted()] ??
                                                        null}
                                                </div>
                                                {header.column.getCanFilter() ? (
                                                    <div>
                                                        <Filter
                                                            column={header.column}
                                                        />
                                                    </div>
                                                ) : null}
                                            </>
                                        )}
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </thead>

                    <tbody>
                        {(!data || data.length === 0) && (
                            <tr>
                                <td
                                    colSpan={columns.length}
                                    className={styles.EmptyTable}
                                >
                                    Aucune donnée disponible.
                                </td>
                            </tr>
                        )}
                        {tableInstance.getRowModel().rows.map((row) => {
                            let style = {};
                            if (props.getTrProps) {
                                style = props.getTrProps(row);
                            }
                            return (
                                <tr key={"tr-r-" + row.id} style={style}>
                                    {row.getVisibleCells().map((cell) => {
                                        let style = {};
                                        if (props.getCellProps) {
                                            style = props.getCellProps(cell);
                                        }
                                        return (
                                            <td key={"td-r-" + cell.id} style={style}>
                                                {flexRender(
                                                    cell.column.columnDef.cell,
                                                    cell.getContext()
                                                )}
                                            </td>
                                        );
                                    })}
                                </tr>
                            );
                        })}
                    </tbody>
                    <tfoot>
                        {tableInstance.getFooterGroups().map((footerGroup) => (
                            <tr key={footerGroup.id}>
                                {footerGroup.headers.map((header) => (
                                    <th key={header.id}>
                                        {header.isPlaceholder
                                            ? null
                                            : flexRender(
                                                  header.column.columnDef.footer,
                                                  header.getContext()
                                              )}
                                    </th>
                                ))}
                            </tr>
                        ))}
                    </tfoot>
                </table>
            </div>

            {!props.disablePagination && (
                <div className={styles.Pagination}>
                    <nav>
                        <ul className="pagination pagination-lg justify-content-center">
                            <li
                                className={
                                    "page-item" +
                                    (!tableInstance.getCanPreviousPage() && " disabled")
                                }
                            >
                                <span
                                    className="page-link"
                                    onClick={() => tableInstance.firstPage()}
                                >
                                    &laquo;
                                </span>
                            </li>
                            <li
                                className={
                                    "page-item" +
                                    (!tableInstance.getCanPreviousPage() && " disabled")
                                }
                            >
                                <span
                                    className="page-link"
                                    onClick={() => tableInstance.previousPage()}
                                >
                                    &lsaquo;
                                </span>
                            </li>
                            <li className={`page-item ${styles.PageNumber}`}>
                                <strong>
                                    {tableInstance.getState().pagination.pageIndex + 1}{" "}
                                    sur {tableInstance.getPageCount().toLocaleString()}
                                </strong>
                            </li>
                            <li className={`page-item ${styles.GoToPage}`}>
                                Aller à la page
                                <input
                                    type="number"
                                    min="1"
                                    max={tableInstance.getPageCount()}
                                    defaultValue={
                                        tableInstance.getState().pagination.pageIndex +
                                        1
                                    }
                                    onChange={(e) => {
                                        const page = e.target.value
                                            ? Number(e.target.value) - 1
                                            : 0;
                                        tableInstance.setPageIndex(page);
                                    }}
                                    className="border p-1 rounded w-16"
                                    disabled={tableInstance.getPageCount() === 1}
                                />
                                <select
                                    defaultValue={
                                        props.defaultPageSize ??
                                        tableInstance.getState().pagination.pageSize
                                    }
                                    onChange={(e) => {
                                        tableInstance.setPageSize(
                                            Number(e.target.value)
                                        );
                                    }}
                                >
                                    {pageSizesList.map((pageSize) => (
                                        <option key={pageSize} value={pageSize}>
                                            Afficher {pageSize}
                                        </option>
                                    ))}
                                </select>
                            </li>
                            <li
                                className={
                                    "page-item" +
                                    (!tableInstance.getCanNextPage() && " disabled")
                                }
                            >
                                <span
                                    className="page-link"
                                    onClick={() => tableInstance.nextPage()}
                                >
                                    &rsaquo;
                                </span>
                            </li>
                            <li
                                className={
                                    "page-item" +
                                    (!tableInstance.getCanNextPage() && " disabled")
                                }
                            >
                                <span
                                    className="page-link"
                                    onClick={() => tableInstance.lastPage()}
                                >
                                    &raquo;
                                </span>
                            </li>
                        </ul>
                    </nav>
                </div>
            )}
        </>
    );
};

function Filter({ column }) {
    const columnFilterValue = column.getFilterValue();

    return (
        <input
            type="text"
            className="w-36 border rounded"
            placeholder={`...`}
            value={columnFilterValue ?? ""}
            onChange={(e) => column.setFilterValue(e.target.value)}
            onClick={(e) => e.stopPropagation()}
        />
    );
}

export default TableContainer;
