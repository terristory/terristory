/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect } from "react";
import TableContainer from "../Utils/TableContainer";
import { Link, useParams } from "react-router-dom";

import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import DetailsPopup from "../DetailsPopup";
import TablePreview from "./TablePreview";

import "bootstrap/dist/css/bootstrap.min.css";
import "../../style/national/dataimport.css";

const DataManageDashboard = (props) => {
    const [tables, setTables] = useState([]);
    const [showDetailsTablesElement, setShowDetailsTablesElement] = useState(false);
    const [detailsTable, setDetailsTable] = useState("");
    const { region } = useParams();

    useEffect(() => {
        const url = buildRegionUrl(config.api_national_import_metadata, region);
        Api.callApi(url, null, "GET")
            .then(setTables)
            .catch((e) => console.error(e));
    }, [region]);

    const toggleDetailsCell = (details) => {
        setShowDetailsTablesElement(!showDetailsTablesElement);
        setDetailsTable(details);
    };

    const downloadButton = (tableName) => {
        let url = buildRegionUrl(config.api_analysis_data_national_export, "national")
            .replace("#territory#", region)
            .replace("#table_name#", tableName);
        return (
            <p className="export-data-button-in-popup">
                <a
                    className="btn btn-info normal-size"
                    target="_blank"
                    rel="noreferrer"
                    href={url}
                >
                    Exporter la table en csv
                </a>
            </p>
        );
    };

    const columns = [
        {
            header: "Etat",
            accessorKey: "etat",
            cell: ({ row }) => (
                <span className="etat-import">
                    {row.original.data_exist ? (
                        <span className="bi bi-check exist"> Table valide</span>
                    ) : (
                        <span className="bi bi-x not-exist"> Table non importée</span>
                    )}
                </span>
            ),
            enableColumnFilter: false,
            enableSorting: false,
        },
        {
            header: "Nom de la table",
            accessorKey: "table_name",
        },
        {
            header: "Date de mise à jour",
            accessorKey: "date_maj",
        },
        {
            header: "Aperçu de la table",
            accessorKey: "table_preview",
            cell: ({ row }) => (
                <button
                    className="unstyled"
                    onClick={() =>
                        toggleDetailsCell(
                            <TablePreview
                                data={row.original.header}
                                additionalElements={
                                    row.original.data_exist
                                        ? downloadButton(row.original.table_key)
                                        : "Aucune donnée à afficher"
                                }
                            />
                        )
                    }
                >
                    <img
                        className="table_preview_icon"
                        src="/img/table_preview.svg"
                        alt="Visualiser une partie des données"
                        title="Visualiser une partie des données"
                    />
                </button>
            ),
            enableColumnFilter: false,
            enableSorting: false,
        },
        {
            header: "Utile pour",
            cell: ({ row }) => {
                const icons = [];
                if (
                    (row.original.requirements &&
                        "strategy" in row.original.requirements) ||
                    (row.original.optional && "strategy" in row.original.optional)
                ) {
                    icons.push(
                        <img
                            src="../../img/national/strategy.svg"
                            title={
                                "Stratégies territoriales" +
                                (!("strategy" in row.original.requirements)
                                    ? " (optionnel)"
                                    : "")
                            }
                            alt="Stratégies territoriales"
                            className={
                                "icon-requirements " +
                                (!("strategy" in row.original.requirements)
                                    ? " icon-optional"
                                    : "")
                            }
                        />
                    );
                }
                if (
                    (row.original.requirements &&
                        "simulator" in row.original.requirements) ||
                    (row.original.optional && "simulator" in row.original.optional)
                ) {
                    icons.push(
                        <img
                            src="../../img/national/simulator_mobility.svg"
                            title={
                                "Simulateur de mobilité" +
                                (!("simulator" in row.original.requirements)
                                    ? " (optionnel)"
                                    : "")
                            }
                            alt="Simulateur de mobilité"
                            className={
                                "icon-requirements " +
                                (!("simulator" in row.original.requirements)
                                    ? " icon-optional"
                                    : "")
                            }
                        />
                    );
                }
                return icons;
            },
            width: 100,
            enableColumnFilter: false,
            enableSorting: false,
        },
        {
            id: "action",
            accessorKey: "action",
            header: "Actions",
            cell: ({ row }) => {
                const params = {
                    table: row.original.table_key,
                    tables: tables,
                };
                const label = row.original.data_exist ? "Modifier" : "Importer";
                const className = row.original.data_exist ? "info" : "success";
                return (
                    <Link
                        to={{
                            pathname:
                                "/national/" +
                                props.parentApi.data.region +
                                "/import/formulaire",
                            state: { params: params },
                        }}
                        className={`tsbtn ${className}`}
                    >
                        {label}
                    </Link>
                );
            },
            enableColumnFilter: false,
        },
    ];

    return (
        <div className="panel panel-default centered-row data-manage-dashboard">
            <div>
                <Link
                    className="btn btn-light return-button"
                    to={"/national/" + props.parentApi.data.region + "/portail"}
                >
                    <i className="bi bi-arrow-left"></i>
                </Link>
            </div>
            {showDetailsTablesElement && (
                <DetailsPopup
                    title="Aperçu de la table"
                    content={detailsTable}
                    show={showDetailsTablesElement}
                    emptyMsg="Nothing to show"
                    callbackAfterClosing={() => toggleDetailsCell()}
                />
            )}
            <div className="panel-body user-scenarii">
                <h3 className="tstitle">Import des données</h3>
                <TableContainer
                    data={tables}
                    columns={columns}
                    tableClassName="table-striped"
                    defaultPageSize={30}
                    filterable={true}
                />
            </div>
        </div>
    );
};

export default DataManageDashboard;
