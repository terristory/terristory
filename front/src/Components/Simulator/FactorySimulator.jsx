/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import SimulatorMobility from "./Mobility/SimulatorMobility.jsx";
import SimulatorEnR from "./RenewableEnergy/SimulatorEnR.jsx";

const FactorySimulator = ({ type, ...rest }) => {
    switch (type) {
        case "mobility":
            return <SimulatorMobility {...rest} />;
        case "enr":
            return <SimulatorEnR {...rest} />;
        default:
            return (
                <div className="widgets full-screen-widget">
                    <div className="tout">
                        <div className="support">
                            <div className="corps">
                                <p>
                                    <code>Error: Unknown simulator type</code>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            );
    }
};

export default FactorySimulator;
