/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import Slider from "rc-slider";
import { Pie } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { Link } from "react-router-dom";

import Api from "../../../Controllers/Api";
import config from "../../../settings";
import configData from "../../../settings_data";
import "../../../style/simulator.css";
import {
    buildRegionUrl,
    createPdfMethodoLink,
    capitalizeFirstLetter,
} from "../../../utils";
import SEO from "../../SEO";
import {
    getResults,
    updateProductionFromPuissance,
    updatePuissanceFromProduction,
} from "./results";

import "bootstrap/dist/css/bootstrap.min.css";
import "rc-slider/assets/index.css";

function SimulatorImpact({
    impact,
    handleImpactChange,
    generatePieChartData,
    targetYear,
}) {
    const { impactType, color, impactTitle, enabled, unit, defaultValue } = impact;
    const currentValues = defaultValue?.current;
    const targetValues = defaultValue?.target;
    let name = unit ? impactTitle + " (en " + unit + ") :" : impactTitle;

    if (!enabled || !currentValues || !targetValues) {
        return (
            <div key={impactType} className="simulator-impact">
                <div>
                    <input
                        style={{ color: color }}
                        type="checkbox"
                        id={impactType}
                        name={impactType}
                        defaultChecked={enabled}
                        onClick={handleImpactChange}
                    ></input>
                    <label style={{ color: color }} htmlFor={impactType}>
                        {name}
                    </label>
                    <div className="two-columns"></div>
                </div>
            </div>
        );
    }

    return (
        <div key={impactType} className="simulator-impact">
            <div>
                <input
                    style={{ color: color }}
                    type="checkbox"
                    id={impactType}
                    name={impactType}
                    defaultChecked={enabled}
                    onClick={handleImpactChange}
                ></input>
                <label style={{ color: color }} htmlFor={impactType}>
                    {name}
                </label>
                <div className="two-columns">
                    <div className="column">
                        <label className="header">Actuel</label>
                        {enabled &&
                            Object.keys(currentValues).map((key) => {
                                const value = currentValues[key];
                                const { type, label, unit, impactKey } = value;
                                let maxValue = Number(value.default?.toFixed(0));
                                let defaultVal =
                                    value.value !== undefined
                                        ? Number(value.value?.toFixed(0))
                                        : Number(value.default?.toFixed(0));

                                let handleBorder = "";
                                if (value.default === maxValue) {
                                    handleBorder = " handle-border";
                                }
                                if (value.default > maxValue) {
                                    handleBorder = " over-handle-border";
                                }
                                let defaultCurrentValue = {
                                    style: {
                                        color: "black",
                                        marginTop: "28px",
                                    },
                                    label: new Intl.NumberFormat("fr-FR").format(
                                        Math.round(value.default)
                                    ),
                                };
                                let maxValueMarks =
                                    Intl.NumberFormat("fr-FR").format(maxValue);
                                const { data, options } = generatePieChartData(
                                    "current",
                                    impactType
                                );
                                switch (type) {
                                    case "bar":
                                        // Handling slider
                                        return (
                                            <div key={key} className="impact-row">
                                                <p>{label}</p>
                                                <OverflowSlider
                                                    type={type + " " + impactKey}
                                                    handleBorder={handleBorder}
                                                    defaultVal={defaultVal}
                                                    maxValue={defaultVal}
                                                    defaultCurrentValue={
                                                        defaultCurrentValue
                                                    }
                                                    maxValueMarks={maxValueMarks}
                                                />
                                            </div>
                                        );
                                    case "text":
                                        return (
                                            <div key={key} className="impact-row">
                                                <p>{label}</p>
                                                <p className="text">
                                                    {Number(defaultVal).toFixed(0)}{" "}
                                                    {unit}
                                                </p>
                                            </div>
                                        );
                                    case "pie":
                                        // Handling chart
                                        return (
                                            <div
                                                key={key}
                                                className="impact-row"
                                                style={{
                                                    width: "190px",
                                                }}
                                            >
                                                <p>
                                                    {label} {"(" + unit + ")"}
                                                </p>
                                                <Pie
                                                    plugins={[ChartDataLabels]}
                                                    data={data}
                                                    options={options}
                                                />
                                            </div>
                                        );
                                    default:
                                        return null;
                                }
                            })}
                    </div>

                    <div className="dashed-line-horizontal"></div>

                    <div className="column">
                        <label className="header">Cible ({targetYear})</label>
                        {enabled &&
                            Object.keys(targetValues).map((key) => {
                                const value = targetValues[key];
                                const { type, label, unit, impactKey } = value;
                                let maxValue = Number(value.default?.toFixed(0));
                                let defaultVal =
                                    value.value !== undefined
                                        ? Number(value.value?.toFixed(0))
                                        : Number(value.default?.toFixed(0));

                                let handleBorder = "";
                                if (value.default === maxValue) {
                                    handleBorder = " handle-border";
                                }
                                if (value.default > maxValue) {
                                    handleBorder = " over-handle-border";
                                }
                                let defaultCurrentValue = {
                                    style: {
                                        color: "black",
                                        marginTop: "28px",
                                    },
                                    label: new Intl.NumberFormat("fr-FR").format(
                                        Math.round(defaultVal)
                                    ),
                                };
                                let maxValueMarks =
                                    Intl.NumberFormat("fr-FR").format(maxValue);
                                const { data, options } = generatePieChartData(
                                    "target",
                                    impactType
                                );
                                switch (type) {
                                    case "bar":
                                        // Handling slider
                                        return (
                                            <div key={key} className="impact-row">
                                                <p>{label}</p>
                                                <OverflowSlider
                                                    type={type + " " + impactKey}
                                                    handleBorder={handleBorder}
                                                    defaultVal={defaultVal}
                                                    maxValue={maxValue}
                                                    defaultCurrentValue={
                                                        defaultCurrentValue
                                                    }
                                                    maxValueMarks={maxValueMarks}
                                                />
                                            </div>
                                        );
                                    case "text":
                                        return (
                                            <div key={key} className="impact-row">
                                                <p>{label}</p>
                                                <p className="text">
                                                    {Number(defaultVal).toFixed(0)}{" "}
                                                    {unit}
                                                </p>
                                            </div>
                                        );
                                    case "pie":
                                        // Handling chart
                                        return (
                                            <div
                                                key={key}
                                                className="impact-row"
                                                style={{
                                                    width: "190px",
                                                }}
                                            >
                                                <p>{label}</p>
                                                <Pie
                                                    plugins={[ChartDataLabels]}
                                                    data={data}
                                                    options={options}
                                                />
                                            </div>
                                        );
                                    default:
                                        return null;
                                }
                            })}
                    </div>
                </div>
            </div>
        </div>
    );
}

class OverflowSlider extends React.Component {
    render() {
        const type = this.props.type;
        const handleBorder = this.props.handleBorder;
        const defaultVal = this.props.defaultVal;
        const maxValue = this.props.maxValue;
        const defaultCurrentValue = this.props.defaultCurrentValue;
        // const maxValueMarks = this.props.maxValueMarks;
        if (defaultVal > maxValue) {
            return (
                <Slider
                    className={
                        "slider-overflowed slider-simulateur-impact slider-enr " +
                        type +
                        handleBorder
                    }
                    defaultValue={maxValue}
                    min={0}
                    max={defaultVal}
                    step={1}
                    disabled={true}
                    value={maxValue}
                    marks={{
                        // 0: 0,
                        // [maxValue]: maxValueMarks,
                        [defaultVal]: defaultCurrentValue,
                    }}
                    style={{ width: (defaultVal / maxValue) * 85 + "%" }}
                />
            );
        }

        return (
            <Slider
                className={"slider-simulateur-impact slider-enr " + type + handleBorder}
                defaultValue={defaultVal}
                min={0}
                max={maxValue}
                step={1}
                disabled={true}
                value={defaultVal}
                marks={{
                    // 0: 0,
                    [defaultVal]: defaultCurrentValue,
                    // [maxValue]: maxValueMarks,
                }}
            />
        );
    }
}

/**
 * This component is used to
 */
class SimulatorEnR extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            configuration: {},
            genericInputParam: {},
            specificInputParam: {},
            genericIntermediateParams: {},
            intermediateCompute: {},
            initialSetting: {
                targetYear: 2030,
                targetReductionConsumption: "",
                targetReductionConsumptionElectricity: "",
            },
            isInitialSettingVisible: false,
            errorStatus: false,
        };
    }

    componentDidMount() {
        this.fetchConfigurations();
    }

    componentWillUnmount() {
        if (this.promise) this.promise.abort();
    }

    fetchConfigurations() {
        let territory = "&maille=" + this.props.parentApi.data.zone.maille;
        let territoryType = "?zone=" + this.props.parentApi.data.zone.zone;
        let territoryCode = "&zone_id=" + this.props.parentApi.data.currentZone;
        let urlPartageable = territoryType + territory + territoryCode + "&type=enr";
        this.props.parentApi.callbacks.mettreAJourParametresUrls(urlPartageable);
        const isNational = this.props.parentApi.data.settings["is_national"];
        const { region } = this.props.parentApi.data;
        const url = this.buildSimulatorUrl(
            isNational,
            region,
            territoryType,
            territory,
            territoryCode
        );
        if (this.promise) this.promise.abort();
        this.promise = Api.callApi(url, null, "GET");
        this.promise
            .then((response) => {
                if (response.metadata.level.status && response.metadata.level.message) {
                    // throw missing data error if the level is not filled in the database
                    this.setState({
                        errorStatus: response.metadata.level.status,
                        errorMsg: response.metadata.level.message,
                        isNational: this.props.parentApi.data.settings["is_national"],
                    });
                } else {
                    //  set default values for each action
                    Object.values(response["metadata"].data).forEach(
                        function (category) {
                            Object.values(category.levers).forEach(function (lever) {
                                Object.values(lever.actions).forEach(function (action) {
                                    for (let nbrAction in response["actions_params"]) {
                                        if (nbrAction === action.number) {
                                            action.defaultValue =
                                                response["actions_params"][nbrAction];
                                        }
                                    }
                                });
                            });
                        }
                    );

                    // set default values for impact graphs
                    Object.values(response["metadata"].impacts).forEach(
                        function (impact) {
                            for (let type in response["impacts_params"]) {
                                if (type === impact["impactType"]) {
                                    impact.defaultValue =
                                        response["impacts_params"][type];
                                }
                            }
                        }
                    );
                    this.setState({
                        configuration: response["metadata"],
                        activatedImpacts: response["activated_impacts"],
                        genericInputParam: response["generic_input_param"],
                        specificInputParam: response["specific_input_param"],
                        genericIntermediateParams: response["generic_inter_param"],
                        intermediateCompute: response["intermediate_compute"],
                        isNational: this.props.parentApi.data.settings["is_national"],
                    });
                }
            })
            .catch((e) => {
                if (e.name === "AbortError") return;
                this.setState({
                    errorStatus: true,
                    errorMsg: e.message,
                    isNational: this.props.parentApi.data.settings["is_national"],
                });
            });
    }

    buildSimulatorUrl(isNational, region, territoryType, territory, territoryCode) {
        const baseApi = isNational
            ? config.api_national_simulator
            : config.api_simulator;
        const baseRegion = isNational ? "national" : region;

        let url = buildRegionUrl(baseApi, baseRegion)
            .replace("#type#", "enr")
            .replace("#territory#", region);

        url += territoryType + territory + territoryCode;

        return url;
    }

    addCategory() {
        let configuration = this.state.configuration;
        let regionName = this.props.parentApi.data.region;

        const handleCategoryChange = (e) => {
            e.stopPropagation();
            // Enable the checked category
            Object.values(configuration.data).forEach((category) => {
                if (parseInt(e.target.name, 10) === category.id) {
                    category.enabled = e.target.checked;
                }
            });
            this.setState({
                configuration: configuration,
            });
        };

        return (
            this.state.configuration.data &&
            Object.keys(this.state.configuration.data).map((categoryId) => {
                let id = this.state.configuration.data[categoryId].id;
                let title = this.state.configuration.data[categoryId].categoryTitle;
                let enabled = this.state.configuration.data[categoryId].enabled;
                let enabledRegionsCategory =
                    this.state.configuration.data[categoryId].regions_enabled;
                let enabledCategory = enabledRegionsCategory.includes(regionName);
                return (
                    enabledCategory && (
                        <div
                            className="checkbox-container-category-simulator"
                            key={"catgory-" + categoryId}
                        >
                            <ul>
                                <li>
                                    <input
                                        type="checkbox"
                                        id={"category-" + id}
                                        name={id}
                                        onClick={handleCategoryChange}
                                    ></input>
                                    <label htmlFor={"category-" + id}>{title}</label>
                                    {this.state.configuration.data[categoryId].levers &&
                                        Object.keys(
                                            this.state.configuration.data[categoryId]
                                                .levers
                                        ).length > 0 &&
                                        enabled &&
                                        Object.keys(
                                            this.state.configuration.data[categoryId]
                                                .levers
                                        ).map((leverId) => {
                                            let enabledRegion =
                                                this.state.configuration.data[
                                                    categoryId
                                                ].levers[leverId].regions_enabled;
                                            let enabledLever =
                                                enabledRegion.includes(regionName);
                                            return this.addLevers(
                                                categoryId,
                                                leverId,
                                                enabledLever
                                            );
                                        })}
                                </li>{" "}
                            </ul>
                        </div>
                    )
                );
            })
        );
    }

    addLevers(categoryId, leverId, enabledLever) {
        return (
            enabledLever && (
                <div
                    key={"category_" + categoryId + "_sub_category_" + leverId}
                    id={"category_" + categoryId + "_sub_category_" + leverId}
                    className="checkbox-container-lever-simulator"
                >
                    <ul>
                        {Object.keys(
                            this.state.configuration.data[categoryId].levers[leverId]
                                .actions
                        ).map((actionId) => {
                            return this.addActions(categoryId, leverId, actionId);
                        })}
                    </ul>
                </div>
            )
        );
    }

    actionDashboard(categoryId, leverId, action) {
        const { specificInputParam, genericInputParam } = this.state;
        // Use global_data if global is true, otherwise use default data
        const { parc_data, projects_data, bilan_data } = action.defaultValue.global
            ? action.defaultValue.global_data
            : action.defaultValue;

        // Handle input changes for parc and projects data
        const handleInputChange = (key, value, item, index) => {
            if (Array.isArray(item[key])) {
                item[key][index] = parseFloat(value) || 0;
            } else {
                item[key] = parseFloat(value) || 0;
            }

            // Update production_annuelle if puissance_annuelle changes
            if (
                key === "puissance_annuelle" &&
                item.puissance_annuelle !== undefined &&
                item.production_annuelle !== undefined
            ) {
                item.production_annuelle = updateProductionFromPuissance(
                    action,
                    genericInputParam,
                    specificInputParam,
                    item
                );
            }
            // Update puissance_annuelle if production_annuelle changes
            if (
                key === "production_annuelle" &&
                item.production_annuelle !== undefined &&
                item.puissance_annuelle !== undefined
            ) {
                item.puissance_annuelle = updatePuissanceFromProduction(
                    action,
                    genericInputParam,
                    specificInputParam,
                    item
                );
            }

            // Handle bilan_data change
            this.updateBilanData(categoryId, leverId, action.number);
        };

        // Render data blocks
        const renderDataBlock = (title, data, isEditable, className) => (
            <div className={`data-block ${className}`}>
                {(data || []).map((item, index) => (
                    <div key={index} className="data-item">
                        <div className="data-title">{title}:</div>
                        <div className="data-values">
                            {isEditable ? (
                                <>
                                    {Array.isArray(item.production_annuelle) ? (
                                        item.production_annuelle.map((val, i) => (
                                            <div key={i} className="data-entry">
                                                {item.labels && item.labels[i] && (
                                                    <span className="data-label">
                                                        {capitalizeFirstLetter(
                                                            item.labels[i]
                                                        )}
                                                        :{" "}
                                                    </span>
                                                )}
                                                <input
                                                    type="number"
                                                    value={Number(val).toFixed(0)}
                                                    onChange={(e) =>
                                                        handleInputChange(
                                                            "production_annuelle",
                                                            e.target.value,
                                                            item,
                                                            i
                                                        )
                                                    }
                                                    className="data-input"
                                                />
                                            </div>
                                        ))
                                    ) : (
                                        <input
                                            type="number"
                                            value={Number(
                                                item.production_annuelle
                                            ).toFixed(0)}
                                            onChange={(e) =>
                                                handleInputChange(
                                                    "production_annuelle",
                                                    e.target.value,
                                                    item
                                                )
                                            }
                                            className="data-input"
                                        />
                                    )}
                                    {item.puissance_annuelle !== undefined && (
                                        <input
                                            type="number"
                                            value={Number(
                                                item.puissance_annuelle
                                            ).toFixed(0)}
                                            onChange={(e) =>
                                                handleInputChange(
                                                    "puissance_annuelle",
                                                    e.target.value,
                                                    item
                                                )
                                            }
                                            className="data-input"
                                        />
                                    )}
                                </>
                            ) : (
                                <>
                                    {Array.isArray(item.production_annuelle) ? (
                                        item.production_annuelle.map((val, i) => (
                                            <div key={i} className="data-entry">
                                                {item.labels && item.labels[i] && (
                                                    <span className="data-label">
                                                        {capitalizeFirstLetter(
                                                            item.labels[i]
                                                        )}
                                                        :{" "}
                                                    </span>
                                                )}
                                                <strong>
                                                    {Number(val).toFixed(0)} GWh
                                                </strong>
                                            </div>
                                        ))
                                    ) : (
                                        <strong>
                                            {Number(item.production_annuelle).toFixed(
                                                0
                                            )}{" "}
                                            GWh
                                        </strong>
                                    )}
                                    {item.puissance_annuelle !== undefined && (
                                        <strong>
                                            {Number(item.puissance_annuelle).toFixed(0)}{" "}
                                            MW
                                        </strong>
                                    )}
                                </>
                            )}
                        </div>
                    </div>
                ))}
            </div>
        );

        return (
            <div className="dashboard-container">
                <div className="dashboard-row">
                    {renderDataBlock(
                        "Actuel",
                        parc_data,
                        false,
                        "non-editable-parc-block"
                    )}
                    <span className="plus-sign">+</span>
                    {renderDataBlock("Projets", projects_data, true, "editable-block")}
                    <span className="equals-sign">=</span>
                    {renderDataBlock(
                        "Bilan",
                        bilan_data,
                        false,
                        "non-editable-bilan-block"
                    )}
                </div>
            </div>
        );
    }

    updateBilanData(categoryId, leverId, actionNumber) {
        const configurationCopy = { ...this.state.configuration };

        // Helper function to calculate sums for a given key
        const calculateSums = (data, key) => {
            return (data || []).reduce(
                (sum, item) => {
                    const value = item[key];

                    if (Array.isArray(value)) {
                        value.forEach((subVal, index) => {
                            sum[index] = (sum[index] ?? 0) + (subVal ?? 0);
                        });
                        return sum;
                    }

                    return (sum ?? 0) + (value ?? 0);
                },
                Array.isArray(data[0]?.[key]) ? [] : 0
            );
        };

        // Function to compute bilan data for an action, sub_action, or global
        const computeBilanForAction = (actionData) => {
            if (
                !actionData ||
                !actionData.bilan_data ||
                actionData.bilan_data.length === 0
            )
                return;

            const parcData = actionData.parc_data || [];
            const projectsData = actionData.projects_data || [];

            // Extract production data
            const parcProduction = parcData.map((i) => i.production_annuelle ?? 0);
            const projectsProduction = projectsData.map(
                (i) => i.production_annuelle ?? 0
            );

            // Compute the summed production by adding corresponding elements
            const summedProduction = parcProduction.map((parc, index) => {
                const parcValue = Array.isArray(parc) ? parc : [parc];
                const projectValue = Array.isArray(projectsProduction[index])
                    ? projectsProduction[index]
                    : [projectsProduction[index]];

                return parcValue.map((val, i) => (val ?? 0) + (projectValue[i] ?? 0));
            });

            // Store the computed production in bilan_data
            actionData.bilan_data[0].production_annuelle = summedProduction.flat();

            // Compute power (puissance_annuelle) if the field exists in both datasets
            const puissanceParc = parcData.some(
                (i) => i.puissance_annuelle !== undefined
            )
                ? calculateSums(parcData, "puissance_annuelle")
                : null;
            const puissanceProjects = projectsData.some(
                (i) => i.puissance_annuelle !== undefined
            )
                ? calculateSums(projectsData, "puissance_annuelle")
                : null;

            // If both power values are available, sum them
            if (puissanceParc !== null && puissanceProjects !== null) {
                actionData.bilan_data[0].puissance_annuelle =
                    puissanceParc + puissanceProjects;
            }
        };

        // Iterate through the actions in the specified category and lever
        Object.values(
            configurationCopy.data[categoryId].levers[leverId].actions
        ).forEach((item) => {
            if (actionNumber === item.number) {
                // Handle sub-actions if they exist
                if (item.defaultValue.sub_action) {
                    Object.entries(item.defaultValue.sub_action).forEach(
                        ([subActionKey, subAction]) => {
                            const updatedSubAction = { ...subAction };
                            computeBilanForAction(updatedSubAction);
                            item.defaultValue.sub_action[subActionKey] =
                                updatedSubAction;
                        }
                    );
                }

                // Handle main action
                computeBilanForAction(item.defaultValue);

                // Handle global if it exists
                if (item.defaultValue.global) {
                    computeBilanForAction(item.defaultValue.global_data);
                }

                // Update the target rhythm after computations
                this.recalculateTargetRhythm();
            }
        });

        // Update state after calculations are completed
        this.setState({ configuration: configurationCopy }, () => {
            // Update impact result
            this.handleParamsChange();
        });
    }

    recalculateTargetRhythm() {
        const { intermediateCompute, initialSetting, configuration } = this.state;
        const helpDataActions = intermediateCompute?.["help_data_actions"];
        const actionsByNumber = {};

        // Map actions by their number from the configuration
        Object.values(configuration.data).forEach((category) => {
            Object.values(category.levers).forEach((lever) => {
                lever.actions.forEach((action) => {
                    actionsByNumber[action.number] = action;
                });
            });
        });

        let updatedHelpDataActions = { ...helpDataActions };
        const targetYear = initialSetting.targetYear;

        Object.entries(updatedHelpDataActions).forEach(([actionKey, item]) => {
            const actionData = item.data;
            const configAction = actionsByNumber[actionKey];

            if (!configAction || !configAction.defaultValue) {
                console.warn(`No matching configuration action for ${actionKey}`);
                return;
            }

            // Helper function to compute target rhythm
            const computeTargetRhythm = (productionData, targetData) => {
                const lastYearDataAvailability = targetData.last_year_data_availability;
                if (
                    productionData !== undefined &&
                    lastYearDataAvailability !== undefined
                ) {
                    targetData.target_rhythm =
                        productionData / (targetYear - lastYearDataAvailability - 1);
                } else {
                    console.error(
                        `Production or last year data is missing for action ${actionKey}`
                    );
                }
            };

            // Handle global case
            if (configAction.defaultValue.global) {
                const globalProjectsData =
                    configAction.defaultValue.global_data.projects_data[0];
                if (globalProjectsData) {
                    computeTargetRhythm(
                        globalProjectsData.production_annuelle,
                        actionData.global
                    );
                } else {
                    console.warn(
                        `Missing global projects data for action ${actionKey}`
                    );
                }
            }

            // Handle sub-actions
            if (configAction.defaultValue.sub_action) {
                Object.entries(configAction.defaultValue.sub_action).forEach(
                    ([subActionKey, subAction]) => {
                        if (!actionData[subActionKey]) {
                            console.warn(
                                `No data found for sub_action ${subActionKey} in ${actionKey}`
                            );
                            return;
                        }

                        const subData = actionData[subActionKey];

                        if (subAction.type_display === "complex") {
                            const productionData =
                                subAction.projects_data[0]?.production_annuelle;
                            const labels = subAction.projects_data[0]?.labels;

                            if (
                                !Array.isArray(productionData) ||
                                !Array.isArray(labels)
                            ) {
                                console.error(
                                    `Invalid data format for production or labels in ${subActionKey} of ${actionKey}`
                                );
                                return;
                            }

                            labels.forEach((label, index) => {
                                if (!subData[label]) {
                                    console.warn(
                                        `No data found for label ${label} in ${subActionKey} of ${actionKey}`
                                    );
                                    return;
                                }
                                computeTargetRhythm(
                                    productionData[index],
                                    subData[label]
                                );
                            });
                        } else {
                            computeTargetRhythm(
                                subAction.projects_data[0]?.production_annuelle,
                                subData
                            );
                        }
                    }
                );
            } else {
                // Handle main action
                if (item.type_display === "complex") {
                    const productionData =
                        configAction.defaultValue.projects_data[0]?.production_annuelle;
                    const labels = configAction.defaultValue.projects_data[0]?.labels;

                    if (!Array.isArray(productionData) || !Array.isArray(labels)) {
                        console.error(
                            `Invalid data format for production or labels in ${actionKey}`
                        );
                        return;
                    }

                    labels.forEach((label, index) => {
                        if (!actionData[label]) {
                            console.warn(
                                `No data found for label ${label} in ${actionKey}`
                            );
                            return;
                        }
                        computeTargetRhythm(productionData[index], actionData[label]);
                    });
                } else {
                    computeTargetRhythm(
                        configAction.defaultValue.projects_data[0]?.production_annuelle,
                        actionData
                    );
                }
            }
        });

        // Update the state
        this.setState((prevState) => ({
            intermediateCompute: {
                ...prevState.intermediateCompute,
                help_data_actions: updatedHelpDataActions,
            },
        }));
    }

    getInitialSetting() {
        const { isInitialSettingVisible, initialSetting, intermediateCompute } =
            this.state;

        // Ensure targetYear defaults to '2030' if not already set
        const targetYear = initialSetting.targetYear || "2030";
        const { targetReductionConsumption, targetReductionConsumptionElectricity } =
            initialSetting;

        // Intermediate data
        const {
            "consumption-evolution-2015-last-year": historicalRhythm,
            "consumption-evolution-diagnosis-2030": consumptionEvolution2030,
            "consumption-evolution-diagnosis-2050": consumptionEvolution2050,
            last_year_prod: lastYearProd,
            diagnosis_year: diagnosisYear,
        } = intermediateCompute["initial-setting"] || {};

        // Helper function for rendering input fields
        const renderInputField = (id, label, value, onChangeHandler) => (
            <li className="form-group">
                <label htmlFor={id}>{label}</label>
                <input
                    type="text"
                    id={id}
                    name={id}
                    value={value}
                    onChange={onChangeHandler}
                    className="input-field"
                />
            </li>
        );

        // Handle Input change
        const handleInputChange = (e) => {
            const { name, value } = e.target;

            // TODOD : Validate year input for targetYear
            // if (name === "targetYear") {
            //     const yearRegex = /^\d{4}$/; // Matches exactly 4-digit numbers
            //     if (!yearRegex.test(value)) {
            //         alert("Veuillez entrer une année valide (par exemple, 2030).");
            //         return;
            //     }
            // }

            // Update state
            this.setState(
                (prevState) => ({
                    initialSetting: {
                        ...prevState.initialSetting,
                        [name]: value,
                    },
                }),
                () => {
                    // Handle impact change
                    this.handleParamsChange();
                    // Handle target rhythm change
                    if (name === "targetYear") {
                        this.recalculateTargetRhythm();
                    }
                }
            );
        };

        // Handle checkbox toggle
        const handleCheckboxToggle = (e) => {
            const { checked } = e.target;
            this.setState({ isInitialSettingVisible: checked });
        };

        // Check if both PCAET values are "NA" or if diagnosis_year is NA, and hide the section
        const isPCAETDataValid =
            consumptionEvolution2030 !== "NA" || consumptionEvolution2050 !== "NA";

        // Rendering Section
        return (
            <div
                className="checkbox-container-category-simulator"
                key="initial-setting"
            >
                <ul>
                    <li>
                        <input
                            type="checkbox"
                            id="initial-setting"
                            name="initial-setting"
                            onChange={handleCheckboxToggle}
                        />
                        <label htmlFor="initial-setting">Paramétrage initial</label>
                    </li>
                </ul>
                {isInitialSettingVisible && (
                    <div className="two-columns">
                        {/* Form column */}
                        <div className="column">
                            <div className="card">
                                <ul>
                                    {renderInputField(
                                        "targetYear",
                                        "Choix de l'année cible :",
                                        targetYear,
                                        handleInputChange
                                    )}
                                    {renderInputField(
                                        "targetReductionConsumption",
                                        <>
                                            Évolution de la consommation (/
                                            {lastYearProd}) en&nbsp;%&nbsp;:
                                        </>,
                                        targetReductionConsumption,
                                        handleInputChange
                                    )}
                                    {renderInputField(
                                        "targetReductionConsumptionElectricity",
                                        <>
                                            Évolution de la consommation d’électricité
                                            (/{lastYearProd}) en&nbsp;%&nbsp;:
                                        </>,
                                        targetReductionConsumptionElectricity,
                                        handleInputChange
                                    )}
                                </ul>
                            </div>
                        </div>
                        {/* Info column */}
                        <div className="column">
                            <div className="card">
                                <ul>
                                    <li className="form-group">
                                        <div className="image-container">
                                            <img
                                                src="/img/lightbulb.png"
                                                alt="Lightbulb icon"
                                            />
                                            <span className="useful-infos">
                                                Informations utiles
                                            </span>
                                        </div>
                                    </li>
                                    {/* Render Historical Rhythm */}
                                    <li className="form-group">
                                        <strong>Rythme historique&nbsp;</strong>&nbsp;(
                                        {diagnosisYear}-{lastYearProd})&nbsp;:&nbsp;
                                        {historicalRhythm ?? "N/A"} %
                                    </li>
                                    {/* Render PCAET Objectives */}
                                    {isPCAETDataValid && (
                                        <li className="form-group">
                                            <ul>
                                                <strong>Objectifs PCAET :</strong>
                                                {consumptionEvolution2030 !== "NA" && (
                                                    <li>
                                                        <i className="bi bi-arrow-right"></i>
                                                        &nbsp;entre&nbsp;{diagnosisYear}
                                                        &nbsp;et&nbsp;2030&nbsp;:&nbsp;
                                                        {consumptionEvolution2030} %
                                                    </li>
                                                )}
                                                {consumptionEvolution2050 !== "NA" && (
                                                    <li>
                                                        <i className="bi bi-arrow-right"></i>
                                                        &nbsp;entre&nbsp;{diagnosisYear}
                                                        &nbsp;et&nbsp;2050&nbsp;:&nbsp;
                                                        {consumptionEvolution2050}
                                                        &nbsp;%
                                                    </li>
                                                )}
                                            </ul>
                                        </li>
                                    )}
                                </ul>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }

    actionHelp(action, subAction = false) {
        const { intermediateCompute, initialSetting } = this.state;

        // Ensure `help_data_actions` exists
        const helpDataActions = intermediateCompute?.["help_data_actions"];
        if (!helpDataActions) {
            return <div>Aucune donnée disponible pour cette action.</div>;
        }

        // Fetch data for the given action.number
        const actionData = helpDataActions[action.number];
        if (!actionData) {
            return (
                <div>Aucune donnée correspondante pour l'action {action.number}.</div>
            );
        }

        // Check if subAction is defined and exists in actionData.data
        let dataSource = actionData.data;
        let typeDisplay = actionData.type_display;
        if (subAction && dataSource[subAction] && !action.defaultValue?.global) {
            dataSource = dataSource[subAction];
            typeDisplay = dataSource.type_display ? dataSource.type_display : "complex";
        } else if (action.defaultValue?.global && dataSource.global) {
            // Handle actions with 2 mode of display global and by sub-sector
            dataSource = dataSource.global;
            typeDisplay = dataSource.type_display;
        }

        // Check if dataSource contains valid data
        if (!dataSource) {
            return <div>Aucune donnée disponible pour cette sous-action.</div>;
        }

        // Render for simple display
        if (typeDisplay === "simple") {
            const lastYear = dataSource?.last_year_data_availability ?? "NA";
            const startYear = lastYear !== "NA" ? lastYear - 3 : "NA";
            const targetStartYear = lastYear + 1;
            const targetLastYear = initialSetting.targetYear;
            const historicalRhythm = dataSource?.historical_rhythm ?? "NA";
            const targetRhythm = dataSource?.target_rhythm ?? "NA";
            const analysis = dataSource?.analysis;
            const pcaet2030 = dataSource?.["pcaet-2030"] ?? "NA";
            const pcaet2050 = dataSource?.["pcaet-2050"] ?? "NA";

            return (
                <div className="action-help">
                    <div className="header">
                        <div className="image-container">
                            <img src="/img/lightbulb.png" alt="Lightbulb" />
                            <span className="useful-infos">Infos utiles</span>
                        </div>
                    </div>
                    <div className="two-columns">
                        {/* Left Column */}
                        <div className="left-column">
                            <ul>
                                {historicalRhythm !== "NA" && (
                                    <li>
                                        <strong>
                                            Rythme historique ({startYear}-{lastYear}) :
                                        </strong>{" "}
                                        {historicalRhythm.toFixed(1)} GWh/an
                                    </li>
                                )}
                                {analysis && analysis.sum !== "NA" && (
                                    <li>
                                        <strong>{analysis.name} :</strong>{" "}
                                        {analysis.sum?.toFixed(2)} {analysis.unit}
                                    </li>
                                )}
                            </ul>
                        </div>

                        {/* Right Column */}
                        <div className="right-column">
                            <ul>
                                {targetRhythm !== "NA" && (
                                    <li>
                                        <strong>
                                            Rythme cible ({targetStartYear}-
                                            {targetLastYear}) :
                                        </strong>{" "}
                                        {targetRhythm.toFixed(1)} GWh supp./ an
                                    </li>
                                )}
                                {/* Only render PCAET goals if they are not "NA" */}
                                {(pcaet2030 !== "NA" || pcaet2050 !== "NA") && (
                                    <li>
                                        <strong>Objectifs PCAET :</strong>
                                        {pcaet2030 !== "NA" && (
                                            <div>
                                                <i className="bi bi-arrow-right"></i>{" "}
                                                {pcaet2030}{" "}
                                                <strong>GWh à horizon 2030</strong>
                                            </div>
                                        )}
                                        {pcaet2050 !== "NA" && (
                                            <div>
                                                <i className="bi bi-arrow-right"></i>{" "}
                                                {pcaet2050}{" "}
                                                <strong>GWh à horizon 2050</strong>
                                            </div>
                                        )}
                                    </li>
                                )}
                            </ul>
                        </div>
                    </div>
                </div>
            );
        }

        // Render for complex display
        else if (typeDisplay === "complex") {
            return (
                <div className="action-help">
                    <div className="header">
                        <div className="image-container">
                            <img src="/img/lightbulb.png" alt="Lightbulb" />
                            <span className="useful-infos">Infos utiles</span>
                        </div>
                    </div>
                    {Object.keys(dataSource).map((key) => {
                        const data = dataSource[key];
                        const lastYear = data?.last_year_data_availability ?? "NA";
                        const startYear = lastYear !== "NA" ? lastYear - 3 : "NA";
                        const targetStartYear = lastYear + 1;
                        const targetLastYear = initialSetting.targetYear;
                        const historicalRhythm = data?.historical_rhythm ?? "NA";
                        const targetRhythm = data?.target_rhythm ?? "NA";
                        const analysis = data?.analysis;
                        const pcaet2030 = data?.["pcaet-2030"] ?? "NA";
                        const pcaet2050 = data?.["pcaet-2050"] ?? "NA";

                        return (
                            <div key={key} className="two-columns">
                                {/* Left Column */}
                                <div className="left-column">
                                    <strong className="header">
                                        {capitalizeFirstLetter(key)}
                                    </strong>
                                    <ul>
                                        {historicalRhythm !== "NA" && (
                                            <li>
                                                <strong>
                                                    Rythme historique ({startYear}-
                                                    {lastYear}) :
                                                </strong>{" "}
                                                {historicalRhythm.toFixed(1)} GWh/an
                                            </li>
                                        )}
                                        {analysis && (
                                            <li>
                                                <strong>{analysis.name} :</strong>{" "}
                                                {analysis.sum?.toFixed(2)}{" "}
                                                {analysis.unit}
                                            </li>
                                        )}
                                    </ul>
                                </div>

                                {/* Right Column */}
                                <div className="right-column">
                                    <ul>
                                        {targetRhythm !== "NA" && (
                                            <li>
                                                <strong>
                                                    Rythme cible ({targetStartYear}-
                                                    {targetLastYear}) :
                                                </strong>{" "}
                                                {targetRhythm.toFixed(1)} GWh supp./ an
                                            </li>
                                        )}
                                        {/* Only render PCAET goals if they are not "NA" */}
                                        {(pcaet2030 !== "NA" || pcaet2050 !== "NA") && (
                                            <li>
                                                <strong>Objectifs PCAET :</strong>
                                                {pcaet2030 !== "NA" && (
                                                    <div>
                                                        <i className="bi bi-arrow-right"></i>{" "}
                                                        {pcaet2030}{" "}
                                                        <strong>
                                                            GWh à horizon 2030
                                                        </strong>
                                                    </div>
                                                )}
                                                {pcaet2050 !== "NA" && (
                                                    <div>
                                                        <i className="bi bi-arrow-right"></i>{" "}
                                                        {pcaet2050}{" "}
                                                        <strong>
                                                            GWh à horizon 2050
                                                        </strong>
                                                    </div>
                                                )}
                                            </li>
                                        )}
                                    </ul>
                                </div>
                            </div>
                        );
                    })}
                </div>
            );
        }
    }

    addActions(categoryId, leverId, actionId) {
        let { configuration, intermediateCompute } = this.state;
        let action = configuration.data[categoryId].levers[leverId].actions[actionId];

        // Handle checkbox click
        const handleActionChange = (e) => {
            const configurationCopy = JSON.parse(JSON.stringify(configuration));
            Object.values(
                configurationCopy.data[categoryId].levers[leverId].actions
            ).forEach((act) => {
                if (parseInt(e.target.name, 10) === act.id) {
                    act.enabled = e.target.checked;
                }
            });

            this.setState({ configuration: configurationCopy });
        };

        // Reset selected action
        const resetAction = (actionNumber, subKey) => {
            const configurationCopy = JSON.parse(JSON.stringify(configuration));
            const helpDataActions = JSON.parse(
                JSON.stringify(intermediateCompute?.["help_data_actions"])
            );

            // Helper function to reset production and power values only if they exist
            const resetProductionData = (data, typeDisplay) => {
                if (data?.[0]) {
                    const resetValues = (key) => {
                        if (key in data[0]) {
                            if (typeDisplay === "simple") {
                                data[0][key] = 0;
                            } else if (typeDisplay === "complex") {
                                data[0][key].forEach((_, index) => {
                                    data[0][key][index] = 0;
                                });
                            }
                        }
                    };
                    resetValues("production_annuelle");
                    resetValues("puissance_annuelle");
                }
            };

            Object.keys(
                configurationCopy.data[categoryId].levers[leverId].actions
            ).forEach((key) => {
                let act =
                    configurationCopy.data[categoryId].levers[leverId].actions[key];
                if (act.number === actionNumber) {
                    let typeDisplay = helpDataActions[act.number].type_display;

                    // Reset main action data
                    resetProductionData(act.defaultValue.projects_data, typeDisplay);

                    // Reset global data if it exists
                    if (act.defaultValue.global) {
                        resetProductionData(
                            act.defaultValue.global_data.projects_data,
                            typeDisplay
                        );
                    }

                    // Reset all sub-actions if they exist
                    if (act.defaultValue.sub_action) {
                        Object.keys(act.defaultValue.sub_action).forEach(
                            (subKeyAct) => {
                                const subAction =
                                    act.defaultValue.sub_action[subKeyAct];
                                if (subKeyAct === subKey) {
                                    resetProductionData(
                                        subAction.projects_data,
                                        subAction.type_display
                                    );
                                }
                            }
                        );
                    }
                }
            });

            // Update state
            this.setState({ configuration: configurationCopy }, () => {
                this.updateBilanData(categoryId, leverId, actionNumber);
            });
        };

        // Handle Global display for action with sub-action and 2 display mode
        const handleGlobalClick = (categoryId, leverId, actionId, isGlobal) => {
            const configurationCopy = JSON.parse(JSON.stringify(configuration));

            // Retrieve the specific action to be updated
            let actionToUpdate =
                configurationCopy.data[categoryId].levers[leverId].actions[actionId];

            // Check if the action has sub-actions
            if (actionToUpdate.defaultValue.sub_action) {
                if (isGlobal) {
                    // Initialize objects to store summed values for parc, projects, and bilan data
                    let summedDataParcData = {};
                    let summedDataProjectData = {};
                    let summedDataBilanData = {};

                    // Helper function to sum values, handling arrays and undefined cases
                    const sumValues = (source, key) => {
                        if (!source) return 0;
                        if (Array.isArray(source[key])) {
                            return source[key].reduce(
                                (acc, value) => acc + (value || 0),
                                0
                            );
                        }
                        return source[key] || 0;
                    };

                    // Flags to check if 'production_annuelle' or 'puissance_annuelle' exist
                    let hasProduction = false;
                    let hasPuissance = false;

                    // Iterate through each sub-action to accumulate values
                    Object.values(actionToUpdate.defaultValue.sub_action).forEach(
                        (subAction) => {
                            // Check and sum 'production_annuelle' if present
                            if (
                                subAction.parc_data[0]?.production_annuelle !==
                                undefined
                            ) {
                                summedDataParcData.production_annuelle =
                                    (summedDataParcData.production_annuelle || 0) +
                                    sumValues(
                                        subAction.parc_data[0],
                                        "production_annuelle"
                                    );
                                hasProduction = true;
                            }
                            if (
                                subAction.projects_data[0]?.production_annuelle !==
                                undefined
                            ) {
                                summedDataProjectData.production_annuelle =
                                    (summedDataProjectData.production_annuelle || 0) +
                                    sumValues(
                                        subAction.projects_data[0],
                                        "production_annuelle"
                                    );
                                hasProduction = true;
                            }
                            if (
                                subAction.bilan_data[0]?.production_annuelle !==
                                undefined
                            ) {
                                summedDataBilanData.production_annuelle =
                                    (summedDataBilanData.production_annuelle || 0) +
                                    sumValues(
                                        subAction.bilan_data[0],
                                        "production_annuelle"
                                    );
                                hasProduction = true;
                            }

                            // Check and sum 'puissance_annuelle' if present
                            if (
                                subAction.parc_data[0]?.puissance_annuelle !== undefined
                            ) {
                                summedDataParcData.puissance_annuelle =
                                    (summedDataParcData.puissance_annuelle || 0) +
                                    sumValues(
                                        subAction.parc_data[0],
                                        "puissance_annuelle"
                                    );
                                hasPuissance = true;
                            }
                            if (
                                subAction.projects_data[0]?.puissance_annuelle !==
                                undefined
                            ) {
                                summedDataProjectData.puissance_annuelle =
                                    (summedDataProjectData.puissance_annuelle || 0) +
                                    sumValues(
                                        subAction.projects_data[0],
                                        "puissance_annuelle"
                                    );
                                hasPuissance = true;
                            }
                            if (
                                subAction.bilan_data[0]?.puissance_annuelle !==
                                undefined
                            ) {
                                summedDataBilanData.puissance_annuelle =
                                    (summedDataBilanData.puissance_annuelle || 0) +
                                    sumValues(
                                        subAction.bilan_data[0],
                                        "puissance_annuelle"
                                    );
                                hasPuissance = true;
                            }
                        }
                    );

                    // Assign summed values to global_data only if at least one value exists
                    actionToUpdate.defaultValue.global_data = {
                        parc_data:
                            hasProduction || hasPuissance ? [summedDataParcData] : [],
                        projects_data:
                            hasProduction || hasPuissance
                                ? [summedDataProjectData]
                                : [],
                        bilan_data:
                            hasProduction || hasPuissance ? [summedDataBilanData] : [],
                    };

                    // Mark the action as global
                    actionToUpdate.defaultValue.global = true;
                } else {
                    // If isGlobal is false, remove global_data and reset global flag
                    delete actionToUpdate.defaultValue.global_data;
                    actionToUpdate.defaultValue.global = false;
                }

                // Update the state with the modified configuration
                this.setState({
                    configuration: configurationCopy,
                });
            }
        };

        let idInput = `category_${categoryId}_lever_${leverId}_action_${action.id}`;
        return (
            <li key={idInput}>
                <input
                    type="checkbox"
                    id={idInput}
                    name={action.id}
                    onClick={handleActionChange}
                />
                <label htmlFor={idInput}>{action.title}</label>
                {action.comment && (
                    <i
                        title={action.comment}
                        className="bi bi-question-circle-fill"
                    ></i>
                )}
                <br />

                {/* Handle simple display */}
                {action.enabled && action.defaultValue.display === "simple" && (
                    <div className="dashboard-card">
                        <div className="reset-container">
                            <i
                                className="bi bi-arrow-clockwise reset-button"
                                title={"Réinitialiser " + action.title.toLowerCase()}
                                onClick={() => resetAction(action.number)}
                            ></i>
                        </div>
                        {this.actionDashboard(categoryId, leverId, action)}
                        <div className="dashed-line-vertical"></div>
                        {this.actionHelp(action)}
                    </div>
                )}

                {/* Handle complex display by iterating over sub_actions */}
                {action.enabled && action.defaultValue.display === "complex" && (
                    <div>
                        {/* Buttons for Global and By Sub-Sector */}
                        <button
                            onClick={() =>
                                handleGlobalClick(categoryId, leverId, actionId, true)
                            }
                            className={`button-complex-display${action.defaultValue.global === true ? " active-button-complex-display" : ""}`}
                        >
                            Au global
                        </button>
                        <button
                            onClick={() =>
                                handleGlobalClick(categoryId, leverId, actionId, false)
                            }
                            className={`button-complex-display${action.defaultValue.global === false || action.defaultValue.global === undefined ? " active-button-complex-display" : ""}`}
                        >
                            Par sous-filière
                        </button>
                    </div>
                )}
                {action.enabled && action.defaultValue.display === "complex" && (
                    <div>
                        {action.defaultValue.global ? (
                            <div className="dashboard-card">
                                <div className="reset-container">
                                    <i
                                        className="bi bi-arrow-clockwise reset-button"
                                        title={
                                            "Réinitialiser " +
                                            action.title.toLowerCase()
                                        }
                                        onClick={() => resetAction(action.number)}
                                    ></i>
                                </div>
                                {this.actionDashboard(categoryId, leverId, {
                                    ...action,
                                })}
                                <div className="dashed-line-vertical"></div>
                                {this.actionHelp(action)}
                            </div>
                        ) : (
                            Object.entries(action.defaultValue["sub_action"] || {}).map(
                                ([subKey, subAction]) => (
                                    <div key={subKey} className="dashboard-card">
                                        <h5>{subAction.title}</h5>
                                        <div className="reset-container">
                                            <i
                                                className="bi bi-arrow-clockwise reset-button"
                                                title={"Reset " + subKey}
                                                onClick={() =>
                                                    resetAction(action.number, subKey)
                                                }
                                            ></i>
                                        </div>
                                        {this.actionDashboard(categoryId, leverId, {
                                            ...action,
                                            defaultValue: subAction,
                                        })}
                                        <div className="dashed-line-vertical"></div>
                                        {this.actionHelp(
                                            { ...action, defaultValue: subAction },
                                            subKey
                                        )}
                                    </div>
                                )
                            )
                        )}
                    </div>
                )}
            </li>
        );
    }

    addImpact() {
        let configuration = this.state.configuration;
        const handleImpactChange = (e) => {
            // Enable the checked impact
            Object.values(configuration.impacts).forEach(function (impact) {
                if (e.target.name === impact.impactType) {
                    impact.enabled = e.target.checked;
                }
            });
            this.setState({
                configuration: configuration,
            });
        };

        const generatePieChartData = (type, impactType) => {
            const actions =
                this.state.configuration?.data?.[1]?.levers?.[1]?.actions || [];

            const labels = [];
            const data = [];
            const backgroundColor = [];

            // Function to retrieve bilan/parc data based on type (current or target)
            const getBilan = (item) => {
                if (type === "current") return item?.parc_data?.[0];
                if (type === "target") return item?.bilan_data?.[0];
                return null;
            };

            // Function to determine the final bilan source (global has priority)
            const getFinalBilan = (action) => {
                if (action.defaultValue?.global) {
                    return getBilan(action.defaultValue.global_data);
                }
                return getBilan(action.defaultValue);
            };

            // Function to process production_annuelle
            const processBilan = (
                bilan,
                actionTitle,
                subActionTitle = "",
                color,
                filterForSubAction = false
            ) => {
                if (!bilan || !bilan.production_annuelle) return;

                if (Array.isArray(bilan.production_annuelle)) {
                    // Case where production_annuelle is an array with labels and colors and type
                    bilan.production_annuelle.forEach((value, index) => {
                        // Apply filter only for sub_action when filterForSubAction is "enr-elec"
                        if (filterForSubAction === "enr-elec") {
                            if (!bilan.type || bilan.type[index] !== "elec") return;
                        }

                        let label =
                            bilan.labels && bilan.labels[index]
                                ? `${actionTitle}${subActionTitle ? ` - ${subActionTitle}` : ""} (${bilan.labels[index]})`
                                : `${actionTitle}${subActionTitle ? ` - ${subActionTitle}` : ""}`;

                        // Add a line break if the label is too long
                        if (label.length > 25) {
                            label = label.substring(0, 25) + "\n" + label.substring(25);
                        }

                        labels.push(label);
                        data.push(value);
                        backgroundColor.push(
                            bilan.colors && bilan.colors[index]
                                ? bilan.colors[index]
                                : color || "#cccccc"
                        );
                    });
                } else {
                    // Case where production_annuelle is a single value (no array)
                    if (filterForSubAction === "enr-elec" && bilan.type !== "elec")
                        return;

                    let label = `${actionTitle}${subActionTitle ? ` - ${subActionTitle}` : ""}`;

                    // Add a line break if the label is too long
                    if (label.length > 25) {
                        label = label.substring(0, 25) + "\n" + label.substring(25);
                    }

                    labels.push(label);
                    data.push(bilan.production_annuelle);
                    backgroundColor.push(color || "#cccccc");
                }
            };

            // Step 1: Process sub_actions FIRST, but skip if global is true
            actions.forEach((action) => {
                // Skip processing sub-actions if action has global set to true
                if (action.defaultValue?.global) return;

                if (action.defaultValue?.sub_action) {
                    Object.values(action.defaultValue.sub_action).forEach(
                        (subAction) => {
                            const subBilan = getBilan(subAction);
                            processBilan(
                                subBilan,
                                action.title,
                                subAction.title,
                                subAction.color,
                                impactType
                            );
                        }
                    );
                }
            });

            // Step 2: Apply the filter for all actions (with or without sub_actions) AFTER sub_actions are processed
            const filteredActions =
                impactType === "enr-elec"
                    ? actions.filter((action) => action.defaultValue?.type === "elec")
                    : actions;

            // Step 3: Process remaining main actions, skipping sub-actions for global actions
            filteredActions.forEach((action) => {
                const finalBilan = getFinalBilan(action);

                // If global exists, use only it
                if (action.defaultValue?.global_data) {
                    processBilan(
                        finalBilan,
                        action.title,
                        "",
                        action.defaultValue?.color
                    );
                    return;
                }

                // Handle case for no sub-action
                if (!action.defaultValue?.sub_action) {
                    processBilan(
                        finalBilan,
                        action.title,
                        "",
                        action.defaultValue?.color
                    );
                }
            });

            return {
                data: {
                    labels,
                    datasets: [
                        {
                            data,
                            backgroundColor,
                        },
                    ],
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            display: false,
                        },
                        tooltip: {
                            callbacks: {
                                label: (tooltipItem) => {
                                    return tooltipItem.raw.toFixed(0);
                                },
                            },
                        },
                        datalabels: {
                            display: true,
                            color: "#fff",
                            font: {
                                weight: "bold",
                                size: 10,
                            },
                            formatter: (value) => value.toFixed(0),
                        },
                    },
                },
            };
        };

        return (
            <div className="checkbox-container-category-simulator">
                <div>
                    {configuration.impacts &&
                        Object.values(configuration.impacts).map((impact) => {
                            return (
                                <SimulatorImpact
                                    {...{
                                        impact,
                                        handleImpactChange,
                                        generatePieChartData,
                                        targetYear:
                                            this.state.initialSetting.targetYear,
                                    }}
                                />
                            );
                        })}
                </div>
            </div>
        );
    }

    handleParamsChange = () => {
        let configuration = { ...this.state.configuration };
        let activatedImpacts = this.state.activatedImpacts;
        let initialSetting = this.state.initialSetting;

        // Iterate through each activated impact type and calculate/update the results
        activatedImpacts.forEach((impactType) => {
            let prodEner = 0,
                consoEnr = 0,
                rateCoverage = 0;

            switch (impactType) {
                case "global-results":
                    // Calculate results for "global-results"
                    const globalResults = getResults(
                        initialSetting,
                        configuration,
                        "global-results"
                    );
                    if (globalResults) {
                        prodEner = globalResults.prodEner;
                        consoEnr = globalResults.consoEnr;
                        rateCoverage = globalResults.rateCoverage;
                    }

                    // Update target values for "global-results"
                    Object.values(configuration.impacts).forEach((impact) => {
                        if (
                            impact.impactType === "global-results" &&
                            impact.defaultValue?.target
                        ) {
                            impact.defaultValue.target["impact-energy"].value =
                                consoEnr;
                            impact.defaultValue.target["impact-production"].value =
                                prodEner;
                            impact.defaultValue.target["coverage-rate"].value =
                                rateCoverage;
                        }
                    });
                    break;

                case "enr-elec":
                    // Calculate results for "enr-elec"
                    const elecResults = getResults(
                        initialSetting,
                        configuration,
                        "enr-elec"
                    );
                    if (elecResults) {
                        prodEner = elecResults.prodEner;
                        consoEnr = elecResults.consoEnr;
                        rateCoverage = elecResults.rateCoverage;
                    }

                    // Update target values for "enr-elec"
                    Object.values(configuration.impacts).forEach((impact) => {
                        if (
                            impact.impactType === "enr-elec" &&
                            impact.defaultValue?.target
                        ) {
                            impact.defaultValue.target["impact-energy"].value =
                                consoEnr;
                            impact.defaultValue.target["impact-production"].value =
                                prodEner;
                            impact.defaultValue.target["coverage-rate"].value =
                                rateCoverage;
                        }
                    });
                    break;

                default:
                    console.warn(`Impact type ${impactType} is not handled`);
                    break;
            }
        });

        // Update state
        this.setState({ configuration });
    };

    getTerritoryName() {
        if (this.state.isNational)
            return (
                this.props.parentApi.data.region.charAt(0).toUpperCase() +
                this.props.parentApi.data.region.slice(1)
            );

        let territoryName = this.props.parentApi.controller.zonesManager.getZoneName(
            this.props.parentApi.data.currentZone,
            this.props.parentApi.data.zone
        );
        if (
            !this.props.parentApi.controller.zonesManager.getZoneName(
                this.props.parentApi.data.currentZone,
                this.props.parentApi.data.zone
            )
        ) {
            territoryName = this.props.parentApi.data.territoryName;
        }
        if (this.props.parentApi.data.zone.zone === "region") {
            territoryName = this.props.parentApi.data.settings.label;
        }

        return territoryName;
    }

    getDashboardURL() {
        if (this.state.isNational) return "";
        let territoryType = "?zone=" + this.props.parentApi.data.zone.zone;
        let maille = "&maille=" + this.props.parentApi.data.zone.maille;
        let codeInseeTerritory = "&zone_id=" + this.props.parentApi.data.currentZone;
        let idDashboard = "&id_tableau=" + this.props.currentDashboard;
        let territoryName = "&nom_territoire=" + this.getTerritoryName();
        if (this.props.parentApi.data.zone.zone === "region") {
            codeInseeTerritory = "&zone_id=" + this.props.parentApi.data.regionCode;
        }

        let url =
            territoryType + maille + codeInseeTerritory + idDashboard + territoryName;
        return url;
    }

    render() {
        // Methodo
        let pdfUrl = createPdfMethodoLink(
            config.methodo_url,
            this.props.parentApi.data.region,
            configData.methodoSimulateurPdf
        );
        let methodoPdf = (
            <a
                href={pdfUrl}
                target="_blank"
                title="Note méthodologique"
                rel="noreferrer"
            >
                <div className="help"></div>
            </a>
        );
        let returnButton = (
            <Link to="/">
                <button
                    type="button"
                    className="close close-big"
                    data-dismiss="alert"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </Link>
        );
        if (this.state.isNational) {
            returnButton = (
                <div>
                    <Link
                        to={
                            "/national/" + this.props.parentApi.data.region + "/portail"
                        }
                        className="btn btn-light return-button"
                    >
                        <i className="bi bi-arrow-left"></i>
                    </Link>
                </div>
            );
        }
        if (this.state.errorStatus) {
            return (
                <div className="widgets full-screen-widget">
                    {returnButton}
                    <div className="tout">
                        <div className="support">
                            <div className="corps">
                                <p>
                                    Désolé, l'application rencontre actuellement un
                                    problème :
                                    <br />
                                    <code>{this.state.errorMsg}</code>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
        if (!this.state.configuration.data) {
            return (
                <div className="widgets full-screen-widget">
                    {this.props.parentApi.data.settings && (
                        <SEO
                            settings={this.props.parentApi.data.settings["seo"]}
                            page="simulator"
                        />
                    )}
                    <div className={"page-content"}>
                        <div className="loader centered-widget"></div>
                    </div>
                </div>
            );
        }
        return (
            <div className="widgets full-screen-widget">
                {this.props.parentApi.data.settings && (
                    <SEO
                        settings={this.props.parentApi.data.settings["seo"]}
                        page="simulator"
                    />
                )}
                {returnButton}
                {methodoPdf}
                <div className="simulator structure-fiche-didactique">
                    <h2> Simulateur d’impacts énergie renouvelable </h2>
                    <h3>[Territoire : {this.getTerritoryName()}]</h3>
                    &nbsp;
                    <div className="block-category-restitution">
                        <p>
                            <em>Objectif :</em> donner les ordres de grandeur d’impacts
                            associés au développement des énergies renouvelables
                        </p>
                        <br /> <br />
                        {this.getInitialSetting()}
                        <br />
                        <div className="row-simulateur">
                            <div className="column-simulateur">
                                {this.state.configuration && this.addCategory()}
                            </div>
                            <div className="column-simulateur right-column-simulateur">
                                {this.addImpact()}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SimulatorEnR;
