/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

/**
 * This file gathers the intermediate calculations of energy consumption and production useful for the simulator
 */

import { get_input_param } from "../utils.js";

export const getResults = (initialSettings, configuration, type) => {
    // Total energy production calculation (prod_ener)
    let prodEner = 0;

    // Iterate through all categories in "data" and their levers
    Object.keys(configuration.data).forEach((dataKey) => {
        const dataCategory = configuration.data[dataKey];
        const levers = dataCategory.levers;

        // Iterate through each lever
        Object.keys(levers).forEach((leverKey) => {
            const lever = levers[leverKey];

            lever.actions.forEach((action) => {
                // Skip if it's not an electric action
                if (
                    type === "enr-elec" &&
                    action.defaultValue.type &&
                    action.defaultValue.type !== "elec"
                ) {
                    return;
                }

                // Function to add productions, handling arrays and types
                const addProduction = (bilan) => {
                    if (!bilan.production_annuelle) return;

                    if (Array.isArray(bilan.production_annuelle)) {
                        bilan.production_annuelle.forEach((value, index) => {
                            if (
                                !bilan.type ||
                                type !== "enr-elec" ||
                                bilan.type[index] === "elec"
                            ) {
                                prodEner += value;
                            }
                        });
                    } else {
                        prodEner += bilan.production_annuelle;
                    }
                };

                // Process sub-actions if they exist
                if (action.defaultValue.sub_action) {
                    Object.values(action.defaultValue.sub_action).forEach(
                        (subAction) => {
                            if (
                                type === "enr-elec" &&
                                subAction.type &&
                                !subAction.type.includes("elec")
                            ) {
                                return; // Skip non-electric sub-actions
                            }
                            subAction.bilan_data.forEach(addProduction);
                        }
                    );
                } else {
                    // Add main production
                    action.defaultValue.bilan_data.forEach(addProduction);
                }
            });
        });
    });

    // Calculate consumption
    let consoEnr = 0;

    // Filter for the impact of type "global-results" or "enr-elec"
    const globalResultImpact = Object.values(configuration.impacts).find(
        (impact) => impact.impactType === type
    );

    if (
        globalResultImpact &&
        globalResultImpact.defaultValue.current["impact-energy"]
    ) {
        const impactEnergyDefault =
            globalResultImpact.defaultValue.current["impact-energy"].default;
        consoEnr =
            impactEnergyDefault *
            (1 +
                (type === "enr-elec"
                    ? initialSettings.targetReductionConsumptionElectricity
                    : initialSettings.targetReductionConsumption) /
                    100);
    }

    // Calculate coverage rate
    let rateCoverage = consoEnr > 0 ? (prodEner / consoEnr) * 100 : 0;

    return { prodEner, consoEnr, rateCoverage };
};

export const updateProductionFromPuissance = (
    action,
    genericInputParam,
    specificInputParam,
    item
) => {
    if (!item || item.puissance_annuelle === undefined) return null;

    // Retrieve input parameters with default fallbacks to prevent undefined values
    const PHOTOVOLTAIC_LOSSES =
        get_input_param(
            genericInputParam,
            "production",
            "PHOTOVOLTAIC_SYSTEM_LOSSES_CANOPIES"
        ) || 0;
    const WIND_CAPACITY_FACTOR =
        get_input_param(
            genericInputParam,
            "production",
            "LAND_BASED_WIND_TURBINE_CAPACITY_FACTOR"
        ) || 0;
    const HYDRO_CAPACITY_FACTOR =
        get_input_param(
            genericInputParam,
            "production",
            "SMALL_HYDROELECTRIC_CAPACITY_FACTOR"
        ) || 0;

    const globalIrradiation = specificInputParam["global-irradiation"] || 0;
    const power = item.puissance_annuelle;

    // Compute production values based on power
    const productionValues = {
        "1a": ((power * globalIrradiation) / 1000) * (1 - PHOTOVOLTAIC_LOSSES / 100),
        "2a": (power * (WIND_CAPACITY_FACTOR / 100) * 8760) / 1000,
        "3a": (power * (HYDRO_CAPACITY_FACTOR / 100) * 8760) / 1000,
    };

    return productionValues[action.number] || null; // Return null if action.number is invalid
};

export const updatePuissanceFromProduction = (
    action,
    genericInputParam,
    specificInputParam,
    item
) => {
    if (!item || item.production_annuelle === undefined) return null;

    // Retrieve input parameters with default fallbacks to prevent undefined values
    const PHOTOVOLTAIC_LOSSES =
        get_input_param(
            genericInputParam,
            "production",
            "PHOTOVOLTAIC_SYSTEM_LOSSES_ROOFTOP"
        ) || 0;
    const WIND_CAPACITY_FACTOR =
        get_input_param(
            genericInputParam,
            "production",
            "LAND_BASED_WIND_TURBINE_CAPACITY_FACTOR"
        ) || 1; // Avoid division by zero
    const HYDRO_CAPACITY_FACTOR =
        get_input_param(
            genericInputParam,
            "production",
            "SMALL_HYDROELECTRIC_CAPACITY_FACTOR"
        ) || 1; // Avoid division by zero

    const globalIrradiation = specificInputParam["global-irradiation"] || 1; // Prevent division by zero
    const production = item.production_annuelle;

    // Compute puissance values based on production
    const puissanceValues = {
        "1a": (production * 1000) / globalIrradiation / (1 - PHOTOVOLTAIC_LOSSES / 100), // Photovoltaic
        "2a": (production * 1000) / (WIND_CAPACITY_FACTOR / 100) / 8760, // Wind
        "3a": (((production * 1000) / HYDRO_CAPACITY_FACTOR) * 100) / 8760, // Hydro
    };

    return puissanceValues[action.number] || null; // Return null if action.number is invalid
};
