import React from "react";
import chroma from "chroma-js";
import NumberFormat from "react-number-format";

import styles from "./ContinuousColorScaleLegend.module.css";

/**
 * Renders a vertical continuous color scale
 *
 * @param {Object} props
 * @param {string} props.colorStart
 * @param {string} props.colorEnd
 * @param {number} props.min
 * @param {number} props.max
 * @param {string} props.unit
 */
function ContinuousColorScaleLegend({ colorStart, colorEnd, min, max, unit }) {
    const nSteps = 100;
    const stepHeight = "1%";
    const tickPositions = [0, 50, 100];

    const colorScale = chroma.scale([colorStart, colorEnd]).mode("lch");
    const colors = colorScale.colors(nSteps);

    return (
        <div className={styles.colorScaleContainer}>
            <div className={styles.gradient}>
                {colors.reverse().map((c, i) => (
                    <span
                        key={i}
                        className={styles.gradientStep}
                        style={{ backgroundColor: c, height: stepHeight }}
                    ></span>
                ))}
            </div>
            <div className={styles.ticks}>
                {tickPositions.map((pos) => (
                    <div
                        key={pos}
                        className={styles.tick}
                        style={{ top: `${100 - pos}%` }}
                    >
                        <NumberFormat
                            value={((max - min) * pos) / 100 + min}
                            displayType={"text"}
                            thousandSeparator={" "}
                        />{" "}
                        <span>{unit}</span>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default ContinuousColorScaleLegend;
