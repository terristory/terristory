/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";

import Charts from "./Charts";
import CollapsableBottomPanel from "./CollapsableBottomPanel";

/**
 * @param {Object} props
 * @param {import('../../types/types.js').Api} props.parentApi
 * @param {Object} props.indicatorConfig
 *
 * @returns {JSX.Element | null}
 */
export function ChartsContainer({ parentApi, indicatorConfig }) {
    if (!indicatorConfig) return null;

    /** @type {import("../../types/apiTypes").IndicatorChartConfig[]} */
    const indicatorChartConfigs = indicatorConfig.charts;
    if (!indicatorChartConfigs || indicatorChartConfigs.length === 0) return null;

    // Filter out types that are marked as not visible in the database or that are displayed separately in the right sidebar
    let chartsToShow = indicatorChartConfigs.filter(
        (chartConfig) =>
            !["switch-button", "selection", "slider"].includes(chartConfig.type) &&
            chartConfig.visible
    );

    // If `display_total` is false (i.e., the data shouldn't be aggregated), charts are displayed
    // only when the selected zone matches the smallest data subdivision. This prevents
    // displaying aggregated values in the bottom panel.
    const selectedZoneType = parentApi.data.zone.zone;
    if (
        !indicatorConfig.display_total &&
        ((!indicatorConfig.only_for_zone && selectedZoneType !== "commune") ||
            (indicatorConfig.only_for_zone &&
                selectedZoneType !== indicatorConfig.only_for_zone))
    )
        return null;

    if (chartsToShow.length === 0) return null;

    const charts = chartsToShow
        .sort((a, b) => a.ordre - b.ordre)
        .map((chartConfig) => {
            let uniqueChartId = `${chartConfig.indicateur}${chartConfig.categorie}`;
            return (
                <Charts
                    key={uniqueChartId}
                    parentApi={parentApi}
                    id={uniqueChartId}
                    chartConfig={chartConfig}
                    categorie={chartConfig.categorie}
                />
            );
        });

    return (
        <CollapsableBottomPanel fullWidth={!parentApi.data.isLeftMenuOpen}>
            {charts}
        </CollapsableBottomPanel>
    );
}

export default ChartsContainer;
