/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect, useCallback } from "react";
import { slugify } from "../../utils";

const IframeGenerator = (props) => {
    const [iframeLink, setIframeLink] = useState("");
    const [display, setDisplay] = useState(false);
    const [copied, setCopied] = useState(false);
    const [shareableLink, setShareableLink] = useState("");
    const [linkCopied, setLinkCopied] = useState(false);
    const resetTime = 10000;

    // Function to generate iframe link
    const handleGenerateIframe = useCallback(() => {
        const domainName = window.location.origin;
        const urlArgs = props.parentApi.data.urlPartageable;
        const siteUrl = domainName + urlArgs + "&embedded=true";
        const checkedPoi = props.parentApi.data.poiLayers?.find(
            (poiLayer) => poiLayer.checked
        );
        const analysis = props.parentApi.data.analysis
            ? props.parentApi.controller.analysisManager.getAnalysisName(
                  props.parentApi.data.analysis
              )
            : undefined;

        const title = analysis || checkedPoi?.name || "TerriSTORY";
        const slug_title = slugify(title);
        const iframeCode = `<iframe id="${slug_title}-iframe" title="${title}" src="${siteUrl}" width="900" height="500" frameborder="0"></iframe>`;
        setIframeLink(iframeCode);
        setShareableLink(domainName + urlArgs);
    }, [props.parentApi]);

    // Function to copy iframe link to clipboard
    const copyToClipboard = () => {
        navigator.clipboard.writeText(iframeLink);
        setCopied(true);
        setLinkCopied(false);

        // Reset copied state
        setTimeout(() => {
            setCopied(false);
        }, resetTime);
    };

    // Function to copy shareable link to clipboard
    const copyShareableLink = () => {
        navigator.clipboard.writeText(shareableLink);
        setLinkCopied(true);
        setCopied(false);

        // Reset copied state
        setTimeout(() => {
            setLinkCopied(false);
        }, resetTime);
    };

    // Function to toggle display and reset copied state when closing
    const toggleDisplayAndGenerate = () => {
        if (!display) {
            handleGenerateIframe();
        }
        setDisplay(!display);
    };

    // Effect to update iframe and reset copied state when analysis changes
    useEffect(() => {
        if (props.parentApi.data.analysis !== undefined) {
            handleGenerateIframe();
        }
    }, [props.parentApi.data.analysis, handleGenerateIframe]);

    // Check if any poiLayer is checked
    const isPoi =
        props.parentApi.data.poiLayers?.some((poiLayer) => poiLayer.checked) ?? false;

    // Return null if neither analysis nor any poiLayer is checked
    if (!props.parentApi.data.analysis && !isPoi) {
        return null;
    }

    return (
        <div className={`iframe float-widget ${display ? "" : "iframe-closed"}`}>
            <button
                type="button"
                className={display ? "iframe-close-button" : "iframe-open-button"}
                aria-label="Masquer/Afficher"
                title={(display ? "Masquer" : "Générer") + " un lien iframe"}
                onClick={toggleDisplayAndGenerate}
            >
                {!display && <i className="bi bi-code"></i>}
                {display && <div className="panel-close-icon" aria-hidden="true"></div>}
            </button>

            {display && (
                <div className="infos-content">
                    {iframeLink && (
                        <div>
                            <h4 style={{ fontSize: "1em", fontWeight: "bold" }}>
                                Pour intégrer cette application dans un site, vous
                                pouvez copier le code suivant dans le contenu HTML de
                                votre site.
                            </h4>
                            <div>
                                <button
                                    className="btn iframe-button"
                                    onClick={copyToClipboard}
                                    disabled={copied}
                                >
                                    <i className="bi bi-copy">
                                        {copied ? " Copié !" : " Copier"}
                                    </i>
                                </button>
                            </div>
                            <br />
                            <code>{iframeLink}</code>
                        </div>
                    )}
                    <br />
                    {shareableLink && (
                        <div>
                            <h4 style={{ fontSize: "1em", fontWeight: "bold" }}>
                                Partager le lien de cette page :
                            </h4>
                            <button
                                className="btn iframe-button"
                                onClick={copyShareableLink}
                                disabled={linkCopied}
                            >
                                <i className="bi bi-copy">
                                    {linkCopied ? " Copié !" : " Copier"}
                                </i>
                            </button>
                            <br />
                            <code>{shareableLink}</code>
                        </div>
                    )}
                </div>
            )}
        </div>
    );
};

export default IframeGenerator;
