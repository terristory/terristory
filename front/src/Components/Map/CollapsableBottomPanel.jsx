/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React, { useState } from "react";

import styles from "./CollapsableBottomPanel.module.css";

function CollapsableBottomPanel({ children, fullWidth }) {
    const [isPanelOpen, setIsPanelOpen] = useState(true);

    const panelClassName = `${styles.panel} ${
        isPanelOpen ? styles.panelOpen : styles.panelCollapsed
    }`;

    const toggleIconClassName = `${styles.panelToggleIcon} ${
        !isPanelOpen ? styles.panelToggleIconCollapsed : ""
    }`;

    const scrollableContainerClassName = `chartsContainer ${
        styles.scrollableContainer
    } ${fullWidth ? styles.fullWidth : ""}`;

    return (
        <div className={panelClassName} data-testid="bottom-panel">
            <button
                type="button"
                className={styles.panelToggleButton}
                aria-label="Masquer/Afficher"
                onClick={() => setIsPanelOpen((prev) => !prev)}
            >
                <div className={toggleIconClassName} aria-hidden="true"></div>
            </button>
            <div
                className={scrollableContainerClassName}
                data-testid="bottom-panel-scrollable-container"
            >
                {children}
            </div>
        </div>
    );
}

export default CollapsableBottomPanel;
