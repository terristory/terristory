/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import NumberFormat from "react-number-format";

import config from "../../settings";
import { consulteAutrePage, createPdfMethodoLink } from "../../utils";
import { ExportIndicatorButton } from "../ExportButton";

import "bootstrap/dist/css/bootstrap.min.css";
import CategorySingleSelect from "../CategorySingleSelect";

/**
 * Component to display a select input with available years from an analysis parameters.
 * @param {Object} props
 * @param {number[]} props.years a list of all available years for this analysis
 * @param {number[]} props.estimatedYears,
 * @param {number} props.currentYear if years is empty, will contain the value to display
 * @param {number} props.yearSelected contains the currently selected year
 * @param {function(number):void} props.onChangeCallback triggered with new integer value when the select value is changed.
 * @param {'selection' | 'slider' | 'switch-button'} props.inputType - if not specified, 'selection' by default
 * @param {Object.<number, string>} [props.customYearLabels]
 * @param {string} [props.id] - the html id for the input
 */
function AnalysisYearsSelection({
    years,
    estimatedYears,
    currentYear,
    yearSelected,
    onChangeCallback,
    inputType = "selection",
    customYearLabels,
    id,
}) {
    // by default use dropdown input if not specified
    if (!["slider", "selection", "switch-button"].includes(inputType))
        inputType = "selection";

    if (years && Array.isArray(years) && years.length > 1) {
        let year = yearSelected ?? years[0];
        let options = years.map((year, i) => {
            const isEstimated = estimatedYears && estimatedYears.includes(year);

            let yearLabel = customYearLabels?.[year] ?? String(year);
            if (isEstimated) yearLabel += " (e)";

            return { label: yearLabel, value: year };
        });
        return (
            <>
                <CategorySingleSelect
                    category={"annee"}
                    options={options}
                    value={year} // use as controlled component
                    onChange={(selectedOption) => {
                        onChangeCallback(selectedOption);
                    }}
                    inputType={inputType}
                    id={id}
                ></CategorySingleSelect>
            </>
        );
    } else if (currentYear) {
        return <span id={id}>{currentYear}</span>;
    } else {
        return "";
    }
}
/**
 * Component to display a select input with available units.
 *
 * @param {Array} units a list of all available units for this analysis
 * @param {integer} singleUnit if units is empty, will contain the value to display
 * @param {integer} unitSelected contains the currently selected unit
 * @param {callable} onChangeCallback triggered with new integer value when the select value is changed.
 */
export function AnalysisUnitSelection({
    units,
    singleUnit,
    zoneName,
    unitSelected,
    onChangeCallback,
    hiddenUnits,
}) {
    if (units && Object.keys(units).length > 0) {
        return (
            <select
                onChange={(e) => onChangeCallback(parseInt(e.target.value, 10))}
                value={unitSelected}
            >
                {units.map((unit) => {
                    // no need to check, hidden units should have the same keys than units
                    if (hiddenUnits[unit.unit_id].split(",").includes(zoneName)) {
                        return "";
                    }
                    return (
                        <option
                            key={"analysis-unit-" + unit.unit_id}
                            value={unit.unit_id}
                        >
                            {unit.unit_name}
                        </option>
                    );
                })}
            </select>
        );
    } else if (singleUnit) {
        return singleUnit;
    } else {
        return "";
    }
}

/**
 * Ce composant permet d'afficher une infobulle.
 */
class Infos extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            display: true,
            nomInstallationCourante: props.parentApi.data.nomInstallationCourante,
            nomIndicateurCourant: props.parentApi.data.nomIndicateurCourant,
            indicateurOuInstallation: undefined,
            poiLayersCheckedLabel: [],
            poiLayersCheckedRubrique: [],
            uiTheme: undefined,
            indicateurActif: false,
            analysisSelectedYear: props.parentApi.data.analysisSelectedYear,
            analysisSelectedUnit: props.parentApi.data.analysisSelectedUnit,
        };
        this.toggleDisplay = this.toggleDisplay.bind(this);
    }

    componentDidUpdate(prevProps) {
        const {
            nomInstallationCourante,
            nomIndicateurCourant,
            poiLayers,
            uiTheme,
            analysis,
            analysisSelectedYear,
            analysisSelectedUnit,
            provenance,
        } = this.props.parentApi.data;

        let indicateurOuInstallation;
        let poiLayerLabel = [];
        let poiLayerRubrique = [];
        let finalUiTheme = this.state.uiTheme;
        const updates = {}; // Accumulate state updates to call setState once

        // Handle changes in poiLayers
        if (prevProps.parentApi.data.poiLayers !== poiLayers) {
            for (let poiLayer of poiLayers) {
                if (poiLayer.checked) {
                    poiLayerLabel.push(poiLayer.label);
                    poiLayerRubrique.push(poiLayer.theme);
                }
            }
            if (
                poiLayerLabel.join() !== this.state.poiLayersCheckedLabel.join() ||
                poiLayerRubrique.join() !== this.state.poiLayersCheckedRubrique.join()
            ) {
                updates.poiLayersCheckedLabel = poiLayerLabel;
                updates.poiLayersCheckedRubrique = poiLayerRubrique;
            }
        }

        // Set indicateurActif to true when analysis exists (regardless of whether it changed)
        if (
            analysis !== undefined &&
            (prevProps.parentApi.data.analysis !== analysis ||
                !this.state.indicateurActif)
        ) {
            updates.indicateurActif = true;
            updates.poiLayersCheckedLabel = []; // Reset for new analysis
            updates.analysisSelectedYear = undefined;
            updates.analysisSelectedUnit = undefined;
            indicateurOuInstallation = nomIndicateurCourant;
        }

        // Handle changes in analysisSelectedYear and analysisSelectedUnit
        if (
            prevProps.parentApi.data.analysisSelectedYear !== analysisSelectedYear &&
            analysisSelectedYear === undefined &&
            this.state.analysisSelectedYear !== undefined
        ) {
            updates.analysisSelectedYear = undefined;
        }

        if (
            prevProps.parentApi.data.analysisSelectedUnit !== analysisSelectedUnit &&
            analysisSelectedUnit === undefined &&
            this.state.analysisSelectedUnit !== undefined
        ) {
            updates.analysisSelectedUnit = undefined;
        }

        // Handle changes in nomInstallationCourante
        if (
            prevProps.parentApi.data.nomInstallationCourante !==
                nomInstallationCourante &&
            !this.state.indicateurActif
        ) {
            indicateurOuInstallation = nomInstallationCourante;

            // If the current installation value is undefined, use the first POI label if available
            if (nomInstallationCourante === undefined) {
                indicateurOuInstallation = poiLayerLabel[0] || undefined;
            }
        } else if (
            prevProps.parentApi.data.nomInstallationCourante ===
                nomInstallationCourante &&
            !this.state.indicateurActif &&
            poiLayerLabel.length > 0
        ) {
            indicateurOuInstallation = poiLayerLabel[0];
        }

        // Update indicateurOuInstallation if it changes
        if (
            indicateurOuInstallation &&
            indicateurOuInstallation !== this.state.indicateurOuInstallation
        ) {
            updates.indicateurOuInstallation = indicateurOuInstallation;
        }

        // Handle changes in uiTheme
        if (
            prevProps.parentApi.data.uiTheme !== uiTheme ||
            poiLayerRubrique.length > 0
        ) {
            if (!this.state.indicateurActif && poiLayerRubrique.length > 0) {
                finalUiTheme = uiTheme || poiLayerRubrique[0];
            } else if (this.state.indicateurActif && poiLayerRubrique.length === 0) {
                finalUiTheme = uiTheme;
            } else if (uiTheme === undefined && poiLayerRubrique.length === 0) {
                finalUiTheme = prevProps.parentApi.data.uiTheme;
            }
        }

        if (
            (finalUiTheme &&
                finalUiTheme !== this.state.uiTheme &&
                provenance !== "sub_indicator") ||
            (!finalUiTheme && uiTheme !== this.state.uiTheme)
        ) {
            updates.uiTheme = finalUiTheme || uiTheme;
        }

        // Apply all accumulated updates in one setState call to avoid multiple re-renders
        if (Object.keys(updates).length > 0) {
            this.setState(updates);
        }
    }

    toggleDisplay() {
        this.setState((prevState) => ({
            display: !prevState.display,
        }));
    }

    /**
     * Check we changed year and, if new year is valid (inside available years)
     * for current analysis, will trigger updateSelectedYearAnalysis from parent api.
     *
     * @param {integer} newYear new value for selected year
     */
    updateAnalysisSelectedYear(newYear) {
        if (newYear !== this.state.analysisSelectedYear) {
            let years =
                this.props.parentApi.data &&
                this.props.parentApi.controller.analysisManager
                    ? this.props.parentApi.controller.analysisManager.getAnalysisYears(
                          this.props.parentApi.data.analysis
                      )
                    : [];
            if (years && Array.isArray(years) && years.includes(newYear)) {
                this.setState({
                    analysisSelectedYear: newYear,
                });
                this.props.parentApi.callbacks.updateSelectedYearAnalysis(newYear);
            }
        }
    }

    /**
     * Check we changed unit and, if new unit is valid (inside available units)
     * for current analysis, will trigger updateSelectedUnitAnalysis from parent api.
     *
     * @param {integer} newUnit new value for selected unit
     */
    updateAnalysisSelectedUnit(newUnit) {
        if (newUnit !== this.state.analysisSelectedUnit) {
            let units = Object.keys(
                this.props.parentApi.data?.analysisMeta?.unitsDefaultDetails || {}
            );
            if (units && Array.isArray(units) && units.includes(newUnit.toString())) {
                this.setState({
                    analysisSelectedUnit: newUnit,
                });
                this.props.parentApi.callbacks.updateSelectedUnitAnalysis(newUnit);
            }
        }
    }

    suiviConsultation(text) {
        let region = this.props.parentApi.data.region;
        let idUtilisateur =
            this.props.parentApi.controller.gestionSuiviConsultations.idUtilisateur;
        let url = config.api_consultation_autre_page_url;
        let nomPdf = text;
        consulteAutrePage(
            url,
            region,
            idUtilisateur,
            "Indicateur - PDF méthodologique",
            nomPdf
        );
        return false;
    }

    /**
     * Build html legend for current analysis sources and providers
     */
    buildSource() {
        if (this.props.parentApi.data.analysisMeta) {
            let meta = this.props.parentApi.data.analysisMeta;
            return (
                <div className="row">
                    <div className="content sources-analysis">
                        <DataSource
                            label={
                                meta.creditsDataSources?.length > 1
                                    ? "Sources des données"
                                    : "Source des données"
                            }
                            tableSources={meta.creditsDataSources}
                        />
                        <DataSource
                            label={
                                meta.creditsDataProducers?.length > 1
                                    ? "Producteurs des données"
                                    : "Producteur des données"
                            }
                            tableSources={meta.creditsDataProducers}
                        />
                        <DataSource
                            label={
                                meta.creditsAnalysisProducers?.length > 1
                                    ? "Producteurs de l'indicateur"
                                    : "Producteur de l'indicateur"
                            }
                            tableSources={meta.creditsAnalysisProducers}
                        />
                    </div>
                </div>
            );
        }
        return "";
    }

    /**
     * Build html inputs (select, switch, slider) to select category when manipulating categories
     *
     * @returns {JSX.Element[]}
     */
    categorySelectionInputs() {
        let output = [];

        const analysisManager = this.props.parentApi.controller?.analysisManager;
        const analysisData = this.props.parentApi.data.analysis;

        if (!analysisManager || !analysisData) {
            return;
        }

        let currentIndicator = analysisManager.analysis?.find(
            (a) => a.id === analysisData
        );

        if (!currentIndicator) return;

        currentIndicator.charts
            .filter((chart) =>
                ["switch-button", "selection", "slider"].includes(chart.type)
            )
            .forEach((chart) => {
                let data = analysisManager.getDataChart(
                    analysisData,
                    analysisData + chart.categorie
                );

                if (!data?.data?.datasets?.[0]?.data) {
                    return;
                }

                // Build options list
                let options = [];
                for (let i in data.data.datasets[0].data) {
                    if (data.data.labels[i] === "indisponible") {
                        continue;
                    }
                    options.push({
                        label: data.data.labels[i],
                        value: data.data.labels[i],
                    });
                }

                // Get current selected option
                let selectedOption =
                    this.props.parentApi.data.localMapFilter?.[
                        data.data.categorie
                    ]?.[0]?.filtre_categorie?.split(".")[1];

                output.push(
                    <CategorySingleSelect
                        key={data.data.categorie}
                        category={data.data.categorie}
                        options={options}
                        defaultValue={selectedOption}
                        onChange={(selectedOption) => {
                            this.props.parentApi.callbacks.updateMapFilter({
                                category: data.data.categorie,
                                set: [`${data.data.categorie}.${selectedOption}`],
                            });
                        }}
                        inputType={data.data.type}
                        title={data.data.titre}
                        tooltip={data.data.description}
                        isLoading={!this.props.parentApi.data.dataLoaded}
                    ></CategorySingleSelect>
                );
            });
        return output;
    }

    render() {
        // Get zone selected
        let res = "";
        let sum = "";
        let zoneInfos = "";
        let source = this.buildSource();

        let zoneName = this.props.parentApi.controller.zonesManager.getZoneName(
            this.props.parentApi.data.currentZone,
            this.props.parentApi.data.zone
        );

        if (this.props.parentApi.data.zone.zone === "region") {
            zoneInfos = this.props.parentApi.data.regionLabel;
        } else {
            zoneInfos = zoneName ? zoneName : "";
        }

        let meta = this.props.parentApi.data.analysisMeta;

        if (
            meta &&
            meta.sum &&
            meta.display_total &&
            this.props.parentApi.data.analysis
        ) {
            sum = meta.sum;
            if (Number(sum) === 0) {
                sum = "";
            }
            if (meta.sum === "confidentiel") {
                sum = "confidentiel";
            }
        }

        if (
            this.props.parentApi.data.infos !== "" ||
            this.props.parentApi.data.analysis !== undefined ||
            this.props.parentApi.data.zone.zone ||
            this.props.parentApi.data.zone.maille
        ) {
            let methodoPdf = "";
            let pdfUrl = "";
            let pdfName =
                this.props.parentApi.controller.analysisManager.getAnalysisMethodoPdf(
                    this.props.parentApi.data.analysis
                );

            if (pdfName) {
                pdfUrl = createPdfMethodoLink(
                    config.methodo_url,
                    this.props.parentApi.data.region,
                    pdfName
                );
                methodoPdf = (
                    <a
                        href={pdfUrl}
                        onClick={() => this.suiviConsultation(pdfName)}
                        title="Méthodologie"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <div className="pdf"></div>
                    </a>
                );
            }

            let classe = "infos float-widget";
            if (
                (meta && meta.dataDeuxiemeRepresentation) ||
                (meta && meta.afficherVersionSimple)
            ) {
                classe = "infos infos-double-width float-widget";
            }

            // Handle years
            let years = [];
            let estimatedYears = [];

            const indicatorConfig =
                this.props.parentApi.controller.analysisManager.analysis.find(
                    (a) => a.id === parseInt(this.props.parentApi.data.analysis, 10)
                );

            let isSelectedYearEstimated = false;
            let yearSelectionInput = indicatorConfig?.year_selection_input_type;

            // Custom year labels are defined in the indicator_representation_detail table
            // and allow displaying custom text instead of the year.
            let customYearLabels =
                indicatorConfig?.representation_details?.customYearLabels;

            if (customYearLabels) {
                try {
                    customYearLabels = JSON.parse(customYearLabels);
                } catch {
                    customYearLabels = undefined;
                }
            }

            let currentYear =
                this.props.parentApi.controller.analysisManager.getAnalysisLastYear(
                    this.props.parentApi.data.analysis
                );
            if (
                this.props.parentApi.data &&
                this.props.parentApi.controller.analysisManager
            ) {
                years =
                    this.props.parentApi.controller.analysisManager.getAnalysisYears(
                        this.props.parentApi.data.analysis
                    );
                estimatedYears =
                    this.props.parentApi.controller.analysisManager.getAnalysisEstimatedYears(
                        this.props.parentApi.data.analysis
                    );
                isSelectedYearEstimated = estimatedYears.includes(
                    this.state.analysisSelectedYear ?? currentYear
                );
            }

            let ratioYearImprecise = false;
            if (
                meta &&
                meta.ratio_best_year &&
                meta.ratio_best_year !== this.state.analysisSelectedYear
            ) {
                ratioYearImprecise = (
                    <div className="row warning-ratio-year">
                        Attention, les données utilisées pour le calcul du ratio sont
                        issues de l'année {meta.ratio_best_year} contrairement aux
                        données absolues qui sont bien dans l'année sélectionnée
                        ci-dessus.
                    </div>
                );
            }

            // Handle categories
            const numberOfThemes =
                (this.state.indicateurActif ? 1 : 0) +
                this.state.poiLayersCheckedRubrique.length;

            const unitsParams =
                this.props.parentApi.controller.analysisManager.getUnitParamsForIndicator(
                    this.state.analysisSelectedUnit,
                    meta,
                    this.props.parentApi.data.zone.maille
                );
            let { unit } = unitsParams;
            const { relevantUnits, defaultUnitForThisZoneType } = unitsParams;

            // Title
            const noteTitle = (
                <>
                    Analyse :{" "}
                    {this.state.indicateurOuInstallation ??
                        this.props.parentApi.controller.analysisManager.getAnalysisName(
                            this.props.parentApi.data.analysis
                        )}
                    {this.state.poiLayersCheckedLabel.length > 1 &&
                        this.state.indicateurActif === false &&
                        ", ..."}
                    <br />
                    {this.state.uiTheme && (
                        <>
                            Thématique : {this.state.uiTheme}
                            {numberOfThemes > 1 && ", ..."}
                            <br style={{ display: "block", marginBottom: "2%" }} />
                        </>
                    )}
                    {zoneInfos !== "" ? "Territoire : " + zoneInfos : ""}
                    {this.props.parentApi.data.embedded && (
                        <>
                            <br />
                            Échelle : {this.props.parentApi.data.zone.zone}
                            <br />
                            Maille :{this.props.parentApi.data.zone.maille}
                        </>
                    )}
                </>
            );

            let libelleTotal = "Total ";
            if (meta) {
                if (meta && this.props.parentApi.data.analysis) {
                    if (sum === "confidentiel" || sum === "") {
                        unit = "";
                    }
                }
                if (
                    meta.moyennePonderee ||
                    ["accessibilite_emploi", "depenses_carburant_menage"].includes(
                        meta.data_type
                    )
                ) {
                    libelleTotal = "Moyenne ";
                }
            }
            let total_label = "Total : ";
            let total = "";

            if (
                typeof this.props.parentApi.data.zone.maille !== "undefined" &&
                sum !== ""
            ) {
                // Généraliser pour toutes les mailles et résoudre le problème d'affichage
                total_label = libelleTotal + " : ";
                total = (
                    <div className="row">
                        <label>
                            <b>{total_label}</b>{" "}
                            <NumberFormat
                                value={sum}
                                displayType={"text"}
                                thousandSeparator={" "}
                            />{" "}
                            {unit}{" "}
                        </label>
                        <div className="content"></div>
                    </div>
                );
            }

            let donneesExportables = [];
            if (meta) {
                if (meta.sum === "confidentiel") {
                    total = (
                        <div className="row">
                            <label>
                                {total_label} {"confidentiel"}{" "}
                            </label>
                            <div className="content"></div>
                        </div>
                    );
                }

                if (meta.donnees_exportables) {
                    donneesExportables = meta.donnees_exportables.split(",");
                }

                if (["accessibilite_emploi"].includes(meta.data_type)) {
                    currentYear = "Cf. Méthodologie de l'indicateur";
                }
            }
            let exportCSV = "";
            if (
                donneesExportables.indexOf(this.props.parentApi.data.zone.maille) !== -1
            ) {
                exportCSV = (
                    <ExportIndicatorButton
                        parentApi={this.props.parentApi}
                        analysisSelectedYear={this.state.analysisSelectedYear}
                        className="csv csv-float-widget"
                    />
                );
            }

            // potential categories requiring some selection input inside Infos module
            const selectionInputs = this.categorySelectionInputs();

            res = (
                <div className={classe + (this.state.display ? "" : " infos-closed")}>
                    <button
                        type="button"
                        className={
                            this.state.display
                                ? "infos-close-button"
                                : "infos-open-button"
                        }
                        aria-label="Masquer/Afficher"
                        title={(this.state.display ? "Masquer" : "Afficher") + " infos"}
                        onClick={this.toggleDisplay}
                    >
                        {this.state.display && (
                            <div className="panel-close-icon" aria-hidden="true"></div>
                        )}
                    </button>
                    {this.state.display && (
                        <div className="infos-content">
                            <div className="d-flex justify-content-between">
                                <h1
                                    style={{
                                        fontSize: "1em",
                                        fontWeight: "bold",
                                    }}
                                >
                                    {noteTitle}
                                </h1>

                                <div className="d-flex gap-1">
                                    {exportCSV} {methodoPdf}
                                </div>
                            </div>
                            {this.state.indicateurActif && currentYear && (
                                <>
                                    <div className="d-flex flex-wrap gap-2 align-items-center justify-content-start">
                                        <label
                                            htmlFor="year-selection"
                                            className="mb-0 "
                                        >
                                            Année :
                                        </label>
                                        {yearSelectionInput !== "slider" && (
                                            <div className="my-2">
                                                <AnalysisYearsSelection
                                                    id="year-selection"
                                                    years={years}
                                                    estimatedYears={estimatedYears}
                                                    currentYear={currentYear}
                                                    yearSelected={
                                                        this.state.analysisSelectedYear
                                                    }
                                                    customYearLabels={customYearLabels}
                                                    onChangeCallback={(e) =>
                                                        this.updateAnalysisSelectedYear(
                                                            e
                                                        )
                                                    }
                                                    inputType={yearSelectionInput}
                                                />
                                            </div>
                                        )}
                                        {isSelectedYearEstimated && (
                                            <span
                                                role="tooltip"
                                                title="Données estimées pour cette année, voir fiche méthodologique ci-dessus"
                                                style={{
                                                    fontWeight: "normal",
                                                    width: "auto",
                                                }}
                                            >
                                                (estimée)
                                            </span>
                                        )}
                                    </div>
                                    {yearSelectionInput === "slider" && (
                                        <div className="my-2">
                                            <AnalysisYearsSelection
                                                id="year-selection"
                                                years={years}
                                                estimatedYears={estimatedYears}
                                                currentYear={currentYear}
                                                yearSelected={
                                                    this.state.analysisSelectedYear
                                                }
                                                customYearLabels={customYearLabels}
                                                onChangeCallback={(e) =>
                                                    this.updateAnalysisSelectedYear(e)
                                                }
                                                inputType={yearSelectionInput}
                                            />
                                        </div>
                                    )}
                                </>
                            )}
                            {Object.keys(relevantUnits).length > 0 && (
                                <div className="row">
                                    <label>
                                        <b>Unité</b> :{" "}
                                        <AnalysisUnitSelection
                                            units={relevantUnits}
                                            hiddenUnits={meta?.unitsHiddenDetails}
                                            singleUnit={unit}
                                            zoneName={
                                                this.props.parentApi.data.zone.maille
                                            }
                                            unitSelected={
                                                this.state.analysisSelectedUnit ??
                                                defaultUnitForThisZoneType
                                            }
                                            onChangeCallback={(e) =>
                                                this.updateAnalysisSelectedUnit(e)
                                            }
                                        />
                                    </label>
                                </div>
                            )}
                            {ratioYearImprecise}
                            {this.state.indicateurActif && total}
                            {source}

                            {selectionInputs}
                        </div>
                    )}
                </div>
            );
        }
        return res;
    }
}

/**
 * Renders a sources/providers list for an analysis (or a POI)
 *
 * @prop {String} label Label of the sources list
 * @prop {Array<{name: string, url?: string}>} tableSources List of the sources
 */
export class DataSource extends React.Component {
    render() {
        if (!this.props.tableSources || !this.props.tableSources.length) {
            return null;
        }

        const output = this.props.tableSources.map((element) => {
            // if we have an URL, we display a HTML link
            if (element.url && element.url !== "") {
                let url = element.url.startsWith("http")
                    ? element.url
                    : "http://" + element.url;
                return (
                    <a target="_blank" rel="noreferrer" href={url}>
                        {element.name}
                    </a>
                );
            } else {
                // otherwise, just the text
                return <>{element.name}</>;
            }
        });

        return (
            <div>
                <strong>{this.props.label} :</strong>{" "}
                {output.reduce((previous, current) => (
                    <>
                        {previous}, {current}
                    </>
                ))}
            </div>
        );
    }
}

export default Infos;
