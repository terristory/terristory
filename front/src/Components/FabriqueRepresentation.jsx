/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import BarGraph from "./Graphs/BarGraph";
import CourbesEmpilees from "./Graphs/CourbesEmpilees";
import CourbesComparees from "./Graphs/CourbesComparees";
import DiagrammeCirculaire from "./Graphs/DiagrammeCirculaire";
import Jauge from "./Graphs/Jauge";
import JaugeCirculaire from "./Graphs/JaugeCirculaire";
import Histogramme from "./Graphs/Histogramme";
import DiagrammeRadar from "./Graphs/DiagrammeRadar";
import HistogrammeNormalise from "./Graphs/HistogrammeNormalise";
import HistogrammeDataRatio from "./Graphs/HistogrammeDataRatio";
import MarqueurSVG from "./Graphs/MarqueurSVG";
import AnalysisLauncher from "./Graphs/AnalysisLauncher";
import AnalysisMap from "./Graphs/AnalysisMap";
import PoiMap from "./Graphs/PoiMap";
import Logo from "./Graphs/Logo";
import DidacticFileLauncher from "./Graphs/DidacticFileLauncher";
import LinkLauncher from "./Graphs/LinkLauncher";
import DataTable from "./Graphs/DataTable";
import PCAETTrajectory from "./Graphs/PCAETTrajectory";
import TextBlock from "./Graphs/TextBlock";
import DashboardIframe from "./Graphs/DashboardIframe";
import CategorySingleSelect from "./CategorySingleSelect";

/**
 * This component is used to instantiate a representation component (CircularDiagram, StackedCurves etc.)
 * according to the representation parameter to add in the properties.
 */
class FabriqueRepresentation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            customFilters: {},
            customFiltersInitiated: false,
        };
    }

    componentDidMount() {
        const { analysisManager } = this.props.parentApi.controller;
        const categoriesSettings = analysisManager.getCategories(
            this.props.id_analysis
        );
        this.initCustomFilters(categoriesSettings);
    }

    initCustomFilters(categoriesSettings) {
        // if we have no category, no need to display anything
        // if we have no props filters -> no constraints -> nothing to do
        if (!categoriesSettings || !this.props.filters) {
            this.setState({ customFiltersInitiated: true });
            return "";
        }
        let customFilters = {};
        for (const categorySettings of categoriesSettings) {
            if (!categorySettings) continue;
            const category = categorySettings.categorie;
            if (
                !this.props.filters.hasOwnProperty(category) ||
                !["switch-button", "selection", "slider"].includes(
                    categorySettings?.type
                )
            ) {
                continue;
            }
            // for each valid category (ie. category using the right representation), we save the custom filters
            customFilters = {
                ...customFilters,
                [category]: this.props.filters[category],
            };
        }
        // if we performed any difference
        if (
            customFilters &&
            JSON.stringify(customFilters) !== JSON.stringify(this.state.customFilters)
        ) {
            this.setState({ customFilters });
        }
        this.setState({ customFiltersInitiated: true });
    }

    updateCurrentDashboardWhenEditing(_filters) {
        if (this.props.provenance === "tableau_de_bord") {
            // we also callback the updated filters in case of dashboard edition
            // if we have no filters yet, we initialize the object
            let currentFilters =
                this.props.parentApi.data.tableauBordDonnees.donnees[
                    this.props.thematique
                ].indicateurs[this.props.id].filters;
            if (!currentFilters) {
                currentFilters =
                    this.props.parentApi.controller.analysisManager.initFiltersByCategory(
                        parseInt(this.props.id_analysis)
                    );
            }

            this.props.parentApi.data.tableauBordDonnees.donnees[
                this.props.thematique
            ].indicateurs[this.props.id].filters = {
                ...currentFilters,
                ..._filters,
            };
        }
    }

    /**
     * Update the specific filters currently stored based on switch button edited.
     */
    switchButtonCategory(category, modalities_filtered) {
        const currentCat = this.state.customFilters?.[category]?.[0]?.filtre_categorie;
        let newValue;
        if (!currentCat || currentCat === modalities_filtered[0]) {
            newValue = modalities_filtered[1];
        } else {
            newValue = modalities_filtered[0];
        }
        this.setState({
            customFilters: {
                ...this.state.customFilters,
                [category]: [{ filtre_categorie: newValue }],
            },
        });
        this.updateCurrentDashboardWhenEditing({
            [category]: [{ filtre_categorie: newValue }],
        });
    }

    /**
     * Update the specific filters currently stored based on selection field.
     */
    changeCategorySelection(category, modality) {
        this.setState({
            customFilters: {
                ...this.state.customFilters,
                [category]: [{ filtre_categorie: modality }],
            },
        });
        this.updateCurrentDashboardWhenEditing({
            [category]: [{ filtre_categorie: modality }],
        });
    }

    /**
     * Get specific rules applying to current graph
     * @param {string} key the unique key used to identify current graph in the dashboard
     * @returns a list of filters
     */
    getSpecificCategoryFilters() {
        let output = {};
        for (const category in this.state.customFilters) {
            // if no specific rules for this category => continue with next
            if (!this.state.customFilters?.[category]) {
                continue;
            }

            // otherwise, we include our filter
            output = {
                ...output,
                [category]: this.state.customFilters?.[category],
            };
        }
        return output;
    }

    /**
     * Add switch or selection field for specific categories.
     */
    insertSpecificCategorySelector(categoriesSettings, filtersInitiated) {
        // if we have no category, no need to display anything
        if (!categoriesSettings) {
            return "";
        }
        let outputs = [];

        for (const categorySettings of categoriesSettings) {
            if (!categorySettings) continue;
            const category = categorySettings.categorie;
            const filters = filtersInitiated?.["____all_" + category];
            // if we have the category is not concerned => nothing to display
            if (
                !["switch-button", "selection", "slider"].includes(
                    categorySettings?.type
                ) ||
                !filters
            ) {
                continue;
            }

            // build options
            let options = [];
            for (let filter of filters) {
                options.push({
                    label: filter.filtre_categorie.split(".")[1],
                    value: filter.filtre_categorie.split(".")[1],
                });
            }

            // default filter form, by order of priority : the default filters specific to the dashboard if specified,  the default filter for the indicator (filter in meta.indicateur) specific to the dif specifie, the first option in the list otherwise

            const selectedOption =
                this.state.customFilters?.[category]?.[0]?.filtre_categorie?.split(
                    "."
                )[1] ||
                filtersInitiated?.[category]?.[0]?.filtre_categorie?.split(".")[1] ||
                undefined;

            // Only add the control once the dashboard default filters have been loaded
            // (the value of CategorySelect is uncontrolled, so the first defaultValue provided has to be correct)
            if (this.state.customFiltersInitiated)
                outputs.push(
                    <CategorySingleSelect
                        key={categorySettings.categorie}
                        category={categorySettings.categorie}
                        options={options}
                        defaultValue={selectedOption} // use as uncontrolled component
                        onChange={(selectedOption) => {
                            this.setState({
                                customFilters: {
                                    ...this.state.customFilters,
                                    [category]: [
                                        {
                                            filtre_categorie: `${categorySettings.categorie}.${selectedOption}`,
                                        },
                                    ],
                                },
                            });
                            this.updateCurrentDashboardWhenEditing({
                                [category]: [
                                    {
                                        filtre_categorie: `${categorySettings.categorie}.${selectedOption}`,
                                    },
                                ],
                            });
                        }}
                        inputType={categorySettings.type}
                        title={categorySettings.titre}
                        tooltip={categorySettings.description}
                        isLoading={!this.props.parentApi.data.dataLoaded}
                    ></CategorySingleSelect>
                );
        }
        return (
            <div className="d-flex flex-row flex-wrap align-items-center gap-2">
                {outputs}
            </div>
        );
    }

    render() {
        const unit = this.props.unit ? this.props.unit : false;
        const zoneType = this.props?.zoneType ?? this.props.parentApi.data.zone.zone;
        const zoneId = this.props?.zoneId ?? this.props.parentApi.data.currentZone;
        const zoneMaille =
            this.props?.zoneMaille ?? this.props.parentApi.data.zone.maille;
        const mapCallback = this.props?.mapCallback;

        let forcedFilters = false;
        let selector = "";

        if (
            [
                "line",
                "line-years",
                "bar",
                "bar-years",
                "pie",
                "courbes_croisees",
                "radar",
                "courbes-historiques",
                "marqueur-svg",
                "map",
            ].includes(this.props.representation)
        ) {
            const { analysisManager } = this.props.parentApi.controller;
            const categoriesSettings = analysisManager.getCategories(
                this.props.id_analysis
            );
            const representationDetails =
                analysisManager.getAnalysisRepresentationDetails(
                    this.props.id_analysis
                );
            const filtersInitiated = analysisManager.getFullCategoriesValues(
                this.props.id_analysis
            );
            selector = this.insertSpecificCategorySelector(
                categoriesSettings,
                filtersInitiated,
                representationDetails,
                this.props.id
            );
            forcedFilters = this.getSpecificCategoryFilters();
        }

        let graphique = "";
        if (
            this.props.representation === "line" ||
            this.props.representation === "line-years"
        ) {
            graphique = (
                <CourbesEmpilees
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    width={this.props.width}
                    height={this.props.height}
                    representation={this.props.representation}
                    thematique={this.props.thematique}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                    unit={this.props.unit}
                    options={this.props.options}
                />
            );
        } else if (
            this.props.representation === "bar" ||
            this.props.representation === "bar-years"
        ) {
            graphique = (
                <BarGraph
                    parentApi={this.props.parentApi}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    representation={this.props.representation}
                    width={this.props.width}
                    height={this.props.height}
                    thematique={this.props.thematique}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                    unit={this.props.unit}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                />
            );
        } else if (this.props.representation === "pie") {
            graphique = (
                <DiagrammeCirculaire
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    thematique={this.props.thematique}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                    unit={this.props.unit}
                />
            );
        } else if (this.props.representation === "courbes_croisees") {
            graphique = (
                <CourbesComparees
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    type={this.props.type}
                    options={this.props.options}
                    width={this.props.width}
                    height={this.props.height}
                />
            );
        } else if (this.props.representation === "jauge") {
            graphique = (
                <Jauge
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    type={this.props.type}
                />
            );
        } else if (this.props.representation === "jauge-circulaire") {
            graphique = (
                <JaugeCirculaire
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    type={this.props.type}
                />
            );
        } else if (this.props.representation === "histogramme") {
            graphique = (
                <Histogramme
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    thematique={this.props.thematique}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                    unit={this.props.unit}
                />
            );
        } else if (this.props.representation === "radar") {
            graphique = (
                <DiagrammeRadar
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    thematique={this.props.thematique}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    inverserAxeVertical={this.props.inverserAxeVertical}
                    data={this.props.data}
                    mapCallback={mapCallback}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                    unit={this.props.unit}
                />
            );
        } else if (this.props.representation === "histogramme-normalise") {
            graphique = (
                <HistogrammeNormalise
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    thematique={this.props.thematique}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                    unit={this.props.unit}
                />
            );
        } else if (this.props.representation === "courbes-historiques") {
            graphique = (
                <CourbesComparees
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    type={this.props.type}
                    options={this.props.options}
                    forcedFilters={forcedFilters}
                />
            );
        } else if (this.props.representation === "histogramme-data-ratio") {
            graphique = (
                <HistogrammeDataRatio
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    unit={unit}
                />
            );
        } else if (this.props.representation === "marqueur-svg") {
            graphique = (
                <MarqueurSVG
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    tailleSVG={this.props.tailleSVG}
                    tailleData={this.props.tailleData}
                    image={this.props.image}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                />
            );
        } else if (this.props.representation === "analysis-launcher") {
            graphique = (
                <AnalysisLauncher
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    color={this.props.couleur}
                />
            );
        } else if (this.props.representation === "map") {
            graphique = (
                <AnalysisMap
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    thematique={this.props.thematique}
                    filters={this.props.filters}
                    forcedFilters={forcedFilters}
                    unit={this.props.unit}
                />
            );
        } else if (this.props.representation === "poi_map") {
            graphique = (
                <PoiMap
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    thematique={this.props.thematique}
                    filters={this.props.filters}
                    unit={this.props.unit}
                />
            );
        } else if (this.props.representation === "logo") {
            graphique = (
                <Logo
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                />
            );
        } else if (this.props.representation === "didactic-file-launcher") {
            graphique = (
                <DidacticFileLauncher
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    color={this.props.couleur}
                    id_tableau_bord={this.props.id_tableau_bord}
                />
            );
        } else if (this.props.representation === "link-launcher") {
            graphique = (
                <LinkLauncher
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    name={this.props.nom}
                    id={this.props.id}
                    typeLink={this.props.typeLink}
                    nameTypeLink={this.props.nameTypeLink}
                    keyLink={this.props.keyLink}
                    icon={this.props.icon}
                    provenance={this.props.provenance}
                    color={this.props.couleur}
                    id_tableau_bord={this.props.id_tableau_bord}
                />
            );
        } else if (this.props.representation === "pcaet-trajectory-line") {
            graphique = (
                <PCAETTrajectory
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    width={this.props.width}
                    height={this.props.height}
                    representation={this.props.representation}
                    thematique={this.props.thematique}
                    filters={this.props.filters}
                    unit={this.props.unit}
                    options={this.props.options}
                />
            );
        } else if (this.props.representation === "pcaet-trajectory-table") {
            graphique = (
                <DataTable
                    parentApi={this.props.parentApi}
                    zoneType={zoneType}
                    zoneId={zoneId}
                    zoneMaille={zoneMaille}
                    id_analysis={this.props.id_analysis}
                    name={this.props.nom}
                    id={this.props.id}
                    type={this.props.type}
                    provenance={this.props.provenance}
                    color={this.props.couleur}
                    id_tableau_bord={this.props.id_tableau_bord}
                />
            );
        } else if (this.props.representation === "text-block") {
            graphique = (
                <div className="text-block-show">
                    <TextBlock content={this.props.content} />
                </div>
            );
        } else if (this.props.representation === "iframe") {
            graphique = (
                <div>
                    <DashboardIframe
                        src={this.props.content.iframeSrc}
                        width={this.props.content.iframeWidth}
                        height={this.props.content.iframeHeight}
                        title={this.props.content.iframeTitle}
                    ></DashboardIframe>
                </div>
            );
        }
        return (
            <div>
                {" "}
                {selector}
                {graphique}
            </div>
        );
    }
}

export default FabriqueRepresentation;
