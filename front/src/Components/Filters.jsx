/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import ReactTooltip from "react-tooltip";
import { Link } from "react-router-dom";
import { buildRegionUrl, slugify } from "../utils";
import Api from "../Controllers/Api";
import { MainZoneSelect } from "./SelectionObjet";
import config from "../settings";
import {
    Accordion,
    AccordionItem,
    AccordionItemPanel,
    AccordionItemHeading,
    AccordionItemButton,
} from "react-accessible-accordion";
import "react-accessible-accordion/dist/fancy-example.css";
import "bootstrap/dist/css/bootstrap.min.css";
import SEO from "./SEO";

/**
 * This component is used to filter the data sources used to calculate the cartographic data.
 * It manages the home page of the site with the display of the map, graphics, legends and any filters.
 */
class Filters extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            analysis: props.parentApi.data.analysis,
            searchPattern: "",
            dataReferencement: undefined,
            installationsCochées: [],
            nomTerritoire: "",
            uiTheme: props.parentApi.data.uiTheme,
            nomIndicateurCourant: props.parentApi.data.nomIndicateurCourant,
            idInstallationCouranteTermine: false,
            disabled: false,
        };
    }

    componentDidUpdate(prevProps, prevState) {
        let { zone, currentZone, connected, analysis, sankeySelected } =
            this.props.parentApi.data;
        let {
            uiTheme, // get the theme passed in url when the user enters a url to an indicator
            idInstallationCourante, // get the installation passed in url when the user enters a url to an installation
            nomInstallationCourante, // current installation name
        } = this.props.parentApi.data;

        if (
            prevProps.parentApi.data.zone !== zone ||
            currentZone !== prevProps.parentApi.data.currentZone
        ) {
            let nomTerritoire =
                this.props.parentApi.controller.zonesManager.getZoneName(
                    currentZone,
                    zone
                );
            if (zone.zone === "region") {
                nomTerritoire = this.props.parentApi.data.regionLabel;
            }
            this.setState({
                disabled: this.showMenuIfTerritorySelected(zone, currentZone),
                nomTerritoire: nomTerritoire,
            });
        }

        // updating the state of the current indicator name in the index.js component
        if (this.state.nomIndicateurCourant !== prevState.nomIndicateurCourant) {
            this.props.parentApi.callbacks.miseAjourNomIndicateurCourant(
                this.state.nomIndicateurCourant
            );
        }

        let url = "";
        let nomTerritoire = this.props.parentApi.controller.zonesManager.getZoneName(
            currentZone,
            zone
        );
        if (!nomTerritoire) {
            // get the name of the territory passed in url when the user enters a url to an indicator/equipment
            nomTerritoire = this.props.parentApi.data.nomTerritoire;
            if (this.state.nomTerritoire) {
                nomTerritoire = this.state.nomTerritoire;
            }
        }

        if (zone) {
            url += "?zone=" + zone.zone + "&maille=" + zone.maille;
            if (zone.zone === "region") {
                url += "&zone_id=" + this.props.parentApi.data.regionCode;
            } else if (currentZone) {
                url += "&zone_id=" + currentZone;
            }
        }

        if (analysis) {
            url += "&analysis=" + analysis;
        }

        if (sankeySelected) {
            url += "&sankey=" + sankeySelected;
        }

        if (this.state.uiTheme || uiTheme) {
            // specify in the URL the theme associated with the indicator
            url += "&theme=" + (uiTheme ?? this.state.uiTheme);
        }

        let url_nom_territoire = "";
        if (nomTerritoire) {
            url_nom_territoire = "&nom_territoire=" + encodeURI(nomTerritoire);
        }
        if (zone.zone === "region") {
            url_nom_territoire =
                "&nom_territoire=" + encodeURI(this.props.parentApi.data.regionLabel);
        }

        url += url_nom_territoire;

        if (idInstallationCourante) {
            url += "&installation=" + idInstallationCourante;
            if (
                !this.state.idInstallationCouranteTermine &&
                this.props.parentApi.data.depuisUrl
            ) {
                // specify in the url the installation
                let installations = idInstallationCourante.split("-");
                for (let install of installations) {
                    let inputInstallation = document.getElementById(install);
                    if (!inputInstallation.checked) {
                        setTimeout(function () {
                            inputInstallation.click();
                        }, 2000);
                    }
                }

                this.setState({
                    idInstallationCouranteTermine: true,
                });
            }
        } else if (!this.state.idInstallationCouranteTermine) {
            this.setState({
                idInstallationCouranteTermine: true,
            });
        }

        if (
            prevProps.parentApi.data.zone !== zone ||
            currentZone !== prevProps.parentApi.data.currentZone ||
            analysis !== prevProps.parentApi.data.analysis
        ) {
            this.props.parentApi.callbacks.mettreAJourParametresUrls(url);
        }

        if (prevProps.parentApi.data.zone.maille !== zone.maille) {
            // deactivate the chart if we go from one maille to another (epci, departement, teposcv)
            this.props.parentApi.callbacks.displayChart(false);
        }

        if (
            prevProps.parentApi.data.connected !== connected ||
            this.props.parentApi.data.chargementListeTableauxDeBordNecessaire
        ) {
            // Reload the analysis list if we connect / disconnect
            this.props.parentApi.callbacks.updateAnalysisList();
            this.props.parentApi.callbacks.updateDashboardsList();
            this.props.parentApi.callbacks.chargementListeTableauxDeBordNecessaire(
                false
            );
        }

        let nomIndicateurCourant =
            this.props.parentApi.controller.analysisManager.getAnalysisName(
                this.props.parentApi.data.idIndicateurCourant
            );
        if (!nomIndicateurCourant) {
            nomIndicateurCourant =
                this.props.parentApi.controller.analysisManager.getAnalysisName(
                    parseInt(this.state.analysis, 10)
                );
        }
        // update of the referencing(SEO) if we change territory
        if (
            (prevProps.parentApi.data.zone !== zone ||
                currentZone !== prevProps.parentApi.data.currentZone ||
                prevState.nomTerritoire !== this.state.nomTerritoire) &&
            this.state.uiTheme &&
            nomIndicateurCourant
        ) {
            // update meta title of home page
            const dataReferencement =
                this.state.uiTheme + " - " + nomIndicateurCourant + " " + nomTerritoire;

            this.setState({
                dataReferencement: dataReferencement,
            });
        } else if (
            (prevProps.parentApi.data.zone !== zone ||
                currentZone !== prevProps.parentApi.data.currentZone ||
                prevState.nomTerritoire !== this.state.nomTerritoire) &&
            nomInstallationCourante
        ) {
            // modification of the title key of the home page
            const dataReferencement =
                uiTheme + " - " + nomInstallationCourante + " " + nomTerritoire;
            this.setState({
                dataReferencement: dataReferencement,
            });
        }
    }

    toggleMenu() {
        this.props.parentApi.callbacks.toggleIsLeftMenuOpen();
    }

    showMenu(menuId) {
        this.props.parentApi.callbacks.updateIsLeftMenuOpen(true);
        this.props.parentApi.callbacks.updateCurrentMenu(menuId);
    }

    parseOtherLink(url, replace_epci_group_names = false) {
        let zoneName =
            this.props.parentApi.controller?.zonesManager?.getZoneName(
                this.props.parentApi.data.currentZone,
                this.props.parentApi.data.zone
            ) ?? "unknown";
        // we make the right replacements
        url = url.replaceAll("{zone-name}", zoneName);
        if (url.includes("{zone-name-slug}")) {
            // we create the slug
            let slugZoneName = slugify(zoneName);
            // in  some weird cases, we have to replace the groups names by their
            // full length names (Communautés de communes)
            if (replace_epci_group_names) {
                if (slugZoneName.startsWith("cc-")) {
                    slugZoneName = slugZoneName.replace(
                        "cc-",
                        "communaute-de-communes-"
                    );
                } else if (slugZoneName.startsWith("ca-")) {
                    slugZoneName = slugZoneName.replace(
                        "ca-",
                        "communaute-d-agglomeration-"
                    );
                }
            }
            url = url.replaceAll("{zone-name-slug}", slugZoneName);
        }
        const zoneId = this.props.parentApi.data?.currentZone ?? "unknown";
        url = url.replaceAll("{zone-id}", zoneId);
        url = url.replaceAll("{zone-id-slug}", slugify(zoneId));
        return url;
    }

    showMenuIfTerritorySelected(zone) {
        let disabled = true;
        if (zone.zone === "region" || this.props.territoireSelectionne) {
            disabled = false;
            this.props.parentApi.callbacks.updateIsLeftMenuOpen(true);
        }
        return disabled;
    }

    handleSearchChange(event) {
        this.setState({ searchPattern: event.target.value });
    }

    render() {
        const { newPoi } = this.props.parentApi.callbacks;
        const { analysisManager } = this.props.parentApi.controller;

        const launchAnalysis = (e, id, id_tableau_bord, item) => {
            e.stopPropagation();

            if (id === "simulateur") {
                this.props.parentApi.callbacks.updateProvenance("filters");
            }

            if (id_tableau_bord) {
                // Needed to know which dashboard to render when clicking on the title of one of them in the left menu.
                this.props.parentApi.callbacks.definirTableauBordCourant(
                    id_tableau_bord
                );
                this.props.parentApi.callbacks.updateAnalysis(undefined);
            }

            // Update dataReferencement
            const dataReferencement = `${item.ui_theme} - ${item.name} ${this.state.nomTerritoire}`;
            this.setState({
                dataReferencement: dataReferencement,
                analysis: id,
                uiTheme: item.ui_theme,
                nomIndicateurCourant: item.name,
            });

            // Check if id is a number before updating analysis name and uiTheme
            if (!isNaN(id)) {
                this.props.parentApi.callbacks.miseAJourUiTheme(item.ui_theme);
                this.props.parentApi.callbacks.miseAjourNomIndicateurCourant(item.name);
            }

            if (parseInt(id, 10) !== parseInt(this.props.parentApi.data.analysis, 10)) {
                this.props.parentApi.callbacks.updateAnalysis(
                    id,
                    item.options,
                    "carto"
                );
                this.props.parentApi.callbacks.updateProvenance("carto");
            }

            this.props.parentApi.callbacks.displayChart(false);
        };

        const handlePOILayerVisibilityChange = (uiTheme, nomInstallationCourante) => {
            return (e) => {
                const target = e.target;
                const value =
                    target.type === "checkbox" ? target.checked : target.value;
                // creation of a temporary array that contains the array returned by the state
                let tempArray = this.state.installationsCochées;
                let installationsCocheesStr = "";
                // update uiTheme
                this.props.parentApi.callbacks.miseAJourUiTheme(uiTheme);
                // if an installation is checked
                if (target.checked) {
                    // add the name of the installation to the table
                    tempArray.push(target.name);
                    for (let eq of tempArray) {
                        if (tempArray.indexOf(eq) !== tempArray.length - 1) {
                            installationsCocheesStr += eq + "-";
                        } else {
                            installationsCocheesStr += eq;
                        }
                    }
                    // update the state with new array
                    this.setState({
                        installationsCochées: tempArray,
                    });

                    this.props.parentApi.callbacks.miseAjourNomInstallationCourante(
                        nomInstallationCourante,
                        installationsCocheesStr,
                        uiTheme
                    );
                } else {
                    // if the installation is unchecked
                    // get the position from the installation name in the array
                    let pos = tempArray.indexOf(target.name);
                    // remove the installation from the array
                    tempArray.splice(pos, 1);
                    // maj du state avec le nouveau tableau
                    this.setState({
                        installationsCochées: tempArray,
                    });
                    // reset current value
                    this.props.parentApi.callbacks.miseAjourNomInstallationCourante(
                        undefined
                    );
                    this.props.parentApi.callbacks.miseAJourUiTheme(undefined);
                }

                const { analysis } = this.props.parentApi.data;
                const analysisUrl = analysis ? "&analysis=" + analysis : "";

                // if the state array is not empty
                if (this.state.installationsCochées.length > 0) {
                    // converting array elements to comma separated string
                    let installationsConcat =
                        this.state.installationsCochées.toString();
                    // replace commas with dashes
                    let installationsConcatAvecTirets = installationsConcat.replaceAll(
                        ",",
                        "-"
                    );
                    // update the url
                    let zoneId = this.props.parentApi.data.currentZone;
                    if (this.props.parentApi.data.zone.zone === "region") {
                        zoneId = this.props.parentApi.data.regionCode;
                    }
                    let nomTerritoire = this.state.nomTerritoire;
                    if (!this.state.nomTerritoire) {
                        nomTerritoire =
                            this.props.parentApi.controller.zonesManager.getZoneName(
                                this.props.parentApi.data.currentZone,
                                this.props.parentApi.data.zone
                            );
                    }
                    this.props.parentApi.callbacks.mettreAJourParametresUrls(
                        "?region=" +
                            this.props.parentApi.data.region +
                            "&zone=" +
                            this.props.parentApi.data.zone.zone +
                            "&maille=" +
                            this.props.parentApi.data.zone.maille +
                            "&zone_id=" +
                            zoneId +
                            "&nom_territoire=" +
                            nomTerritoire +
                            "&theme=" +
                            uiTheme +
                            "&installation=" +
                            installationsConcatAvecTirets +
                            analysisUrl
                    );

                    // update referencement
                    const dataReferencement =
                        uiTheme + " - " + nomInstallationCourante + " " + nomTerritoire;
                    this.setState({
                        dataReferencement: dataReferencement,
                    });
                } else {
                    // if the array is empty
                    // update the url
                    this.props.parentApi.callbacks.mettreAJourParametresUrls(
                        "?zone=" +
                            this.props.parentApi.data.zone.zone +
                            "&maille=" +
                            this.props.parentApi.data.zone.maille +
                            "&zone_id=" +
                            this.props.parentApi.data.currentZone +
                            "&nom_territoire=" +
                            this.state.nomTerritoire +
                            analysisUrl
                    );
                }
                let nom_couche = "";
                let cochee = false;
                for (let poiLayer of this.props.parentApi.data.poiLayers) {
                    if (poiLayer.nom === target.name) {
                        poiLayer.checked = value;
                        nom_couche = poiLayer.label;
                        cochee = value;
                    }
                }
                let region = this.props.parentApi.data.region;
                let urlCodeTerritoire =
                    "?code_territoire=" + this.props.parentApi.data.currentZone;
                if (this.props.parentApi.zone) {
                    if (this.props.parentApi.zone.zone === "region") {
                        urlCodeTerritoire =
                            "?code_territoire=" + this.props.parentApi.data.regionCode;
                    }
                }
                let urlTypeTerritoire =
                    "&type_territoire=" + this.props.parentApi.data.zone.zone;
                let urlIdUtilisateur =
                    "&id_utilisateur=" +
                    this.props.parentApi.controller.gestionSuiviConsultations
                        .idUtilisateur;
                let urlSuiviPoi =
                    config.api_poi_consult_layer_url.replace(
                        "#layer#",
                        nom_couche.replace("/", " et ")
                    ) +
                    "/" +
                    cochee +
                    urlCodeTerritoire +
                    urlTypeTerritoire +
                    urlIdUtilisateur;
                Api.callApi(buildRegionUrl(urlSuiviPoi, region), null, "GET");
                this.props.parentApi.callbacks.updatePoiLayers(
                    this.props.parentApi.data.poiLayers.slice()
                );
            };
        };

        let poiLayersFinal = [];
        let couchePoiParRubrique = {};
        for (let poiLayer of this.props.parentApi.data.poiLayers) {
            if (!couchePoiParRubrique[poiLayer.theme]) {
                couchePoiParRubrique[poiLayer.theme] = [];
            }
            couchePoiParRubrique[poiLayer.theme].push(poiLayer);
        }

        for (let theme in couchePoiParRubrique) {
            let poiLayersMenu = [];
            for (let poiLayer of couchePoiParRubrique[theme]) {
                let editButton = "";
                if (
                    this.props.parentApi.data.connected &&
                    poiLayer.checked &&
                    poiLayer.modifiable
                ) {
                    editButton = (
                        <button
                            type="button"
                            className="menu-button"
                            onClick={() => newPoi(poiLayer.nom)}
                        >
                            <div className="poi-add"></div>
                        </button>
                    );
                }

                if (this.props.parentApi.data.settings.ui_show_poi) {
                    poiLayersMenu.push(
                        <div className="layer-check" key={poiLayer.nom}>
                            <label>
                                <input
                                    id={poiLayer.nom}
                                    name={poiLayer.nom}
                                    type="checkbox"
                                    checked={poiLayer.checked}
                                    onChange={handlePOILayerVisibilityChange(
                                        theme,
                                        poiLayer.label
                                    )}
                                />

                                {poiLayer.label}
                                <span
                                    data-tip={
                                        "Cliquer ici pour ajouter un(e) " +
                                        poiLayer.label
                                    }
                                >
                                    {editButton}
                                </span>
                            </label>
                        </div>
                    );
                }
            }
            poiLayersFinal.push(
                <Accordion allowMultipleExpanded allowZeroExpanded key={theme}>
                    <AccordionItem>
                        <AccordionItemHeading>
                            <AccordionItemButton className="accordion-indicateur accordion__button">
                                <b className="retour-ligne-fleche">{theme}</b>
                            </AccordionItemButton>
                        </AccordionItemHeading>
                        <AccordionItemPanel className="liste-indicateurs">
                            {poiLayersMenu}
                        </AccordionItemPanel>
                    </AccordionItem>
                </Accordion>
            );
        }

        const isAnalysisEnabled = (item) => {
            let zone = this.props.parentApi.data.zone;
            const analysis = analysisManager.analysis.find(
                (ca) => item.id === ca.id.toString()
            );
            if (analysis !== undefined) {
                if (
                    analysis.disabled_for_macro_level !== null &&
                    analysis.disabled_for_macro_level.split(",").includes(zone.zone)
                ) {
                    const plural = analysis.disabled_for_macro_level.includes(",");
                    const info = `Indicateur non disponible pour ${
                        plural ? "les types de territoire" : "le type de territoire"
                    } : ${analysis.disabled_for_macro_level.replace(",", ", ")}.`;
                    return [false, info];
                }
                if (
                    analysis.disabled_for_zone !== null &&
                    analysis.disabled_for_zone.split(",").includes(zone.maille)
                ) {
                    const plural = analysis.disabled_for_zone.includes(",");
                    const info = `Indicateur non disponible ${
                        plural ? "aux mailles" : "à la maille"
                    } : ${analysis.disabled_for_zone.replace(",", ", ")}.`;
                    return [false, info];
                }
                if (
                    analysis.only_for_zone &&
                    !analysis.only_for_zone.split(",").includes(zone.maille)
                ) {
                    const plural = analysis.only_for_zone.includes(",");
                    const info = `Indicateur uniquement disponible ${
                        plural ? "aux mailles" : "à la maille"
                    } : ${analysis.only_for_zone.replace(",", ", ")}.`;
                    return [false, info];
                }
            }
            // Special case for region & synthese
            if (
                item.id === "synthese_territoriale" &&
                ["region", "commune"].includes(zone.zone)
            ) {
                return [
                    false,
                    "Synthèse non disponible à l'échelle régionale ou communale.",
                ];
            }

            return [true];
        };

        /** Build UI elements for sub-indicators */
        const renderSubIndicatorList = (subIndicators) => {
            return subIndicators.map((subIndic) => {
                let [subIndicEnabled, subIndicTitle] = isAnalysisEnabled(subIndic);
                let subIndicClassName = subIndicEnabled ? "" : "disabled";

                if (subIndicTitle) {
                    subIndicTitle +=
                        " Sélectionnez un autre territoire en haut de la page.";
                }

                // Highlight the item if it is the currently analyzed one
                subIndicClassName =
                    parseInt(this.props.parentApi.data.analysis, 10) ===
                        parseInt(subIndic.id, 10) && this.props.parentApi.data.analysis
                        ? "active"
                        : subIndicClassName;

                // Return a list of sub-indicator elements for each indicator
                return (
                    <li
                        className={subIndicClassName}
                        key={subIndic.id}
                        onClick={
                            subIndicEnabled
                                ? (e) =>
                                      launchAnalysis(
                                          e,
                                          subIndic.id,
                                          undefined,
                                          subIndic
                                      )
                                : undefined
                        }
                    >
                        <span title={subIndicTitle}>{subIndic.name}</span>
                    </li>
                );
            });
        };

        const indicatorsList = [];
        const analysesList = [];

        let disabled = this.state.disabled || !this.props.territoireSelectionne;

        if (analysisManager.analysis && !disabled) {
            for (let a of analysisManager.analysis) {
                if (a.concatenation === null || a.concatenation === undefined) {
                    if (this.state.searchPattern !== "") {
                        let found = a.nom
                            .toLowerCase()
                            .indexOf(this.state.searchPattern);
                        if (found === -1) {
                            continue;
                        }
                    }

                    if (
                        this.props.parentApi.data.profil === "admin" ||
                        this.props.parentApi.data.accesIndicateursDesactives ||
                        a.active
                    ) {
                        let subItem;
                        try {
                            subItem = a.sub_indicator
                                ? JSON.parse(a.sub_indicator)?.map(
                                      (item) => item.indicator
                                  )
                                : undefined;
                        } catch (error) {
                            // Handle JSON parsing error
                            console.error("Error parsing JSON:", error);
                        }

                        indicatorsList.push({
                            id: a.id.toString(),
                            name: a.nom,
                            ui_theme: a.ui_theme,
                            sub_indicator: subItem,
                            is_sub_indicator: a.is_sub_indicator,
                        });
                    }
                }
            }

            const { settings } = this.props.parentApi.data;
            if (settings?.ui_show_cesba) {
                analysesList.push({
                    id: "synthese_territoriale",
                    name: "Synthèse des indicateurs territoriaux",
                    ui_theme: "Profil du territoire",
                    is_sub_indicator: false,
                });
            }

            if (settings?.other_links) {
                const { zone, currentZone } = this.props.parentApi.data;
                for (let group in settings.other_links) {
                    for (let link of settings.other_links[group]) {
                        if (
                            (link.zones_types ?? "").split(",").includes(zone.zone) &&
                            !link.zones_excluded?.[zone.zone]?.includes(currentZone)
                        ) {
                            const url = this.parseOtherLink(
                                link.url,
                                link.replace_epci_group_names
                            );
                            analysesList.push({
                                id: link.slug,
                                name: link.label,
                                ui_theme: group,
                                url: url,
                                show_external: link.show_external,
                            });
                        }
                    }
                }
            }

            // special case: Energy trajectory tracking, Carbon trajectory tracking, Air pollutant trajectory tracking
            if (settings?.analyses_pages) {
                if (settings.analyses_pages.includes("suivi_energetique")) {
                    analysesList.push({
                        id: "suivi_energetique",
                        name: "Suivi de la trajectoire énergétique",
                        ui_theme: "Suivi de la trajectoire énergétique",
                        is_sub_indicator: false,
                    });
                }
                if (settings.analyses_pages.includes("suivi_emission_ges")) {
                    analysesList.push({
                        id: "suivi_emission_ges",
                        name: "Suivi des émissions de GES",
                        ui_theme: "Suivi de la trajectoire carbone",
                        is_sub_indicator: false,
                    });
                }
                if (settings.analyses_pages.includes("suivi_polluants_nox")) {
                    analysesList.push({
                        id: "suivi_polluants_nox",
                        name: "Suivi des émissions de NOx",
                        ui_theme: "Suivi des trajectoires des polluants atmosphériques",
                        is_sub_indicator: false,
                    });
                }
                if (settings.analyses_pages.includes("suivi_polluants_pm10")) {
                    analysesList.push({
                        id: "suivi_polluants_pm10",
                        name: "Suivi des émissions de PM10 ",
                        ui_theme: "Suivi des trajectoires des polluants atmosphériques",
                        is_sub_indicator: false,
                    });
                }
                if (settings.analyses_pages.includes("suivi_polluants_pm25")) {
                    analysesList.push({
                        id: "suivi_polluants_pm25",
                        name: "Suivi des émissions de PM2.5 ",
                        ui_theme: "Suivi des trajectoires des polluants atmosphériques",
                        is_sub_indicator: false,
                    });
                }
                if (settings.analyses_pages.includes("suivi_polluants_covnm")) {
                    analysesList.push({
                        id: "suivi_polluants_covnm",
                        name: "Suivi des émissions de COVNM",
                        ui_theme: "Suivi des trajectoires des polluants atmosphériques",
                        is_sub_indicator: false,
                    });
                }
                if (settings.analyses_pages.includes("suivi_polluants_so2")) {
                    analysesList.push({
                        id: "suivi_polluants_so2",
                        name: "Suivi des émissions de SO2 ",
                        ui_theme: "Suivi des trajectoires des polluants atmosphériques",
                        is_sub_indicator: false,
                    });
                }
                if (settings.analyses_pages.includes("suivi_polluants_nh3")) {
                    analysesList.push({
                        id: "suivi_polluants_nh3",
                        name: "Suivi des émissions de NH3",
                        ui_theme: "Suivi des trajectoires des polluants atmosphériques",
                        is_sub_indicator: false,
                    });
                }

                // We limit sankeys to EPCIs (by choice)
                if (this.props.parentApi.data.zone?.zone && settings?.ui_show_sankey) {
                    settings.sankeys.forEach((sankey) => {
                        // do not show sankeys that were not enabled
                        if (
                            !sankey["geographical_levels_enabled"].includes(
                                this.props.parentApi.data.zone.zone
                            )
                        ) {
                            return;
                        }
                        analysesList.push({
                            id: "diagramme_sankey",
                            name:
                                sankey["sankey_name"] !== ""
                                    ? sankey["sankey_name"]
                                    : sankey["data_table"],
                            options: {
                                tableName: sankey["data_table"],
                            },
                            key: "diagramme_sankey_" + sankey["data_table"],
                            ui_theme: "Suivi des flux",
                            is_sub_indicator: false,
                        });
                    });
                }
            }
            if (
                this.props.parentApi.controller.authManager?.userInfos?.profil ===
                    "admin" &&
                ["region", "departement"].includes(this.props.parentApi.data.zone?.zone)
            ) {
                analysesList.push({
                    id: "agregation_pcaet",
                    name: "Agrégation des PCAET",
                    ui_theme: "Profil du territoire",
                });
            }
        }

        let listThemeActions = [];
        listThemeActions.push({
            id: "indicateur",
            items: [],
            list: indicatorsList,
        });
        listThemeActions.push({
            id: "analyse",
            items: [],
            list: analysesList,
        });

        for (let theme of listThemeActions) {
            let uiThemeList = [];
            for (let item of theme.list) {
                // Classify indicators by ui_theme
                let indUiTheme = uiThemeList.find(
                    (uiTheme) => uiTheme.id === item.ui_theme
                );
                if (!indUiTheme) {
                    indUiTheme = {
                        id: item.ui_theme,
                        items: [],
                    };
                    uiThemeList.push(indUiTheme);
                }

                // Check if the current item is enabled for analysis
                let [enabled, title] = isAnalysisEnabled(item);
                let className = enabled ? "" : "disabled";

                // Add a message to the title if applicable
                if (title) {
                    title += " Sélectionnez un autre territoire en haut de la page.";
                }

                // Highlight the item if it is the currently analyzed one
                className =
                    parseInt(this.props.parentApi.data.analysis, 10) ===
                        parseInt(item.id, 10) && this.props.parentApi.data.analysis
                        ? "active"
                        : className;

                if (item.is_sub_indicator) continue;
                if (item.url) {
                    // If the item is a link
                    indUiTheme.items.push(
                        <li className={className} key={item.key ?? item.id}>
                            <a
                                className={item.show_external && "navbar-external-link"}
                                target="_blank"
                                rel="noopener noreferrer"
                                href={item.url}
                            >
                                {item.name}
                            </a>
                        </li>
                    );
                } else {
                    // find sub-indicators metadata
                    const subIndicatorList = theme.list.filter((elem) =>
                        (item.sub_indicator || []).find(
                            (id) => parseInt(id, 10) === parseInt(elem.id, 10)
                        )
                    );

                    // Create a list of UI elements for sub-indicators
                    const subIndicatorUi = renderSubIndicatorList(subIndicatorList);

                    // Add the item and its sub-indicators to the UI theme list
                    indUiTheme.items.push(
                        <div key={item.key ?? item.id}>
                            <li
                                className={className}
                                onClick={
                                    enabled
                                        ? (e) =>
                                              launchAnalysis(
                                                  e,
                                                  item.id,
                                                  undefined,
                                                  item
                                              )
                                        : undefined
                                }
                            >
                                <span title={title}>{item.name}</span>
                            </li>
                            <ul>{subIndicatorUi}</ul>
                        </div>
                    );
                }
            }

            let items = uiThemeList.map((ui_theme) => (
                <FiltersAccordion key={ui_theme.id} title={ui_theme.id}>
                    {ui_theme.items}
                </FiltersAccordion>
            ));

            if (items.length > 0) {
                theme.items = (
                    <div className="panel-container">
                        <ul className="analysis-themes">
                            {items.map((item) => (
                                <li key={item.key}>{item}</li>
                            ))}
                        </ul>
                    </div>
                );
            }
        }

        let mesTableauxDeBord = (
            <FiltersAccordion
                title="Mes tableaux de bord"
                emptyMessage="Veuillez vous connecter"
            />
        );
        let creationTableauBord = (
            <FiltersAccordion
                title="Création d'un tableau de bord"
                emptyMessage="Veuillez vous connecter"
            />
        );

        // Dashboard module. Only active when user is connected
        let itemCreaTionTdB = {
            id: "creation_tableaux_bord",
            name: "Création d'un tableau de bord",
            ui_theme: "Création d'un tableau de bord",
        };
        if (this.props.parentApi.data.connected) {
            mesTableauxDeBord = (
                <FiltersAccordion
                    title="Mes tableaux de bord"
                    emptyMessage="Sélectionnez un territoire"
                />
            );
            creationTableauBord = (
                <FiltersAccordion title="Création d'un tableau de bord">
                    <li
                        onClick={(e) =>
                            launchAnalysis(
                                e,
                                "creation_tableaux_bord",
                                undefined,
                                itemCreaTionTdB
                            )
                        }
                    >
                        Créer un tableau de bord
                    </li>
                </FiltersAccordion>
            );
            let listMyDashboards =
                this.props.parentApi.controller.dashboardManager.listMyDashboards;
            if (listMyDashboards && this.props.territoireSelectionne) {
                if (listMyDashboards.statut) listMyDashboards = [];
                // List the dashboards created by the connected person
                mesTableauxDeBord = listMyDashboards.map((tableau) => {
                    const itemRestitutionTdB = {
                        id: "restitution_tableaux_bord_" + tableau.id,
                        name: tableau.titre,
                        ui_theme: tableau.titre,
                    };
                    return (
                        <li
                            key={tableau.id}
                            onClick={(e) =>
                                launchAnalysis(
                                    e,
                                    "restitution_tableaux_bord",
                                    tableau.id,
                                    itemRestitutionTdB
                                )
                            }
                        >
                            {tableau.titre}
                        </li>
                    );
                });
                mesTableauxDeBord = (
                    <FiltersAccordion
                        title="Mes tableaux de bord"
                        emptyMessage="Vous n'avez aucun tableau de bord pour ce territoire"
                    >
                        {mesTableauxDeBord}
                    </FiltersAccordion>
                );
            }
        }
        let tableauxDeBordPublics = (
            <FiltersAccordion
                title="Tableaux de bord disponibles pour le territoire sélectionné"
                emptyMessage="Sélectionnez un territoire"
            />
        );
        if (
            this.props.territoireSelectionne &&
            this.props.parentApi.controller.dashboardManager.listPublicDashboards
        ) {
            // If there are dashboards available for the selected territory
            // to access the dashboards available for this territory
            tableauxDeBordPublics =
                this.props.parentApi.controller.dashboardManager.listPublicDashboards.map(
                    (tableau) => {
                        let itemRestitutionTdB = {
                            id: "restitution_tableaux_bord_" + tableau.id,
                            name: tableau.titre,
                            ui_theme: tableau.titre,
                        };
                        return (
                            <li
                                key={tableau.id}
                                onClick={(e) =>
                                    launchAnalysis(
                                        e,
                                        "restitution_tableaux_bord",
                                        tableau.id,
                                        itemRestitutionTdB
                                    )
                                }
                            >
                                {tableau.titre}
                            </li>
                        );
                    }
                );
            tableauxDeBordPublics = (
                <FiltersAccordion
                    title="Tableaux de bord disponibles pour le territoire sélectionné"
                    emptyMessage={
                        "Il n' y pas de tableau de bord disponible pour ce territoire"
                    }
                >
                    {tableauxDeBordPublics}
                </FiltersAccordion>
            );
        }
        let dashboardsPanel = (
            <div className="panel-container">
                <ul className="analysis-themes">
                    <li>{tableauxDeBordPublics}</li>
                    <li>{mesTableauxDeBord}</li>
                    <li>{creationTableauBord}</li>
                </ul>
            </div>
        );

        let listSimulators = [];
        let strategiesLink = null;
        if (this.props.parentApi.data.settings.ui_show_plan_actions) {
            strategiesLink = (
                <li className="element-outside-accordion">
                    <Link to="/strategies_territoriales" className="unstyled">
                        Stratégies territoriales
                    </Link>
                </li>
            );
            if (this.props.territoireSelectionne) {
                if (this.props.parentApi.data.settings.ui_show_simulators["mobility"]) {
                    listSimulators.push({
                        id: "simulateur",
                        name: "Mobilité",
                        options: {
                            type: "mobility",
                        },
                        ui_theme: "Simulateurs",
                        is_sub_indicator: false,
                    });
                }
                if (this.props.parentApi.data.settings.ui_show_simulators["enr"]) {
                    listSimulators.push({
                        id: "simulateur",
                        name: "EnR",
                        options: {
                            type: "enr",
                        },
                        ui_theme: "Simulateurs",
                        is_sub_indicator: false,
                    });
                }
            }
            listSimulators = listSimulators.map((item) => (
                <li>
                    <button
                        className="unstyled"
                        onClick={(e) => launchAnalysis(e, item.id, undefined, item)}
                    >
                        {item.name}
                    </button>
                </li>
            ));
        }
        let strategiesPanel = (
            <div className="panel-container">
                <ul className="analysis-themes">
                    <li>
                        <ul className="analysis-list">{strategiesLink}</ul>
                    </li>
                    <li>
                        <FiltersAccordion
                            title="Simulateurs"
                            emptyMessage="Sélectionnez un territoire"
                        >
                            {listSimulators}
                        </FiltersAccordion>
                    </li>
                </ul>
            </div>
        );

        let requisiteItem = (
            <label className="alert alert-warning">Sélectionnez un territoire</label>
        );

        let listIndicateurs = listThemeActions.find(
            (theme) => theme.id === "indicateur"
        ).items;
        if (listIndicateurs.length === 0) {
            listIndicateurs = requisiteItem;
        }

        let listAnalyses = listThemeActions.find(
            (theme) => theme.id === "analyse"
        ).items;
        if (listAnalyses.length === 0) {
            listAnalyses = requisiteItem;
        }

        const classIsLeftMenuOpen = this.props.parentApi.data.isLeftMenuOpen
            ? "visible"
            : "hidden";
        const classToggleButton = this.props.parentApi.data.isLeftMenuOpen
            ? "toggle_off"
            : "toggle_on";

        const classTheme = this.props.parentApi?.data
            ? " menu-" + this.props.parentApi.data.theme
            : "";

        const moduleIcon = (id, label) => {
            return (
                <li
                    key={id}
                    className={
                        id +
                        (this.props.parentApi.data.currentMenu === id ? " active" : "")
                    }
                    onClick={() => this.showMenu(id)}
                    data-tip={label}
                >
                    <div className="menu-icon" />
                </li>
            );
        };

        let autresFiltres = (
            <div className="left-menu">
                <div className="menu-container">
                    <ul className={"menu" + classTheme}>
                        <li
                            className={classToggleButton}
                            onClick={(e) => this.toggleMenu()}
                            data-tip="Afficher/Masquer le menu"
                        >
                            <div className="menu-icon" />
                        </li>
                        {moduleIcon("indicateur", "Indicateurs")}
                        {this.props.parentApi.data.settings.ui_show_poi &&
                            moduleIcon("installation", "Équipements")}
                        {this.props.parentApi.data.settings.ui_show_tableau_bord &&
                            moduleIcon("tableaux-bord", "Tableaux de bord")}
                        {this.props.parentApi.data.settings.ui_show_analyse &&
                            moduleIcon("analyse", "Analyses territoriales")}
                        {this.props.parentApi.data.settings.ui_show_plan_actions &&
                            moduleIcon("plan-action", "Stratégies territoriales")}
                    </ul>
                </div>
                <ReactTooltip
                    place="right"
                    type="warning"
                    effect="float"
                    data-delay-show="1000"
                    className="react-tooltip-custom"
                />

                <div className={"toggle-panel toggle-panel-" + classIsLeftMenuOpen}>
                    <div
                        className={
                            "panel panel-" +
                            (this.props.parentApi.data.currentMenu === "indicateur"
                                ? "on"
                                : "off")
                        }
                    >
                        <label className="title">Indicateurs</label>
                        <input
                            type="text"
                            name="search"
                            value={this.state.searchPattern}
                            onChange={(e) => this.handleSearchChange(e)}
                            placeholder="Rechercher"
                        />
                        <div className="sub-panel">{listIndicateurs}</div>
                    </div>

                    <div
                        className={
                            "panel panel-" +
                            (this.props.parentApi.data.currentMenu === "installation"
                                ? "on"
                                : "off")
                        }
                    >
                        <label className="title">Equipements</label>
                        <div className="sub-panel">
                            <div className="panel-container">{poiLayersFinal}</div>
                        </div>
                    </div>

                    <div
                        className={
                            "panel panel-" +
                            (this.props.parentApi.data.currentMenu === "tableaux-bord"
                                ? "on"
                                : "off")
                        }
                    >
                        <label className="title">Tableaux de bord</label>
                        <div className="sub-panel">{dashboardsPanel}</div>
                    </div>

                    <div
                        className={
                            "panel panel-" +
                            (this.props.parentApi.data.currentMenu === "analyse"
                                ? "on"
                                : "off")
                        }
                    >
                        <label className="title">Analyses territoriales</label>
                        <div className="sub-panel">{listAnalyses}</div>
                    </div>

                    <div
                        className={
                            "panel panel-" +
                            (this.props.parentApi.data.currentMenu === "plan-action"
                                ? "on"
                                : "off")
                        }
                    >
                        <label className="title">Stratégies territoriales</label>
                        <div className="sub-panel">{strategiesPanel}</div>
                    </div>
                </div>
            </div>
        );

        const { zonesManager } = this.props.parentApi.controller;
        let filtreTerritoires = "";
        if (zonesManager.zones) {
            filtreTerritoires = <MainZoneSelect parentApi={this.props.parentApi} />;
        }

        return (
            <div>
                <SEO
                    settings={this.props.parentApi.data.settings["seo"]}
                    page="home-page"
                    seoDetails={this.state.dataReferencement}
                />
                {filtreTerritoires}
                {autresFiltres}
            </div>
        );
    }
}

/**
 * Renders an accordion with appropriate classNames and styles
 *
 * @param{Object} props
 * @param{String} props.title The title shown when the accordion is colapsed
 * @param{Array<React.JSX.Element>} props.children The content of the accordion
 * @param{string} [props.emptyMessage] Message to show when the content is empty
 */
function FiltersAccordion({ title, children, emptyMessage }) {
    let content;
    if (!children?.length && emptyMessage != null) {
        content = <label className="alert alert-warning">{emptyMessage}</label>;
    } else {
        content = <ul className="analysis-list">{children}</ul>;
    }
    return (
        <Accordion allowMultipleExpanded allowZeroExpanded>
            <AccordionItem>
                <AccordionItemHeading>
                    <AccordionItemButton className="accordion-indicateur accordion__button">
                        <b className="retour-ligne-fleche">{title}</b>
                    </AccordionItemButton>
                </AccordionItemHeading>
                <AccordionItemPanel className="liste-indicateurs">
                    {content}
                </AccordionItemPanel>
            </AccordionItem>
        </Accordion>
    );
}

export default Filters;
