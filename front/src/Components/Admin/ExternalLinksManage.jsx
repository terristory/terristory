/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect, useCallback } from "react";
import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import "bootstrap/dist/css/bootstrap.min.css";
import TableContainer from "../Utils/TableContainer";

/**
 * This component displays a table from which you can delete a data source logo or access the update interface of a data source logo
 * (which is the same as that of adding a data source logo but with the values pre-filled with what has previously saved in the database).
 */

const ExternalLinksManage = ({ connected, userInfos, parentApi, updateTab }) => {
    const [data, setData] = useState([]);
    const [status, setStatus] = useState("");

    const getDataSource = useCallback(() => {
        const url = buildRegionUrl(config.api_logo_sources_list, parentApi.data.region);
        Api.callApi(url, null, "GET").then((response) => {
            setData(response);
        });
    }, [parentApi.data.region]);

    useEffect(() => {
        getDataSource();
    }, [getDataSource, status]);

    const deleteDataSource = (props) => {
        const id = props.row.original.id;
        const url = buildRegionUrl(
            config.api_delete_logo_sources,
            parentApi.data.region
        ).replace("#id#", id);
        Api.callApi(url, null, "DELETE").then((response) => {
            setStatus(response.message);
        });
    };

    const displayLogo = (props) => {
        const { id, extension } = props.row.original;
        return (
            <span style={{ height: "40px", maxWidth: "60px" }}>
                <img
                    id={id}
                    src={`img/logo_source_fiches/${parentApi.data.region}_logo${id}.${extension}`}
                    alt=""
                    style={{ height: "40px", maxWidth: "60px" }}
                />
            </span>
        );
    };

    const updateDataSourceLogo = (props) => {
        const { url, name, path_logo, id } = props.row.original;
        const dataSourceLogoCourant = {
            id,
            name,
            url,
            path_logo,
        };
        updateTab("update", dataSourceLogoCourant);
    };

    const columns = [
        {
            header: "Logo",
            filterable: false,
            cell: (props) => displayLogo(props),
        },
        {
            header: "Nom",
            accessorKey: "name",
        },
        {
            header: "URL",
            accessorKey: "url",
        },
        {
            header: "URL par défaut",
            accessorKey: "default_url",
        },
        {
            header: "Zones activées",
            accessorKey: "enabled_zone",
        },
        {
            id: "activate",
            header: "Action",
            accessorKey: "actif",
            cell: (props) => (
                <div className="actions">
                    <button
                        className="btn btn-danger"
                        onClick={() => deleteDataSource(props)}
                    >
                        Supprimer
                    </button>
                    <button
                        className="btn btn-warning"
                        onClick={() => updateDataSourceLogo(props)}
                    >
                        Mettre à jour
                    </button>
                </div>
            ),
        },
    ];

    if (!connected || !userInfos || userInfos?.profil !== "admin") {
        return <div>Non accessible.</div>;
    }

    return (
        <div>
            <div className="panel-body user-scenarii">
                <h3 className="panel-title pull-left">Gestion des logos</h3>
                <TableContainer
                    data={data}
                    columns={columns}
                    rowKey="id"
                    className="table-striped"
                    defaultPageSize={30}
                    filterable={true}
                />
            </div>
        </div>
    );
};

export default ExternalLinksManage;
