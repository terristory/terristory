/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import TableContainer from "../Utils/TableContainer";

import Api from "../../Controllers/Api";
import { TablePreview } from "./DataManage";
import DetailsPopup from "../DetailsPopup";

import config from "../../settings";
import { buildRegionUrl, exportToCSV } from "../../utils";
import AddCategoryForm from "./AddCategoryForm";

import "bootstrap/dist/css/bootstrap.min.css";

import styles from "../../style/admin/analyses.module.css";

/**
 * This component is used to manage indicator categories
 */
class CategoriesManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showDetailsTablesElement: false,
            showNewCategory: false,
            detailsTable: "",
            detailsTableTitle: "",
            categoriesFullData: undefined,
            isLoadingData: false,
            isForcedImport: {},
        };
        this.region = this.props.forcedRegion ?? this.props.parentApi.data.region;
        this.getCategoriesList();
    }

    componentDidMount() {
        if (!this.state.isLoadingData) {
            this.getCategoriesDataTables();
        }
    }

    /**
     * Retrieve the list of available categories
     * @return {JSON} the list of categories available for this region
     */
    getCategoriesList() {
        Api.callApi(buildRegionUrl(config.api_categories_list_url, this.region))
            .then((response) => {
                let categories = [];
                for (let cat in response) {
                    categories.push({
                        nom: cat,
                        miseAJourEnabled: false,
                    });
                }
                this.setState({ categories: categories });
            })
            .catch((e) => this.setState({ status: e.message }));
    }

    /**
     * Retrieve list of categories tables contents
     * @return {json} the list of tables
     */
    getCategoriesDataTables() {
        this.setState({ isLoadingData: true });
        // Call API to retrieve analysis
        let url = buildRegionUrl(config.api_categories_data_list_url, this.region);
        Api.callApi(url, null, "GET")
            .then((response) => {
                this.setState({
                    categoriesFullData: response,
                    isLoadingData: false,
                });
            })
            .catch((e) => this.setState({ categoriesFullData: undefined }));
    }

    /**
     * update the component state with the uploaded category file
     */
    onFichierCategorieSelection = (event, row) => {
        let categories = JSON.parse(JSON.stringify(this.state.categories));
        for (let cat of categories) {
            if (row.props.row.original.nom === cat.nom) {
                cat.miseAJourEnabled = true;
                cat.fichier = event.target.files[0];
            } else {
                cat.miseAJourEnabled = false;
            }
        }
        this.setState({
            categories: categories,
        });
    };

    /**
     *
     * @param {obj} details the objects
     */
    toggleDetailsCell(details, title) {
        let toggle = this.state.showDetailsTablesElement;
        // we toggle between displaying details or not
        if (!toggle) {
            this.setState({
                showDetailsTablesElement: true,
                detailsTable: details,
                detailsTableTitle: title,
            });
        } else {
            this.setState({
                showDetailsTablesElement: false,
                detailsTable: "",
                detailsTableTitle: "",
            });
        }
    }

    toggleAddNewCategory() {
        this.setState({
            showNewCategory: !this.state.showNewCategory,
        });
    }
    /**
     * Export table data to CSV file using exportToCSV function (utils).
     */
    exportCategoryContent(categoryName) {
        if (
            this.state.categoriesFullData &&
            this.state.categoriesFullData[categoryName]
        ) {
            exportToCSV(this.state.categoriesFullData[categoryName], "csv");
        }
    }

    /**
     * @return {dom} the rendering of the component
     */
    render() {
        if (
            !this.props.connected ||
            !this.props.userInfos ||
            this.props.userInfos?.profil !== "admin"
        ) {
            return <div>Non accessible.</div>;
        }

        /**
         * Callback to update a category
         * @param  {json} analysis : current row
         */
        const updateCategory = (categorie) => {
            // Object of type FormData
            let formData = new FormData();

            for (let cat of this.state.categories) {
                if (cat.nom === categorie.props.row.original.nom) {
                    formData.append("fichierCategorie", cat.fichier);
                    formData.append("nomFichierfichierCategorie", cat.fichier.name);
                    break;
                }
            }

            formData.append("categorie", categorie.props.row.original.nom);

            // Call API with the file and the name of the category to update
            Api.callApi(
                buildRegionUrl(config.api_categorie_url, this.region),
                formData,
                "PUT",
                "default"
            )
                .then((response) => {
                    this.setState({ status: response.message });
                })
                .catch((e) => {
                    this.setState({ status: e.message });
                });
        };

        /**
         * Callback to delete a category
         * @param  {json} analysis : current row
         */
        const deleteCategory = (category) => {
            let url = buildRegionUrl(
                config.api_categorie_edit_url,
                this.region
            ).replace("#categorie#", category.props.row.original.nom);
            // Call API with the name of the category to delete
            Api.callApi(url, null, "DELETE", "default")
                .then((response) => {
                    const newLine = "\r\n";
                    let r = window.confirm(
                        "Attention, cette opération n'est pas réversible. Voulez-vous supprimer cette catégorie ?" +
                            newLine +
                            newLine +
                            "Il y a : " +
                            newLine +
                            " - " +
                            response["related_charts"] +
                            " graphique(s) d'indicateurs liés (automatiquement désactivés après suppression)" +
                            newLine +
                            " - " +
                            response["related_datatables"] +
                            " table(s) de données ayant cette catégorie (inchangé(e)s après suppression)." +
                            newLine +
                            " - " +
                            response["related_dashboards"] +
                            " tableau(x) de bords ayant cette catégorie (graphiques retirés après suppression)."
                    );
                    if (r === true) {
                        url += "?confirm=true";
                        Api.callApi(url, null, "DELETE", "default")
                            .then((second_response) => {
                                this.setState({ status: second_response.message });
                                this.getCategoriesList();
                            })
                            .catch((e) => {
                                this.setState({ status: e.message });
                            });
                    }
                })
                .catch((e) => {
                    this.setState({ status: e.message });
                });
        };

        /**
         * Callback to import a category in current regional schema
         * @param  {json} category : current row data
         * @param  {boolean} force : force the update of an indicator
         */
        const importCategory = (category, force = false) => {
            let url =
                buildRegionUrl(
                    config.api_import_common_category.replace(
                        "#category#",
                        category.nom
                    ),
                    this.props.parentApi.data.region
                ) + `?forceUpdate=${force}`;
            Api.callApi(url, null, "PUT")
                .then((response) => {
                    this.setState({ status: "Import réussi !" });
                })
                .catch((e) => {
                    this.setState({ status: e.message });
                });
        };

        // Show errors
        let erreurs = "";
        if (this.state.status) {
            erreurs = <div className="alert alert-warning">{this.state.status}</div>;
        }

        const showDeletionButton =
            this.region !== "france" || this.props.userInfos?.globalAdmin;

        const columns = [
            {
                header: "Nom",
                accessorKey: "nom",
            },
            {
                header: "Aperçu de la table",
                accessorKey: "table_preview",
                cell: (props) => {
                    // we get the data from full categories query results
                    // (if available), else we send empty object.
                    const data =
                        this.state.categoriesFullData &&
                        this.state.categoriesFullData[props.row.original.nom]
                            ? this.state.categoriesFullData[props.row.original.nom]
                            : {};

                    const downloadButton = (
                        <p className="export-data-button-in-popup">
                            <button
                                className="btn btn-info"
                                onClick={() =>
                                    this.exportCategoryContent(props.row.original.nom)
                                }
                            >
                                Exporter le contenu de la table en csv
                            </button>
                        </p>
                    );
                    return (
                        <div>
                            <img
                                className="table_preview_icon"
                                src="img/table_preview.svg"
                                alt="Visualiser une partie des données"
                                title="Visualiser une partie des données"
                                onClick={() =>
                                    this.toggleDetailsCell(
                                        <TablePreview
                                            data={data}
                                            additionalElements={downloadButton}
                                        />,
                                        props.row.original.nom
                                    )
                                }
                            />
                        </div>
                    );
                },
                enableColumnFilter: false,
            },
            {
                id: "activate",
                header: "Action",
                accessorKey: "actif",
                cell: (props) => (
                    <div className={styles.AnalysisActions}>
                        <input
                            type="file"
                            onChange={(e) => {
                                this.onFichierCategorieSelection(e, {
                                    props,
                                });
                            }}
                        />
                        <button
                            className={
                                "btn btn-warning " +
                                (props.row.original.miseAJourEnabled ? "" : "hidden")
                            }
                            onClick={() => updateCategory({ props })}
                        >
                            Mettre à jour
                        </button>
                        {showDeletionButton && (
                            <button
                                className="btn btn-danger"
                                onClick={() => deleteCategory({ props })}
                            >
                                Supprimer
                            </button>
                        )}
                    </div>
                ),
                filterable: false,
            },
        ];

        if (this.region === "france") {
            columns.push({
                id: "common_actions",
                header: "Import",
                filterable: false,
                cell: (props) => (
                    <div class="row">
                        <div class="col">
                            <input
                                type="checkbox"
                                class="form-check-input"
                                id={"forceImport-" + props.row.original.nom}
                                checked={
                                    this.state.isForcedImport[props.row.original.nom]
                                }
                                onChange={(e) => {
                                    this.setState({
                                        isForcedImport: {
                                            ...this.state.isForcedImport,
                                            [props.row.original.nom]: e.target.checked,
                                        },
                                    });
                                }}
                            />
                            <label
                                class="form-check-label"
                                for={"forceImport-" + props.row.original.nom}
                            >
                                Écraser les données existantes
                            </label>
                        </div>
                        <div class="col">
                            <button
                                className={"btn btn-primary"}
                                onClick={() =>
                                    importCategory(
                                        props.row.original,
                                        this.state.isForcedImport[
                                            props.row.original.nom
                                        ]
                                    )
                                }
                            >
                                Importer dans mon schéma
                            </button>
                        </div>
                    </div>
                ),
            });
        }
        return (
            <div>
                {erreurs}
                {this.state.showDetailsTablesElement ? (
                    <DetailsPopup
                        title={
                            <>
                                Contenu de la table{" "}
                                <em>{this.state.detailsTableTitle}</em>{" "}
                            </>
                        }
                        content={this.state.detailsTable}
                        show={this.state.showDetailsTablesElement}
                        emptyMsg="Nothing to show"
                        callbackAfterClosing={() => this.toggleDetailsCell()}
                    />
                ) : null}
                {this.state.showNewCategory ? (
                    <DetailsPopup
                        title={"Ajouter une nouvelle catégorie"}
                        content={
                            <AddCategoryForm
                                region={this.region}
                                onAdd={() => {
                                    this.getCategoriesList();
                                    this.toggleAddNewCategory();
                                }}
                            ></AddCategoryForm>
                        }
                        show={this.state.showNewCategory}
                        emptyMsg="Nothing to show"
                        callbackAfterClosing={() => this.toggleAddNewCategory()}
                    />
                ) : null}
                <div className="panel-body user-scenarii">
                    <h3 className="panel-title pull-left">
                        Gestion des catégories
                        {this.region === "france" && " du socle commun"}
                    </h3>
                    <p>
                        <button
                            className={"btn btn-primary"}
                            onClick={() => this.toggleAddNewCategory()}
                        >
                            Ajouter une catégorie
                        </button>
                    </p>
                    <TableContainer
                        data={this.state.categories}
                        columns={columns}
                        tableClassName="table-striped"
                        defaultPageSize={30}
                        filterable={true}
                    />
                </div>
            </div>
        );
    }
}

export default CategoriesManage;
