/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import Api from "../../Controllers/Api";

import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";

/**
 * This component is a form used to create new categories for indicators.
 *
 * It is used in the indicator create/edit page.
 */
function AddCategoryForm({ region, onAdd }) {
    const [isCreating, setIsCreating] = useState(false);
    const [successMessage, setSuccessMessage] = useState("");
    const [errorMessage, setErrorMessage] = useState("");

    const handleSubmit = (event) => {
        setIsCreating(true);
        event.preventDefault();

        // Reset previous messages
        setSuccessMessage("");
        setErrorMessage("");

        // get named inputs values
        const formData = new FormData(event.target);

        Api.callApi(
            buildRegionUrl(config.api_categorie_ajouter_url, region),
            formData,
            "POST",
            "default"
        )
            .then((response) => {
                setSuccessMessage("La catégorie a été ajoutée avec succès !");
                onAdd();
                event.target.reset(); // reset form
            })
            .catch((e) => {
                // Handle error
                setErrorMessage(
                    e.message ??
                        "Une erreur s'est produite lors de l'ajout de la catégorie."
                );
            })
            .finally(() => setIsCreating(false));
    };

    return (
        <div>
            <form onSubmit={handleSubmit} className="">
                <div className="row g-3 align-items-center">
                    <div className="col-md-5">
                        <label htmlFor="csvFile" className="form-label">
                            Importer le fichier CSV
                        </label>
                        <input
                            className="form-control"
                            type="file"
                            id="csvFile"
                            name="fichier" // important. name expected by the api
                            accept=".csv"
                            aria-describedby="csvHelp"
                            required
                        />
                        <div id="csvHelp" className="form-text">
                            Le format à respecter est donné par{" "}
                            <a
                                className="form-text"
                                href={"/doc/template_nouvelle_categorie.csv"}
                                title="Template csv catégorie"
                                target="_blank"
                                rel="noreferrer"
                            >
                                ce template
                            </a>
                        </div>
                    </div>
                    <div className="col-md-5">
                        <label htmlFor="category-name" className="form-label">
                            Nom de la nouvelle catégorie
                        </label>
                        <input
                            className="form-control"
                            id="category-name"
                            name="nom" // important. name expected by the api
                            maxLength="100"
                            aria-describedby="category-name-help"
                            required
                        ></input>
                        <div id="category-name-help" className="form-text">
                            Doit correspondre au nom de la colonne dans la table des
                            données.
                        </div>
                    </div>
                    <div className="col-md-2">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            disabled={isCreating}
                        >
                            Ajouter la catégorie
                            {isCreating && (
                                <span
                                    className="spinner-border spinner-border-sm ms-2"
                                    role="status"
                                    aria-hidden="true"
                                ></span>
                            )}
                        </button>
                    </div>
                </div>
            </form>
            {successMessage && <p className="text-success mb-3">{successMessage}</p>}
            {errorMessage && <p className="text-danger mb-3">{errorMessage}</p>}
        </div>
    );
}

export default AddCategoryForm;
