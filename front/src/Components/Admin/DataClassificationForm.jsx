import React, { useState, useEffect } from "react";

/**
 * @typedef {Object} ClassificationConfig
 * @property {"dynamic" | "unclassified"} classification_type
 * @property {{ min: number, max: number } | undefined} [classification_extent] -
 */

/**
 * A form to configure data classification for chorpleth map visualization.
 *
 * @param {Object} props
 * @param {ClassificationConfig} props.classificationConfig
 * @param {(classificationConfig: ClassificationConfig) => void} onClassificationConfigChange
 * @returns {JSX.Element}
 */
function DataClassificationForm({
    classificationConfig,
    onClassificationConfigChange,
}) {
    const [errorMessage, setErrorMessage] = useState(undefined);

    useEffect(() => {
        const [, error] = validateClassificationConfig(classificationConfig);
        setErrorMessage(error);
    }, [classificationConfig]);

    const validateAndUpdate = (updatedConfig) => {
        const [, error] = validateClassificationConfig(updatedConfig);
        setErrorMessage(error);
        onClassificationConfigChange(updatedConfig);
    };

    const handleClassificationTypeChange = (e) => {
        const updatedConfig = {
            ...classificationConfig,
            classification_type: e.target.value,
        };
        validateAndUpdate(updatedConfig);
    };

    const handleMinValueChange = (e) => {
        const updatedConfig = {
            ...classificationConfig,
            classification_extent: {
                ...classificationConfig.classification_extent,
                min: parseFloat(e.target.value) || 0,
            },
        };
        validateAndUpdate(updatedConfig);
    };

    const handleMaxValueChange = (e) => {
        const updatedConfig = {
            ...classificationConfig,
            classification_extent: {
                ...classificationConfig.classification_extent,
                max: parseFloat(e.target.value) || 0,
            },
        };
        validateAndUpdate(updatedConfig);
    };

    const classificationOptions = [
        { value: "dynamic", label: "Classification dynamique" },
        { value: "unclassified", label: "Sans classes (échelle continue)" },
    ];

    return (
        <div className="card mb-3">
            <div className="card-body">
                <h5>Classification</h5>
                <p>
                    Configurer la classification utilisée pour la représentation des
                    données sous forme de carte choroplèthe.
                </p>
                <div className="mb-3">
                    <label htmlFor="classificationType" className="form-label">
                        Type de Classification
                    </label>
                    <select
                        className="form-select"
                        id="classificationType"
                        value={classificationConfig.classification_type}
                        onChange={handleClassificationTypeChange}
                    >
                        {classificationOptions.map((option) => (
                            <option key={option.value} value={option.value}>
                                {option.label}
                            </option>
                        ))}
                    </select>
                </div>

                {classificationConfig.classification_type === "unclassified" ? (
                    <p>
                        On utilise une échelle de couleur continue (pas de
                        classification) entre les valeurs min et max définies
                        ci-dessous. L'échelle reste fixe au changement de filtre,
                        territoire, année...
                    </p>
                ) : (
                    <p>
                        La classification (en 6 classes) est calculée automatiquement en
                        fonction des données présentées et selon la méthode sélectionnée
                        (parmi quantile (par défaut), equidistant, logarithmique).
                        L'échelle est donc recalculée à chaque changement de filtre,
                        territoire, année...{" "}
                    </p>
                )}

                {classificationConfig.classification_type === "unclassified" && (
                    <div className="row g-3">
                        <div className="col-md-6">
                            <label htmlFor="minValue" className="form-label">
                                Borne inferieure
                            </label>
                            <input
                                type="number"
                                className="form-control"
                                id="minValue"
                                value={classificationConfig.classification_extent?.min}
                                onChange={handleMinValueChange}
                            />
                        </div>
                        <div className="col-md-6">
                            <label htmlFor="maxValue" className="form-label">
                                Borne Superieure
                            </label>
                            <input
                                type="number"
                                className="form-control"
                                id="maxValue"
                                value={classificationConfig.classification_extent?.max}
                                onChange={handleMaxValueChange}
                            />
                        </div>
                    </div>
                )}
                {errorMessage && <div className="text-danger mt-3">{errorMessage}</div>}
            </div>
        </div>
    );
}

export default DataClassificationForm;

/**
 * Validates the classification configuration.
 *
 * @param {ClassificationConfig} classificationConfig
 * @returns {[boolean, string | undefined]} Tuple of (isValid, errorMessage)
 */
export function validateClassificationConfig(classificationConfig) {
    const { classification_type, classification_extent } = classificationConfig;

    if (!["dynamic", "unclassified"].includes(classification_type)) {
        return [false, "Type de classification invalide."];
    }

    if (classification_type === "unclassified") {
        if (!classification_extent) {
            return [
                false,
                "L'étendue de classification est requise pour le type 'Sans classe (échelle continue)'.",
            ];
        }

        const { min, max } = classification_extent;

        if (min === undefined || max === undefined) {
            return [
                false,
                "Les valeurs min et max doivent être définies pour l'étendue de classification",
            ];
        }

        if (min >= max) {
            return [
                false,
                "La valeur min doit être strictement inférieure à la valeur max.",
            ];
        }
    }

    return [true, undefined];
}
