/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect, useCallback } from "react";
import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import Select from "react-select";
import "bootstrap-icons/font/bootstrap-icons.css";

/**
 * This component is used to add or update a data source logo.
 */
const ExternalLinkAdd = ({ mode, dataSourceLogoCourant, parentApi }) => {
    const [sources, setSources] = useState([
        {
            id: undefined,
            name: "",
            file: undefined,
            url: "",
            extension: "",
            isValidUrl: undefined,
            default_url: "",
            enabled_zone: [],
        },
    ]);
    const [status, setStatus] = useState(false);
    const [statusMessage, setStatusMessage] = useState([]);
    const [urlEnabled, setUrlEnabled] = useState(false);

    const zones = Object.keys(parentApi.controller.zonesManager.zoneLists).map(
        (zone) => ({
            value: zone,
            label: zone,
        })
    );

    const allowedVariables = [
        "<type_territoire>",
        "<code_territoire>",
        "<nom_territoire_slug>",
    ];

    const getDataSource = useCallback(async () => {
        let id = dataSourceLogoCourant.id;
        let url = buildRegionUrl(
            config.api_logo_sources,
            parentApi.data.region
        ).replace("#id#", id);

        try {
            const response = await Api.callApi(url, null, "GET");
            // Reform the Select component
            const formattedSource = {
                ...response,
                enabled_zone: response.enabled_zone
                    ? response.enabled_zone.split(",").map((zoneValue) => ({
                          value: zoneValue.trim(),
                          label: zoneValue.trim(),
                      }))
                    : [], // Set to an empty array if enabled_zone does not exist
            };
            setSources([formattedSource]);
        } catch (error) {
            setStatus("error");
            setStatusMessage(error.message);
        }
    }, [dataSourceLogoCourant.id, parentApi.data.region]);

    useEffect(() => {
        if (mode === "update") {
            getDataSource();
        }
    }, [mode, getDataSource]);

    useEffect(() => {
        // Enable URL input only when one or more zone is selected
        setUrlEnabled(sources.some((source) => source.enabled_zone.length >= 1));
    }, [sources]);

    const handleSubmit = async (actionType) => {
        const isUpdateMode = mode === "update";

        // Iterate over all sources to validate fields
        for (let item of sources) {
            if (!item.name || !item.url || (!item.file && !isUpdateMode)) {
                setStatus("error");
                setStatusMessage(
                    "Tous les champs (nom, URL, fichier) doivent être remplis avant de soumettre."
                );
                return; // Prevent submission if any required field is missing
            }
        }

        // Convert enabled_zone (list of { value, label }) to a comma-separated string
        const formattedSources = sources.map((item) => ({
            ...item,
            enabled_zone: item.enabled_zone
                ? item.enabled_zone.map((zone) => zone.value).join(",")
                : "",
        }));

        // Prepare form data
        const formData = new FormData();
        formData.append("sources", JSON.stringify(formattedSources));

        // Append files to formData
        formattedSources.forEach((item) => {
            if (item.file) {
                formData.append(`file${item.id}`, item.file);
            }
        });

        const apiUrl = buildRegionUrl(
            actionType === "update"
                ? config.api_update_logo_sources
                : config.api_integration_logo_sources,
            parentApi.data.region
        ).replace("#id#", dataSourceLogoCourant?.id || "");

        try {
            const response = await Api.callApi(apiUrl, formData, "PUT", "default");

            setStatus("success");
            setStatusMessage(response.message);
        } catch (error) {
            setStatus("warning");
            setStatusMessage(error.message);
        }
    };

    const addInput = () => {
        setSources([
            ...sources,
            {
                id: undefined,
                name: "",
                file: undefined,
                url: "",
                extension: "",
                isValidUrl: undefined,
                default_url: "",
                enabled_zone: [],
            },
        ]);
    };

    const removeInput = (i) => {
        setSources(sources.filter((_, index) => index !== i));
    };

    const handleChange = (i, field, value) => {
        setStatus(false);
        setStatusMessage("");

        const updatedSources = [...sources];
        updatedSources[i] = {
            ...updatedSources[i],
            id: mode === "update" ? updatedSources[i].id : i,
            [field]: field === "file" ? value : value,
        };
        // URL validation
        if (field === "url") {
            // Check if the value contains any opening/closing angle brackets
            const hasOpeningBracket = value.includes("<");
            const hasClosingBracket = value.includes(">");

            if (hasOpeningBracket || hasClosingBracket) {
                // Validate that every '<' has a matching '>'
                const regex = /<[^>]+>/g; // Match anything between < and >
                const matches = value.match(regex);

                if (!matches || hasOpeningBracket !== hasClosingBracket) {
                    // Invalid case: mismatched or incomplete brackets
                    updatedSources[i].isValidUrl = false;
                    setStatus("error");
                    setStatusMessage(
                        "L'URL contient des variables incomplètes ou mal formatées. Assurez-vous que chaque '<' a un '>'."
                    );
                } else {
                    // Check if all matched variables are valid (case-insensitive)
                    const isValid = matches.every((variable) =>
                        allowedVariables.includes(variable.toLowerCase())
                    );

                    if (!isValid) {
                        updatedSources[i].isValidUrl = false;
                        setStatus("error");
                        setStatusMessage(
                            `Les variables dans l'URL ne sont pas valides. Utilisez l'une des suivantes : ${allowedVariables
                                .map((v) => v.toUpperCase())
                                .join(", ")}`
                        );
                    } else {
                        updatedSources[i].isValidUrl = true;
                        setStatus("success");
                        setStatusMessage("URL valide.");
                    }
                }
            } else {
                // Allow URLs without < >
                updatedSources[i].isValidUrl = true;
                setStatus("success");
                setStatusMessage("URL valide.");
            }
        }

        setSources(updatedSources);
    };

    const displayLogo = (id, extension) => (
        <span style={{ height: "100px", maxWidth: "200px" }}>
            <img
                id={id}
                src={`img/logo_source_fiches/${parentApi.data.region}_logo${id}.${extension}`}
                style={{ height: "150px", maxWidth: "200px" }}
                alt="Logo"
            />
            <br />
            <em>Logo actuel</em>
        </span>
    );

    return (
        <div className="container">
            <h3 className="mb-4">
                {mode === "update"
                    ? "Mettre à jour un lien externe"
                    : "Ajouter des liens externes"}
            </h3>
            <div className="help-text-external-link">
                <p>
                    Cette page vous permet de créer un ou plusieurs liens externes. Vous
                    pouvez :
                </p>
                <ul>
                    <li>
                        <strong>Créer un lien simple et statique</strong> : Un lien sans
                        variables, qui ne change pas.
                    </li>
                    <li>
                        <strong>Créer des liens dynamiques</strong> : Des liens
                        contenant des variables qui seront remplacées en fonction des
                        informations territoriales.
                    </li>
                </ul>
                <p>
                    Les liens dynamiques peuvent rediriger vers des outils externes,
                    tels qu'ALDO, CRATER, ou d'autres plateformes, en adaptant chaque
                    lien selon le type et le code du territoire sélectionné.
                </p>
                <p>Vous disposez de trois types de variables utilisables :</p>
                <ul>
                    <li>
                        <strong>&lt;TYPE_TERRITOIRE&gt;</strong> : Le type de territoire
                        (par exemple, département, commune, EPCI)
                    </li>
                    <li>
                        <strong>&lt;CODE_TERRITOIRE&gt;</strong> : Le code correspondant
                        au territoire sélectionné (par exemple, code INSEE, SIREN)
                    </li>
                    <li>
                        <strong>&lt;NOM_TERRITOIRE_SLUG&gt;</strong> : Nom du territoire
                    </li>
                </ul>
                <p>
                    <strong>Exemples d’utilisation</strong> :
                </p>
                <ul>
                    <li>
                        Pour rediriger vers une page ALDO, vous pouvez construire un
                        lien comme suit :
                        <code>
                            {" "}
                            https://aldo.territoiresentransitions.fr/&lt;TYPE_TERRITOIRE&gt;/&lt;CODE_TERRITOIRE&gt;
                        </code>
                        <br />
                        Si vous sélectionnez une commune avec le code 69280, le lien
                        final sera :
                        <code>
                            {" "}
                            https://aldo.territoiresentransitions.fr/commune/69280
                        </code>
                    </li>
                    <br />
                    <li>
                        Pour rediriger vers une page CRATER, vous pouvez utiliser le
                        lien :
                        <code>
                            {" "}
                            https://crater.resiliencealimentaire.org/diagnostic/&lt;NOM_TERRITOIRE_SLUG&gt;
                        </code>
                        <br />
                        Pour une redirection vers la ville de Lyon, le lien final serait
                        :
                        <code>
                            {" "}
                            https://crater.resiliencealimentaire.org/diagnostic/lyon
                        </code>
                    </li>
                </ul>
            </div>

            <br />
            <div className="form-group">
                {sources.map((item, i) => (
                    <div key={`logo_form_${i}`} className="card mb-3">
                        <div className="card-header">
                            <h5 className="mb-0">Lien {i + 1}</h5>
                        </div>
                        <div className="card-body">
                            <div className="mb-3">
                                <label htmlFor={`name_${i}`} className="form-label">
                                    Nom<span style={{ color: "red" }}>*</span>
                                </label>
                                <input
                                    type="text"
                                    className="form-control form-link-control"
                                    id={`name_${i}`}
                                    defaultValue={item.name}
                                    onChange={(e) =>
                                        handleChange(i, "name", e.target.value)
                                    }
                                    placeholder="Nom de la source"
                                />
                            </div>
                            <div className="mb-3">
                                <label htmlFor={`url_${i}`} className="form-label">
                                    URL<span style={{ color: "red" }}>*</span>
                                </label>
                                <input
                                    type="text"
                                    className={`form-control form-link-control ${
                                        item.isValidUrl === false
                                            ? "is-invalid"
                                            : item.isValidUrl === true
                                              ? "is-valid"
                                              : ""
                                    }`}
                                    id={`url_${i}`}
                                    defaultValue={item.url}
                                    onChange={(e) =>
                                        handleChange(i, "url", e.target.value)
                                    }
                                    placeholder="URL de la source"
                                />
                                {item.isValidUrl === false && (
                                    <div className="invalid-feedback">
                                        {statusMessage}
                                    </div>
                                )}
                            </div>
                            <div className="mb-3">
                                <label htmlFor={`file_${i}`} className="form-label">
                                    Image<span style={{ color: "red" }}>*</span>
                                </label>
                                <input
                                    type="file"
                                    className="form-control form-link-control"
                                    id={`file_${i}`}
                                    onChange={(e) =>
                                        handleChange(i, "file", e.target.files[0])
                                    }
                                />
                            </div>
                            <div className="mb-3">
                                <label
                                    htmlFor={`zonesSelect_${i}`}
                                    className="form-label"
                                >
                                    Seulement pour les échelles
                                </label>
                                <Select
                                    id={`zonesSelect_${i}`}
                                    options={zones}
                                    isMulti
                                    value={item.enabled_zone}
                                    onChange={(selectedOptions) =>
                                        handleChange(i, "enabled_zone", selectedOptions)
                                    }
                                />
                            </div>
                            {urlEnabled && (
                                <div className="mb-3">
                                    <label htmlFor="default_url" className="form-label">
                                        URL par défaut
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control form-link-control"
                                        id={`default_url_${i}`}
                                        value={item.default_url}
                                        onChange={(e) =>
                                            handleChange(
                                                i,
                                                "default_url",
                                                e.target.value
                                            )
                                        }
                                        placeholder="URL par défaut"
                                    />
                                </div>
                            )}

                            {mode !== "update" && (
                                <button
                                    className="btn btn-danger mt-2"
                                    onClick={() => removeInput(i)}
                                >
                                    Supprimer cette source{" "}
                                    <i className="bi bi-dash-circle-fill"></i>
                                </button>
                            )}
                        </div>
                    </div>
                ))}
            </div>

            {mode !== "update" && (
                <button className="btn btn-success mb-4" onClick={addInput}>
                    Ajouter une autre source <i className="bi bi-plus-circle-fill"></i>
                </button>
            )}

            {mode === "update" &&
                sources[0].id &&
                displayLogo(sources[0].id, sources[0].extension)}

            <hr />

            <button
                className="btn btn-primary"
                onClick={() => {
                    if (sources.every((item) => item.isValidUrl !== false)) {
                        handleSubmit(mode === "update" ? "update" : "add");
                    } else {
                        setStatus("error");
                        setStatusMessage(
                            "Veuillez corriger les erreurs de validation des URL avant de soumettre."
                        );
                    }
                }}
            >
                {mode === "update" ? "Mettre à jour le lien" : "Ajouter le lien"}
            </button>
            <br />
            {status && (
                <div
                    className={`alert ${
                        status === "error" ? "alert-warning" : `alert-${status}`
                    }`}
                    role="alert"
                >
                    {statusMessage}
                </div>
            )}
        </div>
    );
};

export default ExternalLinkAdd;
