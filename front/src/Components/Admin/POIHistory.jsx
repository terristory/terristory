/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import TableContainer from "../Utils/TableContainer";

import Api from "../../Controllers/Api";
import { buildRegionUrl } from "../../utils";

import config from "../../settings";

import "bootstrap/dist/css/bootstrap.min.css";

function displayLinkCoordinates(coordinates) {
    if (coordinates === undefined || !coordinates) {
        return "";
    }
    let coords = coordinates.split(" ");
    return (
        <a
            target="_blank"
            rel="noreferrer"
            className="link_coordinates"
            href={
                "https://www.openstreetmap.org/#map=14/" + coords[1] + "/" + coords[0]
            }
        >
            {Math.round(coords[1] * 1000) / 1000}, {Math.round(coords[0] * 1000) / 1000}
        </a>
    );
}

/**
 * This component allows you to manage the history of modifications on data layers
 */
class POIHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: undefined,
        };
        this.region = this.props.forcedRegion ?? this.props.parentApi.data.region;

        this.getHistory();
    }

    // Get history of modifications on layers
    getHistory() {
        // Call api to get history
        Api.callApi(
            buildRegionUrl(config.api_poi_history_url, this.region),
            undefined,
            "GET"
        )
            .then((response) => {
                response = response.map((r) => ({
                    ...r,
                    mise_a_jour: new Date(r.mise_a_jour),
                }));
                // Store user list
                this.setState({
                    history: response,
                });
            })
            .catch((e) => this.setState({ status: e.message }));
    }

    render() {
        if (
            !this.props.connected ||
            !this.props.userInfos ||
            this.props.userInfos?.profil !== "admin"
        ) {
            return <div>Non accessible.</div>;
        }
        const columns = [
            {
                header: "Id",
                accessorKey: "id",
            },
            {
                header: "Couche",
                accessorKey: "layer",
            },
            {
                header: "Action",
                accessorKey: "action",
                maxWidth: 100,
            },
            {
                header: "Utilisateur",
                accessorKey: "utilisateur",
            },
            {
                header: "Changement",
                accessorKey: "changement",
                cell: (props) => {
                    return Array.isArray(props.getValue())
                        ? props.getValue().map((element) => {
                              return (
                                  <>
                                      {element}
                                      <br />
                                  </>
                              );
                          })
                        : (props.getValue() ?? "-");
                },
                minWidth: 250,
            },
            {
                header: "Geolocalisation",
                accessorKey: "geolocalisation",
            },
            {
                header: "Position courante",
                accessorKey: "position_courante",
                cell: (props) => {
                    return displayLinkCoordinates(props.row.original.position_courante);
                },
            },
            {
                header: "Position précédente",
                accessorKey: "position_precedente",
                cell: (props) => {
                    return displayLinkCoordinates(
                        props.row.original.position_precedente
                    );
                },
            },
            {
                header: "Mise à jour",
                accessorKey: "mise_a_jour",
                cell: (props) => {
                    return props.getValue().toLocaleString();
                },
            },
        ];
        return (
            <div>
                <div className="panel-body">
                    <h3 className="panel-title pull-left">
                        Dernières modifications d'équipements
                    </h3>
                    <TableContainer
                        data={this.state.history}
                        columns={columns}
                        tableClassName="table-striped"
                        defaultPageSize={10}
                        filterable={true}
                    />
                </div>
            </div>
        );
    }
}

export default POIHistory;
