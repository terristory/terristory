/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect, useCallback, useRef, useMemo } from "react";
import { SketchPicker } from "react-color";
import { Line } from "react-chartjs-2";
import { ReactGrid } from "@silevis/reactgrid";
import "bootstrap/dist/css/bootstrap.min.css";

import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import configData from "../../settings_data";
import { ZoneSelect } from "../SelectionObjet";

/**
 * This component is used to add or update an objective
 */
function SupraGoalsAdd(props) {
    const { zonesManager } = props.parentApi.controller;
    const { region } = props.parentApi.data;

    const [goalTitle, setGoalTitle] = useState("");
    const [goalDescription, setGoalDescription] = useState("");
    const [goalIndicator, setGoalIndicator] = useState("");
    const [goalDetailLevel, setGoalDetailLevel] = useState("global");
    const [goalFilter, setGoalFilter] = useState([]);
    const [goalRefYear, setGoalRefYear] = useState("2020");
    const [goalLastYear, setGoalLastYear] = useState(
        configData.planActionDerniereAnnee.toFixed(0)
    );
    const [goalLineColor, setGoalLineColor] = useState("#999999");
    const [goalTrajectory, setGoalTrajectory] = useState({
        annees_valeurs: {},
        annees_modifiees: {},
    });
    const [goalZoneAssignations, setGoalAssignations] = useState([]);

    const [status, setStatus] = useState({});

    const [hasChanges, setHasChanges] = useState(props.mode !== "mise_a_jour");
    useEffect(() => {
        setHasChanges(true);
    }, [
        goalTitle, goalDescription, goalIndicator, goalDetailLevel, goalFilter,
        goalRefYear, goalLastYear, goalLineColor, goalTrajectory, goalZoneAssignations,
    ]); // prettier-ignore

    const [availableIndicators, setAvailableIndicators] = useState([]);
    useEffect(() => {
        Api.callApi(buildRegionUrl(config.api_pcaet_trajectories_list, region)).then(
            setAvailableIndicators
        );
    }, [region]);

    const [availableRefYears, setAvailableRefYears] = useState([]);
    useEffect(() => {
        if (!goalIndicator) return;
        Api.callApi(
            buildRegionUrl(
                config.api_liste_annees_url.replace("#table#", goalIndicator),
                region
            )
        ).then((years) => {
            setAvailableRefYears(years);
            setGoalRefYear((prevState) => prevState || Math.max(...years).toFixed(0));
        });
    }, [goalIndicator, region]);

    const [availableLastYears, setAvailableLastYears] = useState([]);
    useEffect(() => {
        let years = [];
        for (
            let y = parseInt(goalRefYear) + 1;
            y <= configData.planActionDerniereAnnee;
            y++
        ) {
            years.push(y);
        }
        setAvailableLastYears(years);
        setGoalLastYear((lastYear) =>
            years.includes(parseInt(lastYear)) ? lastYear : String(years[0])
        );
    }, [goalRefYear]);

    useEffect(() => {
        setGoalTrajectory((prevTrajectory) => {
            let { annees_valeurs, annees_modifiees } = prevTrajectory;
            annees_valeurs = Object.fromEntries(
                Object.entries(annees_valeurs).filter(
                    ([year, _]) => goalRefYear <= year && year <= goalLastYear
                )
            );
            annees_modifiees = Object.fromEntries(
                Object.entries(annees_modifiees).filter(
                    ([year, _]) => goalRefYear <= year && year <= goalLastYear
                )
            );
            if (!Object.values(annees_modifiees).some(Boolean)) {
                annees_valeurs[goalRefYear] = 0;
                annees_modifiees[goalRefYear] = true;
            }
            return { annees_valeurs, annees_modifiees };
        });
    }, [goalRefYear, goalLastYear]);

    useEffect(() => {
        if (!props.objectifCourant || props.mode !== "mise_a_jour") {
            return;
        }
        let url = buildRegionUrl(config.api_objectifs, region);
        url = url.replace("#supra_goal_id#", props.objectifCourant);
        // Call API to get objectives data
        const promise = Api.callApi(url);
        promise
            .then(({ goal_meta, goal_values, assignations }) => {
                setGoalTitle(goal_meta.titre);
                setGoalDescription(goal_meta.description);
                setGoalIndicator(goal_meta.graphique);
                setGoalRefYear(goal_meta.annee_reference);
                setGoalLastYear(goal_meta.derniere_annee_projection);
                setGoalLineColor(goal_meta.couleur);
                setGoalDetailLevel(
                    goal_meta.filter.length ? "par-categorie" : "global"
                );
                setGoalFilter(goal_meta.filter);

                let trajectory = { annees_valeurs: {}, annees_modifiees: {} };
                for (const { annee, valeur, annee_modifiee } of goal_values) {
                    // Convert data to the format expected by the component
                    trajectory.annees_valeurs[annee] = valeur;
                    trajectory.annees_modifiees[annee] = annee_modifiee;
                }
                setGoalTrajectory(trajectory);

                setGoalAssignations(
                    assignations.map(({ type_territoire, code_territoire }) => ({
                        // Convert data to the format expected by the component
                        zoneType: code_territoire
                            ? type_territoire
                            : type_territoire.slice(0, -1),
                        zoneId: code_territoire || undefined,
                    }))
                );

                setTimeout(() => setHasChanges(false), 300);
            })
            .catch((error) => {
                if (error.name === "AbortError") return;
                setStatus({ type: "error", message: error.message });
            });

        return () => promise.abort();
    }, [props.objectifCourant, props.mode, region]);

    async function onFormSubmit(event) {
        event.preventDefault();

        let parameters = {
            graphiques: goalIndicator,
            annee: parseInt(goalRefYear),
            titre: goalTitle,
            couleur: goalLineColor,
            derniereAnneeProjection: parseInt(goalLastYear),
            description: goalDescription,
            filter: goalDetailLevel === "global" ? [] : goalFilter,
        };

        let goalDataframe = [];
        for (let year in goalTrajectory.annees_valeurs) {
            goalDataframe.push({
                annee: parseInt(year),
                valeur: goalTrajectory.annees_valeurs[year],
                annee_modifiee: goalTrajectory.annees_modifiees[year],
            });
        }

        if (goalDataframe.length === 0) {
            setStatus({
                type: "error",
                message: "Veuillez saisir les valeurs de votre objectif",
            });
            return;
        }

        let assignations = goalZoneAssignations.map(({ zoneType, zoneId }) => ({
            type_territoire: zoneType,
            code_territoire: zoneId,
        }));

        let body = {
            obj: goalDataframe,
            params: parameters,
            link: assignations,
        };
        let url;
        if (props.mode === "mise_a_jour") {
            url = buildRegionUrl(config.api_maj_objectif, region);
            url = url.replace("#supra_goal_id#", props.objectifCourant);
        } else {
            url = buildRegionUrl(config.api_integration_objectifs, region);
        }
        let goalId;
        try {
            const response = await Api.callApi(url, JSON.stringify(body), "PUT");
            setStatus({ type: "success", message: response.message });
            setHasChanges(false);
            goalId = response.supra_goal_id ?? props.objectifCourant;
        } catch (e) {
            setStatus({ type: "error", message: e.message });
            return;
        }
        if (props.mode !== "mise_a_jour") {
            props.modeAjoutMiseAJourObjectifs("mise_a_jour", goalId, false);
        }
    }

    let formTitle = (
        <>
            <h3 className="tstitle2">Ajouter un objectif</h3>
            <div className="input">
                <label htmlFor="title">Nom</label>
                <input
                    type="text"
                    id="title"
                    value={goalTitle}
                    onChange={(e) => setGoalTitle(e.target.value)}
                    required
                ></input>
            </div>
        </>
    );
    if (props.mode === "mise_a_jour") {
        formTitle = <h3 className="tstitle2">Modifier l’objectif : {goalTitle}</h3>;
    }

    let formDescription = (
        <div className="input">
            <label htmlFor="description">Description</label>
            <textarea
                id="description"
                value={goalDescription}
                onChange={(e) => setGoalDescription(e.target.value)}
            ></textarea>
        </div>
    );

    let formIndicator = "";
    if (props.mode !== "mise_a_jour") {
        formIndicator = (
            <div className="input">
                <label htmlFor="trajectory-type">Indicateur</label>
                <select
                    id="trajectory-type"
                    value={goalIndicator}
                    onChange={(e) => setGoalIndicator(e.target.value)}
                    required
                >
                    <option disabled value="">
                        Veuillez sélectionner un indicateur
                    </option>
                    {availableIndicators.map(({ table, name }) => (
                        <option key={table} value={table}>
                            {name}
                        </option>
                    ))}
                </select>
            </div>
        );
    }
    if (props.mode === "mise_a_jour" && goalIndicator) {
        const label = availableIndicators.find(
            ({ table }) => table === goalIndicator
        )?.name;
        if (label) {
            formIndicator = (
                <div className="input">
                    <label>Indicateur</label>
                    <output>{label}</output>
                </div>
            );
        }
    }

    let formDetailLevel = "";
    let formCategoryFilter = "";
    if (goalIndicator && availableIndicators.length) {
        const trajectoire = availableIndicators.find(
            ({ table }) => table === goalIndicator
        );
        // On suppose qu'il y a exactement une catégorie
        const categorie = Object.values(trajectoire.categories)[0];
        const filtreMeta = trajectoire.filters[categorie.categorie];
        formDetailLevel = (
            <div className="input">
                <label htmlFor="detail-level">Niveau de détail</label>
                <select
                    id="detail-level"
                    value={goalDetailLevel}
                    onChange={(e) => setGoalDetailLevel(e.target.value)}
                >
                    <option value="global">Trajectoire globale</option>
                    <option value="par-categorie">{categorie.titre}</option>
                </select>
            </div>
        );
        if (goalDetailLevel === "par-categorie") {
            const onFilterChange = (event) =>
                setGoalFilter((filter) => {
                    filter = [...filter];
                    const idIfPresent = filter.findIndex((x) => x === event.target.id);
                    if (event.target.checked && idIfPresent === -1) {
                        filter.push(event.target.id);
                    }
                    if (!event.target.checked && idIfPresent !== -1) {
                        filter.splice(idIfPresent, 1);
                    }
                    return filter;
                });

            formCategoryFilter = (
                <fieldset>
                    <legend>Filtre {categorie.titre.toLowerCase()}</legend>
                    {filtreMeta.map(({ filtre_categorie }) => (
                        <div key={filtre_categorie} className="check-input">
                            <input
                                type="checkbox"
                                id={filtre_categorie}
                                onChange={onFilterChange}
                                checked={goalFilter.includes(filtre_categorie)}
                            />{" "}
                            <label htmlFor={filtre_categorie} style={{ width: "67%" }}>
                                {filtre_categorie.split(".")[1]}
                            </label>
                        </div>
                    ))}
                </fieldset>
            );
        }
    }

    let formReferenceYear = "";
    let formLastYear = "";
    if (availableRefYears.length) {
        formReferenceYear = (
            <div className="input">
                <label htmlFor="ref-year">Année de référence</label>
                <select
                    id="ref-year"
                    value={goalRefYear}
                    onChange={(e) => setGoalRefYear(e.target.value)}
                >
                    {availableRefYears.map((year) => (
                        <option key={year} value={year}>
                            {year}
                        </option>
                    ))}
                </select>
            </div>
        );
        formLastYear = (
            <div className="input">
                <label htmlFor="last-year">Dernière année de projection</label>
                <select
                    id="last-year"
                    value={goalLastYear}
                    onChange={(e) => setGoalLastYear(e.target.value)}
                >
                    {availableLastYears.map((year) => (
                        <option key={year} value={year}>
                            {year}
                        </option>
                    ))}
                </select>
            </div>
        );
    }

    let formLineColor = "";
    if (goalIndicator) {
        formLineColor = (
            <ColorPicker
                color={goalLineColor}
                label="Couleur de la ligne"
                callback={(color) => setGoalLineColor(color.hex)}
            />
        );
    }

    let traj = "";
    let formResetTrajButton = "";
    if (goalIndicator && availableIndicators.length && goalTrajectory) {
        traj = (
            <Trajectory
                parentApi={props.parentApi}
                referenceYear={parseInt(goalRefYear)}
                lastYear={parseInt(goalLastYear)}
                userTrajectory={goalTrajectory}
                setUserTrajectory={setGoalTrajectory}
                color={goalLineColor}
            />
        );

        formResetTrajButton = (
            <input
                type="button"
                className="tsbtn warning"
                onClick={() =>
                    setGoalTrajectory({ annees_valeurs: {}, annees_modifiees: {} })
                }
                value="Réinitialiser les valeurs"
            />
        );
    }

    let formSubmitButton = "";
    if (goalIndicator) {
        const label =
            props.mode === "mise_a_jour"
                ? "Mettre à jour l’objectif"
                : "Ajouter l’objectif";
        formSubmitButton = (
            <input
                type="submit"
                className="tsbtn info big"
                value={label}
                disabled={!hasChanges}
            />
        );
    }

    const [currentZone, setCurrentZone] = useState({});
    const [allZones, setAllZones] = useState(false);

    function onTerritoryAdd() {
        let zone = {};
        if (allZones) {
            zone = { zoneType: currentZone.zoneType };
        } else {
            zone = { zoneType: currentZone.zoneType, zoneId: currentZone.zoneId };
        }
        if (!zone.zoneType) return;
        if (
            goalZoneAssignations.some(
                ({ zoneType, zoneId }) =>
                    zoneType === zone.zoneType && zoneId === zone.zoneId
            )
        )
            return;
        setGoalAssignations((assignations) => [...assignations, zone]);
    }

    let formTerritorialAssignations = "";
    if (goalIndicator) {
        const deleteButton = (index) => (
            <input
                type="button"
                className="tsbtn danger"
                value="Supprimer"
                onClick={() =>
                    setGoalAssignations((assignations) => {
                        assignations = [...assignations];
                        assignations.splice(index, 1);
                        return assignations;
                    })
                }
            />
        );
        formTerritorialAssignations = (
            <fieldset>
                <legend>Activer l’objectif dans les territoires...</legend>
                <div className="input">
                    <label>Territoire</label>
                    <ZoneSelect
                        parentApi={props.parentApi}
                        title=""
                        disableMaille
                        className="multi-select"
                        onSelect={setCurrentZone}
                    />
                </div>
                <div className="check-input">
                    <input
                        type="checkbox"
                        id="all-zones"
                        checked={allZones}
                        onChange={(e) => setAllZones(e.target.checked)}
                    />
                    <label htmlFor="all-zones">Tous les territoires de ce type</label>
                </div>
                <input
                    type="button"
                    className="tsbtn success"
                    value="Ajouter"
                    onClick={onTerritoryAdd}
                />
                <table>
                    <tbody>
                        {goalZoneAssignations.map(({ zoneType, zoneId }, index) => {
                            const zoneName = zonesManager.getZoneName(zoneId, {
                                zone: zoneType,
                            });
                            zoneType = zonesManager.obtenirLibelleZone(zoneType).trim();
                            let label = zoneId ? `${zoneName} (${zoneType})` : zoneType;
                            return (
                                <tr key={label}>
                                    <td>{label}</td>
                                    <td>{deleteButton(index)}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </fieldset>
        );
    }

    return (
        <div className="corps-tableau-bord">
            <form
                className="tsform"
                style={{ width: "100%", maxWidth: "revert", paddingBottom: 40 }}
                onSubmit={onFormSubmit}
            >
                {formTitle}
                {formDescription}
                {formIndicator}
                {formDetailLevel}
                {formCategoryFilter}
                {formReferenceYear}
                {formLastYear}
                {formLineColor}
                {traj}
                {formResetTrajButton}
                {formTerritorialAssignations}
                {formSubmitButton}
            </form>
            {status?.type === "error" ? (
                <div className="alert alert-warning">{status.message}</div>
            ) : status?.type === "success" ? (
                <div className="alert alert-success">{status.message}</div>
            ) : null}
        </div>
    );
}

/**
 * Component that displays a form input allowing to choose a color. The color is stored
 * outside of current component and selection triggers the callback given.
 *
 * @param {string} color the color selected
 * @param {callable} callback the function to call when changing the color selected
 */
function ColorPicker(props) {
    const [isPickerDisplayed, displayPicker] = useState(false);

    // handle opening and closing the picker
    const handlePickerClick = () => {
        displayPicker(true);
    };
    const handlePickerClose = () => {
        displayPicker(false);
    };

    const title = props.title ?? undefined;

    return (
        <div className="input">
            <label title={title}>{props.label ?? "Sélectionner une couleur"}</label>
            <div style={{ width: "67%" }}>
                <button
                    type="button"
                    className="unstyled color-picker-swatch"
                    onClick={handlePickerClick}
                >
                    <div
                        className="color-picker-color"
                        style={{ background: props.color }}
                    />
                </button>
                {isPickerDisplayed ? (
                    <div className="color-picker-popover">
                        <div
                            className="color-picker-cover"
                            onClick={handlePickerClose}
                        />
                        <SketchPicker color={props.color} onChange={props.callback} />
                    </div>
                ) : null}
            </div>
        </div>
    );
}

function Trajectory({
    referenceYear,
    lastYear,
    userTrajectory,
    setUserTrajectory,
    color,
}) {
    lastYear = lastYear ?? configData.planActionDerniereAnnee;
    const years = useMemo(() => {
        const years = [];
        for (let y = referenceYear; y <= lastYear; y++) {
            years.push(y);
        }
        return years;
    }, [referenceYear, lastYear]);

    const _setUserTrajectory = useCallback(
        (value) =>
            setUserTrajectory((prevState) => {
                if (value instanceof Function) {
                    value = value(prevState);
                }
                let { annees_valeurs, annees_modifiees } = value;
                annees_valeurs = linearInterpolation(annees_valeurs, annees_modifiees);
                return { annees_valeurs, annees_modifiees };
            }),
        [setUserTrajectory]
    );

    // Initialize or adapt the trajectory if years changed
    useEffect(() => {
        const first_year = Math.min(
            ...Object.keys(userTrajectory?.annees_valeurs ?? {})
        );
        if (
            isFinite(first_year) &&
            userTrajectory?.annees_modifiees &&
            userTrajectory.annees_modifiees[first_year] === false
        ) {
            // Always mark the first trajectory value as modified
            _setUserTrajectory({
                annees_valeurs: userTrajectory.annees_valeurs,
                annees_modifiees: {
                    ...userTrajectory.annees_modifiees,
                    [first_year]: true,
                },
            });
        }

        if (
            userTrajectory?.annees_modifiees &&
            Math.min(...Object.keys(userTrajectory.annees_modifiees)) === years[0] &&
            Math.max(...Object.keys(userTrajectory.annees_modifiees)) ===
                years[years.length - 1]
        )
            return;

        const values = {};
        const modified = {};
        for (const year of years) {
            if (year === years[0]) {
                values[year] = 0;
                modified[year] = true;
            } else {
                values[year] = userTrajectory?.annees_valeurs[year]; // can be undefined
                modified[year] = userTrajectory?.annees_modifiees[year] ?? false;
            }
        }
        _setUserTrajectory({
            annees_valeurs: values,
            annees_modifiees: modified,
        });
    }, [userTrajectory, years, _setUserTrajectory]);

    // TABLE CONFIGURATION
    const tableColumns = [
        { columnId: "header", width: 100 },
        ...years.map((year) => ({ columnId: `${year}`, width: 60 })),
    ];
    const tableRows = [
        {
            rowId: "years",
            cells: [
                { type: "header", text: "" },
                ...years.map((year) => ({ type: "header", text: `${year}` })),
            ],
        },
    ];
    const percentFormat = Intl.NumberFormat("fr-FR", { maximumFractionDigits: 2 });
    if (userTrajectory?.annees_valeurs) {
        const { annees_valeurs, annees_modifiees } = userTrajectory;
        tableRows.push({
            rowId: "values",
            cells: [
                { type: "header", text: "Évolution (%)" },
                ...years.map((year, index) => ({
                    type: "text",
                    text: percentFormat.format(parseFloat(annees_valeurs[year])),
                    className: annees_modifiees[year] ? "" : "cell-placeholder",
                    nonEditable: index === 0,
                })),
            ],
        });
    }
    const handleTableChange = (changes) => {
        let values = { ...userTrajectory.annees_valeurs };
        let modified = { ...userTrajectory.annees_modifiees };
        changes.forEach(({ rowId, columnId, newCell }) => {
            if (newCell.text == null || newCell.text === "") {
                modified[columnId] = false;
            } else if (rowId === "values") {
                let value = parseFloat(
                    newCell.text.replace(/\s/, "").replace(",", ".")
                );
                if (isNaN(value)) return;
                values[columnId] = value;
                modified[columnId] = true;
            }
        });

        _setUserTrajectory({
            annees_valeurs: values,
            annees_modifiees: modified,
        });
    };

    // CHART CONFIGURATION
    const chartData = { datasets: [] };

    if (
        userTrajectory?.annees_valeurs &&
        Object.keys(userTrajectory.annees_valeurs).length
    ) {
        const { annees_valeurs, annees_modifiees } = userTrajectory;

        const dataSet = Object.entries(annees_valeurs).map(([year, value]) => ({
            x: new Date(year + "-01-01"),
            y: value,
        }));
        chartData.datasets.push({
            borderColor: color,
            backgroundColor: color,
            pointStyle: "circle",
            label: "Trajectoire cible",
            pointRadius: Object.values(annees_modifiees).map((m) => (m ? 5 : 3)),
            pointHoverRadius: 7,
            borderWidth: 2,
            order: 0,
            data: dataSet,
            labels: Object.keys(annees_valeurs),
        });
    }

    const chartOptions = {
        scales: {
            x: {
                type: "time",
                title: {
                    text: "Années",
                    font: { size: 16 },
                },
                time: { unit: "year" },
                ticks: {
                    minRotation: 65,
                    source: "data",
                },
            },
            y: {
                suggestedMax: 100,
                suggestedMin: -100,
            },
        },
        plugins: {
            legend: {
                position: "bottom",
                labels: {
                    font: { size: 16 },
                    usePointStyle: true,
                },
                reverse: true,
            },
            tooltip: {
                mode: "point",
                reverse: true,
                callbacks: {
                    title: function (data) {
                        return new Date(data[0].label).getFullYear();
                    },
                    label: function (data) {
                        return (
                            data.raw.y.toLocaleString(undefined, {
                                maximumSignificantDigits: 4,
                            }) + " %"
                        );
                    },
                },
            },
        },
        width: 1200,
        height: 600,
    };

    // CHART INTERACTIONS
    const trajectoryGraph = useRef(null);
    useEffect(() => {
        const canvas = trajectoryGraph.current?.ctx?.canvas;
        if (!canvas) {
            return;
        }

        let selectedElement, chart, scale, grabOffsetY, newYPosition;

        function selectDataOnTrajectory(event) {
            if (selectedElement) {
                endMoveDataOnTrajectory();
            }
            let elements = trajectoryGraph.current.getElementsAtEventForMode(
                event,
                "point",
                { intersect: true },
                false
            );
            selectedElement = elements[0];
            if (!selectedElement) return;

            chart = trajectoryGraph.current.config._config;
            chart.options.animation = false;
            scale = trajectoryGraph.current.scales.y;
            // Get pixel y-offset from datapoint to mouse/touch point
            grabOffsetY =
                scale.getPixelForValue(
                    chart.data.datasets[selectedElement.datasetIndex].data[
                        selectedElement.index
                    ].y,
                    selectedElement.index
                ) - event.clientY;
        }

        function moveDataOnTrajectory(event) {
            if (!selectedElement) return;
            const curDataset = chart.data.datasets[selectedElement.datasetIndex];
            const selectedYear = curDataset.data[selectedElement.index].x.getFullYear();

            // On interdit la modification de la première année (valeur de référence)
            if (selectedYear === referenceYear) return;

            /** Lerp value from interval 1 to interval 2 */
            function mapChart(value, start1, stop1, start2, stop2) {
                return (
                    start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1))
                );
            }
            newYPosition = mapChart(
                event.clientY + grabOffsetY,
                scale.chart.chartArea.bottom,
                scale.chart.chartArea.top,
                scale.min,
                scale.max
            );
            curDataset.data[selectedElement.index].y = newYPosition;
            scale.chart.update(0);
        }

        function endMoveDataOnTrajectory() {
            if (newYPosition === undefined) {
                selectedElement = undefined;
                return;
            }

            const curDataset = chart.data.datasets[selectedElement.datasetIndex];
            const selectedYear = curDataset.data[selectedElement.index].x.getFullYear();
            const yPositionCopy = newYPosition; // newYPosition is reset just after this. setUserTrajectory being (kind of) async, the order can vary so we need to store a copy

            _setUserTrajectory(({ annees_valeurs, annees_modifiees }) => ({
                annees_valeurs: { ...annees_valeurs, [selectedYear]: yPositionCopy },
                annees_modifiees: { ...annees_modifiees, [selectedYear]: true },
            }));
            chart.options.animation = true;
            newYPosition = undefined;
            selectedElement = undefined;
        }

        canvas.addEventListener("mousedown", selectDataOnTrajectory);
        canvas.addEventListener("mousemove", moveDataOnTrajectory);
        canvas.addEventListener("mouseup", endMoveDataOnTrajectory);
        canvas.addEventListener("mouseout", endMoveDataOnTrajectory);

        return () => {
            canvas.removeEventListener("mousedown", selectDataOnTrajectory);
            canvas.removeEventListener("mousemove", moveDataOnTrajectory);
            canvas.removeEventListener("mouseup", endMoveDataOnTrajectory);
            canvas.removeEventListener("mouseout", endMoveDataOnTrajectory);
        };
    }, [trajectoryGraph, referenceYear, _setUserTrajectory]);

    // RENDER
    const isLoading = !userTrajectory?.annees_valeurs;
    const isEmpty = Object.keys(userTrajectory?.annees_valeurs ?? {}).length === 0;
    return (
        <div className="trajectory" style={{ width: "100%" }}>
            {isLoading || isEmpty ? (
                <p>Chargement en cours...</p>
            ) : (
                <>
                    <div className="scrollable-grid-container">
                        <ReactGrid
                            rows={tableRows}
                            columns={tableColumns}
                            onCellsChanged={handleTableChange}
                            enableFillHandle
                            enableRangeSelection
                        />
                    </div>
                    <Line
                        ref={trajectoryGraph}
                        hidden={isLoading || isEmpty}
                        data={chartData}
                        options={chartOptions}
                        width={chartOptions.width}
                        height={chartOptions.height}
                    />
                </>
            )}
        </div>
    );
}

/**
 * Interpolate given values depending on a "known" boolean dictionnary.
 *
 * @param {{[key: number]: number}} values Dictionnary of preexisting values depending
 * on the year
 * @param {{[key: number]: boolean}} isKnown Dictionnary containing true for years where
 * values are known and false for years that need to be interpolated
 * @param {boolean} inPlace By default, the function modify the values dict. Set to
 * false to return a copy
 */
function linearInterpolation(values, isKnown, inPlace = true) {
    if (!inPlace) values = { ...values };
    const allYears = Object.keys(isKnown).map((year) => parseInt(year, 10));
    allYears.sort((a, b) => a - b); // Shouldn't be necessary in our use case
    const knownYears = allYears.filter((year) => isKnown[year]);
    if (knownYears.length === 0) {
        throw Error(
            `isKnown should have at least one true value. Got ${JSON.stringify(
                isKnown
            )}`
        );
    }
    for (const year of allYears) {
        if (isKnown[year]) continue;

        // Find where in the known data, the values to interpolate would be inserted
        const prevKnownYear = [...knownYears].reverse().find((y) => y < year);
        const nextKnownYear = knownYears.find((y) => y >= year);

        // Special cases outside the known region
        if (prevKnownYear === undefined) {
            values[year] = values[nextKnownYear];
            continue;
        }
        if (nextKnownYear === undefined) {
            values[year] = values[prevKnownYear];
            continue;
        }

        // Calculate the slope of the interval in which the current year is
        const prevKnownVal = values[prevKnownYear];
        const nextKnownVal = values[nextKnownYear];
        const slope = (nextKnownVal - prevKnownVal) / (nextKnownYear - prevKnownYear);

        // Calculate the actual value at the current year
        values[year] = slope * (year - prevKnownYear) + prevKnownVal;
    }
    return values;
}

export default SupraGoalsAdd;
