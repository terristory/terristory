/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useCallback, useEffect, useState } from "react";
import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";

import "bootstrap/dist/css/bootstrap.min.css";
import styles from "./AdminStyles.module.css";

/**
 * This component is used to manage the equipment in the database
 */
function POIManage(props) {
    const [poiList, setPOIList] = useState([]);
    const [themes, setThemes] = useState([]);

    const region = props.forcedRegion ?? props.parentApi.data.region;

    const fetchConfiguration = useCallback(() => {
        // Retreive poi list from DB
        Api.callApi(buildRegionUrl(config.api_poi_layers_url, region))
            .then((json) => {
                const themes = json.map((table) => table.theme);
                setPOIList(
                    json.map((layer) => ({
                        ...layer,
                        creditsDataSources: layer.credits_data_sources?.length
                            ? JSON.parse(layer.credits_data_sources)
                            : [],
                        creditsDataProducers: layer.credits_data_producers?.length
                            ? JSON.parse(layer.credits_data_producers)
                            : [],
                        structure_constraints: layer.structure_constraints || {},
                        afficherStatut: layer.statut,
                        exportable: layer.donnees_exportables,
                        typeGeom: layer.type_geom,
                        ancrageIcone: layer.ancrage_icone,
                    }))
                );
                setThemes([...new Set(themes)]);
            })
            .catch((e) => {
                console.log("Problème de téléchargement des données d'équipements");
            });
    }, [region]);

    useEffect(() => {
        fetchConfiguration();
    }, [fetchConfiguration]);

    if (!props.connected || !props.userInfos || props.userInfos?.profil !== "admin") {
        return <div>Non accessible.</div>;
    }

    /**
     *
     * Callback to update an equipment layer
     * @param  {json} equipement : current row
     */
    const updatePOI = (poi) => {
        // We switch to the creation tab, but in update mode
        props.moveToEditPOITab(poi); // 1 represents the edit tab (0 the data management and 1 the list of equipment layers)
    };

    /**
     *
     * Callback to delete an equipment layer
     * @param  {json} equipement : current row
     */
    const deletePOI = (equipements) => {
        let r = window.confirm(
            "Attention, cette opération n'est pas réversible. Voulez-vous supprimer cette couche d'équipements " +
                equipements.nom +
                " ?"
        );
        if (r === true) {
            let url = buildRegionUrl(config.api_poi_layer_url, region).replace(
                "#layer#",
                equipements.id
            );
            Api.callApi(url, null, "DELETE").then((response) => {
                // After the update, it is necessary to refresh props.parentApi.data.poiLayers
                fetchConfiguration();
            });
        }
    };

    /**
     * Makes a layer editable
     * @param  {str} layer : the layer concerned
     * @param  {str} state : new state
     */
    const setEditable = (layer, state) => {
        let url = buildRegionUrl(config.api_poi_layer_rights_url, region).replace(
            "#layer#",
            layer
        );
        const body = JSON.stringify({ droit: state });
        Api.callApi(url, body, "PUT").then((response) => {
            // After the update, it is necessary to refresh props.parentApi.data.poiLayers
            fetchConfiguration();
        });
    };

    /**
     * Import a layer in the regional schema
     * @param  {str} layerId : the layer concerned
     * @param  {str} state : new state
     */
    const importAnalysis = (layerId) => {
        let url = buildRegionUrl(
            config.api_poi_layer_import,
            props.parentApi.data.region
        ).replace("#layerId#", layerId);
        Api.callApi(url, null, "PUT")
            .then(() => {
                props.parentApi.callbacks.updateMessages(
                    "La couche a bien été importée dans le schéma régional !",
                    "success"
                );
            })
            .catch((e) => {
                if (
                    e.status === 403 &&
                    e.message ===
                        "This operation should not be performed but can be forced."
                ) {
                    let r = window.confirm(
                        "La table de données ou la couche de POI existe déjà dans le schéma local. Voulez-vous forcer l'import ?"
                    );
                    if (r === true) {
                        Api.callApi(url + "?forceUpdate=true", null, "PUT").then(() => {
                            props.parentApi.callbacks.updateMessages(
                                "La couche a bien été importée dans le schéma régional en écrasant les anciennes données !",
                                "success"
                            );
                        });
                    }
                } else {
                    props.parentApi.callbacks.updateMessages(
                        "L'import a échoué : " + e.message,
                        "danger"
                    );
                }
            });
    };

    const showDeletionButton = region !== "france" || props.userInfos?.globalAdmin;

    return (
        <div>
            <div className="panel-body block-row">
                <h3 className="panel-title pull-left">
                    Gestion des couches équipements
                </h3>

                {themes.map((theme) => {
                    return (
                        <div key={`poi-manage-theme-${theme}`}>
                            <h4>{theme}</h4>
                            <div>
                                <ul>
                                    {poiList.map((poiLayer) => {
                                        if (poiLayer.theme !== theme) return "";

                                        return (
                                            <li
                                                key={poiLayer.nom}
                                                className={styles.poiButtons}
                                                aria-label={poiLayer.label}
                                            >
                                                <strong>{poiLayer.label}</strong> (nom
                                                de table : <em>{poiLayer.nom}</em>)
                                                <br />
                                                <button
                                                    type="button"
                                                    className={`btn btn-${
                                                        poiLayer.modifiable
                                                            ? "primary"
                                                            : "warning"
                                                    }`}
                                                    onClick={() =>
                                                        setEditable(
                                                            poiLayer.nom,
                                                            !poiLayer.modifiable
                                                        )
                                                    }
                                                >
                                                    Rendre{" "}
                                                    {poiLayer.modifiable && "non"}{" "}
                                                    modifiable
                                                </button>
                                                {region === "france" && (
                                                    <button
                                                        className="btn btn-primary"
                                                        onClick={() =>
                                                            importAnalysis(poiLayer.id)
                                                        }
                                                    >
                                                        Importer dans mon schéma
                                                    </button>
                                                )}
                                                <button
                                                    className="btn btn-success"
                                                    onClick={() => updatePOI(poiLayer)}
                                                >
                                                    Mettre à jour
                                                </button>
                                                {showDeletionButton && (
                                                    <button
                                                        className="btn btn-danger"
                                                        onClick={() =>
                                                            deletePOI(poiLayer)
                                                        }
                                                    >
                                                        Supprimer
                                                    </button>
                                                )}
                                            </li>
                                        );
                                    })}
                                </ul>
                            </div>
                        </div>
                    );
                })}
                {poiList.length === 0 && (
                    <p>Aucune couche de POI enregistrée pour l'instant.</p>
                )}
            </div>
        </div>
    );
}

export default POIManage;
