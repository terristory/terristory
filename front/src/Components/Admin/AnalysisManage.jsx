/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import TableContainer from "../Utils/TableContainer";

import Api from "../../Controllers/Api";

import config from "../../settings";
import { buildRegionUrl } from "../../utils";

import "bootstrap/dist/css/bootstrap.min.css";

import styles from "../../style/admin/analyses.module.css";

/**
 * This component is used to manage existing analysis
 */
class AnalysisManage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            analysis: [],
            statusError: false,
            status: "",
        };
        this.region = this.props.forcedRegion ?? this.props.parentApi.data.region;
        this.getAnalysis();
    }

    // Retrieve analysis
    getAnalysis() {
        // Call API to Retrieve analysis
        let url = buildRegionUrl(config.api_analysis_meta_url, this.region);
        Api.callApi(url, null, "GET")
            .then((response) => {
                this.setState({
                    analysis: response,
                });
            })
            .catch((e) => this.setState({ analysis: undefined }));
    }

    /**
     * getCellProps is used to color the rows of the table according to criteria
     * @param  {json} state : current row state
     * @param  {json} rowInfo : current row info (columns, values)
     * @param  {json} instance : array instance
     * @return {json} the style for the current row
     */
    getCellProps(cell) {
        if (cell) {
            return {
                background: cell.row.original.active ? "#f3f9e6" : "#fdf1da",
            };
        }
        return {};
    }

    /**
     * @return {dom} the rendering of the component
     */
    render() {
        if (
            !this.props.connected ||
            !this.props.userInfos ||
            this.props.userInfos?.profil !== "admin"
        ) {
            return <div>Non accessible.</div>;
        }

        const showDeletionButton =
            this.region !== "france" || this.props.userInfos?.globalAdmin;

        /**
         * Callback for the activation / deactivation of the current analysis
         * @param  {json} analysis : current row
         */
        const activateAnalysis = (analysis) => {
            const baseUrl = analysis.props.row.original.active
                ? config.api_analysis_deactivate_url
                : config.api_analysis_activate_url;
            let url = buildRegionUrl(baseUrl, this.region).replace(
                "#id#",
                analysis.props.row.original.id
            );

            Api.callApi(url, null, "GET")
                .then((response) => {
                    // Reload analysis list
                    this.setState({
                        statusError: false,
                        status: analysis.props.row.original.active
                            ? "Désactivation réussie !"
                            : "Activation réussie !",
                    });
                    if (this.props.userInfos.profil === "admin") {
                        this.getAnalysis();
                    }
                })
                .catch((e) => this.setState({ status: e.message, statusError: true }));
        };

        /**
         * Callback to import an analysis in current regional schema
         * @param  {json} analysis : current row
         * @param  {boolean} force : force the update of an indicator
         */
        const importAnalysis = (analysis, force = false) => {
            let url = buildRegionUrl(
                config.api_import_common_indicator.replace(
                    "#analysis_id#",
                    analysis.props.row.original.id
                ),
                this.props.parentApi.data.region
            );
            Api.callApi(url + `?forceUpdate=${force}`, null, "PUT")
                .then(() => {
                    this.props.parentApi.callbacks.updateMessages(
                        "L'indicateur a bien été importé dans le schéma régional !",
                        "success"
                    );
                })
                .catch((e) => {
                    if (
                        e.status === 403 &&
                        e.message.startsWith(
                            "This operation should not be performed but can be forced."
                        )
                    ) {
                        const details = e.message.split(". Details: ")?.[1];
                        let r = window.confirm(
                            `La table de données ou l'indicateur existent déjà dans le schéma régional, impossible d'importer l'indicateur sans forcer la mise à jour${
                                details && " (" + details + ")"
                            }. Voulez-vous forcer l'import ?`
                        );
                        if (r === true) {
                            Api.callApi(url + `?forceUpdate=true`, null, "PUT").then(
                                () => {
                                    this.props.parentApi.callbacks.updateMessages(
                                        "L'indicateur a bien été importé dans le schéma régional en écrasant les anciennes données !",
                                        "success"
                                    );
                                }
                            );
                        }
                    } else {
                        this.props.parentApi.callbacks.updateMessages(
                            "L'import a échoué : " + e.message,
                            "danger"
                        );
                    }
                });
        };

        /**
         * Callback for the deletion of the current analysis
         * @param  {json} analysis : current row
         */
        const deleteAnalysis = (analysis) => {
            const newLine = "\r\n";
            let url = buildRegionUrl(
                config.api_analysis_url + analysis.props.row.original.id,
                this.region
            );

            Api.callApi(url, null, "DELETE")
                .then((response) => {
                    // Reload analysis list
                    const dashboardsLinked =
                        response["related_dashboards"] === 0
                            ? ""
                            : newLine +
                              " - " +
                              response["related_dashboards"] +
                              " tableau(x) de bord lié(s).";
                    let r = window.confirm(
                        "Confirmez-vous la suppression de l'indicateur " +
                            analysis.props.row.original.nom +
                            " ?" +
                            dashboardsLinked
                    );
                    if (r === true) {
                        url += "?confirm=true";
                        Api.callApi(url, null, "DELETE").then((response) => {
                            this.setState({
                                statusError: false,
                                status: "Suppression réussie !",
                            });
                            this.getAnalysis();
                        });
                    }
                })
                .catch((e) => {
                    this.setState({ status: e.message, statusError: true });
                });
        };

        /**
         * Callback to update an indicator
         * @param  {json} analysis : current row
         */
        const updateAnalysis = (analysis) => {
            // We switch to the creation tab, but in edit mode
            this.props.moveToEditIndicatorTab(analysis.props.row.original);
        };

        const columns = [
            {
                header: "Id",
                accessorKey: "id",
                enableColumnFilter: false,
            },
            {
                header: "Thème",
                accessorKey: "ui_theme",
            },
            {
                header: "Nom",
                accessorKey: "nom",
            },
            {
                header: "Active",
                accessorKey: "active",
                enableColumnFilter: false,
            },
            {
                id: "activate",
                header: "Action",
                accessorKey: "actif",
                cell: (props) => (
                    <div className={styles.AnalysisActions}>
                        {this.region === "france" && (
                            <button
                                className={"btn btn-primary"}
                                onClick={() => importAnalysis({ props }, false)}
                            >
                                Importer dans mon schéma
                            </button>
                        )}
                        <button
                            className={
                                props.row.original.active
                                    ? "btn btn-warning"
                                    : "btn btn-success"
                            }
                            onClick={() => activateAnalysis({ props })}
                        >
                            {props.row.original.active ? "Désactiver" : "Activer"}
                        </button>
                        <button
                            className="btn btn-success"
                            onClick={() => updateAnalysis({ props })}
                        >
                            Mettre à jour
                        </button>
                        {showDeletionButton && (
                            <button
                                className={"btn btn-danger"}
                                onClick={() => deleteAnalysis({ props }, false)}
                            >
                                Supprimer
                            </button>
                        )}
                    </div>
                ),
            },
        ];

        const alertBox = this.state.statusError
            ? "alert alert-danger"
            : "alert alert-success";
        return (
            <div>
                <div className="panel-body user-scenarii">
                    <h3 className="panel-title pull-left">
                        Gestion des indicateurs
                        {this.region === "france" && " du socle commun"}
                    </h3>

                    {this.state.status && (
                        <div className={alertBox}>{this.state.status}</div>
                    )}
                    <TableContainer
                        data={this.state.analysis}
                        columns={columns}
                        tableClassName="table-striped"
                        getCellProps={this.getCellProps}
                        defaultPageSize={30}
                        filterable={true}
                    />
                </div>
            </div>
        );
    }
}

export default AnalysisManage;
