/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import Api from "../../Controllers/Api";

import "bootstrap/dist/css/bootstrap.min.css";
import React, { useCallback, useEffect, useState } from "react";
import Select from "react-select";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";

import AddCategoryForm from "./AddCategoryForm";

/**
 * This component is used in the AnalysisAjouter.js page to handle the selection of the categories to use for the indicator.
 *
 * For each selected category, is specified :
 *      - the title (displayed above the category selection input),
 *      - the order in which to display the categories
 *      - the type of input/chart to use for the category.
 *
 * Some types are charts with category filters as the input (pie chart, bar chart, radar, ). These charts will appear in the bottom section of the map page.
 *
 * Other types are types of select inputs (toggle, dropdown, slider...). These inputs will appear in the side bar of the map page.
 *
 * @param {Object} props - The props object.
 * @param {str} props.region -
 * @param {import("./AnalysisAdd.jsx").Category[]} [props.selectedCategories=[]] - The initially selected categories. See Category typedef in AnalysisAjouter.js
 * @param {Function} props.onChange - Callback function triggered when selected categories change.
 *   It is called with two arguments:
 *   - `updatedCategories` {Category[]} - The updated list of selected categories.
 *   - `isValid` {boolean} - A boolean indicating whether the updated categories are valid
 *
 * @returns {JSX.Element}
 */
function IndicatorCategorySelector({ region, selectedCategories = [], onChange }) {
    const INPUT_TYPE_OPTIONS = [
        { value: "pie", label: "Diagramme circulaire" },
        { value: "histogramme", label: "Diagramme à barres" },
        { value: "hbar", label: "Diagramme à barres horizontales" },
        { value: "history", label: "Courbe historique" },
        { value: "line", label: "Courbe empilée" },
        { value: "radar", label: "Diagramme radar" },
        { value: "switch-button", label: "Commutateur" },
        { value: "selection", label: "Menu déroulant" },
        { value: "slider", label: "Slider" },
    ];

    // The options for the multi select
    const [categoryOptions, setCategoryOptions] = useState([]);

    // is Loading the category options
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    const [showCategoryAddForm, setShowCategoryAddForm] = useState(false);

    const fetchCategories = useCallback(() => {
        setIsLoading(true);
        setIsError(false);
        setCategoryOptions([]);
        Api.callApi(buildRegionUrl(config.api_categories_list_url, region))
            .then((response) => {
                setCategoryOptions(
                    Object.keys(response).map((key) => {
                        return { value: key, label: key };
                    })
                );
            })
            .catch(() => {
                setIsError(true);
            })
            .finally(() => setIsLoading(false));
    }, [region]);

    // Fetch category options on load
    useEffect(() => {
        fetchCategories();
    }, [fetchCategories, region]);

    // checks if a category and its parameters are valid : title required, type in INPUT_TYPE_OPTIONS.
    const isValid = (categories) => {
        for (const category of categories) {
            if (!category.title) return false;
            if (!INPUT_TYPE_OPTIONS.map((i) => i.value).includes(category.type)) {
                return false;
            }
        }
        return true;
    };

    // When categories are selected, extend them with additional properties: name, type, and order.
    const handleCategorySelect = (selectedOptions) => {
        const updatedCategories = selectedOptions.map((option) => {
            const existingCategory = selectedCategories.find(
                (cat) => cat.value === option.value
            );

            if (existingCategory) {
                // If the category is already selected, return it unchanged
                return existingCategory;
            } else {
                // If it's a new selection, create a new category with default values
                return {
                    ...option,
                    category: option.value,
                    title: option.label,
                    type: INPUT_TYPE_OPTIONS[0].value,
                    description: "",
                    is_single_select: false,
                    order: selectedCategories.length + 1,
                };
            }
        });

        // Notify parent of change
        onChange(updatedCategories, isValid(updatedCategories));
    };

    // Handle the input change for name, type, and order
    const handleInputChange = (index, field, value) => {
        const updatedCategories = [...selectedCategories];
        updatedCategories[index][field] = value;
        onChange(updatedCategories, isValid(updatedCategories));
    };

    // Creates the form that is displayed for each selected category
    const selectedCategoryDetailForm = (category, index) => {
        return (
            <div key={category.value} className="card mb-3">
                <div className="card-body">
                    <h5>
                        Catégorie : <span className="fw-bold">{category.label}</span>
                    </h5>
                    {/* Input Type Field */}
                    <div className="row mb-3">
                        <div className="col-md-4">
                            <label
                                className="form-label"
                                htmlFor={`type-${category.value}`}
                                title={`Type de graphique ou sélecteur pour la catégorie ${category.value}. Les menus déroulants et les commutateurs seront affichés dans la légende tandis que les autres graphiques seront intégrés en bas de la carte. L'ordre des éléments dans les différents graphiques (diagrammes, menu déroulant, commutateur) est déterminé par la colonne ordre dans la définition des catégories dans l'onglet correspondant.`}
                                required
                            >
                                Graphique ou sélecteur à utiliser
                            </label>
                            <select
                                id={`type-${category.value}`}
                                className="form-select"
                                value={category.type}
                                onChange={(e) =>
                                    handleInputChange(index, "type", e.target.value)
                                }
                            >
                                {INPUT_TYPE_OPTIONS.map((type) => (
                                    <option key={type.value} value={type.value}>
                                        {type.label}
                                    </option>
                                ))}
                            </select>
                        </div>
                        {/* Title Field */}
                        <div className="col-md-8">
                            <label
                                className="form-label"
                                htmlFor={`title-${category.value}`}
                                title={`Titre apposé au graphique ou au sélecteur pour la catégorie ${category.value}`}
                                required
                            >
                                Titre
                            </label>
                            <input
                                type="text"
                                id={`title-${category.value}`}
                                className="form-control"
                                value={category.title}
                                onChange={(e) =>
                                    handleInputChange(index, "title", e.target.value)
                                }
                            />
                        </div>
                    </div>
                    {/* Description Field */}
                    <div className="mb-3">
                        <label
                            className="form-label"
                            htmlFor={`description-${category.value}`}
                            title={`Texte affiché au passage de la souris sur le label de catégorie pour fournir des explications complémentaires. Pas encore implémenté pour les graphiques.`}
                        >
                            Description
                        </label>
                        <input
                            type="text"
                            id={`description-${category.value}`}
                            className="form-control"
                            value={category.description}
                            onChange={(e) =>
                                handleInputChange(index, "description", e.target.value)
                            }
                        />
                    </div>
                    <div className="row mb-3">
                        {/* Mutually Exclusive Selection Field */}
                        <div className="col-md-6">
                            <label
                                className="form-label"
                                htmlFor={`single-select-${category.value}`}
                                title={`Cocher si les modalités de cette catégorie doivent être sélectionnées de manière mutuellement exclusive (par exemple climat réel et climat normal ne doivent pas être affichés simultanément). Cela désactivera cette catégorie dans les tableaux de bord pour les graphiques avec une sélection de catégorie.`}
                            >
                                Sélection des modalités mutuellement exclusive
                            </label>
                            <input
                                type="checkbox"
                                id={`single-select-${category.value}`}
                                className="form-check"
                                checked={category.is_single_select}
                                onChange={(e) =>
                                    handleInputChange(
                                        index,
                                        "is_single_select",
                                        e.target.checked
                                    )
                                }
                            />
                        </div>

                        {/* Order Field */}
                        <div className="col-md-6">
                            <label
                                className="form-label"
                                htmlFor={`order-${category.value}`}
                                title="Ordre d'affichage de la catégorie parmi les catégories activées pour cet indicateur"
                            >
                                Ordre
                            </label>
                            <input
                                type="number"
                                id={`order-${category.value}`}
                                className="form-control"
                                value={category.order}
                                onChange={(e) =>
                                    handleInputChange(
                                        index,
                                        "order",
                                        parseInt(e.target.value, 10)
                                    )
                                }
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    };

    return (
        <div>
            <h4>Sélectionnez les catégories associées à cet indicateur </h4>
            <div className="mb-3">
                <label htmlFor="indicator-category" className="form-label">
                    Catégories associées à l'indicateur
                </label>
                <Select
                    id="indicator-category"
                    options={categoryOptions}
                    value={selectedCategories}
                    onChange={handleCategorySelect}
                    isMulti
                    aria-describedby="indicator-category-help"
                    isLoading={isLoading}
                />
                {isError && (
                    <div className="text-danger">
                        Une erreur est survenue lors du chargement des catégories.
                    </div>
                )}
                <div id="indicator-category-help" className="form-text">
                    Sélectionnez les catégories à associer à l'indicateur ainsi que pour
                    chaque, le graphique ou selecteur à utiliser.
                </div>
            </div>
            {/* Input fields for modifying name, type, and order for each selected category */}
            {selectedCategories.map((category, index) =>
                selectedCategoryDetailForm(category, index)
            )}
            <p>
                Si votre indicateur requiert une catégorie non encore présente dans la
                base Terristory, vous pouvez l'y ajouter :
            </p>
            <button
                className="btn btn-secondary mb-3"
                onClick={() => {
                    setShowCategoryAddForm(!showCategoryAddForm);
                }}
            >
                Créer une nouvelle catégorie
                {showCategoryAddForm ? (
                    <i className="bi bi-x-circle-fill ms-2"></i>
                ) : (
                    <i className="bi bi-plus-circle-fill ms-2"></i>
                )}
            </button>
            {showCategoryAddForm && (
                <AddCategoryForm
                    region={region}
                    onAdd={() => {
                        fetchCategories();
                    }}
                ></AddCategoryForm>
            )}
        </div>
    );
}

export default IndicatorCategorySelector;
