/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useEffect, useState } from "react";
import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import "bootstrap/dist/css/bootstrap.min.css";
import TableContainer from "../Utils/TableContainer";

/**
 * This component displays a table from which you can delete an objective or access the update interface of an objective
 * (which is the same as that of adding an objective but with the values pre-filled with what has previously saved in the database).
 */
function SupraGoalsManage(props) {
    const [status, setStatus] = useState("");
    const [data, setData] = useState(undefined);

    /**
     * Get the characteristics of the objectives (title, reference year, graph)
     */
    function obtenirObjectifs() {
        let url = buildRegionUrl(
            config.api_integration_objectifs,
            props.parentApi.data.region
        );
        Api.callApi(url, null, "GET").then((supraGoals) => {
            supraGoals.forEach((goal) => {
                goal.annees = `${goal.annee_reference} à ${goal.derniere_annee_projection}`;
                goal.niveau_detail = goal.filter?.length
                    ? `Par ${goal.filter[0].split(".")[0]}`
                    : "Trajectoire globale";
            });
            setData(supraGoals);
        });
    }
    useEffect(obtenirObjectifs, [props.parentApi.data.region]);

    /**
     * Delete an objective
     * @param {objet clé => valeur} propsTableau : object to access the values displayed in the columns of the table
     */
    function supprimerObjectif(propsTableau) {
        let supra_goal_id = propsTableau.row.original.supra_goal_id;
        let url = buildRegionUrl(
            config.api_objectifs,
            props.parentApi.data.region
        ).replace("#supra_goal_id#", supra_goal_id);
        Api.callApi(url, null, "DELETE").then((response) => {
            setStatus(response.message);
            obtenirObjectifs();
        });
    }

    function mettreAJourObjectif(propsTableau) {
        let supra_goal_id = propsTableau.row.original.supra_goal_id;
        props.modeAjoutMiseAJourObjectifs("mise_a_jour", supra_goal_id);
    }

    if (!props.connected || !props.userInfos || props.userInfos?.profil !== "admin") {
        return <div>Non accessible.</div>;
    }

    const columns = [
        {
            header: "Titre",
            accessorKey: "titre",
        },
        {
            header: "Années",
            accessorKey: "annees",
        },
        {
            header: "Indicateur",
            accessorKey: "graphique",
        },
        {
            header: "Niveau de détail",
            accessorKey: "niveau_detail",
        },
        {
            header: "Actions",
            /**
             * Browses the data transmitted to the ReactTable component and builds the lines in which
             * we integrate the delete button and the one that allows access to the objective update interface.
             * @param {key => value} props: Property of each of these lines. They allow access to the data
             * that is transmitted to the ReactTable component using the accessors defined just above (in this columns variable).
             */
            cell: (props) => (
                <div className="actions">
                    <button
                        className={"btn btn-danger"}
                        onClick={() => supprimerObjectif(props)}
                    >
                        Supprimer
                    </button>
                    <button
                        className="btn btn-warning"
                        onClick={() => mettreAJourObjectif(props)}
                    >
                        Mettre à jour
                    </button>
                </div>
            ),
        },
    ];

    return (
        <div className="panel-body user-scenarii">
            <h3 className="panel-title pull-left">
                Gestion des objectifs supra-territoriaux
            </h3>
            {status && <div className="alert alert-success">{status}</div>}
            {/* Data contained in this.state.donnees are automatically browsed with the method associated with the Cell key in columns */}
            <TableContainer
                data={data}
                columns={columns}
                tableClassName="table-striped"
                defaultPageSize={30}
            />
        </div>
    );
}

export default SupraGoalsManage;
