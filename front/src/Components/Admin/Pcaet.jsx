/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl, exportToCSV } from "../../utils";

/**
 * This admin page manages PCAET data and updates them from ADEME's files.
 */
function Pcaet({ connected, userInfos, parentApi }) {
    if (!connected || !userInfos || userInfos.profil !== "admin") {
        return <div>Non accessible.</div>;
    }

    return (
        <div>
            <h3>Gestion des PCAET</h3>
            <div className="block-etape" style={{ marginBottom: "32px" }}>
                <h4>Récupération des PCAET</h4>
                <UpdatePcaetFromAdeme region={parentApi.data.region} />
            </div>
            <UpdateSirenAssignment region={parentApi.data.region} />
            <div className="block-etape">
                <h4>Liste des PCAET</h4>
                <Link
                    to="/agregation_pcaet"
                    onClick={() =>
                        parentApi.callbacks.updateZoneType(
                            { zone: "region", maille: "epci" },
                            parentApi.data.regionCode
                        )
                    }
                >
                    C’est par ici !
                </Link>
            </div>
        </div>
    );
}

function UpdatePcaetFromAdeme({ region, onSuccess }) {
    const [pcaetUpdateMessage, setPcaetUpdateMessage] = useState("");

    const loadingMessage = "Mise à jour en cours... ceci peut prendre quelques minutes";
    const errorMessage =
        "Erreur lors du chargement de données. Réessayez ultérieurement.\nSi l'erreur persiste, contactez AURA-EE.";

    /** Call the API to update PCAET data from ADEME */
    function run_update_pcaet() {
        setPcaetUpdateMessage(loadingMessage);
        let url = buildRegionUrl(config.mise_a_jour_des_pcaet_ademe, region);
        Api.callApi(url)
            .then(() => {
                setPcaetUpdateMessage("Mise à jour réussie !");
                if (onSuccess) onSuccess();
            })
            .catch((e) => {
                console.error(e);
                setPcaetUpdateMessage(errorMessage);
                alert(errorMessage);
            });
    }

    return (
        <>
            <p>
                Le bouton ci-dessous permet de mettre à jour les données de PCAET dans
                toutes les régions de TerriSTORY®. Il récupère les tableaux du portail
                open data de l’ADEME :{" "}
                <a href="https://data.ademe.fr/">data.ademe.fr</a>. Il change ensuite le
                format des données pour mieux convenir à un format "base de données", et
                calcule les valeurs selon la méthodologie relative aux données
                observatoire.
            </p>
            <p>
                La table telle qu’elle est en base de données est disponible à cette
                adresse : <a href={config.api_raw_pcaet}>{config.api_raw_pcaet}</a>
            </p>
            <button
                className="btn btn-primary"
                disabled={pcaetUpdateMessage === loadingMessage}
                onClick={() => run_update_pcaet()}
            >
                Mettre à jour les PCAET
            </button>
            {/* display or not the loading/success/error message */}
            {pcaetUpdateMessage && <p>{pcaetUpdateMessage}</p>}
        </>
    );
}

function UpdateSirenAssignment({ region, onSuccess }) {
    const [sirenData, setSirenData] = useState([]);
    const [updateSwitch, forceUpdate] = useState(false);

    useEffect(() => {
        const url = buildRegionUrl(config.api_siren_assignment, region);
        Api.callApi(url).then(setSirenData);
    }, [region, updateSwitch]);

    const updateSirenAssignment = (e) => {
        e.preventDefault();

        const file = e.target.elements.file.files[0];
        let formData = new FormData();
        formData.append("fichier", file);

        const url = buildRegionUrl(config.api_siren_assignment, region);
        Api.callApi(url, formData, "PUT", "default")
            .then(() => {
                forceUpdate(!updateSwitch);
                if (onSuccess) onSuccess();
            })
            .catch((error) => {
                alert(error);
            });
    };

    return (
        <div className="block-etape">
            <h4>Voir ou modifier les numéro de SIREN</h4>
            <p>
                Cette section permet d’afficher les PCAET des zones non-EPCI. Pour cela,
                on a besoin de lier le numéro de SIREN tel que rempli sur la plateforme
                Territoires&amp;Climat aux identifiants de zone de TerriSTORY®.
            </p>
            <p>
                Le fichier doit être un CSV séparé par des points-virgules et avec une
                ligne d’entête.
            </p>
            <p>Colonnes nécessaires dans la table :</p>
            <ul>
                <li>
                    <code>zone_type</code> : Échelle territoriale, telle qu’elle
                    apparait dans les URL de TerriSTORY® (terristory.fr/?zone=
                    <b>scot</b>)
                </li>
                <li>
                    <code>zone_id</code> : Identifiant du territoire, tel qu’il apparait
                    dans les URL de TerriSTORY® (terristory.fr/?zone_id=<b>12345</b>)
                </li>
                <li>
                    <code>siren</code> : Code SIREN du territoire, tel qu’il a été saisi
                    sur la plateforme Territoires&amp;Climat
                </li>
                <li>
                    <code>epcis</code> : Optionnel, liste des identifiants des EPCI
                    composant le territoire, au format{" "}
                    <code>["2XXXXXXXX","2XXXXXXXX"]</code> (entre crochets, séparés par
                    des virgules et délimité par des guillemets). Cette information ne
                    sert qu’à l’agrégation et la cartographie des PCAET.
                </li>
            </ul>
            <p>
                Le tableau suivant représente le contenu actuel de la table de
                correspondance entre ces codes SIREN et les codes territoriaux.
            </p>
            <table className="table_preview_header">
                <tbody>
                    <tr>
                        <th>zone_type</th>
                        <th>zone_id</th>
                        <th>siren</th>
                        <th>epcis</th>
                    </tr>
                    {sirenData.length ? (
                        sirenData.map((row) => (
                            <tr key={row.zone_id}>
                                <td>{row.zone_type}</td>
                                <td>{row.zone_id}</td>
                                <td>{row.siren}</td>
                                <td>{JSON.stringify(row.epcis)}</td>
                            </tr>
                        ))
                    ) : (
                        <tr style={{ color: "grey" }}>
                            <td colSpan={4}>[Table vide]</td>
                        </tr>
                    )}
                </tbody>
            </table>
            <button
                className="btn btn-info"
                onClick={() => exportToCSV(sirenData, "csv")}
                style={{ margin: "4px" }}
            >
                Exporter en CSV
            </button>
            <form onSubmit={updateSirenAssignment}>
                <input type="file" name="file" required />
                <input
                    className="btn btn-success"
                    type="submit"
                    value="Mettre à jour"
                />
            </form>
            <br />
        </div>
    );
}

export default Pcaet;
