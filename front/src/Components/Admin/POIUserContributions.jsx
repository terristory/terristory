/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import TableContainer from "../Utils/TableContainer";

import Api from "../../Controllers/Api";
import { buildRegionUrl } from "../../utils";

import config from "../../settings";

import "bootstrap/dist/css/bootstrap.min.css";
import styles from "./AdminStyles.module.css";

function displayLinkCoordinates(coordinates) {
    if (coordinates === undefined || !coordinates) {
        return "";
    }
    let coords = coordinates.split(" ");
    return (
        <a
            target="_blank"
            rel="noreferrer"
            className="link_coordinates"
            href={
                "https://www.openstreetmap.org/#map=14/" + coords[1] + "/" + coords[0]
            }
        >
            {Math.round(coords[1] * 1000) / 1000}, {Math.round(coords[0] * 1000) / 1000}
        </a>
    );
}

/**
 * This component allows you to manage the history of modifications on data layers
 */
class POIUserContributions extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: undefined,
            contributions: [],
        };
    }
    componentDidMount() {
        this.getContributionsList();
    }

    // Get contributions of modifications on layers
    getContributionsList() {
        // Call api to get contributions
        Api.callApi(
            buildRegionUrl(
                config.api_poi_contributions_admin_url,
                this.props.parentApi.data.region
            ),
            undefined,
            "GET"
        )
            .then((response) => {
                response = response.map((r) => ({
                    ...r,
                    mise_a_jour: new Date(r.mise_a_jour),
                }));
                // Store user list
                this.setState({
                    contributions: response,
                });
            })
            .catch((e) => this.setState({ status: e.message }));
    }

    // Handle a single contribution
    handleContribution(edit_id, action) {
        if (action !== "accepted" && action !== "refused") {
            return;
        }
        Api.callApi(
            buildRegionUrl(
                config.api_poi_contribution_manage_url,
                this.props.parentApi.data.region
            )
                .replace("#edit_id#", edit_id)
                .replace("#action#", action),
            undefined,
            "POST"
        )
            .then((response) => {
                this.props.parentApi.callbacks.updateMessages(
                    response.message,
                    "success"
                );
                this.getContributionsList();
            })
            .catch((e) =>
                this.props.parentApi.callbacks.updateMessages(e.message, "danger")
            );
    }

    /**
     * getTrProps is used to color the rows of the table according to criteria
     * @param  {json} state state of the current row
     * @param  {json} rowInfo current row info (columns, values)
     * @param  {json} instance array instance
     * @return {json} the style definition for the current row
     */
    getCellProps(cell) {
        if (!cell) {
            return {};
        }
        return {
            background:
                cell.row.original.state === "pending"
                    ? "#b2e1ff"
                    : cell.row.original.state === "accepted"
                      ? "#bcff7d"
                      : "#ff7c7e",
        };
    }

    render() {
        if (
            !this.props.connected ||
            !this.props.userInfos ||
            (this.props.userInfos?.profil !== "admin" &&
                !this.props.userInfos?.canValidatePOIContributions)
        ) {
            return <div>Non accessible.</div>;
        }

        const columns = [
            {
                header: "Id",
                accessorKey: "edit_id",
                maxWidth: 100,
            },
            {
                header: "Couche",
                accessorKey: "poi_table",
                minWidth: 150,
            },
            {
                header: "Action",
                accessorKey: "action",
                maxWidth: 100,
            },
            {
                header: "Utilisateur",
                accessorKey: "utilisateur",
            },
            {
                header: "Changement",
                accessorKey: "changement",
                cell: (props) => {
                    return props.getValue() ? (
                        <ul>
                            {props.getValue().map((element, i) => {
                                return <li key={i}>{element}</li>;
                            })}
                        </ul>
                    ) : (
                        "-"
                    );
                },
                minWidth: 250,
            },
            {
                header: "Geolocalisation",
                accessorKey: "geolocalisation",
            },
            {
                header: "Position avant mise à jour",
                accessorKey: "position_precedente",
                cell: (props) => {
                    return displayLinkCoordinates(
                        props.row.original.position_precedente
                    );
                },
            },
            {
                header: "Nouvelle position",
                accessorKey: "position_courante",
                cell: (props) => {
                    return displayLinkCoordinates(props.row.original.position_courante);
                },
            },
            {
                header: "Mise à jour",
                accessorKey: "mise_a_jour",
                cell: (props) => {
                    return props.getValue().toLocaleString();
                },
            },
            {
                header: "Actions",
                accessorKey: "actions",
                cell: (props) => {
                    if (props.row.original.state !== "pending") {
                        return null;
                    }
                    return (
                        <div>
                            <button
                                className="btn btn-success"
                                onClick={() =>
                                    this.handleContribution(
                                        props.row.original.edit_id,
                                        "accepted"
                                    )
                                }
                            >
                                Accepter
                            </button>
                            <button
                                className="btn btn-danger"
                                onClick={() =>
                                    this.handleContribution(
                                        props.row.original.edit_id,
                                        "refused"
                                    )
                                }
                            >
                                Refuser
                            </button>
                        </div>
                    );
                },
            },
        ];
        return (
            <div>
                <div className="panel-body">
                    <h3 className="panel-title pull-left">
                        Contributions proposées par les utilisateurs
                    </h3>
                    <TableContainer
                        tableClassName={`table-striped`}
                        containerClassName={styles.fullViewportWidth}
                        data={this.state.contributions}
                        columns={columns}
                        defaultPageSize={10}
                        getCellProps={this.getCellProps}
                    />
                </div>
            </div>
        );
    }
}

export default POIUserContributions;
