/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState } from "react";

/**
 * This component manages the display of the application footer
 */
function RegionalEmbeddedFooter(props) {
    const [display, setDisplay] = useState(true);
    let footer = {};
    footer.left = [];
    footer.right = [];
    if (
        props.parentApi &&
        props.parentApi.data &&
        props.parentApi.data?.settings?.footer
    ) {
        // Construction of the footer
        for (let dir of ["left", "right"]) {
            for (let block of JSON.parse(props.parentApi.data.settings.footer)[dir]) {
                let title = block.title;
                let subtitle = block.subtitle;
                let key = title + Date.now();
                let logos = [];
                for (let logo of block.logos) {
                    logos.push(
                        <img
                            key={logo.title}
                            src={"/img/" + logo.img}
                            alt={logo.title}
                        />
                    );
                }
                footer[dir].push(
                    <div className="embedded-footer-block" key={key}>
                        <label>{title}</label>
                        <div>{logos}</div>
                        <label className="subtitle">{subtitle}</label>
                    </div>
                );
            }
        }
    }

    return (
        <div
            className={`embedded-footer-widgets ${
                display ? "" : "float-widget embedded-footer-closed"
            }`}
        >
            <button
                type="button"
                className={
                    display
                        ? "embedded-footer-close-button"
                        : "embedded-footer-open-button"
                }
                aria-label={display ? "Masquer" : "Accédez au graphique sur TerriSTORY"}
                title={display ? "Masquer" : "Accédez au graphique sur TerriSTORY"}
                onClick={() => setDisplay(!display)}
            >
                {!display && <i className="bi bi-info-lg"></i>}
                {display && (
                    <div className="bi bi-caret-left-fill" aria-hidden="true"></div>
                )}
            </button>

            {display && (
                <div className="infos-content embedded-footer">
                    <a href="/" target="_blank" rel="noopener noreferrer">
                        <div className="img-embedded-footer">
                            <img
                                src="../../img/logo_TerriSTORY_transparent.png"
                                alt="Logo de TerriSTORY®"
                                title="Cliquez ici pour ouvrir ce graphique sur TerriSTORY et accéder à l'ensemble des fonctionnalités"
                            ></img>
                        </div>
                    </a>
                    {footer["left"]}
                    {/* {footer["right"]} */}
                </div>
            )}
        </div>
    );
}

export default RegionalEmbeddedFooter;
