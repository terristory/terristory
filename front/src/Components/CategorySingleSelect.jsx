/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState } from "react";
import SliderSingleSelectInput from "./Inputs/SliderSingleSelectInput";

/**
 * Renders a single selection input for a given category, allowing the user to choose from provided options.
 * Can be in the form of a toggle switch, a select dropdown, or a slider, depending on the specified inputType.
 *
 * This component is used both in the map sidebar (Info.js) and in the dashboards.
 *
 *
 * @param {Object} props
 * @param {string} props.category - The name of the category
 * @param {Array<{value: string, label: string}>} props.options - An array of the options for the category : [{value:..., label:...}]
 * @param {string} props.value - An optional controlled value for the input (when used as a controlled component)
 * @param {string} props.defaultValue - An optionnal default value for the input (when used as an uncontrolled component)
 * @param {Function} props.onChange - Called on selection change. It receives the category and the selected option arguments.
 * @param {string} props.inputType - The type of input to render. Can be one of: "selection", "switch-button", "slider"
 * @param {string} props.title - A title for the input
 * @param {string} props.tooltip - A text to show on hover
 * @param {boolean} props.isLoading - A flag indicating whether the component is in a loading state (allows disabling during loading)
 * @param {string} [props.id] - the html id for the input
 *
 * @returns {JSX.Element} A React component that allows the user to select a single option for the specified category.
 */
function CategorySingleSelect({
    category,
    options,
    value,
    defaultValue,
    onChange,
    inputType,
    title,
    tooltip,
    isLoading,
    id,
}) {
    // Component can be both controlled and uncontrolled
    const isControlled = typeof value !== "undefined";
    const [internalValue, setInternalValue] = useState(
        isControlled ? value : defaultValue
    );
    // Determine the current value based on controlled or uncontrolled
    const currentValue = isControlled ? value : internalValue;

    let input = "";
    if (inputType === "switch-button") {
        input = (
            <div key={"switch-button-" + category}>
                <span key="before" className="me-2" htmlFor={"input-" + category}>
                    {options[0].label}
                </span>
                <label key="button" className="switch">
                    {!isLoading ? (
                        <>
                            <input
                                type="checkbox"
                                checked={currentValue === options[1].value}
                                onChange={(e) => {
                                    let selectedOption = e.target.checked
                                        ? options[1].value
                                        : options[0].value;

                                    onChange(selectedOption);
                                    setInternalValue(selectedOption);
                                }}
                                id={id || "input-" + category}
                                name={"input-" + category}
                            />
                            <span className="slider round"></span>
                        </>
                    ) : (
                        <>
                            <span
                                className="spinner-border spinner-border-sm me-1"
                                role="status"
                                aria-hidden="true"
                            ></span>
                            <span className="visually-hidden">Loading...</span>
                        </>
                    )}
                </label>
                <span
                    key="after"
                    htmlFor={"infos-switch-button-" + category}
                    className="ms-2"
                >
                    {options[1].label}
                </span>
            </div>
        );
    } else if (inputType === "selection") {
        input = (
            <div key={"selection-" + category}>
                <select
                    value={currentValue}
                    onChange={(e) => {
                        let selectedValue = options.find(
                            (o) => String(o.value) === e.target.value
                        ).value;
                        onChange(selectedValue);
                        setInternalValue(selectedValue);
                    }}
                    id={id || "input-" + category}
                    name={"input-" + category}
                >
                    {options.map(({ label, value: optionValue }) => {
                        return (
                            <option
                                key={optionValue}
                                value={optionValue}
                                style={{
                                    // color: option.color,
                                    fontWeight: "bold",
                                }}
                            >
                                {label}
                            </option>
                        );
                    })}
                </select>
            </div>
        );
    } else if (inputType === "slider") {
        input = (
            <SliderSingleSelectInput
                id={id}
                options={options}
                value={currentValue}
                onChange={(selectedOption) => {
                    onChange(selectedOption);
                    setInternalValue(selectedOption);
                }}
            ></SliderSingleSelectInput>
        );
    } else {
        throw new Error(`Invalid input type : ${inputType} (${typeof inputType})`);
    }

    return (
        <>
            {title && (
                <label htmlFor={id || "input-" + category} title={tooltip}>
                    {title}
                </label>
            )}

            <div
                className={`d-flex flex-row  ${
                    inputType === "selection"
                        ? ""
                        : "justify-content-center flex-grow-1"
                } `}
            >
                {input}
            </div>
        </>
    );
}

export default CategorySingleSelect;
