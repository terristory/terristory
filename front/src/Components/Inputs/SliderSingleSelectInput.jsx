/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import Slider from "rc-slider";
import React, { useState } from "react";

import "rc-slider/assets/index.css";
import styles from "./SliderSingleSelectInput.module.css";

/**
 * Component that allows the user to select a single value from a list of string or number options using a slider.
 *
 *
 * @param {Object} props
 * @param {Array<{value: string|Number, label: string}>} props.options - The list of options that the slider will select from.
 * @param {string} props.value - The default selected option value (when used as a controlled component)
 * @param {string} props.defaultValue - The default selected option value (when used as an uncontrolled component)
 * @param {Function} props.onChange - Callback function called when the selected option changes.
 * @param {boolean} props.isNumerical - Set to true if options are numerical and marks should be placed accordingly
 * @param {string} [id] - the html id for the input
 *
 * @returns {JSX.Element} A React component that renders a slider input for selecting a string from the list of options.
 */
function SliderSingleSelectInput({
    options,
    value: valueFromProps,
    defaultValue,
    onChange,
    isNumerical = false,
    id,
}) {
    // Automatically detect if values are numerical
    isNumerical = options.every((option) => typeof option.value === "number");

    // Component can be both controlled and uncontrolled
    const isControlled = typeof valueFromProps !== "undefined";
    const [internalValue, setInternalValue] = useState(
        isControlled ? valueFromProps : defaultValue
    );

    // map current value to the corresponding slider numerical value
    const currentValue = isControlled ? valueFromProps : internalValue;
    const sliderNumericalValue = isNumerical
        ? Number(currentValue)
        : options.map((o) => o.value).indexOf(currentValue);

    // State that tracks the real time value of the slider (internalValue is only updated on change complete)
    const [internalSliderNumericalValue, setInternalSliderNumericalValue] =
        useState(sliderNumericalValue);

    const handleSliderChangeComplete = (sliderNumericalValue) => {
        // map slider numerical value back to option value
        const selectedOption = isNumerical
            ? options.find((o) => o.value === sliderNumericalValue).value
            : options[sliderNumericalValue].value;
        onChange(selectedOption);

        // for when used as uncontrolled
        setInternalValue(selectedOption);
    };

    let numericalValues = isNumerical ? options.map((o) => Number(o.value)) : null;

    let min = isNumerical ? Math.min(...numericalValues) : 0;
    let max = isNumerical ? Math.max(...numericalValues) : options.length - 1;

    let marks = options.reduce((acc, option, index) => {
        const key = isNumerical ? Number(option.value) : index;
        acc[key] = {
            label: (
                <span
                    title={option.label}
                    style={{
                        display: "-webkit-inline-box",
                        WebkitBoxOrient: "vertical",
                        WebkitLineClamp: 3, // Limits text to 3 lines. Needs enough bottom padding.
                        textOverflow: "ellipsis",
                        ...(options.length > 2 && {
                            maxWidth: "60px",
                            overflow: "hidden",
                        }), // Should allow 4 or 5 elements to display correctly
                    }}
                >
                    {option.label}
                </span>
            ),
            style: { color: "#222222" },
        };
        return acc;
    }, {});

    // make slider smaller when only 2 options
    const sliderStyle = {
        width: options.length < 3 ? "50%" : "100%",
    };

    // Custom styles for rail and handle
    const railStyle = {
        backgroundColor: "#cccccc",
    };

    const handleStyle = {
        borderColor: "#222222",
        backgroundColor: "#fff",
        borderWidth: "2px",
    };

    const trackStyle = {
        backgroundColor: "#cccccc",
    };

    const dotStyle = {
        backgroundColor: "#ffffff",
        borderColor: "#595959",
    };

    const activeDotStyle = {};
    return (
        <div className={styles.sliderWrapper} style={sliderStyle}>
            <Slider
                id={id}
                min={min}
                max={max}
                value={internalSliderNumericalValue}
                onChange={(v) => setInternalSliderNumericalValue(v)}
                onChangeComplete={handleSliderChangeComplete}
                marks={marks}
                step={null}
                styles={{
                    track: trackStyle,
                    rail: railStyle,
                    handle: handleStyle,
                }}
                dotStyle={dotStyle}
                activeDotStyle={activeDotStyle}
            />
        </div>
    );
}

export default SliderSingleSelectInput;
