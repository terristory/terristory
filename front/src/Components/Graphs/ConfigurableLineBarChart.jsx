/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React from "react";
import { Chart } from "react-chartjs-2";
import deepmerge from "deepmerge";

import styles from "./ConfigurableLineBarChart.module.css";

/**
 * A configurable chart component for rendering bar or line charts using the react-chartjs-2 library
 * This component serves as a wrapper around the Chart.js "Chart" component, providing default options (fonts, styles, layout...)
 *
 * @param {Object} props
 * @param {("bar" | "line")} props.type - The type of the chart, either 'bar' or 'line'.
 * @param {import("chart.js").ChartData<'bar' | 'line'>} props.data - The data object that is passed into the Chart.js chart
 * @param {import("chart.js").ChartOptions} [props.options] - The options object that is passed into the Chart.js chart
 * @param {("category" | "linear" | "time")} [props.xScaleType="linear"] - The type of the x-axis scale.
 * @param {boolean} [props.stacked=false] - Whether to stack the bars in the chart (x and y axis)
 * @param {string} [props.title=""] - The title of the chart
 * @returns {JSX.Element}
 */
function ConfigurableLineBarChart({
    type,
    data,
    options = {},
    xScaleType = "linear",
    stacked = false,
    title = "",
}) {
    const defaultOptions = {
        responsive: true,
        maintainAspectRatio: false,
        elements: {
            point: {
                radius: 3,
            },
        },
        scales: {
            x: {
                type: xScaleType,
                stacked: stacked,
            },
            y: {
                stacked: stacked,
            },
        },
        hover: {
            mode: "index",
            intersect: false,
        },
        plugins: {
            title: {
                display: true,
                text: title,
            },
            legend: {
                display: true,
                position: "bottom",
                align: "start",
                labels: {
                    boxWidth: 10,
                },
            },
        },
    };

    // Add fill option to datasets if stacked prop is true
    const mergedData = {
        ...data,
        datasets: data.datasets.map((dataset) => ({
            ...(type === "line" && stacked
                ? {
                      fill: {
                          target: "origin",
                          above: dataset.backgroundColor || "rgba(0,0,0,0.1)",
                      },
                  }
                : {}),
            ...dataset,
        })),
    };

    const mergedOptions = deepmerge(defaultOptions, options);

    return (
        <div className={styles.ResponsiveChartContainer}>
            <Chart type={type} data={mergedData} options={mergedOptions}></Chart>
        </div>
    );
}

export default ConfigurableLineBarChart;
