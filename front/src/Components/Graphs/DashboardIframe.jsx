import React from "react";

/**
 * @typedef {Object} IframeConfig
 * @property {string} src
 * @property {number} width in px
 * @property {number} height in px
 * @property {string} title
 */

/**
 * A React component that displays an iframe. For use in dashboards.
 * @param {IframeConfig} config
 * @returns {JSX.Element}
 */
function DashboardIframe({ src, width, height, title }) {
    if (!src.startsWith("https://")) {
        return null;
    }
    return (
        <>
            {title && <h4 className="nom-indicateur">{title}</h4>}
            <iframe
                src={src}
                width={width}
                height={height}
                style={{
                    border: "none",
                }}
                sandbox="allow-scripts allow-same-origin"
                title={title}
            />
        </>
    );
}

export default DashboardIframe;
