/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React from "react";

import styles from "./MultiSelectLegend.module.css";

/**
 * @typedef {Object} LegendOption
 * @property {string} value
 * @property {string} label
 * @property {string} color
 * @property {boolean} disabled
 */

/**
 * Displays a checkbox and a colored square for each option of the category.
 *
 * Display order matches the order of the options prop
 *
 * @param {Object} props
 * @param {LegendOption[]} props.options
 * @param {Set<string>} props.selectedOptions - Set of LegendOption.value representing currently selected options.
 * @param {function(Set<string>): void} props.onChange
 *
 * @returns {JSX.Element} A multi-select legend with checkboxes.
 */
function MultiSelectLegend({ options, selectedOptions, onChange }) {
    if (new Set(options.map((option) => option.value)).size !== options.length) {
        throw new Error("Options for MultiSelectLegend must have unique values");
    }

    const handleCheckboxChange = (/** @type {string} */ value) => {
        const newSelectedOptions = new Set(selectedOptions);
        newSelectedOptions.has(value)
            ? newSelectedOptions.delete(value)
            : newSelectedOptions.add(value);
        onChange(newSelectedOptions);
    };

    const handleSelectAll = () => {
        onChange(
            new Set(
                options
                    .filter((option) => !option.disabled)
                    .map((option) => option.value)
            )
        );
    };
    const handleDeselectAll = () => {
        onChange(new Set());
    };

    return (
        <div>
            {options.map(({ value, label, color, disabled }) => {
                return (
                    <div key={value} className={styles.LegendOptionContainer}>
                        <input
                            type="checkbox"
                            id={value}
                            disabled={disabled}
                            checked={selectedOptions.has(value)}
                            onChange={() => handleCheckboxChange(value)}
                        />
                        <label htmlFor={value} className={styles.LegendLabel}>
                            <span
                                className={styles.LegendColorSquare}
                                style={{
                                    backgroundColor: color,
                                }}
                            />
                            {label}
                        </label>
                    </div>
                );
            })}
            <span className={styles.SelectDeselectContainer}>
                <button className={styles.SelectDeselectAll} onClick={handleSelectAll}>
                    Tout sélectionner
                </button>
                /
                <button
                    className={styles.SelectDeselectAll}
                    onClick={handleDeselectAll}
                >
                    Tout déselectionner
                </button>
            </span>
        </div>
    );
}

export default MultiSelectLegend;
