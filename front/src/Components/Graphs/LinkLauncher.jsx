/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";

import { Link } from "react-router-dom";

import "bootstrap-icons/font/bootstrap-icons.css";

/**
 * This component is used to create a link launcher that dynamically generates
 * a URL based on various props.
 */
class LinkLauncher extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id_analyse: String(props.id_analysis),
            name: props.name,
            representation: "link-launcher",
            type: props.typeLink,
            typeName: props.nameTypeLink,
            keyLink: props.keyLink,
            icon: props.icon,
            widthDOM: window.innerWidth,
            color: props.color,
            displayColorStartPicker: false,
            displayColorEndPicker: false,
            zoneType: props.zoneType ?? props.parentApi.data.zone.zone,
            zoneId: props.zoneId ?? props.parentApi.data.currentZone,
            zoneMaille: props.zoneMaille ?? props.parentApi.data.zone.maille,
        };
    }

    componentDidMount() {
        let id = this.props.id;
        if (typeof id === "string" && ["-"].includes(id)) {
            id = id.split("-")[-1];
        }

        // Adjust the chart dimensions if needed
        let widthDiv = document.getElementsByClassName("charts-legende")[0].offsetWidth;
        if (
            this.props.parentApi.data.tailleDiv !== widthDiv &&
            this.props.provenance === "tableau_de_bord_restitue"
        ) {
            this.props.parentApi.callbacks.tailleDiv(widthDiv);
        }

        this.props.parentApi.callbacks.updateDataLoaded(true);
    }

    /**
     * Builds a dynamic URL based on the current zone, type, and other parameters from the component's state and props.
     * @returns {string} The constructed URL.
     */
    getUrlParams() {
        const zone = this.state.zoneType;
        const currentZone = this.state.zoneId;
        const maille = this.state.zoneMaille;
        const { type, typeName } = this.state;

        const territoryType = `?zone=${zone}`;
        const territory = `&maille=${maille}`;
        const codeInseeTerritory = `&zone_id=${currentZone}`;
        const typeParam = type ? `&${typeName}=${type}` : "";

        return `${territoryType}${territory}${codeInseeTerritory}${typeParam}`;
    }

    render() {
        const { name, keyLink, icon } = this.state;
        const url = keyLink + this.getUrlParams();
        return (
            <div>
                <div className="charts-legende">
                    <Link to={url} target="_blank" className="no-link-description">
                        <div className="btn">
                            <div className={`img link-launcher ${icon}`}></div>
                            {name}
                        </div>
                    </Link>
                    <span className="only-print">
                        Contient un{" "}
                        <a href={window.location.origin + "/" + url}>lien</a>
                    </span>
                </div>
            </div>
        );
    }
}

export default LinkLauncher;
