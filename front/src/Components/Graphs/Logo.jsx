/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect } from "react";
import Api from "../../Controllers/Api";
import config from "../../settings";
import { buildRegionUrl, slugify } from "../../utils";

/**
 * This component is used to create a clickable data source logo.
 */

const Logo = ({ id_analysis, parentApi, provenance, zoneType, zoneId, zoneMaille }) => {
    const [selectedLogo, setSelectedLogo] = useState(null);
    const allowedVariables = [
        "<type_territoire>",
        "<code_territoire>",
        "<nom_territoire_slug>",
    ];

    useEffect(() => {
        const url = buildRegionUrl(
            config.api_logo_sources,
            parentApi.data.region
        ).replace("#id#", id_analysis);

        Api.callApi(url, null, "GET")
            .then((response) => {
                setSelectedLogo(response);
                parentApi.callbacks.updateDataLoaded(true);
            })
            .catch((error) => {
                console.error(error);
                parentApi.callbacks.updateDataLoaded(true);
            });
    }, [id_analysis, parentApi.callbacks, parentApi.data.region]);

    const replaceVariablesInUrl = (url, variables) => {
        let replacedUrl = url;

        variables.forEach((variable) => {
            const variableLowerCase = variable.toLowerCase();
            const valueToReplace = getValueForVariable(variableLowerCase, parentApi);
            const regex = new RegExp(variableLowerCase, "ig"); // Case-insensitive regex
            replacedUrl = replacedUrl.replace(regex, valueToReplace); // Use regex to replace ignoring case
        });

        return replacedUrl;
    };

    const getValueForVariable = (variable, parentApi) => {
        const zone = { zone: zoneType, maille: zoneMaille };
        // Retrieve the territory name and replace prefixes if needed
        let nomTerritoire = (
            parentApi.controller.zonesManager.getZoneName(
                zoneId ?? parentApi.data.currentZone, // Use zoneId if it exists
                zone
            ) ?? parentApi.data.nomTerritoire
        )
            .replace(/^CC\s/, "Communauté de communes ")
            .replace(/^CA\s/, "Communauté d'agglomération ")
            .replace(/^CU\s/, "Communauté urbaine ");

        const normalizedVariable = variable.toLowerCase();
        switch (normalizedVariable) {
            case "<type_territoire>":
                return zoneType ?? parentApi.data.zone.zone; // Use ZoneType if it exists

            case "<code_territoire>":
                return zoneId ?? parentApi.data.currentZone; // Use zoneId if it exists

            case "<nom_territoire_slug>":
                return slugify(nomTerritoire); // Slugify the modified territory name

            default:
                return variable;
        }
    };

    const displayLogo = (id, extension, url, name, enabled_zone, default_url) => {
        const enabledZonesArray = enabled_zone ? enabled_zone.split(",") : [];
        const zone = zoneType ?? parentApi.data.zone.zone;
        const finalUrl =
            enabledZonesArray.includes(zone) || !default_url
                ? replaceVariablesInUrl(url, allowedVariables) // Use the original URL if zone exists
                : replaceVariablesInUrl(default_url, allowedVariables); // Use default_url if zone does not exist

        return (
            <figure style={{ cursor: "pointer" }}>
                <a href={finalUrl} target="_blank" rel="noopener noreferrer">
                    <img
                        id={id}
                        src={`img/logo_source_fiches/${parentApi.data.region}_logo${id}.${extension}`}
                        style={{ maxHeight: "260px", maxWidth: "260px" }}
                        alt={name ? `Logo for ${name}` : "Logo"}
                        title={name}
                        loading="lazy" // for performance
                    />
                </a>
                <figcaption className="only-print" style={{ marginLeft: "20px" }}>
                    Contient un <a href={finalUrl}>lien</a>
                </figcaption>
            </figure>
        );
    };

    return (
        <div className="charts-legende">
            {selectedLogo &&
                displayLogo(
                    selectedLogo.id,
                    selectedLogo.extension,
                    selectedLogo.url,
                    selectedLogo.name,
                    selectedLogo.enabled_zone,
                    selectedLogo.default_url
                )}
        </div>
    );
};

export default Logo;
