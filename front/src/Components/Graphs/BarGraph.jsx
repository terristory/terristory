/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import { Bar } from "react-chartjs-2";

import chroma from "chroma-js";
import pattern from "patternomaly";

import Api from "../../Controllers/Api";
import { buildRegionUrl, splitCategoryFilters } from "../../utils";

import config from "../../settings";
import configData from "../../settings_data";

/**
 * This component used to create a graph with stacked curves
 */
class BarGraph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id_analyse: String(props.id_analysis),
            representation: this.props.representation ?? "bar",
            id: props.type.categorie,
            disabled: false,
            filters: {},
            isLoading: false,
        };

        this.widthGraph = 650;
        this.heightGraph = 300;
    }

    componentDidMount() {
        // Retrieve initial filters
        let filters =
            this.props.filters ??
            this.props.parentApi.controller.analysisManager.initFiltersByCategory(
                parseInt(this.props.id_analysis)
            );
        this.setState({ filters });

        // Get data from API
        this.fetchConfiguration(filters);

        let widthDiv = document.getElementsByClassName("charts-legende")[0].offsetWidth;
        if (
            this.props.parentApi.data.tailleDiv !== widthDiv &&
            this.props.provenance === "tableau_de_bord_restitue"
        ) {
            this.props.parentApi.callbacks.tailleDiv(widthDiv);
        }
    }

    /**
     * This function updates the data from which we build the stacked curves charts when changing territory
     * @param {object key => value} prevProps : main component properties before territory change via selection tool
     */
    componentDidUpdate(prevProps, prevState) {
        let currentZone = this.props.zoneId ?? this.props.parentApi.data.currentZone;
        let prevCurrentZone = prevProps.zoneId ?? prevProps.parentApi.data.currentZone;
        let filtreCategorieCourant = this.props.parentApi.data.filtreCategorieCourant;
        let numeroIndicateurCourant = this.props.parentApi.data.numeroIndicateur;

        if (this.state.dataTraj && prevState.dataTraj) {
            if (
                this.state.dataTraj.codeInseeTerritoire !==
                    prevState.dataTraj.codeInseeTerritoire &&
                !this.state.isLoading
            ) {
                this.fetchConfiguration();
            }
        }

        // we find different reasons to retrieve configuration
        const wasUnitUpdated = prevProps.unit !== this.props.unit;
        const wereFiltersUpdated =
            JSON.stringify(this.state.filters) !== JSON.stringify(prevState.filters);
        const wasZoneUpdated = prevCurrentZone !== currentZone && this.state.id_analyse;
        const wasCurrentFilterUpdated =
            filtreCategorieCourant !==
                prevProps.parentApi.data.filtreCategorieCourant &&
            numeroIndicateurCourant === this.props.id;
        const werePropsFiltersForced =
            this.props.forcedFilters &&
            JSON.stringify(this.props.forcedFilters) !==
                JSON.stringify(prevProps.forcedFilters);
        if (
            wasUnitUpdated ||
            wereFiltersUpdated ||
            wasZoneUpdated ||
            wasCurrentFilterUpdated
        ) {
            this.fetchConfiguration(this.state.filters);
        } else if (werePropsFiltersForced) {
            this.setState({
                filters: {
                    ...this.state.filters,
                    ...this.props.forcedFilters,
                },
            });
        }
    }

    componentWillUnmount() {
        if (this.dataPromise) {
            this.dataPromise.abort();
            this.props.parentApi.callbacks.updateDataLoaded(true);
        }
    }

    fetchConfiguration(filtres) {
        let codeInseeTerritoire =
            this.props.zoneId ?? this.props.parentApi.data.currentZone;
        let pZone =
            "?zone=" + (this.props.zoneType ?? this.props.parentApi.data.zone.zone);
        let pMaille =
            "&maille=" +
            (this.props.zoneMaille ?? this.props.parentApi.data.zone.maille);
        let pZoneId = "&zone_id=" + codeInseeTerritoire;
        if ((this.props.zoneType ?? this.props.parentApi.data.zone.zone) === "region") {
            pZoneId = "&zone_id=" + this.props.parentApi.data.regionCode;
        }
        let unit = this.props?.unit ? "&unit_id=" + this.props.unit : "";

        this.setState({ isLoading: true });
        let id_analyse = this.state.id_analyse;
        let representation = this.state.representation;
        let dataSource =
            config.api_analysis_meta_url +
            id_analyse +
            "/graphique/" +
            representation +
            pZone +
            pMaille +
            pZoneId +
            unit;

        this.props.parentApi.callbacks.updateDataLoaded(false);

        let body = JSON.stringify(filtres);
        if (this.dataPromise) this.dataPromise.abort();
        this.dataPromise = Api.callApi(
            buildRegionUrl(dataSource, this.props.parentApi.data.region),
            body,
            "POST"
        );
        this.dataPromise
            .then((json) => {
                this.props.parentApi.callbacks.updateDataLoaded(true);
                if (!json) {
                    return;
                }

                if (json["disabled"] && json["disabled"] === true) {
                    this.setState({
                        disabled: true,
                        isLoading: false,
                    });

                    return;
                }

                json["codeInseeTerritoire"] = codeInseeTerritoire;
                if (
                    codeInseeTerritoire ===
                    (this.props.zoneId ?? this.props.parentApi.data.currentZone)
                ) {
                    this.setState({
                        dataTraj: json,
                        isLoading: false,
                    });
                }
            })
            .catch((error) => {
                if (error.name === "AbortError") return;
                this.props.parentApi.callbacks.updateDataLoaded(true);
                this.setState({ isLoading: false });
            });
    }

    /**
     * Launches a main component method to add/remove a modality to the filter
     * Triggered when a checkbox associated with a modality is checked
     * @param {chaine de caractère} modalite : modality checked / unchecked
     */
    filtrerSurLegende(event, modalite, categorie) {
        let filterWithTableName = categorie + "." + modalite;
        // if we do have filters
        if (!this.state.filters) return;

        let _filters = JSON.parse(JSON.stringify(this.state.filters));
        // if we do not have current filter
        if (!_filters[categorie]) return;

        // we first retrieve the filter ID if present inside the filters
        const idIfPresent = _filters[categorie].findIndex(
            (element) => element && element.filtre_categorie === filterWithTableName
        );

        // do we have to add or do we have to remove
        if (event.target.checked) {
            // if we couldn't find it, it means we need to add it
            if (idIfPresent === -1) {
                _filters[categorie].push({
                    filtre_categorie: filterWithTableName,
                    type: "pie",
                });
            }
        } else {
            // we are going to delete the filter if we can find it
            if (idIfPresent !== -1) {
                delete _filters[categorie][idIfPresent];
                _filters[categorie] = _filters[categorie].flat();
            }
        }

        // if the action resulted in some changes between previous filters
        // and new filters => we update the state
        if (JSON.stringify(this.state.filters) !== JSON.stringify(_filters)) {
            this.setState({
                filters: _filters,
            });
            if (this.props.provenance === "tableau_de_bord") {
                // we also callback the updated filters in case of dashboard edition
                this.props.parentApi.data.tableauBordDonnees.donnees[
                    this.props.thematique
                ].indicateurs[this.props.id].filters = _filters;
            }
        }
    }

    processConfidentialityColor(ds) {
        let bgColor = undefined;
        let borderColor = undefined;
        if (ds.confidentiel === "oui") {
            // confidential
            bgColor = pattern.draw("diagonal-right-left", "#9e0012");
            borderColor = chroma("#9e0012").darken().alpha(1).css();
        } else {
            bgColor = chroma(ds.couleur).alpha(1).css();
            borderColor = chroma(ds.couleur).darken().alpha(1).css();
        }
        return { bgColor: bgColor, borderColor: borderColor };
    }

    /**
     * Builds the legend with which we can select or delete modalities
     * in the data to be displayed using checkboxes, from modalities by categories.
     * @param {object ke value} chart : data for a chart constructed as follows:
     * {
     *     data : {
     *         confid : Type of confidentiality,
     *         id : indicator identifier,
     *         labels : list of modalities,
     *         datasets : [{
     *             data : List of values in the correct order to match them
     *             to the order of the modalities,
     *             backgroundColor : List of colors in the correct order to match them
     *             to the order of the modalities
     *         }]
     *     }
     * }
     * This array contains other keys that we don't use in this method.
     */
    buildLegend(data, categorie) {
        let identifiant_indicateur = this.state.id_analyse;
        let legende = [];
        let tailleSpan = "10px";
        let taillePoliceModalites = "";
        let hauteurLegendeModalites = "";
        let hauteurLegende = "";
        if (
            this.props.provenance !== "tableau_de_bord" &&
            this.props.provenance !== "tableau_de_bord_restitue"
        ) {
            tailleSpan = "15px";
            taillePoliceModalites = " taille-police-suivi-energetique";
            hauteurLegendeModalites = " legende-modalite-suivi-energetique";
            hauteurLegende = " liste-modalites-suivi-energetique";
        }
        let retourLigneLibelleModalites = "";
        if (this.props.provenance === "tableau_de_bord_restitue") {
            retourLigneLibelleModalites = " legende-modalite-tableau-bord";
        }
        let filtreInitial = this.state.filters;
        let listeModaliteFiltre = [];
        for (let elem of filtreInitial[categorie]) {
            listeModaliteFiltre.push(splitCategoryFilters(elem["filtre_categorie"])[1]);
        }
        for (let i in data[categorie].categories) {
            let checked = false;
            let disabled = false;
            let couleur = data[categorie].categories[i].couleur;
            if (listeModaliteFiltre.indexOf(data[categorie].categories[i].nom) !== -1) {
                checked = true;
            }
            if (data[categorie].categories[i].confidentiel === "oui") {
                disabled = true;
                couleur = "#9e0012";
            }
            legende.push(
                <div
                    className={"legende-modalite" + hauteurLegendeModalites}
                    key={"legende-" + i + "-" + data[categorie].categories[i].nom}
                >
                    <span
                        className="element-legende-modalites"
                        style={{
                            backgroundColor: couleur,
                            display: "block",
                            height: tailleSpan,
                            width: tailleSpan,
                        }}
                    ></span>
                    <input
                        type="checkbox"
                        id={
                            "legende-" +
                            this.props.id +
                            "-" +
                            i +
                            this.state.id +
                            "-" +
                            data[categorie].categories[i].nom
                        }
                        defaultChecked={checked}
                        className="element-legende-modalites"
                        disabled={disabled}
                        onChange={(event) =>
                            this.filtrerSurLegende(
                                event,
                                data[categorie].categories[i].nom,
                                categorie
                            )
                        }
                    ></input>
                    <label
                        htmlFor={
                            "legende-" +
                            this.props.id +
                            "-" +
                            i +
                            this.state.id +
                            "-" +
                            data[categorie].categories[i].nom
                        }
                        className={
                            "element-legende-modalites" +
                            retourLigneLibelleModalites +
                            taillePoliceModalites
                        }
                    >
                        {" "}
                        {data[categorie].categories[i].nom}
                    </label>
                </div>
            );
        }
        // legende = legende.reverse();
        return (
            <ul
                className={"liste-modalites" + hauteurLegende}
                key={"legende-finale-" + identifiant_indicateur}
            >
                {legende}
            </ul>
        );
    }

    /**
     * This function returns the total of values by type. Thus, they are formatted
     * in order to be used in the graph where the curves of renewable energy production and final energy
     * consumption are displayed, within the impact results.
     * @param  {object} data : array of data by type, then by year/value
     * @param  {object} liste_annees: list of years for which data is available for consumption and production
     */
    totalParAnnee(donnees, type) {
        let miseEnFormeTotal = {
            backgroundColor: "rgb(255, 255, 255, 0)", // add transparency to avoid the option "fill : false"
            borderColor: "rgba(0, 0, 0, 0)",
            label: "",
            yAxisID: "y-axis-trajectoire",
            borderWidth: 2,
            order: 2,
        };

        if (donnees) {
            let liste_annees = [];
            if (donnees[type].traj[0] && donnees[type].traj !== "Confidentiel") {
                for (let j in donnees[type].traj[0].data) {
                    if (
                        donnees[type].traj[0].data[j]["annee"] >=
                            (configData.anneeMin[this.props.parentApi.data.region] ??
                                configData.anneeMin.default) -
                                1 &&
                        (this.props.provenance === "tableau_de_bord" ||
                            this.props.provenance === "tableau_de_bord_restitue")
                    ) {
                        liste_annees.push(donnees[type].traj[0].data[j]["annee"]); // generate the list of data to be entered as parameters of dataForConsoVsProd
                    } else if (this.props.provenance === "suivi_energetique") {
                        liste_annees.push(donnees[type].traj[0].data[j]["annee"]);
                    }
                }
                let dataSet = [];
                for (let annee in liste_annees) {
                    // For each year
                    let v = 0;
                    for (let i in donnees[type].traj) {
                        // We browse each modality (arbitrarily by type of energy because common to the production and consumption)
                        for (let k in donnees[type].traj[i].data) {
                            // We browse each row of data {year: value}
                            if (
                                donnees[type].traj[i].data[k].annee ===
                                liste_annees[annee]
                            ) {
                                // If the year matches then
                                v += parseFloat(
                                    donnees[type].traj[i].data[k].valeur.toFixed(2)
                                ); // We sum all the types to calculate the total
                            }
                        }
                    }
                    dataSet.push({
                        x: new Date(parseInt(liste_annees[annee], 10) + "-01-01"),
                        y: parseFloat(v.toFixed(2)),
                    });
                }
                let donneesFinales = { dataset: dataSet, labels: liste_annees }; // We transmit the data in the correct format
                miseEnFormeTotal.data = donneesFinales.dataset;
                return miseEnFormeTotal;
            }
        }
    }

    render() {
        if (this.state.disabled === true) {
            return (
                <div className="charts-legende">
                    <div className={"confid-chart"}>
                        Cet indicateur n'est pas activé actuellement, veuillez contacter
                        l'administrateur régional.
                    </div>
                </div>
            );
        }

        let consoData = {};
        let uniteConso = "";
        let categorie = this.props.type.categorie;
        let titre = "";
        if (this.props.type) {
            titre = this.props.type.titre;
        }
        let nomIndicateur =
            this.props.parentApi.controller.analysisManager.getAnalysisName(
                parseInt(this.state.id_analyse, 10)
            );
        let legendeFinale = "";
        let datasets = [];
        let annotations = [];
        let estimatedRatioYearsLegend = "";
        let yearsAssociation = {};

        if (this.state.dataTraj && categorie) {
            if (
                this.state.dataTraj["analyse"][categorie] &&
                this.state.dataTraj["analyse"][categorie].traj &&
                this.state.dataTraj["analyse"][categorie].traj !== "Confidentiel"
            ) {
                legendeFinale = this.buildLegend(
                    this.state.dataTraj.analyse,
                    categorie
                );
                let tot = this.totalParAnnee(this.state.dataTraj.analyse, categorie);
                if (this.state.representation === "bar-years") {
                    let orderedYears = [];
                    tot.data.forEach((element, i) => {
                        orderedYears.push(element.x.getFullYear());
                        yearsAssociation[element.x.getFullYear()] = i;
                    });
                    orderedYears = orderedYears.sort();
                    // add offset between non-sequential years
                    let offset = 0;
                    for (let index = 0; index < orderedYears.length; index++) {
                        const currentYear = orderedYears[index];
                        yearsAssociation[currentYear] = index + offset;
                        if (index < orderedYears.length - 1) {
                            const nextYear = orderedYears[index + 1];
                            yearsAssociation[currentYear] = index + offset;
                            if (nextYear - currentYear > 1) {
                                offset += 0.5;
                            }
                        }
                    }

                    tot.data = tot.data.map((element) => {
                        element.x = yearsAssociation[element.x.getFullYear()];
                        return element;
                    });
                }
                if (
                    this.state.dataTraj["analyse"][categorie] &&
                    this.state.dataTraj["analyse"][categorie].traj
                ) {
                    for (let ds of this.state.dataTraj["analyse"][categorie].traj) {
                        let colors = this.processConfidentialityColor(ds);
                        let dataset = {
                            backgroundColor: colors.bgColor,
                            borderColor: colors.borderColor,
                            label: ds.nom.split("\n"),
                            fill: { target: "origin", above: colors.bgColor },
                        };
                        let datasetData = [];
                        for (let d of ds.data) {
                            if (
                                d.annee >=
                                    (configData.anneeMin[
                                        this.props.parentApi.data.region
                                    ] ?? configData.anneeMin.default) -
                                        1 &&
                                (this.props.provenance === "tableau_de_bord" ||
                                    this.props.provenance ===
                                        "tableau_de_bord_restitue")
                            ) {
                                if (this.state.representation === "bar-years") {
                                    datasetData.push({
                                        x: yearsAssociation[d.annee],
                                        y: d.valeur,
                                    });
                                } else {
                                    datasetData.push({
                                        x: new Date(d.annee + "-01-01"),
                                        y: d.valeur,
                                    });
                                }
                            } else if (this.props.provenance === "suivi_energetique") {
                                datasetData.push({
                                    x: new Date(d.annee + "-01-01"),
                                    y: d.valeur,
                                });
                            }
                        }
                        dataset.data = datasetData;
                        datasets.push(dataset);
                        consoData[categorie] = {
                            datasets: datasets,
                        };
                        uniteConso = this.state.dataTraj["unite"];
                    }
                }
                if (tot) {
                    consoData[categorie].datasets.unshift(tot);
                    tot.data.forEach((yearVal) => {
                        let val = Math.round(yearVal.y);
                        if (val >= 10e9) {
                            val =
                                new Intl.NumberFormat("fr-FR").format(
                                    Math.round(val / 1000 / 1000 / 10) / 100
                                ) + "";
                        } else if (val >= 10e6) {
                            val =
                                new Intl.NumberFormat("fr-FR").format(
                                    Math.round(val / 1000 / 10) / 100
                                ) + "";
                        } else if (val >= 100000) {
                            val =
                                new Intl.NumberFormat("fr-FR").format(
                                    Math.round(val / 100) / 10
                                ) + " M";
                        } else {
                            val = new Intl.NumberFormat("fr-FR").format(val);
                        }
                        annotations.push({
                            type: "label",
                            xValue: yearVal.x,
                            xAdjust: 0,
                            yValue: yearVal.y,
                            yAdjust: yearVal.y >= 0 ? -10 : 10,
                            content: [val],
                            color: "rgb(33, 37, 41)",
                            borderRadius: 5,
                            position: "center",
                            font: {
                                size: 13,
                            },
                        });
                    });
                }
                consoData[categorie].datasets = consoData[categorie].datasets.reverse();
            }

            if (
                this.state.dataTraj["analyse"][categorie] &&
                this.state.dataTraj["analyse"][categorie].estimated_years_for_ratio
            ) {
                const { estimated_years_for_ratio } =
                    this.state.dataTraj["analyse"][categorie];
                if (estimated_years_for_ratio && estimated_years_for_ratio.length) {
                    let yearLegends = estimated_years_for_ratio.map(
                        (yearEstimation) =>
                            yearEstimation.original + " par " + yearEstimation.used
                    );
                    if (yearLegends.length > 0) {
                        yearLegends = yearLegends.join(", ");
                        estimatedRatioYearsLegend = `Attention ! Les années suivantes ont été estimées pour le calcul de ratio: ${yearLegends}.`;
                    }
                }
            }
        }

        let estimatedYears = [];
        let estimatedLegend = "";
        let customYearLabels;
        if (this.props.parentApi.controller.analysisManager) {
            estimatedYears =
                this.props.parentApi.controller.analysisManager.getAnalysisEstimatedYears(
                    parseInt(this.props.id_analysis, 10)
                );

            const indicatorConfig =
                this.props.parentApi.controller.analysisManager.analysis.find(
                    (a) => a.id === parseInt(this.props.id_analysis, 10)
                );
            customYearLabels =
                indicatorConfig?.representation_details?.customYearLabels;
            if (customYearLabels) {
                try {
                    customYearLabels = JSON.parse(customYearLabels);
                } catch {
                    customYearLabels = undefined;
                }
            }
        }
        if (estimatedYears.length > 0) {
            estimatedLegend = " ; (e) données estimées";
        }

        let titreAxeOrdonnees = nomIndicateur + " en " + uniteConso;
        let chartOptionsSuivi = {
            maintainAspectRatio: true,
            spanGaps: false,
            scales: {
                x: {
                    type: this.state.representation === "bar-years" ? "linear" : "time",
                    stacked: true,
                    display: true,
                    title: {
                        display: true,
                        text: "Années" + estimatedLegend,
                        color: "rgb(33, 37, 41)",
                        font: {
                            size: 16,
                        },
                    },
                    time: {
                        unit: "year",
                        displayFormats: {
                            year: "YYYY",
                        },
                    },
                    ticks: {
                        autoSkip: false,
                        minRotation: 45,
                        source: "data",
                        color: "rgb(33, 37, 41)",
                        font: {
                            size: 16,
                        },
                        callback: (value, index, values) => {
                            // Handle the "bar-years" case
                            if (this.state.representation === "bar-years") {
                                const tickText = Object.entries(
                                    yearsAssociation
                                ).filter((v, k) => v[1] === value);
                                if (tickText[0]) {
                                    const year = tickText[0][0];
                                    return (
                                        year +
                                        (estimatedYears.includes(+year) ? " (e)" : "")
                                    );
                                } else {
                                    return "";
                                }
                            }
                            // Handle the "time" (non-bar-years) case
                            else {
                                const date = new Date(value);
                                const year = date.getFullYear();

                                const customYearLabel =
                                    customYearLabels?.[year]?.split(" ");

                                return (
                                    customYearLabel ??
                                    year + (estimatedYears.includes(year) ? " (e)" : "")
                                );
                            }
                        },
                    },
                },
                y: {
                    stacked: true,
                    display: true,
                    ticks: {
                        reverse: false,
                        font: {
                            size: 14,
                        },
                    },
                    title: {
                        display: true,
                        text: titreAxeOrdonnees,
                        font: {
                            size: 14,
                        },
                        marginRight: 20,
                        paddingLeft: 20,
                    },
                    beginAtZero: true,
                },
                "y-axis-trajectoire": {
                    reverse: true,
                    type: "linear",
                    display: false,
                    position: "right",
                    grid: {
                        drawOnChartArea: false,
                    },
                    beginAtZero: true,
                },
            },
            hover: {
                mode: "single",
            },
            plugins: {
                title: {
                    display: true,
                    text: titre,
                    padding: {
                        bottom: 20,
                    },
                    color: "rgb(33, 37, 41)",
                    font: {
                        size: 16,
                    },
                },
                legend: {
                    position: "right",
                    display: false,
                    labels: {
                        color: "rgb(33, 37, 41)",
                        font: {
                            size: 16,
                        },
                        boxWidth: 10,
                    },
                    reverse: false,
                    maxWidth: 500,
                },
                tooltip: {
                    reverse: true,
                    mode: "x",
                    intersect: false,
                    color: "rgb(33, 37, 41)",
                    callbacks: {
                        title: function (data) {
                            data[0].dataset.label = "Total";
                            const date = new Date(data[0].label);
                            const year = date.getFullYear();

                            const customYearLabel = customYearLabels?.[year];

                            return (
                                customYearLabel ??
                                year + (estimatedYears.includes(year) ? " (e)" : "")
                            );
                        },
                        label: function (tooltipItems) {
                            return (
                                tooltipItems.dataset.label +
                                ": " +
                                new Intl.NumberFormat("fr-FR").format(
                                    tooltipItems.raw.y
                                )
                            );
                        },
                    },
                },
                annotation: {
                    clip: false,
                    annotations: annotations,
                },
            },

            width: this.widthGraph,
            height: this.heightGraph,
            devicePixelRatio: configData.chartsDevicePixelRatio,
            elements: {
                point: {
                    radius: 0,
                },
            },
            animation: {
                duration: 0,
            },
        };

        let chartEvolConsoSecteur = "";

        if (this.state.dataTraj) {
            if (
                this.state.dataTraj["analyse"][categorie] &&
                this.state.dataTraj["analyse"][categorie].traj !== "Confidentiel"
            ) {
                chartEvolConsoSecteur = (
                    <div>
                        <div className="block-row">
                            <Bar
                                width={chartOptionsSuivi.width}
                                height={chartOptionsSuivi.height}
                                options={chartOptionsSuivi}
                                data={consoData[categorie]}
                            />
                        </div>
                        {estimatedRatioYearsLegend ? (
                            <p className="warning-ratio-year">
                                {estimatedRatioYearsLegend}
                            </p>
                        ) : (
                            ""
                        )}
                    </div>
                );
            } else if (
                this.state.dataTraj["analyse"][categorie] &&
                this.state.dataTraj["analyse"][categorie].traj === "Confidentiel"
            ) {
                chartEvolConsoSecteur = (
                    <div className={"confid-chart"}>Données confidentielles</div>
                );
            } else if (!this.state.dataTraj["analyse"][categorie]) {
                chartEvolConsoSecteur = (
                    <div className={"confid-chart"}>
                        Catégorie non disponible pour cet indicateur, veuillez contacter
                        l'administrateur régional.
                    </div>
                );
            }
        }

        return (
            <div>
                <div className="charts-legende" style={{ width: "101%" }}>
                    {chartEvolConsoSecteur}
                    <div className="choisir-methode elem-courbes-empilees">
                        {legendeFinale}
                    </div>
                </div>
            </div>
        );
    }
}

export default BarGraph;
