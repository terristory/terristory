/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React, { useState, useEffect } from "react";

import Api from "../../Controllers/Api";
import { buildRegionUrl } from "../../utils.js";

import config from "../../settings.js";
import IndicatorHistoryLineBarChart from "./IndicatorHistoryLineBarChart";
import MultiSelectLegend from "./MultiSelectLegend";
import LoadingOverlay from "../Utils/LoadingOverlay";

/**
 *
 * @import {LegendOption} from './MultiSelectLegend'
 * @import {IndicatorHistoryDataApiResponse} from '../../types/apiTypes'
 * @import {HistoryChartOptions} from '../../types/apiTypes'
 * @import {IndicatorCategoriesValues} from '../../Controllers/types.js'
 * @import {IndicatorData} from './IndicatorHistoryLineBarChart'
 */

/**
 * Callback for handling filter changes.
 * @callback OnFilterChange
 * @param {Object} filterChange
 * @param {string} filterChange.category - The name of the category being filtered.
 * @param {Set<string>} filterChange.enable - The list of options to enable for the category.
 * @param {Set<string>} filterChange.disable - The list of options to disable for the category.
 * @returns {void}
 */

/**
 *
 * @param {object} props
 * @param {any} props.parentApi
 * @param {number} props.indicatorId
 * @param {string} props.zoneType
 * @param {string} props.zoneId
 * @param {string} props.zoneSubdivision
 * @param {string} props.category
 * @param {string} props.title
 * @param {HistoryChartOptions} [props.chartOptions]
 * @param {IndicatorCategoriesValues|undefined} [props.initialFilters] - initial filters value for uncontrolled mode
 * @param {IndicatorCategoriesValues|undefined} [props.filters] - Controlled filters
 * @param {OnFilterChange} [props.onFilterChange] - Callback for handling filter changes.
 * @param {number|undefined} [props.unitId]
 *
 * @returns {JSX.Element}
 */
function IndicatorHistoryRepresentation({
    parentApi,
    indicatorId,
    zoneType,
    zoneId,
    zoneSubdivision,
    category,
    title,
    chartOptions,
    initialFilters,
    filters: controlledFilters,
    onFilterChange: controlledOnFilterChange,
    unitId,
}) {
    if (controlledFilters && controlledFilters[category] === undefined) {
        throw Error(
            "The filters don't match with the chartConfig. The filter and current indicator states might be out of sync ?"
        );
    }

    const uncontrolledInitialFilters =
        initialFilters ??
        parentApi.controller.analysisManager.initFiltersByCategory(indicatorId);

    const [uncontrolledFilters, setUncontrolledFilters] = useState(
        uncontrolledInitialFilters
    );

    const filters = controlledFilters ?? uncontrolledFilters;

    /** @type {OnFilterChange} updatedFilters */
    const uncontrolledOnFilterChange = (updatedFilters) => {
        setUncontrolledFilters((prev) => {
            return {
                ...prev,
                [updatedFilters.category]: Array.from(updatedFilters.enable).map(
                    (option) => {
                        return {
                            filtre_categorie: `${updatedFilters.category}.${option}`,
                        };
                    }
                ),
            };
        });
    };
    /** @type {OnFilterChange} */
    const onFilterChange = controlledOnFilterChange ?? uncontrolledOnFilterChange;

    const [legendOptions, setLegendOptions] = useState(
        /** @type {LegendOption[] | null} */ (null)
    );
    const [indicatorData, setIndicatorData] = useState(
        /** @type {IndicatorData | null} */ (null)
    );

    const [isFetchingData, setIsFetchingData] = useState(false);

    // Get custom year labels from indicator config
    const indicatorConfig = parentApi.controller.analysisManager.analysis.find(
        (a) => a.id === indicatorId
    );
    let customYearLabels = indicatorConfig?.representation_details?.customYearLabels;

    if (customYearLabels) {
        try {
            customYearLabels = JSON.parse(customYearLabels);
        } catch {
            customYearLabels = undefined;
        }
    }

    /**
     * Fetches the history data.
     *
     * This component is responsible for independently fetching it's own data.
     */
    useEffect(() => {
        setIsFetchingData(true);

        /**
         * @param {IndicatorHistoryDataApiResponse} data
         * @returns {{ indicatorData: IndicatorData, legendOptions: LegendOption[] }}
         */
        const processIndicatorDataResponse = (data) => {
            const analyse = data.analyse || {};
            const categoryData = analyse[category]?.traj || [];
            const categories = analyse[category]?.categories || [];

            const legendOptions = categories.map(({ nom, couleur, confidentiel }) => {
                /** @type {LegendOption} */
                const option = {
                    value: nom,
                    label: nom,
                    color: couleur,
                    disabled: confidentiel === "oui",
                };
                return option;
            });

            /** @type {IndicatorData} */
            const indicatorData = {
                unit: data.unite,
                categories: [
                    {
                        name: category,
                        categories: categoryData.map((optionData) => {
                            return {
                                name: optionData.nom,
                                data: optionData.data.map(
                                    ({ annee: year, valeur: value }) => ({
                                        year,
                                        value,
                                    })
                                ),
                                color: optionData.couleur,
                                confidential: optionData.confidentiel === "oui",
                            };
                        }),
                    },
                ],
            };

            return {
                indicatorData,
                legendOptions,
            };
        };

        const endpointUrl =
            config.api_analysis_meta_url + `${indicatorId}/graphique/bar`;
        const queryParams = new URLSearchParams({
            zone: zoneType,
            maille: zoneSubdivision,
            zone_id: zoneId.toString(),
        });
        if (unitId) {
            queryParams.append("unit_id", unitId.toString());
        }

        const url = `${endpointUrl}?${queryParams.toString()}`;
        const body = JSON.stringify(filters);

        const apiPromise = Api.callApi(
            buildRegionUrl(url, parentApi.data.region),
            body,
            "POST"
        );

        apiPromise
            .then((/** @type {IndicatorHistoryDataApiResponse} */ data) => {
                const { indicatorData, legendOptions } =
                    processIndicatorDataResponse(data);
                setIndicatorData(indicatorData);
                setLegendOptions((prevLegendOptions) => {
                    // Only set the legend the first time.
                    // because legend options are based on API response and response
                    // doesn't contain options when no checkboxes are selected (maybe fix the API?)
                    if (!prevLegendOptions) {
                        return legendOptions;
                    }
                    return prevLegendOptions;
                });
                setIsFetchingData(false);
            })
            .catch((error) => {
                if (error.name === "AbortError") return;
                setIsFetchingData(false);
                throw error;
            });

        return () => apiPromise.abort();
    }, [
        zoneType,
        zoneId,
        zoneSubdivision,
        filters,
        indicatorId,
        unitId,
        parentApi.data.region,
        category,
    ]);

    /**
     * @param {object} params
     * @param {string} params.categoryName
     * @param {Set<LegendOption["value"]>} params.enable
     * @param {Set<LegendOption["value"]>} params.disable
     */
    const handleFilterChange = ({ categoryName, enable, disable }) => {
        if (enable.intersection(disable).size > 0) {
            throw new Error(
                `Error: enable and disable sets cannot have common elements. Overlapping values: ${enable.intersection(
                    disable
                )}`
            );
        }
        onFilterChange({
            category: categoryName,
            enable: enable,
            disable: disable,
        });
    };

    const LineBarChartOptions = {
        ...(title && { title }),
        ...(chartOptions?.type && { type: chartOptions.type }),
        ...(chartOptions?.stacked && { stacked: chartOptions.stacked }),
        ...(customYearLabels && { customYearLabels }),
    };

    return (
        <div className="d-flex flex-wrap align-items-center gap-3">
            {indicatorData ? (
                <>
                    <LoadingOverlay isLoading={isFetchingData}>
                        <IndicatorHistoryLineBarChart
                            indicatorData={indicatorData}
                            chartOptions={LineBarChartOptions}
                        ></IndicatorHistoryLineBarChart>
                    </LoadingOverlay>
                    {/*
                        We don't display the legend when there is only one option.
                        This behavior allows a workaround where a fake category with a single option
                        can be created to display the historical data for the total of an indicator.
                        This workaround is needed because there is a strong dependency between indicator categories
                        and the charts displayed in the bottom panel (through the meta.charts table).
                        Ideally categories and charts would be defined independently.
                        */}
                    {legendOptions.length > 1 && (
                        <MultiSelectLegend
                            options={legendOptions}
                            selectedOptions={
                                new Set(
                                    filters[category].map(
                                        (
                                            /** @type {{ filtre_categorie: string; }} */ f
                                        ) => {
                                            return f.filtre_categorie.split(".")[1];
                                        }
                                    )
                                )
                            }
                            onChange={(selectedOptions) => {
                                handleFilterChange({
                                    categoryName: category,
                                    enable: selectedOptions,
                                    disable: new Set(
                                        legendOptions.map((o) => o.value)
                                    ).difference(selectedOptions),
                                });
                            }}
                        ></MultiSelectLegend>
                    )}
                </>
            ) : (
                <>
                    <span
                        className="spinner-border spinner-border"
                        role="status"
                        aria-hidden="true"
                    ></span>
                    <span className="visually-hidden">Loading...</span>
                </>
            )}
        </div>
    );
}

export default IndicatorHistoryRepresentation;
