/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React, { useEffect } from "react";
import ConfigurableLineBarChart from "./ConfigurableLineBarChart";

/**
 * @typedef {Object} IndicatorData
 * @property {string} unit
 * @property {Category[]} categories
 */

/**
 * @typedef {Object} Category
 * @property {string} name
 * @property {SubCategory[]} categories
 */

/**
 * @typedef {Object} SubCategory
 * @property {string} name
 * @property {string} color
 * @property {boolean} confidential
 * @property {DataPoint[]} data
 */

/**
 * @typedef {Object} DataPoint
 * @property {number} year
 * @property {number} value
 */

/**
 * @typedef {Object} IndicatorChartOptions
 * @property {"line"|"bar"} [type='line']
 * @property {boolean} [stacked=false]
 * @property {string} [title='']
 * @property {boolean} [enableEvolution=false]
 * @property {number} [evolutionRefYear]
 * @property {Object.<number, string>} [customYearLabels]
 */

/**
 *
 * - If time scale :
 *   - format years correctly
 *   - add (e) tick callback
 *
 * - If linear scale
 *
 * - If category scale
 *
 *  - format each category data as a chartjs dataset
 *
 *  - add title to chart if specified
 *  - add annotations if evolution
 *
 *
 *
 *  - determine the correct options for ConfigurableLineBarChart (axis type, stacked...)
 *
 *
 *
 * Render the chart with the formatted data using ConfigurableLineBarChart
 *
 * @param {Object} props
 * @param {IndicatorData} props.indicatorData
 * @param {IndicatorChartOptions} [props.chartOptions]
 *
 * @returns {JSX.Element} Chart component.
 */
function IndicatorHistoryLineBarChart({ indicatorData, chartOptions = {} }) {
    // TODO : Add all the options and parameters for the Bar/Line Chart.
    // - units.

    const defaultOptions = {
        title: "",
        type: "bar",
        stacked: true,
        enableEvolution: false,
        customYearLabels: undefined,
    };

    const chartOptionsWithDefaults = { ...defaultOptions, ...chartOptions };

    const unit = indicatorData.unit;

    // Format for the Chart Component.
    const chartData = {
        datasets: indicatorData.categories[0].categories.map((categoryData) => {
            return {
                backgroundColor: categoryData.color,
                label: [categoryData.name],
                data: categoryData.data.map(({ year, value: y }) => {
                    return { x: new Date(year, 0, 1), y };
                }),
            };
        }),
    };

    /** @type {import("chart.js").ChartOptions} */
    let chartJSOptions = {
        plugins: {
            legend: {
                display: false,
            },
            tooltip: {
                mode: "index",
                intersect: false,
                callbacks: {
                    title: function (tooltipItems) {
                        const year = new Date(tooltipItems[0].label)
                            .getFullYear()
                            .toString();

                        return (
                            chartOptionsWithDefaults?.customYearLabels?.[year] ?? year
                        );
                    },
                    afterTitle: function (tooltipItems) {
                        const total = tooltipItems.reduce(
                            (sum, item) => sum + item.raw.y,
                            0
                        );
                        const formattedTotal = new Intl.NumberFormat("fr-FR").format(
                            total
                        );
                        return `Total: ${formattedTotal} ${unit}`;
                    },
                    label: function (tooltipItems) {
                        return `${tooltipItems.dataset.label}: ${new Intl.NumberFormat(
                            "fr-FR"
                        ).format(tooltipItems.raw.y)} ${unit}`;
                    },
                },
            },
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: "Année",
                },
                ticks: {
                    source: "data",
                    includeBounds: true,
                    autoSkip: false,
                    callback: (value) => {
                        // Format year for ticks and apply custom labels
                        const year = new Date(value).getFullYear();

                        const customYearLabel =
                            chartOptionsWithDefaults.customYearLabels?.[year]?.split(
                                " "
                            );

                        return customYearLabel ?? year.toString();
                    },
                },
            },
            y: {
                title: {
                    display: true,
                    text: unit ? `en ${unit}` : "",
                },
            },
        },
        animation: {
            duration: 0,
        },
    };
    return (
        <ConfigurableLineBarChart
            type={chartOptionsWithDefaults.type}
            data={chartData}
            options={chartJSOptions}
            title={chartOptionsWithDefaults.title}
            xScaleType="time"
            stacked={chartOptionsWithDefaults.stacked}
        ></ConfigurableLineBarChart>
    );
}

export default IndicatorHistoryLineBarChart;
