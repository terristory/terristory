/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { useState, useEffect, useId } from "react";
import { Link } from "react-router-dom";

import Api from "../Controllers/Api";
import config from "../settings";
import { buildRegionUrl, exportToCSV } from "../utils";
import TableContainer from "./Utils/TableContainer";
import { MainZoneSelect } from "./SelectionObjet";

const numberFormat = Intl.NumberFormat("fr-FR", { maximumSignificantDigits: 4 });
const integerFormat = Intl.NumberFormat("fr-FR", { maximumFractionDigits: 0 });
const percentFormat = Intl.NumberFormat("fr-FR", { maximumFractionDigits: 2 });

export default function PcaetAggregation({ parentApi, connected }) {
    const { region, zone, currentZone } = parentApi.data;
    const { analysisManager } = parentApi.controller;

    const [pcaetList, setPcaetList] = useState([]);
    const [methodIsRelative, setMethodRelative] = useState(false);
    const [years, setYears] = useState({
        2021: false,
        2026: false,
        2030: false,
        2050: false,
    });
    const [trajectories, setTrajectories] = useState({});
    const [demarche, setDemarche] = useState("");
    const [modalities, setModalities] = useState(undefined);
    const [activeSirens, setActiveSirens] = useState({});
    const setAllActiveSirens = (bool) => {
        setActiveSirens((old) =>
            Object.fromEntries(Object.keys(old).map((siren) => [siren, bool]))
        );
    };
    const [isLoading, setIsLoading] = useState(false);
    const [status, setStatus] = useState({});

    function setYearActivation(year, active) {
        setYears((years) => ({ ...years, [year]: active }));
    }
    function setTrajectoryActivation(traj, active) {
        setTrajectories((trajectories) => ({
            ...trajectories,
            [traj]: { ...trajectories[traj], active },
        }));
    }
    function setModalityActivation(id, active) {
        setModalities((categories) => ({
            ...categories,
            [id]: { ...categories[id], active },
        }));
    }

    function exportTableToCSV(data) {
        let label = "Export_PCAET";
        if (zone?.zone && currentZone) label += `_${zone.zone}_${currentZone}`;
        exportToCSV(data, "csv", label);
    }

    useEffect(() => {
        // Load trajectories
        Api.callApi(buildRegionUrl(config.api_pcaet_trajectories_list, region)).then(
            (trajs) =>
                setTrajectories(
                    Object.fromEntries(
                        trajs.map(({ id, ...traj }) => [id, { ...traj, active: false }])
                    )
                )
        );
    }, [region]);

    useEffect(() => {
        // Load data
        setIsLoading(true);
        let url =
            buildRegionUrl(config.api_all_pcaet, region) +
            `?relative_pcaet=${methodIsRelative}`;
        if (zone?.zone) url += `&zone_type=${zone.zone}`;
        if (currentZone) url += `&zone_id=${currentZone}`;
        let promise = Api.callApi(url);
        promise
            .then((pcaets) => {
                setPcaetList(pcaets);
                setActiveSirens(
                    Object.fromEntries(pcaets.map(({ siren }) => [siren, true]))
                );
                setIsLoading(false);
                setStatus({});
            })
            .catch((error) => {
                if (error.name === "AbortError") return;
                setIsLoading(false);
                setStatus({ type: "error", message: error.message });
            });
        return () => promise.abort();
    }, [region, methodIsRelative, zone, currentZone]);

    useEffect(() => {
        (async () => {
            // Init category filter
            const activeTrajs = Object.keys(trajectories).filter(
                (key) => trajectories[key].active
            );
            let activeCategories = activeTrajs.map(
                (key) => Object.keys(trajectories[key].categories)[0]
            );
            activeCategories = [...new Set(activeCategories)];
            if (activeCategories.length === 1) {
                let cat = await analysisManager.getCategory(activeCategories[0]);
                cat = Object.fromEntries(
                    cat.map(({ modalite, modalite_id }) => [
                        modalite_id,
                        { name: modalite, active: true },
                    ])
                );
                setModalities(cat);
            } else {
                setModalities(undefined);
            }
        })();
    }, [trajectories, analysisManager]);

    const totalCheckbox = useId();
    useEffect(() => {
        // indeterminate property cannot be set through attributes, so we need an effect
        const indeterminate =
            Object.values(activeSirens).some((a) => a) &&
            !Object.values(activeSirens).every((a) => a);
        const checkbox = document.getElementById(totalCheckbox);
        if (checkbox) checkbox.indeterminate = indeterminate;
    });

    const territory = pcaetList.find(({ siren }) => siren === "");
    const cellWithPercent = (cell, accessorKey, formatter) => {
        if (cell?.getValue() == null) return null;
        const value = filterAndSum(cell.getValue(), modalities);
        let percent = null;
        if (cell.row.original.nom === "Total" && territory?.[accessorKey]) {
            const divisor = filterAndSum(territory[accessorKey], modalities);
            if (divisor !== 0) {
                percent = (value / divisor) * 100;
                percent = <i title={percent}>({percentFormat.format(percent)} %)</i>;
            }
        }
        return (
            <>
                <output title={value}>{formatter.format(value)}</output> {percent}
            </>
        );
    };
    const filterAndSum = (val, filter) => {
        if (!(val instanceof Object)) {
            return val;
        }
        if (filter) {
            return Object.entries(val)
                .filter(([key, _]) => filter[key]?.active)
                .map(([_, value]) => value)
                .reduce((a, b) => a + b, 0);
        }
        return Object.values(val).reduce((a, b) => a + b, 0);
    };
    let columns = [
        {
            id: "active",
            cell: ({ row }) => {
                if (row.original.nom === "Total") {
                    return (
                        <input
                            id={totalCheckbox}
                            type="checkbox"
                            checked={Object.values(activeSirens).some((a) => a)}
                            onChange={(e) => setAllActiveSirens(e.target.checked)}
                        />
                    );
                }
                if (!row.original.siren) {
                    return null;
                }
                return (
                    <input
                        type="checkbox"
                        checked={activeSirens[row.original.siren]}
                        onChange={(e) =>
                            setActiveSirens((old) => ({
                                ...old,
                                [row.original.siren]: e.target.checked,
                            }))
                        }
                    />
                );
            },
        },
        { header: "Nom du territoire", accessorKey: "nom" },
        { header: "SIREN", accessorKey: "siren" },
        {
            header: "Nombre d’EPCI",
            accessorKey: "epci_nb",
            cell: (cell) => cellWithPercent(cell, "epci_nb", integerFormat),
        },
        {
            header: "Population",
            accessorKey: "population",
            cell: (cell) => cellWithPercent(cell, "population", integerFormat),
        },
        { header: "Démarche", accessorKey: "demarche" },
    ];
    const activeTrajectories = Object.entries(trajectories)
        .filter(([_, { active }]) => active)
        .map(([traj, _]) => traj);
    const activeYears = Object.entries(years)
        .filter(([_, active]) => active)
        .map(([year, _]) => year);

    for (const trajectory of activeTrajectories) {
        const { unit, name, obs_year } = trajectories[trajectory];
        columns.push({
            header: `${name} observée ${obs_year} (${unit})`,
            accessorKey: `${trajectory}_obs`,
            cell: (cell) => cellWithPercent(cell, `${trajectory}_obs`, numberFormat),
        });
        for (const year of activeYears) {
            columns.push({
                header: `${name} ${year} (${unit})`,
                accessorKey: `${trajectory}_${year}`,
                cell: ({ cell }) => {
                    if (cell?.getValue() == null) return null;
                    const value = filterAndSum(cell.getValue(), modalities);
                    return <output title={value}>{numberFormat.format(value)}</output>;
                },
            });
        }
    }

    let displayedPcaetList = pcaetList.filter((pcaet) => {
        if (pcaet.siren === "") {
            return false;
        }
        if (demarche !== "" && pcaet.demarche !== demarche) {
            return false;
        }
        for (const trajectory of activeTrajectories) {
            if (pcaet[`${trajectory}_obs`] === undefined) {
                return false;
            }
            for (const year of activeYears) {
                if (pcaet[`${trajectory}_${year}`] === undefined) {
                    return false;
                }
            }
        }
        return true;
    });

    const territoryRow = {};
    const total = { nom: "Total" };
    for (const col of columns) {
        if (["active"].includes(col.id)) continue;
        territoryRow[col.accessorKey] = territory?.[col.accessorKey];
        if (["siren", "nom", "demarche"].includes(col.accessorKey)) continue;
        total[col.accessorKey] = displayedPcaetList
            .filter((row) => activeSirens[row.siren])
            .map((row) => row[col.accessorKey])
            .map((val) => filterAndSum(val, modalities))
            .reduce((val1, val2) => val1 + val2, 0);
    }
    displayedPcaetList = [territoryRow, total, ...displayedPcaetList];

    let catFilterName = "";
    if (modalities && activeTrajectories.length) {
        const trajectory = trajectories[activeTrajectories[0]];
        const category = Object.values(trajectory.categories)[0];
        catFilterName = category.titre.replace(/^[Pp]ar /, "");
        catFilterName = catFilterName.charAt(0).toUpperCase() + catFilterName.slice(1);
    }
    const modalityInput = ([id, { active, name }]) => (
        <label key={id}>
            <input
                type="checkbox"
                defaultChecked={active}
                onChange={(e) => setModalityActivation(id, e.target.checked)}
            />{" "}
            {name}
            <br />
        </label>
    );

    let content = null;
    const { userInfos } = parentApi.controller.authManager;
    if (!connected || userInfos?.profil !== "admin") {
        content = <p>Page non accessible.</p>;
    } else if (
        ![undefined, "region", "departement"].includes(parentApi.data?.zone?.zone)
    ) {
        // TODO: paramétrer dans regions_configuration ?
        content = (
            <p>
                Module non accessible pour ce territoire. Veuillez en choisir un autre.
            </p>
        );
    } else {
        content = (
            <>
                <div>
                    <label className="switch">
                        <input
                            type="checkbox"
                            id="method-switch"
                            defaultChecked={methodIsRelative}
                            onChange={(e) => setMethodRelative(e.target.checked)}
                        />
                        <span className="slider round" />
                    </label>{" "}
                    <label htmlFor="method-switch">
                        Méthode de calcul en pourcentage par rapport à l'annnée de
                        référence
                    </label>
                </div>
                <h3>Filtrer par…</h3>
                <form className="pcaet-aggregation-filter">
                    <fieldset>
                        Type de trajectoire :
                        {Object.entries(trajectories).map(([id, { active, name }]) => (
                            <label key={id}>
                                <input
                                    type="checkbox"
                                    defaultChecked={active}
                                    onChange={(e) =>
                                        setTrajectoryActivation(id, e.target.checked)
                                    }
                                />{" "}
                                {name}
                                <br />
                            </label>
                        ))}
                    </fieldset>
                    <fieldset>
                        Années d'objectif :
                        {["2021", "2026", "2030", "2050"].map((year) => (
                            <label key={year}>
                                <input
                                    type="checkbox"
                                    defaultChecked={years[year]}
                                    onChange={(e) =>
                                        setYearActivation(year, e.target.checked)
                                    }
                                />{" "}
                                {year}
                                <br />
                            </label>
                        ))}
                    </fieldset>
                    <fieldset>
                        Démarche :
                        <label>
                            <input
                                type="checkbox"
                                checked={demarche === "Obligée"}
                                onChange={(e) =>
                                    setDemarche(e.target.checked ? "Obligée" : "")
                                }
                            />{" "}
                            Obligée
                        </label>
                        <label>
                            <input
                                type="checkbox"
                                checked={demarche === "Volontaire"}
                                onChange={(e) =>
                                    setDemarche(e.target.checked ? "Volontaire" : "")
                                }
                            />{" "}
                            Volontaire
                        </label>
                    </fieldset>
                    {modalities && (
                        <fieldset>
                            {catFilterName} :
                            {Object.entries(modalities).map(modalityInput)}
                        </fieldset>
                    )}
                </form>
                {isLoading ? (
                    <div className="loader" role="status" />
                ) : (
                    <>
                        <p className="export-data-button-in-popup">
                            <button
                                className="tsbtn info"
                                onClick={() => exportTableToCSV(displayedPcaetList)}
                            >
                                Exporter le contenu de la table en csv
                            </button>
                        </p>
                        <TableContainer
                            data={displayedPcaetList}
                            columns={columns}
                            tableClassName="table-striped"
                            disablePagination={true}
                            getTrProps={(row) => {
                                return row.original.nom === "Total"
                                    ? {
                                          fontWeight: "bold",
                                          borderBottom: "solid 1px #999",
                                      }
                                    : !row.original.siren
                                      ? { fontWeight: "bold" }
                                      : undefined;
                            }}
                            getCellProps={(cell) => {
                                return activeSirens[cell.row.original.siren] === false
                                    ? { color: "#666" }
                                    : undefined;
                            }}
                        />
                    </>
                )}
            </>
        );
    }

    return (
        <div className="widgets full-screen-widget">
            <Link className="back-to-map" to={"/" + parentApi.data.urlPartageable}>
                <span className="big">&times;</span> <span>Retour à la carte</span>
            </Link>
            <div className="ui-main">
                <h1 className="tstitle left">Agrégation des PCAET</h1>
                <MainZoneSelect
                    parentApi={parentApi}
                    className="mb-4"
                    containerClassName=""
                    callback={({ zoneType, zoneMaille, zoneId }) =>
                        parentApi.callbacks.mettreAJourParametresUrls(
                            `?zone=${zoneType}&maille=${zoneMaille}&zone_id=${zoneId}`
                        )
                    }
                />
                {content}
                {status?.type === "raw" ? (
                    status.message
                ) : status?.type === "error" ? (
                    <div className="alert alert-warning">{status.message}</div>
                ) : status?.type === "success" ? (
                    <div className="alert alert-success">{status.message}</div>
                ) : null}
            </div>
        </div>
    );
}
