/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import { Link } from "react-router-dom";

import Api from "../Controllers/Api";
import { buildRegionUrl, saveAsPng, createPdfMethodoLink } from "../utils";
import FabriqueRepresentation from "./FabriqueRepresentation";
import { ExportIndicatorButton } from "./ExportButton";

import config from "../settings";

/**
 * This function is used to display the pollutant emissions tracking graphs
 */
class SuiviPolluants extends React.Component {
    constructor(props) {
        super(props);

        // Get data from API
        this.state = {
            parameters: {},
            pollutantId: undefined,
            pollutantName: undefined,
            identifiantIndicateurEmiPolluant: false,
            passageTable: false,
        };
    }

    componentDidMount() {
        // pollutant tracking selected by the user and retrieved in this.props
        let suiviPolluant = this.props.parentApi.data.currentMenu;

        // Get the data for the graph
        this.getParametersFromApi(suiviPolluant);
    }

    getParametersFromApi(suiviPolluant) {
        const url = buildRegionUrl(
            config.api_analysis_page_parameters,
            this.props.parentApi.data.region
        ).replace("#page#", suiviPolluant);
        Api.callApi(url).then((json) => {
            if (!json || !json.parameters || !json.passage_table) {
                return;
            }
            let pZone = "?zone=" + this.props.parentApi.data.zone.zone;
            let pMaille = "&maille=" + this.props.parentApi.data.zone.maille;
            let pZoneId = "&zone_id=" + this.props.parentApi.data.currentZone;
            let nomTerritoire =
                "&nom_territoire=" +
                encodeURI(
                    this.props.parentApi.controller.zonesManager.getZoneName(
                        this.props.parentApi.data.currentZone,
                        this.props.parentApi.data.zone
                    )
                );
            this.props.parentApi.callbacks.mettreAJourParametresUrls(
                pZone + pMaille + pZoneId + nomTerritoire
            );

            // name of the table containing the information on the pollutant in DB
            let idPolluant;
            // pollutant name
            let nomPolluant;
            // set of conditions that determines the content of the idPolluant and nomPolluant variables according to pollutant tracking
            if (suiviPolluant === "suivi_polluants_covnm") {
                idPolluant = "DataSet.POLLUTANT_COVNM";
                nomPolluant = "COVNM";
            } else if (suiviPolluant === "suivi_polluants_nh3") {
                idPolluant = "DataSet.POLLUTANT_NH3";
                nomPolluant = "NH3";
            } else if (suiviPolluant === "suivi_polluants_nox") {
                idPolluant = "DataSet.POLLUTANT_NOX";
                nomPolluant = "NOx";
            } else if (suiviPolluant === "suivi_polluants_pm10") {
                idPolluant = "DataSet.POLLUTANT_PM10";
                nomPolluant = "PM10";
            } else if (suiviPolluant === "suivi_polluants_pm25") {
                idPolluant = "DataSet.POLLUTANT_PM25";
                nomPolluant = "PM2.5";
            } else if (suiviPolluant === "suivi_polluants_so2") {
                idPolluant = "DataSet.POLLUTANT_SOX";
                nomPolluant = "SO2";
            }
            this.setState({
                parameters: json.parameters,
                passageTable: json.passage_table,
                identifiantIndicateurEmiPolluant:
                    this.props.parentApi.controller.analysisManager.obtenirIdentifiantAnalyse(
                        json.passage_table[idPolluant].name
                    ),
                pollutantId: json.passage_table[idPolluant].name,
                pollutantName: nomPolluant,
            });
        });
    }

    render() {
        if (this.state.identifiantIndicateurEmiPolluant === false) {
            return <></>;
        }

        // get data for the graph
        const urlPartageable = "";

        // reference to the tag containing the graph
        let refEvolEmiSecteur = React.createRef();
        let classTheme = this.props.parentApi.data.theme;
        let source_suivi_traj = JSON.parse(
            this.props.parentApi.data.settings["source_suivi_traj"]
        );
        let zoneName = this.props.parentApi.controller.zonesManager.getZoneName(
            this.props.parentApi.data.currentZone,
            this.props.parentApi.data.zone
        );
        // territory name
        let title = zoneName;
        if (!title) {
            title = this.props.parentApi.data.nomTerritoire;
        }

        // chart parameters
        let width = 620;
        let height = 450;

        let chartEvoEmiSecteur = (
            <FabriqueRepresentation
                // FabriqueRepresentation instantiates the representation specified in the representation property
                // Here, we instantiate a CourbesEmpilees component
                parentApi={this.props.parentApi}
                id_analysis={this.state.identifiantIndicateurEmiPolluant}
                representation={"line"}
                type={{ categorie: "secteur", titre: "Par secteur" }}
                provenance="suivi_energetique"
                width={width}
                height={height}
                id={1}
            />
        );

        const getPdfSaveButtons = (id_analysis, ref, nom) => {
            return (
                <div className="selection-graph bouton-methodo do-not-print">
                    <button
                        id="buttons"
                        type="button"
                        className="btn btn-info btn-save block-row bouton-flex liste-modalites-diagramme"
                        onClick={() => {
                            saveAsPng(ref, nom);
                        }}
                    >
                        <span>PNG</span>
                    </button>
                    <ExportIndicatorButton
                        parentApi={this.props.parentApi}
                        analysisId={id_analysis}
                    />
                    <a
                        target="_blank"
                        rel="noreferrer"
                        href={createPdfMethodoLink(
                            config.methodo_url,
                            this.props.parentApi.data.region,
                            this.props.parentApi.controller.analysisManager.getAnalysisMethodoPdf(
                                parseInt(id_analysis, 10)
                            )
                        )}
                    >
                        <div className="pdf"></div>
                    </a>
                </div>
            );
        };

        return (
            <div
                className="suivi-energetique widgets full-screen-widget"
                id="suivi-energetique"
            >
                <Link
                    className="back-to-map"
                    to={"/" + this.props.parentApi.data.urlPartageable}
                >
                    <button
                        type="button"
                        className="close close-big"
                        data-dismiss="alert"
                        aria-label="Close"
                        onClick={() => {
                            this.props.parentApi.callbacks.mettreAJourParametresUrls(
                                urlPartageable
                            );
                            this.props.parentApi.callbacks.updateCurrentMenu("analyse");
                        }}
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </Link>
                <div
                    className={"page-content " + classTheme}
                    id="suivi-energetique-content"
                >
                    <div className="title centered-row">
                        <label className="suivi-energetique-title">
                            Suivi de la trajectoire de {this.state.pollutantName}
                        </label>
                        <label className="suivi-energetique-territoire">
                            Territoire : {title}
                        </label>
                    </div>

                    <h3>Évolution des émissions de {this.state.pollutantName}</h3>
                    <div align="center"></div>
                    <div>
                        <div ref={refEvolEmiSecteur}>
                            <h4>Par secteur :</h4>
                            {getPdfSaveButtons(
                                this.state.identifiantIndicateurEmiPolluant,
                                refEvolEmiSecteur,
                                "Évolution des émissions de " + this.state.pollutantName
                            )}
                            {chartEvoEmiSecteur}
                            <div className="chart-legend">
                                {source_suivi_traj["source"]}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SuiviPolluants;
