/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { act } from "react";
import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";

import PcaetAggregation from "../Components/PcaetAggregation";
import config from "../settings";
import { buildRegionUrl } from "../utils";
import Api from "../Controllers/Api";

vi.mock("../Controllers/Api.js");
vi.mock("../utils.js");
vi.mock("../Components/SelectionObjet.jsx");

const parentApi = {
    data: {
        region: "pytest",
        callbacks: {
            updateConnected: (c) => "updateConnected",
        },
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        authManager: {
            getMe: async (_) => false,
        },
        analysisManager: {
            getCategory: () => [{ modalite: "mod", modalite_id: 1 }],
        },
    },
};

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(config.api_pcaet_trajectories_list, "pytest"),
        results: [
            {
                id: "traj",
                name: "traj",
                unit: "unit",
                categories: { cat: { category: "cat", titre: "Meow", visible: true } },
            },
        ],
    },
    {
        url: buildRegionUrl(config.api_all_pcaet, "pytest"),
        results: [
            {
                siren: "1",
                nom: "Territoire_A",
                epci_nb: 1,
                demarche: "Volontaire",
                population: 12,
                traj_obs: { 1: 37 },
                traj_2026: { 1: 137 },
            },
            {
                siren: "2",
                nom: "Territoire_B",
                epci_nb: 1,
                demarche: "Obligée",
                population: 24,
                traj_obs: { 1: 53 },
                traj_2030: { 1: 153 },
            },
        ],
    },
    {
        url: buildRegionUrl(config.api_siren_assignment, "pytest"),
        results: [],
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the PCAET aggregation page for non connected user", async () => {
    const { getByText } = render(
        <MemoryRouter>
            <PcaetAggregation parentApi={parentApi} connected={false} />
        </MemoryRouter>
    );
    expect(getByText("Page non accessible.")).toBeInTheDocument();
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the PCAET aggregation page for non admin connected user", async () => {
    parentApi.controller.authManager.userInfos = { profil: "utilisateur" };
    const { getByText } = render(
        <MemoryRouter>
            <PcaetAggregation parentApi={parentApi} connected={true} />
        </MemoryRouter>
    );
    expect(getByText("Page non accessible.")).toBeInTheDocument();
});

/**
 * Check that the page is available for admin user.
 */
test("renders the PCAET aggregation page for admin connected user", async () => {
    parentApi.controller.authManager.userInfos = { profil: "admin" };
    const { getByText, getAllByRole } = render(
        <MemoryRouter>
            <PcaetAggregation parentApi={parentApi} connected={true} />
        </MemoryRouter>
    );
    expect(getByText("Agrégation des PCAET")).toBeInTheDocument();
    await waitFor(async () => {
        expect(getAllByRole("checkbox", { name: "traj" })).toHaveLength(1);
        expect(getByText("Territoire_A")).toBeInTheDocument();
    });
});

/**
 * Check that the user can filter on trajectory, year and "Démarche"
 */
test("lets the user filter the PCAET aggregation table", async () => {
    parentApi.controller.authManager.userInfos = { profil: "admin" };
    const { getByText, getAllByText, queryByText, getAllByRole } = render(
        <MemoryRouter>
            <PcaetAggregation parentApi={parentApi} connected={true} />
        </MemoryRouter>
    );

    async function checkAndClick(role, name) {
        let element = screen.queryAllByRole(role, { name });
        expect(element).toHaveLength(1);
        element = element[0];
        await act(async () => void fireEvent.click(element));
    }

    await waitFor(async () => {
        expect(getAllByRole("checkbox", { name: "traj" })).toHaveLength(1);
        expect(getByText("Territoire_A")).toBeInTheDocument();
    });
    expect(getByText("Territoire_B")).toBeInTheDocument();
    expect(queryByText("traj 2026 (unit)")).toBeNull();
    await checkAndClick("checkbox", "traj");
    expect(getByText(37 + 53)).toBeInTheDocument(); // sum of observed values is shown
    await checkAndClick("checkbox", "2026");
    expect(getByText("Territoire_A")).toBeInTheDocument();
    expect(queryByText("Territoire_B")).toBeNull(); // Territoire_B was filtered out
    expect(getByText("traj 2026 (unit)")).toBeInTheDocument(); // column traj 2026 was added
    expect(getAllByText(137)).toHaveLength(2); // value of traj 2026 is shown twice (territory + total)
});
