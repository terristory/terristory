/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";

import config from "../settings";
import { buildRegionUrl } from "../utils";
import Api from "../Controllers/Api";
import FactorySimulator from "../Components/Simulator/FactorySimulator";
import data_simulator from "./data/data.json";

vi.mock("../Controllers/Api.js");
vi.mock("../utils.js");
vi.mock("../Components/Simulator/RenewableEnergy/SimulatorEnR");

// we simulate the parentApi context
const parentApi = {
    data: {
        region: "pytest",
        zone: {
            maille: "epci",
            zone: "region",
        },
        currentZone: "84",
        settings: {
            label: "pytest",
            seo: [
                {
                    region: "auvergne-rhone-alpes",
                    page: "simulator",
                    meta_title: "Main page title",
                    meta_description: "Meta description",
                },
            ],
        },
    },
    controller: {
        zonesManager: {
            getZoneName: () => "",
        },
    },
    callbacks: {},
};
parentApi.callbacks.mettreAJourParametresUrls = vi.fn();
const MOCK_SCENARIOS = data_simulator;

const MOCK_URLS_RESULTS = [
    {
        url:
            buildRegionUrl(config.api_simulator, parentApi.data.region).replace(
                "#type#",
                "mobility"
            ) +
            "?zone=region" +
            "&maille=epci" +
            "&zone_id=84",
        results: MOCK_SCENARIOS,
    },
    {
        url:
            buildRegionUrl(config.api_simulator, parentApi.data.region).replace(
                "#type#",
                "mobility"
            ) +
            "?zone=region" +
            "&maille=epci" +
            "&zone_id=14",
        results: {},
    },
];

let wrapper;
beforeEach(() => {
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
    wrapper = undefined;
    // create meta and title
    const meta = document.createElement("meta");
    document.head.appendChild(meta);
    const title = document.createElement("title");
    document.head.appendChild(title);
});

test("renders the page for " + parentApi.data.region, async () => {
    // we render
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <FactorySimulator type="mobility" parentApi={parentApi} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    // Check that "mettreAJourParametresUrls" is called
    expect(parentApi.callbacks.mettreAJourParametresUrls).toBeCalled();

    // Check if the title is displayed
    await waitFor(() => {
        // Check seo configuration
        expect(document.querySelector("meta[name='description']").content).toBe(
            "Meta description"
        );
        expect(document.querySelector("title").innerHTML).toBe("Main page title");
        expect(screen.queryByText("Simulateur d’impacts mobilité")).toBeInTheDocument();
    });

    // Check if "objective" text is displayed
    expect(screen.queryByText("Objectif :")).toBeInTheDocument();
    expect(
        screen.queryByText(
            "donner les ordres de grandeur d’impacts associés aux principaux leviers de la mobilité"
        )
    ).toBeInTheDocument();
    // check if the region is displayed
    expect(screen.queryByText("[Territoire : pytest]")).toBeInTheDocument();
    // Check if territorial strategy button is displayed
    const buttonPull = screen.getByRole("link", {
        name: "Prêt pour construire votre plan d'actions « mobilité » ?",
    });
    expect(buttonPull).toBeInTheDocument();
    // Check if the link to territorial strategy works
    expect(buttonPull).toHaveAttribute("href", "/strategies_territoriales");

    // Check if the button to return to the dashboard is displayed
    expect(
        screen.queryByText("Retour au tableau de bord Mobilité")
    ).not.toBeInTheDocument();
});

test("check if checkboxes are displayed and working", async () => {
    // we render
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <FactorySimulator type="mobility" parentApi={parentApi} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });
    expect(parentApi.callbacks.mettreAJourParametresUrls).toBeCalled();
    // Category lever
    // check if category lever checkbox are displayed and functional
    const activatedCategoryLever = [
        "Leviers pour « moins se déplacer »",
        "Leviers pour « se déplacer autrement »",
    ];
    for (let category of activatedCategoryLever) {
        const buttonPull = screen.getByRole("checkbox", { name: category });
        expect(buttonPull).toBeInTheDocument();
        await act(async () => {
            fireEvent.click(buttonPull);
        });
    }
    // check if the last category is not displayed
    expect(
        screen.queryByText("Leviers pour « se déplacer plus efficacement »")
    ).not.toBeInTheDocument();

    // levers
    // check if activated levers are displayed
    const activatedLevers = [
        "Leviers de réduction – trajets domicile-travail",
        "Leviers de réduction – déplacements personnels hors travail",
    ];
    for (let lever of activatedLevers) {
        expect(screen.getByText(lever)).toBeInTheDocument();
    }

    // check if disabled levers are not displayed
    const disabledLevers = [
        "Leviers de substitution – passage aux modes actifs",
        "Leviers d’amélioration – véhicules particuliers",
        "Leviers d’amélioration – bus et cars",
    ];
    for (let lever of disabledLevers) {
        expect(screen.queryByText(lever)).not.toBeInTheDocument();
    }

    // actions
    // check if activated actions checkboxs are displayed and functional
    const activatedActions = [
        "Réduire le nombre d'actifs devant se déplacer",
        "Réduire le nombre de km parcourus par trajet",
        "Réduire le nombre de km à parcourir pour accéder aux services & loisirs",
    ];
    for (let action of activatedActions) {
        const buttonPull = screen.getByRole("checkbox", { name: action });
        expect(buttonPull).toBeInTheDocument();
        await act(async () => {
            fireEvent.click(buttonPull);
        });
    }

    // check if disabled actions checkboxs are not displayed
    const disabledAction = [
        "Evolution part modale vélo/marche à pied - trajets < 2km",
        "Changement de motorisation des véhicules particuliers",
        "Changement de motorisation des bus et cars",
    ];
    for (let action of disabledAction) {
        expect(screen.queryByText(action)).not.toBeInTheDocument();
    }

    // check if activated impacts are displayed and functional
    const data_impact = data_simulator["metadata"]["impacts"];
    for (let i in data_impact) {
        const impactCheckBox = screen.getByRole("checkbox", {
            name: data_impact[i].impactTitle + " (en " + data_impact[i].unit + ") :",
        });
        expect(impactCheckBox).toBeInTheDocument();
        await act(async () => {
            fireEvent.click(impactCheckBox);
        });
    }
});

test("check if sliders are displayed and working", async () => {
    // we render
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <FactorySimulator type="mobility" parentApi={parentApi} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });
    expect(parentApi.callbacks.mettreAJourParametresUrls).toBeCalled();

    // check if all sliders are displayed
    const sliders = screen.getAllByRole("slider");
    sliders.forEach((slider) => expect(slider).toBeInTheDocument());
    // TODO : check if sliders are functional
    //fireEvent.change(sliders[0], { target: { value: 25 } });
});

test("renders the error page : missing data for " + parentApi.data.region, async () => {
    const parentApiMissingData = JSON.parse(JSON.stringify(parentApi));
    parentApiMissingData.callbacks.mettreAJourParametresUrls = vi.fn();
    parentApiMissingData.data.currentZone = "14";
    // we render
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <FactorySimulator
                        type="mobility"
                        parentApi={parentApiMissingData}
                    />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    expect(parentApi.callbacks.mettreAJourParametresUrls).toBeCalled();
    expect(document.querySelector("meta[name='description']")).not.toBeInTheDocument();

    // Check if error page is displayed
    await waitFor(() => {
        expect(
            screen.queryByText(
                "Désolé, l'application rencontre actuellement un problème :"
            )
        ).toBeInTheDocument();
    });
});
