/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { act } from "react";
import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";

import Profile from "../Components/Profile";
import config from "../settings";
import { buildRegionUrl } from "../utils";
import Api from "../Controllers/Api";

vi.mock("../Controllers/Api.js");
vi.mock("../utils.js");
vi.mock("chartjs-adapter-moment", () => {
    return {};
});

const parentApi = {
    data: {
        region: "pytest",
        callbacks: {
            updateConnected: (c) => "updateConnected",
        },
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        authManager: {
            getMe: async (_) => false,
        },
        zonesManager: {
            getZoneName: () => "",
        },
    },
};

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(config.api_liste_donnees_registre, "pytest"),
        results: {
            liste_tables_rte: [{ nom: "table1", jeu_de_donnees: "table1" }],
            liste_tables_configuree: [],
        },
    },
    {
        url: buildRegionUrl(
            config.api_liste_colonnes_donnees_registre,
            "pytest"
        ).replace("#table#", "table1"),
        results: {
            liste_colonnes: [],
            valeurs_par_colonnes: [],
            donnees_finales: {},
            liste_tables_dispo: [],
        },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

test("renders the Profile page from non connected user", async () => {
    const connected = false;

    // we render
    const { container } = render(
        <MemoryRouter>
            <Profile parentApi={parentApi} connected={connected} />
        </MemoryRouter>
    );

    // Check that user-specific content is not rendered
    expect(screen.queryByText("Mon compte")).not.toBeInTheDocument();
    expect(screen.queryByText("Infos personnelles")).not.toBeInTheDocument();
});

test("renders the Profile page from connected user", async () => {
    const connected = true;
    parentApi.controller.authManager = {
        getMe: async (_) => true,
        userInfos: {
            id: "test@terristory-nowhere.dev",
            prenom: "John",
            nom: "Doe",
            organisation: "ExampleOrganization",
            fonction: "ExampleFunction",
            code_postal: "01001",
            profil: "utilisateur",
            territoire_predilection: '{"zoneType": "region", "zoneMaille": "epci"}',
        },
    };

    // we render
    const { getByText } = render(
        <MemoryRouter>
            <Profile parentApi={parentApi} connected={connected} />
        </MemoryRouter>
    );

    // we check that the titles are present
    const titleValue = getByText("Mon compte");
    expect(titleValue).toBeInTheDocument();
    const personalTab = getByText("Infos personnelles");
    expect(personalTab).toBeInTheDocument();

    // Check that all the user's infos are present
    expect(getByText("test@terristory-nowhere.dev")).toBeInTheDocument();
    expect(getByText("John")).toBeInTheDocument();
    expect(getByText("Doe")).toBeInTheDocument();
    expect(getByText("ExampleOrganization")).toBeInTheDocument();
    expect(getByText("ExampleFunction")).toBeInTheDocument();
    expect(getByText("01001")).toBeInTheDocument();
});

test("renders the Profile page from admin user", async () => {
    const connected = true;
    parentApi.controller.authManager = {
        getMe: async (_) => true,
        userInfos: {
            id: "test@terristory-nowhere.dev",
            prenom: "John",
            nom: "Doe",
            organisation: "ExampleOrganization",
            fonction: "ExampleFunction",
            code_postal: "01001",
            profil: "admin",
            territoire_predilection: '{"zoneType": "region", "zoneMaille": "epci"}',
        },
    };

    // we render
    const { getByText } = render(
        <MemoryRouter>
            <Profile parentApi={parentApi} connected={connected} />
        </MemoryRouter>
    );

    // we check that the titles are present
    const titleValue = getByText("Administration");
    expect(titleValue).toBeInTheDocument();

    async function checkAndClick(role, name) {
        let element = screen.queryAllByRole(role, { name });
        expect(element).toHaveLength(1);
        element = element[0];
        await act(async () => void fireEvent.click(element));
    }

    await checkAndClick("tab", "Indicateurs, données");
    await checkAndClick("tab", "Données du registre");
    await waitFor(async () => {
        expect(getByText("Gestion des données du registre")).toBeInTheDocument();
    });
    await checkAndClick("button", "Configurer");
    let element = screen.queryAllByRole("tab", { name: "Configurer table1" });
    // If this test doesn't pass, it probably is because you added or removed a tab.
    // Update the id of the configuration tab in the Profile component
    expect(element).toHaveLength(1);
});
