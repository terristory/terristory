/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";
import { HelmetProvider } from "react-helmet-async";

import OpenSource from "../../Components/StaticPages/OpenSource";
import config from "../../settings";
import Api from "../../Controllers/Api";

vi.mock("../../utils.js");
vi.mock("../../Controllers/Api.js");

beforeEach(() => {
    // create meta and title tags to be sure they are present in final page
    const meta = document.createElement("meta");
    document.head.appendChild(meta);
    const title = document.createElement("title");
    document.head.appendChild(title);
});

const mockConfigUrl = config;

const MOCK_API_DEFINITIONS = {
    content:
        'Tous les changements notables sont <a rel="noreferrer" target="_blank" href="https://gitlab.com/terristory/terristory/-/blob/prod-2024.12/CHANGELOG.md"> document\u00e9s ici</a>.',
    last_version: "2000.00",
    last_version_date: "2000-00-00",
};
const MOCK_URLS_RESULTS = [
    {
        url: mockConfigUrl.changelog_url,
        results: MOCK_API_DEFINITIONS,
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

test("renders the open source page", async () => {
    // simulate the parentApi context
    const parentApi = {
        data: {
            region: "auvergne-rhone-alpes",
            settings: {
                seo: [
                    {
                        region: "auvergne-rhone-alpes",
                        page: "open-source",
                        meta_title: "Main page title",
                        meta_description: "Meta description",
                    },
                ],
            },
        },
        controller: {
            gestionSuiviConsultations: {
                idUtilisateur: -1,
            },
        },
    };

    // render
    render(
        <MemoryRouter>
            <HelmetProvider>
                <OpenSource parentApi={parentApi} />
            </HelmetProvider>
        </MemoryRouter>
    );

    // check that meta and title were changed
    await waitFor(() => {
        expect(document.querySelector("meta[name='description']").content).toBe(
            "Meta description"
        );
        expect(document.querySelector("title").innerHTML).toBe("Main page title");
        expect(
            screen.getByText(`version ${MOCK_API_DEFINITIONS["last_version"]}`)
        ).toBeInTheDocument();
        expect(
            screen.getByRole("link", { name: "ici pour le code source" })
        ).toHaveAttribute(
            "href",
            `https://gitlab.com/terristory/terristory/-/tree/prod-${MOCK_API_DEFINITIONS["last_version"]}`
        );
    });
});
