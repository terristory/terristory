/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import { render, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";

import LegalInformation from "../../Components/StaticPages/LegalInformation";
import { HelmetProvider } from "react-helmet-async";

vi.mock("../../utils.js");

beforeEach(() => {
    // create meta and title tags to be sure they are present in final page
    const meta = document.createElement("meta");
    document.head.appendChild(meta);
    const title = document.createElement("title");
    document.head.appendChild(title);
});

test("renders the Legal notice page", async () => {
    // simulate the parentApi context
    const parentApi = {
        data: {
            region: "auvergne-rhone-alpes",
            regionLabel: "Name of the region",
            settings: {
                contact_email: "fake-contact-email@terristory.fr",
                seo: [
                    {
                        region: "auvergne-rhone-alpes",
                        page: "legal-notice",
                        meta_title: "Main page title",
                        meta_description: "Meta description",
                    },
                ],
            },
        },
        controller: {
            gestionSuiviConsultations: {
                idUtilisateur: -1,
            },
        },
    };

    // render
    render(
        <MemoryRouter>
            <HelmetProvider>
                <LegalInformation parentApi={parentApi} />
            </HelmetProvider>
        </MemoryRouter>
    );

    // check that meta and title were changed
    await waitFor(() => {
        expect(document.querySelector("meta[name='description']").content).toBe(
            "Meta description"
        );
        expect(document.querySelector("title").innerHTML).toBe("Main page title");
    });
});
