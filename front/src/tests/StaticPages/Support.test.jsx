/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Support from "../../Components/StaticPages/Support";

vi.mock("../../utils.js");

import { consulteAutrePage } from "../../utils";
const regions = ["auvergne-rhone-alpes", "occitanie", "nouvelle-aquitaine"];
let nbConsult = 0;

regions.forEach((region) => {
    test("renders the page for " + region, async () => {
        // we simulate the parentApi context
        const parentApi = {
            data: {
                region: region,
            },
            controller: {
                gestionSuiviConsultations: {
                    idUtilisateur: -1,
                },
            },
        };
        const connected = true;
        // we render
        const { container, rerender } = render(
            <MemoryRouter>
                <Support parentApi={parentApi} connected={connected} />
            </MemoryRouter>
        );

        // checking the title is correct
        expect(container).toHaveTextContent(
            "Bienvenue dans la page support aux utilisateurs de l'outil TerriSTORY®."
        );
        expect(consulteAutrePage.mock.calls.length).toEqual(nbConsult);

        // we force the update cycle to make sure componentDidUpdate was called
        rerender(
            <MemoryRouter>
                <Support parentApi={parentApi} connected={connected} />
            </MemoryRouter>
        );
        expect(consulteAutrePage.mock.calls.length).toEqual(++nbConsult);

        // only test for specific pages which have full elements, other pages are much simpler
        if (region === "occitanie" || region === "auvergne-rhone-alpes") {
            // if we have links to pdf, open one and check that we did call the back
            let links = screen.getAllByRole("link");
            expect(links.length).toBeGreaterThanOrEqual(2);

            fireEvent.click(links[1]);
            expect(consulteAutrePage.mock.calls.length).toEqual(++nbConsult);
        }
    });
});
