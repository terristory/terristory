/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React from "react";
import { render, screen, waitFor, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";

import { ZoneSelect } from "../Components/SelectionObjet";
import Zones from "../Controllers/Zones";
import config from "../settings";
import { buildRegionUrl } from "../utils";
import Api from "../Controllers/Api";

vi.mock("../Controllers/Api.js");
vi.mock("../utils.js");

// Warning: This file also tests Controllers/Zones.js instead of mocking it

const DOWN_ARROW = { keyCode: 40 };

const parentApi = {
    data: {
        region: "pytest",
        settings: {},
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
    },
};

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(config.api_area_url, "pytest") + "region",
        results: [{ code: "1", nom: "Pytest", communes: ["00001", "00002"] }],
    },
    {
        url: buildRegionUrl(config.api_area_url, "pytest") + "commune",
        results: [
            { code: "00001", nom: "Le Puy-en-Velu", communes: ["00001"] },
            { code: "00002", nom: "Bourg-en-Brosse", communes: ["00002"] },
        ],
    },
    {
        url: buildRegionUrl(config.api_area_url, "pytest"),
        results: [
            { libelle: "Région - commune", nom: "region", maille: "commune", order: 1 },
            { libelle: "Commune", nom: "commune", maille: "commune", order: 2 },
        ],
    },
];

beforeEach(async () => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
    // Create the zonesManager
    parentApi.controller.zonesManager = await new Promise((resolve, reject) => {
        try {
            const zonesManager = new Zones(() => resolve(zonesManager), "pytest");
        } catch (e) {
            reject(e);
        }
    });
});

test("renders the ZoneSelect component", async () => {
    const callback = vi.fn();
    render(<ZoneSelect onSelect={callback} parentApi={parentApi} />);
    expect(callback).not.toHaveBeenCalled();

    const comboboxes = screen.getAllByRole("combobox");
    expect(comboboxes).toHaveLength(3);
    expect(comboboxes[0]).toHaveValue("");
    expect(comboboxes[0]).toHaveTextContent("Échelle");
    expect(comboboxes[0]).toHaveTextContent("Région");
    expect(comboboxes[0]).toHaveTextContent("Commune");

    const selectedOptions = screen.getAllByRole("option", { selected: true });
    expect(selectedOptions).toHaveLength(2);
    expect(selectedOptions[0]).toHaveTextContent("Échelle");
    expect(selectedOptions[1]).toHaveTextContent("Maille");
});

test("selects Région - maille commune", async () => {
    const callback = vi.fn();
    render(<ZoneSelect onSelect={callback} parentApi={parentApi} />);
    const comboboxes = screen.getAllByRole("combobox");
    expect(comboboxes).toHaveLength(3);

    // Select scale "Région"
    fireEvent.change(comboboxes[0], { target: { value: "region" } });
    await waitFor(() => {
        const selectedOptions = screen.getAllByRole("option", { selected: true });
        expect(selectedOptions).toHaveLength(2);
        expect(selectedOptions[0]).toHaveTextContent("Région");
        expect(screen.getByText("Pytest")).toBeInTheDocument();
        expect(selectedOptions[1]).toHaveTextContent("Maille");
    });
    expect(callback).not.toHaveBeenCalled();

    // Select maille "commune"
    fireEvent.change(comboboxes[2], { target: { value: "commune" } });
    await waitFor(() => {
        const selectedOptions = screen.getAllByRole("option", { selected: true });
        expect(selectedOptions).toHaveLength(2);
        expect(selectedOptions[1]).toHaveTextContent("commune");
    });
    expect(callback).toHaveBeenCalledTimes(1);
    expect(callback).toHaveBeenCalledWith({
        zoneType: "region",
        zoneMaille: "commune",
        zoneId: "1",
    });
});

test("selects commune - maille commune", async () => {
    const callback = vi.fn();
    render(<ZoneSelect onSelect={callback} parentApi={parentApi} />);
    let comboboxes = screen.getAllByRole("combobox");
    expect(comboboxes).toHaveLength(3);

    // Select scale "commune"
    fireEvent.change(comboboxes[0], { target: { value: "commune" } });
    await waitFor(() => {
        const selectedOptions = screen.getAllByRole("option", { selected: true });
        expect(selectedOptions).toHaveLength(2);
        expect(selectedOptions[0]).toHaveTextContent("Commune");
        expect(selectedOptions[1]).toHaveTextContent("Commune");
    });
    expect(callback).not.toHaveBeenCalled();

    comboboxes = screen.getAllByRole("combobox");
    expect(comboboxes[1]).not.toHaveTextContent("Le Puy-en-Velu");

    // Select commune #00001 (based on https://stackoverflow.com/questions/55575843/how-to-test-react-select-with-react-testing-library)
    fireEvent.keyDown(comboboxes[1], DOWN_ARROW);
    await waitFor(() => expect(screen.getByText("Le Puy-en-Velu")).toBeInTheDocument());
    fireEvent.click(screen.getByText("Le Puy-en-Velu"));
    expect(callback).toHaveBeenCalledTimes(1);
    expect(callback).toHaveBeenCalledWith({
        zoneType: "commune",
        zoneMaille: "commune",
        zoneId: "00001",
    });
});
