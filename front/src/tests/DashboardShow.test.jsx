/*
* TerriSTORY®
*
© Copyright 2022 AURA-EE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* A copy of the GNU Affero General Public License should be present along
* with this program at the root of current repository. If not, see
* http://www.gnu.org/licenses/.
*/

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import DashboardShow from "../Components/DashboardShow";
import config from "../settings";
import { buildRegionUrl } from "../utils";
import Api from "../Controllers/Api";
import { HelmetProvider } from "react-helmet-async";

vi.mock("../Controllers/Api.js");

vi.mock("jspdf", () => ({
    default: vi.fn(() => ({
        internal: {
            pageSize: {
                getWidth: vi.fn(),
            },
        },
        addImage: vi.fn(),
        addPage: jest.fn(),
        save: vi.fn(),
    })),
}));
vi.mock("html2canvas", () => ({
    default: vi.fn(() =>
        Promise.resolve({
            toDataURL: () => "IMG DATA",
        })
    ),
}));
vi.mock("../Components/FabriqueRepresentation.jsx");

vi.mock("../utils.js");

vi.mock("react-chartjs-2", () => ({
    Chart: {
        register: () => null,
    },
}));

vi.mock("chartjs-plugin-datalabels", () => {
    return {};
});

const parentApi = {
    data: {
        region: "pytest",
        zone: {
            zone: "epci",
            maille: "commune",
        },
        currentZone: "111",
        nomTerritoire: "Nowhere",
        settings: {
            // mocking general Notes page metadata
            cesba_notes: "<div id='part1'>Notes page header</div>",
            cesba_elements: JSON.stringify({
                titre: "Notes description giving information",
                pied: "Nothing in the footer",
                "cesba-logo": [
                    { "logo-entete": "logo.png", "logo-pied": "footer-logo.png" },
                ],
            }),
            seo: [
                {
                    region: "auvergne-rhone-alpes",
                    page: "simulator",
                    meta_title: "Main page title",
                    meta_description: "Meta description",
                },
            ],
        },
        urlPartageable: "/",
        regionReferencement: null,
    },
    callbacks: {
        updateProvenance: vi.fn(),
        mettreAJourParametresUrls: vi.fn(),
        chargerTableauBord: vi.fn(),
        updateAnalysis: vi.fn(),
        tailleDiv: vi.fn(),
    },
    controller: {
        zonesManager: {
            zones: [{ nom: "region", maille: "epci", libelle: "Région - maille EPCI" }],
            zonesInNewFormat: {
                region: { label: "Région", mailles: { epci: { label: "EPCI" } } },
            },
            getZoneName: () => "Unknown zone",
            getZoneLevelName: () => "Unknown zone level name",
            getZoneList: () => [{ code: "1", label: "Test Region" }],
        },
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        equipementsManager: {
            getEquipementsLayers: vi.fn(),
        },
        dashboardManager: {
            listMyDashboards: vi.fn(),
        },
        analysisManager: {
            analysis: [
                { id: 43, nom: "Indicator #43", display_total: true },
                { id: 38, nom: "Indicator #38", display_total: true },
                { id: 36, nom: "Indicator #36", display_total: true },
                { id: 23, nom: "Indicator #23", display_total: true },
                { id: 23, nom: "Indicator #23", display_total: true },
                { id: 46, nom: "Indicator #46", display_total: true },
            ],
            getAnalysisLastYear: vi.fn(),
            getAnalysisEstimatedYears: vi.fn(),
            getAnalysisNameTheme: vi.fn(),
            getAnalysisMethodoPdf: vi.fn(),
            getUnitsForAnalysis: vi.fn(),
            getUnitParamsForIndicator: () => {
                return { relevantUnits: [], defaultUnitForThisZoneType: "1" };
            },
            getOnlyForZones: vi.fn(),
            getDisabledZones: vi.fn(),
            getDisabledMacroLevels: vi.fn(),
            isDisabledInDashboard: vi.fn().mockImplementation(() => false),
            isActive: vi.fn(),
        },
    },
};
const mockConfigUrl = config;

const MOCK_STATS_TYPE = {
    id: 5,
    titre: "Indicateurs cles",
    description:
        "Ce tableau de bord, a travers quelques  indicateurs incontournables, propose une vision synthetique des enjeux de transition du territoire. \nPour aller plus loin dans l'analyse, n'hesitez pas a consulter le detail des indicateurs dans le menu Indicateurs. ",
    date: "2021-02-18 07:21",
    thematiques: [
        {
            id: 1412,
            titre: "Cadre de vie",
            description: "",
            graphiques: [
                {
                    numero_analyse: "7",
                    id_analysis: 43,
                    representation: "jauge",
                    categories: {},
                },
                {
                    numero_analyse: "8",
                    id_analysis: 38,
                    representation: "jauge-circulaire",
                    categories: {},
                },
                {
                    numero_analyse: "9",
                    id_analysis: 36,
                    representation: "pie",
                    categories: {
                        secteur_clap5: {
                            categorie: "secteur_clap5",
                            titre: "Repartition en 5 secteurs",
                            visible: true,
                        },
                        secteur_clap17: {
                            categorie: "secteur_clap17",
                            titre: "Repartition en 17 secteurs",
                            visible: false,
                        },
                    },
                },
            ],
            ordre: null,
        },
        {
            id: 1413,
            titre: "Enjeux energetiques",
            description: "",
            graphiques: [
                {
                    numero_analyse: "4",
                    id_analysis: 23,
                    representation: "jauge",
                    categories: {
                        type_prod_enr: {
                            categorie: "type_prod_enr",
                            titre: "Par filiere de production",
                            visible: true,
                        },
                    },
                },
                {
                    numero_analyse: "5",
                    id_analysis: 23,
                    representation: "courbes_croisees",
                    categories: {
                        type_prod_enr: {
                            categorie: "type_prod_enr",
                            titre: "Par filiere de production",
                            visible: true,
                        },
                    },
                },
                {
                    numero_analyse: "6",
                    id_analysis: 46,
                    representation: "line",
                    categories: {
                        type_prod_enr: {
                            categorie: "type_prod_enr",
                            titre: "Par filiere de production",
                            visible: true,
                        },
                    },
                },
            ],
            ordre: null,
        },
    ],
    user_table: true,
};
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url:
            buildRegionUrl(mockConfigUrl.tableau_bord_url, "pytest") +
            "/1?zone=" +
            parentApi.data.zone.zone +
            "&zone_id=" +
            parentApi.data.currentZone,
        results: MOCK_STATS_TYPE,
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_consultation_tableau_bord_url, "pytest"),
        results: [],
    },
];

beforeEach(() => {
    vi.clearAllMocks();
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

test("renders the module without a right table", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <DashboardShow parentApi={parentApi} tableauBordCourant={0} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    expect(wrapper).toHaveTextContent(
        "Ce tableau n'existe pas, n'a pas été publié ou encore n'est pas accessible à cette échelle."
    );
});

test("renders the module with a right table", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <DashboardShow parentApi={parentApi} tableauBordCourant={1} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    // Two calls => one for consultation + one for retrieving the table afterward
    expect(Api.callApi.mock.calls).toHaveLength(2);

    // Check the content of the request
    expect(Api.callApi.mock.calls[0][0]).toBe(MOCK_URLS_RESULTS[0].url);

    expect(
        screen.getByRole("heading", { name: MOCK_STATS_TYPE.titre })
    ).toBeInTheDocument();

    for (let groupId in MOCK_STATS_TYPE.thematiques) {
        const group = MOCK_STATS_TYPE.thematiques[groupId];
        expect(screen.getByRole("heading", { name: group.titre })).toBeInTheDocument();
    }
});

test("compare with another territory", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <DashboardShow parentApi={parentApi} tableauBordCourant={1} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    const compareButton = screen.getByRole("button", {
        name: "Comparer avec un autre territoire",
    });

    // we check we have only one indicator displayed
    expect(screen.getAllByRole("heading", { name: "Indicator #46" })).toHaveLength(1);

    // we check we don't have the "add territory" comparison button yet
    expect(screen.queryByRole("button", { name: "Ajouter" })).not.toBeInTheDocument();
    expect(
        screen.getByRole("heading", { name: "Territoire : Unknown zone" })
    ).toBeInTheDocument();

    act(() => {
        fireEvent.click(compareButton);
    });

    // now we can add a territory
    expect(screen.getByRole("button", { name: "Ajouter" })).toBeInTheDocument();

    const addButton = screen.getByRole("button", {
        name: "Ajouter",
    });

    act(() => {
        // Select first item of territory select
        let territorySelect = screen.getAllByRole("combobox")[0];
        fireEvent.change(territorySelect, { target: { value: "region" } });
    });
    act(() => {
        // Select first item of territory select
        let mailleSelect = screen.getAllByRole("combobox")[2];
        fireEvent.change(mailleSelect, { target: { value: "epci" } });
    });
    act(() => {
        fireEvent.click(addButton);
    });

    // add button should have disappeared
    expect(screen.queryByRole("button", { name: "Ajouter" })).not.toBeInTheDocument();

    // now we have two territories
    expect(
        screen.getByRole("heading", { name: "1 - Unknown zone" })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("heading", { name: "2 - Unknown zone level name X" })
    ).toBeInTheDocument();

    // now we have same indicator twice
    expect(screen.getAllByRole("heading", { name: "Indicator #46" })).toHaveLength(2);

    // we try adding the same territory another time => shouldn't work

    // we check we don't have the "add territory" comparison button yet
    expect(screen.queryByRole("button", { name: "Ajouter" })).not.toBeInTheDocument();
    act(() => {
        fireEvent.click(compareButton);
    });

    // now we can add a territory
    expect(screen.getByRole("button", { name: "Ajouter" })).toBeInTheDocument();

    const newAddButton = screen.getByRole("button", {
        name: "Ajouter",
    });

    act(() => {
        // Select first item of territory select
        let territorySelect = screen.getAllByRole("combobox")[0];
        fireEvent.change(territorySelect, { target: { value: "region" } });
    });
    act(() => {
        // Select first item of territory select
        let territorySelect = screen.getAllByRole("combobox")[2];
        fireEvent.change(territorySelect, { target: { value: "epci" } });
    });
    act(() => {
        fireEvent.click(newAddButton);
    });

    // add button is still here
    expect(screen.getByRole("button", { name: "Ajouter" })).toBeInTheDocument();

    // now we have two territories
    expect(
        screen.getByRole("heading", { name: "1 - Unknown zone" })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("heading", { name: "2 - Unknown zone level name X" })
    ).toBeInTheDocument();

    // now we have same indicator twice
    expect(screen.getAllByRole("heading", { name: "Indicator #46" })).toHaveLength(2);
});

test("compare with other territories from URL", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <DashboardShow
                        parentApi={{
                            ...parentApi,
                            data: {
                                ...parentApi.data,
                                urlPartageable:
                                    "?zone=epci&maille=commune&zone_id=200072015&id_tableau=1&nom_territoire=CA Annonay Rhône Agglo&c0Zone=region&c0Maille=epci&c0ZoneId=84&c1Zone=epci&c1Maille=commune&c1ZoneId=200072015",
                            },
                        }}
                        tableauBordCourant={1}
                    />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("button", {
            name: "Comparer avec un autre territoire",
        })
    ).not.toBeInTheDocument();

    // we check we have the same indicator displayed three time once we have selected three territories from URL
    expect(screen.getAllByRole("heading", { name: "Indicator #46" })).toHaveLength(3);
});

test("print PDF", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <DashboardShow parentApi={parentApi} tableauBordCourant={1} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("button", {
            name: "Télécharger PDF",
        })
    ).toBeInTheDocument();

    const printPdfButton = screen.getByRole("button", {
        name: "Télécharger PDF",
    });

    act(() => {
        fireEvent.click(printPdfButton);
    });

    // TODO: mock window.print and check that it was called
});

test("print PNG", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <DashboardShow parentApi={parentApi} tableauBordCourant={1} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    expect(
        screen.queryAllByRole("button", {
            name: "PNG",
        })
    ).toHaveLength(2);

    const printPNGButtons = screen.getAllByRole("button", {
        name: "PNG",
    });

    act(() => {
        fireEvent.click(printPNGButtons[0]);
    });
});

test("edit table", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <HelmetProvider>
                    <DashboardShow parentApi={parentApi} tableauBordCourant={1} />
                </HelmetProvider>
            </MemoryRouter>
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("button", {
            name: "Modifier le tableau",
        })
    ).toBeInTheDocument();

    const editButton = screen.getByRole("button", {
        name: "Modifier le tableau",
    });

    act(() => {
        fireEvent.click(editButton);
    });
});
