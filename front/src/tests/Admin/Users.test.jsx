/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";

import UsersManage from "../../Components/Admin/UsersManage";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

vi.mock("../../Components/Admin/UserEdition.jsx", () => ({
    default: vi.fn(() => <div data-testid="user-edition">this is mock component</div>),
}));
const grecaptcha = {
    reset: vi.fn(),
};
Object.defineProperty(window, "grecaptcha", {
    value: grecaptcha,
});

const parentApi = {
    data: {
        region: "pytest",
        settings: {
            label: "PYTEST region",
        },
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        authManager: {
            userInfos: {
                territoire_predilection: {
                    zoneType: "region",
                    zoneMaille: "epci",
                    zoneId: "84",
                },
            },
        },
        zonesManager: {
            zones: [{ nom: "region", maille: "epci", libelle: "region-epci" }],
            zonesInNewFormat: {
                region: { label: "Région", mailles: { epci: { label: "EPCI" } } },
            },
            getZoneList: (d) => [{ code: "1", label: "Test Region" }],
        },
    },
    callbacks: {},
};
const mockConfigUrl = config;
config.DISABLE_CAPTCHA = true;

const MOCK_API_DEFINITIONS = [
    {
        mail: "example@test.com",
        prenom: "Alice",
        nom: "Foe",
        organisation: "Test",
        fonction: "Engineer",
        territoire: "Lyon",
        territoire_predilection:
            '{"zoneType":"region","zoneMaille":"epci","zoneId":"84"}',
        utiliser_territoire_predilection: false,
        code_postal: "69100",
        profil: "utilisateur",
        publication: true,
        actif: true,
        connexion: 10,
        derniere_connexion: null,
        region: "auvergne-rhone-alpes",
        acces_indicateurs_desactives: null,
        global_admin: false,
    },
    {
        mail: "second_example@test.com",
        prenom: "Bob",
        nom: "Bar",
        organisation: "Test",
        fonction: "Engineer",
        territoire: "Lyon",
        territoire_predilection:
            '{"zoneType":"region","zoneMaille":"epci","zoneId":"84"}',
        utiliser_territoire_predilection: false,
        code_postal: "69100",
        profil: "admin",
        publication: true,
        actif: false,
        connexion: 0,
        derniere_connexion: null,
        region: "auvergne-rhone-alpes",
        acces_indicateurs_desactives: null,
        global_admin: false,
    },
];
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(mockConfigUrl.users_url, "pytest"),
        results: MOCK_API_DEFINITIONS,
    },
    {
        url: buildRegionUrl(
            mockConfigUrl.user_delete_url + MOCK_API_DEFINITIONS[0]["mail"],
            "pytest"
        ),
        results: { status: "success", message: "Success!" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.user_deactivate_url, "pytest"),
        results: { status: "success", message: "Success!" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.user_activate_url, "pytest"),
        results: { status: "success", message: "Success!" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.create_user_admin_url, "pytest"),
        results: { message: "success", email_status: "ok" },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

afterEach(() => {
    vi.clearAllMocks();
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    const { container } = render(
        <UsersManage parentApi={parentApi} connected={connected} />
    );
    expect(container).toHaveTextContent("");
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <UsersManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields (title, two date inputs, one select object that
 * show both types mocked).
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <UsersManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    // we check the title is correctly displayed

    expect(screen.getByRole("heading")).toHaveTextContent("Gestion des utilisateurs");

    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    // we check we do have the two API defined
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS[0].mail })
    ).toBeInTheDocument();
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS[1].mail })
    ).toBeInTheDocument();
});

/**
 * Try showing data details from popup
 */
test("try showing details popup", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <UsersManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check we don't have yet the user edition form (mocked here)
    expect(screen.queryByTestId("user-edition")).not.toBeInTheDocument();

    const editButtons = screen.getAllByRole("button", {
        name: "Modifier",
    });
    expect(editButtons).toHaveLength(MOCK_API_DEFINITIONS.length);

    // we hit pull button
    await act(async () => {
        fireEvent.click(editButtons[0]);
    });

    expect(screen.getByTestId("user-edition")).toBeInTheDocument();
});

/**
 * Check that it is possible to enable a user
 */
test("try disabling a user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <UsersManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const disableButtons = screen.queryAllByRole("button", {
        name: "Désactiver",
    });
    expect(disableButtons).toHaveLength(1);
    const disableButton = disableButtons[0];
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    await act(async () => {
        fireEvent.click(disableButton);
    });

    expect(Api.callApi).toHaveBeenNthCalledWith(
        1,
        buildRegionUrl(config.users_url, parentApi.data.region),
        undefined,
        "GET"
    );
    expect(Api.callApi).toHaveBeenNthCalledWith(
        2,
        buildRegionUrl(config.user_deactivate_url, parentApi.data.region),
        JSON.stringify({ login: MOCK_API_DEFINITIONS[0].mail }),
        "POST"
    );
    expect(Api.callApi).toHaveBeenNthCalledWith(
        3,
        buildRegionUrl(config.users_url, parentApi.data.region),
        undefined,
        "GET"
    );
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row
});

/**
 * Check that it is possible to enable a user
 */
test("try enabling a user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <UsersManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const enableButtons = screen.queryAllByRole("button", {
        name: "Activer",
    });
    expect(enableButtons).toHaveLength(1);
    const enableButton = enableButtons[0];
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    await act(async () => {
        fireEvent.click(enableButton);
    });

    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(config.user_activate_url, parentApi.data.region),
        JSON.stringify({ login: MOCK_API_DEFINITIONS[1].mail }),
        "POST"
    );
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row
});

/**
 * Check that it is possible to delete a user
 */
test("try deleting a user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <UsersManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const deleteButtons = screen.queryAllByRole("button", {
        name: "Supprimer",
    });
    expect(deleteButtons).toHaveLength(2);
    const deleteButton = deleteButtons[0];
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    window.confirm = vi.fn().mockImplementation(() => true);

    await act(async () => {
        fireEvent.click(deleteButton);
    });

    expect(Api.callApi).toHaveBeenCalled();
    expect(window.confirm).toHaveBeenCalled();
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row
});

/**
 * Check that it is possible to display the "addition" form in popup.
 */
test("try adding a new user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <UsersManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // check that the popup is not yet visible
    expect(screen.queryByText("Nouvel utilisateur")).not.toBeInTheDocument();

    // we check the title is correctly displayed
    const addButton = screen.queryByRole("button", {
        name: "Ajouter un utilisateur",
    });
    expect(addButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(addButton);
    });
    // check that the popup is now visible
    expect(screen.queryByText("Nouvel utilisateur")).toBeInTheDocument();

    // we check the title is correctly displayed
    const sendForm = screen.queryByRole("button", {
        name: "Envoyer",
    });
    expect(sendForm).toBeInTheDocument();

    // we fill in the forms
    let inputsNames = {
        "Prénom*": "First name",
        "Nom*": "Last name",
        "Mail*": "New mail",
        "Organisation*": "Org",
        "Fonction*": "Job",
        "Territoire*": "region",
        "Code postal*": "69100",
        "Profil*": "admin",
    };

    for (const label in inputsNames) {
        if (Object.hasOwnProperty.call(inputsNames, label)) {
            const value = inputsNames[label];
            const input = screen.getByLabelText(label);
            expect(input).toBeInTheDocument();

            // we need to wait here
            await act(async () => {
                fireEvent.change(input, { target: { value: value } });
            });
            expect(input.value).toBe(value);
        }
    }
    let mailleSelect = screen.getAllByRole("combobox")[2];
    fireEvent.change(mailleSelect, { target: { value: "epci" } });
    expect(mailleSelect.value).toBe("epci");

    // we expect the form to stay opened as we miss data
    await act(async () => {
        fireEvent.click(sendForm);
    });

    // check that the popup is still visible
    expect(screen.queryByText("Nouvel utilisateur")).not.toBeInTheDocument();

    expect(Api.callApi).toHaveBeenCalled();
});
