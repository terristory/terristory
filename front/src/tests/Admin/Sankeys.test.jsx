/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";

import Sankeys from "../../Components/Admin/Sankeys";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const parentApi = {
    data: {
        region: "pytest",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
    },
    callbacks: {},
};
const mockConfigUrl = config;

const MOCK_API_DEFINITIONS = [
    {
        year: 2019,
        unit: "GWh",
        introduction_text: "Texte d'introduction",
        source: "Source fictive",
        copyright: false,
        division_factors: {
            region: "0.001",
            epci: "10002",
        },
        division_units: {
            epci: "MWh",
            pnr: "TWh",
        },
        sankey_name: "Diagramme de flux",
        is_regional_default: true,
        data_table: "pytest_sankey_test_1",
        geographical_levels_enabled: ["epci", "region"],
        nb_decimals: 2,
        data_header: [
            {
                source: "Source fictive",
                target: "Cible fictive",
                id_source: 0,
                id_target: 0,
                colors: "#ffc552",
                label: null,
                code: "12445",
                valeur: 100.0,
            },
        ],
    },
    {
        year: 2020,
        unit: "GWh",
        introduction_text: "Deuxième texte d'introduction",
        source: "Source fictive 2",
        copyright: false,
        division_factors: {
            epci: "1000",
            region: "1000",
        },
        division_units: {
            epci: "MWh",
            region: "GWh",
            departement: "GWh",
        },
        sankey_name: "Diagramme de flux 2",
        is_regional_default: false,
        data_table: "pytest_sankey_test_2",
        geographical_levels_enabled: ["epci", "region"],
        nb_decimals: 0,
        data_header: [
            {
                source: "Source fictive",
                target: "Cible fictive",
                id_source: 0,
                id_target: 0,
                colors: "#ffc552",
                label: null,
                code: "12445",
                valeur: 100.0,
            },
            {
                source: "Source fictive",
                target: "Cible fictive",
                id_source: 0,
                id_target: 0,
                colors: "#ffc552",
                label: null,
                code: "12445",
                valeur: 100.0,
            },
        ],
    },
];
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(mockConfigUrl.api_list_sankeys, "pytest"),
        results: MOCK_API_DEFINITIONS,
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_sankey_layout, "pytest").replace(
            "#table_name#",
            MOCK_API_DEFINITIONS[0].data_table
        ),
        results: {
            status: "success",
            message: "Import successful",
        },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_sankey_data, "pytest").replace(
            "#table_name#",
            MOCK_API_DEFINITIONS[0].data_table
        ),
        results: {
            status: "success",
            message: "Import successful",
        },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_update_sankey_metadata, "pytest").replace(
            "#table_name#",
            MOCK_API_DEFINITIONS[0].data_table
        ),
        results: { message: "Got it!" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_update_sankey_metadata, "pytest").replace(
            "#table_name#",
            MOCK_API_DEFINITIONS[1].data_table
        ),
        results: { status: "failed", message: "Wrong!" },
        fail: true,
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_update_sankey_new, "pytest"),
        results: { status: "success", message: "Import successful!" },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    const { container } = render(
        <Sankeys parentApi={parentApi} connected={connected} />
    );
    expect(container).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields (title, two date inputs, one select object that
 * show both types mocked).
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    // we check the title is correctly displayed

    expect(screen.getByRole("heading")).toHaveTextContent("Diagrammes de Sankey");

    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    // we check we do have the two API defined
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS[0].sankey_name })
    ).toBeInTheDocument();
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS[1].sankey_name })
    ).toBeInTheDocument();
});

/**
 * Try showing data details from popup
 */
test("try showing details popup", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    const showDetails = screen.getAllByRole("img", {
        name: "Visualiser une partie des données",
    });

    // we check we don't have yet the "export" button
    expect(
        screen.queryByRole("link", {
            name: "Exporter le contenu complet de la table en csv",
        })
    ).not.toBeInTheDocument();
    expect(showDetails).toHaveLength(MOCK_API_DEFINITIONS.length);

    // we hit pull button
    await act(async () => {
        fireEvent.click(showDetails[0]);
    });

    expect(
        screen.getByRole("heading", { name: "Aperçu de la table" })
    ).toBeInTheDocument();

    // the show button is now visible
    expect(
        screen.queryByRole("link", {
            name: "Exporter le contenu complet de la table en csv",
        })
    ).toBeInTheDocument();

    // we just click it
    const buttonExport = screen.getByRole("link", {
        name: "Exporter le contenu complet de la table en csv",
    });
    fireEvent.click(buttonExport);

    // we close the window
    expect(screen.queryByRole("button", { name: "×" })).toBeInTheDocument();
    const buttonClose = screen.getByRole("button", { name: "×" });
    expect(buttonClose).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(buttonClose);
    });

    // now we don't have the text anymore
    expect(
        screen.queryByRole("heading", { name: "Aperçu de la table" })
    ).not.toBeInTheDocument();
});

/**
 * Retrieve layout
 */
test("Try retrieving layout", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    const showDetails = screen.getAllByRole("link", { name: "Télécharger" });

    // we check we don't have yet the "export" button
    expect(showDetails).toHaveLength(MOCK_API_DEFINITIONS.length);
    expect(showDetails[0]).toHaveAttribute(
        "href",
        buildRegionUrl(mockConfigUrl.api_sankey_layout, "pytest").replace(
            "#table_name#",
            MOCK_API_DEFINITIONS[0].data_table
        )
    );

    // we hit pull button
    await act(async () => {
        fireEvent.click(showDetails[0]);
    });
});

/**
 * Retrieve layout
 */
test("edit Sankey", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).not.toBeInTheDocument();

    const showDetails = screen.getAllByRole("button", { name: "Modifier" });
    expect(showDetails).toHaveLength(MOCK_API_DEFINITIONS.length);

    // we hit edit button
    fireEvent.click(showDetails[0]);

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).toBeInTheDocument();

    let inputsNames = [
        "Nom du diagramme de Sankey :",
        "Texte d'introduction :",
        "Facteurs multiplicatifs* :",
        "Unités* :",
        "Année :",
        "Nombre de décimales :",
        "Unité :",
        "Source :",
    ];

    for (let index = 0; index < inputsNames.length; index++) {
        const inputName = inputsNames[index];

        const input = screen.getByLabelText(inputName);
        expect(input).toBeInTheDocument();

        // we need to wait here
        await act(async () => {
            fireEvent.change(input, { target: { value: "Fake new value" } });
        });
        expect(input.value).toBe("Fake new value");
    }

    const input = screen.getByRole("checkbox", {
        name: "Est le diagramme par défaut de la région (remplace le précédent éventuellement) :",
    });
    expect(input).toBeInTheDocument();
    await act(async () => {
        fireEvent.change(input, { target: { value: false } });
    });

    const saveChanges = screen.getByRole("button", {
        name: "Enregistrer les modifications",
    });
    expect(saveChanges).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(saveChanges);
    });

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).not.toBeInTheDocument();
});

test("edit Sankey and fail and show error message", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).not.toBeInTheDocument();

    const showDetails = screen.getAllByRole("button", { name: "Modifier" });
    expect(showDetails).toHaveLength(MOCK_API_DEFINITIONS.length);

    // we hit edit button
    await act(async () => {
        fireEvent.click(showDetails[1]);
    });

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).toBeInTheDocument();

    const saveChanges = screen.getByRole("button", {
        name: "Enregistrer les modifications",
    });
    expect(saveChanges).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(saveChanges);
    });

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).toBeInTheDocument();

    expect(screen.getByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent("Wrong!");
});

test("Show edition popup and close it", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).not.toBeInTheDocument();

    const showDetails = screen.getAllByRole("button", { name: "Modifier" });
    expect(showDetails).toHaveLength(MOCK_API_DEFINITIONS.length);

    // we hit edit button
    fireEvent.click(showDetails[0]);

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).toBeInTheDocument();
    // we close the window
    expect(screen.queryByRole("button", { name: "×" })).toBeInTheDocument();
    const buttonClose = screen.getByRole("button", { name: "×" });
    expect(buttonClose).toBeInTheDocument();
    fireEvent.click(buttonClose);

    expect(
        screen.queryByRole("heading", {
            name: "Modification d'un diagramme de Sankey",
        })
    ).not.toBeInTheDocument();
});

/**
 * Check that it is possible to display the "addiion" form in popup.
 */
test("try adding a new sankey diagram", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // check that the popup is not yet visible
    expect(
        screen.queryByText("Modification d'un diagramme de Sankey")
    ).not.toBeInTheDocument();

    // we check the title is correctly displayed
    const addButton = screen.queryByRole("button", {
        name: "Ajouter un nouveau diagramme de Sankey",
    });
    expect(addButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(addButton);
    });
    // check that the popup is now visible
    expect(
        screen.queryByText("Modification d'un diagramme de Sankey")
    ).toBeInTheDocument();

    // we check the title is correctly displayed
    const sendForm = screen.queryByRole("button", {
        name: "Enregistrer les modifications",
    });
    expect(sendForm).toBeInTheDocument();

    // we fill in the forms
    let inputsNames = {
        "Nom de la table de données dans laquelle les données sont stockées. Cette table va être créée automatiquement :":
            "Sankey data table example",
        "Nom du diagramme de Sankey :": "Sankey Diagram example",
        "Texte d'introduction :": "Ceci est un texte d'introduction",
        "Facteurs multiplicatifs* :": "epci: 1000",
        "Unités* :": "epci: MWh",
        "Échelles auxquelles le diagramme est disponible :": "epci,region",
        "Année :": "2020",
        "Est le diagramme par défaut de la région (remplace le précédent éventuellement) :":
            "true",
        "Nombre de décimales :": "2",
        "Unité :": "GWh",
        "Source :": "ORCAE",
    };

    for (const label in inputsNames) {
        if (Object.hasOwnProperty.call(inputsNames, label)) {
            const value = inputsNames[label];
            const input = screen.getByLabelText(label);
            expect(input).toBeInTheDocument();

            // we need to wait here
            await act(async () => {
                fireEvent.change(input, { target: { value: value } });
            });
            expect(input.value).toBe(value);
        }
    }

    // we expect the form to stay opened as we miss data
    await act(async () => {
        fireEvent.click(sendForm);
    });
    // check that the popup is still visible
    expect(
        screen.queryByText("Modification d'un diagramme de Sankey")
    ).not.toBeInTheDocument();

    expect(Api.callApi).toHaveBeenCalled();
});

/**
 * Check that it is possible to delete a Sankey diagram
 */
test("try deleting a Sankey", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const deleteButtons = screen.queryAllByRole("button", {
        name: "Supprimer",
    });
    expect(deleteButtons).toHaveLength(2);
    const deleteButton = deleteButtons[0];
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    window.confirm = vi.fn().mockImplementation(() => true);
    parentApi.callbacks.updateMessages = vi.fn();

    await act(async () => {
        fireEvent.click(deleteButton);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();
    expect(window.confirm).toHaveBeenCalled();
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row
});

/**
 * Check that it is possible to upload a new layout
 */
test("upload a new layout", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    const str = JSON.stringify({ layoutExample: [] });
    const blob = new Blob([str]);
    const file = new File([blob], "layout.json", {
        type: "application/JSON",
    });
    File.prototype.text = vi.fn().mockResolvedValueOnce(str);

    // we check the title is correctly displayed
    const uploaders = screen.getAllByLabelText("Nouveau template :");
    const uploader = uploaders[0];
    await act(async () => {
        fireEvent.change(uploader, {
            target: { files: [file] },
        });
    });

    const uploadButtons = screen.queryAllByRole("button", {
        name: "Mettre à jour",
    });
    expect(uploadButtons).toHaveLength(4);
    const uploadButton = uploadButtons[0];
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    window.confirm = vi.fn().mockImplementation(() => true);
    parentApi.callbacks.updateMessages = vi.fn();

    await act(async () => {
        fireEvent.click(uploadButton);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();
    expect(window.confirm).toHaveBeenCalled();
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row
});

/**
 * Check that it is possible to upload a new layout
 */
test("upload a new data set", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Sankeys
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    const str =
        "test;valeur\
1;2";
    const blob = new Blob([str]);
    const file = new File([blob], "layout.csv", {
        type: "text/csv",
    });
    File.prototype.text = vi.fn().mockResolvedValueOnce(str);

    // we check the title is correctly displayed
    const uploaders = screen.getAllByLabelText("Nouvelles données :");
    const uploader = uploaders[0];
    await act(async () => {
        fireEvent.change(uploader, {
            target: { files: [file] },
        });
    });

    const uploadButtons = screen.queryAllByRole("button", {
        name: "Mettre à jour",
    });
    expect(uploadButtons).toHaveLength(4);
    const uploadButton = uploadButtons[1];
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row

    window.confirm = vi.fn().mockImplementation(() => true);
    parentApi.callbacks.updateMessages = vi.fn();

    await act(async () => {
        fireEvent.click(uploadButton);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();
    expect(window.confirm).toHaveBeenCalled();
    expect(screen.getAllByRole("row")).toHaveLength(4); // 2 rows according to the fixtures + header row + footer row
});
