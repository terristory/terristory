/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";

import RegionScenarioTable from "../../Components/Admin/RegionScenarioTable";
import config from "../../settings";
import { buildRegionUrl, exportToCSV } from "../../utils";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
    },
};
const mockConfigUrl = config;

const MOCK_SCENARIOS = [
    {
        id: 101,
        mail: "test@test.com",
        titre: "Test",
        description: "this is a test scenario",
        zone_type: "epci#commune",
        nom_territoire: "CC Test",
        derniere_modif: "2023-02-22T13:59:23",
    },
    {
        id: 102,
        mail: "test2@test.com",
        titre: "Issoire",
        description: "PA complet",
        zone_type: "teposcv#commune",
        nom_territoire: "TEPOS-CV Test",
        derniere_modif: "2021-04-21T08:57:39",
    },
];
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(
            mockConfigUrl.region_scenarios_url,
            parentApi.data.region
        ).split("#")[0],
        results: MOCK_SCENARIOS,
    },
];

let wrapper;

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
    wrapper = undefined;
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    await act(async () => {
        const { container } = render(
            <RegionScenarioTable parentApi={parentApi} connected={connected} />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is unavailable for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    await act(async () => {
        const { container } = render(
            <RegionScenarioTable
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields (title, button).
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    await act(async () => {
        const { container } = render(
            <RegionScenarioTable
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    // we check the title is correctly displayed
    expect(screen.getByRole("heading")).toHaveTextContent(
        "Stratégies territoriales des utilisateurs de la région"
    );
    // check an export button is displayed
    expect(screen.getAllByRole("button", { name: "Exporter" })).toHaveLength(1);
});

/**
 * Check that the export button is disabled before API call response
 */
/*test("disables the export button before the API response", async () => {
    const userInfos = { profil: "admin" };
    const connected = true;

    // TODO: fix this to be sure API has not returned anything?
    await act(async () => {
        const { container } = render(
            <RegionScenarioTable
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;

        expect(screen.getAllByRole("button", { name: "Exporter" })).toHaveLength(1);
        let button = screen.getByRole("button", { name: "Exporter" });
        expect(button).toBeDisabled();
    });
});*/

/**
 * Check that the export button is enabled after API response
 */
test("enables the export button after the API response", async () => {
    const userInfos = { profil: "admin" };
    const connected = true;
    // use async act to wait for API response
    await act(async () => {
        const { container } = render(
            <RegionScenarioTable
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(screen.getAllByRole("button", { name: "Exporter" })).toHaveLength(1);
    let button = screen.getByRole("button", { name: "Exporter" });
    expect(button).not.toBeDisabled();
});

/**
 * Check displaying the data
 */
test("retrieves and displays the scenarios from the API", async () => {
    const userInfos = { profil: "admin" };
    const connected = true;
    await act(async () => {
        // use mount() this time to also render the child ReactTable
        const { container } = render(
            <RegionScenarioTable
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we get the headers
    let columnHeaders = screen.getAllByRole("columnheader", {
        name: new RegExp(".+", "i"),
    });
    let cells = screen.getAllByRole("cell", {
        name: new RegExp(".+", "i"),
    });
    expect(cells).toHaveLength(columnHeaders.length * MOCK_SCENARIOS.length);

    MOCK_SCENARIOS.forEach((scenario) => {
        for (const key in scenario) {
            if (Object.hasOwnProperty.call(scenario, key)) {
                let element = scenario[key];
                if (key === "id") {
                    continue;
                }
                if (key === "derniere_modif") {
                    element = new Date(element).toLocaleString();
                }
                if (element) {
                    expect(screen.queryAllByText(element)).toHaveLength(1);
                }
            }
        }
    });
});

/**
 * Compute results and check that we can export them through csv export.
 */
test("exports to csv", async () => {
    const userInfos = { profil: "admin" };
    const connected = true;
    await act(async () => {
        const { container } = render(
            <RegionScenarioTable
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check that exportToCsv was not called before
    expect(exportToCSV).not.toHaveBeenCalled();

    // simulate export to csv
    expect(screen.getAllByRole("button", { name: "Exporter" })).toHaveLength(1);
    let exportButton = screen.getByRole("button", { name: "Exporter" });
    fireEvent.click(exportButton);

    // The mock function was called once after clicking
    expect(exportToCSV.mock.calls).toHaveLength(1);
    // All the scenarios were exported
    expect(exportToCSV.mock.calls[0][0]).toHaveLength(MOCK_SCENARIOS.length);
    // The filetype was .csv
    expect(exportToCSV.mock.calls[0][1]).toBe("csv");
    // the file name contains the region name
    expect(exportToCSV.mock.calls[0][2].includes(parentApi.data.region)).toBe(true);
});
