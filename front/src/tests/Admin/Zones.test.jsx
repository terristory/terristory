/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";

import ZonesManage from "../../Components/Admin/ZonesManage";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const mockConfigUrl = config;

const MOCK_API_DEFINITIONS = {
    ZONES: [
        {
            nom: "region",
            maille: "epci",
            libelle: "region / epci",
            hide: false,
        },
        {
            nom: "region",
            maille: "departement",
            libelle: "region / departement",
            hide: false,
        },
        {
            nom: "departement",
            maille: "epci",
            libelle: "departement / epci",
            hide: true,
        },
    ],
    TERRITORIAL_LEVELS: ["region", "epci", "departement", "scot"],
};

const parentApi = {
    data: {
        region: "pytest",
        settings: {},
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        zonesManager: {
            fetchConfiguration: vi.fn().mockResolvedValue("TEST"),
            fetchZones: vi.fn().mockResolvedValue("TEST"),
            zones: MOCK_API_DEFINITIONS.ZONES,
        },
    },
    callbacks: {
        updateMessages: vi.fn(),
    },
};

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(mockConfigUrl.api_area_url, "pytest"),
        results: MOCK_API_DEFINITIONS.ZONES,
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_territorial_levels_url, "pytest"),
        results: MOCK_API_DEFINITIONS.TERRITORIAL_LEVELS,
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_add_zone_url, "pytest"),
        results: { status: "success", message: "success" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_edit_zone_url, "pytest"),
        results: { status: "success", message: "success" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_delete_zone_url, "pytest"),
        results: { status: "success", message: "success" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_order_zone_url, "pytest"),
        results: { status: "success", message: "success" },
    },
    {
        url:
            buildRegionUrl(mockConfigUrl.api_analysis_data_url, "pytest") +
            "/territoire?territoire=True",
        results: { status: "success", message: "success" },
    },
    {
        url:
            buildRegionUrl(
                mockConfigUrl.api_transfer_data_from_global_region_url,
                "pytest"
            ).replace("#table_name#", "territoire") +
            "?territoire=1&date_perimetre=" +
            parseInt(new Date().getFullYear(), 10),
        results: { status: "success", message: "success" },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    let container;
    await act(async () => {
        ({ container } = render(
            <ZonesManage parentApi={parentApi} connected={connected} />
        ));
    });
    expect(container).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields (title, two date inputs, one select object that
 * show both types mocked).
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    // we check the title is correctly displayed

    expect(
        screen.getByRole("heading", { name: "Gestion des zones disponibles" })
    ).toBeInTheDocument();

    expect(screen.getAllByRole("row")).toHaveLength(5);

    // we check we do have the two zones defined
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS.ZONES[0].libelle })
    ).toBeInTheDocument();
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS.ZONES[1].libelle })
    ).toBeInTheDocument();
});

test("edit zone", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("heading", { name: "Modifier une zone existante" })
    ).not.toBeInTheDocument();

    const editZoneButtons = screen.getAllByRole("button", { name: "Modifier" });
    expect(editZoneButtons).toHaveLength(MOCK_API_DEFINITIONS.ZONES.length);

    // we hit edit button
    fireEvent.click(editZoneButtons[0]);

    expect(
        screen.queryByRole("heading", { name: "Modifier une zone existante" })
    ).toBeInTheDocument();

    let inputsNames = {
        Libellé: "New libelle",
        Zone: "scot",
        Maille: "epci",
    };

    for (const label in inputsNames) {
        if (Object.hasOwnProperty.call(inputsNames, label)) {
            const value = inputsNames[label];
            const input = screen.getByLabelText(label);

            expect(input).toBeInTheDocument();

            // we need to wait here
            await act(async () => {
                fireEvent.change(input, { target: { value: value } });
            });
            expect(input.value).toBe(value);
        }
    }

    const saveChanges = screen.getByRole("button", {
        name: "Modifier la zone",
    });
    expect(saveChanges).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(saveChanges);
    });

    expect(
        screen.queryByRole("heading", { name: "Modifier une zone existante" })
    ).not.toBeInTheDocument();

    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(mockConfigUrl.api_edit_zone_url, "pytest"),
        JSON.stringify({
            old_name: MOCK_API_DEFINITIONS.ZONES[0].libelle,
            name: inputsNames["Libellé"],
            level: inputsNames["Zone"],
            maille: inputsNames["Maille"],
        }),
        "PUT"
    );
});

test("add a new zone", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(
        screen.queryByRole("heading", { name: "Ajouter une nouvelle zone" })
    ).not.toBeInTheDocument();

    const addZoneButton = screen.getByRole("button", { name: "Ajouter une zone" });

    // we hit edit button
    fireEvent.click(addZoneButton);

    expect(
        screen.queryByRole("heading", { name: "Ajouter une nouvelle zone" })
    ).toBeInTheDocument();

    let inputsNames = {
        Libellé: "New libelle",
        Zone: "scot",
        Maille: "epci",
    };

    for (const label in inputsNames) {
        if (Object.hasOwnProperty.call(inputsNames, label)) {
            const value = inputsNames[label];
            const input = screen.getByLabelText(label);

            expect(input).toBeInTheDocument();

            // we need to wait here
            await act(async () => {
                fireEvent.change(input, { target: { value: value } });
            });
            expect(input.value).toBe(value);
        }
    }

    const saveChanges = screen.getByRole("button", {
        name: "Modifier la zone",
    });
    expect(saveChanges).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(saveChanges);
    });

    expect(
        screen.queryByRole("heading", { name: "Ajouter une nouvelle zone" })
    ).not.toBeInTheDocument();

    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(mockConfigUrl.api_add_zone_url, "pytest"),
        JSON.stringify({
            name: inputsNames["Libellé"],
            level: inputsNames["Zone"],
            maille: inputsNames["Maille"],
        }),
        "POST"
    );
});

/**
 * Check that it is possible to delete a zone definition
 */
test("try deleting a zone definition", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const deleteButtons = screen.queryAllByRole("button", {
        name: "Supprimer",
    });
    expect(deleteButtons).toHaveLength(3);
    const deleteButton = deleteButtons[0];

    window.confirm = vi.fn().mockImplementation(() => true);
    parentApi.callbacks.updateMessages = vi.fn();

    await act(async () => {
        fireEvent.click(deleteButton);
    });

    expect(window.confirm).toHaveBeenCalled();
    expect(parentApi.callbacks.updateMessages).toBeCalled();

    expect(Api.callApi).toHaveBeenLastCalledWith(
        buildRegionUrl(mockConfigUrl.api_delete_zone_url, "pytest") +
            MOCK_API_DEFINITIONS.ZONES[0].nom +
            "-" +
            MOCK_API_DEFINITIONS.ZONES[0].maille,
        null,
        "DELETE"
    );
});

/**
 * Check that it is possible to order zone definitions
 */
test("change order of zones definitions", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const upButtons = screen.queryAllByRole("button", {
        name: "Monter",
    });
    expect(upButtons).toHaveLength(2);
    const upButton = upButtons[0];

    await act(async () => {
        fireEvent.click(upButton);
    });

    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(config.api_order_zone_url, "pytest"),
        JSON.stringify({
            name: MOCK_API_DEFINITIONS.ZONES[1].libelle,
            direction: "up",
        }),
        "PUT"
    );

    // we check the title is correctly displayed
    const downButtons = screen.queryAllByRole("button", {
        name: "Descendre",
    });
    expect(downButtons).toHaveLength(2);
    const downButton = downButtons[0];

    await act(async () => {
        fireEvent.click(downButton);
    });

    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(config.api_order_zone_url, "pytest"),
        JSON.stringify({
            name: MOCK_API_DEFINITIONS.ZONES[0].libelle,
            direction: "down",
        }),
        "PUT"
    );
});

/**
 * Check that it is possible to hide a zone definition
 */
test("try hiding a zone definition", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const hideButtons = screen.queryAllByRole("button", {
        name: "Cacher",
    });
    expect(hideButtons).toHaveLength(2);
    const hideButton = hideButtons[0];

    parentApi.callbacks.updateMessages = vi.fn();

    await act(async () => {
        fireEvent.click(hideButton);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();

    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(mockConfigUrl.api_toggle_zone_url, "pytest"),
        JSON.stringify({
            name: MOCK_API_DEFINITIONS.ZONES[0].libelle,
        }),
        "PUT"
    );
});

/**
 * Check that it is possible to show a hidden zone definition
 */
test("try showing a zone definition", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ZonesManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const showButtons = screen.queryAllByRole("button", {
        name: "Montrer",
    });
    expect(showButtons).toHaveLength(1); // 1 because MOCKED value only have one zone hidden
    const showButton = showButtons[0];

    parentApi.callbacks.updateMessages = vi.fn();

    await act(async () => {
        fireEvent.click(showButton);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();

    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(mockConfigUrl.api_toggle_zone_url, "pytest"),
        JSON.stringify({
            name: MOCK_API_DEFINITIONS.ZONES[0].libelle,
        }),
        "PUT"
    );
});
