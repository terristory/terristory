/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent, within } from "@testing-library/react";

import POIManage from "../../Components/Admin/POIManage";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const POI_LAYERS = [
    {
        id: 1,
        nom: "multi_icons_test",
        label: "Multi icons layer test",
        couleur: "#e600ffff",
        modifiable: false,
        checked: false,
        afficherStatut: false,
        exportable: false,
        theme: "test",
        type_installation: "A,B,C,D,E",
        structure_constraints: {},
        typeGeom: "Point",
        ancrageIcone: "",
        creditsDataSources: [],
        creditsDataProducers: [],
    },
    {
        id: 2,
        nom: "linear_test",
        label: "Linear layer test",
        couleur: "#00aae6",
        modifiable: false,
        checked: false,
        afficherStatut: true,
        exportable: false,
        theme: "second test category",
        type_installation: null,
        structure_constraints: {},
        typeGeom: "MultiLineString",
        ancrageIcone: "",
        creditsDataSources: [
            {
                name: "Test",
                url: "https://test.test",
            },
        ],
        creditsDataProducers: [
            {
                name: "Test",
                url: "https://test.test",
            },
        ],
    },
    {
        id: 3,
        nom: "polygon_test",
        label: "Polygon layer test",
        couleur: "#0bbc8cbd",
        modifiable: true,
        checked: false,
        afficherStatut: false,
        exportable: false,
        theme: "test",
        type_installation: null,
        structure_constraints: {},
        typeGeom: "MultiPolygon",
        ancrageIcone: "",
        creditsDataSources: [],
        creditsDataProducers: [],
    },
];

const parentApi = {
    data: {
        region: "pytest",
        poiLayers: POI_LAYERS,
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
    },
    callbacks: {
        updatePoiRights: vi.fn(),
        moveToEditPOITab: vi.fn(),
    },
};
const mockConfigUrl = config;

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layer_url, "pytest").replace(
            "#layer#",
            1
        ),
        results: { message: "Couche d'équipement supprimée" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layer_rights_url, "pytest").replace(
            "#layer#",
            "multi_icons_test"
        ),
        results: {},
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layer_rights_url, "pytest").replace(
            "#layer#",
            "polygon_test"
        ),
        results: {},
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest"),
        results: POI_LAYERS,
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
    window.confirm = vi.fn(() => true); // always click 'yes'
});

afterEach(() => {
    vi.clearAllMocks();
});
/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    const { container } = render(
        <POIManage parentApi={parentApi} connected={connected} />
    );
    expect(container).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields based on mocked inputs.
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                moveToEditPOITab={parentApi.callbacks.moveToEditPOITab}
            />
        );
        wrapper = container;
    });
    // we check the title is correctly displayed

    expect(
        screen.getByRole("heading", { name: "Gestion des couches équipements" })
    ).toBeInTheDocument();

    // one layer is already in edit mode
    expect(screen.getAllByRole("button", { name: "Rendre modifiable" })).toHaveLength(
        parentApi.data.poiLayers.filter((layer) => !layer.modifiable).length
    );
    expect(
        screen.getAllByRole("button", { name: "Rendre non modifiable" })
    ).toHaveLength(parentApi.data.poiLayers.filter((layer) => layer.modifiable).length);

    // we have three layers
    expect(screen.getAllByRole("button", { name: "Mettre à jour" })).toHaveLength(
        parentApi.data.poiLayers.length
    );

    const items = screen.getAllByRole("listitem");
    expect(items).toHaveLength(parentApi.data.poiLayers.length);

    parentApi.data.poiLayers.forEach((layer) => {
        expect(screen.getByRole("listitem", { name: layer.label })).toBeInTheDocument();
    });
});

test("edit an item", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                moveToEditPOITab={parentApi.callbacks.moveToEditPOITab}
            />
        );
        wrapper = container;
    });

    const editButton = screen.getAllByRole("button", { name: "Mettre à jour" })[0];
    expect(editButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(editButton);
    });

    expect(parentApi.callbacks.moveToEditPOITab).toBeCalled();
    expect(parentApi.callbacks.moveToEditPOITab).toBeCalledWith({
        ...parentApi.data.poiLayers[0],
        afficherStatut: undefined,
        ancrageIcone: undefined,
        exportable: undefined,
        typeGeom: undefined,
    });
});

test("delete an item", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                moveToEditPOITab={parentApi.callbacks.moveToEditPOITab}
            />
        );
        wrapper = container;
    });

    const deleteButton = screen.getAllByRole("button", { name: "Supprimer" })[0];
    expect(deleteButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(deleteButton);
    });

    expect(window.confirm).toBeCalled();

    expect(Api.callApi).toHaveBeenCalledTimes(3);
    expect(Api.callApi).toHaveBeenNthCalledWith(
        1,
        buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest")
    );

    expect(Api.callApi).toHaveBeenNthCalledWith(
        2,
        buildRegionUrl(mockConfigUrl.api_poi_layer_url, "pytest").replace(
            "#layer#",
            parentApi.data.poiLayers[0].id
        ),
        null,
        "DELETE"
    );
    expect(Api.callApi).toHaveBeenNthCalledWith(
        3,
        buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest")
    );
});

test("set an item editable", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                moveToEditPOITab={parentApi.callbacks.moveToEditPOITab}
            />
        );
        wrapper = container;
    });

    const setEditableButton = screen.getAllByRole("button", {
        name: "Rendre modifiable",
    })[0];
    expect(setEditableButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(setEditableButton);
    });

    expect(Api.callApi).toHaveBeenCalledTimes(3);
    expect(Api.callApi).toHaveBeenNthCalledWith(
        1,
        buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest")
    );

    expect(Api.callApi).toHaveBeenNthCalledWith(
        2,
        buildRegionUrl(mockConfigUrl.api_poi_layer_rights_url, "pytest").replace(
            "#layer#",
            parentApi.data.poiLayers[0].nom
        ),
        JSON.stringify({ droit: true }),
        "PUT"
    );
    expect(Api.callApi).toHaveBeenNthCalledWith(
        3,
        buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest")
    );
});

test("set an item not editable", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIManage
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                moveToEditPOITab={parentApi.callbacks.moveToEditPOITab}
            />
        );
        wrapper = container;
    });

    const setEditableButton = screen.getAllByRole("button", {
        name: "Rendre non modifiable",
    })[0];
    expect(setEditableButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(setEditableButton);
    });

    expect(Api.callApi).toHaveBeenCalledTimes(3);
    expect(Api.callApi).toHaveBeenNthCalledWith(
        1,
        buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest")
    );

    expect(Api.callApi).toHaveBeenNthCalledWith(
        2,
        buildRegionUrl(mockConfigUrl.api_poi_layer_rights_url, "pytest").replace(
            "#layer#",
            parentApi.data.poiLayers[2].nom
        ),
        JSON.stringify({ droit: false }),
        "PUT"
    );
    expect(Api.callApi).toHaveBeenNthCalledWith(
        3,
        buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest")
    );
});
