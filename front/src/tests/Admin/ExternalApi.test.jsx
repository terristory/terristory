/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";

import ExternalAPI from "../../Components/Admin/ExternalAPI";
import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const parentApi = {
    data: {
        region: "pytest",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
    },
    callbacks: {},
};
const mockConfigUrl = config;

const MOCK_API_DEFINITIONS = [
    {
        api_id: 1,
        region: "pytest",
        name: "This is a mocked external API name",
        slug: "slug-test",
        actor: "Actor",
        url: "mocked-url",
        api_key_parameter: "apikey",
        key: "*************",
        format: "application/json",
        method: "GET",
        table_name: "table_name",
        geographical_column: "epci",
        post_data: "{}",
        columns: { code_siren_insee: "epci", etoiles: "valeur", annee: "annee" },
        filters: { referentiel: "cae" },
        date_maj: "2023-04-01, 00:00:00",
    },
    {
        api_id: 2,
        region: "pytest",
        name: "This is another mocked external API name",
        slug: "failed-test",
        actor: "Actor",
        url: "mocked-url",
        api_key_parameter: "apikey",
        key: "*************",
        format: "application/json",
        method: "GET",
        table_name: "table_name",
        geographical_column: "epci",
        post_data: "{}",
        columns: { code_siren_insee: "epci", etoiles: "valeur", annee: "annee" },
        filters: { referentiel: "cae" },
        date_maj: "2023-04-01, 00:00:00",
    },
    {
        api_id: 3,
        region: "pytest",
        name: "This is a mocked name with subkeys",
        slug: "slug-subkey-test",
        actor: "Actor",
        url: "mocked-url",
        api_key_parameter: "apikey",
        key: "*************",
        format: "application/json",
        method: "GET",
        table_name: "table_name",
        geographical_column: "epci",
        subkeys: "test,other_key",
        post_data: "{}",
        columns: { code_siren_insee: "epci", etoiles: "valeur", annee: "annee" },
        filters: { referentiel: "cae" },
        date_maj: "2023-04-01, 00:00:00",
    },
];
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(mockConfigUrl.api_get_external_api_list, "pytest"),
        results: MOCK_API_DEFINITIONS,
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_call_external_api, "pytest").replace(
            "#api_key#",
            "slug-test"
        ),
        results: {
            status: "success",
            message: "Import successful",
            full_response: [{ great: "test" }],
        },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_call_external_api, "pytest").replace(
            "#api_key#",
            "slug-subkey-test"
        ),
        results: {
            status: "success",
            message: "Import successful",
            full_response: { test: { other_key: [{ great: "test" }] } },
        },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_call_external_api, "pytest").replace(
            "#api_key#",
            "failed-test"
        ),
        results: { status: "failed", message: "Wrong!" },
    },
    {
        url: buildRegionUrl(
            mockConfigUrl.api_update_external_api_metadata,
            "pytest"
        ).replace("#slug#", "slug-test"),
        results: { status: "success", message: "Import successful!" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_update_external_api_new, "pytest"),
        results: { status: "success", message: "Import successful!" },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    const { container } = render(
        <ExternalAPI parentApi={parentApi} connected={connected} />
    );
    expect(container).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ExternalAPI
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields (title, two date inputs, one select object that
 * show both types mocked).
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ExternalAPI
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    // we check the title is correctly displayed

    expect(screen.getByRole("heading")).toHaveTextContent(
        "Connexion à des API externes"
    );

    expect(
        screen.queryByRole("button", { name: "Ajouter un lien vers une API externe" })
    ).toBeInTheDocument();

    expect(screen.getAllByRole("row")).toHaveLength(5); // 3 rows according to the fixtures + header row + footer

    // we check we do have the two API defined
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS[0].name })
    ).toBeInTheDocument();
    expect(
        screen.queryByRole("cell", { name: MOCK_API_DEFINITIONS[1].name })
    ).toBeInTheDocument();
});

/**
 * Pull API results and display them
 */
test("try pulling results and showing details popup", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ExternalAPI
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    parentApi.callbacks.updateMessages = vi.fn();

    // we check we don't have yet the "show" button
    expect(screen.queryByRole("button", { name: "Afficher" })).not.toBeInTheDocument();
    expect(screen.queryAllByRole("button", { name: "Importer" })).toHaveLength(3);

    // we hit pull button
    const buttonPull = screen.getAllByRole("button", { name: "Importer" })[0];
    expect(buttonPull).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(buttonPull);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();
    expect(parentApi.callbacks.updateMessages).toBeCalledWith(
        "Import successful",
        "success"
    );

    // the show button is now visible
    expect(screen.queryByRole("button", { name: "Afficher" })).toBeInTheDocument();
    // as we haven't clicked it yet, it is not visible
    expect(
        screen.queryByText("Réponse complète de l'API externe")
    ).not.toBeInTheDocument();

    // we just click it
    const buttonShow = screen.getAllByRole("button", { name: "Afficher" })[0];
    expect(buttonShow).toBeInTheDocument();
    fireEvent.click(buttonShow);

    // now we have the text
    expect(screen.queryByText("Réponse complète de l'API externe")).toBeInTheDocument();

    // we close the window
    expect(screen.queryByRole("button", { name: "×" })).toBeInTheDocument();
    const buttonClose = screen.getByRole("button", { name: "×" });
    expect(buttonClose).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(buttonClose);
    });

    // now we have the text
    expect(
        screen.queryByText("Réponse complète de l'API externe")
    ).not.toBeInTheDocument();
});

/**
 * Pull API results with sub keys and display them
 */
test("try pulling results with subkeys and showing details popup", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ExternalAPI
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    parentApi.callbacks.updateMessages = vi.fn();

    // we check we don't have yet the "show" button
    expect(screen.queryByRole("button", { name: "Afficher" })).not.toBeInTheDocument();
    expect(screen.queryAllByRole("button", { name: "Importer" })).toHaveLength(3);

    // we hit pull button
    const buttonPull = screen.getAllByRole("button", { name: "Importer" })[2];
    expect(buttonPull).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(buttonPull);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();
    expect(parentApi.callbacks.updateMessages).toBeCalledWith(
        "Import successful",
        "success"
    );

    // the show button is now visible
    expect(screen.queryByRole("button", { name: "Afficher" })).toBeInTheDocument();
    // as we haven't clicked it yet, it is not visible
    expect(
        screen.queryByText("Réponse complète de l'API externe")
    ).not.toBeInTheDocument();

    // we just click it
    const buttonShow = screen.getAllByRole("button", { name: "Afficher" })[0];
    expect(buttonShow).toBeInTheDocument();
    fireEvent.click(buttonShow);

    // now we have the text
    expect(screen.queryByText("Réponse complète de l'API externe")).toBeInTheDocument();

    // we close the window
    expect(screen.queryByRole("button", { name: "×" })).toBeInTheDocument();
    const buttonClose = screen.getByRole("button", { name: "×" });
    expect(buttonClose).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(buttonClose);
    });

    // now we have the text
    expect(
        screen.queryByText("Réponse complète de l'API externe")
    ).not.toBeInTheDocument();
});

/**
 * Compute results and check that they are present.
 */
test("try failing API call", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ExternalAPI
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    parentApi.callbacks.updateMessages = vi.fn();

    // we check we don't have yet the "show" button
    expect(screen.queryByRole("button", { name: "Afficher" })).not.toBeInTheDocument();
    expect(screen.queryAllByRole("button", { name: "Importer" })).toHaveLength(3);

    // we hit pull button
    const buttonPull = screen.getAllByRole("button", { name: "Importer" })[1];
    expect(buttonPull).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(buttonPull);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();
    expect(parentApi.callbacks.updateMessages).toBeCalledWith("Wrong!", "danger");

    // we still don't have a "Show" button
    expect(screen.queryByRole("button", { name: "Afficher" })).not.toBeInTheDocument();
});

/**
 * Check that it is possible to display the "addiion" form in popup.
 */
test("try adding a new api link", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ExternalAPI
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // check that the popup is not yet visible
    expect(
        screen.queryByText("Modification d'un lien vers une API externe")
    ).not.toBeInTheDocument();

    // we check the title is correctly displayed
    const addButton = screen.queryByRole("button", {
        name: "Ajouter un lien vers une API externe",
    });
    expect(addButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(addButton);
    });
    // check that the popup is now visible
    expect(
        screen.queryByText("Modification d'un lien vers une API externe")
    ).toBeInTheDocument();

    // we check the title is correctly displayed
    const sendForm = screen.queryByRole("button", {
        name: "Enregistrer les modifications",
    });
    expect(sendForm).toBeInTheDocument();

    // we fill in the forms
    let inputsNames = {
        "Nom de la table de données :": "Fake new value",
        "Nom :": "Fake new value",
        "Acteur :": "Fake new value",
        "URL principale :": "Fake new value",
        "Millésime (année) du périmètre géographique :": "2023",
        "Année statique :": "2022",
        "Colonne de détails pour l'affichage :": "details",
        "Maille géographique :": "epci",
        "Colonnes* :": "years: annee",
        "Filtres* :": "test: 12",
        "POST data* :": "valeur: valeur",
        "Clé :": "AAAAA",
        "Nom du paramètre contenant la clé :": "key",
        "Méthode :": "GET",
        "Format :": "json",
        "Sous-clés :": "test,bb",
    };

    for (const label in inputsNames) {
        if (Object.hasOwnProperty.call(inputsNames, label)) {
            const value = inputsNames[label];
            const input = screen.getByLabelText(label);
            expect(input).toBeInTheDocument();

            // we need to wait here
            await act(async () => {
                fireEvent.change(input, { target: { value: value } });
            });
            expect(input.value).toBe(value);
        }
    }

    // we expect the form to stay opened as we miss data
    await act(async () => {
        fireEvent.click(sendForm);
    });
    // check that the popup is still visible
    expect(
        screen.queryByText("Modification d'un lien vers une API externe")
    ).not.toBeInTheDocument();

    expect(Api.callApi).toHaveBeenCalled();
});

/**
 * Check that it is possible to delete an api link.
 */
test("try deleting an api link", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <ExternalAPI
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    const deleteButtons = screen.queryAllByRole("button", {
        name: "Supprimer",
    });
    expect(deleteButtons).toHaveLength(3);
    const deleteButton = deleteButtons[0];
    expect(screen.getAllByRole("row")).toHaveLength(5); // 3 rows according to the fixtures + header row + footer

    window.confirm = vi.fn().mockImplementation(() => true);
    parentApi.callbacks.updateMessages = vi.fn();

    await act(async () => {
        fireEvent.click(deleteButton);
    });

    expect(parentApi.callbacks.updateMessages).toBeCalled();
    expect(window.confirm).toHaveBeenCalled();
    expect(screen.getAllByRole("row")).toHaveLength(5); // 3 rows according to the fixtures + header row + footer
});
