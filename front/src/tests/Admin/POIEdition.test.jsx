/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import { create, select } from "react-select-event";

import config from "../../settings";
import { buildRegionUrl } from "../../utils";
import Api from "../../Controllers/Api";
import POIEdition from "../../Components/Admin/POIEdition";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const POI_LAYERS = [
    {
        id: 1,
        nom: "multi_icons_test",
        label: "Multi icons layer test",
        couleur: "#e600ffff",
        modifiable: false,
        checked: false,
        afficherStatut: false,
        exportable: false,
        theme: "test",
        type_installation: "A,B,C,D,E",
        structure_constraints: {},
        typeGeom: "Point",
        ancrageIcone: "",
        creditsDataSources: [],
        creditsDataProducers: [],
    },
    {
        id: 2,
        nom: "linear_test",
        label: "Linear layer test",
        couleur: "#00aae6",
        modifiable: false,
        checked: false,
        afficherStatut: true,
        exportable: false,
        theme: "second test category",
        type_installation: null,
        structure_constraints: {},
        typeGeom: "MultiLineString",
        ancrageIcone: "",
        creditsDataSources: [
            {
                name: "Test",
                url: "https://test.test",
            },
        ],
        creditsDataProducers: [
            {
                name: "Test",
                url: "https://test.test",
            },
        ],
    },
    {
        id: 3,
        nom: "polygon_test",
        label: "Polygon layer test",
        couleur: "#0bbc8cbd",
        modifiable: true,
        checked: false,
        afficherStatut: false,
        exportable: false,
        theme: "test",
        type_installation: null,
        structure_constraints: {},
        typeGeom: "MultiPolygon",
        ancrageIcone: "",
        creditsDataSources: [],
        creditsDataProducers: [],
    },
    {
        id: 4,
        nom: "single_point_normal_case",
        label: "Single point normal test",
        couleur: "#e600ffff",
        modifiable: false,
        checked: false,
        afficherStatut: false,
        exportable: false,
        theme: "test",
        type_installation: null,
        structure_constraints: {},
        typeGeom: "Point",
        ancrageIcone: "",
        creditsDataSources: [],
        creditsDataProducers: [],
    },
];
const parentApi = {
    data: {
        region: "pytest",
        poiLayers: POI_LAYERS,
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        equipementsManager: {
            themes: ["test", "second test category"],
            poiLayers: POI_LAYERS,
        },
    },
    callbacks: {
        updatePoiRights: vi.fn(),
        updateTabEquipements: vi.fn(),
    },
};
const mockConfigUrl = config;

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_add_layer_url, "pytest"),
        results: { message: "Couche d'équipement ajoutée" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layers_url, "pytest"),
        results: POI_LAYERS,
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layer_url, "pytest").replace(
            "#layer#",
            POI_LAYERS[0].nom
        ),
        results: { message: "Couche d'équipement modifiée" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layer_url, "pytest").replace(
            "#layer#",
            POI_LAYERS[1].nom
        ),
        results: { message: "Couche d'équipement modifiée" },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_poi_layer_url, "pytest").replace(
            "#layer#",
            POI_LAYERS[3].nom
        ),
        results: { message: "Couche d'équipement bien modifiée !" },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

afterEach(() => {
    vi.clearAllMocks();
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    const { container } = render(
        <POIEdition parentApi={parentApi} connected={connected} />
    );
    expect(container).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields based on mocked inputs.
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    expect(
        screen.getByRole("heading", { name: "Ajout d'une nouvelle couche équipement" })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("heading", {
            name: "Renseignez les informations pour cette couche équipement",
        })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("heading", { name: "Sélectionnez les données à importer" })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("heading", { name: "Fichier de données" })
    ).toBeInTheDocument();

    // we check the form inputs are available
    expect(
        screen.getByRole("checkbox", {
            name: "Afficher le statut du projet dans la légende",
        })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("checkbox", {
            name: "Icônes multiples en fonction du champ type",
        })
    ).toBeInTheDocument();

    // we check we have the right buttons
    expect(
        screen.getByRole("button", { name: "Ajouter la couche équipement" })
    ).toBeInTheDocument();
});

test("add a new layer", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { getByText, container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = getByText;
    });

    const editButton = screen.getAllByRole("button", {
        name: "Ajouter la couche équipement",
    })[0];
    expect(editButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(editButton);
    });

    // we check we have the right buttons
    expect(screen.getByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent("Vous devez renseigner un nom");

    // we fill in all main fields
    await create(screen.getByLabelText("Nom de la table"), "test_de_nom_de_table", {
        createOptionText: /^Créer /,
    });
    await select(screen.getByLabelText("Rubrique"), "second test category");
    await act(async () => {
        const nameField = screen.getByRole("textbox", { name: "Nom de la couche" });
        fireEvent.change(nameField, { target: { value: "Layer name" } });
        const isExportable = screen.getByRole("checkbox", {
            name: "Autoriser l'export des données de l'équipement",
        });
        fireEvent.click(isExportable);
        const showInLegend = screen.getByRole("checkbox", {
            name: "Afficher le statut du projet dans la légende",
        });
        fireEvent.click(showInLegend);
        const dataFile = screen.getByRole("checkbox", {
            name: "Afficher le statut du projet dans la légende",
        });
        fireEvent.click(dataFile);
    });

    // we select the geometry point type
    const geometryTypeField = screen.getByRole("combobox", {
        name: "Type de géométrie",
    });
    await act(async () => {
        fireEvent.keyDown(geometryTypeField, { key: "ArrowDown" });
    });
    await act(async () => {
        fireEvent.click(wrapper("Point"));
    });

    const str =
        "test;valeur\
    1;2";
    const blob = new Blob([str]);
    const file = new File([blob], "layout.csv", {
        type: "text/csv",
    });
    File.prototype.text = vi.fn().mockResolvedValueOnce(str);
    // we upload a data file
    const uploaders = screen.getAllByLabelText("Fichier de données :");
    const uploader = uploaders[0];
    await act(async () => {
        fireEvent.change(uploader, {
            target: { files: [file] },
        });
    });
    // we upload a PDF file
    const pdfUploader = screen.getAllByLabelText("Fichier PDF méthodologique")[0];
    await act(async () => {
        fireEvent.change(pdfUploader, {
            target: {
                files: [
                    new File([], "layout.pdf", {
                        type: "application/pdf",
                    }),
                ],
            },
        });
    });

    // we upload SVG icons
    const iconUploader = screen.getAllByLabelText(
        "Icône pour l'affichage (format SVG)"
    )[0];
    await act(async () => {
        fireEvent.change(iconUploader, {
            target: {
                files: [
                    new File([], "icon.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });
    const iconProjectUploader = screen.getAllByLabelText(
        "Icône pour l'affichage en projet (format SVG)"
    )[0];
    await act(async () => {
        fireEvent.change(iconProjectUploader, {
            target: {
                files: [
                    new File([], "icon.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });
    const iconLegendUploader = screen.getAllByLabelText(
        "Icône pour la légende (format SVG)"
    )[0];
    await act(async () => {
        fireEvent.change(iconLegendUploader, {
            target: {
                files: [
                    new File([], "icon.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });

    await act(async () => {
        fireEvent.click(editButton);
    });

    expect(screen.queryByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent("Couche d'équipement ajoutée");
    expect(Api.callApi).toHaveBeenCalledTimes(2); // retrieve initially tables list + save/edit
    expect(parentApi.callbacks.updatePoiRights).toBeCalled();
});

test("add a new layer with multiple icons", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { getByText, container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = getByText;
    });

    const editButton = screen.getAllByRole("button", {
        name: "Ajouter la couche équipement",
    })[0];
    expect(editButton).toBeInTheDocument();

    // we fill in all main fields
    await create(screen.getByLabelText("Nom de la table"), "test_de_nom_de_table", {
        createOptionText: /^Créer /,
    });
    await select(screen.getByLabelText("Rubrique"), "second test category");
    await act(async () => {
        const nameField = screen.getByRole("textbox", { name: "Nom de la couche" });
        fireEvent.change(nameField, { target: { value: "Layer name" } });
        const isExportable = screen.getByRole("checkbox", {
            name: "Autoriser l'export des données de l'équipement",
        });
        fireEvent.click(isExportable);
        const showInLegend = screen.getByRole("checkbox", {
            name: "Afficher le statut du projet dans la légende",
        });
        fireEvent.click(showInLegend);
        const dataFile = screen.getByRole("checkbox", {
            name: "Afficher le statut du projet dans la légende",
        });
        fireEvent.click(dataFile);
    });

    // we select the geometry point type
    const geometryTypeField = screen.getByRole("combobox", {
        name: "Type de géométrie",
    });
    await act(async () => {
        fireEvent.keyDown(geometryTypeField, { key: "ArrowDown" });
    });
    await act(async () => {
        fireEvent.click(wrapper("Point"));
    });

    // we enable multiple icons mode
    const isMultiIcon = screen.getByRole("checkbox", {
        name: "Icônes multiples en fonction du champ type",
    });

    await act(async () => {
        fireEvent.click(isMultiIcon);
    });

    // we add two icons
    const addIconButton = screen.getByRole("button", {
        name: "Ajouter une icône",
    });
    await act(async () => {
        fireEvent.click(addIconButton);
    });
    await act(async () => {
        fireEvent.click(addIconButton);
    });

    // we check we have two fields
    const catFields = screen.getAllByRole("textbox", {
        name: "Valeur de la catégorie correspondant à cette icône",
    });
    expect(catFields).toHaveLength(2);

    // we fill in both icons data
    await act(async () => {
        fireEvent.change(catFields[0], { target: { value: "Category modality 0" } });
        fireEvent.change(catFields[1], { target: { value: "Category modality 1" } });
    });

    // we mock uploading a file
    const iconUploader = screen.getAllByLabelText("Modifier l'icône")[0];
    await act(async () => {
        fireEvent.change(iconUploader, {
            target: {
                files: [
                    new File([new Blob(["TEST"])], "icon1.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });

    const str =
        "test;valeur\
    1;2";
    const blob = new Blob([str]);
    const file = new File([blob], "layout.csv", {
        type: "text/csv",
    });
    File.prototype.text = vi.fn().mockResolvedValueOnce(str);
    // we upload a data file
    const dataFileUploader = screen.getAllByLabelText("Fichier de données :")[0];
    await act(async () => {
        fireEvent.change(dataFileUploader, {
            target: { files: [file] },
        });
    });

    // we upload a PDF file
    const pdfUploader = screen.getAllByLabelText("Fichier PDF méthodologique")[0];
    await act(async () => {
        fireEvent.change(pdfUploader, {
            target: {
                files: [
                    new File([], "layout.pdf", {
                        type: "application/pdf",
                    }),
                ],
            },
        });
    });

    await act(async () => {
        fireEvent.click(editButton);
    });

    expect(screen.queryByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent(
        "Une icône manque pour la modalité Category modality 1"
    );

    // we mock uploading the second icon file
    const secondIconUploader = screen.getAllByLabelText("Modifier l'icône")[1];
    await act(async () => {
        fireEvent.change(secondIconUploader, {
            target: {
                files: [
                    new File([new Blob(["TEST"])], "icon2.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });

    await act(async () => {
        fireEvent.click(editButton);
    });

    expect(screen.queryByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent("Couche d'équipement ajoutée");
    expect(Api.callApi).toHaveBeenCalledTimes(2); // retrieve initially tables list + save/edit
    expect(parentApi.callbacks.updatePoiRights).toBeCalled();
});

test("add a source", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    const editButton = screen.getAllByRole("button", {
        name: "Ajouter une source",
    })[0];
    expect(editButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(editButton);
    });
    await act(async () => {
        fireEvent.click(editButton);
    });

    const deleteButtons = screen.getAllByRole("button", {
        name: "Supprimer",
    });
    expect(deleteButtons).toHaveLength(2);

    await act(async () => {
        const nameField = screen.getAllByRole("textbox", { name: "Nom :" })[0];
        fireEvent.change(nameField, { target: { value: "Source 0" } });
        const urlField = screen.getAllByRole("textbox", { name: "URL :" })[0];
        fireEvent.change(urlField, { target: { value: "URL 0" } });
        const nameField2 = screen.getAllByRole("textbox", { name: "Nom :" })[1];
        fireEvent.change(nameField2, { target: { value: "Source 1" } });
        const urlField2 = screen.getAllByRole("textbox", { name: "URL :" })[1];
        fireEvent.change(urlField2, { target: { value: "URL 1" } });
    });

    const inputFieldsBefore = screen.getAllByRole("textbox", {
        name: "Nom :",
    });
    expect(inputFieldsBefore).toHaveLength(2);
    await act(async () => {
        fireEvent.click(deleteButtons[0]);
    });
    const inputFieldsAfter = screen.getAllByRole("textbox", {
        name: "Nom :",
    });
    expect(inputFieldsAfter).toHaveLength(1);
    expect(inputFieldsAfter[0]).toHaveValue("Source 1");
});

test("use multiple icons", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    const isMultiIcon = screen.getByRole("checkbox", {
        name: "Icônes multiples en fonction du champ type",
    });

    await act(async () => {
        fireEvent.click(isMultiIcon);
    });

    // we add two icons
    const addIconButton = screen.getByRole("button", {
        name: "Ajouter une icône",
    });
    await act(async () => {
        fireEvent.click(addIconButton);
    });
    await act(async () => {
        fireEvent.click(addIconButton);
    });

    // we check we have two fields
    const catFields = screen.getAllByRole("textbox", {
        name: "Valeur de la catégorie correspondant à cette icône",
    });
    expect(catFields).toHaveLength(2);

    // we fill in both icons data
    await act(async () => {
        fireEvent.change(catFields[0], { target: { value: "Category modality 0" } });
        fireEvent.change(catFields[1], { target: { value: "Category modality 1" } });
    });

    // we delete the first icon data
    const deleteButtons = screen.getAllByRole("button", {
        name: "Retirer cette icône",
    });
    expect(deleteButtons).toHaveLength(2);

    const inputFieldsBefore = screen.getAllByRole("textbox", {
        name: "Valeur de la catégorie correspondant à cette icône",
    });
    expect(inputFieldsBefore).toHaveLength(2);
    await act(async () => {
        fireEvent.click(deleteButtons[0]);
    });
    const inputFieldsAfter = screen.getAllByRole("textbox", {
        name: "Valeur de la catégorie correspondant à cette icône",
    });
    expect(inputFieldsAfter).toHaveLength(1);
    expect(inputFieldsAfter[0]).toHaveValue("Category modality 1");

    // we mock uploading a file
    const uploaders = screen.getAllByLabelText("Modifier l'icône");
    const uploader = uploaders[0];
    await act(async () => {
        fireEvent.change(uploader, {
            target: { files: [undefined] },
        });
    });
});

test("renders the page while editing a POI layer", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                mode={"modifier"}
                equipementCourant={POI_LAYERS[0]}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    expect(
        screen.getByRole("heading", {
            name:
                "Mettre à jour la couche d'équipements \"" + POI_LAYERS[0].label + '"',
        })
    ).toBeInTheDocument();

    const editButton = screen.getAllByRole("button", {
        name: "Mettre à jour la couche équipement",
    })[0];
    expect(editButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(editButton);
    });

    // we check we have the right buttons
    expect(screen.queryByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent("Couche d'équipement modifiée");
    expect(Api.callApi).toHaveBeenCalledTimes(2); // retrieve initially tables list + save/edit
    expect(parentApi.callbacks.updatePoiRights).toBeCalled();
});

test("renders the page while editing a linear POI layer", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                mode={"modifier"}
                equipementCourant={POI_LAYERS[1]}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    expect(
        screen.getByRole("heading", {
            name:
                "Mettre à jour la couche d'équipements \"" + POI_LAYERS[1].label + '"',
        })
    ).toBeInTheDocument();

    const editButton = screen.getAllByRole("button", {
        name: "Mettre à jour la couche équipement",
    })[0];
    expect(editButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(editButton);
    });

    // we check we have the right buttons
    expect(screen.queryByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent("Couche d'équipement modifiée");
    expect(Api.callApi).toHaveBeenCalledTimes(2); // retrieve initially tables list + save/edit
    expect(parentApi.callbacks.updatePoiRights).toBeCalled();
});

test("renders the page while editing a single point normal POI layer", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <POIEdition
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
                mode={"modifier"}
                equipementCourant={POI_LAYERS[3]}
            />
        );
        wrapper = container;
    });

    // we check the title is correctly displayed
    expect(
        screen.getByRole("heading", {
            name:
                "Mettre à jour la couche d'équipements \"" + POI_LAYERS[3].label + '"',
        })
    ).toBeInTheDocument();

    // we upload the icons
    const iconUploader = screen.getAllByLabelText(
        "Icône pour l'affichage (format SVG)"
    )[0];
    await act(async () => {
        fireEvent.change(iconUploader, {
            target: {
                files: [
                    new File([], "icon.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });
    const iconProjectUploader = screen.getAllByLabelText(
        "Icône pour l'affichage en projet (format SVG)"
    )[0];
    await act(async () => {
        fireEvent.change(iconProjectUploader, {
            target: {
                files: [
                    new File([], "icon.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });
    const iconLegendUploader = screen.getAllByLabelText(
        "Icône pour la légende (format SVG)"
    )[0];
    await act(async () => {
        fireEvent.change(iconLegendUploader, {
            target: {
                files: [
                    new File([], "icon.svg", {
                        type: "image/svg",
                    }),
                ],
            },
        });
    });

    const editButton = screen.getAllByRole("button", {
        name: "Mettre à jour la couche équipement",
    })[0];
    expect(editButton).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(editButton);
    });

    // we check we have the right buttons
    expect(screen.queryByRole("alert")).toBeInTheDocument();
    expect(screen.getByRole("alert")).toHaveTextContent(
        "Couche d'équipement bien modifiée !"
    );
    expect(Api.callApi).toHaveBeenCalledTimes(2); // retrieve initially tables list + save/edit
    expect(parentApi.callbacks.updatePoiRights).toBeCalled();
});
