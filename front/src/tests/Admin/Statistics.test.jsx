/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import selectEvent from "react-select-event";

import Statistics from "../../Components/Admin/Statistics";
import config from "../../settings";
import { buildRegionUrl, exportToCSV } from "../../utils";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
    },
};
const mockConfigUrl = config;

const MOCK_STATS_TYPE = ["This tests we can render Select", "test2"];
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildRegionUrl(mockConfigUrl.api_stats_types, parentApi.data.region),
        results: {
            stats_types: MOCK_STATS_TYPE,
        },
    },
    {
        url: buildRegionUrl(mockConfigUrl.api_stats_query, parentApi.data.region).split(
            "#"
        )[0],
        results: { data: [{ header: "data_example" }], headers: ["header"] },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the page is unavailable for non-connected user.
 */
test("renders the page for non connected user", async () => {
    // we simulate the parentApi context
    const connected = false;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Statistics parentApi={parentApi} connected={connected} />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available but blocked for non-admin user.
 */
test("renders the page for non admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "utilisateur" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Statistics
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    expect(wrapper).toHaveTextContent("Non accessible.");
});

/**
 * Check that the page is available for admin and that it does contain
 * the required fields (title, two date inputs, one select object that
 * show both types mocked).
 */
test("renders the page for admin connected user", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Statistics
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });
    // we check the title is correctly displayed
    expect(screen.getByRole("heading")).toHaveTextContent(
        "Statistiques de consultation"
    );

    // we check we do have the two date inputs
    expect(screen.getAllByRole("dialog")).toHaveLength(2);

    // we check we do have the select input
    expect(screen.getByRole("combobox")).toBeInTheDocument();

    // select first value
    await selectEvent.select(screen.getByRole("combobox"), MOCK_STATS_TYPE[0]);
    expect(screen.getByRole("form")).toHaveFormValues({
        stat_type: MOCK_STATS_TYPE[0],
    });

    // select second value
    await selectEvent.select(screen.getByRole("combobox"), MOCK_STATS_TYPE[1]);
    expect(screen.getByRole("form")).toHaveFormValues({
        stat_type: MOCK_STATS_TYPE[1],
    });
});

/**
 * Change value inside select and check that it was correctly propagated
 * to the state. Also change the input dates.
 */
test("change the value in select and various inputs", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Statistics
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    const mySelectComponent = screen.getByRole("combobox");
    // we check we did found a select
    expect(mySelectComponent).toBeInTheDocument();

    // we change its value
    await selectEvent.select(screen.getByRole("combobox"), MOCK_STATS_TYPE[1]);
    expect(screen.getByRole("form")).toHaveFormValues({
        stat_type: MOCK_STATS_TYPE[1],
    });

    const inputs = screen.getAllByRole("dialog");
    expect(inputs).toHaveLength(2);
    let i = 0;
    for (const input of inputs) {
        fireEvent.change(input, { target: { value: "2023-09-21" } });
        expect(input.value).toBe("2023-09-21");
        // we send the form
        const buttons = screen.getAllByRole("button");
        fireEvent.click(buttons[0]);
        await screen.findByText("Exporter les résultats en csv");
        // we check query sent is successfully taking the parameters into account
        expect(Api.callApi).toHaveBeenCalledWith(
            buildRegionUrl(config.api_stats_query, parentApi.data.region)
                .replace("#stat_type#", "test2")
                .replace("#from_date#", "2023-09-21"),
            undefined,
            "GET"
        );
        i++;
        // .state(i === 0 ? "fromDate" : "toDate")).toBe("2023-09-21");
    }
});

/**
 * Try to download results with an empty from date and check that the call
 * was not launched but raised an error message.
 */
test("try downloading results with empty from date", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Statistics
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    // we set fromDate to undefined
    const inputs = screen.getAllByRole("dialog");
    expect(inputs).toHaveLength(2);
    fireEvent.change(inputs[0], { target: { value: "" } });
    expect(inputs[0].value).toBe("");

    // we find send button
    const buttonSend = screen.getByRole("button");
    expect(buttonSend).toBeDefined();
    fireEvent.click(buttonSend);

    // we check that we did trigger an error by not providing fromDate
    await screen.findByText(
        "La date de début ou le type de statistiques n'ont pas été renseignés."
    );
    expect(
        screen.getByText(
            "La date de début ou le type de statistiques n'ont pas été renseignés."
        )
    ).toBeInTheDocument();
    expect(screen.queryByText("Exporter les résultats en csv")).not.toBeInTheDocument();
});

/**
 * Compute results and check that they are present.
 */
test("try computing results", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Statistics
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(screen.queryByText("Exporter les résultats en csv")).not.toBeInTheDocument();
    // we set fromDate to undefined
    const inputs = screen.getAllByRole("dialog");
    expect(inputs).toHaveLength(2);
    fireEvent.change(inputs[0], { target: { value: "2023-09-21" } });
    expect(inputs[0].value).toBe("2023-09-21");

    // we send
    const buttonSend = screen.getByRole("button");
    expect(buttonSend).toBeInTheDocument();
    fireEvent.click(buttonSend);

    // the export button is now visible
    await screen.findByText("Exporter les résultats en csv");
    expect(screen.getByText("Exporter les résultats en csv")).toBeInTheDocument();

    const headers = screen.getAllByRole("columnheader");
    expect(headers).toHaveLength(2); // header + empty footer

    // we check first cell has the right content
    const cells = screen.getAllByRole("cell");
    expect(cells[0]).toHaveTextContent("data_example");

    // TODO: check that the results are displayed on the page
});

/**
 * Compute results and check that we can export them through csv export.
 */
test("try exporting csv results", async () => {
    // we simulate the parentApi context
    const userInfos = { profil: "admin" };
    const connected = true;
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <Statistics
                parentApi={parentApi}
                userInfos={userInfos}
                connected={connected}
            />
        );
        wrapper = container;
    });

    expect(screen.queryByText("Exporter les résultats en csv")).not.toBeInTheDocument();
    const buttonSend = screen.getByRole("button");
    expect(buttonSend).toBeInTheDocument();

    // we extract the results
    fireEvent.click(buttonSend);
    await screen.findByText("Exporter les résultats en csv");

    // we find button to export results as csv
    const buttonExport = screen.getByText("Exporter les résultats en csv");
    expect(buttonExport).toBeDefined();
    expect(buttonExport).not.toBeNull();

    // we check that exportToCsv was not called before
    expect(exportToCSV.mock.calls).toHaveLength(0);
    fireEvent.click(buttonExport);
    // The mock function was called once after clicking
    expect(exportToCSV.mock.calls).toHaveLength(1);
});
