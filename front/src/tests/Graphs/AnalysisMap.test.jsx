/*
* TerriSTORY®
*
© Copyright 2022 AURA-EE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* A copy of the GNU Affero General Public License should be present along
* with this program at the root of current repository. If not, see
* http://www.gnu.org/licenses/.
*/

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";

import AnalysisMap from "../../Components/Graphs/AnalysisMap";
import config from "../../settings";
import Api from "../../Controllers/Api";
import OlMap from "../../Components/Map/OlMap";
import { buildRegionUrl } from "../../utils";

vi.mock("../../Controllers/Api.js");
vi.mock("../../Components/Map/OlMap.jsx", () => ({
    default: vi.fn(() => "ol-map-mocking"),
}));
vi.mock("../../utils.js");

const MOCKED_ANALYSES = [
    {
        id: 1,
        type: "choropleth",
    },
    {
        id: 3,
        type: "stars",
        representation_details: {
            minValue: 1,
            maxValue: 10,
            color: "red",
        },
    },
    {
        id: 4,
        type: "circle",
    },
    {
        id: 5,
        type: "pixels",
    },
    {
        id: 6,
        type: "pixels_cat",
    },
    {
        id: 7,
        type: "flow",
    },
    {
        id: 8,
        type: "choropleth",
    },
    {
        // empty pixels
        id: 9,
        type: "pixels_cat",
    },
    {
        // no data
        id: 10,
        type: "stars",
    },
];

const analysisParams = (id = 1) => ({
    id,
    id_analysis: id,
    representation: MOCKED_ANALYSES.find((e) => e.id === id),
    MOCK_DATA_INPUTS: (data, type) => {
        // handle cats filtering
        let mapData = [
            {
                code: "00000",
                nom: "Blab",
                x: 12,
                y: 10,
                val: 95,
                val_applats: 100,
            },
            {
                code: "00001",
                nom: "Last example",
                x: 6,
                y: 0,
                val: 81,
                val_applats: 100,
            },
            {
                code: "00002",
                nom: "Other example",
                x: 3,
                y: 7,
                val: 68,
                val_applats: 100,
            },
        ];
        if (data === '{"cat_name":[{"filtre_categorie":"cat_name.First label"}]}') {
            mapData[0].val = 20;
            mapData[1].val = 10;
            mapData[2].val = 0;
        }
        if (id === 10) {
            mapData[0].val = 0;
            mapData[1].val = 0;
            mapData[2].val = 0;
        }

        let distinctValues = [
            {
                valeur: "95",
                count: 1,
                modalite: "Data unknown",
                couleur: "rgba(72, 182, 237, 0.5)",
            },
            {
                valeur: 81,
                count: 2,
                modalite: "Data average",
                couleur: "rgba(0, 255, 0, 0.3)",
            },
        ];
        if (id === 9) {
            distinctValues = [];
        }

        return {
            afficher_indicateur_maille: false,
            map: mapData,
            charts: [
                {
                    indicateur: 1,
                    name: "cat_name",
                    labels: ["First label", "Second label"],

                    data: [10, 20],
                    colors: ["#fecf44", "#933588"],
                    modalites_disponibles: ["First label", "Second label"],
                },
            ],
            confid: {
                charts: id === 8 ? "D" : [],
            },
            donnees_deuxieme_representation: "",
            bornes_filtres: "",
            min_max_valeurs_filtrees: "",
            min_max_valeurs: {
                min: 0,
                max: id === 10 ? 0 : 5,
            },
            intervalle_temps: 2020,
            intervalles_temps_recent_ancien: 2000,
            distinct_values: distinctValues,
            stations_mesures: "",
            nb_classes_color_representation: 6,
        };
    },
});
const disabledAnalysisParams = {
    id_analysis: 2,
    representation: "analysis-launcher",
    MOCK_DATA_INPUTS: {
        disabled: true,
    },
};

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
        zone: {
            zone: "departement",
            maille: "epci",
        },
        currentZone: "01",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        zonesManager: {
            getZoneName: () => {
                return "test";
            },
        },
        analysisManager: {
            getAnalysisNameTheme: () => {
                return "name analysis";
            },
            initFiltersByCategory: () => {
                return {
                    cat_name: [
                        {
                            filtre_categorie: "cat_name.First label",
                        },
                        {
                            filtre_categorie: "cat_name.Second label",
                        },
                    ],
                };
            },
            getLegendTitles: () => {
                return "Fake legend title";
            },
            getFilterDefaultValue: (id) => undefined,
            isActive: (id) => true,
            getProportionForCircleDisplay: (id) => ({}),
            analysis: MOCKED_ANALYSES,
        },
    },
    callbacks: {
        updateDataLoaded: vi.fn(),
    },
};

const buildUrl = (params) => {
    return (
        buildRegionUrl(mockConfigUrl.api_analysis_meta_url, "auvergne-rhone-alpes") +
        params.id_analysis +
        "/data"
    );
};

const mockConfigUrl = config;

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildUrl(disabledAnalysisParams),
        results: disabledAnalysisParams.MOCK_DATA_INPUTS,
    },
];

for (let i = 0; i < MOCKED_ANALYSES.length; i++) {
    const element = MOCKED_ANALYSES[i];
    const params = analysisParams(element.id);
    MOCK_URLS_RESULTS.push({
        url: buildUrl(params),
        results: params.MOCK_DATA_INPUTS,
    });
}

let wrapper;

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
    wrapper = undefined;
});

afterEach(() => {
    vi.clearAllMocks();
});

/**
 * Check that the graph is not rendered when no indicator given
 */
test("renders the graph when unavailable", async () => {
    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={disabledAnalysisParams.id_analysis}
                id={disabledAnalysisParams.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(
        screen.getByText(
            "Cet indicateur n'est pas activé actuellement, veuillez contacter l'administrateur régional."
        )
    ).toBeInTheDocument();
    expect(screen.queryByText("ol-map-mocking")).not.toBeInTheDocument();
});

test("renders the graph with circles", async () => {
    const analysisParam = analysisParams(1);
    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.getByText("ol-map-mocking")).toBeInTheDocument();
});

test("renders the graph with stars", async () => {
    const analysisParam = analysisParams(3);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.getByText("ol-map-mocking")).toBeInTheDocument();
});

test("renders the graph with circle", async () => {
    const analysisParam = analysisParams(4);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.getByText("ol-map-mocking")).toBeInTheDocument();
});

test("renders the graph with pixels", async () => {
    const analysisParam = analysisParams(5);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.getByText("ol-map-mocking")).toBeInTheDocument();
});

test("renders the graph with pixels_cat", async () => {
    const analysisParam = analysisParams(6);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.getByText("ol-map-mocking")).toBeInTheDocument();
});

test("renders the graph with empty cats pixels_cat", async () => {
    const analysisParam = analysisParams(9);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.getByText("ol-map-mocking")).toBeInTheDocument();
    expect(
        screen.getByText("Pas de données disponibles pour ce territoire.")
    ).toBeInTheDocument();
});

test("renders the graph with flow", async () => {
    const analysisParam = analysisParams(7);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.getByText("ol-map-mocking")).toBeInTheDocument();
});

test("renders the graph with confidential data", async () => {
    const analysisParam = analysisParams(8);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.queryByText("ol-map-mocking")).not.toBeInTheDocument();
    expect(screen.getByText("Données confidentielles")).toBeInTheDocument();
});

test("renders the graph with no data for stars display", async () => {
    const analysisParam = analysisParams(10);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    expect(OlMap).toHaveBeenCalled();

    // we check nothing is available yet
    expect(screen.queryByText("ol-map-mocking")).toBeInTheDocument();
    expect(screen.queryByText("Pas de données")).toBeInTheDocument();
});

test("filters the graph with circles", async () => {
    const analysisParam = analysisParams(1);

    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisMap
                parentApi={parentApi}
                id_analysis={analysisParam.id_analysis}
                id={analysisParam.id}
                zoneType={parentApi.data.zone.zone}
                zoneId={parentApi.data.currentZone}
                zoneMaille={parentApi.data.zone.maille}
                provenance="tableau_de_bord_restitue"
                type={{ categorie: "cat_name", type: "type_name" }}
                color="#60B5DB"
            />
        );
        wrapper = container;
    });
    // 2 times?
    // the reason could be that map is shown at first without any data,
    // then it is rendered again when data are received!
    expect(OlMap).toHaveBeenCalled();

    // we check we have the right legend
    const legend = screen.queryByRole("group");
    expect(legend).toBeInTheDocument();
    expect(legend).toHaveTextContent("68");
    expect(legend).not.toHaveTextContent("20");

    const filters = screen.getAllByRole("checkbox");
    expect(filters).toHaveLength(2);

    fireEvent.click(filters[0]);
    expect(filters[0].checked).toEqual(false);
    // the legend has been updated
    waitFor(() => {
        const newLegend = screen.queryByRole("group");
        expect(newLegend).toBeInTheDocument();
        expect(newLegend).toHaveTextContent("20");
        expect(newLegend).not.toHaveTextContent("68");
    });

    // we reset to initial data
    fireEvent.click(filters[0]);
    expect(filters[0].checked).toEqual(true);
    waitFor(() => {
        const resetLegend = screen.queryByRole("group");
        expect(resetLegend).toBeInTheDocument();
        expect(resetLegend).toHaveTextContent("68");
        expect(resetLegend).not.toHaveTextContent("20");
    });
});
