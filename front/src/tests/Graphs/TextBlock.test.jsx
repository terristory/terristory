/*
* TerriSTORY®
*
© Copyright 2022 AURA-EE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* A copy of the GNU Affero General Public License should be present along
* with this program at the root of current repository. If not, see
* http://www.gnu.org/licenses/.
*/

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render } from "@testing-library/react";

import TextBlock from "../../Components/Graphs/TextBlock";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

/**
 * Check that the graph is correctfully rendered
 */
test("renders the module", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <TextBlock content="This is an example of **markdown** text!" />
        );
        wrapper = container;
    });

    expect(wrapper).toContainHTML(
        "<p>This is an example of <strong>markdown</strong> text!</p>"
    );
});

test("renders the module with injections to check sanitization", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <TextBlock content="This is an example of **markdown** text with injections: <script>alert('Hello');</script>!" />
        );
        wrapper = container;
    });

    expect(wrapper).toContainHTML(
        "<p>This is an example of <strong>markdown</strong> text with injections: !</p>"
    );
});
