export const PCAET_MOCKED_DATA = {
    historical_data: [
        {
            nom: "Agriculture, sylviculture et aquaculture",
            couleur: "#1fa22E",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 3000 },
                { annee: 2015, valeur: 3000 },
                { annee: 2016, valeur: 3000 },
                { annee: 2017, valeur: 2000 },
                { annee: 2018, valeur: 2000 },
                { annee: 2019, valeur: 2000 },
                { annee: 2020, valeur: 2000 },
                { annee: 2021, valeur: 2000 },
            ],
        },
        {
            nom: "Gestion des déchets",
            couleur: "#933588",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 1000 },
                { annee: 2015, valeur: 1000 },
                { annee: 2016, valeur: 1000 },
                { annee: 2017, valeur: 1000 },
                { annee: 2018, valeur: 1000 },
                { annee: 2019, valeur: 1000 },
                { annee: 2020, valeur: 1000 },
                { annee: 2021, valeur: 1000 },
            ],
        },
        {
            nom: "Résidentiel",
            couleur: "#fecf44",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 64000 },
                { annee: 2015, valeur: 64000 },
                { annee: 2016, valeur: 62000 },
                { annee: 2017, valeur: 62000 },
                { annee: 2018, valeur: 64000 },
                { annee: 2019, valeur: 62000 },
                { annee: 2020, valeur: 61000 },
                { annee: 2021, valeur: 62000 },
            ],
        },
        {
            nom: "Transport routier",
            couleur: "#a74d47",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 69000 },
                { annee: 2015, valeur: 69000 },
                { annee: 2016, valeur: 71000 },
                { annee: 2017, valeur: 71000 },
                { annee: 2018, valeur: 72000 },
                { annee: 2019, valeur: 72000 },
                { annee: 2020, valeur: 58000 },
                { annee: 2021, valeur: 61000 },
            ],
        },
        {
            nom: "Industrie hors branche énergie",
            couleur: "#cfd1d2",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 45000 },
                { annee: 2015, valeur: 46000 },
                { annee: 2016, valeur: 45000 },
                { annee: 2017, valeur: 45000 },
                { annee: 2018, valeur: 46000 },
                { annee: 2019, valeur: 47000 },
                { annee: 2020, valeur: 43000 },
                { annee: 2021, valeur: 44000 },
            ],
        },
        {
            nom: "Autres transports",
            couleur: "#91d4f2",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 1000 },
                { annee: 2015, valeur: 1000 },
                { annee: 2016, valeur: 1000 },
                { annee: 2017, valeur: 1000 },
                { annee: 2018, valeur: 2000 },
                { annee: 2019, valeur: 1000 },
                { annee: 2020, valeur: 1000 },
                { annee: 2021, valeur: 1000 },
            ],
        },
        {
            nom: "Tertiaire",
            couleur: "#f29400",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 32000 },
                { annee: 2015, valeur: 32000 },
                { annee: 2016, valeur: 31000 },
                { annee: 2017, valeur: 32000 },
                { annee: 2018, valeur: 31000 },
                { annee: 2019, valeur: 31000 },
                { annee: 2020, valeur: 30000 },
                { annee: 2021, valeur: 30000 },
            ],
        },
        {
            nom: "Industrie branche énergie",
            couleur: "#d1d3ac",
            confidentiel: "non",
            data: [
                { annee: 2014, valeur: 6000 },
                { annee: 2015, valeur: 6000 },
                { annee: 2016, valeur: 6000 },
                { annee: 2017, valeur: 6000 },
                { annee: 2018, valeur: 5000 },
                { annee: 2019, valeur: 5000 },
                { annee: 2020, valeur: 4000 },
                { annee: 2021, valeur: 5000 },
            ],
        },
    ],
    pcaet_trajectory: [
        {
            nom: "Agriculture, sylviculture et aquaculture",
            data: [
                { annee: 2015, valeur: 3000 },
                { annee: 2021, valeur: 3000 },
                { annee: 2026, valeur: 3000 },
                { annee: 2030, valeur: 2000 },
                { annee: 2050, valeur: 0 },
            ],
        },
        {
            nom: "Gestion des déchets",
            data: [
                { annee: 2015, valeur: 1000 },
                { annee: 2021, valeur: 1000 },
                { annee: 2026, valeur: 1000 },
                { annee: 2030, valeur: 500 },
                { annee: 2050, valeur: 50 },
            ],
        },
        {
            nom: "Résidentiel",
            data: [
                { annee: 2015, valeur: 64000 },
                { annee: 2021, valeur: 62000 },
                { annee: 2026, valeur: 50000 },
                { annee: 2030, valeur: 25000 },
                { annee: 2050, valeur: 5000 },
            ],
        },
        {
            nom: "Transport routier",
            data: [
                { annee: 2015, valeur: 69000 },
                { annee: 2021, valeur: 61000 },
                { annee: 2026, valeur: 50000 },
                { annee: 2030, valeur: 30000 },
                { annee: 2050, valeur: 5000 },
            ],
        },
        {
            nom: "Industrie hors branche énergie",
            data: [
                { annee: 2015, valeur: 46000 },
                { annee: 2021, valeur: 44000 },
                { annee: 2026, valeur: 30000 },
                { annee: 2030, valeur: 15000 },
                { annee: 2050, valeur: 3000 },
            ],
        },
        {
            nom: "Autres transports",
            data: [
                { annee: 2015, valeur: 1000 },
                { annee: 2021, valeur: 1000 },
                { annee: 2026, valeur: 1200 },
                { annee: 2030, valeur: 500 },
                { annee: 2050, valeur: 0 },
            ],
        },
        {
            nom: "Tertiaire",
            data: [
                { annee: 2015, valeur: 32000 },
                { annee: 2021, valeur: 30000 },
                { annee: 2026, valeur: 25000 },
                { annee: 2030, valeur: 15000 },
                { annee: 2050, valeur: 1000 },
            ],
        },
        {
            nom: "Industrie branche énergie",
            data: [
                { annee: 2015, valeur: 6000 },
                { annee: 2021, valeur: 5000 },
                { annee: 2026, valeur: 4000 },
                { annee: 2030, valeur: 2000 },
                { annee: 2050, valeur: 0 },
            ],
        },
    ],
    supra_goals: [
        {
            titre: "SRADDET 2030 AURA - Conso",
            couleur: "#780000",
            annee_reference: 2015,
            valeur_reference: 221000,
            affectation: '{"epcis":""}',
            table: "DataSet.CONSUMPTION",
            filter: [],
            valeurs_annees: [
                { annee: 2020, valeur: -5 },
                { annee: 2025, valeur: -10 },
                { annee: 2030, valeur: -15 },
            ],
        },
    ],
    unit: "GWh",
    ref_year: "2015",
    category: "secteur",
};

export const INITIAL_FILTERS = {
    secteur: [
        {
            filtre_categorie: "secteur.Agriculture, sylviculture et aquaculture",
        },
        { filtre_categorie: "secteur.Gestion des déchets" },
        { filtre_categorie: "secteur.Résidentiel" },
        { filtre_categorie: "secteur.Transport routier" },
        {
            filtre_categorie: "secteur.Industrie hors branche énergie",
        },
        { filtre_categorie: "secteur.Autres transports" },
        // { filtre_categorie: "secteur.Tertiaire" },
        { filtre_categorie: "secteur.Industrie branche énergie" },
    ],
};

export const MOCKED_PCAET_PARAM = {
    id: "emissions",
    name: "Émissions de GES",
    unit: "GWh",
    categories: {
        secteur: {
            categorie: "secteur",
            titre: "Par secteur",
            visible: true,
        },
    },
    filters: {
        secteur: [
            {
                filtre_categorie: "secteur.Agriculture, sylviculture et aquaculture",
            },
            { filtre_categorie: "secteur.Gestion des déchets" },
            { filtre_categorie: "secteur.Résidentiel" },
            { filtre_categorie: "secteur.Transport routier" },
            {
                filtre_categorie: "secteur.Industrie hors branche énergie",
            },
            { filtre_categorie: "secteur.Autres transports" },
            { filtre_categorie: "secteur.Tertiaire" },
            { filtre_categorie: "secteur.Industrie branche énergie" },
        ],
    },
};

export const CONFID_PCAET_MOCKED_DATA = {
    historical_data: [
        {
            nom: "Agriculture, sylviculture et aquaculture",
            couleur: "#1fa22E",
            confidentiel: "oui",
            data: [
                { annee: 2014, valeur: 3000 },
                { annee: 2015, valeur: 3000 },
                { annee: 2016, valeur: 3000 },
                { annee: 2017, valeur: 2000 },
                { annee: 2018, valeur: 2000 },
                { annee: 2019, valeur: 2000 },
                { annee: 2020, valeur: 2000 },
                { annee: 2021, valeur: 2000 },
            ],
        },
    ],
    pcaet_trajectory: [
        {
            nom: "Agriculture, sylviculture et aquaculture",
            data: [
                { annee: 2015, valeur: 3000 },
                { annee: 2021, valeur: 3000 },
                { annee: 2026, valeur: 3000 },
                { annee: 2030, valeur: 2000 },
                { annee: 2050, valeur: 0 },
            ],
        },
    ],
    unit: "GWh",
    ref_year: "2015",
    category: "secteur",
};

export const FULL_CONFID_PCAET_MOCKED_DATA = {
    historical_data: "Confidentiel",
    pcaet_trajectory: [
        {
            nom: "Agriculture, sylviculture et aquaculture",
            data: [
                { annee: 2015, valeur: 3000 },
                { annee: 2021, valeur: 3000 },
                { annee: 2026, valeur: 3000 },
                { annee: 2030, valeur: 2000 },
                { annee: 2050, valeur: 0 },
            ],
        },
    ],
    unit: "GWh",
    ref_year: "2015",
    category: "secteur",
};
