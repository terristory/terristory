/*
* TerriSTORY®
*
© Copyright 2022 AURA-EE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* A copy of the GNU Affero General Public License should be present along
* with this program at the root of current repository. If not, see
* http://www.gnu.org/licenses/.
*/

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";

import DataTable from "../../Components/Graphs/DataTable";
import config from "../../settings";
import Api from "../../Controllers/Api";
import { buildRegionUrl } from "../../utils";
import { PCAET_MOCKED_DATA as MOCK_DATA_INPUTS } from "./pcaet_data";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
        zone: {
            zone: "departement",
            maille: "epci",
        },
        currentZone: "01",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        analysisManager: {
            initFiltersByCategory: () => ({}),
        },
        dashboardManager: {
            applyBottomMargin: () => undefined,
        },
    },
    callbacks: {
        updateDataLoaded: () => {},
    },
};

let codeInseeTerritoire = parentApi.data.currentZone;
let pZone = "?zone=" + parentApi.data.zone.zone;
let pMaille = "&maille=" + parentApi.data.zone.maille;
let pZoneId = "&zone_id=" + codeInseeTerritoire;
let urlArgs = pZone + pMaille + pZoneId;

const analysisParams = {
    id_analysis: "energy_consumption",
};
const disabledAnalysisParams = {
    id_analysis: "pollutant",
};
const mockConfigUrl = config;

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url:
            buildRegionUrl(
                mockConfigUrl.api_pcaet_trajectory_details,
                parentApi.data.region
            ).replace("#trajectory_id#", analysisParams.id_analysis) + urlArgs,
        results: MOCK_DATA_INPUTS,
    },
    {
        url:
            buildRegionUrl(
                mockConfigUrl.api_pcaet_trajectory_details,
                parentApi.data.region
            ).replace("#trajectory_id#", disabledAnalysisParams.id_analysis) + urlArgs,
        results: { disabled: true },
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the graph is correctfully rendered
 */
test("renders the module", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <DataTable
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
        wrapper = container;
    });

    expect(Api.callApi).toHaveBeenCalled();
    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(
            mockConfigUrl.api_pcaet_trajectory_details,
            parentApi.data.region
        ).replace("#trajectory_id#", analysisParams.id_analysis) + urlArgs
    );

    expect(
        screen.getByRole("columnheader", {
            name: "Agriculture, sylviculture et aquaculture",
        })
    ).toBeInTheDocument();
});

/**
 * Check that the graph is correctfully disabled
 */
test("renders the module disabled", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <DataTable
                parentApi={parentApi}
                id_analysis={disabledAnalysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
        wrapper = container;
    });

    expect(Api.callApi).toHaveBeenCalled();
    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(
            mockConfigUrl.api_pcaet_trajectory_details,
            parentApi.data.region
        ).replace("#trajectory_id#", disabledAnalysisParams.id_analysis) + urlArgs
    );

    expect(
        screen.getByText(
            "Cet indicateur n'est pas activé actuellement, veuillez contacter l'administrateur régional."
        )
    ).toBeInTheDocument();
});

/**
 * Check that the graph is correctfully rendered
 */
test("renders the module at regional level", async () => {
    parentApi.data.zone = {
        zone: "region",
        maille: "epci",
    };
    parentApi.data.currentZone = "01";
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <DataTable
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
        wrapper = container;
    });

    expect(
        screen.getByText("Aucune donnée disponible actuellement.")
    ).toBeInTheDocument();
});
