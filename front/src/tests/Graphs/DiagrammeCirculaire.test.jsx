/*
* TerriSTORY®
*
© Copyright 2022 AURA-EE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* A copy of the GNU Affero General Public License should be present along
* with this program at the root of current repository. If not, see
* http://www.gnu.org/licenses/.
*/

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render } from "@testing-library/react";

import DiagrammeCirculaire from "../../Components/Graphs/DiagrammeCirculaire";
import config from "../../settings";
import Api from "../../Controllers/Api";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

vi.mock("chartjs-plugin-datalabels", () => {
    return {};
});

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
        zone: {
            zone: "departement",
            maille: "epci",
        },
        currentZone: "01",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        analysisManager: {
            initFiltersByCategory: () => ({}),
        },
        dashboardManager: {
            applyBottomMargin: () => undefined,
        },
    },
    callbacks: {
        updateDataLoaded: () => {},
    },
};
const analysisParams = {
    id_analyse: 1,
    representation: "pie",
};
const mockConfigUrl = config;

const MOCK_DATA_INPUTS = {
    map: [],
    confid: {
        charts: [],
    },
    charts: [
        {
            indicateur: 1,
            name: "secteur",
            data: [1000.0, 600.0],
            labels: ["R\u00e9sidentiel", "Transport"],
            colors: ["#60B5DB", "#C1C0C0"],
            modalites_disponibles: [
                "R\u00e9sidentiel",
                "Tertiaire",
                "Industrie",
                "Transport",
                "Agriculture",
            ],
        },
        {
            indicateur: 47,
            name: "energie",
            data: [400.0, 0, 1200.0],
            labels: ["Electricit\u00e9", "Gaz"],
            colors: ["#A6DEF7", "#C5C24D"],
            modalites_disponibles: ["Gaz", "Electricit\u00e9"],
        },
    ],
    donnees_par_categorie: {
        secteur: [
            {
                modalite: "R\u00e9sidentiel",
                couleur: "#60B5DB",
                val: 35036.58517677774,
            },
            {
                modalite: "Transport",
                couleur: "#C1C0C0",
                val: 38994.70936689802,
            },
        ],
        energie: [
            {
                modalite: "Electricit\u00e9",
                couleur: "#A6DEF7",
                val: 32721.182564601007,
            },
            {
                modalite: "Gaz",
                couleur: "#C5C24D",
                val: 15952.426632721395,
            },
        ],
    },
    total: {
        val: 110489.0,
        divider: null,
    },
    min_max_valeurs: {
        min: 4.0,
        max: 13320.0,
    },
    filtre_initial_modalites_par_categorie: {
        energie: [
            {
                filtre_categorie: "energie.Electricit\u00e9",
            },
            {
                filtre_categorie: "energie.Gaz",
            },
        ],
        secteur: [
            {
                filtre_categorie: "secteur.Transport",
            },
            {
                filtre_categorie: "secteur.R\u00e9sidentiel",
            },
        ],
    },
    filtre_initial: {
        energie: [
            {
                filtre_categorie: "energie.Electricit\u00e9",
            },
            {
                filtre_categorie: "energie.Gaz",
            },
        ],
        secteur: [
            {
                filtre_categorie: "secteur.Transport",
            },
            {
                filtre_categorie: "secteur.R\u00e9sidentiel",
            },
        ],
    },
    afficher_calcul_et_donnees_table: null,
    moyenne_ponderee: null,
};
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url:
            mockConfigUrl.api_analysis_meta_url +
            analysisParams.id_analyse +
            "/graphique/" +
            analysisParams.representation,
        results: MOCK_DATA_INPUTS,
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the graph is correctfully rendered
 */
test("renders the page for admin connected user", async () => {
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <DiagrammeCirculaire
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
        wrapper = container;
    });

    // we find send button
    const mainDiv = wrapper.getElementsByClassName(".charts-legende");
    expect(mainDiv).toBeDefined();
    expect(mainDiv).not.toBeNull();

    // we find send button
    const clickableLink = wrapper.getElementsByClassName(".analysis-launcher-button");
    expect(clickableLink).toBeDefined();
    expect(clickableLink).not.toBeNull();
});
