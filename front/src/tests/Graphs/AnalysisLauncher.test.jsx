/*
* TerriSTORY®
*
© Copyright 2022 AURA-EE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* A copy of the GNU Affero General Public License should be present along
* with this program at the root of current repository. If not, see
* http://www.gnu.org/licenses/.
*/

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, fireEvent } from "@testing-library/react";

import AnalysisLauncher from "../../Components/Graphs/AnalysisLauncher";
import config from "../../settings";
import Api from "../../Controllers/Api";
import { buildRegionUrl } from "../../utils";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
        zone: {
            zone: "departement",
            maille: "epci",
        },
        currentZone: "01",
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        zonesManager: {
            getZoneName: () => {
                return "test";
            },
        },
        analysisManager: {
            getAnalysisNameTheme: () => {
                return "name analysis";
            },
        },
    },
    callbacks: {
        updateAnalysisNameColor: vi.fn(),
        tailleDiv: vi.fn(),
    },
};
const analysisParams = {
    id_analysis: 1,
    representation: "analysis-launcher",
    MOCK_DATA_INPUTS: {
        valeur: 10.0,
        unite: "Euros",
        id_indicateur: 1,
        nom: "Ceci est un test",
    },
};
const disabledAnalysisParams = {
    id_analysis: 2,
    representation: "analysis-launcher",
    MOCK_DATA_INPUTS: {
        disabled: true,
    },
};

const buildUrl = (params) => {
    return (
        buildRegionUrl(mockConfigUrl.api_analysis_meta_url, "auvergne-rhone-alpes") +
        params.id_analysis +
        "/graphique/" +
        params.representation
    );
};

const mockConfigUrl = config;

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url: buildUrl(analysisParams),
        results: analysisParams.MOCK_DATA_INPUTS,
    },
    {
        url: buildUrl(disabledAnalysisParams),
        results: disabledAnalysisParams.MOCK_DATA_INPUTS,
    },
];

let wrapper;

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
    wrapper = undefined;
});

/**
 * Check that the graph is correctfully rendered
 */
test("renders the graph", async () => {
    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisLauncher
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                provenance="tableau_de_bord_restitue"
                color="#60B5DB"
            />
        );
        wrapper = container;
    });

    // we find send button
    const mainDiv = wrapper.getElementsByClassName("charts-legende");
    expect(mainDiv.length).toBe(1);
    // we find send button
    const contentDisplayButton = wrapper.getElementsByClassName(
        "analysis-launcher-button"
    );
    expect(contentDisplayButton.length).toBe(1);
    // check div content;

    const clickableLink = wrapper.getElementsByClassName("btn");
    expect(clickableLink.length).toBe(1);

    const spyWindowOpen = vi.spyOn(window, "open");
    spyWindowOpen.mockImplementation(vi.fn());

    const mockPathname = vi.fn();
    Object.defineProperty(window, "location", {
        value: {
            get pathname() {
                return mockPathname();
            },
        },
    });

    // we mock update callback
    parentApi.callbacks.mettreAJourParametresUrls = vi.fn();
    mockPathname.mockReturnValue("some");
    fireEvent.click(clickableLink[0]);
    expect(parentApi.callbacks.mettreAJourParametresUrls).toBeCalled();
    expect(spyWindowOpen).toBeCalled();
    expect(spyWindowOpen).toHaveBeenCalledWith(
        "http://localhost:3000?zone=departement&maille=epci&zone_id=01&analysis=1&theme=name analysis&nom_territoire=" +
            parentApi.controller.zonesManager.getZoneName(),
        "_blank"
    );
});

/**
 * Check that the graph is correctfully rendered
 */
test("renders the graph in dashboard creation", async () => {
    // we render
    let _rerender;
    await act(async () => {
        const { rerender, container } = render(
            <AnalysisLauncher
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                provenance="tableau_de_bord_restitue"
                color="#60B5DB"
            />
        );
        wrapper = container;
        _rerender = rerender;
    });

    // we find send button
    const mainDiv = wrapper.getElementsByClassName("charts-legende");
    expect(mainDiv.length).toBe(1);
    // we find send button
    const contentDisplayButton = wrapper.getElementsByClassName(
        "analysis-launcher-button"
    );
    expect(contentDisplayButton.length).toBe(1);
    // check div content
    expect(contentDisplayButton[0]).toHaveTextContent(
        analysisParams.MOCK_DATA_INPUTS.nom
    );

    let clickableLink = wrapper.getElementsByClassName("btn");
    expect(clickableLink.length).toBe(1);

    const spyWindowOpen = vi.spyOn(window, "open");
    spyWindowOpen.mockImplementation(vi.fn());

    const mockPathname = vi.fn();
    Object.defineProperty(window, "location", {
        value: {
            get pathname() {
                return mockPathname();
            },
        },
    });
    mockPathname.mockReturnValue("some");

    // we update props
    // re-render the same component with different props
    _rerender(
        <AnalysisLauncher
            parentApi={parentApi}
            id_analysis={analysisParams.id_analysis}
            id={analysisParams.id}
            provenance="tableau_de_bord"
            color="#60B5DB"
        />
    );

    // we mock update callback
    parentApi.callbacks.mettreAJourParametresUrls = vi.fn();
    clickableLink = wrapper.getElementsByClassName("btn");
    expect(clickableLink.length).toBe(1);
    fireEvent.click(clickableLink[0]);
    expect(parentApi.callbacks.mettreAJourParametresUrls).toHaveBeenCalledTimes(1);
});

/**
 * Check when the graph is disabled
 */
test("renders a disabled graph", async () => {
    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisLauncher
                parentApi={parentApi}
                id_analysis={disabledAnalysisParams.id_analysis}
                id={disabledAnalysisParams.id}
                provenance="tableau_de_bord_restitue"
                color="#60B5DB"
            />
        );
        wrapper = container;
    });

    // we find send button
    const mainDiv = wrapper.getElementsByClassName("charts-legende");
    expect(mainDiv.length).toBe(1);
    // we don't find the button
    const clickableLink = wrapper.getElementsByClassName("analysis-launcher-button");
    expect(clickableLink.length).toBe(0);
    // we find disabled message
    const disabledMessage = wrapper.getElementsByClassName("confid-chart");
    expect(disabledMessage.length).toBe(1);
    expect(disabledMessage[0]).toHaveTextContent(
        "Cet indicateur n'est pas activé actuellement, veuillez contacter l'administrateur régional."
    );
});

/**
 * Check that the graph is correctfully edited
 */
test("edit the graph", async () => {
    // we render
    await act(async () => {
        const { container } = render(
            <AnalysisLauncher
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
        wrapper = container;
    });

    // we find send button
    const mainDiv = wrapper.getElementsByClassName("charts-legende");
    expect(mainDiv.length).toBe(1);
    // we find form to edit the color
    const clickableLink = wrapper.getElementsByClassName(
        "formulaire-horizontal-alignement-vertical-centre"
    );
    expect(clickableLink.length).toBe(1);
    // we do not find color picker
    const notPresentColorPicker =
        wrapper.getElementsByClassName("color-picker-popover");
    expect(notPresentColorPicker.length).toBe(0);

    // we find form to edit the color
    const clickOnEditTrigger = wrapper.getElementsByClassName("color-picker-swatch");
    expect(clickOnEditTrigger.length).toBe(1);
    fireEvent.click(clickOnEditTrigger[0]);

    // we do not find color picker
    const presentColorPicker = wrapper.getElementsByClassName("color-picker-popover");
    expect(presentColorPicker.length).toBe(1);
});
