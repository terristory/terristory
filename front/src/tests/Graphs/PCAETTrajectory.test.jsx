/*
* TerriSTORY®
*
© Copyright 2022 AURA-EE
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU Affero General Public License for more details.
*
* A copy of the GNU Affero General Public License should be present along
* with this program at the root of current repository. If not, see
* http://www.gnu.org/licenses/.
*/

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";

import PCAETTrajectory from "../../Components/Graphs/PCAETTrajectory";
import config from "../../settings";
import Api from "../../Controllers/Api";
import { buildRegionUrl } from "../../utils";
import {
    PCAET_MOCKED_DATA,
    MOCKED_PCAET_PARAM,
    CONFID_PCAET_MOCKED_DATA,
    FULL_CONFID_PCAET_MOCKED_DATA,
    INITIAL_FILTERS,
} from "./pcaet_data";

vi.mock("../../Controllers/Api.js");
vi.mock("../../utils.js");

vi.mock("react-chartjs-2", () => ({
    Chart: {
        register: () => null,
    },
    Line: () => null,
}));

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
        zone: {
            zone: "departement",
            maille: "epci",
        },
        currentZone: "01",
        tableauBordDonnees: {
            donnees: {
                energie: {
                    indicateurs: {
                        "departement-01-epci-02": {
                            filters: undefined,
                        },
                        "departement-01-epci-03": {
                            filters: undefined,
                        },
                    },
                },
            },
        },
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        analysisManager: {
            initFiltersByCategory: () => ({}),
            getDetailsOnTrajectory: () => {
                return MOCKED_PCAET_PARAM;
            },
        },
        dashboardManager: {
            applyBottomMargin: () => undefined,
        },
    },
    callbacks: {
        updateDataLoaded: () => {},
        updateDashboard: vi.fn(),
    },
};

let codeInseeTerritoire = parentApi.data.currentZone;
let pZone = "?zone=" + parentApi.data.zone.zone;
let pMaille = "&maille=" + parentApi.data.zone.maille;
let pZoneId = "&zone_id=" + codeInseeTerritoire;
let urlArgs = pZone + pMaille + pZoneId;

const analysisParams = {
    id_analysis: "energy_consumption",
    id: "departement-01-epci-02",
};
const disabledAnalysisParams = {
    id_analysis: "pollutant",
    id: "departement-01-epci-03",
};
const confidAnalysisParams = {
    id_analysis: "pollutant-two",
    id: "departement-01-epci-03",
};
const fullConfidAnalysisParams = {
    id_analysis: "confid-pollutant",
    id: "departement-01-epci-03",
};
const mockConfigUrl = config;

// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url:
            buildRegionUrl(
                mockConfigUrl.api_pcaet_trajectory_details,
                parentApi.data.region
            ).replace("#trajectory_id#", analysisParams.id_analysis) + urlArgs,
        results: PCAET_MOCKED_DATA,
    },
    {
        url:
            buildRegionUrl(
                mockConfigUrl.api_pcaet_trajectory_details,
                parentApi.data.region
            ).replace("#trajectory_id#", disabledAnalysisParams.id_analysis) + urlArgs,
        results: { disabled: true },
    },
    {
        url:
            buildRegionUrl(
                mockConfigUrl.api_pcaet_trajectory_details,
                parentApi.data.region
            ).replace("#trajectory_id#", confidAnalysisParams.id_analysis) + urlArgs,
        results: CONFID_PCAET_MOCKED_DATA,
    },
    {
        url:
            buildRegionUrl(
                mockConfigUrl.api_pcaet_trajectory_details,
                parentApi.data.region
            ).replace("#trajectory_id#", fullConfidAnalysisParams.id_analysis) +
            urlArgs,
        results: FULL_CONFID_PCAET_MOCKED_DATA,
    },
];

beforeEach(() => {
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the graph is correctfully rendered
 */
test("renders the module", async () => {
    // we render
    await act(async () => {
        render(
            <PCAETTrajectory
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
                filters={INITIAL_FILTERS}
            />
        );
    });

    expect(Api.callApi).toHaveBeenCalled();
    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(
            mockConfigUrl.api_pcaet_trajectory_details,
            parentApi.data.region
        ).replace("#trajectory_id#", analysisParams.id_analysis) + urlArgs
    );

    // we check that legend successfully showed
    const enabledCheckbox = screen.getByRole("checkbox", {
        name: "Agriculture, sylviculture et aquaculture",
    });
    expect(enabledCheckbox).toBeInTheDocument();
    expect(enabledCheckbox).toBeChecked();
    const disabledCheckbox = screen.getByRole("checkbox", {
        name: "Tertiaire",
    });
    expect(disabledCheckbox).toBeInTheDocument();
    expect(disabledCheckbox).not.toBeChecked();
});

/**
 * Check that the graph is correctfully disabled
 */
test("renders the module disabled", async () => {
    // we render
    await act(async () => {
        render(
            <PCAETTrajectory
                parentApi={parentApi}
                id_analysis={disabledAnalysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
    });

    expect(Api.callApi).toHaveBeenCalled();
    expect(Api.callApi).toHaveBeenCalledWith(
        buildRegionUrl(
            mockConfigUrl.api_pcaet_trajectory_details,
            parentApi.data.region
        ).replace("#trajectory_id#", disabledAnalysisParams.id_analysis) + urlArgs
    );

    expect(
        screen.getByText(
            "Cet indicateur n'est pas activé actuellement, veuillez contacter l'administrateur régional."
        )
    ).toBeInTheDocument();
});

/**
 * Check that the legend works
 */
test("renders the module and filter on legend", async () => {
    // we render
    await act(async () => {
        render(
            <PCAETTrajectory
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
                filters={INITIAL_FILTERS}
            />
        );
    });

    const initalFiltersLength = INITIAL_FILTERS.secteur.length;

    // we check that legend successfully showed
    const legendButton = screen.getByRole("checkbox", {
        name: "Agriculture, sylviculture et aquaculture",
    });
    expect(legendButton).toBeInTheDocument();

    // try to disable the checkbox
    await act(async () => {
        fireEvent.click(legendButton);
    });
    let { calls } = parentApi.callbacks.updateDashboard.mock;
    expect(calls).toHaveLength(1);
    expect(calls[0]?.[4]?.filters?.secteur?.length).toBeDefined();
    expect(calls[0][4].filters.secteur).toHaveLength(initalFiltersLength - 1);
    expect(
        calls[0][4].filters.secteur.map(({ filtre_categorie }) => filtre_categorie)
    ).not.toContain("secteur.Agriculture, sylviculture et aquaculture");

    // re-enable the checkbox
    await act(async () => {
        fireEvent.click(legendButton);
    });
    ({ calls } = parentApi.callbacks.updateDashboard.mock);
    expect(calls).toHaveLength(2);
    expect(calls[1]?.[4]?.filters?.secteur?.length).toBeDefined();
    expect(calls[1][4].filters.secteur).toHaveLength(initalFiltersLength);
    expect(
        calls[1][4].filters.secteur.map(({ filtre_categorie }) => filtre_categorie)
    ).toContain("secteur.Agriculture, sylviculture et aquaculture");
});

/**
 * Check that the confid works
 */
test("renders the module with confid on one sector", async () => {
    // we render
    await act(async () => {
        render(
            <PCAETTrajectory
                parentApi={parentApi}
                id_analysis={confidAnalysisParams.id_analysis}
                id={confidAnalysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
    });

    // we check that legend successfully showed
    const legendButton = screen.getByRole("checkbox", {
        name: "Agriculture, sylviculture et aquaculture",
    });
    expect(legendButton).toBeInTheDocument();
    expect(legendButton).toBeDisabled();
});

/**
 * Check that the confid works
 */
test("renders the module with confid on whole historical traj", async () => {
    // we render
    await act(async () => {
        render(
            <PCAETTrajectory
                parentApi={parentApi}
                id_analysis={fullConfidAnalysisParams.id_analysis}
                id={fullConfidAnalysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
    });

    waitFor(() => {
        expect(screen.getByText("Données confidentielles")).toBeInTheDocument();
    });
});

/**
 * Check that the graph is not rendered when looking at another level
 */
test("renders the module at regional level", async () => {
    parentApi.data.zone = {
        zone: "region",
        maille: "epci",
    };
    parentApi.data.currentZone = "01";
    // we render
    await act(async () => {
        render(
            <PCAETTrajectory
                parentApi={parentApi}
                id_analysis={analysisParams.id_analysis}
                id={analysisParams.id}
                thematique={"energie"}
                type={{ categorie: "energie" }}
                provenance="tableau_de_bord"
                color="#60B5DB"
            />
        );
    });

    expect(
        screen.getByText(
            "Catégorie non disponible pour cet indicateur, veuillez contacter l'administrateur régional."
        )
    ).toBeInTheDocument();
});
