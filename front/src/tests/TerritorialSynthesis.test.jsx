/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

"use strict";

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import TerritorialSynthesis from "../Components/TerritorialSynthesis";
import config from "../settings";
import { buildRegionUrl } from "../utils";
import Api from "../Controllers/Api";

vi.mock("../Controllers/Api.js");
vi.mock("../utils.js");
const { getComputedStyle } = window;
window.getComputedStyle = (elt) => getComputedStyle(elt);

vi.mock("react-chartjs-2", () => ({
    Bar: () => null,
}));

vi.mock("jspdf", () => ({
    default: vi.fn(() => ({
        internal: {
            pageSize: {
                getWidth: vi.fn(),
            },
        },
        addImage: vi.fn(),
        addPage: vi.fn(),
        save: vi.fn(),
    })),
}));
vi.mock("html2canvas", () => ({
    default: vi.fn(() =>
        Promise.resolve({
            toDataURL: () => "IMG DATA",
        })
    ),
}));

const estimatedAnalysis = 15;

const parentApi = {
    data: {
        region: "pytest",
        zone: {
            zone: "epci",
            maille: "commune",
        },
        nomTerritoire: "Nowhere",
        settings: {
            // mocking general TerritorialSynthesis page metadata
            cesba_notes: "<div id='part1'>Notes page header</div>",
            cesba_elements: JSON.stringify({
                titre: "Notes description giving information",
                pied: "Nothing in the footer",
                "cesba-logo": [
                    { "logo-entete": "logo.png", "logo-pied": "footer-logo.png" },
                ],
            }),
        },
    },
    callbacks: {
        mettreAJourParametresUrls: vi.fn(),
        updateAnalysis: vi.fn(),
    },
    controller: {
        zonesManager: {
            getZoneName: vi.fn(),
        },
        analysisManager: {
            getAnalysisYears: () => {
                return [];
            },
            getAnalysisEstimatedYears: (i) => {
                return i === estimatedAnalysis ? ["2020"] : [];
            },
        },
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
    },
};
const mockConfigUrl = config;

const MOCK_STATS_TYPE = [
    {
        id: 1,
        nom: "First indicator",
        numero: "B8",
        theme: "B - Second category",
        unit: "kg",
        note: 4.54,
        median_note: 2.49,
        median: 5,
        vmin: 0,
        vmax: 20,
        years: 2021,
    },
    {
        id: 2,
        nom: "\u00c9missions GES tertiaire / employ\u00e9",
        numero: "A8",
        theme: "A - First category",
        unit: "t",
        note: 4.19,
        median_note: 3.12,
        median: 3,
        vmin: 2,
        vmax: 5,
        years: 2021,
    },
    {
        id: estimatedAnalysis,
        nom: "\u00c9missions GES tertiaire / employ\u00e9",
        numero: "A2",
        theme: "A - First category",
        unit: "t",
        note: 4.19,
        median_note: 3.12,
        median: 3,
        vmin: 2,
        vmax: 5,
        years: 2020,
    },
];
// we first describe the results associated with URLs used in this module
const MOCK_URLS_RESULTS = [
    {
        url:
            buildRegionUrl(mockConfigUrl.api_notes_territorialsynthesis_url, "pytest") +
            "epci/1?id_utilisateur=-1",
        results: MOCK_STATS_TYPE,
    },
    {
        url:
            buildRegionUrl(mockConfigUrl.api_notes_territorialsynthesis_url, "pytest") +
            "departement/1?id_utilisateur=-1",
        results: MOCK_STATS_TYPE,
    },
    {
        url:
            buildRegionUrl(mockConfigUrl.api_notes_territorialsynthesis_url, "pytest") +
            "epci/2?id_utilisateur=-1",
        results: null,
    },
];

beforeEach(() => {
    vi.clearAllMocks();
    // Set up some mocked out url results info before each test
    Api.__setMockUrlsResults(MOCK_URLS_RESULTS);
});

/**
 * Check that the page is unavailable when data are missing from the back.
 */
test("renders the page with missing data", async () => {
    parentApi.data.currentZone = "2";
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <TerritorialSynthesis parentApi={parentApi} fromMenu={true} />
            </MemoryRouter>
        );
        wrapper = container;
    });
    // we have an error message
    expect(wrapper).toHaveTextContent("Non accessible pour ce territoire.");
});

/**
 * Check the page is rendered with the right territory and right amount of buttons.
 */
test("renders the page with real data", async () => {
    parentApi.data.currentZone = "1";

    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <TerritorialSynthesis parentApi={parentApi} fromMenu={true} />
            </MemoryRouter>
        );
        wrapper = container;
    });
    // we check the title is correctly displayed
    expect(
        screen.getByRole("heading", { name: "Notes description giving information" })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("heading", {
            name: "Territoire : " + parentApi.data.nomTerritoire,
        })
    ).toBeInTheDocument();

    // we check we do have the three buttons
    // close, print, export data
    expect(screen.getAllByRole("button")).toHaveLength(3);

    // we check we do have six tables
    // one for each row (3) + one for each separator (2) + one for header
    expect(screen.getAllByRole("table")).toHaveLength(6);
});

/**
 * Check that the page is rendered with specific changes for Departement level.
 */
test("renders the page with departement data", async () => {
    parentApi.data.currentZone = "1";
    parentApi.data.zone.zone = "departement";

    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <TerritorialSynthesis parentApi={parentApi} fromMenu={true} />
            </MemoryRouter>
        );
        wrapper = container;
    });
    // we check the title is correctly displayed
    expect(wrapper).toHaveTextContent(
        "Profil du territoire par rapport à la valeur médiane* des DÉPARTEMENTS"
    );
});

/**
 * Check that the page is rendered and show correct mocked data inside table.
 */
test("renders the page with correct data", async () => {
    parentApi.data.currentZone = "1";

    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <TerritorialSynthesis parentApi={parentApi} fromMenu={true} />
            </MemoryRouter>
        );
        wrapper = container;
    });

    const rows = screen.getAllByRole("row");
    // we check we do have the six rows
    // one for header, one for each separator, one per datarow
    expect(rows).toHaveLength(6);
    // we quickly check that the categories are well rendered
    expect(rows[2]).toHaveTextContent(MOCK_STATS_TYPE[0]["theme"]);
    expect(rows[4]).toHaveTextContent(MOCK_STATS_TYPE[1]["theme"]);
    expect(rows[5]).toHaveTextContent(MOCK_STATS_TYPE[1]["theme"]);
});

/**
 * Check that we can export PDF file by click Print button.
 */
test("export pdf", async () => {
    parentApi.data.currentZone = "1";

    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <TerritorialSynthesis parentApi={parentApi} fromMenu={true} />
            </MemoryRouter>
        );
        wrapper = container;
    });

    // we check Print button is available and we click it
    const buttonExportPdf = screen.getByRole("button", {
        name: "Print",
    });
    expect(buttonExportPdf).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(buttonExportPdf);
    });
});

/**
 * Check that TerritorialSynthesis page allow to export data without changing anything inside
 * Export modal.
 */
test("export data", async () => {
    parentApi.data.currentZone = "1";

    window.scrollTo = vi.fn();
    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <TerritorialSynthesis parentApi={parentApi} fromMenu={true} />
            </MemoryRouter>
        );
        wrapper = container;
    });

    // we can access to main Export button and we click it
    const buttonExportData = screen.getByRole("button", {
        name: "Exporter les données",
    });
    expect(buttonExportData).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(buttonExportData);
    });

    // we check we have access to popup export button and that we can click on it
    const popupButton = screen.getByRole("button", {
        name: "Exporter",
    });
    expect(popupButton).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(popupButton);
    });
});

/**
 * Check that we can export data with both pdf and csv file.
 */
test("export data with pdf and csv", async () => {
    parentApi.data.currentZone = "1";

    // we render
    let wrapper;
    await act(async () => {
        const { container } = render(
            <MemoryRouter>
                <TerritorialSynthesis parentApi={parentApi} fromMenu={true} />
            </MemoryRouter>
        );
        wrapper = container;
    });

    const buttonExportData = screen.getByRole("button", {
        name: "Exporter les données",
    });
    expect(buttonExportData).toBeInTheDocument();

    await act(async () => {
        fireEvent.click(buttonExportData);
    });

    const csvFormat = screen.getByRole("radio", {
        name: "CSV",
    });
    expect(csvFormat).toBeInTheDocument();
    expect(csvFormat).not.toBeChecked();
    await act(async () => {
        fireEvent.click(csvFormat);
    });
    expect(csvFormat).toBeChecked();

    const includePdf = screen.getByRole("checkbox", {
        name: "Inclure le PDF méthodologique",
    });
    expect(includePdf).toBeInTheDocument();
    await act(async () => {
        fireEvent.click(includePdf);
    });

    const popupButton = screen.getByRole("button", {
        name: "Exporter",
    });
    expect(popupButton).toBeInTheDocument();
    // fireEvent.click(popupButton);
});
