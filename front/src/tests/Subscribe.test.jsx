/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { act } from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { MemoryRouter } from "react-router-dom";

import Subscribe from "../Components/Subscribe";
import Api from "../Controllers/Api";
import config from "../settings";

vi.mock("../utils.js");
vi.mock("../Controllers/Api.js");

// we disable temporarily captcha
// TODO: mock it?
config.DISABLE_CAPTCHA = true;
const grecaptcha = {
    reset: vi.fn(),
};
Object.defineProperty(window, "grecaptcha", {
    value: grecaptcha,
});

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
        settings: {
            contact_resp_rgpd: "test-rgpd",
        },
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        zonesManager: {
            zones: [{ nom: "region", maille: "epci", libelle: "Région - maille EPCI" }],
            zonesInNewFormat: {
                region: { label: "Région", mailles: { epci: { label: "EPCI" } } },
            },
            getZoneName: () => "Unknown zone",
            getZoneList: () => [{ code: "1", label: "Test Region" }],
        },
        authManager: {
            connected: false,
        },
    },
};

it("renders the Subscribe page", async () => {
    await act(async () => {
        render(
            <MemoryRouter>
                <Subscribe parentApi={parentApi} />
            </MemoryRouter>
        );
    });
    // we check that the title is present
    expect(screen.queryByText("S'inscrire")).toBeInTheDocument();
});

it("subscribes a new account", async () => {
    await act(async () => {
        render(
            <MemoryRouter>
                <Subscribe parentApi={parentApi} />
            </MemoryRouter>
        );
    });

    // Fill the form
    screen.getByLabelText("Prénom*").value = "John";
    screen.getByLabelText("Nom*").value = "Doe";
    screen.getByLabelText("Mail*").value = "test@terristory-nowhere.dev";
    screen.getByLabelText("Organisation*").value = "AURA-EE";
    screen.getByLabelText("Fonction*").value = "Boss";
    screen.getByLabelText("Code postal*").value = "01001";
    for (var checkbox of screen.getAllByRole("checkbox")) checkbox.click();
    // Select first item of territory select
    let territorySelect = screen.getAllByRole("combobox")[0];
    fireEvent.change(territorySelect, { target: { value: "region" } });
    let mailleSelect = screen.getAllByRole("combobox")[2];
    fireEvent.change(mailleSelect, { target: { value: "epci" } });

    // Send
    expect(Api.callApi).not.toHaveBeenCalled();
    let button = screen.getByRole("button", { name: "Envoyer" });
    await act(async () => {
        fireEvent.click(button);
    });
    expect(Api.callApi.mock.calls).toHaveLength(1);

    // Check the content of the request
    expect(Api.callApi.mock.calls[0][0]).toBe(
        config.create_user_url.replace("#region#", "auvergne-rhone-alpes")
    );
    let body = JSON.parse(Api.callApi.mock.calls[0][1]);
    expect(body["mail"]).toBe("test@terristory-nowhere.dev");
    expect(body["rgpd"]).toBe(true);
});
