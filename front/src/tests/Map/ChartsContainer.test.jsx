/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React from "react";
import "@testing-library/jest-dom";
import { render } from "@testing-library/react";
import ChartsContainer from "../../Components/Map/ChartsContainer";

import Charts from "../../Components/Map/Charts";

vi.mock("chartjs-plugin-datalabels", () => {
    return {};
});

vi.mock("../../Components/Map/Charts");

/** @type {import('../../types/types.js').Api} */
const mockParentApi = {
    data: {
        localMapFilter: {},
        zone: { zone: "commune" },
    },
    controller: {},
    callbacks: {},
};

const mockValidIndicatorChartConfigs = {
    /** @type {import("../../types/apiTypes").IndicatorChartConfig[]} */
    charts: [
        {
            type: "pie",
            ordre: 2,
            titre: "Par type d'énergie",
            visible: true,
            categorie: "energie",
            data_type: null,
            indicateur: 15018,
            is_single_select: false,
            description: "",
            chart_options: {},
            datasets: [],
        },
        {
            type: "pie",
            ordre: 1,
            titre: "secteur",
            visible: true,
            categorie: "secteur",
            data_type: null,
            indicateur: 15018,
            is_single_select: false,
            description: "",
            chart_options: {},
            datasets: [],
        },
    ],
};

it("Should not show charts of types switch-button, selection, slider, or invisible charts", () => {
    const chartConfigsThatShouldBeFilteredOut = {
        /** @type {import("../../types/apiTypes").IndicatorChartConfig[]} */
        charts: [
            {
                type: "pie",
                ordre: 1,
                titre: "usage",
                visible: false, // should not create a Chart
                categorie: "usage",
                data_type: null,
                indicateur: 15018,
                is_single_select: false,
                description: "",
                chart_options: {},
                datasets: [],
            },
            ...["switch-button", "selection", "slider"].map((type) => ({
                type: /** @type {import("../../types/apiTypes").ChartType} */ (type),
                titre: `test_${type}`,
                categorie: `category_${type}`,
                is_single_select: false,
                indicateur: 15018,
                data_type: null,
                ordre: 4,
                description: "",
                visible: true,
                chart_options: {},
                datasets: [],
            })),
        ],
    };

    render(
        <ChartsContainer
            parentApi={mockParentApi}
            indicatorConfig={chartConfigsThatShouldBeFilteredOut}
        />
    );

    expect(Charts).not.toHaveBeenCalled();
});

it("should render Charts for valid configs and respect order", () => {
    render(
        <ChartsContainer
            parentApi={mockParentApi}
            indicatorConfig={mockValidIndicatorChartConfigs}
        />
    );
    expect(Charts).toHaveBeenCalledTimes(2);

    expect(Charts).toHaveBeenNthCalledWith(
        1,
        expect.objectContaining({
            id: "15018secteur", // test that id is indicateur+category
            categorie: "secteur",
            chartConfig: expect.objectContaining({
                titre: "secteur",
                ordre: 1,
                type: "pie",
                visible: true,
            }),
        }),
        {}
    );

    expect(Charts).toHaveBeenNthCalledWith(
        2,
        expect.objectContaining({
            id: "15018energie",
            categorie: "energie",
            chartConfig: expect.objectContaining({
                titre: "Par type d'énergie",
                ordre: 2,
                type: "pie",
                visible: true,
            }),
        }),
        {}
    );
});
