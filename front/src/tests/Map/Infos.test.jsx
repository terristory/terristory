/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */

import React, { act } from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";

import Infos from "../../Components/Map/Infos";

vi.mock("../../utils.js");
vi.mock("../../Controllers/Api.js");

const INDICATOR_DEF = {
    id: 1,
    nom: "Indicator name",
    data: "air_polluant_covnm_copie",
    type: "circle",
    color_start: "#29a7a2ff",
    color_end: "#182e85ff",
    unit: "t",
    methodo_pdf: "pdf_url",
    confidentiel: "",
    disabled_for_zone: "",
    only_for_zone: "",
    isratio: false,
    filter: "",
    decimals: 2,
    ui_theme: "Test group",
    data_ratio: null,
    years: [2021, 2020, 2019, 2018],
    display_total: true,
    active: true,
    region: "auvergne-rhone-alpes",
    data_deuxieme_representation: null,
    valeur_filtre_defaut: null,
    titre_legende: null,
    titre_legende_deuxieme_representation: null,
    concatenation: null,
    ordre_ui_theme: 1,
    ordre_indicateur: 1,
    data_type: null,
    donnees_exportables: "",
    titre_dans_infobulle: null,
    afficher_proportion: null,
    titre: null,
    moyenne_ponderee: null,
    afficher_calcul_et_donnees_table: null,
    titre_graphiques_indicateurs: null,
    credits_analysis_producers: "[]",
    credits_data_sources: "[]",
    disabled_for_macro_level: "",
    nb_classes_color_representation: null,
    credits_data_producers: "[]",
    estimated_years: [2019],
    year_selection_input_type: null,
    charts: [
        {
            type: "pie",
            ordre: null,
            titre: "Par type d'énergie",
            visible: true,
            categorie: "energie",
            data_type: null,
            indicateur: 15018,
            is_single_select: false,
        },
        {
            type: "pie",
            ordre: null,
            titre: "secteur",
            visible: true,
            categorie: "secteur",
            data_type: null,
            indicateur: 15018,
            is_single_select: false,
        },
        {
            type: "pie",
            ordre: null,
            titre: "usage",
            visible: true,
            categorie: "usage",
            data_type: null,
            indicateur: 15018,
            is_single_select: false,
        },
    ],
    representation_details: {
        additional_details_text_type_climat:
            "Ceci est un petit texte explicatif pour trouver des détails sur la différence climat réel / normal.",
    },
    representations_possibles: [
        {
            nom: "Courbes empilées",
            arg: "line",
        },
        {
            nom: "Diagramme en barres",
            arg: "bar",
        },
        {
            nom: "Diagrammes circulaires",
            arg: "pie",
        },
        {
            nom: "Radar",
            arg: "radar",
        },
        {
            nom: "pictogramme",
            arg: "marqueur-svg",
        },
        {
            nom: "Nom simple de l'indicateur",
            arg: "analysis-launcher",
        },
        {
            nom: "Carte",
            arg: "map",
        },
    ],
    definition_indicateur: {
        indicateur: "data_table_name",
        coeff: "",
        ratio: "",
        coeff_details: {},
    },
    schema_region: "auvergne_rhone_alpes",
    filtre_initial_modalites_par_categorie: {
        energy: [
            {
                filtre_categorie: "energy.Gas",
            },
            {
                filtre_categorie: "energy.Electricity",
            },
            {
                filtre_categorie: "energy.Oil",
            },
            {
                filtre_categorie: "energy.Unknown",
            },
        ],
        secteur: [
            {
                filtre_categorie: "sector.Residential",
            },
            {
                filtre_categorie: "sector.Transport",
            },
        ],
    },
};

const INDICATOR_SWITCH_DEF = {
    id: 2,
    nom: "Indicator with switch",
    charts: [
        ...INDICATOR_DEF.charts,
        {
            type: "switch-button",
            ordre: null,
            titre: "Par type de climat",
            visible: true,
            categorie: "type_climat",
            is_single_select: true,
            data_type: null,
            indicateur: 15018,
        },
    ],
};
const INDICATOR_SELECT_DEF = {
    id: 3,
    nom: "Indicator with select",
    charts: [
        ...INDICATOR_DEF.charts,
        {
            type: "selection",
            ordre: null,
            titre: "Par type de climat",
            visible: true,
            categorie: "energie",
            is_single_select: true,
            data_type: null,
            indicateur: 15018,
        },
    ],
};

// we disable temporarily captcha

const parentApi = {
    data: {
        region: "auvergne-rhone-alpes",
        settings: {
            contact_resp_rgpd: "test-rgpd",
        },
        zone: {
            zone: "departement",
            maille: "epci",
        },
        currentZone: "01",
        analysis: INDICATOR_DEF.id,
        analysisMeta: {
            creditsDataSources: [
                { url: "/blob", name: "test" },
                { url: "http://terristory.fr", name: "other_example" },
            ],
            creditsDataProducers: [{ url: "/blib", name: "real test" }],
            creditsAnalysisProducers: [],
            sum: 0,
            display_total: false,
            unit: "specific unit",
            moyennePonderee: "",
            data_type: "",
            donnees_exportables: "departement,epci",
            afficherVersionSimple: false,
            ratio_best_year: 2019,
        },
        analysisSelectedYear: undefined,
    },
    callbacks: {
        updateSelectedYearAnalysis: (year) => {
            parentApi.data.analysisSelectedYear = year;
        },
        updateMapFilter: vi.fn(),
    },
    controller: {
        gestionSuiviConsultations: {
            idUtilisateur: -1,
        },
        zonesManager: {
            zones: [{ nom: "region", maille: "epci", libelle: "Région - maille EPCI" }],
            getZoneName: () => "Unknown zone",
            getZoneList: () => [{ code: "1", label: "Test Region" }],
        },
        analysisManager: {
            analysis: [
                INDICATOR_DEF,
                {
                    ...INDICATOR_DEF,
                    ...INDICATOR_SWITCH_DEF,
                },
                {
                    ...INDICATOR_DEF,
                    ...INDICATOR_SELECT_DEF,
                },
            ],
            getDataChart: (id) => {
                if (id === INDICATOR_SWITCH_DEF.id) {
                    return {
                        data: {
                            type: "switch-button",
                            datasets: [
                                {
                                    data: [74252.48, 0],
                                    backgroundColor: ["#0f5117", "#c0d362"],
                                    origBackgroundColor: ["#0f5117", "#c0d362"],
                                    hoverBackgroundColor: ["#0f5117", "#c0d362"],
                                },
                            ],
                            name: "type_climat",
                            labels: ["Climat réel", "Climat normal"],
                            categorie: "type_climat",
                        },
                    };
                } else if (id === INDICATOR_SELECT_DEF.id) {
                    return {
                        data: {
                            type: "selection",
                            ordre: 2,
                            titre: "Vecteur énergétique",
                            categorie: "energie",
                            datasets: [
                                {
                                    data: [0, 0, 0],
                                    backgroundColor: ["#ff0000", "#c0d362", "#616161"],
                                    origBackgroundColor: [
                                        "#ff0000",
                                        "#c0d362",
                                        "#616161",
                                    ],
                                    hoverBackgroundColor: [
                                        "#ff0000",
                                        "#c0d362",
                                        "#616161",
                                    ],
                                },
                            ],
                            labels: [
                                "EnR thermiques",
                                "Organo-carburants",
                                "Électricité",
                            ],
                        },
                    };
                } else {
                    return {};
                }
            },
            getDataMap: () => {},
            getAnalysisLastYear: () => INDICATOR_DEF.years[0],
            getAnalysisMethodoPdf: () => INDICATOR_DEF.methodo_pdf,
            getAnalysisYears: () => INDICATOR_DEF.years,
            getAnalysisEstimatedYears: () => INDICATOR_DEF.estimated_years,
            getAnalysisName: () => INDICATOR_DEF.nom,
            getUnitParamsForIndicator: () => {
                return {
                    unit: "GWh",
                    relevantUnits: [],
                    defaultUnitForThisZoneType: "1",
                };
            },
        },
    },
};

it("renders the Infos module", async () => {
    await act(async () => {
        render(
            <MemoryRouter>
                <Infos parentApi={parentApi} />
            </MemoryRouter>
        );
    });

    expect(screen.getByRole("heading")).toBeInTheDocument();
    expect(screen.getByRole("heading")).toHaveTextContent(INDICATOR_DEF.nom);
    expect(screen.getByRole("heading")).toHaveTextContent(
        parentApi.controller.zonesManager.getZoneName()
    );

    // we check that the link with pdf is present
    expect(
        screen.getByRole("link", {
            name: "Méthodologie",
        })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("link", {
            name: "Méthodologie",
        })
    ).toHaveAttribute(
        "href",
        "/pdf/" + parentApi.data.region + "/" + INDICATOR_DEF.methodo_pdf
    );
});

it("checks that source providers are published", async () => {
    await act(async () => {
        render(
            <MemoryRouter>
                <Infos parentApi={parentApi} />
            </MemoryRouter>
        );
    });

    expect(screen.getByText("Sources des données :")).toBeInTheDocument();
    expect(
        screen.getByRole("link", {
            name: parentApi.data.analysisMeta.creditsDataSources[0].name,
        })
    ).toBeInTheDocument();
    expect(
        screen.getByRole("link", {
            name: parentApi.data.analysisMeta.creditsDataSources[1].name,
        })
    ).toBeInTheDocument();
    // http was added
    expect(
        screen.getByRole("link", {
            name: parentApi.data.analysisMeta.creditsDataSources[0].name,
        })
    ).toHaveAttribute(
        "href",
        "http://" + parentApi.data.analysisMeta.creditsDataSources[0].url
    );
    // raw url
    expect(
        screen.getByRole("link", {
            name: parentApi.data.analysisMeta.creditsDataSources[1].name,
        })
    ).toHaveAttribute("href", parentApi.data.analysisMeta.creditsDataSources[1].url);
});

it("checks that years are displayed", async () => {
    let _rerender;
    await act(async () => {
        const { rerender } = render(
            <MemoryRouter>
                <Infos parentApi={parentApi} />
            </MemoryRouter>
        );
        _rerender = rerender;
    });

    // deep copy of parentApi data
    let parentApiDataCopy = JSON.parse(JSON.stringify(parentApi.data));
    parentApiDataCopy.analysis = 2;

    await act(async () => {
        _rerender(
            <MemoryRouter>
                <Infos parentApi={{ ...parentApi, data: parentApiDataCopy }} />
            </MemoryRouter>
        );
    });
    const yearsSelection = screen.getByLabelText("Année :");
    // we check that the year selection field is present
    expect(yearsSelection).toBeInTheDocument();

    await act(async () => {
        fireEvent.change(yearsSelection, { target: { value: 2019 } });
    });

    const updatedYear = screen.getByLabelText("Année :");
    expect(updatedYear.value).toBe("2019");

    expect(
        screen.getByRole("tooltip", {
            name: "(estimée)",
        })
    ).toBeInTheDocument();
});

it("rerender with a new layer checked", async () => {
    let _rerender;
    await act(async () => {
        const { rerender } = render(
            <MemoryRouter>
                <Infos parentApi={parentApi} />
            </MemoryRouter>
        );
        _rerender = rerender;
    });

    expect(
        screen.queryByRole("heading", {
            name: "Analyse : Ceci est une nouvelle couche Territoire : Unknown zone",
        })
    ).not.toBeInTheDocument();

    await act(async () => {
        _rerender(
            <MemoryRouter>
                <Infos
                    parentApi={{
                        ...parentApi,
                        data: {
                            ...parentApi.data,
                            nomInstallationCourante: "Ceci est une nouvelle couche",
                        },
                    }}
                />
            </MemoryRouter>
        );
    });
    expect(
        screen.getByRole("heading", {
            name: "Analyse : Ceci est une nouvelle couche Territoire : Unknown zone",
        })
    ).toBeInTheDocument();
});

it("rerender with a new POI layer checked", async () => {
    let _rerender;
    await act(async () => {
        const { rerender } = render(
            <MemoryRouter>
                <Infos parentApi={parentApi} />
            </MemoryRouter>
        );
        _rerender = rerender;
    });

    // deep copy of parentApi data
    const poiLayersExample = [
        {
            id: 3,
            nom: "borne_irve",
            label: "Bornes de recharge de véhicules électriques",
            couleur: "#2018b8",
            modifiable: true,
            checked: true,
            afficherStatut: true,
            exportable: true,
            theme: "Mobilité",
            type_installation: null,
            typeGeom: "Point",
            ancrageIcone: "milieu_bas",
            creditsDataSources: [
                {
                    name: "Data.gouv",
                    url: "https://www.data.gouv.fr/fr/datasets/fichier-consolide-des-bornes-de-recharge-pour-vehicules-electriques/",
                },
                {
                    name: "Open Charge Map",
                    url: "https://openchargemap.org/site",
                },
            ],
            creditsDataProducers: [
                {
                    name: "AURA-EE",
                    url: "https://www.auvergnerhonealpes-ee.fr/",
                },
            ],
        },
    ];

    await act(async () => {
        _rerender(
            <MemoryRouter>
                <Infos
                    parentApi={{
                        ...parentApi,
                        data: {
                            ...parentApi.data,
                            poiLayers: poiLayersExample,
                        },
                    }}
                />
            </MemoryRouter>
        );
    });
    expect(
        screen.getByRole("heading", {
            name:
                "Analyse : " +
                poiLayersExample[0].label +
                " Thématique : Mobilité, ... Territoire : Unknown zone",
        })
    ).toBeInTheDocument();
});

it("consult PDF file", async () => {
    await act(async () => {
        render(
            <MemoryRouter>
                <Infos parentApi={parentApi} />
            </MemoryRouter>
        );
    });

    expect(
        screen.getByRole("link", {
            name: "Méthodologie",
        })
    ).toBeInTheDocument();

    const linkButton = screen.getByRole("link", {
        name: "Méthodologie",
    });
    fireEvent.click(linkButton);
});

it("render with a switch button layer checked", async () => {
    await act(async () => {
        render(
            <MemoryRouter>
                <Infos
                    parentApi={{
                        ...parentApi,
                        data: {
                            ...parentApi.data,
                            analysis: INDICATOR_SWITCH_DEF.id,
                            dataLoaded: true,
                        },
                    }}
                />
            </MemoryRouter>
        );
    });

    expect(
        screen.queryByRole("heading", {
            name: "Analyse : Indicator name Territoire : Unknown zone",
        })
    ).toBeInTheDocument();

    // we check we have the filtering button
    expect(screen.getByText("Climat réel")).toBeInTheDocument();
    expect(screen.getByText("Climat normal")).toBeInTheDocument();
    expect(
        screen.queryByRole("checkbox", { id: "input-type_climat" })
    ).toBeInTheDocument();

    // we click on the button
    const switchButton = screen.getByRole("checkbox", {
        id: "input-type_climat",
    });
    fireEvent.click(switchButton);
    expect(parentApi.callbacks.updateMapFilter).toHaveBeenCalled();
    expect(parentApi.callbacks.updateMapFilter).toHaveBeenCalledWith({
        category: "type_climat",
        set: ["type_climat.Climat normal"],
    });
});

it("render with a selection field layer checked", async () => {
    await act(async () => {
        render(
            <MemoryRouter>
                <Infos
                    parentApi={{
                        ...parentApi,
                        data: {
                            ...parentApi.data,
                            analysis: INDICATOR_SELECT_DEF.id,
                            dataLoaded: true,
                        },
                    }}
                />
            </MemoryRouter>
        );
    });

    expect(
        screen.queryByRole("heading", {
            name: "Analyse : Indicator name Territoire : Unknown zone",
        })
    ).toBeInTheDocument();

    // we check we have the filtering button
    expect(
        screen.queryByRole("combobox", { name: "Vecteur énergétique" })
    ).toBeInTheDocument();
    expect(screen.queryAllByRole("option")).toHaveLength(3);

    // we click on the button
    const selectionField = screen.getByRole("combobox", {
        name: "Vecteur énergétique",
    });

    fireEvent.change(selectionField, {
        target: { value: "Organo-carburants" },
    });
    expect(parentApi.callbacks.updateMapFilter).toHaveBeenCalled();
    expect(parentApi.callbacks.updateMapFilter).toHaveBeenCalledWith({
        category: "energie",
        set: ["energie.Organo-carburants"],
    });
});
