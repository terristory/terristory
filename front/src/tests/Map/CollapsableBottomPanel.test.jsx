/*
 * TerriSTORY®
 *
 © Copyright 2022 AURA-EE
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * A copy of the GNU Affero General Public License should be present along
 * with this program at the root of current repository. If not, see
 * http://www.gnu.org/licenses/.
 */
import React from "react";
import "@testing-library/jest-dom";
import { render, screen, fireEvent } from "@testing-library/react";
import CollapsableBottomPanel from "../../Components/Map/CollapsableBottomPanel";

import styles from "../../Components/Map/CollapsableBottomPanel.module.css";

it("renders children correctly", () => {
    render(
        <CollapsableBottomPanel fullWidth={true}>
            <div>Child 1</div>
        </CollapsableBottomPanel>
    );
    expect(screen.getByText(/child/i)).toBeInTheDocument();
});

it("collapses and expands the panel when the toggle button is clicked", () => {
    render(
        <CollapsableBottomPanel fullWidth={true}>
            <div>Child</div>
        </CollapsableBottomPanel>
    );

    const toggleButton = screen.getByRole("button", {
        name: /masquer\/afficher/i,
    });
    expect(toggleButton).toBeInTheDocument();

    const chartsPanel = screen.getByTestId("bottom-panel");

    expect(chartsPanel).toHaveClass(styles.panelOpen);

    fireEvent.click(toggleButton);
    expect(chartsPanel).toHaveClass(styles.panelCollapsed);

    fireEvent.click(toggleButton);
    expect(chartsPanel).toHaveClass(styles.panelOpen);
});

it("adds or remove left padding based on the fullWidth prop", () => {
    const { rerender } = render(
        <CollapsableBottomPanel fullWidth={true}>
            <div>Child</div>
        </CollapsableBottomPanel>
    );

    const scrollableContainer = screen.getByTestId("bottom-panel-scrollable-container");
    expect(scrollableContainer).toHaveClass(styles.fullWidth);

    rerender(
        <CollapsableBottomPanel fullWidth={false}>
            <div>Child</div>
        </CollapsableBottomPanel>
    );

    expect(scrollableContainer).not.toHaveClass(styles.fullWidth);
});
