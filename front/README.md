# TerriSTORY - Front

## Installation

```bash
npm install
cp src/settings-local.js.sample src/settings-local.js
```

## Mode dev

```bash
npm start
```

## Rappel

- passer de l'appli reactapp a une appli webpack : https://github.com/boundlessgeo/sdk/blob/master/docs/getting-started.md#ejecting-create-react-app
